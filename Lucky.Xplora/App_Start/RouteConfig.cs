﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Lucky.Xplora
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //#region Route Fotografico

            ///// <summary>
            ///// Autor: jlucero
            ///// Fecha: 2015-03-27
            ///// </summary>
            //routes.MapRoute(
            //    name: "Fotografico",
            //    url: "Fotografico/{_a}",
            //    defaults: new { controller = "BackusReporting", action = "Fotografico", _a = UrlParameter.Optional }
            //);

            //#endregion

            routes.MapRoute(
                name: "LuckySystemDescarga",
                url: "LuckySystemDescarga/{*path}",
                defaults: new { controller = "LuckySystem", action = "Descarga", path = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "LuckySystem",
                url: "LuckySystem/Explora/{*path}",
                defaults: new { controller = "LuckySystem", action = "Explora", path = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Inventario", action = "Login", id = UrlParameter.Optional }
                //defaults: new { controller = "LogOn", action = "Login", id = UrlParameter.Optional }
                //defaults: new { controller = "BackusDataMercaderista", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}