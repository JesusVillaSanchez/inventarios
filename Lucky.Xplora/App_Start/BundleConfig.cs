﻿using System.Web;
using System.Web.Optimization;

namespace Lucky.Xplora
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Otros
            bundles.Add(new StyleBundle("~/bundles/content").Include(
                        "~/Content/bootstrap/css/bootstrap.min.css",
                        "~/Content/Site.css",
                        "~/Content/ColgReporting.css",
                        "~/Content/animate.css"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/jquery-1.11.3.js",
                        "~/scripts/jquery.alicorp.js",
                        "~/Content/bootstrap/js/bootstrap.min.js",
                        "~/Content/Print/jspdf.js",
                        "~/Content/Print/jspdf.plugin.from_html.js",
                        "~/Content/Print/jspdf.plugin.split_text_to_size.js",
                        "~/Content/Print/jspdf.plugin.standard_fonts_metrics.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/contentscript").Include(
                        "~/Content/bootstrap/js/bootstrap.min.js",
                        "~/Content/Print/jspdf.js",
                        "~/Content/Print/jspdf.plugin.from_html.js",
                        "~/Content/Print/jspdf.plugin.split_text_to_size.js",
                        "~/Content/Print/jspdf.plugin.standard_fonts_metrics.js"

                        ));

            bundles.Add(new StyleBundle("~/bundles/CssUnacem").Include("~/Content/Unacem/UnacemFW.css",
                        "~/Content/Unacem/style/jquery.dataTables.min.css",
                        "~/Content/Unacem/style/fixedColumns.dataTables.min.css",
                        "~/Content/Unacem/style/components.css",
                        "~/Content/jquery.pnotify.default.css",
                        "~/Content/DemoMaps/toggles.css"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/SpUnacem").Include(
                        "~/Content/Unacem/js/jquery.dataTables.min.js",
                        "~/Content/Unacem/js/dataTables.fixedColumns.min.js",
                        "~/Content/Unacem/UnacemFW.js",
                        "~/Content/jquery.inputmask.bundle.min.js",
                        "~/Content/jquery.pnotify.min.js",
                        "~/Scripts/pace.min.js"
                        ));

            bundles.Add(new StyleBundle("~/bundles/CssNestle").Include("~/Content/Nestle/NestleFW.css",
                        "~/Content/Nestle/style/jquery.dataTables.min.css",
                        "~/Content/Nestle/style/fixedColumns.dataTables.min.css",
                        "~/Content/Nestle/style/components.css",
                        "~/Content/jquery.pnotify.default.css",
                        "~/Content/DemoMaps/toggles.css"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/SpNestle").Include(
                        "~/Content/Nestle/js/jquery.dataTables.min.js",
                        "~/Content/Nestle/js/dataTables.fixedColumns.min.js",
                        "~/Content/Nestle/NestleFW.js",
                        "~/Content/jquery.inputmask.bundle.min.js",
                        "~/Content/jquery.pnotify.min.js",
                        "~/Scripts/pace.min.js"
                        ));

            bundles.Add(new StyleBundle("~/bundles/CssCastrol").Include("~/Content/Castrol/CastrolFW.css",
                        "~/Content/Castrol/style/jquery.dataTables.min.css",
                        "~/Content/Castrol/style/fixedColumns.dataTables.min.css",
                        "~/Content/Castrol/style/components.css",
                        "~/Content/jquery.pnotify.default.css",
                        "~/Content/DemoMaps/toggles.css"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/SpCastrol").Include(
                        "~/Content/Castrol/js/jquery.dataTables.min.js",
                        "~/Content/Castrol/js/dataTables.fixedColumns.min.js",
                        "~/Content/Castrol/CastrolFW.js",
                        "~/Content/jquery.inputmask.bundle.min.js",
                        "~/Content/jquery.pnotify.min.js",
                        "~/Scripts/pace.min.js"
                        ));
            #endregion

            #region Kimberly Clark
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkPanel").Include(
                        "~/Content/KimberlyClark/js/Panel.js"
                        ));

            #region Reporting
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkTradeMarketing").Include(
                        "~/Content/KimberlyClark/js/TradeMarketing.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkAgenda").Include(
                        "~/Content/KimberlyClark/js/Agenda.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkPrecioCondicion").Include(
                        "~/Content/KimberlyClark/js/PrecioCondicion.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkMarketing").Include(
                        "~/Content/KimberlyClark/js/Marketing.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkManual").Include(
                        "~/Content/KimberlyClark/js/Manual.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkFotoExito").Include(
                        "~/Content/KimberlyClark/js/FotoExito.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkDriverFuerzaVenta").Include(
                        "~/Content/KimberlyClark/js/DriverFuerzaVenta.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkCatalogoProducto").Include(
                        "~/Content/KimberlyClark/js/CatalogoProducto.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkObjetivo").Include(
                        "~/Content/KimberlyClark/js/Objetivo.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkPrioridad").Include(
                        "~/Content/KimberlyClark/js/Prioridad.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkMecanica").Include(
                        "~/Content/KimberlyClark/js/Mecanica.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkPrecio").Include(
                        "~/Content/KimberlyClark/js/Precio.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkAdministrativo").Include(
                        "~/Content/KimberlyClark/js/Administrativo.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkInnovacion").Include(
                        "~/Content/KimberlyClark/js/Innovacion.js"
                        ));
            #endregion

            #region Data
            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkDataQuiebre").Include(
                        "~/Content/KimberlyClark/js/DataQuiebre.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkDataEvaluacion").Include(
                        "~/Content/KimberlyClark/js/DataEvaluacion.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkDataOsa").Include(
                        "~/Content/KimberlyClark/js/DataOsa.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkDataArriendo").Include(
                        "~/Content/KimberlyClark/js/DataArriendo.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkDataCuestionario").Include(
                        "~/Content/KimberlyClark/js/DataCuestionario.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/KimberlyClarkDataPoseidonPresencia").Include(
                        "~/Content/KimberlyClark/js/DataPoseidonPresencia.js"
                        ));
            #endregion
            #endregion

            #region PhotoSwipe
            bundles.Add(new StyleBundle("~/Content/PhotoSwipe.v4.1.1/PhotoSwipeStyle").Include(
                        "~/Content/PhotoSwipe.v4.1.1/photoswipe.css",
                        "~/Content/PhotoSwipe.v4.1.1/default-skin.css"
                        ));

            bundles.Add(new ScriptBundle("~/Content/PhotoSwipe.v4.1.1/PhotoSwipeScript").Include(
                        "~/Content/PhotoSwipe.v4.1.1/photoswipe.js",
                        "~/Content/PhotoSwipe.v4.1.1/photoswipe-ui-default.js"
                        ));

            bundles.Add(new ScriptBundle("~/Content/PhotoSwipe.v4.1.1/PhotoSwipeScriptFromDOM").Include(
                        "~/Content/PhotoSwipe.v4.1.1/photoswipe-fromdom.js"
                        ));
            #endregion

            #region Is.js
            bundles.Add(new ScriptBundle("~/bundles/is").Include(
                        "~/Content/is/js/is.min.v0.8.0.js"
                        ));
            #endregion

            #region Inputmask
            bundles.Add(new ScriptBundle("~/bundles/inputmask").Include(
                        "~/Content/inputmask.3.3.4.2/jquery.inputmask.bundle.js"
                        ));
            #endregion

            #region Bootstrap Table
            bundles.Add(new StyleBundle("~/Content/bootstrap-table-v1.11.0/BootstrapTableStyle").Include(
                        "~/Content/bootstrap-table-v1.11.0/bootstrap-table.css"
                        ));

            bundles.Add(new ScriptBundle("~/Content/bootstrap-table-v1.11.0/BootstrapTableScript").Include(
                        "~/Content/bootstrap-table-v1.11.0/bootstrap-table.js"
                        ));
            #endregion

            #region Alicorp
            #region AASS
            bundles.Add(new StyleBundle("~/bundles/StyleAlicorpAASSReportingPrecio").Include(
                        "~/Content/Alicorp/AASSReportingPrecio.css"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/ScriptAlicorpAASSReportingPrecio").Include(
                        "~/Content/Alicorp/AASSReportingPrecio.js"
                        ));
            
            #endregion
            #endregion

            #region OpenMaps
            bundles.Add(new StyleBundle("~/bundles/OpenMapStyle").Include(
                        "~/Content/Openmaps/ol.css"
                        ));
            bundles.Add(new StyleBundle("~/bundles/OpenMapScript").Include(
                                    "~/Content/Openmaps/ol.js"
                                    ));
            #endregion

            #region Unacem
            #region Reporting
            bundles.Add(new ScriptBundle("~/bundles/UnacemReportingWorkFlowScript").Include(
                                    "~/Content/Unacem/ReportingWorkFlow.js"
                                    ));
            #endregion
            #endregion

            BundleTable.EnableOptimizations = true;
        }
    }
}