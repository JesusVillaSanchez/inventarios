﻿function fnOnchangeCategoria() {
    var parametro = [];

    parametro.push('19');
    parametro.push($('#_categoria').val());

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Precio',
        type: 'post',
        dataType: 'json',
        data: { __a: '5', __b: parametro.join(',') },
        success: function (__s) {
            $('#_marca').empty();
            $('#_marca').append('<option value="0" selected="selected">-- Todas las marcas --</option>');
           
            $.each(__s, function (i, v) {
                $('#_marca').append('<option value="' + v._a + '">' + v._b + '</option>');
                console.log(__s);
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnchangeAnio() {
    var parametro = [];
    parametro.push('19');
    parametro.push($('#_anio').val())


    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Precio',
        type: 'post',
        dataType: 'json',
        data: { __a: '1', __b: parametro.join(',') },
        success: function (__s) {
            $('#_mes').empty();


            $.each(__s, function (i, v) {
                $('#_mes').append('<option value="' + v._a + '">' + v._b + '</option>');
                console.log(__s);
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOchangeMes(){
    var parametro = [];

    parametro.push('19');
    parametro.push($('#_anio').val())
    parametro.push($('#_mes').val());

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Precio',
        type: 'post',
        dataType: 'json',
        data: { __a: '2', __b: parametro.join(',') },
        success: function (__s) {
            $('#_periodo').empty();
            $('#_periodo').append('<option value="0" selected="selected">-- Todo el mes --</option>');

            $.each(__s, function (i, v) {
                $('#_periodo').append('<option value="' + v._a + '">' + v._b + '</option>');
                console.log(__s);
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}