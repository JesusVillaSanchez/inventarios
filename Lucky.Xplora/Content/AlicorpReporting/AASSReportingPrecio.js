﻿var contenedor = [];
var contenedorGrafico = [];
var contenedorPeriodos = [];

$(function () {
    fnOnClickNavbar('navbar-vertical', false);
})
function fnOnchangeCategoria() {
    var parametro = [];

    parametro.push('19');
    parametro.push($('#_categoria').val());

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Precio',
        type: 'post',
        dataType: 'json',
        data: { __a: '5', __b: parametro.join(',') },
        success: function (__s) {
            $('#_marca').empty();
            $('#_marca').append('<option value="0" selected="selected">-- Todas las marcas --</option>');

            $.each(__s, function (i, v) {
                $('#_marca').append('<option value="' + v._a + '">' + v._b + '</option>');
                
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnchangeAnio() {
    var parametro = [];
    parametro.push('19');
    parametro.push($('#_anio').val())


    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Precio',
        type: 'post',
        dataType: 'json',
        data: { __a: '1', __b: parametro.join(',') },
        success: function (__s) {
            $('#_mes').empty();


            $.each(__s, function (i, v) {
                $('#_mes').append('<option value="' + v._a + '">' + v._b + '</option>');
               
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOchangeMes() {
    var parametro = [];

    parametro.push('19');
    parametro.push($('#_anio').val());
    parametro.push($('#_mes').val());

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Precio',
        type: 'post',
        dataType: 'json',
        data: { __a: '2', __b: parametro.join(',') },
        success: function (__s) {
            $('#_periodo').empty();
            
            $.each(__s, function (i, v) {
                $('#_periodo').append('<option value="' + v._a + '">' + v._b + '</option>');
                
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnclickConsultar() {

    var tabla = $('#table-Precio'),
       tablaBody = $('#table-Precio tbody'),
       tablaHead = $('#table-Precio thead');
   
    var arrayBody = [],
        arrayHead = [],
        arrayHead2 =[],
        parametro = [];


    parametro.push('19');
    parametro.push($('#_periodo').val());
    parametro.push($('#_oficina').val());
    parametro.push($('#_categoria').val());
    parametro.push($('#_marca').val());
    
    var arrayrevers = [];
    var arrayPrecios = [];
    contenedor = [];
    contenedorGrafico = [];
    contenedorPeriodos = [];
    

    $.ajax({
        beforeSend: function (__s) {
            tablaBody.empty();
            tablaHead.empty();
            $('#lucky-load').modal('show');
        },
        url: 'Precio',
        type: 'post',
        dataType: 'json',
        data: { __a: '6', __b: parametro.join(',') },
        success: function (__s) {
            contenedor = __s;

            console.log(contenedor);
            $.each(contenedor, function (i, v) { /*Compania*/
                
                
                $.each(v._c, function (ii, vx) { /*Categoria*/
                    
                    
                     $.each(vx._c, function (iii, vy) {/*Marca*/
                    
                         
                         $.each(vy._c, function (ix, vz) {/*Producto*/

                             if (i == 0 && ii==0 && iii == 0 && ix == 0) {

                                 arrayHead.push('<td class="cls-vacio" style="border-bottom-color:black;" colspan="5"></td>');
                                 arrayHead.push('<th class="cls-vacio"></th>');
                                 arrayHead2.push('<th style="border-right-color:#ddd !important ;"></th>');
                                 arrayHead2.push('<th>Empresa</th>');
                                 arrayHead2.push('<th>Categoria</th>');
                                 arrayHead2.push('<th>Marca</th>');
                                 arrayHead2.push('<th>Producto</th>');
                                 arrayHead2.push('<th class="cls-vacio"></th>');
                               
                             }
                                arrayBody.push('<td> <input type="checkbox" onchange="fnOnChangeCheckBox(\'' + i + '\',\'' + ii + '\',\'' + iii + '\',\'' + ix + '\');"/></td>')
                                arrayBody.push('<td>'+v._b+'</td>');
                                arrayBody.push('<td>' + vx._b + '</td>');
                                arrayBody.push('<td>' + vy._b + '</td>');
                                arrayBody.push('<td>' + vz._b + '</td>');
                                arrayBody.push('<td class="cls-vacio"></td>');
                               
                          
                                arrayrevers = vz._c;
                                $.each(arrayrevers.reverse(), function (iy, va) {/*Periodo*/
                                    
                                    if (i == 0 && ii == 0 && iii == 0 && ix == 0) {
                                            contenedorPeriodos.push(va._b);
                                            arrayHead.push('<th colspan="3" class="col text-center columna-' + iy + '">' + va._b + '</th>');
                                            arrayHead2.push('<th class="col text-center columna-' + iy + '">P.REG</th>');
                                            arrayHead2.push('<th class="col text-center columna-' + iy + '" >P.OF</th>');
                                            arrayHead2.push('<th class="col text-center columna-' + iy + '">%<br>Margen</th>');
                                        
                                    }
                                  
                                    $.each(va._c, function (iz, vb) {//Cadena
                                        if (iz == 0) {/* Inserta solo el total( a confirmar)*/

                                            arrayBody.push('<td class="col text-center columna-' + iy + '">' + vb._c + '</td>');

                                            arrayBody.push('<td class="col text-center columna-' + iy + '">' + vb._d + '</td>');

                                            arrayBody.push('<td class="col text-center columna-' + iy + '">' + vb._e + '</td>');

                                        }
                                        arrayPrecios = va._c;                                        
                                    });                                   
                                });

                                if (ix == 0 && iii == 0 && ii == 0 && i == 0) {
                                   
                                    arrayHead.push('<th class="cls-vacio"></th>');
                                    arrayHead.push('<th colspan="4" class="text-center" style="background-color:#595959;color:white;">ULTIMO PERIODO</th>');

                                    arrayHead2.push('<th class="cls-vacio"></th>');
                                    /*Inserta cabecera AASS*/
                                    arrayHead2.push('<th>TOTTUS</th>');
                                    arrayHead2.push('<th>WONG</th>');
                                    arrayHead2.push('<th>PLAZA VEA</th>');
                                    arrayHead2.push('<th>METRO</th>');
                                }
                                
                                 arrayBody.push('<td class="cls-vacio"></td>');
                                /*Ultimo Periodo*/
                                $.each(arrayrevers, function (ih, vg) {
                                    $.each(arrayPrecios, function (ik, vj) {
                                 
                                        if (ih == 10) {
                                            if (ik != 0) {                                                
                                                    arrayBody.push('<td>' + vj._c  + '</td>');       
                                            }
                                        }
                                    });
                                });
                               
                                tablaBody.append('<tr>' + arrayBody.join('') + '</tr>');
                                arrayBody = [];
                         });
                         
                     });
                    
                });
               
            });

            tablaHead.append('<tr>' +arrayHead.join('') + '</tr>');
            tablaHead.append('<tr>' + arrayHead2.join('') + '</tr>');

            arrayHead = [];
            arrayHead2 = [];           
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });

};
function fnOnChangeCheckBox(__a,__b,__c,__d) {
    var estado = false;

    $.each(contenedorGrafico, function (i, v) {
        if (v.compania == __a && v.categoriaId == __b && v.marcaId==__c && v.productId==__d) {
            contenedorGrafico.splice(i, 1);
            estado = true;
            return;
        }
    });

    if (estado == false) {
        contenedorGrafico.push({ compania: __a, categoriaId: __b, marcaId: __c, productId: __d, productoDescripcion: contenedor[__a]._c[__b]._c[__c]._c[__d]._b, precio: contenedor[__a]._c[__b]._c[__c]._c[__d]._c});
    }
    console.log(contenedorGrafico);
}


