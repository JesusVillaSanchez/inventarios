﻿function fnOnClickDescarga(informe) {
    var Parametro = [];

    if ($('#select-periodo').val() == 0) return;

    Parametro.push(0);
    Parametro.push(0);
    Parametro.push($('#select-periodo').val());
    Parametro.push(0);
    Parametro.push(0);
    Parametro.push(0);

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '14', parametro: Parametro.join(','), descarga: true, informe: informe },
        success: function (s) {
            if (s == null) return;

            window.open(Objeto.Temp + s.Archivo, '_blank');
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickCarga(informe) {
    var FormId = (informe == '202180' ? '#FormArchivo202180' : (informe == 'Descuento' ? '#FormDescuento' : ''));
    var FormArchivo202180 = new FormData($(FormId)[0]);

    FormArchivo202180.append('Opcion', 14);
    FormArchivo202180.append('informe', informe);

    $.ajax({
        url: 'Inicio',
        beforeSend: function () {
            $('#lucky-load').modal('show');
        },
        type: 'post',
        dataType: 'json',
        data: FormArchivo202180,
        cache: false,
        contentType: false,
        processData: false,
        success: function (s) {
            if (s == null) return;

            alert('Se actualizaron ' + s.Resultado + ' registro(s).');
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__e) {
            console.error(__e);
            $('#lucky-load').modal('hide');
        },
    });
}