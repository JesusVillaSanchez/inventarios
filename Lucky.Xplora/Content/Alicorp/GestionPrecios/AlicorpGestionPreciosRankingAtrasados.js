﻿var IndiceFinal = 0,
    ColumnasVisibles = true;

$(function () {
    $('td[class^="col-"],td[class*="col-"],th[class^="col-"],th[class*="col-"]').hide();
})

function fnOnClickBuscar() {
    var Parametro = [],
        ArrayTableRankingLiderTheadUno = [],
        ArrayTableRankingLiderTheadDos = [],
        ArrayTableRankingLiderTbody = [],
        TableRankingLiderThead = $('#TableRankingLider thead'),
        TableRankingLiderTbody = $('#TableRankingLider tbody'),
        TableRankingTradeThead = $('#TableRankingTrade thead'),
        TableRankingTradeTbody = $('#TableRankingTrade tbody');

    Parametro.push($('#select-grupocategoria').val());

    $.ajax({
        beforeSend: function (__s) {
            TableRankingLiderThead.empty();
            TableRankingLiderTbody.empty();
            TableRankingTradeThead.empty();
            TableRankingTradeTbody.empty();

            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '11', parametro: Parametro.join(',') },
        success: function (s) {
            if (s == null && s.length == 0) return;

            $.each(s, function (i, v) {
                if (i == 0) {
                    ArrayTableRankingLiderTheadUno.push('<th class="TbRankingFase"></th><th class="TbRankingFaseEquipo"></th><th class="TbRankingFaseEquipoPersona"></th>');
                    ArrayTableRankingLiderTheadDos.push('<th class="TbRankingFase">EQUIPO</th><th class="TbRankingFaseEquipo">OFICINA</th><th class="TbRankingFaseEquipoPersona">RESPONSABLE</th>');
                }

                $.each(v._b, function (iEquipo, vEquipo) {
                    if (iEquipo == 0) {
                        ArrayTableRankingLiderTbody.push('<td rowspan="' + v._b.length + '" class="TbRankingFase">' + (v._a == 1 ? 'LIDER' : 'TRADE') + '</td>')
                    }

                    ArrayTableRankingLiderTbody.push('<td class="TbRankingFaseEquipo">' + (v._a == 1 ? vEquipo._b : vEquipo._d) + '</td>');
                    ArrayTableRankingLiderTbody.push('<td class="TbRankingFaseEquipoPersona">' + vEquipo._f + '</td>');

                    $.each(vEquipo._g, function (iGrupo, vGrupo) {
                        if (i == 0 && iEquipo == 0) {
                            ArrayTableRankingLiderTheadUno.push('<th colspan="' + vGrupo._b.length + '" class="text-center col-' + iGrupo + '">' + vGrupo._a + '</th>');
                        }

                        $.each(vGrupo._b, function (iPeriodo, vPeriodo) {
                            if (i == 0 && iEquipo == 0) {
                                ArrayTableRankingLiderTheadDos.push('<th class="text-center col-' + iGrupo + '">' + (vPeriodo._a < 1 ? vPeriodo._c : vPeriodo._e) + '</th>');
                            }

                            ArrayTableRankingLiderTbody.push('<td class="text-center col-' + iGrupo + '" ' + (vPeriodo._d == null ? 'style="background-color:#ddd;"' : '') + '>' + (vPeriodo._d == null || vPeriodo._d == '0' ? '' : vPeriodo._d) + '</td>');
                        });
                    });

                    if (v._a == 1) {
                        TableRankingLiderTbody.append('<tr>' + ArrayTableRankingLiderTbody.join('') + '</tr>');
                    } else {
                        TableRankingTradeTbody.append('<tr>' + ArrayTableRankingLiderTbody.join('') + '</tr>');
                    }

                    ArrayTableRankingLiderTbody = [];
                });
            });

            TableRankingLiderThead.append('<tr>' + ArrayTableRankingLiderTheadUno.join('') + '</tr>');
            TableRankingLiderThead.append('<tr>' + ArrayTableRankingLiderTheadDos.join('') + '</tr>');
            TableRankingTradeThead.append('<tr>' + ArrayTableRankingLiderTheadUno.join('') + '</tr>');
            TableRankingTradeThead.append('<tr>' + ArrayTableRankingLiderTheadDos.join('').replace('OFICINA', 'CATEGORIA') + '</tr>');
        },
        complete: function () {
            fnOnClickNavegar(11);

            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickNavegar(indice) {
    var IndiceInicio = 0;

    if (indice == -1) {
        if (ColumnasVisibles == true) {
            for (var i = 0; i < 12; i++) {
                $('.col-' + i).hide();
            }

            ColumnasVisibles = false;
        }
        else
        {
            fnOnClickNavegar(IndiceFinal);

            ColumnasVisibles = true;
        }

        console.log(ColumnasVisibles);

        return;
    } else if (indice > 10) {
        IndiceInicio = 6;
        IndiceFinal = 11;
    } else if (indice < 6) {
        IndiceInicio = 0;
        IndiceFinal = 5;
    }
    else {
        IndiceInicio = indice - 5;
        IndiceFinal = indice;
    }

    for (var i = 0; i < 12; i++) {
        if (i <= IndiceFinal && i >= IndiceInicio) {
            $('.col-' + i).show();
        } else {
            $('.col-' + i).hide();
        }
    }
}