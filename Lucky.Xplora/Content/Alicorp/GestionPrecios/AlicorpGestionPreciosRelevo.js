﻿var WindowSetTimeOut;

$(function () {
    fnOnResizeGrilla();
    fnOnClickNavbar('navbar-vertical', null);

    fnSetTimeOut();

    $('#DivDataGridRelevo').kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: 'Inicio',
                    type: 'post',
                    data: function () {
                        var Parametro = [];

                        fnLocalStoragePeriodo();

                        $('#lucky-load').modal('show');

                        Parametro.push(Objeto.PerId);
                        Parametro.push(0);
                        Parametro.push($('#select-periodo').val());
                        Parametro.push($('#select-oficina').val());
                        Parametro.push($('#select-categoria').val());
                        Parametro.push(1);

                        return {
                            opcion: 3,
                            parametro: Parametro.join(',')
                        };
                    },
                    complete: function (c) {
                        //fnOnChangeCount();

                        fnFaseStatus();
                        $('#lucky-load').modal('hide');
                    },
                    error: function (e) {
                        console.error(e);
                        $('#lucky-load').modal('hide');
                    }
                }
            },
            batch: true,
            schema: {
                model: {
                    fields: {
                        _a: { editable: false },
                        _b: { editable: false },
                        _c: { editable: false },
                        _d: { editable: false },
                        _e: { editable: false },
                        _f: { editable: false },
                        _g: { editable: false },
                        _h: { editable: false },
                        _i: { editable: false },
                        _j: { editable: false },
                        _k: { editable: false },
                        _l: { editable: false },
                        _m: { editable: false },
                        _n: { editable: true, type: 'number' },
                        _o: { editable: true, type: 'number' },
                        _p: { editable: true, type: 'number' },
                        _q: { editable: true, type: 'number' },
                        _r: { editable: true },
                        _s: { editable: false },
                        _t: { editable: false },
                        _w: { editable: false },
                        _x: { editable: false }
                    }
                }
            }
        }),
        dataBound: function (e) {
            var grid = this;

            grid.tbody.find('tr').each(function () {
                var dataItem = grid.dataItem(this),
                    ChatText = fnHtmlGetTexto(dataItem._r),
                    ArrayValidacionVariacionSinComentarioMpm = [],
                    ArrayValidacionVariacionSinComentarioRvt = [];

                if (dataItem._h == 1562) {
                    $(this).children('td:nth-child(5)').css('background-color', '#2E2E2E').css('color', 'white');
                    $(this).children('td:nth-child(7)').css('background-color', '#2E2E2E').css('color', 'white');
                } else {
                    $(this).children('td:nth-child(5)').css('background-color', '#f2f3f4').css('color', '#626567');
                }

                $(this).children('td:nth-child(6)').css('background-color', '#f2f3f4').css('color', '#626567');

                var PorcentajeMpm = (((dataItem._n - dataItem._w) / dataItem._w) * 100),
                    PorcentajeRvt = (((dataItem._q - dataItem._x) / dataItem._x) * 100),
                    VariacionMpm = (PorcentajeMpm < 0 ? PorcentajeMpm * -1 : PorcentajeMpm),
                    VariacionRvt = (PorcentajeRvt < 0 ? PorcentajeRvt * -1 : PorcentajeRvt);

                if (dataItem._w != null && dataItem._n != null) {
                    if ((VariacionMpm >= 50) && ChatText.length == 0 && dataItem._h == 1565) {
                        ArrayValidacionVariacionSinComentarioMpm.push(fnPalabraCapital(dataItem._m + ' (' + parseFloat(VariacionMpm).toFixed(2) + ' %)'));
                        $(this).css('background-color', '#D98880').css('color', '#000');
                    }
                }
                if (dataItem._x != null && dataItem._q != null) {
                    if ((VariacionRvt >= 50) && ChatText.length == 0) {
                        ArrayValidacionVariacionSinComentarioRvt.push(fnPalabraCapital(dataItem._m + ' (' + parseFloat(VariacionRvt).toFixed(2) + ' %)'));
                        $(this).css('background-color', '#D98880').css('color', '#000');
                    }
                }

                if (ArrayValidacionVariacionSinComentarioMpm.length > 0 || ArrayValidacionVariacionSinComentarioRvt.length > 0) {
                    $('#ModalAlertaMaterial').html((ArrayValidacionVariacionSinComentarioMpm.length > 0 ? '<b>MPM</b><br /> >> ' + ArrayValidacionVariacionSinComentarioMpm.join('<br /> >> ') : '') + (ArrayValidacionVariacionSinComentarioRvt.length > 0 ? '<br /><b>RVTA</b><br /> >>' + ArrayValidacionVariacionSinComentarioRvt.join('<br /> >> ') : ''));
                    $('#ModalAlerta').modal('show');
                }
            });
        },
        filterable: { extra: false },
        navigatable: true,
        toolbar: [
            { template: kendo.template($('#KendoTemplateToolBar').html()) },
            { template: '<h4 style="margin-top: 5px;">Indice&nbsp;<label id="_indice"></label> %</h4>' }
        ],
        columns: [
            { field: '_i', width: '70px', title: 'EMPRESA' },
            { field: '_f', width: '90px', title: 'CATEGORIA' },
            { field: '_k', width: '90px', title: 'MARCA' },
            { field: '_m',                title: 'MATERIAL' },
            { field: '_w', width: '80px', title: 'MPM ANT.',    format: '{0:n2}', filterable: false },
            { field: '_x', width: '80px', title: 'RVT ANT.',    format: '{0:n2}', filterable: false },
            { field: '_n', width: '80px', title: 'MPM ACT.',    format: '{0:n2}', filterable: false, editor: function (container, options) {
                var model = options.model;

                if (model._h == 1565) {
                    $('<input id="MpmEditor" data-bind="value:_n" />').appendTo(container).kendoNumericTextBox({
                        min: 0, step: 0.01, change: function (e) {
                            var ChatText = fnHtmlGetTexto(model._r);

                            if (model._w == null) return;
                            var Porcentaje = (((model._n - model._w) / model._w) * 100),
                                Variacion = (Porcentaje < 0 ? Porcentaje * -1 : Porcentaje);
                            if ((Variacion >= 50) && ChatText.length == 0) {
                                alert('Existe una variacion con respecto al mes anterior, favor de comentar el porque de esta variación.');
                            }
                            //var ChatText = fnHtmlGetTexto(model._r);

                            //if (model._w == null) return;
                            //if((model._n !== model._w) && ChatText.length == 0){
                            //    alert('Existe una variacion con respecto al mes anterior, favor de comentar el porque de esta variación.');
                            //}
                            /*fnOnChangeCount();*/
                        }
                    });
                }
            }},
            { field: '_q', width: '80px', title: 'RVTA',        format: '{0:n2}', filterable: false, editor: function (container, options) {
                var model = options.model;

                $('<input id="RvtMayEditor" data-bind="value:_q" />').appendTo(container).kendoNumericTextBox({
                    min: 0, step: 0.01, change: function (e) {
                        var ChatText = fnHtmlGetTexto(model._r);

                        if (model._x == null) return;
                        var Porcentaje = (((model._q - model._x) / model._x) * 100),
                            Variacion = (Porcentaje < 0 ? Porcentaje * -1 : Porcentaje);
                        if ((Variacion >= 50) && ChatText.length == 0) {
                            alert('Existe una variacion con respecto al mes anterior, favor de comentar el porque de esta variación.');
                        }
                        //var ChatText = fnHtmlGetTexto(model._r);

                        //if(model._x == null) return;
                        //if((model._q !== model._x) && ChatText.length == 0){
                        //    alert('Existe una variacion con respecto al mes anterior, favor de comentar el porque de esta variación.');
                        //}
                        /*fnOnChangeCount();*/
                    }
                });
            }},
            { field: '_r', width: '300px', title: 'COMENTARIO', template: '#= _r#', editor: function (container, options) {
                var model = options.model;

                $('<input id="ChatEditor" class="form-control" value="' + fnHtmlGetTexto(model.get('_r')) + '" />').change(function () {
                    model.set('_r', fnHtmlSetTexto(model.get('_r'), $('#ChatEditor').val()));
                    /*fnOnChangeCount();*/
                }).appendTo(container);
            }}
        ],
        editable: true
    }).data('kendoGrid');
})

function fnSetTimeOut() {
    WindowSetTimeOut = window.setTimeout('fnOnClickGraba(\'Guarda\')', 300000);
}
function fnClearTimeOut() {
    clearTimeout(WindowSetTimeOut);
    fnSetTimeOut();
}
function fnOnChangePeriodo() {
    var Parametro = [];

    Parametro.push(Objeto.PerId);
    Parametro.push(0);
    Parametro.push($('#select-periodo').val());
    Parametro.push(0);

    $.ajax({
        beforeSend: function (__s) {
            Objeto.Gca = '';
            fnRemoveOptionSelect('select-oficina');
            fnRemoveOptionSelect('select-categoria');

            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '0', parametro: Parametro.join(',') },
        success: function (s) {
            if (s == null && s.length == 0) return;

            $('#select-oficina option').remove();
            $.each(s, function (i, v) {
                $('#select-oficina').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
            });
        },
        complete: function () {
            $.ajax({
                beforeSend: function (__s) { },
                url: 'Inicio',
                type: 'post',
                dataType: 'json',
                data: { opcion: '1', parametro: Parametro.join(',') },
                success: function (s) {
                    if (s == null && s.length == 0) return;

                    $('#select-categoria option').remove();
                    $.each(s, function (i, v) {
                        $('#select-categoria').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
                    });
                },
                complete: function () {
                    $.ajax({
                        beforeSend: function (__s) { },
                        url: 'Inicio',
                        type: 'post',
                        dataType: 'json',
                        data: { opcion: '10', parametro: Parametro.join(',') },
                        success: function (s) {
                            if (s == null && s.length == 0) return;

                            Objeto.Gca = s._b;
                        },
                        complete: function () { },
                        error: function (e) {
                            console.error(e);
                            $('#lucky-load').modal('hide');
                        }
                    });
                },
                error: function (e) {
                    console.error(e);
                    $('#lucky-load').modal('hide');
                }
            });

            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickBuscar() {
    var _KendoGrid = $('#DivDataGridRelevo').data('kendoGrid');
    _KendoGrid.dataSource.read();
    _KendoGrid.refresh();
}
function fnOnClickGraba(Evento) {
    var ArrayParametro = [],
        Parametro = [],
        //ArrayValidacionVariacionSinComentarioMpm = [],
        //ArrayValidacionVariacionSinComentarioRvt = [],
        KendoGrid = $('#DivDataGridRelevo').data('kendoGrid').dataSource;

    $('#ModalAlertaMaterial').html('');
    $('#ModalAlerta').modal('hide');

    if (KendoGrid.data().length == 0) return;
    if (Evento == 'Envia') {
        if (!confirm('Se esta enviando la información a la siguiente fase, ¿Desea proseguir?')) return;

        //$.each(KendoGrid.data(), function (i, v) {
        //    var ChatText = fnHtmlGetTexto(v._r);

        //    var PorcentajeMpm = (((v._n - v._w) / v._w) * 100),
        //        PorcentajeRvt = (((v._q - v._x) / v._x) * 100),
        //        VariacionMpm = (PorcentajeMpm < 0 ? PorcentajeMpm * -1 : PorcentajeMpm),
        //        VariacionRvt = (PorcentajeRvt < 0 ? PorcentajeRvt * -1 : PorcentajeRvt);

        //    if (v._w != null) {
        //        if ((VariacionMpm >= 50) && ChatText.length == 0 && v._h == 1565) {
        //            ArrayValidacionVariacionSinComentarioMpm.push(fnPalabraCapital(v._m + ' (' + parseFloat(VariacionMpm).toFixed(2) + ' %)'));
        //        }
        //    }
        //    if (v._x != null) {
        //        if ((VariacionRvt >= 50) && ChatText.length == 0) {
        //            ArrayValidacionVariacionSinComentarioRvt.push(fnPalabraCapital(v._m + ' (' + parseFloat(VariacionRvt).toFixed(2) + ' %)'));
        //            console.log(VariacionRvt);
        //        }
        //    }
        //});

        //if (ArrayValidacionVariacionSinComentarioMpm.length > 0 || ArrayValidacionVariacionSinComentarioRvt.length > 0) {
        //    //alert('SKU CON VARIACIÓN EN PRECIO, DEBE COLOCAR COMENTARIO PARA PROSEGUIR: ' + (ArrayValidacionVariacionSinComentarioMpm.length > 0 ? '\n MPM \n >> ' + ArrayValidacionVariacionSinComentarioMpm.join('\n >> ') : '') + (ArrayValidacionVariacionSinComentarioRvt.length > 0 ? '\n RVTA \n >>' + ArrayValidacionVariacionSinComentarioRvt.join('\n >> ') : ''));
        //    $('#ModalAlertaMaterial').html((ArrayValidacionVariacionSinComentarioMpm.length > 0 ? '<b>MPM</b><br /> >> ' + ArrayValidacionVariacionSinComentarioMpm.join('<br /> >> ') : '') + (ArrayValidacionVariacionSinComentarioRvt.length > 0 ? '<br /><b>RVTA</b><br /> >>' + ArrayValidacionVariacionSinComentarioRvt.join('<br /> >> ') : ''));
        //    $('#ModalAlerta').modal('show');
        //    return;
        //}
    }

    $.each(KendoGrid.data(), function (i, v) {
        ArrayParametro.push({
            _a: v._a, _b: v._b, _c: v._c, _d: v._d, _e: v._e, _f: v._f, _g: v._g, _h: v._h, _i: v._i, _j: v._j, _k: v._k, _l: v._l, _m: v._m, _n: v._n, _o: v._o, _p: v._p, _q: v._q,
            _r: (v._r).replace(/,/g, '&#44;'),
            _s: v._s,
            _t: (v._t == true ? 1 : 0),
            _u: v._u, _v: v._v, _w: v._w, _x: v._x, _y: v._y, _z: v._z, _aa: v._aa, _ab: v._ab, _ac: v._ac, _ad: v._ad
        });
    });

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '4', parametro: JSON.stringify(ArrayParametro), evento: Evento },
        success: function (s) {
            if (s == null) return;
            $('#lucky-load').modal('hide');
            //alert('Se grabaron ' + s.Resultado + ' registro(s).');
            alert('Se grabaron satisfactoriamente sus registro(s).');
        },
        complete: function () {
            fnOnClickBuscar();
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });

    fnClearTimeOut();
}
function fnOnResizeGrilla() {
    $('#DivDataGridRelevo').height($(window).height() - $('#l-navbar-horizontal').height() - 1);
}
function fnHtmlGetTexto(ChatModelo) {
    if (ChatModelo == null || ChatModelo.length == 0) return '';

    var Chat = $.parseHTML(ChatModelo),
        ChatTexto = '';

    $.each(Chat, function (iChat, eChat) {
        if ($(eChat).children('ChatActivo').text() == '1') {
            ChatTexto = $(eChat).children('ChatTexto').text();
        }
    });

    return ChatTexto;
}
function fnHtmlSetTexto(ChatModelo, texto) {
    var Chat = $.parseHTML(ChatModelo),
        ChatArray = [];

    $.each(Chat, function (iChat, eChat) {
        if ($(eChat).children('ChatActivo').text() == '1') {
            $(eChat).children('ChatTexto').text(texto);
        }

        ChatArray.push(eChat.outerHTML);
    });

    return ChatArray.join('');
}
function fnOnChangeCount() {
    var kendoGrid = $('#DivDataGridRelevo').data('kendoGrid'),
        Contador = 0,
        Cantidad = 0,
        Porcentaje = 0;

    kendoGrid.tbody.find('tr').each(function () {
        var kendoGridDataItem = kendoGrid.dataItem(this),
            Mpm = (kendoGridDataItem._n == '' ? 0 : kendoGridDataItem._n),
            Rvt = (kendoGridDataItem._q == '' ? 0 : kendoGridDataItem._q),
            Comentario = fnHtmlGetTexto(kendoGridDataItem._r);

        if (kendoGridDataItem._h == 1562) {
            if (Rvt > 0 || Comentario.length > 0) {
                Contador += 1;
            }
        } else {
            if (Mpm == 0 && Rvt == 0 && Comentario.length == 0) {
                Contador += 0;
            } else {
                if (Mpm > 0 && Rvt > 0 && Comentario.length > 0) {
                    Contador += 1;
                } else if (Mpm > 0 && Comentario.length > 0) {
                    Contador += 1;
                } else if (Rvt > 0 && Comentario.length > 0) {
                    Contador += 1;
                } else if (Mpm > 0 && Rvt > 0) {
                    Contador += 1;
                } else if (Comentario.length > 0) {
                    Contador += 1;
                }
            }
        }

        Cantidad += 1;
    });

    Porcentaje = Math.round((Contador / Cantidad) * 100);

    $('#_indice').text(isNaN(Porcentaje) ? 0 : Porcentaje);
}
function fnFaseStatus() {
    var parametro = [];

    parametro.push($('#select-periodo').val());
    parametro.push(Objeto.Gca);

    $.ajax({
        beforeSend: function (__s) {
            //$('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '9', parametro: parametro.join(',') },
        success: function (s) {
            if (s == null) return;

            $.each(s._a, function (i,v) {
                if (Objeto.PerId == v._f) {
                    var Porcentaje = (v._i == null ? 0 : parseFloat(v._i.replace('%', '')));

                    $('#_indice').text(Porcentaje);

                    if (Porcentaje == 100) {
                        fnOnClickGraba('Envia');
                    }
                }
            });
        },
        complete: function () { },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnModalExcel() {
    if ($('#select-periodo').val() == 0) return;

    $('#ModalExcel').modal('show');
}
function fnCargaDescargaInformacion(Evento) {
    var Parametro = [],
        KendoGrid = $('#DivDataGridRelevo').data('kendoGrid').dataSource,
        FormArchivo = new FormData($('#FormArchivo')[0]);

    if (KendoGrid.data().length == 0) return;

    if (Evento == 'Descarga') {
        $.ajax({
            beforeSend: function (__s) {
                $('#lucky-load').modal('show');
            },
            url: 'Relevo',
            type: 'post',
            dataType: 'json',
            data: { parametro: JSON.stringify(KendoGrid.data()), evento: Evento, nombre: Objeto.PerNombre, oficina: $('#select-oficina option:selected').text() },
            success: function (s) {
                if (s == null) return;

                window.open(Objeto.Temp + s.Archivo, '_blank');
            },
            complete: function () {
                $('#lucky-load').modal('hide');
            },
            error: function (e) {
                console.error(e);
                $('#lucky-load').modal('hide');
            }
        });
    } else {
        FormArchivo.append('Evento', 'Carga');

        $.ajax({
            beforeSend: function () {
                $('#lucky-load').modal('show');
            },
            url: 'Relevo',
            type: 'post',
            dataType: 'json',
            data: FormArchivo,
            cache: false,
            contentType: false,
            processData: false,
            success: function (s) {
                if (s == null) return;

                $('#lucky-load').modal('hide');
                //alert('Se grabaron ' + s.Resultado + ' registro(s).');
                alert('Se grabaron satisfactoriamente sus registro(s).');
            },
            complete: function () {
                fnOnClickBuscar();
            },
            error: function (e) {
                console.error(e);
                $('#lucky-load').modal('hide');
            }
        });
    }
}
function fnValidaConteoDuplicados(ArrayElementos, unico) {
    var compressed = [];
    var copy = ArrayElementos.slice(0);
    var arryaElementosUnicos = [];

    for (var i = 0; i < ArrayElementos.length; i++) {
        var myCount = 0;

        for (var w = 0; w < copy.length; w++) {
            if (ArrayElementos[i] == copy[w]) {
                myCount++;
                delete copy[w];
            }
        }

        if (myCount > 0) {
            var a = new Object();
            a.Material = ArrayElementos[i];
            a.Cantidad = myCount;
            compressed.push(a);
        }
    }

    if (unico == true) {
        $.each(compressed, function (i, v) {
            if (v.Cantidad > 1) {
                arryaElementosUnicos.push(v.Material);
            }
        });
    }

    return arryaElementosUnicos;
}
function fnLocalStoragePeriodo() {
    var Periodo = GetQueryString('Rpl');

    if (Periodo) {
        $('#select-periodo').val(Periodo);
        fnOnChangePeriodo();

        history.replaceState({}, '', RemoveURLParameter('Rpl'));
    }
    //if (localStorage.getItem('MiPeriodo') !== null) {
    //    var vkey = localStorage.getItem('MiPeriodo');

    //    $('#select-periodo').val(vkey);
    //    localStorage.removeItem('MiPeriodo');
    //    fnOnChangePeriodo();
    //    //return vkey;
    //}
}
function fnPalabraCapital(palabra) {
    return palabra.charAt(0).toUpperCase() + palabra.slice(1).toLowerCase();
}

$(window).resize(function () {
    fnOnResizeGrilla();
});