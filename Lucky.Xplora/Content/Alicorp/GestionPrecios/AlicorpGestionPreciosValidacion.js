﻿$(function () {
    fnOnResizeGrilla();
    fnOnClickNavbar('navbar-vertical', null);

    $('#DivDataGridRelevo').kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: 'Inicio',
                    type: 'post',
                    data: function () {
                        var Parametro = [];

                        fnLocalStoragePeriodo();

                        $('#lucky-load').modal('show');

                        Parametro.push(0);
                        Parametro.push(Persona.PerId);
                        Parametro.push($('#select-periodo').val());
                        Parametro.push($('#select-oficina').val());
                        Parametro.push($('#select-categoria').val());
                        Parametro.push('2|4');

                        return {
                            opcion: 3,
                            parametro: Parametro.join(',')
                        };
                    },
                    complete: function (c) {
                        $('#lucky-load').modal('hide');
                    },
                    error: function (e) {
                        console.error(e);
                        $('#lucky-load').modal('hide');
                    }
                }
            },
            batch: true,
            schema: {
                model: {
                    fields: {
                        _a: { editable: false },
                        _b: { editable: false },
                        _c: { editable: false },
                        _d: { editable: false },
                        _e: { editable: false },
                        _f: { editable: false },
                        _g: { editable: false },
                        _h: { editable: false },
                        _i: { editable: false },
                        _j: { editable: false },
                        _k: { editable: false },
                        _l: { editable: false },
                        _m: { editable: false },
                        _n: { editable: false, type: 'number' },
                        _o: { editable: false, type: 'number' },
                        _p: { editable: false, type: 'number' },
                        _q: { editable: false, type: 'number' },
                        _r: { editable: true },
                        _s: { editable: false },
                        _t: { editable: true, type: 'boolean' },
                        _u: { editable: false },
                        _v: { editable: false },
                        _w: { editable: false },
                        _x: { editable: false },
                        _y: { editable: false },
                        _z: { editable: false },
                        _aa: { editable: false },
                        _ab: { editable: false },
                        _ac: { editable: false },
                        _ad: { editable: false }
                    }
                }
            }
        }),
        filterable: { extra: false },
        navigatable: true,
        toolbar: [
            { template: kendo.template($('#KendoTemplateToolBar').html()) }
        ],
        columns: [
            { field: '_u', width: '70px', title: 'OFICINA' },
            { field: '_k', width: '90px', title: 'MARCA' },
            { field: '_m', title: 'MATERIAL', template: '# if(_h == 1562) { #<a href="javascript:fnModalDescuentos(\'#=_a#\');">#=_m#</a># } else { #<span>#=_m#</span># } #' },
            { field: '_n', width: '55px', title: 'MPM', format: '{0:n2}', filterable: false },
            { field: '_y', width: '55px', title: '% VAR', format: '{0:n2}', filterable: false },
            { field: '_q', width: '55px', title: 'RVT', format: '{0:n2}', filterable: false },
            { field: '_z', width: '55px', title: '% VAR', format: '{0:n2}', filterable: false },
            { field: '_v', width: '55px', title: 'MG MAY', format: '{0:n2}', filterable: false },
            { field: '_ab', width: '55px', title: '% MG', format: '{0:n2}', filterable: false },
            { field: '_o', width: '55px', title: 'DEX', format: '{0:n2}', filterable: false },
            { field: '_aa', width: '55px', title: '% VAR', format: '{0:n2}', filterable: false },
            {
                field: '_r', width: '300px', title: 'COMENTARIO', template: '#= _r#', editor: function (container, options) {
                    var model = options.model;

                    $('<input id="ChatEditor" class="form-control" value="' + fnHtmlGetTexto(model.get('_r')) + '" />').change(function () {
                        var ChatEditor = $('#ChatEditor').val();

                        model.set('_r', fnHtmlSetTexto(model.get('_r'), ChatEditor));

                        if (ChatEditor.length == 0) {
                            model.set('_t', true);
                        } else {
                            model.set('_t', false);
                        }
                    }).appendTo(container);
                }
            },
            {
                field: '_t', title: ' ', width: '30px', filterable: false, template: function (r) {
                    //if (r._s == '4') {
                    //    return '<i class="fa fa-close" style="color:#000000;font-size: 15px;"></i>';
                    //} else {
                    if (r._t == '1') {
                        return '<i class="fa fa-check" style="color:#2ECD71;font-size: 15px;"></i>';
                    } else {
                        return '<i class="fa fa-close" style="color:#E94C3D;font-size: 15px;"></i>';
                    }
                    //}
                }
            }
            //{ field: '_t', title: ' ', width: '30px', filterable: false, template: '<img src="../Img/# if(_t == "1"){ #marcado.png# }else{ #desmarcado.gif# } #" />' }
        ],
        editable: true
    }).data('kendoGrid');
})

function fnOnChangePeriodo() {
    var Parametro = [];

    Parametro.push(0);
    Parametro.push(Persona.PerId);
    Parametro.push($('#select-periodo').val());
    Parametro.push(0);

    $.ajax({
        beforeSend: function (__s) {
            fnRemoveOptionSelect('select-oficina');
            fnRemoveOptionSelect('select-categoria');

            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '0', parametro: Parametro.join(',') },
        success: function (s) {
            if (s == null && s.length == 0) return;

            $('#select-oficina option').remove();
            $.each(s, function (i, v) {
                $('#select-oficina').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
            });
        },
        complete: function () {
            $.ajax({
                beforeSend: function (__s) { },
                url: 'Inicio',
                type: 'post',
                dataType: 'json',
                data: { opcion: '1', parametro: Parametro.join(',') },
                success: function (s) {
                    if (s == null && s.length == 0) return;

                    $('#select-categoria option').remove();
                    $.each(s, function (i, v) {
                        $('#select-categoria').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
                    });
                },
                complete: function () { },
                error: function (e) {
                    console.error(e);
                    $('#lucky-load').modal('hide');
                }
            });

            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickBuscar() {
    var _KendoGrid = $('#DivDataGridRelevo').data('kendoGrid');
    _KendoGrid.dataSource.read();
    _KendoGrid.refresh();
}
function fnOnClickGraba(Evento) {
    var ArrayParametro = [],
        ArrayValidacionMaterialActivo = [],
        ArrayValidacionMaterialInactivo = [],
        ArrayValidacionMaterialComentarioEstado = [],
        KendoGrid = $('#DivDataGridRelevo').data('kendoGrid').dataSource;

    if (Evento == 'Envia') {
        if (!confirm('Se esta enviando la información a la siguiente fase, ¿Desea proseguir?')) return;

        //$.each(KendoGrid.data(), function (i, v) {
        //    var ChatTexto = fnHtmlGetTexto(v._r);

        //    if (v._t == false && ChatTexto.length == 0 && (v._c == 127 || v._c == 128)) {
        //        ArrayValidacionMaterialInactivo.push(v._m);
        //    }

        //    //if (v._ac == 1 && v._t == true && (v._c == 127 || v._c == 128)) {
        //    //    ArrayValidacionMaterialActivo.push(v._m);
        //    //} else if (v._ac == 1 && v._t == false && ChatTexto.length == 0 && (v._c == 127 || v._c == 128)) {
        //    //    ArrayValidacionMaterialInactivo.push(v._m);
        //    //}

        //    //if (v._ac == 1 && (v._c == 127 || v._c == 128) && (ChatTexto.length > 0 || v._t == true)) {
        //    //    ArrayValidacionMaterialComentarioEstado.push(v._m);
        //    //}
        //});

        ////var ArrayMaterialUnicoActivo = fnValidaConteoDuplicados(ArrayValidacionMaterialActivo);
        //var ArrayMaterialUnicoInactivo = fnValidaConteoDuplicados(ArrayValidacionMaterialInactivo);
        ////var ArrayMaterialUnicoComentarioEstado = fnValidaConteoDuplicados(ArrayValidacionMaterialComentarioEstado);

        ////if (ArrayMaterialUnicoComentarioEstado.length > 0) {
        ////    alert('Error de validacion para los siguientes materiales paras las oficinas de \'Lima\': \n >> ' + ArrayMaterialUnicoComentarioEstado.join('\n >> '));
        ////    return;
        ////}

        ////if (ArrayMaterialUnicoActivo.length > 0 || ArrayMaterialUnicoInactivo.length > 0) {
        ////    alert('Validar y/o Invalidar los siguientes materiales paras las oficinas de \'Lima\': \n >> ' + ArrayMaterialUnicoActivo.concat(ArrayMaterialUnicoInactivo).join('\n >> '));
        ////    return;
        ////}
        //if (ArrayMaterialUnicoInactivo.length > 0) {
        //    alert('Validar y/o Invalidar los siguientes materiales paras las oficinas de \'Lima\': \n >> ' + ArrayMaterialUnicoInactivo.join('\n >> '));
        //    return;
        //}
    }

    $.each(KendoGrid.data(), function (i, v) {
        var ChatTexto = fnHtmlGetTexto(v._r);

        if (Evento == 'Envia') {
            if (v._t == false && ChatTexto.length == 0 && (v._c == 127 || v._c == 128)) {
                v._s = -1;
            } else if (v._s == 4 && v._t == false) {
                v._s = -1;
            }
            else if (v._t == true) {
                v._s = 4;
            }
        }

        //if (v._ac == 1 && v._t == false && ChatTexto.length == 0 && (v._c == 127 || v._c == 128)) {
        //  v._s = -1;
        //}
        //else if (v._ac == 1 && v._s == 2 && v._t == false && ChatTexto.length == 0 && (v._c == 127 || v._c == 128)) {
        //    v._s = -1;
        //} else if (v._ac == 1 && v._s == 4 && (v._c == 127 || v._c == 128)) {
        //    v._s = -1;
        //} else if (v._s == 4 && v._t == false) {
        //    v._s = -1;
        //} else if (v._s == 2 && v._t == true) {
        //    v._s = 4;
        //}

        ArrayParametro.push({
            _a: v._a, _b: v._b, _c: v._c, _d: v._d, _e: v._e, _f: v._f, _g: v._g, _h: v._h, _i: v._i, _j: v._j, _k: v._k, _l: v._l, _m: v._m, _n: v._n, _o: v._o, _p: v._p, _q: v._q,
            _r: (v._r).replace(/,/g, '&#44;'),
            _s: v._s,
            _t: (v._t == true ? 1 : 0),
            _u: v._u, _v: v._v, _w: v._w, _x: v._x, _y: v._y, _z: v._z, _aa: v._aa, _ab: v._ab, _ac: v._ac, _ad: v._ad
        });
    });

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '4', parametro: JSON.stringify(ArrayParametro), evento: Evento },
        success: function (s) {
            if (s == null) return;
            $('#lucky-load').modal('hide');
            alert('Se grabaron ' + s.Resultado + ' registro(s).');
        },
        complete: function () {
            fnOnClickBuscar();
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnResizeGrilla() {
    $('#DivDataGridRelevo').height($(window).height() - $('#l-navbar-horizontal').height() - 1);
}
function fnHtmlGetTexto(ChatModelo) {
    if (ChatModelo == null || ChatModelo.length == 0) return '';

    var Chat = $.parseHTML(ChatModelo),
        ChatTexto = '';

    $.each(Chat, function (iChat, eChat) {
        if ($(eChat).children('ChatActivo').text() == '1') {
            ChatTexto = $(eChat).children('ChatTexto').text();
        }
    });

    return ChatTexto;
}
function fnHtmlSetTexto(ChatModelo, texto) {
    var Chat = $.parseHTML(ChatModelo),
        ChatArray = [];

    $.each(Chat, function (iChat, eChat) {
        if ($(eChat).children('ChatActivo').text() == '1') {
            $(eChat).children('ChatTexto').text(texto);
        }

        ChatArray.push(eChat.outerHTML);
    });

    return ChatArray.join('');
}
function fnValidaConteoDuplicados(ArrayElementos) {
    var compressed = [];
    var copy = ArrayElementos.slice(0);
    var arryaElementosUnicos = [];

    for (var i = 0; i < ArrayElementos.length; i++) {
        var myCount = 0;

        for (var w = 0; w < copy.length; w++) {
            if (ArrayElementos[i] == copy[w]) {
                myCount++;
                delete copy[w];
            }
        }

        if (myCount > 0) {
            var a = new Object();
            a.Material = ArrayElementos[i];
            a.Cantidad = myCount;
            compressed.push(a);
        }
    }

    $.each(compressed, function (i, v) {
        if (v.Cantidad > 1) {
            arryaElementosUnicos.push(v.Material);
        }
    });

    return arryaElementosUnicos;
}
function fnLocalStoragePeriodo() {
    var Periodo = GetQueryString('Rpl');

    if (Periodo) {
        $('#select-periodo').val(Periodo);
        fnOnChangePeriodo();

        history.replaceState({}, '', RemoveURLParameter('Rpl'));
    }
    //if (localStorage.getItem('MiPeriodo') !== null) {
    //    var vkey = localStorage.getItem('MiPeriodo');

    //    $('#select-periodo').val(vkey);

    //    localStorage.removeItem('MiPeriodo');

    //    //return vkey;
    //}
}
function fnOnClickDescarga() {
    var KendoGrid = $('#DivDataGridRelevo').data('kendoGrid').dataSource,
        ArrayParametro = [];

    $.each(KendoGrid.data(), function (i, v) {
        var ChatTexto = fnHtmlGetTexto(v._r);

        ArrayParametro.push({
            _a: v._a, _b: v._b, _c: v._c, _d: v._d, _e: v._e, _f: v._f, _g: v._g, _h: v._h, _i: v._i, _j: v._j, _k: v._k, _l: v._l, _m: v._m, _n: v._n, _o: v._o, _p: v._p, _q: v._q,
            _r: ChatTexto,
            _s: v._s,
            _t: (v._t == true ? 1 : 0),
            _u: v._u, _v: v._v, _w: v._w, _x: v._x, _y: v._y, _z: v._z, _aa: v._aa, _ab: v._ab, _ac: v._ac, _ad: v._ad
        });
    });

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Validacion',
        type: 'post',
        dataType: 'json',
        data: { opcion: 1, parametro: JSON.stringify(ArrayParametro) },
        success: function (s) {
            if (s == null) return;

            window.open(Persona.Temp + s.Archivo, '_blank');
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnModalDescuentos(Id) {
    var Parametro = [],
        TablBodyDescuento = $('#ModalDescuentos table tbody');

    Parametro.push(Id);

    $.ajax({
        beforeSend: function (b) {
            TablBodyDescuento.empty();
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '18', parametro: Parametro.join(',') },
        success: function (s) {
            if (s == null && s.length == 0) return;

            $.each(s, function (i,v) {
                TablBodyDescuento.append('<tr>\
                    <td><input type="hidden" id="InputDescuentoTrpId" value="' + v._a + '" /><input type="number" id="InputDescuentoPrecioMpm" class="form-control" value="' + (v._b == null ? 0 : v._b) + '" /></td>\
                    <td><input type="number" id="InputDescuentoContado" class="form-control" onblur="fnDescuentoCalculo();" value="' + (v._c == null ? 0 : v._c) + '" /></td>\
                    <td><input type="number" id="InputDescuentoLinealidad" class="form-control" onblur="fnDescuentoCalculo();" value="' + (v._d == null ? 0 : v._d) + '" /></td>\
                    <td><input type="number" id="InputDescuentoRebate" class="form-control" onblur="fnDescuentoCalculo();" value="' + (v._e == null ? 0 : v._e) + '" /></td>\
                    <td><input type="number" id="InputDescuentoBonificacion" class="form-control" onblur="fnDescuentoCalculo();" value="' + (v._f == null ? 0 : v._f) + '" /></td>\
                    <td><input type="number" id="InputDescuentoResultado" class="form-control" disabled="disabled" /></td>\
                </tr>');
            });
        },
        complete: function () {
            fnDescuentoCalculo();
            $('#lucky-load').modal('hide');
            $('#ModalDescuentos').modal('show');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnDescuentoCalculo() {
    var InputDescuentoTrpId = $('#InputDescuentoTrpId').val(),
        InputDescuentoPrecioMpm = $('#InputDescuentoPrecioMpm').val(),
        InputDescuentoContado = $('#InputDescuentoContado').val(),
        InputDescuentoLinealidad = $('#InputDescuentoLinealidad').val(),
        InputDescuentoRebate = $('#InputDescuentoRebate').val(),
        InputDescuentoBonificacion = $('#InputDescuentoBonificacion').val(),
        Resultado = 0,
        ResultadoFinal = 0;

    Resultado = (parseFloat(InputDescuentoPrecioMpm) - (parseFloat(InputDescuentoPrecioMpm) * (parseFloat(InputDescuentoContado) + parseFloat(InputDescuentoLinealidad) + parseFloat(InputDescuentoRebate))));
    ResultadoFinal = Resultado - (Resultado * parseFloat(InputDescuentoBonificacion));

    $('#InputDescuentoResultado').val(parseFloat(ResultadoFinal).toFixed(2));
}
function fnModalDescuentosGraba() {
    var InputDescuentoTrpId = $('#InputDescuentoTrpId').val(),
        InputDescuentoPrecioMpm = $('#InputDescuentoPrecioMpm').val(),
        InputDescuentoContado = $('#InputDescuentoContado').val(),
        InputDescuentoLinealidad = $('#InputDescuentoLinealidad').val(),
        InputDescuentoRebate = $('#InputDescuentoRebate').val(),
        InputDescuentoBonificacion = $('#InputDescuentoBonificacion').val();
    var Parametro = [];

    if (InputDescuentoTrpId.length == 0) {
        alert('Llene el Precio lista.');
        return;
    }

    if (InputDescuentoContado.length == 0) {
        alert('Llene el descuento al contado.');
        return;
    }

    if (InputDescuentoLinealidad.length == 0) {
        alert('Llene el descuento de lienalidad.');
        return;
    }

    if (InputDescuentoRebate.length == 0) {
        alert('Llene el descuento de rebate.');
        return;
    }

    if (InputDescuentoBonificacion.length == 0) {
        alert('Llene el descuento de bonificacion.');
        return;
    }

    Parametro.push(InputDescuentoTrpId);
    Parametro.push(InputDescuentoPrecioMpm);
    Parametro.push(InputDescuentoContado);
    Parametro.push(InputDescuentoLinealidad);
    Parametro.push(InputDescuentoRebate);
    Parametro.push(InputDescuentoBonificacion);

    $.ajax({
        beforeSend: function (b) {
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '18', parametro: Parametro.join(','), evento: 'Guarda' },
        success: function (s) {
            if (s == null && s.length == 0) return;

            if (s.Resultado > 0) {
                alert('Los descuentos se grabaron con exito.');
                $('#ModalDescuentos').modal('hide');
            }
        },
        complete: function () {
            fnOnClickBuscar();

            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}

$(window).resize(function () {
    fnOnResizeGrilla();
});