﻿$(function () {
    fnOnResizeGrilla();
    fnOnClickNavbar('navbar-vertical', null);

    $('#DivDataGridRelevo').kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: 'Inicio',
                    type: 'post',
                    data: function () {
                        var Parametro = [];

                        fnLocalStoragePeriodo();

                        $('#lucky-load').modal('show');

                        Parametro.push(Persona.PerId);
                        Parametro.push(0);
                        Parametro.push($('#select-periodo').val());
                        Parametro.push($('#select-oficina').val());
                        Parametro.push($('#select-categoria').val());
                        Parametro.push(3);

                        return {
                            opcion: 3,
                            parametro: Parametro.join(',')
                        };
                    },
                    complete: function (c) {
                        $('#lucky-load').modal('hide');
                    },
                    error: function (e) {
                        console.error(e);
                        $('#lucky-load').modal('hide');
                    }
                }
            },
            batch: true,
            schema: {
                model: {
                    fields: {
                        _a: { editable: false },
                        _b: { editable: false },
                        _c: { editable: false },
                        _d: { editable: false },
                        _e: { editable: false },
                        _f: { editable: false },
                        _g: { editable: false },
                        _h: { editable: false },
                        _i: { editable: false },
                        _j: { editable: false },
                        _k: { editable: false },
                        _l: { editable: false },
                        _m: { editable: false },
                        _n: { editable: true, type: 'number' },
                        _o: { editable: false, type: 'number' },
                        _p: { editable: false, type: 'number' },
                        _q: { editable: true, type: 'number' },
                        _r: { editable: true },
                        _s: { editable: false },
                        _t: { editable: false },
                        _u: { editable: false },
                        _v: { editable: false },
                        _w: { editable: false },
                        _x: { editable: false },
                        _y: { editable: false },
                        _z: { editable: false },
                        _aa: { editable: false },
                        _ab: { editable: false },
                        _ac: { editable: false },
                        _ad: { editable: false }
                    }
                }
            }
        }),
        filterable: { extra: false },
        navigatable: true,
        toolbar: [
            { template: kendo.template($('#KendoTemplateToolBar').html()) }
        ],
        columns: [
            { field: '_f', width: '90px', title: 'CATEGORIA' },
            { field: '_m', title: 'MATERIAL' },
            { field: '_n', width: '55px', title: 'MPM', format: '{0:n2}', filterable: false },
            { field: '_y', width: '55px', title: '% VAR', format: '{0:n2}', filterable: false },
            { field: '_q', width: '55px', title: 'RVT', format: '{0:n2}', filterable: false },
            { field: '_z', width: '55px', title: '% VAR', format: '{0:n2}', filterable: false },
            { field: '_v', width: '55px', title: 'MG MAY', format: '{0:n2}', filterable: false },
            { field: '_ab', width: '55px', title: '% MG', format: '{0:n2}', filterable: false },
            { field: '_r', width: '300px', title: 'COMENTARIO', template: '#= _r#', editor: function (container, options) {
                var model = options.model;

                $('<input id="ChatEditor" class="form-control" value="' + fnHtmlGetTexto(model.get('_r')) + '" />').change(function () {
                    var ChatEditor = $('#ChatEditor').val();

                    model.set('_r', fnHtmlSetTexto(model.get('_r'), ChatEditor));

                    if (ChatEditor.length == 0) {
                        model.set('_t', true);
                    } else {
                        model.set('_t', false);
                    }
                }).appendTo(container);
            }},
            { field: '_t', title: ' ', width: '30px', filterable: false, template: function (r) {
                if (r._t == '1') {
                    return '<i class="fa fa-check" style="color:#2ECD71;font-size: 15px;"></i>';
                } else {
                    return '<i class="fa fa-close" style="color:#E94C3D;font-size: 15px;"></i>';
                }
            }}
        ],
        editable: true
    }).data('kendoGrid');
})

function fnOnChangePeriodo() {
    var Parametro = [];

    Parametro.push(Persona.PerId);
    Parametro.push(0);
    Parametro.push($('#select-periodo').val());
    Parametro.push(0);

    $.ajax({
        beforeSend: function (__s) {
            fnRemoveOptionSelect('select-oficina');
            fnRemoveOptionSelect('select-categoria');

            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '0', parametro: Parametro.join(',') },
        success: function (s) {
            if (s == null && s.length == 0) return;

            $('#select-oficina option').remove();
            $.each(s, function (i, v) {
                $('#select-oficina').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
            });
        },
        complete: function () {
            $.ajax({
                beforeSend: function (__s) { },
                url: 'Inicio',
                type: 'post',
                dataType: 'json',
                data: { opcion: '1', parametro: Parametro.join(',') },
                success: function (s) {
                    if (s == null && s.length == 0) return;

                    $('#select-categoria option').remove();
                    $.each(s, function (i, v) {
                        $('#select-categoria').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
                    });
                },
                complete: function () { },
                error: function (e) {
                    console.error(e);
                    $('#lucky-load').modal('hide');
                }
            });

            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickBuscar() {
    var _KendoGrid = $('#DivDataGridRelevo').data('kendoGrid');
    _KendoGrid.dataSource.read();
    _KendoGrid.refresh();
}
function fnOnClickGraba(Evento) {
    var ArrayParametro = [],
        ArraySubsanacion = [],
        KendoGrid = $('#DivDataGridRelevo').data('kendoGrid').dataSource;

    if (KendoGrid.data().length == 0) return;
    if (Evento == 'Envia') {
        if (!confirm('Se esta enviando la información a la siguiente fase, ¿Desea proseguir?')) return;

        $.each(KendoGrid.data(), function (i, v) {
            var ChatText = fnHtmlGetTexto(v._r);

            if (ChatText.length == 0) {
                ArraySubsanacion.push(fnPalabraCapital(v._m));
            }
        });

        if (ArraySubsanacion.length > 0) {
            alert('Favor de ingresar comentarios para los siguientes elementos:\n >> ' + ArraySubsanacion.join('\n >> '));
            return;
        }
    }

    $.each(KendoGrid.data(), function (i, v) {
        ArrayParametro.push({
            _a: v._a, _b: v._b, _c: v._c, _d: v._d, _e: v._e, _f: v._f, _g: v._g, _h: v._h, _i: v._i, _j: v._j, _k: v._k, _l: v._l, _m: v._m, _n: v._n, _o: v._o, _p: v._p, _q: v._q,
            _r: (v._r).replace(/,/g, '&#44;'),
            _s: v._s,
            _t: (v._t == true ? 1 : 0),
            _u: v._u, _v: v._v, _w: v._w, _x: v._x, _y: v._y, _z: v._z, _aa: v._aa, _ab: v._ab, _ac: v._ac, _ad: v._ad
        });
    });

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '4', parametro: JSON.stringify(ArrayParametro), evento: Evento },
        success: function (s) {
            if (s == null) return;
            $('#lucky-load').modal('hide');
            alert('Se grabaron ' + s.Resultado + ' registro(s).');
        },
        complete: function () {
            fnOnClickBuscar();
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnResizeGrilla() {
    $('#DivDataGridRelevo').height($(window).height() - $('#l-navbar-horizontal').height() - 1);
}
function fnHtmlGetTexto(ChatModelo) {
    if (ChatModelo == null || ChatModelo.length == 0) return '';

    var Chat = $.parseHTML(ChatModelo),
        ChatTexto = '';

    $.each(Chat, function (iChat, eChat) {
        if ($(eChat).children('ChatActivo').text() == '1') {
            ChatTexto = $(eChat).children('ChatTexto').text();
        }
    });

    return ChatTexto;
}
function fnHtmlSetTexto(ChatModelo, texto) {
    var Chat = $.parseHTML(ChatModelo),
        ChatArray = [];

    $.each(Chat, function (iChat, eChat) {
        if ($(eChat).children('ChatActivo').text() == '1') {
            $(eChat).children('ChatTexto').text(texto);
        }

        ChatArray.push(eChat.outerHTML);
    });

    return ChatArray.join('');
}
function fnLocalStoragePeriodo() {
    var Periodo = GetQueryString('Rpl');

    if (Periodo) {
        $('#select-periodo').val(Periodo);
        fnOnChangePeriodo();

        history.replaceState({}, '', RemoveURLParameter('Rpl'));
    }
    //if (localStorage.getItem('MiPeriodo') !== null) {
    //    var vkey = localStorage.getItem('MiPeriodo');

    //    $('#select-periodo').val(vkey);

    //    localStorage.removeItem('MiPeriodo');

    //    //return vkey;
    //}
}
function fnPalabraCapital(palabra) {
    return palabra.charAt(0).toUpperCase() + palabra.slice(1).toLowerCase();
}

$(window).resize(function () {
    fnOnResizeGrilla();
});