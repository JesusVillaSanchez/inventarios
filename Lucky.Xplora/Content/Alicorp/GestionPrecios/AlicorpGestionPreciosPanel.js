﻿$(function () {
    fnVisorStatus();
})

function fnOnClickBuscar() {
    var Conteo = 0,
        ConteoTotal = 0,
        Finalizado = false,
        Parametro = [],
        ArrayTableCronogramaThead = [],
        ArrayTableCronogramaTbody = [],
        ArrayContenedor = [],
        ArrayDefault = [],
        ArrayLider = [],
        ArrayTrade = [],
        TableCronogramaThead = $('#TableCronograma thead'),
        TableCronogramaTbody = $('#TableCronograma tbody'),
        TableBody = $('#table-tbody-informefinal tbody');

    if ($('#select-grupocategoria').val() == 0) {
        alert('Seleccion algun item de plataforma.');
        return;
    }

    Parametro.push($('#select-periodo').val());
    Parametro.push($('#select-grupocategoria').val());
    //Parametro.push(0);

    $.ajax({
        beforeSend: function (__s) {
            fnRemoveOptionSelect('select-lider-trade');

            TableCronogramaThead.empty();
            TableCronogramaTbody.empty();

            $('#TableFase1 tbody').empty();
            $('#TableFase2 tbody').empty();
            $('#TableFase3 tbody').empty();
            $('#TableFase4 tbody').empty();
            $('#TableFaseRankingLider tbody').empty();
            $('#TableFaseRankingTrade tbody').empty();
            $('#PanelFase1 .panel-heading small').html('');
            $('#PanelFase2 .panel-heading small').html('');
            $('#PanelFase3 .panel-heading small').html('');
            $('#PanelFase4 .panel-heading small').html('');
            $('#span-finalizado').text('');

            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '8', parametro: Parametro.join(',') },
        success: function (s) {
            if (s == null) return;

            $.each(s._a, function (i, v) {
                ArrayTableCronogramaTbody.push('<td class="text-center">' + v._b + '</td>');

                $.each(v._d, function (iFecha, vFecha) {
                    if (i == 0) {
                        ArrayTableCronogramaThead.push('<th class="text-center">' + vFecha._b + '</th>');
                    }

                    ArrayTableCronogramaTbody.push('<td style="background-color:#' + vFecha._c + '"></td>');

                    if (vFecha._c == 'E94C3D') {
                        Conteo += 1;
                    } else if (vFecha._c == '000000') {
                        Finalizado = true;
                    }
                });

                if (i == 0) {
                    TableCronogramaThead.append('<tr><th class="text-center">ETAPA</th>' + ArrayTableCronogramaThead.join('') + '<th class="text-center">DÍAS DE RETRASO</th></tr>');
                }
                TableCronogramaTbody.append('<tr>' + ArrayTableCronogramaTbody.join('') + '<td class="text-center" style="background-color: ##ECF0F1;text-shadow: 1px 1px 2px #fff;color: #000;">' + Conteo + '</td></tr>');

                ConteoTotal += Conteo;

                $('#span-total-dias').text('TOTAL DE DÍAS DE RETRASO: ' + ConteoTotal);
                if (Finalizado == true) $('#span-finalizado').text('(FINALIZADO)');

                Conteo = 0;
                ArrayTableCronogramaThead = [];
                ArrayTableCronogramaTbody = [];
            });
            $.each(s._b._a, function (i, v) {
                $('#TableFase1 tbody').append('<tr><td>' + v._c + '</td><td>' + v._g + '</td><td class="text-center">' + (parseFloat(v._i.replace('%', '')) == 100 ? '<i class="fa fa-check" style="color:#2ECD71"></i>' : '<i class="fa fa-remove" style="color:#E94C3D"></i>') + '&nbsp;' + v._i + '</td><td class="text-center">' + v._k + '</td></tr>');
            });
            $.each(s._b._b, function (i, v) {
                $('#TableFase2 tbody').append('<tr><td>' + v._e + '</td><td>' + v._g + '</td><td class="text-center">' + v._i + '</td><td class="text-center">' + v._k + '</td></tr>');
            });
            $.each(s._b._c, function (i, v) {
                $('#TableFase3 tbody').append('<tr><td>' + v._c + '</td><td>' + v._g + '</td><td class="text-center">' + v._i + '&nbsp;' + v._j + '</td><td class="text-center">' + v._k + '</td></tr>');
            });
            $.each(s._b._d, function (i, v) {
                $('#TableFase4 tbody').append('<tr><td>' + v._e + '</td><td>' + v._g + '</td><td class="text-center">' + v._i + '</td><td class="text-center">' + v._k + '</td></tr>');
            });
            $.each(s._b._e, function (i, v) {
                var ArrayJ = v._j.split(',');

                $('#TableFaseRankingLider tbody').append('<tr><td>' + v._c + '</td><td>' + v._g + '</td><td class="text-center">' + ArrayJ[0] + '</td><td class="text-center">' + ArrayJ[1] + '</td><td class="text-center">' + v._k + '</td><td class="text-center">' + v._i + '</td></tr>');
            });
            $.each(s._b._f, function (i, v) {
                var ArrayJ = v._j.split(',');

                $('#TableFaseRankingTrade tbody').append('<tr><td>' + v._e + '</td><td>' + v._g + '</td><td class="text-center">' + ArrayJ[0] + '</td><td class="text-center">' + ArrayJ[1] + '</td><td class="text-center">' + v._k + '</td><td class="text-center">' + v._i + '</td></tr>');
            });
            $.each(s._c, function (i, v) {
                $('#PanelFase' + v._c)
                    .removeClass()
                    .addClass(v._g == 1 ? 'panel panel-primary' : 'panel panel-default');
                $('#PanelFase' + v._c + ' .panel-heading small').html('<i class="fa fa-calendar"></i>&nbsp;' + v._e + ' al ' + v._f);
            });
        },
        complete: function () {
            $.ajax({
                beforeSend: function (b) { },
                url: 'Inicio',
                type: 'post',
                dataType: 'json',
                data: { opcion: '16', parametro: Parametro.join(',') },
                success: function (s) {
                    if (s == null && s.length == 0) return;

                    $.each(s, function (i, v) {
                        if (v._d == 'LIDER') {
                            ArrayLider.push({ Codigo: v._a, Descripcion: v._b, Seleccionado: v._c });
                        } else if (v._d == 'TRADE') {
                            ArrayTrade.push({ Codigo: v._a, Descripcion: v._b, Seleccionado: v._c });
                        } else {
                            ArrayDefault.push({ Codigo: v._a, Descripcion: v._b, Seleccionado: v._c });
                        }
                    });

                    ArrayContenedor.push({ Grupo: '', Personal: ArrayDefault });
                    ArrayContenedor.push({ Grupo: 'LIDER', Personal: ArrayLider });
                    ArrayContenedor.push({ Grupo: 'TRADE', Personal: ArrayTrade });

                    $('#select-lider-trade optgroup').remove();
                    $('#select-lider-trade option').remove();
                    $.each(ArrayContenedor, function (iGrupo, vGrupo) {
                        ArrayDefault = [];
                        $.each(vGrupo.Personal, function (iPersonal, vPersonal) {
                            ArrayDefault.push('<option value="' + vPersonal.Codigo + '" ' + (vPersonal.Seleccionado == 1 ? 'selected="selected"' : '') + '>' + vPersonal.Descripcion + '</option>');
                        });

                        $('#select-lider-trade').append(vGrupo.Grupo.length == 0 ? ArrayDefault.join('') : '<optgroup label="' + vGrupo.Grupo + '">' + ArrayDefault.join('') + '</optgroup>');
                    });
                },
                complete: function () {
                    $('#lucky-load').modal('hide');
                }, error: function (e) {
                    console.error(e);
                    $('#lucky-load').modal('hide');
                }
            });
        }, error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnVisorStatus() {
    var Parametro = [],
        TableVisorStatusTbody = [],
        TableVisorStatus = $('#TableVisorStatus tbody');

    Parametro.push($('#select-periodo').val());
    Parametro.push($('#select-grupocategoria').val());

    $.ajax({
        beforeSend: function (b) {
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '12', parametro: Parametro.join('') },
        success: function (s) {
            if (s == null && s.length == 0) return;

            $.each(s, function (i, v) {
                $.each(v._c, function (iPeriodo, vPeriodo) {
                    var DiasAtrasados = 0,
                        TextoVistaPrevia = "";

                    if (iPeriodo == 0) {
                        TableVisorStatusTbody.push('<td rowspan="' + v._c.length + '">' + v._b + '</td>');
                    }

                    TableVisorStatusTbody.push('<td>' + vPeriodo._b + '</td>');

                    $.each(vPeriodo._c, function (iFase, vFase) {
                        var DivColor = [];

                        $.each(vFase._c, function (iColor, vColor) {
                            if (vColor._a == 'E94C3D') {
                                DiasAtrasados = DiasAtrasados + parseFloat(vColor._b == null ? 0 : vColor._b);
                            }

                            if (vColor._a == 'E94C3D' && vColor._b != null) {
                                //TextoVistaPrevia = v._b + '|' + vPeriodo._b + '|' + vFase._b + '|' + vColor._c + '|' + DiasAtrasados;
                                TextoVistaPrevia = v._b + '|' + vPeriodo._b + '|' + vFase._b + '|' + vColor._c + '|' + vColor._b;
                            }

                            DivColor.push('<div style="background-color:#' + vColor._a + ';width:' + (vColor._b == null ? '' : vColor._b * 10) + 'px;height: 29px;float: left;color: #fff;text-shadow: 1px 1px 1px #000;font-size: 11px;text-align:center;">' + (vColor._b == null ? '' : vColor._b) + '</div>');
                        });

                        TableVisorStatusTbody.push('<td style="padding:0;">' + DivColor.join('') + '</td>');
                    });

                    TableVisorStatusTbody.push('<td class="text-center"><div class="checkbox" style="margin-top: 0px;margin-bottom: 0px;"><label><input name="VisorStatusCheckBox" type="checkbox" data-texto="' + TextoVistaPrevia + '" value=""> ' + DiasAtrasados + '</label></div></td>');

                    TableVisorStatus.append('<tr>' + TableVisorStatusTbody.join('') + '</tr>');
                    TableVisorStatusTbody = [];
                });
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        }, error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
    //var TableVisorStatusTbody = [],
    //    TableVisorStatus = $('#TableVisorStatus tbody');

    //if (ContenedorVisorStatus == null) return;

    //$.each(ContenedorVisorStatus, function (i, v) {
    //    $.each(v._c, function (iPeriodo, vPeriodo) {
    //        var DiasAtrasados = 0,
    //            TextoVistaPrevia = "";

    //        //if (iPeriodo == 0) {
    //        //    TableVisorStatusTbody.push('<td rowspan="' + v._c.length + '">' + v._b + '</td>');
    //        //}

    //        TableVisorStatusTbody.push('<td>' + vPeriodo._b + '</td>');

    //        $.each(vPeriodo._c, function (iFase, vFase) {
    //            var DivColor = [];

    //            $.each(vFase._c, function (iColor, vColor) {
    //                if (vColor._a == 'E94C3D') {
    //                    DiasAtrasados = DiasAtrasados + parseFloat(vColor._b == null ? 0 : vColor._b);
    //                }

    //                if (vColor._a == 'E94C3D' && vColor._b != null) {
    //                    //TextoVistaPrevia = v._b + '|' + vPeriodo._b + '|' + vFase._b + '|' + vColor._c + '|' + DiasAtrasados;
    //                    TextoVistaPrevia = v._b + '|' + vPeriodo._b + '|' + vFase._b + '|' + vColor._c + '|' + vColor._b;
    //                }

    //                DivColor.push('<div style="background-color:#' + vColor._a + ';width:' + (vColor._b == null ? '' : vColor._b * 10) + 'px;height: 29px;float: left;color: #fff;text-shadow: 1px 1px 1px #000;font-size: 11px;text-align:center;">' + (vColor._b == null ? '' : vColor._b) + '</div>');
    //            });

    //            TableVisorStatusTbody.push('<td style="padding:0;">' + DivColor.join('') + '</td>');
    //        });

    //        TableVisorStatusTbody.push('<td class="text-center"><div class="checkbox" style="margin-top: 0px;margin-bottom: 0px;"><label><input name="VisorStatusCheckBox" type="checkbox" data-texto="' + TextoVistaPrevia + '" value=""> ' + DiasAtrasados + '</label></div></td>');

    //        TableVisorStatus.append('<tr>' + TableVisorStatusTbody.join('') + '</tr>');
    //        TableVisorStatusTbody = [];
    //    });
    //});
}
function fnVisorStatusDescarga() {
    var parametro = [];

    $('input[name="VisorStatusCheckBox"]:checked').each(function () {
        var texto = $(this).data('texto');

        if (texto.length > 0)
        {
            parametro.push(texto);
        }
    });

    if (parametro.length == 0) return;

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '12', parametro: parametro.join('&&'), evento: 'Descarga' },
        success: function (s) {
            if (s == null && s.length == 0) return;

            window.open(Objeto.Temp + s.Archivo, '_blank');
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickLiderTrade() {
    if ($('#select-lider-trade').val() == 0) {
        alert('Seleccione un usuario.');
        return;
    }

    window.open(Objeto.Acceso + '?Id=' + $('#select-lider-trade').val());
}