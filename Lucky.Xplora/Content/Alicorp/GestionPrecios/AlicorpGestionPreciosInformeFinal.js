﻿var Contenedor = [];

$(function () {
    fnOnResizeGrilla();

    $('#select-periodo-resumen').multiselect({
        numberDisplayed: 2,
        buttonWidth: '270px',
        maxHeight: 200,
        dropRight: true,
        nonSelectedText: '- Periodo -',
        nSelectedText: ' seleccionados',
        buttonClass: 'btn btn-primary btn-sm',
        includeSelectAllOption: true,
        allSelectedText: '- Periodo -',
        selectAllText: 'Todo'
    });

    $('#select-cadena-descarga').multiselect({
        includeSelectAllOption: true,
        allSelectedText: '- Cadena -',
        buttonWidth: '270px',
        maxHeight: 200,
        dropRight: true,
        nonSelectedText: '- Cadena -',
        nSelectedText: ' seleccionados',
        buttonClass: 'btn btn-primary btn-sm'
    });
    $("#select-cadena-descarga").multiselect('selectAll', false);
    $("#select-cadena-descarga").multiselect('updateButtonText');
    
    $('#divCadena').css('display', 'none');
    $('#divCosto').css('display', 'none');
    $('#divPvp').css('display', 'none');
    $('#var5').change(function () {
        var checked = $(this).is(':checked');

        if (checked == true) {
            $('#divCadena').css('display', 'block');
            $('#divCosto').css('display', 'block');
            $('#divPvp').css('display', 'block');
        } else {
            $('#divCadena').css('display', 'none');
            $('#divCosto').css('display', 'none');
            $('#divPvp').css('display', 'none');

            $('input[type=checkbox][name=Variables][value=13]').removeAttr("checked");
            $('input[type=checkbox][name=Variables][value=14]').removeAttr("checked");
        }
    });

    //alert($('input[type=checkbox][name=Variables][value=13]').is(':checked'));
})

function fnOnResizeGrilla() {
    var HeightPageHeading = $('#container-principal .page-content .page-heading').height() + 52;

    $('#DivInformeFinal').height($(window).height() - ($('#l-navbar-horizontal').height() + HeightPageHeading));
}
function fnOnChangePeriodo() {
    var Parametro = [];

    Parametro.push(0);
    Parametro.push(0);
    Parametro.push($('#select-periodo').val());
    Parametro.push($('#select-grupocategoria').val());

    $.ajax({
        beforeSend: function (__s) {
            fnRemoveOptionSelect('select-oficina');
            fnRemoveOptionSelect('select-categoria');

            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '0', parametro: Parametro.join(',') },
        success: function (s) {
            if (s == null && s.length == 0) return;

            $('#select-oficina option').remove();
            $.each(s, function (i, v) {
                $('#select-oficina').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
            });
        },
        complete: function () {
            $.ajax({
                beforeSend: function (__s) { },
                url: 'Inicio',
                type: 'post',
                dataType: 'json',
                data: { opcion: '1', parametro: Parametro.join(',') },
                success: function (s) {
                    if (s == null && s.length == 0) return;

                    $('#select-categoria option').remove();
                    $.each(s, function (i, v) {
                        $('#select-categoria').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
                    });
                },
                complete: function () { },
                error: function (e) {
                    console.error(e);
                    $('#lucky-load').modal('hide');
                }
            });

            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnChangeGrupoCategoria() {
    var Parametro = [];

    Parametro.push(0);
    Parametro.push(0);
    Parametro.push($('#select-periodo').val());
    Parametro.push($('#select-grupocategoria').val());

    $.ajax({
        beforeSend: function (__s) {
            fnRemoveOptionSelect('select-categoria');

            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '1', parametro: Parametro.join(',') },
        success: function (s) {
            if (s == null && s.length == 0) return;

            $('#select-categoria option').remove();
            $.each(s, function (i, v) {
                $('#select-categoria').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickBuscar() {
    var Parametro = [],
        TableBody = $('#table-tbody-informefinal tbody');

    Parametro.push($('#select-periodo').val());
    Parametro.push($('#select-oficina').val());
    Parametro.push($('#select-categoria').val());
    Parametro.push($('#select-cadena').val());
    Parametro.push($('#select-grupocategoria').val());

    $.ajax({
        beforeSend: function (__s) {
            Contenedor = [];
            TableBody.empty();
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '5', parametro: Parametro.join(',') },
        success: function (s) {
            if (s == null) return;

            Contenedor = s;

            $.each(Contenedor, function (i, v) {
                var TableBodyTd = [];

                TableBodyTd.push('<td>' + (v._u == null ? '' : v._u) + '</td>');
                //TableBodyTd.push('<td><a href="javascript:fnModalHistorialComentario(\'' + i + '\')">' + (v._m == null ? '' : v._m) + '<a></td>');
                TableBodyTd.push('<td onclick="fnModalHistorialComentario(\'' + i + '\');">' + (v._m == null ? '' : v._m) + '</td>');
                TableBodyTd.push('<td>' + (v._n == null ? '' : v._n) + '</td>');
                TableBodyTd.push('<td>' + (v._q == null ? '' : v._q) + '</td>');
                TableBodyTd.push('<td style="' + (v._q == v._x ? 'color: #0000FF;' : (v._q > v._x ? 'color: #00B050;' : 'color: #FF0000;')) + '">' + (v._z == null ? '' : v._z) + '</td>');
                TableBodyTd.push('<td>' + (v._v == null ? '' : v._v) + '</td>');
                TableBodyTd.push('<td>' + (v._ab == null ? '' : v._ab) + '</td>');
                TableBodyTd.push('<td>' + (v._ae == null ? '' : v._ae) + '</td>');
                TableBodyTd.push('<td>' + (v._af == null ? '' : v._af) + '</td>');
                TableBodyTd.push('<td>' + (v._ag == null ? '' : v._ag) + '</td>');
                TableBodyTd.push('<td>' + (v._o == null ? '' : v._o) + '</td>');
                TableBodyTd.push('<td>' + (v._ah == null ? '' : v._ah) + '</td>');
                TableBodyTd.push('<td>' + (v._ai == null ? '' : v._ai) + '</td>');
                TableBodyTd.push('<td>' + (v._aj == null ? '' : v._aj) + '</td>');
                TableBodyTd.push('<td>' + (v._ak == null ? '' : v._ak) + '</td>');
                TableBodyTd.push('<td>' + (v._al == null ? '' : v._al) + '</td>');
                TableBodyTd.push('<td>' + (v._am == null ? '' : v._am) + '</td>');
                TableBodyTd.push('<td>' + (v._an == null ? '' : v._an) + '</td>');
                TableBodyTd.push('<td>' + (v._ao == null ? '' : v._ao) + '</td>');
                TableBodyTd.push('<td>' + (v._ap == null ? '' : v._ap) + '</td>');

                TableBody.append('<tr>' + TableBodyTd.join('') + '</tr>');
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnModalHistorialComentario(indice) {
    var ContenedorItem = Contenedor[indice];

    $('#HistorialComentarioProducto').empty();
    $('#HistorialComentarioTablaChat tbody').empty();

    if (ContenedorItem == null) return;
    if (ContenedorItem._aq == null || ContenedorItem._aq.length == 0) return;

    $('#HistorialComentarioProducto').html(ContenedorItem._m);

    $.each(ContenedorItem._aq, function (i, v) {
        $('#HistorialComentarioTablaChat tbody').append('<tr><td>' + v._c + '</td><td>' + (v._d == null ? '' : v._d) + '</td></tr>');
    });

    $('#ModalHistorialComentario').modal('show');
}
function fnOnClickDescargaInforme() {
    if (Contenedor == null || Contenedor.length == 0) return;

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '5', parametro: JSON.stringify(Contenedor), descarga: true },
        success: function (s) {
            if (s == null) return;

            window.open(Objeto.Temp + s.Archivo, '_blank');
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickGlosario() {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];

    __arrayFotos.push(
            { src: Objeto.Xplora + '/Img/Alicorp/GestionPrecios/Inteligencia_Comercial_Glosario.jpg', w: 811, h: 513 }
        );

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false,
        loop: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
}
function fnOnClickInformeResumen() {

    //alert($('input[type=checkbox][name=Variables][value=5]').is(':checked'));
    $('#id-md-InformeResumen').modal({ backdrop: 'static' });
}
function fnOnClickDescargaInformeDinamico() {
    var ArrayVariables = [],
        ArrayPeriodo = [],
        ArrayCadena = [];

    ArrayPeriodo = $('#select-periodo-resumen').val();
    ArrayCadena = $('#select-cadena-descarga').val();

    $('input[name=\'Variables\']:checked').each(function () {
        ArrayVariables.push($(this).val());
    });

    if (ArrayPeriodo == null || ArrayPeriodo.length == 0) {
        alert('Seleccione periodo(s).');
        return;
    }

    //console.log(ArrayVariables);
    //console.log(jQuery.inArray('5', ArrayVariables));

    if (jQuery.inArray('5', ArrayVariables) != -1 && (ArrayCadena == null || ArrayCadena.length == 0)) {
        alert('Seleccione cadena(s).');
        return;
    }

    if (ArrayVariables.length == 0) {
        alert('Seleccione alguna variable.');
        return;
    }
    //else {
    //    if (jQuery.inArray(5, ArrayVariables) != -1) {

    //        ArrayCadena = $('#select-cadena-descarga').val();
    //        if (ArrayCadena == null || ArrayCadena.length == 0) {
    //            alert('Seleccione cadena(s).');
    //            return;
    //        }
    //        vtercerparametro = ', ' + ArrayCadena.join('|');
    //    }
    //}
    console.log(ArrayPeriodo.join('|') + ',' + ArrayVariables.join('|') + ',' + ArrayCadena.join('|'));

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Inicio',
        type: 'post',
        dataType: 'json',
        data: { opcion: '19', parametro: ArrayPeriodo.join('|') + ',' + ArrayVariables.join('|') + ',' + ArrayCadena.join('|') },
        success: function (s) {
            if (s == null) return;

            if (s.Archivo.length != 0) {
                window.open(Objeto.Temp + s.Archivo, '_blank');
            } else {
                alert('No se encontro informacion en los periodos seleccionados.');
            }
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}

$(window).resize(function () {
    fnOnResizeGrilla();
});