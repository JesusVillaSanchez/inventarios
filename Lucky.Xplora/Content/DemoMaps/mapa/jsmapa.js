﻿/* Inicio Content icon report */
var _vmarcadores = [];
$(function () {
    $("#rightmenu-trigger").hide();
    $('[data-toggle="tooltip"]').tooltip();
    $('.toggle').toggles();
    $(".cls_content_panel_reporte_grafico").hide();
    $("#dv_content_panel_reporte").hide();
    $("#dv_content_panel_minimapa").hide();
    $(".cls_content_fotografico").hide();

    $('#id_toggle_grafico').on('toggle', function (e, active) {
        if (active) {
            $(".cls_content_panel_reporte_grafico").removeClass("fadeOutUp");
            $(".cls_content_panel_reporte_grafico").addClass("fadeInDown").delay(500).queue(function () {
                funct_grafico_cobertura();
                $(this).dequeue();
            });
            $(".cls_content_panel_reporte_grafico").show();
        } else {
            $(".cls_content_panel_reporte_grafico").removeClass("fadeInDown").addClass("fadeOutUp").delay(300).queue(function () {
                $(".cls_content_panel_reporte_grafico").hide();
                $(this).dequeue();
            });;
            event.preventDefault();
        }
    });

    $(".dv_content_incon_rep").hide();
    $("#cls_texto_incon_rep").hide();
    $(".cls_content_fotografico img").hover(function () {
        $(this).addClass("animated pulse");
    }, function () {
        $(this).removeClass("animated pulse");
    });

    $(document).on('click', '#cls_btn_incon_rep', function (e) {
        $(".dv_content_incon_rep").toggle();
        $(".cls_up_btn").toggle();
    });

    $(document).on('click', '.cls_content_incon_rep .dv_content_incon_rep div img', function (e) {

        $("#cls_btn_incon_rep").trigger("click");

        var _vfoto = $(this).data("id");

         if (_vfoto != null) {
             $(".cls_content_fotografico").show();
         } else {
             $("#dv_content_panel_reporte").hide().delay(200).queue(function () {
                 $(this).show();
                 $("#dv_content_panel_minimapa").show();
                 funct_minimapa();
                 $(this).dequeue();
             });
         }



    });

    $(document).on('click','#btn_close_panel_reporte',function(e){
        $("#dv_content_panel_reporte").addClass("animated fadeOutRight").delay(500).queue(function () {
            $(this).hide();
            $(this).dequeue();
        }).delay(500).queue(function () {
            $(this).removeClass(("animated fadeOutRight"));
            $(this).dequeue();
        });
        $("#dv_content_panel_minimapa").addClass("animated fadeOutUp").delay(500).queue(function () {
            $(this).hide();
            $(this).dequeue();
        }).delay(500).queue(function () {
            $(this).removeClass(("animated fadeOutUp"));
            $(this).dequeue();
        });

        

    });

    $(document).on('click', '.cls_content_fotografico .cls_close i', function (e) {
        $(".cls_content_fotografico").removeClass("fadeInUp").addClass("fadeOutDown").delay(300).queue(function () {
            $(".cls_content_fotografico").hide();
            $(this).dequeue();
        }).delay(200).queue(
        function () {
            $(".cls_content_fotografico").removeClass("fadeOutDown");
            $(".cls_content_fotografico").addClass("fadeInUp");
            $(this).dequeue();
        });;
    });

    $(document).on('click', '#sp_center', function (e) {
        //map.setView(new ol.View({
        //    center: ol.proj.transform([-59.5320485, -8.6153994], 'EPSG:4326', 'EPSG:3857'),
        //    zoom: 5
        //}));
        
        var pan = ol.animation.pan({
            duration: 1000,
            source: /** @type {ol.Coordinate} */ (map.getView().getCenter())
        });
        map.beforeRender(pan);
        map.getView().setCenter(ol.proj.transform([-59.5320485, -8.6153994], 'EPSG:4326', 'EPSG:3857'));
        map.getView().setZoom(5);
    });

    //Activacion de infobar
    $(document).on('click', '.cls_op_menu_right', function (e) {
        
        switch ($(this).data("id")) {
            case "capa":
                if ($(this).hasClass("active")) {
                    $("body").removeClass("infobar-active");                    
                    $(this).removeClass("active");
                } else {
                    $("body").addClass("infobar-active");
                    $(".cls_op_menu_right").removeClass("active");
                    $(this).addClass("active");
                }
                $(".cls_filtro_combo").hide();
                $(".cls_filtro_capa").show();
                break;
            case "filtro":                
                if ($(this).hasClass("active")) {
                    $("body").removeClass("infobar-active");
                    $(this).removeClass("active");
                } else {
                    $("body").addClass("infobar-active");
                    $(".cls_op_menu_right").removeClass("active");
                    $(this).addClass("active");
                }
                $(".cls_filtro_capa").hide();
                $(".cls_filtro_combo").show();
                break;
        }

    });

    // Agregar marcador
    $(document).on('click', '.agregar_marcador', function (e) {
        funct_marcadores();
    })
    $(document).on('click', '.quitar_marcador', function (e) {
        //map.removeOverlay(_vmarcadores[0]);
        //map.removeLayer(_layer);
        _layer.setVisible(false);
    });

    $("#dv_fotografico").slick(
        //{
        //centerMode: true,
        //centerPadding: '60px',
        //slidesToShow: 5,
        //responsive: [
        //  {
        //      breakpoint: 768,
        //      settings: {
        //          arrows: false,
        //          centerMode: true,
        //          centerPadding: '40px',
        //          slidesToShow: 3
        //      }
        //  },
        //  {
        //      breakpoint: 480,
        //      settings: {
        //          arrows: false,
        //          centerMode: true,
        //          centerPadding: '40px',
        //          slidesToShow: 1
        //      }
        //  }
        //]
        //}
    );
});

/* Inicio grafico Cobertura */
function funct_grafico_cobertura() {
    $('.cls_content_panel_reporte_grafico').empty();
    $('.cls_content_panel_reporte_grafico').html('<div style="height:100%;"></div>');
    $('.cls_content_panel_reporte_grafico div').highcharts({
        title: {
            text: null
        },
        xAxis: {
            categories: ['PDVs Universo', 'PDVs Coberturados', 'PDVs Visitados']
        }
        , yAxis: [
		{
		    title: {
		        text: null,
		        style: {
		            color: Highcharts.getOptions().colors[2]
		        }
		    }
		}
        ]
		, credits: false
		, series: [{
		    type: 'column',
		    name: 'Cluster A',
		    data: [3, 2, 1]
		}, {
		    type: 'column',
		    name: 'Cluster B',
		    data: [2, 3, 5]
		}, {
		    type: 'column',
		    name: 'Cluster C',
		    data: [4, 3, 3]
		}, {
		    type: 'spline',
		    name: 'Total',
		    data: [9, 8, 9],
		    marker: {
		        lineWidth: 2,
		        lineColor: Highcharts.getOptions().colors[2],
		        fillColor: 'white'
		    }
		}]
    });

}


function funct_minimapa() {
  
    //$("#dv_minimapa").empty();

    //var map = new ol.Map({
    //    layers: [
    //      new ol.layer.Tile({
    //          source: new ol.source.OSM()
    //      }),
    //      new ol.layer.Image({
    //          source: new ol.source.ImageVector({
    //              source: new ol.source.Vector({
    //                  url: '/Scripts/DataJson/data4.json',
    //                  format: new ol.format.GeoJSON()
    //              }),
    //              style: new ol.style.Style({
    //                  fill: new ol.style.Fill({
    //                      color: 'rgba(255, 255, 255, 0.6)'
    //                  }),
    //                  stroke: new ol.style.Stroke({
    //                      color: '#319FD3',
    //                      width: 1
    //                  })
    //              })
    //          })
    //      })
    //    ],
    //    target: 'dv_minimapa',
    //    view: new ol.View({
    //        //center: [-10997148, 4569099],
    //        center: ol.proj.transform([-74.3774414, -9.3189902], 'EPSG:4326', 'EPSG:3857'),
    //        zoom: 5
    //    })
    //});

    //var featureOverlay = new ol.FeatureOverlay({
    //    map: map,
    //    style: new ol.style.Style({
    //        stroke: new ol.style.Stroke({
    //            color: '#f00',
    //            width: 1
    //        }),
    //        fill: new ol.style.Fill({
    //            color: 'rgba(255,0,0,0.1)'
    //        })
    //    })
    //});

    //var highlight;
    //var displayFeatureInfo = function (pixel) {

    //    var feature = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
    //        return feature;
    //    });

    //    var info = document.getElementById('info');
    //    if (feature) {
    //        info.innerHTML = feature.getId() + ': ' + feature.get('name');
    //    } else {
    //        info.innerHTML = '&nbsp;';
    //    }

    //    if (feature !== highlight) {
    //        if (highlight) {
    //            featureOverlay.removeFeature(highlight);
    //        }
    //        if (feature) {
    //            featureOverlay.addFeature(feature);
    //        }
    //        highlight = feature;
    //    }

    //};

    //var displayjs = function (pixel) {

    //    var feature = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
    //        return feature;
    //    });


    //    return feature.getId();
    //};

    //map.on('pointermove', function (evt) {
    //    if (evt.dragging) {
    //        return;
    //    }
    //    var pixel = map.getEventPixel(evt.originalEvent);
    //    displayFeatureInfo(pixel);
    //});

    //map.on('click', function (evt) {
    //    displayFeatureInfo(evt.pixel);
    //    //alert(displayjs(evt.pixel));
    //    //$("#dv_icon_reportes").html("<h2>" + displayjs(evt.pixel) + "</h2>");
    //});

}

function funct_marcadores() {
    var vectorSources = new ol.source.Vector();

    $.ajax({
        dataType: 'json',
        url: '/Scripts/DataJson/PDV_Lima.json',
        success: function (datos) {
            $.each(datos.features, function (key, value) {
                //console.log(value.properties);
                _vmarcadores.push(new ol.Overlay({
                    position: ol.proj.transform([value.properties.Longitud, value.properties.Latitud], 'EPSG:4326', 'EPSG:3857')
                    , element: $('<img src="/Content/DemoMaps/img/marcador/marcador2.png" title="' + value.properties.DIRECCION + ' ">')
                      .css({ cursor: 'pointer' }).on('click', function (e) {
                          alert(value.properties.DIRECCION);
                      })
                }));

                var _punto = new ol.Feature({
                    geometry: new ol.geom.Point(ol.proj.transform([value.properties.Longitud, value.properties.Latitud], 'EPSG:4326', 'EPSG:3857'))
                });
                vectorSources.addFeature(_punto);

                map.addOverlay(_vmarcadores[key]);

            });
        },
        error: function () { alert("Error leyendo fichero jsonP"); }
    });

    map.getView().fitExtent(vectorSources.getExtent(), map.getSize())

}

//$(".cls_content_incon_rep .dv_content_incon_rep div img").hover(function () {
//    $("#cls_texto_incon_rep").show();
//    var _valor = $(this).data("id");
//    $("#cls_texto_incon_rep").html(_valor);
//}, function () {
//    $("#cls_texto_incon_rep").hide();
//});



