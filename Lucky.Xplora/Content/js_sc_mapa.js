﻿var Pyliline;
var Lista_marker = [];
function funct_pintar_pdv(__i,_vdata) {
    map.removePolylines();
    map.removeMarkers();
    _datalinea = [];
    var limits = new google.maps.LatLngBounds();
    //var myLatlng = '';
    //var marcador = '';
    $.each(_vdata, function (key, value) {
        
        //if (value._l == 1) {
            var _vvontent = "";
            if (parseFloat(value._b) != 0 && parseFloat(value._c) != 0) {
                var time = new Date(),
                    ms = time.getMilliseconds();

                _vvontent += '<div style="height: 100px;width:400px;height:220px;position:relative;">';
                _vvontent += '<table><tr><td><strong >PDV</strong></td><td>: ' + value._i + '</td></tr>';
                _vvontent += '<tr><td><strong>Dirección</strong></td><td>: ' + value._m + '</td></tr>';
                _vvontent += '<tr><td><strong>Fecha Visita</strong></td><td>: ' + value._d + ' ' + value._e + ' </td></tr>';
                _vvontent += '<tr><td><strong>Tiempo gestion</strong></td><td>: ' + value._h + '</td></tr>';
                _vvontent += '<tr><td><strong>Estado</strong></td><td>: ' + value._j + '</td></tr>';
                _vvontent += '<tr><td><strong>Motivo</strong></td><td>: ' + value._k + '</td></tr>';
                _vvontent += '<tr><td><strong>Numero</strong></td><td>: ' + value._a + '</td></tr>';
                _vvontent += '</table>';
                _vvontent += '<div>';
                //_vvontent += '<div class="clsFotografico">';

                if (value._o.length > 0) {
                    if (_veq == '002892382010') {
                        _vvontent += '<button type="button" class="btn btn-primary btn-block" onclick="fnOnClickMarker(\'' + __i + '\',\'' + key + '\');"><i class="fa fa-camera"></i>&nbsp;Ver galeria de fotos</button>';
                    } else {
                        if (value._o != null) {
                            $.each(value._o, function (key2, value2) {
                                ////_vvontent += '<a href="' + value2._a + '" rel="prettyPhoto[cls' + value._a + value._n + ms + ']"><img src="' + value2._a + '" width="60" height="60" alt="Categoria" class="img-thumbnail" /></a>';
                                //_vvontent += '<img src="' + value2._a + '" data-index="' + key2 + '" alt="Categoria" class="img-thumbnail cls-img-makert">';
                                _vvontent += '<img src="/Img/Aje/btn-metro.png" data-index="' + key2 + '" alt="Categoria" class="img-thumbnail cls-img-makert">';
                                
                            });
                        }
                    }
                }

                _vvontent += '</div>';
                _vvontent += '</div>';

                var myLatlng = new google.maps.LatLng(value._b, value._c);

                if (value._l == 1) {
                    var marcador = map.addMarker({
                        lat: parseFloat(value._b),
                        lng: parseFloat(value._c),
                        title: value._i,
                        code: value._i,
                        //icon: '/xplora/Img/iconmarkers/green/icon_green' + value._a + '.png',
                        icon: '/Img/iconmarkers/green/icon_green' + value._a + '.png',
                        click: function (e) {
                            //fnClickMarker(value._a + value._n + ms);
                        },
                        infoWindow: {
                            content: _vvontent
                        }
                    });
                    //map.setZoom(15);
                    //Lista_marker.push(marcador);
                    Lista_marker[value._a] = marcador;
                    _datalinea.push(myLatlng);
                    limits.extend(myLatlng);
                } else {
                    var marcador = map.addMarker({
                        lat: parseFloat(value._b),
                        lng: parseFloat(value._c),
                        title: value._i,
                        code: value._i,
                        icon: '/xplora/Img/iconmarkers/red/icon_red.png',

                        click: function (e) {
                        },
                        infoWindow: {
                            content: _vvontent
                        }
                    });
                    //map.setZoom(15);
                    //Lista_marker.push(marcador);
                    Lista_marker[value._a] = marcador;
                  //  _datalinea.push(myLatlng);  // modificar
                }

            }
        //} else {
        //    map.addMarker({
        //        lat: parseFloat(value._b),
        //        lng: parseFloat(value._c),
        //        title: value._i,
        //        code: value._i,
        //        icon: '/Img/iconmarkers/red/icon_red.png',
        //        click: function (e) {
        //        },
        //        infoWindow: {
        //            content: '<p>No visitado.</p>'
        //        }
        //    });
        //}
    });
 
    Pyliline = map.drawPolyline({
        path: _datalinea,
        strokeColor: '#131540',
        strokeOpacity: 0.6,
        strokeWeight: 6
    });
    
    map.fitBounds(limits);

    $('#div_grilla').hide();
    $('#btn_mostrar_div').show();
}

function fnOnClickMarker(__a, __b) {
    var $pdv = _vgdatarecorrido._b[__a]._b[__b],
        $modal = $('#galeria-fotografico'),
        $modalContent = $('.modal-fotografico');

    $modalContent.empty();
    $modal.modal('show');

    $.each($pdv._o, function (i, v) {
        $modalContent.append('<a href="' + v._a + '" rel="prettyPhoto[galeriaFotografico]"><img src="' + v._a + '" alt="P.D.V.: ' + $pdv._i + ' \\ Fecha: ' + v._b + ' \\ Hora: ' + v._c + '" class="img-thumbnail" style=" width: 100px; height: 100px; margin: 10px; " /></a>');
    });

    $('.modal-fotografico:first a[rel^=\'prettyPhoto\']').prettyPhoto({ animation_speed: 'normal' });
}

function funct_mostrar_dato(_vid, _vLati, _vLongi, __cabecera, __detalle) {
    var $contenedor = [],
        $pdv = _vgdatarecorrido._b[__cabecera]._b[__detalle];

    map.removePolylines();
    map.removeMarkers();

    if (parseFloat($pdv._b) != 0 && parseFloat($pdv._c) != 0) {
        $contenedor.push('<div>');
        $contenedor.push('<table><tr><td><strong >PDV</strong></td><td>: ' + $pdv._i + '</td></tr>');
        $contenedor.push('<tr><td><strong>Dirección</strong></td><td>: ' + $pdv._m + '</td></tr>');
        $contenedor.push('<tr><td><strong>Fecha Visita</strong></td><td>: ' + $pdv._d + ' ' + $pdv._e + ' </td></tr>');
        $contenedor.push('<tr><td><strong>Tiempo gestion</strong></td><td>: ' + $pdv._h + '</td></tr>');
        $contenedor.push('<tr><td><strong>Estado</strong></td><td>: ' + $pdv._j + '</td></tr>');
        $contenedor.push('<tr><td><strong>Motivo</strong></td><td>: ' + $pdv._k + '</td></tr>');
        $contenedor.push('<tr><td><strong>Numero</strong></td><td>: ' + $pdv._a + '</td></tr>');
        $contenedor.push('</table>');
        $contenedor.push('<div>');

        if ($pdv._o.length > 0) {
            if (_veq == '002892382010') {
                $contenedor.push('<button type="button" class="btn btn-primary btn-block" onclick="fnOnClickMarker(\'' + __cabecera + '\',\'' + __detalle + '\');"><i class="fa fa-camera"></i>&nbsp;Ver galeria de fotos</button>');
            } else {
                if ($pdv._o != null) {
                    $.each($pdv._o, function (key2, value2) {
                        console.log(value2._a);
                        $contenedor.push('<img src="' + value2._a + '" data-index="' + key2 + '" alt="Categoria" class="img-thumbnail cls-img-makert">');
                    });
                }
            }
        }

        $contenedor.push('</div>');
        $contenedor.push('</div>');

        var myLatlng = new google.maps.LatLng($pdv._b, $pdv._c);
        console.log('/Img/iconmarkers/' + ($pdv._l == 1 ? 'green/icon_green' + $pdv._a : 'red/icon_red') + '.png');
        var marcador = map.addMarker({
            lat: parseFloat($pdv._b),
            lng: parseFloat($pdv._c),
            title: $pdv._i,
            code: $pdv._i,
            icon: '/xplora/Img/iconmarkers/' + ($pdv._l == 1 ? 'green/icon_green' + $pdv._a : 'red/icon_red') + '.png',
            click: function (e) { },
            infoWindow: {
                content: $contenedor.join('')
            }
        });

        Lista_marker[$pdv._a] = marcador;

        map.setCenter(parseFloat($pdv._b), parseFloat($pdv._c));
        map.setZoom(15);

        $('#div_grilla').hide();
        $('#btn_mostrar_div').show();
    }
}