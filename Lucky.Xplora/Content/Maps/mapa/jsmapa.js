﻿/* Inicio Content icon report */
//var _vmarcadores = [];
var _vmpas_g_ubigeo;
$(function () {
    $("#rightmenu-trigger").hide();
    $('[data-toggle="tooltip"]').tooltip();
    $('.toggle').toggles();
    $(".cls_content_panel_reporte_grafico").hide();
    $("#dv_content_panel_reporte").hide();
    //$("#dv_content_panel_minimapa").hide();
    $(".cls_content_fotografico").hide();

    $('#id_toggle_grafico').on('toggle', function (e, active) {
        if (active) {
            $(".cls_content_panel_reporte_grafico").removeClass("fadeOutUp");
            $(".cls_content_panel_reporte_grafico").addClass("fadeInDown").delay(500).queue(function () {
                funct_grafico_cobertura();
                $(this).dequeue();
            });
            $(".cls_content_panel_reporte_grafico").show();
        } else {
            $(".cls_content_panel_reporte_grafico").removeClass("fadeInDown").addClass("fadeOutUp").delay(300).queue(function () {
                $(".cls_content_panel_reporte_grafico").hide();
                $(this).dequeue();
            });;
            event.preventDefault();
        }
    });

    $(".dv_content_incon_rep").hide();
    $("#cls_texto_incon_rep").hide();
    $(".cls_content_fotografico img").hover(function () {
        $(this).addClass("animated pulse");
    }, function () {
        $(this).removeClass("animated pulse");
    });

    $(document).on('click', '#cls_btn_incon_rep', function (e) {
        $(".dv_content_incon_rep").toggle();
        $(".cls_up_btn").toggle();
    });

    // Menu de reportes -- parte inferior
    $(document).on('click', '.cls_content_incon_rep .dv_content_incon_rep div img', function (e) {
        $("#cls_btn_incon_rep").trigger("click");
        var _vid_elemento = $(this).data("id");
        funct_menu_inferior(_vid_elemento);
    });
    $(document).on('click', '.cls_button_horizontal span', function (e) {
        var _vid_elemento = $(this).data("id");
        funct_menu_inferior(_vid_elemento);
    });

    $(document).on('click','#btn_close_panel_reporte',function(e){
        $("#dv_content_panel_reporte").addClass("animated fadeOutRight").delay(500).queue(function () {
            $(this).hide();
            $(this).dequeue();
        }).delay(500).queue(function () {
            $(this).removeClass(("animated fadeOutRight"));
            $(this).dequeue();
        });
        $("#dv_content_panel_minimapa").addClass("animated fadeOutUp").delay(500).queue(function () {
            $(this).hide();
            $(this).dequeue();
        }).delay(500).queue(function () {
            $(this).removeClass(("animated fadeOutUp"));
            $(this).dequeue();
        });
    });

    $(document).on('click', '.cls_content_fotografico .cls_close i', function (e) {
        $(".cls_content_fotografico").removeClass("fadeInUp").addClass("fadeOutDown").delay(300).queue(function () {
            $(".cls_content_fotografico").hide();
            $(this).dequeue();
        }).delay(200).queue(
        function () {
            $(".cls_content_fotografico").removeClass("fadeOutDown");
            $(".cls_content_fotografico").addClass("fadeInUp");
            $(this).dequeue();
        });;
    });

    $(document).on('click', '#sp_center', function (e) {
        

    });

    //Activacion de infobar
    $(document).on('click', '.cls_op_menu_right', function (e) {
        
        switch ($(this).data("id")) {
            case "capa":
                if ($(this).hasClass("active")) {
                    $("body").removeClass("infobar-active");                    
                    $(this).removeClass("active");
                } else {
                    $("body").addClass("infobar-active");
                    $(".cls_op_menu_right").removeClass("active");
                    $(this).addClass("active");
                }
                $(".cls_filtro_combo").hide();
                $(".cls_filtro_capa").show();
                break;
            case "filtro":                
                if ($(this).hasClass("active")) {
                    $("body").removeClass("infobar-active");
                    $(this).removeClass("active");
                } else {
                    $("body").addClass("infobar-active");
                    $(".cls_op_menu_right").removeClass("active");
                    $(this).addClass("active");
                }
                $(".cls_filtro_capa").hide();
                $(".cls_filtro_combo").show();
                break;
        }

    });

    // Agregar marcador
    $(document).on('click', '.agregar_marcador', function (e) {
        funct_marcadores();
    })
    $(document).on('click', '.quitar_marcador', function (e) {
        //map.removeOverlay(_vmarcadores[0]);
        //map.removeLayer(_layer);
        _layer.setVisible(false);
    });

    $("#dv_fotografico").slick();

    //Button 
    $(document).on('click', '#btn_filtros_rep', function (e) {
        $("#dv_rep_panel_filtros").toggle();
    });
});

/* function para menu reportes inferior */
function funct_menu_inferior(_vid) {

    if (funct_validar_select() != 1) {

        if (_vid == "foto") {
            $(".cls_content_fotografico").show();
        } else {
            $("#dv_content_panel_reporte").hide().delay(200).queue(function () {
                $('#dv_rep_panel_body').html("");
                $(this).show();
                funct_reporte(_vid);
                //$("#dv_content_panel_minimapa").show();
                //funct_minimapa();
                $(this).dequeue();
            });
        }
    }
}

// Filtros
$(function () {

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a : "1"
            , __b: _g_veq
            , __c: "cbo_anio"
        },
        success: function (response) {
            $('#cbo_anio').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "2"
            , __b: ""
            , __c: "cbo_mes"
        },
        success: function (response) {
            $('#cbo_mes').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "4"
            , __b: _g_veq
            , __c: "cbo_giro"
        },
        success: function (response) {
            $('#cbo_tipogiro').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });

    $(document).on('change', '#cbo_anio', function (e) {
        funct_periodo();
    });
    $(document).on('change', '#cbo_mes', function (e) {
        funct_periodo();
    });


    //Filtros right
    $(document).on('change', '#cbo_categoria_right', function (e) {
        v_reporte = $(this).data("id");
        v_marca = $(this).data("marca");
        v_repreal = $(this).data("repreal");
        if (v_marca != null && v_marca != "") {
            funct_marca('cbo_marca_right', v_reporte, $(this).val(), v_repreal)
        } else {
            switch (v_repreal)
            {
                case 21:
                    funct_sod_panel();
                    break;
                case 66:
                    funct_actcompe_panel();
                    break;
            }
        }
    });
    $(document).on('change', '#cbo_marca_right', function (e) {
        v_repreal = $("#cbo_marca_right").data("repreal");
        switch (v_repreal) {
            case 58:
                    funct_presencia_panel();
                break;
            case 19:
                    funct_precio_panel();
                break;
        }
    });
    $(document).on('change', '#cbo_segmento_right', function (e) {
        v_repreal = $("#cbo_segmento_right").data("repreal");
        switch (v_repreal) {
            case 58:
                funct_presencia_panel();
                break;
            case 19:
                funct_precio_panel();
                break;
            case 21:
                funct_sod_panel();
                break;
            case 66:
                funct_actcompe_panel();
                break;
        }
    });
    $(document).on('change', '#cbo_tipo_right', function (e) {
        funct_actcompe_panel();
    });

    //Filtros Datos x PDV
    $(document).on('change', '#cbo_pdv_foto_cat', function (e) {
       
        $("#dv_content_Fotografico").empty();
        var sw = 0;
        $.each(_g_vfotos, function (key, value) {            
            if ($("#cbo_pdv_foto_cat").val() == value._a) {
                $("#dv_content_Fotografico").append("<div class='cls_item_fotografico'><img src='" + _g_vurlimg + value._c + "' alt='...' class='img-thumbnail' /> <b>" + value._d + "</b> </div> ");
                sw += 1;
            }
        });
        if (sw == 0) {
            $("#dv_content_Fotografico").html("<div class='md-col-12'><center><h5>Sin datos disponibles...</h5></div>");
        }
    });
    $(document).on('change', '#cbo_pdv_pres_cat', function (e) {
        var vreporte = $("#cbo_pdv_pres_cat").data('id');
        funct_marca('cbo_pdv_pres_mar', vreporte, $("#cbo_pdv_pres_cat").val());
    });
    $(document).on('change', '#cbo_pdv_pres_mar', function (e) {
        funct_pres_x_pdv();
    });
    $(document).on('change', '#cbo_pdv_prec_cat', function (e) {
        var vreporte = $("#cbo_pdv_prec_cat").data('id');
        funct_marca('cbo_pdv_prec_mar', vreporte, $("#cbo_pdv_prec_cat").val());
    });
    $(document).on('change', '#cbo_pdv_prec_mar', function (e) {
        funct_prec_x_pdv();
    });
    $(document).on('change', '#cbo_pdv_sod_cat', function (e) {
        funct_sod_x_pdv();
    })
    $(document).on('change', '#cbo_pdv_act_cat', function (e) {
        funct_actcompe_x_pdv();
    })
    $(document).on('change', '#cbo_pdv_act_tipo', function (e) {
        funct_actcompe_x_pdv();
    })


    $(document).on('click', '#btn_pdv_mapa', function (e) {        
        $("body").removeClass("infobar-active");
        if (funct_validar_select(1) == 0) {
            $("#btn_close_panel_reporte").trigger("click");
            funct_pdv_unico($("#txt_PDV").val());
        }      
    });
});

function funct_tipo(_vid) {
    $("#" + _vid).parent().parent().show();
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "8"
            , __b: ""
            , __c: _vid
        },
        success: function (response) {
            $("#"+_vid).replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_categoria(_vid, _vreporte,_vmarca,vrepreal) {
    $("#" + _vid).parent().parent().show();
    $.ajax({
        beforeSend: function (xhr) { },
        async: true,
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "5"
            , __b: _g_veq+","+_vreporte
            , __c: _vid
        },
        success: function (response) {
            $("#" + _vid).replaceWith(response);
            $("#" + _vid).attr('data-id', _vreporte);
            $("#" + _vid).attr('data-marca', _vmarca);
            $("#" + _vid).attr('data-repreal', vrepreal);
            if (_vmarca != null && _vmarca != "" ) {
                funct_marca(_vmarca, _vreporte, $("#" + _vid).val(), vrepreal);
            }
            if (_vreporte == 23) {
                $("#cbo_pdv_foto_cat").trigger("change");
            }
            if (vrepreal == 21) {
                funct_segmento('cbo_segmento_right', 21);
            }
            if (_vid == "cbo_pdv_sod_cat") {
                funct_sod_x_pdv();
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_marca(_vid, _vreporte, _vcategoria, vrepreal) {
    
    $("#" + _vid).parent().parent().show();
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "6"
            , __b: _g_veq + "," + _vreporte+","+_vcategoria
            , __c: _vid
        },
        success: function (response) {
            $("#" + _vid).replaceWith(response);
            switch (_vid) {
                case  'cbo_pdv_pres_mar':
                    funct_pres_x_pdv();
                    break;
                case 'cbo_pdv_prec_mar':
                    funct_prec_x_pdv();
                    break;
                case 'cbo_marca_right':
                    if (vrepreal == 58) {
                        $("#" + _vid).attr('data-repreal', vrepreal);
                        funct_presencia_panel();
                    }
                    if (vrepreal == 19) {
                        $("#" + _vid).attr('data-repreal', vrepreal);
                        funct_precio_panel();
                    }

                    break;
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_segmento(_vid, vrepreal) {
    $("#" + _vid).parent().parent().show();
    $.ajax({
        beforeSend: function (xhr) { },
        async: true,
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "7"
            , __b: $("#cbo_tipogiro").val() + "," + _vmpas_g_ubigeo.grupo + "," + _vmpas_g_ubigeo.codigo
            , __c: _vid
        },
        success: function (response) {
            $("#"+_vid).replaceWith(response);
            $("#" + _vid).attr('data-repreal', vrepreal);
            if (vrepreal == 21) {
                funct_sod_panel();
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}


function funct_periodo() {

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "3"
            , __b: _g_veq+",19,"+$("#cbo_anio").val()+","+$("#cbo_mes").val()
            , __c: "cbo_periodo"
        },
        success: function (response) {
            $('#cbo_periodo').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });

}

function funct_validar_select(_vop) {
    debugger;
    var _vbody_alert = "";
    var sw = 0;
    if (_vmpas_g_ubigeo == null)
    {
        _vbody_alert += "<li>Seleccionar capa para realizar la consulta...</li>";
        sw = 1;
    }
    if ($("#cbo_anio").val() == 0 || $("#cbo_anio").val() == null)
    {
        _vbody_alert += "<li>Seleccionar el filtro año...</li>";
        sw = 1;
    }
    if ($("#cbo_mes").val() == 0 || $("#cbo_mes").val() == null) {
        _vbody_alert += "<li>Seleccionar el filtro mes...</li>";
        sw = 1;
    }
    if ( $("#cbo_periodo").val() == null) {
        _vbody_alert += "<li>Seleccionar el filtro periodo...</li>";
        sw = 1;
    }
    if (_vop ==1 ) {
        if ($("#txt_PDV").val() == null || $("#txt_PDV").val() == "0" || $("#txt_PDV").val() == "")
        {
            _vbody_alert += "<li>Ingresar codigo de PDV...</li>";
            sw = 1;
        }
    }

    if (sw == 1) {
        $("#MD_alert_body").html(_vbody_alert);
        $("#MD_alert").modal();
    }

    return sw;
}

function funct_centrar_mapa() {
    var pan = ol.animation.pan({
        duration: 1000,
        source: /** @type {ol.Coordinate} */ (map.getView().getCenter())
    });
    map.beforeRender(pan);
    map.getView().setCenter(ol.proj.transform([-59.5320485, -8.6153994], 'EPSG:4326', 'EPSG:3857'));
    map.getView().setZoom(5);
}
