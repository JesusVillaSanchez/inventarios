﻿_vmarcadores=[];
function funct_reporte(velemento) {
    funct_remove_pdv();
    $("#dv_rep_panel_filtros > div").hide();
    $("#sp_nom_reporte").html(velemento);
    if (velemento == "Cobertura")
    {
        $(".cls_filtro_rep").hide();
        funct_cobertura_panel();
    }
    if (velemento == "Share Of Display")
    {
        
        funct_categoria('cbo_categoria_right', 19, '', 21);        
        $("#dv_btn_filtros_rep").show();
        //funct_sod_panel();
        $('#dv_rep_panel_body').html('<center><h4>Seleccinar filtros</h4></center>');
    }
    if (velemento == "Actividad Competencia")
    {
        $("#dv_btn_filtros_rep").show();
        funct_tipo('cbo_tipo_right');
        funct_categoria('cbo_categoria_right', 19, '',66);
        funct_segmento('cbo_segmento_right',66);
        funct_actcompe_panel();
    }
    if (velemento == "Presencia de Producto (OSA)")
    {
        funct_segmento('cbo_segmento_right', 58);
        $("#dv_btn_filtros_rep").show(); 
        funct_categoria('cbo_categoria_right', 19, 'cbo_marca_right', 58);        
        //funct_presencia_panel();
    }
    if (velemento == "Tracking de Precio")
    {
        funct_segmento('cbo_segmento_right', 19);
        $("#dv_btn_filtros_rep").show();
        funct_categoria('cbo_categoria_right', 19, 'cbo_marca_right',19);        
        //funct_precio_panel();
    }

}

/* Inicio reportes panel */

function funct_cobertura_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'Cobertura',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: ""
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_presencia_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'Presencia',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: ""
            , __i: $("#cbo_categoria_right").val()
            , __j: $("#cbo_marca_right").val()
            , __k: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __l: ""
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_precio_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'Precio',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()

        , __h: ""
        , __i: $("#cbo_categoria_right").val()
        , __j: $("#cbo_marca_right").val()
        , __k: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
        , __l: ""
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_sod_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        async: true,
        url: 'ShareOfDisplay',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: ""
            , __i: $("#cbo_categoria_right").val()
            , __j: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            $('#dv_rep_panel_body').html('<div id="dv_rep_panel_body" ></div>');
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_actcompe_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'ActividadCompetencia',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: ""
            , __i: ($("#cbo_categoria_right").val() == null || $("#cbo_categoria_right").val() == "" ? "0" : $("#cbo_categoria_right").val())
            , __j: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __k: ($("#cbo_tipo_right").val() == null || $("#cbo_tipo_right").val() == "" ? 0 : $("#cbo_tipo_right").val())
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin reportes panel */

function funct_reportes_pdv() {    
    funct_filtros_x_PDV();
    // Datos PDV
    funct_datos_pdv();
    //funct_sod_x_pdv();
    $('#dv_content_SOD').html("<center><h4>Seleccionar filtros</h4></center>");
    funct_actcompe_x_pdv();
    funct_foto_x_pdv();
}

/* Inicio reportes por PDV */
function funct_datos_pdv() {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Pdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __c: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __d: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __e: _g_vpdv
        },
        success: function (response) {
            $("#dp_foto1").attr('src', _g_vurlimg + response._j[0]._c);
            $("#dp_foto2").attr('src', _g_vurlimg + response._j[1]._c);
            $("#pd_nom_pdv").html(response._a);
            $("#pd_direccion").html(response._b);
            $("#pd_distrito").html(response._c);
            $("#pd_ult_visita").html(response._d);
            $("#pd_nom_gie").html(response._e);
            // $("#pd_nom_comerciante").html();
            $("#pd_email").html(response._f);
            $("#pd_nom_segmento").html(response._g);
            $("#pd_nom_ciudad").html(response._h);
            $("#pd_nom_giro").html(response._i);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_sod_x_pdv() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_SOD').html(""); },
        url: 'ShareOfDisplay_PDV',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
             , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: _g_vpdv
            , __i: $("#cbo_pdv_sod_cat").val()
            , __j: ""
        },
        success: function (response) {
            $('#dv_content_SOD').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_actcompe_x_pdv() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_ActividadCompe').html(""); },
        url: 'ActividadCompetenciaPDV',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: _g_vpdv
            , __i: ( $("#cbo_pdv_act_cat").val() == null || $("#cbo_pdv_act_cat").val() == "" ? "0" : $("#cbo_pdv_act_cat").val() )
            , __j: "0"
            , __k: ($("#cbo_pdv_act_tipo").val() == null || $("#cbo_pdv_act_tipo").val() == "" ? "0" : $("#cbo_pdv_act_tipo").val())
        },
        success: function (response) {
            $('#dv_content_ActividadCompe').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pres_x_pdv() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_presenciasku').html(""); },
        url: 'PresenciaSku',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
             , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: _g_vpdv
            , __i: $("#cbo_pdv_pres_cat").val()
            , __j: $("#cbo_pdv_pres_mar").val()
            , __k: "0"
            , __l: ""
        },
        success: function (response) {
            $('#dv_content_presenciasku').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_prec_x_pdv() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_preciosku').html(""); },
        url: 'PrecioSku',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
             , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: _g_vpdv
            , __i: $("#cbo_pdv_prec_cat").val()
            , __j: $("#cbo_pdv_prec_mar").val()
            , __k: ""
            , __l: ""
        },
        success: function (response) {
            $('#dv_content_preciosku').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_foto_x_pdv() {
    _g_vfotos = null;
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Fotografico',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __c: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __d: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __e: _g_vpdv
            , __f: 0
        },
        success: function (response) {
            //$("#dv_content_Fotografico").html("");
            _g_vfotos = response;
            //var sw = 0;
            //$.each(response, function (key, value) {
            //    $("#dv_content_Fotografico").append("<div class='cls_item_fotografico'><img src='" + _g_vurlimg + value._c + "' alt='...' class='img-thumbnail' /> <b>" + value._d + "</b> </div> ");
            //    sw += 1;
            //});
            //if (sw == 0) {
            //    $("#dv_content_Fotografico").html("<div class='md-col-12'><center><h5>Sin datos disponibles...</h5></div>");
            //}
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin Reportes por PDV */

function funct_filtros_x_PDV() {
    // Presencia
    funct_categoria('cbo_pdv_pres_cat', 19, 'cbo_pdv_pres_mar');
    // Precio
    funct_categoria('cbo_pdv_prec_cat', 19, 'cbo_pdv_prec_mar');
    // Share of Display
    funct_categoria('cbo_pdv_sod_cat', 19, '');
    // Actividad Competencia
    funct_categoria('cbo_pdv_act_cat', 19, '');
    funct_tipo('cbo_pdv_act_tipo');
    //Fotografico
    funct_categoria('cbo_pdv_foto_cat', 23, ''); 
}

/* Inicio mostrar PDV en mapa */
function funct_pdv_cobertura(_vgrupo, _vsegmento) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); funct_remove_pdv(); },
        url: 'CoberturaPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: "" //_vpdv
            , __i: _vgrupo
            , __j: _vsegmento
        },
        success: function (response) {
            if (response == null) {
                funct_loading(2);
            } else {
                funct_pintar_pdv(response);
            }            
        },
        error: function (xhr) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pdv_presencia(_vsku) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); funct_remove_pdv(); },
        url: 'PresenciaPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            ,__h : ""
            , __i: $("#cbo_categoria_right").val()
            , __j: $("#cbo_marca_right").val()
            , __k: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __l: _vsku
        },
        success: function (response) {
            if (response == null) {
                funct_loading(2);
            } else {
                funct_pintar_pdv(response);
            }
        },
        error: function (xhr) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pdv_precio(_vsku) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); funct_remove_pdv(); },
        url: 'PrecioPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            ,__h : ""
            , __i: $("#cbo_categoria_right").val()
            , __j: $("#cbo_marca_right").val()
            , __k: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __l: _vsku
        },
        success: function (response) {
            if (response == null) {
                funct_loading(2);
            } else {
                funct_pintar_pdv(response);
            }
        },
        error: function (xhr) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pdv_unico(_vpdv) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); funct_remove_pdv(); },
        url: 'CoberturaPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_tipogiro").val()
            , __h: _vpdv
            , __i: "0"
            , __j: "0"
        },
        success: function (response) {
            if (response == null || response.length <= 0) {
                funct_loading(2);
                alert("El codigo ingresado no contiene visita en el periodo seleccionado...");
            } else {
                funct_pintar_pdv(response);
            }
        },
        error: function (xhr) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}


function funct_pintar_pdv(v_data) {
    
    var vectorSources = new ol.source.Vector();
    funct_remove_pdv();
    _vmarcadores = [];
    $.each(v_data, function (key, value) {
        _vmarcadores.push(new ol.Overlay({
            position: ol.proj.transform([parseFloat(value.d), parseFloat(value.c)], 'EPSG:4326', 'EPSG:3857')
            , element: $('<img src="/Content/Maps/img/marcador/marcador3.png" title="' + value.b + ' ">')
              //, element: $('<i class="fa fa-circle" title="' + value.b + ' "></i>')
              .css({ cursor: 'pointer' }).on('click', function (e) {
                  //alert(value.a);
                  $('#myModal').modal({
                      keyboard: false
                        , backdrop: 'static'
                  }).delay(200).queue(function () {
                      _g_vpdv = value.a;
                      funct_reportes_pdv();
                      $(this).dequeue();
                  });
              })
        }));
        map.addOverlay(_vmarcadores[key]);
        //console.log(key);
        if (key == (v_data.length-1)) {
            funct_loading(2);
            funct_centrar_mapa();
        }
    });

}
function funct_remove_pdv() {

    $.each(_vmarcadores, function (key, value) {        
        map.removeOverlay(value);
        _vmarcadores[key] = null;
        
    });

}
/* fin mostrar PDV en mapa */
function funct_loading(_vop) {
    if (_vop==1) {
        $('#MD_loading').modal({
            keyboard: false
            , backdrop: 'static'
        });
    }else{
        $('#MD_loading').modal('hide')
    }

}