﻿var ArrayFotomontaje = [];

/* Inicio boton nueva solcitud */
$(document).on('click', '#id-btn-nuevo', function (e) {
    fn_md_nueva_solicitud();
});
/* Fin boton nueva solicitud */

/* Inicio Modal nueva solicitud */

$(document).on('click', '#btn-md-grabar', function (e) {
    //fn_reg_solicitud(1); no descomentar

    var vpdv = $('#id-md-nom-pdv').val()
            , vruc = $('#id-md-ruc').val()
            , vcodigo = $('#id-md-codigo').val()
            , vdireccion = $('#id-md-direccion').val()
            , vdistrito = $('#id-md-distrito').val()
            , vprovincia = $('#id-md-provincia').val()
            , vdepartamento = $('#id-md-departamento').val()
            , vcontacto = $('#id-md-contacto').val()
            , vnumtelef = $('#id-md-telefono').val()
            , vnumcel = $('#id-md-celular').val()
            , vdistribuidora = $('#id-md-distribuidora').val()
            , p1_codZona = $('#id-md-zona').val()
            , vmarca = $('#id-md-marca').val()
            //, url_archivo1 = $('#url-archivo-1').val()
            , v_rutafoto1 = $('#ruta-foto-1').val()
            , v_tipo_gral_solicitud = $('#auxbuton').val();

    var _vtiposolic;
    $('input[name="gchktiposolic"]:checked').each(function () {
        _vtiposolic += $(this).data('cod-tiposolic') + ',';
    });
    if (_vtiposolic == "" || _vtiposolic == null) {
        _vtiposolic = "0"
    } else {
        _vtiposolic = _vtiposolic.substring(0, ((_vtiposolic.length) - 1));
        _vtiposolic = _vtiposolic.substring(9, _vtiposolic.length);
    }

    if (String(replaceAll(vpdv, ' ', '')).length > 0 &&
        String(replaceAll(vruc, ' ', '')).length > 0 &&
        String(replaceAll(vcodigo, ' ', '')).length > 0 &&
        ($('#DivGaleriaSolicitud').find('div').length > 1)
        //String(replaceAll(vdireccion, ' ', '')).length > 0 &&
        //String(replaceAll(vdistrito, ' ', '')).length > 0 &&
        //String(replaceAll(vprovincia, ' ', '')).length > 0 &&
        //String(replaceAll(vdepartamento, ' ', '')).length > 0 &&
        //String(replaceAll(vcontacto, ' ', '')).length > 0 &&
        ////String(replaceAll(vnumtelef, ' ', '')).length > 0 &&
        //String(replaceAll(vnumcel, ' ', '')).length > 0 &&
        //String(replaceAll(vdistribuidora, ' ', '')).length > 0 &&
        //_vtiposolic != "0" &&
        //p1_codZona != "0" &&
        //vmarca != "0"
        //(String(replaceAll(url_archivo1, ' ', '')).length > 0 || String(replaceAll(v_rutafoto1, ' ', '')).length > 0)
        ) {

        $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
        //$('.objrequeridsolicitud #url-archivo-1').removeClass('has-error2');
        //$('#id-md-solicitud .cls-loading').html($('.cls-rep-loading1').html());
        $('.lucky-loading').modal('show');

        //fn_carga_archivo('id-md-carga', 1, fn_reg_solicitud);

        var ArrayFoto = [];
        $('div[data-solicitud-archivo]').each(function () {
            var SolicitudArchivo = $(this).data('solicitud-archivo');
            ArrayFoto.push(SolicitudArchivo);
        });
        fn_reg_solicitud(1, ArrayFoto.join(','));
    } else {
        //$('#id-md-nom-pdv').val('').focus();
        //$('.objrequeridsolicitud').addClass('has-error');   //html('Para desaprobar la solicitud ingresar un comentario.');


        if (String(replaceAll(vpdv, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-nom-pdv').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
        }
        if (String(replaceAll(vruc, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-ruc').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
        }
        if (String(replaceAll(vcodigo, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-codigo').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
        }

        if ($('#DivGaleriaSolicitud').find('div').length == 1) {
            alert("Debe ingresar por lo menos una imagen");
        }
        //if (String(replaceAll(url_archivo1, ' ', '')).length == 0 && String(replaceAll(v_rutafoto1, ' ', '')).length == 0) {
        //    $('.objrequeridsolicitud #url-archivo-1').addClass("has-error2");
        //} else {
        //    $('.objrequeridsolicitud #url-archivo-1').removeClass('has-error2');
        //}
    }
});
$(document).on('click', '#btn-md-enviar', function () {
    var vpdv = $('#id-md-nom-pdv').val()
           , vruc = $('#id-md-ruc').val()
           , vcodigo = $('#id-md-codigo').val()
           , vdireccion = $('#id-md-direccion').val()
           , vdistrito = $('#id-md-distrito').val()
           , vprovincia = $('#id-md-provincia').val()
           , vdepartamento = $('#id-md-departamento').val()
           , vcontacto = $('#id-md-contacto').val()
           , vnumtelef = $('#id-md-telefono').val()
           , vnumcel = $('#id-md-celular').val()
           , vdistribuidora = $('#id-md-distribuidora').val()
           , p1_codZona = $('#id-md-zona').val()
           , vmarca = $('#id-md-marca').val()
           //, url_archivo1 = $('#url-archivo-1').val()
           , v_rutafoto1 = $('#ruta-foto-1').val();

    var _vtiposolic;
    $('input[name="gchktiposolic"]:checked').each(function () {
        _vtiposolic += $(this).data('cod-tiposolic') + ',';
    });
    if (_vtiposolic == "" || _vtiposolic == null) {
        _vtiposolic = "0"
    } else {
        _vtiposolic = _vtiposolic.substring(0, ((_vtiposolic.length) - 1));
        _vtiposolic = _vtiposolic.substring(9, _vtiposolic.length);
    }

    if (String(replaceAll(vpdv, ' ', '')).length > 0 &&
        String(replaceAll(vruc, ' ', '')).length > 0 &&
        String(replaceAll(vcodigo, ' ', '')).length > 0 &&
        String(replaceAll(vdireccion, ' ', '')).length > 0 &&
        String(replaceAll(vdistrito, ' ', '')).length > 0 &&
        String(replaceAll(vprovincia, ' ', '')).length > 0 &&
        String(replaceAll(vdepartamento, ' ', '')).length > 0 &&
        String(replaceAll(vcontacto, ' ', '')).length > 0 &&
        //String(replaceAll(vnumtelef, ' ', '')).length > 0 &&
        String(replaceAll(vnumcel, ' ', '')).length > 0 &&
        String(replaceAll(vdistribuidora, ' ', '')).length > 0 &&
        _vtiposolic != "0" &&
        p1_codZona != "0" &&
        vmarca != "0" &&
        ($('#DivGaleriaSolicitud').find('div').length > 1)
        ) {
        //$('.objrequeridsolicitud').removeClass('has-error');

        $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-marca').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-celular').removeClass('has-error2');
        $('.objrequeridsolicitud #div_tipos').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-departamento').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-contacto').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-provincia').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-distrito').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-direccion').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-distribuidora').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-zona').removeClass('has-error2');
        //$('.objrequeridsolicitud #url-archivo-1').removeClass('has-error2');

        //fn_carga_archivo('id-md-carga', 2, fn_reg_solicitud);
        var ArrayFoto = [];
        $('div[data-solicitud-archivo]').each(function () {
            var SolicitudArchivo = $(this).data('solicitud-archivo');
            ArrayFoto.push(SolicitudArchivo);
        });
        fn_reg_solicitud(2, ArrayFoto.join(','));

        $('#id-md-solicitud').modal('hide');

    } else {

        //$('#id-md-nom-pdv').val('').focus();
        //$('.objrequeridsolicitud').addClass('has-error');   //html('Para desaprobar la solicitud ingresar un comentario.');


        if (String(replaceAll(vpdv, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-nom-pdv').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
        }
        if (String(replaceAll(vruc, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-ruc').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
        }
        if (vmarca == "0") {
            $('.objrequeridsolicitud #id-md-marca').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-marca').removeClass('has-error2');
        }
        if (String(replaceAll(vcodigo, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-codigo').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
        }
        if (String(replaceAll(vnumcel, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-celular').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-celular').removeClass('has-error2');
        }
        if (_vtiposolic == "0") {
            $('.objrequeridsolicitud #div_tipos').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #div_tipos').removeClass('has-error2');
        }
        if (String(replaceAll(vdepartamento, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-departamento').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-departamento').removeClass('has-error2');
        }
        if (String(replaceAll(vcontacto, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-contacto').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-contacto').removeClass('has-error2');
        }
        if (String(replaceAll(vprovincia, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-provincia').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-provincia').removeClass('has-error2');
        }
        if (String(replaceAll(vdistrito, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-distrito').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-distrito').removeClass('has-error2');
        }
        if (String(replaceAll(vdireccion, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-direccion').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-direccion').removeClass('has-error2');
        }
        if (String(replaceAll(vdistribuidora, ' ', '')).length == 0) {
            $('.objrequeridsolicitud #id-md-distribuidora').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-distribuidora').removeClass('has-error2');
        }
        if (p1_codZona == "0") {
            $('.objrequeridsolicitud #id-md-zona').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-zona').removeClass('has-error2');
        }

        if ($('#DivGaleriaSolicitud').find('div').length == 1) {
            alert("Debe ingresar por lo menos una imagen");
        }
        //if (String(replaceAll(v_rutafoto1, ' ', '')).length == 0) {
        //    $('.objrequeridsolicitud #url-archivo-1').addClass("has-error2");
        //} else {
        //    $('.objrequeridsolicitud #url-archivo-1').removeClass('has-error2');
        //}

    }
});
$(document).on('click', '#btn-md-editar', function (e) {
    $("#id-md-solicitud input").prop('disabled', false);
    $("#id-md-solicitud select").prop('disabled', false);
    //$("#id-md-solicitud #url-archivo-1").prop('disabled', true);
    $("#id-md-solicitud .modal-footer .pull-left #btn-md-grabar").prop('disabled', false);

    $("#id-md-solicitud .modal-footer .pull-left #btn-md-enviar").prop('disabled', false);

    //$("#btn-md-grabar").css("background-color", "#7FB842");
    $("#btn-md-grabar").css("background-color", "#5AAF00");

    //$("#btn-md-cancelar").css("background-color", "#815685");
    $("#btn-md-cancelar").css("background-color", "#7B0F84");


    //$("#btn-md-enviar").css("background-color", "#009DC8");
    $("#btn-md-enviar").css("background-color", "#009DC8");

    $('div[data-solicitud-archivo] a').each(function () {
        $(this).attr('disabled', false);
    });
});

function fn_md_nueva_solicitud() {
    $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-marca').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-celular').removeClass('has-error2');
    $('.objrequeridsolicitud #div_tipos').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-departamento').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-contacto').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-provincia').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-distrito').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-direccion').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-distribuidora').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-zona').removeClass('has-error2');
    //$('.objrequeridsolicitud #url-archivo-1').removeClass('has-error2');
    //$('#url-archivo-1').val('');
    $('div[data-solicitud-archivo]').each(function () {
        $(this).remove();
    });

    $('#id-hd-codigo').val('0');
    $('#id-md-solicitud').modal({ backdrop: 'static' });
    $("#id-md-solicitud .modal-footer .pull-left button").prop('disabled', false);
    $("#id-md-solicitud input").prop('disabled', false);
    //$("#id-md-solicitud #url-archivo-1").prop('disabled', true);
    $('#btn-md-editar').hide();
    $("#id-md-solicitud input").val('');
    $('.mask').inputmask();
    //for (var i = 1; i < 6; i++) {
    //    //$('#id-img-solicitud-' + i).attr('src', Url.Xplora + '/Fachada/Img/foto_nodisponible.jpg');
    //    $('#id-cls-img-' + i).attr('src', Url.Xplora + '/Fachada/Img/foto_nodisponible.jpg');

    //}
    $('#id-img-solicitud-1').attr('src', Url.Xplora + '/Fachada/Img/foto_nodisponible.jpg');

    $('#id-md-solicitud').on('shown.bs.modal', function (e) {
        $('#id-md-nom-pdv').focus();
        //$("#id-btn-implementacion").css("border-color", "#FF0900");
        //$("#id-btn-mantenimiento").css("border-color", "#000000");
        //$("#auxbuton").val("1");
        if ($("#auxbuton").val() == "1" || $("#auxbuton").val() == "") {
            $("#id-btn-implementacion").css("border-color", "#FF0900");
            $("#id-btn-mantenimiento").css("border-color", "#000000");
            $("#id-btn-tramite").css("border-color", "#000000");
            $("#auxbuton").val("1");
        } else if ($("#auxbuton").val() == "2") {
            $("#id-btn-implementacion").css("border-color", "#000000");
            $("#id-btn-mantenimiento").css("border-color", "#FF0900");
            $("#id-btn-tramite").css("border-color", "#000000");
        } else if ($("#auxbuton").val() == "3") {
            $("#id-btn-implementacion").css("border-color", "#000000");
            $("#id-btn-mantenimiento").css("border-color", "#000000");
            $("#id-btn-tramite").css("border-color", "#FF0900");
        }
    });

    $("#id-md-solicitud select").prop('disabled', false); //YR 1006
    $("#_solicitud").prop('disabled', false); //YR 1006
    $('input:checkbox').removeAttr('checked'); //YR 2006
    $('#id-md-comentario').val('');
    document.getElementById('id-md-zona').selectedIndex = 0;
    document.getElementById('id-md-marca').selectedIndex = 0;
    $('#btn-md-desactivar').css("display", "none");
    $('#btn-md-activar').css("display", "none");
}
function fn_reg_solicitud(vop, foto) {
    var _vtiposolic;
    $('input[name="gchktiposolic"]:checked').each(function () {
        _vtiposolic += $(this).data('cod-tiposolic') + ',';
    });
    if (_vtiposolic == "" || _vtiposolic == null) {
        _vtiposolic = "0"
    } else {
        _vtiposolic = _vtiposolic.substring(0, ((_vtiposolic.length) - 1));
        _vtiposolic = _vtiposolic.substring(9, _vtiposolic.length);
    }
    //alert(_vtiposolic);

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'AddSolicitud',
        type: 'POST',
        dataType: 'json',
        data: {
            vids: ($('#id-hd-codigo').val() == null || $('#id-hd-codigo').val() == "" ? 0 : $('#id-hd-codigo').val())
            , vpdv: $('#id-md-nom-pdv').val()
            , vruc: $('#id-md-ruc').val()
            //, vcodigo: '' 
            , vcodigo: $('#id-md-codigo').val()
            , vdireccion: $('#id-md-direccion').val()
            , vdistrito: $('#id-md-distrito').val()
            , vprovincia: $('#id-md-provincia').val()
            , vdepartamento: $('#id-md-departamento').val()
            , vcontacto: $('#id-md-contacto').val()
            , vnumtelef: $('#id-md-telefono').val()
            //, vnumtelef: '' //$('#id-md-telefono').val()
            , vnumcel: $('#id-md-celular').val()
            , vdistribuidora: $('#id-md-distribuidora').val()
            , vfoto: foto
            , vven_prom_soles: $('#id-md-venta-soles').val()
            , vven_prom_ton: $('#id-md-venta-toneladas').val()
            , vlinea_credito: $('#id-md-credito').val()
            , vdeu_actual: $('#id-md-deuda').val()
            , vop_grabado: vop
            , vop_opera: ($('#id-hd-codigo').val() == '0' || $('#id-hd-codigo').val() == null || $('#id-hd-codigo').val() == "" ? '1' : '2')

            , p1_codperson: $('#id-hd-person').val()
            , p1_codTipo: _vtiposolic
            , p1_codZona: $('#id-md-zona').val()
            , p1_comentarios: $('#id-md-comentario').val()
            , vmarca: $('#id-md-marca').val()
            , v_tipo_gral_solicitud: $('#auxbuton').val()
        },
        success: function (a) {
            //alert(a);                
            fn_confirmar_registro();
            fn_consutar_grilla();

            //$('#id-md-solicitud .cls-loading').html('');
            $('.lucky-loading').modal('hide');
        },
        complete: function (a) {
            fn_md_nueva_solicitud();
            //$('#id-md-solicitud').modal({ backdrop: 'static' });
        },
        error: function (xhr) {
            $('#id-md-solicitud .cls-loading').html('');
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });

}
function fn_confirmar_registro() {
    //$.pnotify({
    //    title: 'Confirmación',
    //    text: 'Solicitud registrada con exito!!!',
    //    type: 'success'
    //});
}
function fn_md_consultar_solicitud() {
    $("#btn-md-grabar").css("background-color", "#8DB95D");
    $("#btn-md-cancelar").css("background-color", "#806882");
    $("#btn-md-enviar").css("background-color", "#6BB3C6");
    //$("#url-archivo-1").val('');


    $('#id-md-solicitud').modal({ backdrop: 'static' });
    $("#id-md-solicitud .modal-footer .pull-left button").prop('disabled', true);
    $("#id-md-solicitud input").prop('disabled', true);
    $("#id-md-solicitud select").prop('disabled', true); //YR 1006
    $("#_solicitud").prop('disabled', true); //YR 1006
}

// Modal Fotomontaje
$(document).on('click', '.cls-item-fotomontaje', function () {
    ArrayFotomontaje = [];

    //$('.cls-img-1').css("display", "none");
    //$('.cls-img-2').css("display", "none");
    //$('.cls-img-3').css("display", "none");
    //$('.cls-img-4').css("display", "none");

    $('#foto1').css("visibility", "hidden");
    $('#foto2').css("visibility", "hidden");
    $('#foto3').css("visibility", "hidden");
    $('#foto4').css("visibility", "hidden");

    $('#id-md-fotomontaje input[type="file"]').val('');
    //$('.cls-eliminar-file').remove();
    $('#id-download-word').hide();
    //$('#id-download-word2').hide();
    $('#id-md-fotomontaje').data('codigo', '');
    $('#id-md-fotomontaje').data('npdv', '');
    $("#id-cls-area-comment").val('');
    var __id = $(this).data('id');
    var __npdv = $(this).data('npdv');
    var __doc = '', __comment = '';
    $('#id-md-fotomontaje').data('codigo', __id);
    $('#id-md-fotomontaje').data('npdv', __npdv);

    $('#id-md-fotomontaje').modal({ backdrop: 'static' });
    $.ajax({
        url: 'Consultar_Fotomontaje',
        type: "POST",
        dataType: "json",
        async: true,
        data: { __a: __id },
        success: function (response) {
            $(".cls-img-1").attr('src', Url.Xplora + '/Fachada/Img/foto_nodisponible.jpg');
            $(".cls-img-2").attr('src', Url.Xplora + '/Fachada/Img/foto_nodisponible.jpg');
            $(".cls-img-3").attr('src', Url.Xplora + '/Fachada/Img/foto_nodisponible.jpg');
            $(".cls-img-4").attr('src', Url.Xplora + '/Fachada/Img/foto_nodisponible.jpg');
            $('#id-hd-img1').val('');
            $('#id-hd-img2').val('');
            $('#id-hd-img3').val('');
            $('#id-hd-img4').val('');
            var _vbodyhistoric = "";
            $.each(response, function (key, value) {
                if (value._g == "1") {
                    __doc = value._d;
                    __comment = value._f;
                    switch (value._b) {
                        case 1:
                            //$(".cls-img-1").attr('src', Url.Xplora + value._c);
                            $('#id-hd-img1').val(value._c);
                            if (value._c != '') {
                                $(".cls-img-1").attr('src', Url.Xplora + value._c);
                                if (_vg_grupo == 3) {
                                    $('#foto1').css("visibility", "visible");
                                }
                                $('.cargar-foto1').css("display", "none");
                                //$('.cls-img-1').css("display", "block");

                                ArrayFotomontaje.push({ Orden: 1, Url: Url.Xplora + value._c });
                            } else if (value._c == '' || value._c == null) {
                                //$(".cls-img-1").attr('src', Url.Xplora + value._c);
                                if (_vg_grupo == 3) {
                                    $('.cargar-foto1').css("display", "block");
                                } else {
                                    $('.cargar-foto1').css("display", "none");
                                }
                                //$('.cls-img-1').css("display", "none");
                            }
                            break;
                        case 2:
                            //$(".cls-img-2").attr('src', Url.Xplora + value._c);
                            $('#id-hd-img2').val(value._c);
                            if (value._c != '') {
                                $(".cls-img-2").attr('src', Url.Xplora + value._c);
                                if (_vg_grupo == 3) {
                                    $('#foto2').css("visibility", "visible");

                                }
                                $('.cargar-foto2').css("display", "none");
                                //$('.cls-img-2').css("display", "block");

                                ArrayFotomontaje.push({ Orden: 2, Url: Url.Xplora + value._c });
                            } else if (value._c == '' || value._c == null) {
                                if (_vg_grupo == 3) {
                                    $('.cargar-foto2').css("display", "block");
                                } else {
                                    $('.cargar-foto2').css("display", "none");
                                }
                                //$('.cls-img-2').css("display", "none");
                            }
                            break;
                        case 3:
                            //$(".cls-img-3").attr('src', Url.Xplora + value._c);
                            $('#id-hd-img3').val(value._c);
                            if (value._c != '') {
                                $(".cls-img-3").attr('src', Url.Xplora + value._c);
                                if (_vg_grupo == 3) {
                                    $('#foto3').css("visibility", "visible");
                                }
                                $('.cargar-foto3').css("display", "none");
                                //$('.cls-img-3').css("display", "block");
                                ArrayFotomontaje.push({ Orden: 3, Url: Url.Xplora + value._c });
                            } else if (value._c == '' || value._c == null) {
                                if (_vg_grupo == 3) {
                                    $('.cargar-foto3').css("display", "block");
                                } else {
                                    $('.cargar-foto3').css("display", "none");
                                }
                                //$('.cls-img-3').css("display", "none");
                            }
                            break;
                        case 4:
                            //$(".cls-img-4").attr('src', Url.Xplora + value._c);
                            $('#id-hd-img4').val(value._c);
                            if (value._c != '') {
                                $(".cls-img-4").attr('src', Url.Xplora + value._c);
                                if (_vg_grupo == 3) {
                                    $('#foto4').css("visibility", "visible");
                                }
                                $('.cargar-foto4').css("display", "none");
                                //$('.cls-img-4').css("display", "block");
                                ArrayFotomontaje.push({ Orden: 4, Url: Url.Xplora + value._c });
                            } else if (value._c == '' || value._c == null) {
                                if (_vg_grupo == 3) {
                                    $('.cargar-foto4').css("display", "block");
                                } else {
                                    $('.cargar-foto4').css("display", "none");
                                }
                                //$('.cls-img-4').css("display", "none");
                            }
                            break;
                    }

                }
                else {

                    switch (value._b) {
                        case 1:
                            _vbodyhistoric += '<td><img style="width: 100px; height: 40px;" src="' + value._c + '" /></td>';
                            //$(".cls-img-1").attr('src', value._c);
                            //$('#id-hd-img1').val(value._c);
                            break;
                        case 2:
                            _vbodyhistoric += '<td><img style="width: 100px; height: 40px;" src="' + value._c + '" /></td>';
                            //$(".cls-img-2").attr('src', value._c);
                            //$('#id-hd-img2').val(value._c);
                            break;
                        case 3:
                            _vbodyhistoric += '<td><img style="width: 100px; height: 40px;" src="' + value._c + '" /></td>';
                            //$(".cls-img-3").attr('src', value._c);
                            //$('#id-hd-img3').val(value._c);
                            break;
                        case 4:
                            _vbodyhistoric += '<td><img style="width: 100px; height: 40px;" src="' + value._c + '" /></td>';
                            //$(".cls-img-4").attr('src', value._c);
                            //$('#id-hd-img4').val(value._c);
                            break;
                    }
                    //_vbodyhistoric+="</tr>"
                }
            });
            //if ($('#id-hd-img1').val().trim() == '') {
            //    $('#foto1').css("visibility", "hidden");;
            //}
            //if ($('#id-hd-img2').val().trim() == '') {
            //    $('#foto2').css("visibility", "hidden");;
            //}
            //if ($('#id-hd-img3').val().trim() == '') {
            //    $('#foto3').css("visibility", "hidden");;
            //}
            //if ($('#id-hd-img4').val().trim() == '') {
            //    $('#foto4').css("visibility", "hidden");;
            //}
            $("#tb_historialfoto tbody").append("<tr>" + _vbodyhistoric + "</tr>");

            $("#id-cls-area-comment").val(__comment);
            if (__doc != null && __doc != "") {
                $('#id-download-word').attr('href', '/Descarga/WFUnacem?vdoc=' + __doc);
                $('#id-download-word').show();
                //$('#id-download-word2').attr('href', '/Descarga/WFUnacem?vdoc=' + __doc);
                //$('#id-download-word2').show();
            }
        }
    });
});

$(document).on('click', 'div[data-solicitud-archivo] img', function (e) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [],
        vImage = new Image(),
        options = {
            history: false,
            focus: false,
            showAnimationDuration: 0,
            hideAnimationDuration: 0,
            shareEl: false
        };

    $.each($('div[data-solicitud-archivo] img'), function (i, v) {
        vImage.src = $(this).attr('src');
        __arrayFotos.push({ src: $(this).attr('src'), w: vImage.naturalWidth, h: vImage.naturalHeight });
    });

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
});

$(document).on('click', '#id-md-fotomontaje img', function (e) {
    //if (vggrupo !== 3) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
    __arrayFotos = [],
    vImage = new Image(),
    options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false
    };
    //console.log(ArrayFotomontaje);
    if (ArrayFotomontaje.length > 0) {
        $.each(ArrayFotomontaje, function (i, v) {
            vImage.src = v.Url;
            __arrayFotos.push({ src: v.Url, w: vImage.naturalWidth, h: vImage.naturalHeight });
        });

        var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
        gallery.init();
    } else {
        //alert("no hay imagenes por mostrar")
    }

    //}    
});


$(document).on('click', '#id-md-fotomontaje input[type="file"]', function (e) {
    if (vggrupo !== 3) {
        return false;
    }
});
$(document).on('change', '#id-md-fotomontaje input[type="file"]', function (e) {
    var sms = $(this).val();
    var vid = $('#id-md-fotomontaje').data('codigo');
    var vimg = $(this).data('img');
    var vhdd = $(this).data('hdd');
    var vorden = $(this).data('orden');
    if (sms) {
        //$(this).parent().children('span').html('<button class="cls-eliminar-file btn btn-success btn-xs" data-orden="' + vorden + '"  data-img="' + vimg + '" data-hdd="' + vhdd + '" >Eliminar</button>');

        $('#id-' + vimg).upload(
               'Carga_Img_FotoMon',
               { __a: vid, __b: vorden },
               function (a) {
                   var _nomfoto = String(a);
                   $('#id-md-fotomontaje .cls-img-fotomontaje td .' + vimg).attr('src', Url.Xplora + _nomfoto);
                   $('#' + vhdd).val(_nomfoto);
                   $("#foto" + vorden).css("visibility", "visible");
                   //$('.cls-img-' + vorden).css("display", "block");
                   $('.cargar-foto' + vorden).css("display", "none");

                   $('.cls-img-' + vorden).css("margin-left", "5px");
                   $('.cls-img-' + vorden).css("margin-right", "5px");
               },
               'json'
        );
    } else {
        $(this).parent().children('span').html('');
    }
});
$(document).on('click', '#id-md-fotomontaje .cls-file span > .btn', function (e) {
    $(this).parent().parent().children('input[type="file"]').val('');
    $(this).parent().parent().children('span').html('');
});
$(document).on('click', '#id-btn-generar-word', function (e) {
    if (!$('#id-hd-img1').val() && !$('#id-hd-img2').val() && !$('#id-hd-img3').val() && !$('#id-hd-img4').val()) {
        alert(".:: Para generar el word tiene que seleccionar minimo una imagen ::.");
    } else {
        $('#id-download-word').hide();
        $.ajax({
            url: 'Generar_Documento',
            beforeSend: function (xhr) { $('#id-md-fotomontaje .cls-loading').html($('.cls-rep-loading1').html()); },
            type: "POST",
            dataType: "json",
            async: true,
            data: {
                __a: $('#id-md-fotomontaje').data('npdv'),
                __b: (!$('#id-hd-img1').val() ? "" : $('#id-hd-img1').val()),
                __c: (!$('#id-hd-img2').val() ? "" : $('#id-hd-img2').val()),
                __d: (!$('#id-hd-img3').val() ? "" : $('#id-hd-img3').val()),
                __e: (!$('#id-hd-img4').val() ? "" : $('#id-hd-img4').val()),
                __f: $('#id-md-fotomontaje').data('codigo'),
                __g: (!$('#id-cls-area-comment').val() ? "" : $('#id-cls-area-comment').val())
            },
            success: function (response) {
                $('#id-md-fotomontaje .cls-loading').html('');
                //$('#id-download-word').attr('href', Url.Fachada + response).show();
                $('#id-download-word').attr('href', '/Descarga/WFUnacem?vdoc=' + response).show();
            }
        });
    }
});
$(document).on('click', '#id-btn-generar-word2', function (e) {
    if (!$('#id-hd-img1').val() && !$('#id-hd-img2').val() && !$('#id-hd-img3').val() && !$('#id-hd-img4').val()) {
        alert(".:: Para generar el word tiene que seleccionar minimo una imagen ::.");
    } else {
        $('#id-download-word2').hide();
        $.ajax({
            url: 'Generar_Documento',
            beforeSend: function (xhr) { $('#id-md-fotomontaje .cls-loading').html($('.cls-rep-loading1').html()); },
            type: "POST",
            dataType: "json",
            async: true,
            data: {
                __a: $('#id-md-fotomontaje').data('npdv'),
                __b: (!$('#id-hd-img1').val() ? "" : $('#id-hd-img1').val()),
                __c: (!$('#id-hd-img2').val() ? "" : $('#id-hd-img2').val()),
                __d: (!$('#id-hd-img3').val() ? "" : $('#id-hd-img3').val()),
                __e: (!$('#id-hd-img4').val() ? "" : $('#id-hd-img4').val()),
                __f: $('#id-md-fotomontaje').data('codigo'),
                __g: (!$('#id-cls-area-comment').val() ? "" : $('#id-cls-area-comment').val())
            },
            success: function (response) {
                $('#id-md-fotomontaje .cls-loading').html('');
                //$('#id-download-word').attr('href', Url.Fachada + response).show();s
                $('#id-download-word2').attr('href', '/Descarga/WFUnacem?vdoc=' + response).show();
            }
        });
    }
});
$(document).on('click', '.cls-eliminar-file', function (e) {
    var vorden = $(this).data('orden');
    var vhh = $(this).data('hdd');
    var vimg = $(this).data('img');
    $.ajax({
        url: 'Eliminar_Img_FotoMon',
        type: "POST",
        dataType: "json",
        async: true,
        data: {
            __a: $('#id-md-fotomontaje').data('codigo'),
            __b: vorden,
            __c: $('#' + vhh).val()
        },
        success: function (response) {
            console.log(response);
            if (response == "1") {
                //$("." + vimg).attr('src', '');
                $("." + vimg).attr('src', Url.Xplora + '/Fachada/Img/foto_nodisponible.jpg');

                $("#id-hd-img" + vorden).val('');

            }
            $("#foto" + vorden).css("visibility", "hidden");
            //$('.cls-img-' + vorden).css("display", "none");
            $('.cargar-foto' + vorden).css("display", "block");

        }
    });
});



// Modal pre-presupuesto
$(document).on('click', '.cls-a-pre-presu', function (e) {
    $('.objrequeridpre').removeClass('has-error');
    $('#id-hd-monto-cpp').val('');
    $('#id-hd-nroarchivo-cpp').val('');
    $('#id-hd-monto-cpp').val($(this).data('monto'));
    $('#id-hd-nroarchivo-cpp').val($(this).data('codigo'));

    $('#md-pre-presupuesto .modal-content .modal-footer').remove();
    $('#md-pre-presupuesto #id-cls-pre-presu').val('');
    $('#md-pre-presupuesto i').remove();
    $('#md-pre-presupuesto .cls-eliminar').remove();
    var _vid = $(this).data('id');
    var _vdoc = $(this).data('doc');
    if (_vdoc != null && _vdoc != "" && _vdoc != '""') {
        $('#md-pre-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="' + _vdoc + '" ><i class="fa fa-cloud-download"></i> Pre-Presupuesto</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
    }
    $('#md-pre-presupuesto').data('id', _vid);
    $('#md-pre-presupuesto').modal({ backdrop: 'static' });

    $("input[name='gchkcodigo']").prop("checked", false);
    $("input[name='gchkcodigo'][value='" + $('#md-pre-presupuesto').data('id') + "']").prop("checked", true); //YR 3006
});

// Modal pre-presupuesto-final
$(document).on('click', '.cls-a-pre-presu-final', function (e) {
    $('.objrequeridpre').removeClass('has-error');
    $('#id-hd-monto-cpp_final').val('');
    $('#id-hd-nroarchivo-cpp-final').val('');
    $('#id-hd-monto-cpp-final').val($(this).data('monto'));
    $('#id-hd-nroarchivo-cpp-final').val($(this).data('codigo'));

    $('#md-pre-presupuesto-final .modal-content .modal-footer').remove();
    $('#md-pre-presupuesto-final #id-cls-pre-presu').val('');
    $('#md-pre-presupuesto-final i').remove();
    $('#md-pre-presupuesto-final .cls-eliminar').remove();
    var _vid = $(this).data('id');
    var _vdoc = $(this).data('doc');
    if (_vdoc != null && _vdoc != "" && _vdoc != '""') {
        $('#md-pre-presupuesto-final .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="' + _vdoc + '" ><i class="fa fa-cloud-download"></i> Presupuesto Final</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
    }
    $('#md-pre-presupuesto-final').data('id', _vid);
    $('#md-pre-presupuesto-final').modal({ backdrop: 'static' });
});

$(document).on('change', '#md-pre-presupuesto #id-cls-pre-presu', function (e) {
    //$(this).parent().children('span').html('<button class="btn btn-success btn-xs cls-eliminar" >Eliminar</button> <i class="fa fa-check-circle" style="margin-left:20px;"></i>');

    var _vdoc = $('#md-pre-presupuesto .modal-content .modal-footer a').attr('href');
    var _vid = $('#md-pre-presupuesto').data('id');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);

    fn_carga_archivo('id-cls-pre-presu', _vid, fn_reg_prepresu, _vdoc);

    var _vstrcodigo;
    $('input[name="gchkcodigo"]:checked').each(function () {
        _vstrcodigo += $(this).val() + ',';
    });
    if (_vstrcodigo == "" || _vstrcodigo == null) {
        _vstrcodigo = "0"
    } else {
        _vstrcodigo = _vstrcodigo.substring(0, ((_vstrcodigo.length) - 1));
        _vstrcodigo = _vstrcodigo.substring(9, _vstrcodigo.length);
    }
    fn_asoc_pdv_prepresu(_vstrcodigo, _vdoc);

});

$(document).on('change', '#md-pre-presupuesto-final #id-cls-pre-presu-final', function (e) {
    //$(this).parent().children('span').html('<button class="btn btn-success btn-xs cls-eliminar" >Eliminar</button> <i class="fa fa-check-circle" style="margin-left:20px;"></i>');

    var _vdoc = $('#md-pre-presupuesto-final .modal-content .modal-footer a').attr('href');
    var _vid = $('#md-pre-presupuesto-final').data('id');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);
    fn_carga_archivo('id-cls-pre-presu-final', _vid, fn_reg_prepresu_final, _vdoc);

});

function fn_reg_prepresu(vid, vdocu) {
    //alert(vdocu)
    $('#md-pre-presupuesto .modal-content .modal-footer').remove();
    $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforeSend: function (xhr) { $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Reg_PrePresupuesto',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: vdocu
        },
        success: function (a) {
            $('#md-pre-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="" ><i class="fa fa-cloud-download"></i> Pre-Presupuesto</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
            $('#md-pre-presupuesto .cls-loading').html('');
            $('#md-pre-presupuesto .modal-content .modal-footer a').attr('href', a);
            $('#md-pre-presupuesto #id-cls-pre-presu').val();
            $('.cls-pre-presu-' + vid).html("<img src='/Img/logoexcel.png' >").data('doc', a);
        },
        error: function (xhr) {
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
}

function fn_reg_prepresu_final(vid, vdocu) {
    //alert(vdocu)
    $('#md-pre-presupuesto-final .modal-content .modal-footer').remove();
    $('#md-pre-presupuesto-final .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforeSend: function (xhr) { $('#md-pre-presupuesto-final .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Reg_PrePresupuesto_final',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: vdocu
        },
        success: function (a) {
            $('#md-pre-presupuesto-final .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="" ><i class="fa fa-cloud-download"></i> Pre-Presupuesto</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
            $('#md-pre-presupuesto-final .cls-loading').html('');
            $('#md-pre-presupuesto-final .modal-content .modal-footer a').attr('href', a);
            $('#md-pre-presupuesto-final #id-cls-pre-presu-final').val();
            $('.cls-pre-presu-final-' + vid).html("<img src='/Img/logoexcel.png' >").data('doc', a);
        },
        error: function (xhr) {
            console.log('[Advertencia]<Reg_PrePresupuesto_final>: Problemas con el servicio.');
        }
    });
}

function fn_asoc_pdv_prepresu(vid, vdocu) {
    $('#md-pre-presupuesto .modal-content .modal-footer').remove();
    $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforeSend: function (xhr) { $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Asociacion_PrePresupuesto',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: vdocu
        },
        success: function (a) {
            $('#md-pre-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="" ><i class="fa fa-cloud-download"></i> Pre-Presupuesto</a></div> </div>');
            $('#md-pre-presupuesto .cls-loading').html('');
            $('#md-pre-presupuesto .modal-content .modal-footer a').attr('href', a);
            $('#md-pre-presupuesto #id-cls-pre-presu').val();
            $('.cls-pre-presu-' + vid).html('Editar').data('doc', a);
        },
        error: function (xhr) {
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
}

function fn_reg_det_prepresu() {
    $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html());
    var _vstrcodigo = "0";
    var montocpp = $('#id-hd-monto-cpp').val();

    montocpp = montocpp.replace(",", "");
    //$('input[name="gchkcodigo"]:checked').each(function () {
    //    _vstrcodigo += $(this).val() + ',';
    //});
    //if (_vstrcodigo == "" || _vstrcodigo == null) {
    //    _vstrcodigo = "0"
    //} else {
    //    _vstrcodigo = _vstrcodigo.substring(0, ((_vstrcodigo.length) - 1));
    //    _vstrcodigo = _vstrcodigo.substring(9, _vstrcodigo.length);
    //}

    $.ajax({
        beforeSend: function (xhr) { $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'RegDetalle_PrePresupuesto',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: $('#md-pre-presupuesto').data('id'),
            __b: montocpp,
            __c: $('#id-hd-nroarchivo-cpp').val(),
            __d: _vstrcodigo
        },
        success: function (a) {
            $('#md-pre-presupuesto .cls-loading').html('');
            alert('Actualizado satisfactoriamente.');
            fn_consutar_grilla();
        },
        error: function (xhr) {
            console.log('[Advertencia]<RegDetalle_PrePresupuesto>: Problemas con el servicio.');
        }
    });
}


function fn_reg_det_prepresu_final() {
    $('#md-pre-presupuesto-final .cls-loading').html($('.cls-rep-loading1').html());
    var _vstrcodigofinal = "0";
    var montocppfinal = $('#id-hd-monto-cpp-final').val();

    montocppfinal = montocppfinal.replace(",", "");

    $.ajax({
        beforeSend: function (xhr) { $('#md-pre-presupuesto-final .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'RegDetalle_PrePresupuesto_Final',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: $('#md-pre-presupuesto-final').data('id'),
            __b: montocppfinal,
            __c: $('#id-hd-nroarchivo-cpp-final').val(),
            __d: _vstrcodigofinal
        },
        success: function (a) {
            $('#md-pre-presupuesto-final .cls-loading').html('');
            alert('Actualizado satisfactoriamente.');
            fn_consutar_grilla();
        },
        error: function (xhr) {
            console.log('[Advertencia]<RegDetalle_PrePresupuesto_Final>: Problemas con el servicio.');
        }
    });
}

$(document).on('click', '#md-pre-presupuesto .cls-eliminar', function () {
    var _vid = $('#md-pre-presupuesto').data('id');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);

    fn_reg_prepresu_elimina(_vid, '')
    $('#md-pre-presupuesto #id-cls-pre-presu').val('');
    $('#md-pre-presupuesto i').remove();
    $('#md-pre-presupuesto .cls-eliminar').remove();
});

$(document).on('click', '#md-pre-presupuesto-final .cls-eliminar', function () {
    var _vid = $('#md-pre-presupuesto-final').data('id');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);

    fn_reg_prepresu_final_elimina(_vid, '')
    $('#md-pre-presupuesto-final #id-cls-pre-presu-final').val('');
    $('#md-pre-presupuesto-final i').remove();
    $('#md-pre-presupuesto-final .cls-eliminar').remove();
});

function fn_reg_prepresu_elimina(vid, vdocu) {
    //alert(vdocu)
    $('#md-pre-presupuesto .modal-content .modal-footer').remove();
    $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforesend: function (xhr) { $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Reg_PrePresupuesto',
        type: 'post',
        datatype: 'json',
        data: {
            __a: vid,
            __b: vdocu
        },
        success: function (a) {
            //$('#md-pre-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="" ><i class="fa fa-cloud-download"></i> pre-presupuesto</a></div> </div>');
            $('#md-pre-presupuesto .cls-loading').html('');
            $('#md-pre-presupuesto .modal-content .modal-footer a').attr('href', a);
            $('#md-pre-presupuesto #id-cls-pre-presu').val();
            $('.cls-pre-presu-' + vid).html('cargar').data('doc', a);
        },
        error: function (xhr) {
            console.log('[advertencia]<addsolicitud>: problemas con el servicio.');
        }
    });
}

function fn_reg_prepresu_final_elimina(vid, vdocu) {
    //alert(vdocu)
    $('#md-pre-presupuesto-final .modal-content .modal-footer').remove();
    $('#md-pre-presupuesto-final .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforesend: function (xhr) { $('#md-pre-presupuesto-final .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Reg_PrePresupuesto_final',
        type: 'post',
        datatype: 'json',
        data: {
            __a: vid,
            __b: vdocu
        },
        success: function (a) {
            //$('#md-pre-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="" ><i class="fa fa-cloud-download"></i> pre-presupuesto</a></div> </div>');
            $('#md-pre-presupuesto-final .cls-loading').html('');
            $('#md-pre-presupuesto-final .modal-content .modal-footer a').attr('href', a);
            $('#md-pre-presupuesto-final #id-cls-pre-presu-final').val();
            $('.cls-pre-presu-final-' + vid).html('cargar').data('doc', a);
        },
        error: function (xhr) {
            console.log('[advertencia]<addsolicitud>: problemas con el servicio.');
        }
    });
}


/* Inicio Grilla */

// Ver detalle de datos
$(document).on('click', '.cls-ver-datos', function (e) {

    ///wlopez
    $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-marca').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-celular').removeClass('has-error2');
    $('.objrequeridsolicitud #div_tipos').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-departamento').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-contacto').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-provincia').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-distrito').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-direccion').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-distribuidora').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-zona').removeClass('has-error2');
    //$('.objrequeridsolicitud #url-archivo-1').removeClass('has-error2');
    //$('#url-archivo-1').val('');
    //wlopez
    $('div[data-solicitud-archivo]').each(function () {
        $(this).remove();
    });

    $("#id-md-zona").children().removeAttr("selected");
    $('#id-md-zona').val($(this).data('p1zona'));

    $('#id-hd-codigo').val($(this).data('ids'));
    $('#id-md-nom-pdv').val($(this).data('pdv'));
    $('#id-md-ruc').val($(this).data('ruc'));
    $('#id-md-codigo').val($(this).data('cod'));
    $('#id-md-direccion').val($(this).data('direc'));
    $('#id-md-distrito').val($(this).data('dist'));
    $('#id-md-provincia').val($(this).data('prov'));
    $('#id-md-departamento').val($(this).data('dep'));
    $('#id-md-contacto').val($(this).data('cont'));
    $('#id-md-telefono').val($(this).data('telf'));
    $('#id-md-celular').val($(this).data('celu'));
    $('#id-md-distribuidora').val($(this).data('distrib'));
    //for (var i = 1; i < 6; i++) {
    //    //$('#id-img-solicitud-' + i).attr('src', Url.Xplora + $(this).data('img-' + i));
    //    $('#id-cls-img-' + i).attr('src', Url.Xplora + $(this).data('img-' + i));


    //    $('#ruta-foto-' + i).val($(this).data('img-' + i));
    //}

    //$('#id-cls-imgsol-1').attr('src', Url.Xplora + $(this).data('img-1'));
    var imagen = $(this).data('img-1');
    var ArrayFoto = [];
    $.each($(this).data('img-1').split(','), function (i, v) {
        var SolicitudArchivo = v.replace('/Fachada/Img/', '');
        var SolicitudArchivo2 = SolicitudArchivo.replace('Img_', '');
        var SolicitudArchivoId = SolicitudArchivo2.split('.', 1);
        if (SolicitudArchivoId != null && SolicitudArchivoId != '') {
            $('#DivGaleriaSolicitud').append('<div class="col-xs-12 col-sm-2 col-md-12 col-lg-2" id="' + SolicitudArchivoId + '" data-solicitud-archivo="' + SolicitudArchivo + '"><a href="javascript:"><img src="' + Url.Fachada + 'Img/' + SolicitudArchivo + '" class="img-thumbnail" alt="' + SolicitudArchivo + '" /></a><a href="javascript:" onclick="fnOnRemoveImage(\'' + SolicitudArchivoId + '\',\'' + SolicitudArchivo + '\');" style="position: absolute; right: 10px; top: -5px;"><i class="fa fa-times" style="background-color: #B40404; padding: 5px; border-radius: 13px; color: white; font-size:10px;"></i></a></div>');
        }

    });
    //$('#ruta-foto-1').val($(this).data('img-1'));

    $('#id-md-venta-soles').val($(this).data('m1'));
    $('#id-md-venta-toneladas').val($(this).data('m2'));
    $('#id-md-credito').val($(this).data('m3'));
    $('#id-md-deuda').val($(this).data('m4'));

    $('#id-md-comentario').val($(this).data('p1comen'));

    $("#id-md-marca").children().removeAttr("selected");
    $('#id-md-marca').val($(this).data('marca'));
    $('#auxbuton').val($(this).data('tipo-gralsolicitud'));

    //alert($(this).data('tipo-gralsolicitud'));
    if ($(this).data('tipo-gralsolicitud') == 1) {
        $("#id-btn-implementacion").css("border-color", "#FF0900");
        $("#id-btn-mantenimiento").css("border-color", "#000000");
    } else if ($(this).data('tipo-gralsolicitud') == 2) {
        $("#id-btn-implementacion").css("border-color", "#000000");
        $("#id-btn-mantenimiento").css("border-color", "#FF0900");
    }
    //alert($(this).data('p1tsolic'));

    //var test = "[" + $(this).data('p1tsolic') + "]";
    //parsedTest = JSON.parse(test);

    //if ($('input[name="gchktiposolic"]').is(':checked')) {
    //    $('input[name="gchktiposolic"]').children().removeAttr("selected");
    //    $.each(parsedTest, function (k, v) {
    //        $('input[name="gchktiposolic"]')[v - 1].checked = true;
    //    });
    //} else {
    //    $.each(parsedTest, function (k, v) {
    //        $('input[name="gchktiposolic"]')[v - 1].checked = true;
    //    });
    //}


    var contador = $('input[name="gchktiposolic"]').length;

    var str = String($(this).data('p1tsolic'));

    var valores = "";
    posicionaux = 0;
    posicion = str.indexOf(",");
    if (posicion != -1) {
        do {
            if (posicionaux == 0) {
                if (valores == "") {
                    valores = parseInt(str.substring(0, posicion)) + ",";
                }
            } else {
                valores += parseInt(str.substring(posicionaux + 1, posicion)) + ",";
            }
            posicionaux = posicion;
            posicion = str.indexOf(",", posicion + 1);
            if (posicion == -1) {
                valores += parseInt(str.substring(posicionaux + 1, str.length)) + ",";
            }
        } while (posicion != -1);
        var valores2 = valores.substring(0, valores.length - 1);
        var test = "[" + valores2 + "]";
    } else {
        var test = [parseInt(str)];
    }

    for (var i = 1; i <= contador; i++) {
        if (jQuery.inArray(i, test) != -1 || jQuery.inArray(i.toString(), test) != -1) {
            $('input[name="gchktiposolic"]')[i - 1].checked = true;
        } else {
            $('input[name="gchktiposolic"]')[i - 1].checked = false;
        }
    }

    $('.mask').inputmask();
    $('#btn-md-editar').show();
    $("#btn-md-editar").prop('disabled', false);
    $("#ruta-foto-1").prop('disabled', false);
    //$("#ruta-foto-2").prop('disabled', false);
    //$("#ruta-foto-3").prop('disabled', false);
    //$("#ruta-foto-4").prop('disabled', false);
    //$("#ruta-foto-5").prop('disabled', false);
    //$("#url-archivo-1").prop('disabled', false);
    //$("#url-archivo-2").prop('disabled', false);
    //$("#url-archivo-3").prop('disabled', false);
    //$("#url-archivo-4").prop('disabled', false);
    //$("#url-archivo-5").prop('disabled', false);
    $('div[data-solicitud-archivo] a').each(function () {
        $(this).attr('disabled', true);
    });

    if ($(this).data('estado') == "Enviado") {
        if ($.trim(imagen) == '') {
        } else {
            $("#btn-md-editar").prop('disabled', true);
        }
    }
    //trade - analis - gerencia trade
    if (_vg_grupo == 2 || _vg_grupo == 3 || _vg_grupo == 5) {
        if ($(this).data('estado-solicitud') == 1) {
            $('#btn-md-desactivar').css("visibility", "visible");
            $('#btn-md-activar').css("visibility", "hidden");
        } else if ($(this).data('estado-solicitud') == 2) {
            $('#btn-md-desactivar').css("visibility", "hidden");
            $('#btn-md-activar').css("visibility", "visible");
            $('#btn-md-activar').css("margin-right", "-140px");
        }
    } else {
        $('#btn-md-desactivar').css("visibility", "hidden");
        $('#btn-md-activar').css("visibility", "hidden");
    }


    fn_md_consultar_solicitud();
});

$(document).on('click', '.cls-ver-historial', function (e) {
    $('#id-md-History-Foto').modal({ backdrop: 'static' });
});

$(document).on('click', '.cls-sw-preg-fotomont i', function (e) {
    if ($(this).hasClass('cls-active') != true) {
        //$(this).parent().children('i').removeClass('cls-active');
        //$(this).addClass('cls-active');
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        var vuser = $(this).data('user');
        var vop = $(this).data('op');
        var vcomentario = $(this).data('comentario');
        var vclase = $(this).data('clase');


        if (vvalor == 'si') {
            //Aprobacion de solicitud
            //alert('SI');
            $(this).parent().children('i').removeClass('cls-active');
            $(this).addClass('cls-active');
            fn_ope_p4_ventas(vid, 1, '', 4, 2, '');
            //fn_p2_aprosolicitud(vid, 1);
        } else if (vvalor == 'no') {
            //alert('NO');
            if (vcomentario != '') {
                $(this).parent().children('i').removeClass('cls-active');
                $(this).addClass('cls-active');
                fn_ope_p4_ventas(vid, 0, '', 4, 2, '');
            } else {
                fn_modal_comentario(vid, vop, vuser, vcomentario, 1, vclase);
            }

            //fn_modal_comentario(vid, vop, vuser,''); // solicitud
        }
    }

});
function fn_ope_p4_ventas(_va, _vb, _vc, _vd, _vtipo, _vclase) {
    if (_vclase != '') {
        $("." + _vclase).parent().children('i').removeClass('cls-active');
        $("." + _vclase).addClass('cls-active');
    }
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso4_Ventas', type: 'POST', dataType: 'json',
        data: { __a: _va, __b: _vb, __c: _vc, __d: _vd },
        success: function (a) {
            //console.log(a);
            //if (_vd == 1 && _vb != 1) {
            //    $('#id-fec-aprosoli_' + _va).html(a._c);
            //    $('#id-comen-fotomontaje_' + _va).html((String(a._d).length > 20 ? String(a._d).substring(0, 20) + "..." : a._d)).data('comentario', a._d);
            //}
            //if (_vd == 1 && _vb == 1) {
            //    $('.cls-fec_apro-fotomon_' + _va).html(a._c);
            //    $('#id-comen-fotomontaje_' + _va).data('comentario', '').html('');
            //}
            if (_vd == 4) {
                $('.cls-fec-apro-prepresu-' + _va).html(a._c);
                $('#id-comen-fotomontaje_' + _va).html((String(a._d).length > 5 ? "<a>Ver</a>" : a._d)).data('comentario', a._d);

                if (_vtipo == 1) {
                    fn_ope_p4_ventas(_va, 0, '', 1, 2, _vclase);
                }
            }
        },
        error: function (xhr) { console.log('[Advertencia]<AprobacionSolicitud>: Problemas con el servicio.'); }
    });
}

$(document).on('click', '.cls-sw-preg-solicitud i', function (e) {
    if ($(this).hasClass('cls-active') != true) {

        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        var vuser = $(this).data('user');
        var vop = $(this).data('op');
        var vcomentario = $(this).data('comentario');
        var vclase = $(this).data('clase');
        if (vvalor == 'si') {
            $(this).parent().children('i').removeClass('cls-active');
            $(this).addClass('cls-active');
            //Aprobacion de solicitud
            fn_p2_aprosolicitud(vid, 1, 2, '');

        } else if (vvalor == 'no') {
            if (vcomentario != '') {
                $(this).parent().children('i').removeClass('cls-active');
                $(this).addClass('cls-active');
                fn_p2_aprosolicitud(vid, 0, 2, '');
            } else {
                fn_modal_comentario(vid, vop, vuser, vcomentario, 1, vclase);
            }

            //fn_modal_comentario(vid, vop, vuser,''); // solicitud
        }
    }
});
$(document).on('click', '#id-btn-apsolicitud-grabar', function (e) {
    //if (String(($("#id-ta-comentario").val()).trim()).length > 0) {
    //    var _vid = $('#id-md-apsolicitud-comen input[type="hidden"]').val();
    //    fn_p2_aprosolicitud(_vid,2);
    //    $('#id-md-apsolicitud-comen').modal('hide');
    //} else {
    //    $('#id-md-apsolicitud-comen #id-ta-comentario').val('').focus();
    //    //$('#id-md-apsolicitud-comen .cls-obligatorio').html('Para desaprobar la solicitud ingresar un comentario.');
    //    $('#id-md-apsolicitud-comen .cls-obligatorio').html('Para grabar primero ingresar un comentario.');
    //}

    var _vtipo = $("#id-tipo").val();
    var _vclase = $("#id-clase").val();
    var _vid = $('#id-md-apsolicitud-comen #id-numreg').val();
    var _texto = $("#id-ta-comentario").val();
    var _op = $("#id-op").val();

    if (_op == 1) {
        $(".cls-sw-preg-solicitud i").data('comentario', _texto);
    } else if (_op == 2) {
        $(".cls-sw-preg-fotomont i").data('comentario', _texto);
    }

    fn_p2_aprosolicitud(_vid, 2, _vtipo, _vclase);
    $('#id-md-apsolicitud-comen').modal('hide');

    //cls-obligatorio
});
$(document).on('click', '.cls-td-comen-solicitud', function (e) {
    var _vcomen = $(this).data('comentario');
    var _vop = $(this).data('op');

    //if (_vcomen.length > 0 || _vop==1) {
    //    fn_visualizar_comentario(_vcomen, _vop);
    //}
    var _vid = $(this).data('id');
    var _vuser = $(this).data('user');
    if (_vop == 1) {

        if (_vuser == 2) {
            fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
        } else {
            if (_vcomen.length > 0) {
                fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
            }
        }
    } else if (_vop == 2) {
        if (_vuser == 1) {
            fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
        } else {
            if (_vcomen.length > 0) {
                fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
            }
        }
    } else if (_vop == 3) {
        if (_vuser == 5) {
            fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
        } else {
            if (_vcomen.length > 0) {
                fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
            }
        }
    }

});
$(document).on('click', '#id-btn-apfotomon-grabar', function (e) {
    //var vobj = $("#id-ta-comentario").val();
    //if (String(replaceAll(vobj, ' ', '')).length > 0) {
    //    var _vid = $('#id-md-apsolicitud-comen input[type="hidden"]').val();
    //    var _texto = $("#id-ta-comentario").val();
    //    fn_ope_p4_ventas(_vid, 2, _texto, 1);
    //    $('#id-md-apsolicitud-comen').modal('hide');
    //} else {
    //    $('#id-md-apsolicitud-comen #id-ta-comentario').val('').focus();
    //    //$('#id-md-apsolicitud-comen .cls-obligatorio').html('Para desaprobar la solicitud ingresar un comentario.');
    //    $('#id-md-apsolicitud-comen .cls-obligatorio').html('Para grabar primero ingresar un comentario.');
    //}
    //cls-obligatorio

    var _vtipo = $("#id-tipo").val();
    var _vclase = $("#id-clase").val();
    var _vid = $('#id-md-apsolicitud-comen #id-numreg').val();
    var _texto = $("#id-ta-comentario").val();
    var _op = $("#id-op").val();

    if (_op == 1) {
        $(".cls-sw-preg-solicitud i").data('comentario', _texto);
    } else if (_op == 2) {
        $(".cls-sw-preg-fotomont i").data('comentario', _texto);
    }

    fn_ope_p4_ventas(_vid, 2, _texto, 1, _vtipo, _vclase);
    //if (vtipo == 1) {
    //    fn_ope_p4_ventas(_vid, 0, '', 1);
    //}
    $('#id-md-apsolicitud-comen').modal('hide');

});
// Aprobacion de solicitud TRADE Paso 2
function fn_p2_aprosolicitud(vid, vop, vtipo, vclase) {
    //alert(vid);//1528
    //alert(vop);//5
    //alert(vtipo);//2
    //alert(vclase);//--
    //return false;
    if (vclase != '') {
        $("." + vclase).parent().children('i').removeClass('cls-active');
        $("." + vclase).addClass('cls-active');
    }
    var _vestado = ""
    var _vcomentario = "";
    if (vop == 1) {
        _vestado = 1;
    } else if (vop == 0) {
        _vestado = 0;
    } else if (vop == 2) {
        _vestado = 2;
    } else if (vop == 3) {
        _vestado = 3;
    } else if (vop == 4) {
        _vestado = 4;
    } else if (vop == 5) {
        _vestado = 5;
    }
    _vcomentario = $("#id-ta-comentario").val();
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'AproSolicitudp2',
        type: 'POST',
        dataType: 'json',
        data: {
            vnumero: vid
            , vestado: _vestado
            , vcomentario: _vcomentario
        },
        success: function (a) {
            if (vop == 0 || vop == 1 || vop == 2) {
                $('#id-fec-aprosoli_' + vid).html(a._a);
                //$('#id-comen-aprosoli_' + vid).html((String(a._b).length > 20 ? String(a._b).substring(0, 20) + "..." : a._b));
                $('#id-comen-aprosoli_' + vid).html((String(a._b).length > 5 ? "<a>Ver</a>" : a._b));
                $('#id-comen-aprosoli_' + vid).data('comentario', a._b);
                if (vtipo == 1) {
                    fn_p2_aprosolicitud(vid, 0, 2, '');
                }
                if (vop == 0) {
                    fn_consutar_grilla();
                }
            } else if (vop == 3 || vop == 4 || vop == 5) {
                $('#id-fec-aprosoli_fv_' + vid).html(a._a);
                //$('#id-comen-aprosoli_' + vid).html((String(a._b).length > 20 ? String(a._b).substring(0, 20) + "..." : a._b));
                $('#id-comen-aprosoli_fv_' + vid).html((String(a._b).length > 5 ? "<a>Ver</a>" : a._b));
                $('#id-comen-aprosoli_fv_' + vid).data('comentario', a._b);
                //alert(vtipo);//1    //2
                //alert(vop);//5      //3
                if (vtipo == 1) {
                    fn_p2_aprosolicitud(vid, 3, 2, '');
                }
                if (vop == 3) {
                    fn_consutar_grilla();
                }
            }

        },
        error: function (xhr) {
            console.log('[Advertencia]<AprobacionSolicitud>: Problemas con el servicio.');
        }
    });
}
function fn_modal_comentario(vid, vop, vuser, vcomen, vtipo, vclase) {
    $("#id-md-apsolicitud-comen #id_tipo").val(vtipo);
    $('#id-md-apsolicitud-comen .pull-left button').hide();
    $('#id-md-apsolicitud-comen .cls-obligatorio').html('');
    $('#id-md-apsolicitud-comen #id-numreg').val("");
    $('#id-md-apsolicitud-comen #id-tipo').val("");
    $('#id-md-apsolicitud-comen #id-clase').val("");
    $("#id-md-apsolicitud-comen .cls-area").val("");
    $("#id-md-apsolicitud-comen .cls-op").val("");
    $('#id-md-apsolicitud-comen #id-numreg').val(vid);
    $('#id-md-apsolicitud-comen #id-tipo').val(vtipo);
    $('#id-md-apsolicitud-comen #id-op').val(vop);
    $('#id-md-apsolicitud-comen #id-clase').val(vclase);
    if (vop == 1) {
        $("#id-md-apsolicitud-comen .cls-area").val(vcomen);
        if (vuser == 2) {
            $('#id-md-apsolicitud-comen #id-btn-apsolicitud-grabar').show();
            $("#id-md-apsolicitud-comen .cls-area").prop('disabled', false);
        } else {
            $("#id-md-apsolicitud-comen .cls-area").prop('disabled', true);
        }
    } else if (vop == 2) {
        $("#id-md-apsolicitud-comen .cls-area").val(vcomen);
        if (vuser == 1) {
            $('#id-md-apsolicitud-comen #id-btn-apfotomon-grabar').show();
            $("#id-md-apsolicitud-comen .cls-area").prop('disabled', false);
        } else {
            $("#id-md-apsolicitud-comen .cls-area").prop('disabled', true);
        }
    } else if (vop == 3) {
        $("#id-md-apsolicitud-comen .cls-area").val(vcomen);
        if (vuser == 5) {
            $('#id-md-apsolicitud-comen #id-btn-apsolicitud-fv-grabar').show();
            $("#id-md-apsolicitud-comen .cls-area").prop('disabled', false);
        } else {
            $("#id-md-apsolicitud-comen .cls-area").prop('disabled', true);
        }
    }

    $('#id-md-apsolicitud-comen').modal({ backdrop: 'static' });
}
function fn_visualizar_comentario(vcomentario, _vop) {
    $('#id-md-apsolicitud-comen .cls-obligatorio').html('');

    $('#id-md-apsolicitud-comen .modal-footer .pull-left button').hide();


    $("#id-md-apsolicitud-comen .cls-area").val(vcomentario);
    $('#id-md-apsolicitud-comen').modal({ backdrop: 'static' });
}

//Carga carta
$(document).on('click', '.cls-crg-carta', function (e) {
    $('#id-md-carta-fotomont .modal-content .modal-footer').remove();
    $('#id-md-carta-fotomont #id-file-up-carta-fotomon').val('');
    $('#id-md-carta-fotomont i').remove();
    $('#id-md-carta-fotomont .cls-eliminar').remove();
    var _vid = $(this).data('idreg');
    var _vdoc = $(this).data('doc');
    if (_vdoc != null && _vdoc != "") {
        $('#id-md-carta-fotomont .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" onclick="mostrarPDF(\'' + Url.Xplora + _vdoc + '\');"  ><i class="fa fa-cloud-download"></i> Carta aceptación</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
        $('#id-md-carta-fotomont').data('doc', _vdoc);
    }
    $('#id-md-carta-fotomont').data('codigo', _vid);
    $('#id-md-carta-fotomont').modal({ backdrop: 'static' });
});

$(document).on('change', '#id-file-up-carta-fotomon', function (e) {
    //$(this).parent().children('span').html('<button class="btn btn-success btn-xs cls-eliminar" >Eliminar</button> <i class="fa fa-check-circle" style="margin-left:20px;"></i>');
    var _vid = $('#id-md-carta-fotomont').data('codigo');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);

    var _vdoc = e.target.files[0].toString();  //$('#id-md-carta-fotomont').data('doc');
    //var tmppath = e.target.files[0].toString(); //URL.createObjectURL(e.target.files[0]);
    //console.log('el nombre es: ' + tmppath);

    fn_carga_archivo('id-file-up-carta-fotomon', _vid, fn_reg_carta, _vdoc);
});

function fn_reg_carta(vid, vdocu) {
    $('#id-md-carta-fotomont .modal-content .modal-footer').remove();
    $('#id-md-carta-fotomont .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforeSend: function (xhr) { $('#id-md-carta-fotomont .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Reg_Paso4_Ventas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: 1,
            __c: vdocu,
            __d: 2
        },
        success: function (a) {
            $('#id-md-carta-fotomont .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" onclick="mostrarPDF(\'' + Url.Xplora + a._c + '\');" ><i class="fa fa-cloud-download"></i> Carta aprobación</a></div>  <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
            $('#id-md-carta-fotomont .cls-loading').html('');
            $('#id-md-carta-fotomont').data('doc', a._c);
            //alert(a._c);
            $('#id-md-carta-fotomont #id-cls-pre-presu').val();
            //$('#id-crg-carta-' + vid).html('Editar').data('doc', a._c);
            $('#id-crg-carta-' + vid).html("<img src='/Img/ipdf.png' >").data('doc', a._c);
            fn_ope_p4_ventas(vid, 1, '', 1, 2, 'CLS-FOTOSI-' + vid);
        },
        error: function (xhr) {
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
}
$(document).on('click', '#id-md-carta-fotomont .cls-eliminar', function (e) {
    var _vid = $('#id-md-carta-fotomont').data('codigo');
    var _vdoc = $('#id-md-carta-fotomont').data('doc');
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso4_Ventas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _vid,
            __b: 2,
            __c: _vdoc,
            __d: 2
        },
        success: function (a) {
            $('#id-file-up-carta-fotomon').parent().children('span').html('');
            $('#id-md-carta-fotomont .modal-content .modal-footer').remove();
            $('#id-md-carta-fotomont').data('doc', '');
            $('#id-md-carta-fotomont #id-file-up-carta-fotomon').val('');
            $('#id-crg-carta-' + _vid).html('Gargar').data('doc', '');
        },
        error: function (xhr) {
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
});

//Carga Carta Presentacion
$(document).on('click', '.cls-crg-carta-present', function (e) {
    $('#id-md-carta-present .modal-content .modal-footer').remove();
    $('#id-md-carta-present #id-file-up-carta-fotomon').val('');
    $('#id-md-carta-present i').remove();
    $('#id-md-carta-present .cls-eliminar').remove();
    var _vid = $(this).data('idreg');
    var _vdoc = $(this).data('doc');
    if (_vdoc != null && _vdoc != "") {
        $('#id-md-carta-present .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" onclick="mostrarPDF(\'' + Url.Xplora + _vdoc + '\');"  ><i class="fa fa-cloud-download"></i> Carta presentación</a></div> </div>');
        $('#id-md-carta-present').data('doc', _vdoc);
    }
    $('#id-md-carta-present').data('codigo', _vid);
    $('#id-md-carta-present').modal({ backdrop: 'static' });
});

$(document).on('change', '#id-file-up-carta-present', function (e) {
    $(this).parent().children('span').html('<button class="btn btn-success btn-xs cls-eliminar" >Eliminar</button> <i class="fa fa-check-circle" style="margin-left:20px;"></i>');
    var _vid = $('#id-md-carta-present').data('codigo');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);

    var _vdoc = e.target.files[0].toString();  //$('#id-md-carta-fotomont').data('doc');
    //var tmppath = e.target.files[0].toString(); //URL.createObjectURL(e.target.files[0]);
    //console.log('el nombre es: ' + tmppath);

    fn_carga_archivo('id-file-up-carta-present', _vid, fn_reg_carta_present, _vdoc);
});

function fn_reg_carta_present(vid, vdocu) {
    //alert(vid)
    //alert(vdocu)
    $('#id-md-carta-present .modal-content .modal-footer').remove();
    $('#id-md-carta-present .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforeSend: function (xhr) { $('#id-md-carta-present .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Reg_Paso4_Ventas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: 1,
            __c: vdocu,
            __d: 3
        },
        success: function (a) {
            $('#id-md-carta-present .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" onclick="mostrarPDF(\'' + Url.Xplora + a._c + '\');" ><i class="fa fa-cloud-download"></i> Carta aprobación</a></div> </div>');
            $('#id-md-carta-present .cls-loading').html('');
            $('#id-md-carta-present').data('doc', a._c);
            alert(a._c);
            $('#id-md-carta-present #id-cls-pre-presu').val();
            $('#id-crg-carta-present-' + vid).html('Editar').data('doc', a._c);
        },
        error: function (xhr) {
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
}
$(document).on('click', '#id-md-carta-present .cls-eliminar', function (e) {
    var _vid = $('#id-md-carta-present').data('codigo');
    var _vdoc = $('#id-md-carta-present').data('doc');
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso4_Ventas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _vid,
            __b: 2,
            __c: _vdoc,
            __d: 3
        },
        success: function (a) {
            $('#id-file-up-carta-present').parent().children('span').html('');
            $('#id-md-carta-present .modal-content .modal-footer').remove();
            $('#id-md-carta-present').data('doc', '');
            $('#id-md-carta-present #id-file-up-carta-present').val('');
            $('#id-crg-carta-present' + vid).html('Grabar').data('doc', '');
        },
        error: function (xhr) {
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
});

//ps 5
$(document).on('click', '.cls-sw-pre-presu i', function (e) {
    if ($(this).hasClass('cls-active') != true) {
        $(this).parent().children('i').removeClass('cls-active');
        $(this).addClass('cls-active');
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        if (vvalor == 'si') {
            fn_ope_p5_trade(vid, '', '', '', 1, 1);
        } else if (vvalor == 'no') {
            fn_ope_p5_trade(vid, '', '', '', 0, 1);
        }
    }
});
function fn_ope_p5_trade(_va, _vb, _vc, _vd, _ve, _vf) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso5_Trade',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc, __d: _vd, __e: _ve, __f: _vf
        },
        success: function (a) {
            if (_vf == 1) {
                $('.cls-fec-apro-prepresu-' + _va).html(a);
            }
            if (_vf == 2) {
                $('.cls-datos-p5 .cls-1-' + _va).html(_vb);
                $('.cls-datos-p5 .cls-2-' + _va).html(_vc);

                $('.cls-datos-p5 .cls-3-' + _va).html(_vd.length > 0 ? "<a>Ver</a>" : _vd);

                $('.cls-datos-p5').data('comentario', _vd);

                $('#id-md-p5-trade').modal('hide');
            }
        },
        error: function (xhr) {
            console.log('[Advertencia]<p5>: Problemas con el servicio.');
        }
    });
}
$(document).on('click', '.cls-datos-p5', function (e) {
    var _vid = $(this).data('id');
    var _comentario_trade = $(this).data('comentario');
    var _code_trade = $(this).data('cod');
    //alert(_code_trade);
    //$('#txt-ord-compra').val('');
    //$('#txt-Hes').val('');
    $('#txt-comen-p5').val('');
    ///////////////////////////
    //$('#txt-ord-compra').val($('.cls-datos-p5 .cls-1-' + _vid).html());
    //$('#txt-Hes').val($('.cls-datos-p5 .cls-2-' + _vid).html());
    $('#txt-comen-p5').val(_comentario_trade);

    $('#id-md-p5-trade').data('id', _vid);
    $('#id-md-p5-trade').modal({ backdrop: 'static' });

    //$("#id-md-p5-trade #id-btn-p5-grabar").css('visibility', 'visible');
    //$('#id-md-p5-trade table td:eq(0)').css('visibility', 'visible');
    //$('#id-md-p5-trade table td:eq(1)').css('visibility', 'visible');
    //$('#id-md-p5-trade table td:eq(2)').css('visibility', 'visible');
    //$('#id-md-p5-trade table td:eq(3)').css('visibility', 'visible');

    //$('#id-md-p5-trade #txt-comen-p5').prop('disabled', false);
    //if (_code_trade == "2") {
    //    $("#id-md-p5-trade #id-btn-p5-grabar").css('visibility', 'hidden');

    //    $('#id-md-p5-trade table td:eq(0)').css('visibility', 'hidden');
    //    $('#id-md-p5-trade table td:eq(1)').css('visibility', 'hidden');
    //    $('#id-md-p5-trade table td:eq(2)').css('visibility', 'hidden');
    //    $('#id-md-p5-trade table td:eq(3)').css('visibility', 'hidden');

    //    $('#id-md-p5-trade #txt-comen-p5').prop('disabled', true);

    //} 
});
$(document).on('click', '#id-btn-p5-grabar', function (e) {
    var _vid = $('#id-md-p5-trade').data('id');
    var _a = $('#txt-ord-compra').val();
    _a = (_a == null ? "" : _a);
    var _b = $('#txt-Hes').val();
    _b = (_b == null ? "" : _b);
    var _c = $('#txt-comen-p5').val();
    _c = (_c == null ? "" : _c);
    fn_ope_p5_trade(_vid, _a, _b, _c, 0, 2);
});

//ps 6
function fn_ope_p6_lucky(_va, _vb, _vc) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_Lucky',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc
        },
        success: function (a) {
            if (_va == 1) {
                alert("Comentario guardado con exito!");
                $('.cls-datos-p6 .cls-1-' + _vc).html(_vb.length > 5 ? "<a>Ver</a>" : _vb);


                $('.cls-datos-p6-' + _vc).data('comentario', _vb);

                $('#id-md-p6-lucky').modal('hide');
            }
        },
        error: function (xhr) {
            console.log('[Advertencia]<p6>: Problemas con el servicio.');
        }
    });
}
$(document).on('click', '.cls-datos-p6', function (e) {
    var _vid = $(this).data('id');
    var _vcomen = $(this).data('comentario');
    var _code = $(this).data('cod');

    if (_vcomen.length > 0) {
        var _vcomen = $(this).data('comentario');
    } else {
        var _vcomen = "";
    }
    //$('#txt-comen-p6').val('');
    ///////////////////////////
    $('#txt-comen-p6').val(_vcomen);

    $('#id-md-p6-lucky').data('id', _vid);
    $('#id-md-p6-lucky').modal({ backdrop: 'static' });
    $("#id-md-p6-lucky #id-btn-p6-grabar").css('visibility', 'visible');
    if (_code != "3") {
        $("#id-md-p6-lucky #id-btn-p6-grabar").css('visibility', 'hidden');
    }
});
$(document).on('click', '#id-btn-p6-grabar', function (e) {
    var _vid = $('#id-md-p6-lucky').data('id');

    var _a = $('#txt-comen-p6').val();
    _a = (_a == null ? "" : _a);

    fn_ope_p6_lucky(1, _a, _vid);
});
function fn_ope_unacem(_va, _vb, _vc) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Unacem',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc
        },
        success: function (a) {
            fn_consutar_grilla();
        },
        error: function (xhr) {
            console.log('[Advertencia]<p6>: Problemas con el servicio.');
        }
    });
}
function fn_ope_p3_lucky(_va, _vb, _vc) {

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso3_Lucky',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc
        },
        success: function (a) {
            if (_va == 4) {

                alert("Registro actualizado con exito!");
                //fn_consutar_grilla();
            }
        },
        error: function (xhr) {
            console.log('[Advertencia]<p3>: Problemas con el servicio.');
        }
    });
}

$(document).on('change', '.cls-datos-p3', function (e) {
    var _vid = $(this).data('id');
    var _a = this.value;
    _a = (_a == null ? "" : _a);

    fn_ope_p3_lucky(4, _a, _vid);
});

//Modal Docs
$(document).on('click', '.cls-ver-docs', function (e) {
    $('.mask').inputmask();
    var _id = $(this).data('id');
    $('#id-md-st-doc-lucky').data('id', _id);
    $('.cls-cl-estado').html('').removeClass('cls-ok').removeClass('cls-pendiente').addClass('cls-pendiente').html('Pendiente');
    $('.mask').val('');
    $('.mask').inputmask();
    $('#id-md-st-doc-lucky table td .pull-right i').hide().data('url', '');
    $('.cls-f-doc').val('');
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_LuckyAnalis',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _id, __b: '', __c: '', __d: 0, __e: 2, __f: ''
        },
        success: function (a) {
            console.log(a);
            var _vcount = 0;
            $.each(a, function (k, v) {
                if (v._c != null && v._c != "") {
                    $('.cls-estado-' + v._b).removeClass('cls-pendiente').addClass('cls-ok').html('OK');
                    $('#id-pdf-doc-' + v._b).show();
                    $('#id-pdf-doc-' + v._b).data('url', v._c);
                    $('#id-btn-doc-' + v._b).data('doc', v._c);
                    _vcount++;
                }
                $('#dt-doc-' + v._b).val(v._e);

                $('#nom-doc-' + v._b).val(v._h);
            });

            var _vcolor = '';
            if (_vcount == 0) {
                _vcolor = '#F47173';
            } else if (_vcount > 0 && _vcount < 10) {
                _vcolor = '#FEDF78';
            } else if (_vcount == 10) {
                _vcolor = '#5DBB77';
            }

            $('.cls-ver-doc-' + _id + ' i').css('color', _vcolor);

            $('#id-md-st-doc-lucky').modal({ backdrop: 'static' });
        },
        error: function (xhr) {
            console.log('[Advertencia]<p6>: Problemas con el servicio.');
        }
    });

});
$(document).on('click', '.cls-cg-fl-doc', function (e) {
    var _orden = $(this).data('orden');
    var _id = 'id-fl-doc-' + _orden;
    var _fecha = $('#dt-doc-' + _orden).val();
    var _nombre = $('#nom-doc-' + _orden).val();
    var id = $('#id-md-st-doc-lucky').data('id');
    var vdoc = $(this).data('doc');
    fn_carga_archivo2(_id, id, fn_ope_p6_LuckyAnalis, vdoc, 1, { "fecha": _fecha, "orden": _orden, "op": 1, "nombre": _nombre });
});
function fn_ope_p6_LuckyAnalis(_vid, _vfoto, _vparametros) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_LuckyAnalis',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _vid, __b: _vfoto, __c: _vparametros.fecha, __d: _vparametros.orden, __e: _vparametros.op, __f: _vparametros.nombre
        },
        success: function (a) {
            if (_vparametros.op == 1) {
                $.each(a, function (k, v) {
                    if (v._c != null && v._c != "") {
                        $('.cls-estado-' + v._b).removeClass('cls-pendiente').addClass('cls-ok').html('OK');
                        $('#id-pdf-doc-' + v._b).show();
                        $('#id-pdf-doc-' + v._b).data('url', v._c);
                        _vcount++;
                    } else {
                        $('.cls-estado-' + v._b).removeClass('cls-pendiente').removeClass('cls-ok').addClass('cls-pendiente').html('Pendiente');
                        $('#id-pdf-doc-' + v._b).data('url', '').hide();
                    }
                });

                var _vcount = 0;
                $("#id-md-st-doc-lucky .cls-cl-estado").each(function (index) {
                    if ($(this).html() == 'OK') { _vcount++; }
                });

                var _vcolor = '';
                if (_vcount == 0) { _vcolor = '#F47173'; }
                else if (_vcount > 0 && _vcount < 10) { _vcolor = '#FEDF78'; }
                else if (_vcount == 10) { _vcolor = '#5DBB77'; }

                $('.cls-ver-doc-' + _vid + ' i').css('color', _vcolor);
                alert('.:: Registro guardado con exito ::.');
            }
            if (_vparametros.op == 2) {
                alert(a);
            }
        },
        error: function (xhr) {
            console.log('[Advertencia]<p6>: Problemas con el servicio.');
        }
    });

}
function fn_mdoc(vid) {
    var _vdoc = $('#' + vid).data('url');
    mostrarPDF(_vdoc);
}

// P7 tramite
$(document).on('click', '.cls-p7-tramite', function (e) {
    $('#txt-fec-ing-expe').val('');
    $('#txt-entre-licencia').val('');
    $('#txt-venc-licencia').val('');
    $('.mask').inputmask();
    $('#fl-tra-f1').data('doc', '').val('');
    $('#fl-tra-f2').data('doc', '').val('');
    var _id = $(this).data('id');
    $('#id-md-p7-lucky-tramite').data('id', _id);
    $('#id-md-p7-lucky-tramite').data('op', 1);
    $('#btn-grabar-tramite').html("Grabar");
    $('#id-md-p7-lucky-tramite').modal({ backdrop: 'static' });
    fn_ope_p7_LuckyTramite(_id, 2);
});

$(document).on('click', '.cls-p7-renovacion', function (e) {
    $('#txt-fec-ing-expe').val('');
    $('#txt-entre-licencia').val('');
    $('#txt-venc-licencia').val('');
    $('.mask').inputmask();
    $('#fl-tra-f1').data('doc', '').val('');
    $('#fl-tra-f2').data('doc', '').val('');
    $('#fl-tra-f3').data('doc', '').val('');

    $('#fl-tra-f3').parent().children('span').html('');
    $('#fl-tra-f2').parent().children('span').html('');
    $('#fl-tra-f1').parent().children('span').html('');

    var _id = $(this).data('id');
    //var _fec_ingreso_expe = $(this).data('fec-ingreso-expe');
    //var _url_rec_tramite = $(this).data('url-rec-tramite');
    //var _obs_tramite = $(this).data('obs_tramite');
    //var _fec_entre_licencia = $(this).data('fec-entre-licencia');
    //var _fec_cadu_licencia = $(this).data('fec-cadu-licencia');
    //var _url_licencia = $(this).data('url-licencia');
    //var _p3_monto_presupuesto_final = $(this).data('p3-monto-presupuesto-final');
    //var _p3_codigo_presupuesto_final = $(this).data('p3-codigo-presupuesto-final');
    //var _url_presu_final = $(this).data('url-presu-final');

    $('#id-md-p7-lucky-tramite').data('id', _id);
    $('#id-md-p7-lucky-tramite').data('op', 2);
    $('#id-md-p7-lucky-tramite').data('fec_ingreso_expe', $(this).data('fec-ingreso-expe'));
    $('#id-md-p7-lucky-tramite').data('url_rec_tramite', $(this).data('url-rec-tramite'));
    $('#id-md-p7-lucky-tramite').data('obs_tramite', $(this).data('obs_tramite'));
    $('#id-md-p7-lucky-tramite').data('fec_entre_licencia', $(this).data('fec-entre-licencia'));
    $('#id-md-p7-lucky-tramite').data('fec_cadu_licencia', $(this).data('fec-cadu-licencia'));
    $('#id-md-p7-lucky-tramite').data('url_licencia', $(this).data('url-licencia'));
    $('#id-md-p7-lucky-tramite').data('p3_monto_presupuesto_final', $(this).data('p3-monto-presupuesto-final'));
    $('#id-md-p7-lucky-tramite').data('p3_codigo_presupuesto_final', $(this).data('p3-codigo-presupuesto-final'));
    $('#id-md-p7-lucky-tramite').data('url_presu_final', $(this).data('url-presu-final'));
    $('#btn-grabar-tramite').html("Renovar");
    $('#id-md-p7-lucky-tramite').modal({ backdrop: 'static' });

    fn_ope_p7_LuckyTramite(_id, 5);
    //fn_ope_p7_LuckyTramite(_id, 5, _fec_ingreso_expe, _url_rec_tramite, _obs_tramite, _fec_entre_licencia, _fec_cadu_licencia, _url_licencia, _p3_monto_presupuesto_final, _p3_codigo_presupuesto_final, _url_presu_final);
});

$(document).on('change', '.cls-tra-file', function (e) {

    var _vid = $(this).attr('id');
    var _vdoc = $(this).data('doc');
    //alert(_vid);
    fn_carga_archivo_tramite(_vid, _vdoc);
});
$(document).on('click', '.cls-bnt-tra-eli', function (e) {

});
$(document).on('click', '#btn-grabar-tramite', function (e) {
    var _vid = $('#id-md-p7-lucky-tramite').data('id'),
        _vop = $('#id-md-p7-lucky-tramite').data('op');

    if (_vop == 2) {
        fn_ope_p7_LuckyTramite(_vid, 7);
    } else if (_vop == 1) {
        fn_ope_p7_LuckyTramite(_vid, 1);
    }
});

function fn_ope_p7_LuckyTramite(vid, vop) {

    var _a = ""; var _b = ""; var _c = ""; var _d = ""; var _e = ""; var _f = ""; var _g = ""; var _h = ""; var _i = "";

    if (vop == 1 || vop == 7) {
        _a = $('#txt-fec-ing-expe').val();
        _b = $('#fl-tra-f1').data('doc');
        _c = $('#txt-obs-ing-expe').val();
        _d = $('#txt-entre-licencia').val();
        _e = $('#txt-venc-licencia').val();
        _f = $('#fl-tra-f2').data('doc');
        _g = $('#txt-monto-pto-final').val();
        _h = $('#txt-nroarchivo-final').val();
        _i = $('#fl-tra-f3').data('doc');


        if (_g != '' && _g != 'undefined' && _g != null) {
            _g = _g.replace(",", "");
        }
    }

    /* else if (vop == 7) {
        var _fec_ingreso_expe = $('#id-md-p7-lucky-tramite').data('fec_ingreso_expe');
        var _url_rec_tramite = $('#id-md-p7-lucky-tramite').data('url_rec_tramite');
        var _obs_tramite = $('#id-md-p7-lucky-tramite').data('obs_tramite');
        var _fec_entre_licencia = $('#id-md-p7-lucky-tramite').data('fec_entre_licencia');
        var _fec_cadu_licencia = $('#id-md-p7-lucky-tramite').data('fec_cadu_licencia');
        var _url_licencia = $('#id-md-p7-lucky-tramite').data('url_licencia');
        var _p3_monto_presupuesto_final = $('#id-md-p7-lucky-tramite').data('p3_monto_presupuesto_final');
        var _p3_codigo_presupuesto_final = $('#id-md-p7-lucky-tramite').data('p3_codigo_presupuesto_final');
        var _url_presu_final = $('#id-md-p7-lucky-tramite').data('url_presu_final');

        if (_p3_monto_presupuesto_final != '' && _p3_monto_presupuesto_final != 'undefined' && _p3_monto_presupuesto_final != null) {
            _p3_monto_presupuesto_final = _p3_monto_presupuesto_final.replace(",", "");
        }

        _a = _fec_ingreso_expe;
        _b = _url_rec_tramite;
        _c = _obs_tramite;
        _d = _fec_entre_licencia;
        _e = _fec_cadu_licencia;
        _f = _url_licencia;
        _g = _p3_monto_presupuesto_final;
        _h = _p3_codigo_presupuesto_final;
        _i = _url_presu_final;

    }*/

    //if (vop != 7) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso7_LuckyTramite',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: _a,
            __c: _b,
            __d: _c,
            __e: _d,
            __f: _e,
            __g: _f,
            __h: vop,
            __i: _g,
            __j: _h,
            __k: _i
        },
        success: function (a) {
            if (a == null) return;

            if (vop == 1) {
                //if (a._a != null && a._a != "" && a._a != "0") {
                if (a._a == null || a._a == '') {
                    $('#id-td-fec_ing_expe-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-fec_ing_expe-' + vid).html(a._a).css({ "text-decoration": "underline", "color": "#3498db" });
                    if (a._b == null || a._b == '') {
                        $('#id-td-fec_ing_expe-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        $('#id-td-fec_ing_expe-' + vid).html(a._a);
                    }
                }
                if (a._b == null || a._b == '') {
                    $('#id-td-reg-tramite-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-reg-tramite-' + vid).html('<img src="/Img/ipdf.png"  class="cls-img-td" / >');
                }
                if (a._c == null || a._c == '') {
                    $('#id-td-obs-tramite-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-obs-tramite-' + vid).html(a._c).css({ "text-decoration": "underline", "color": "#3498db" });
                    $('#id-td-obs-tramite-' + vid).html(a._c);
                }
                if (a._d == null || a._d == '') {
                    $('#id-td-fec_ent-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-fec_ent-licen-' + vid).html(a._d).css({ "text-decoration": "underline", "color": "#3498db" });
                    if (a._f == null || a._f == '') {
                        $('#id-td-fec_ent-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        $('#id-td-fec_ent-licen-' + vid).html(a._d);
                    }
                }
                if (a._e == null || a._e == '') {
                    $('#id-td-fec_cadu-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-fec_cadu-licen-' + vid).html(a._e);
                    if (a._f == null || a._f == '') {
                        $('#id-td-fec_cadu-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        $('#id-td-fec_cadu-licen-' + vid).html(a._e);
                    }
                }
                if (a._f == null || a._f == '') {
                    $('#id-td-url-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-url-licen-' + vid).html('<img src="/Img/ipdf.png"  class="cls-img-td" / >');
                }
                if (a._h == null || a._h == '') {
                    $('#id-td-monto-pto-final-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-monto-pto-final-' + vid).html(a._h).css({ "text-decoration": "underline", "color": "#3498db" });
                    $('#id-td-monto-pto-final-' + vid).html(a._h);
                }
                if (a._i == null || a._i == '') {
                    if (a._j == null || a._j == '') {
                        $('#id-td-url-pto-final-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        $('#id-td-url-pto-final-' + vid).html('<img src="/Img/logoexcel.png"  class="cls-img-td" / >');
                    }
                } else {
                    if (a._j == null || a._j == '') {
                        $('#id-td-url-pto-final-' + vid).html('<span class="label label-success" style="font-size: 100% !Important;" title="' + a._i + '">' + a._i + '</span>');
                    } else {
                        $('#id-td-url-pto-final-' + vid).html('<img src="/Img/logoexcel.png"  class="cls-img-td" / ><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + a._i + '">' + a._i + '</span>');
                    }
                }
                if (a._e != null && a._e != '' && a._f != null && a._f != '') {
                    $('#id-renovacion-' + vid).empty();

                    $('#id-renovacion-' + vid).append('<a class="cls-p7-renovacion" style="text-decoration:underline;" data-id="' + vid + '" data-fec-ingreso-expe="' + a._a + '" data-url-rec-tramite="' + a._b + '" data-obs_tramite="' + a._c + '" data-fec-entre-licencia="' + a._d + '" data-fec-cadu-licencia="' + a._e + '" data-url-licencia="' + a._f + '" data-p3-monto-presupuesto-final="' + a._h + '" data-p3-codigo-presupuesto-final="' + a._i + '" data-url-presu-final="' + a._j + '">Renovar</a>')
                }

                /*if (a._j == null || a._j == '') {
                    if (true) {

                    }
                    $('#id-td-url-pto-final-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-url-pto-final-' + vid).html('<img src="/Img/logoexcel.png"  class="cls-img-td" / >');
                }*/

                alert('.:: Datos registrados con exito ::.');
                //}
            } else if (vop == 2) {
                $('#txt-fec-ing-expe').val(a._a);
                if (a._b != null && a._b != "") {
                    $('#sp-fl1 i').show().data('url', a._b);
                    $('#td-est-fl1').html('Cargado').css('color', 'blue');
                    $('#sp-trash-fl1 i').show();

                } else {
                    $('#sp-fl1 i').hide().data('url', '');
                    $('#td-est-fl1').html('Pendiente').css('color', 'red');
                    $('#sp-trash-fl1 i').hide();
                    $('#fl-tra-f1 i').remove();
                }
                $('#txt-obs-ing-expe').val(a._c);
                $('#txt-entre-licencia').val(a._d);
                $('#txt-venc-licencia').val(a._e);
                if (a._f != null && a._f != "") {
                    $('#sp-fl2 i').show().data('url', a._f);
                    $('#td-est-fl2').html('Cargado').css('color', 'blue');
                    $('#sp-trash-fl2 i').show();
                } else {
                    $('#sp-fl2 i').hide().data('url', '');
                    $('#td-est-fl2').html('Pendiente').css('color', 'red');
                    $('#sp-trash-fl2 i').hide();
                    $('#fl-tra-f2 i').remove();
                }
                $('#txt-monto-pto-final').val(a._h);
                $('#txt-nroarchivo-final').val(a._i);
                if (a._j != null && a._j != "") {
                    $('#sp-fl3 i').show().data('url', a._j);
                    $('#sp-trash-fl3 i').show();
                } else {
                    $('#sp-fl3 i').hide().data('url', '');
                    $('#sp-trash-fl3 i').hide();
                    $('#fl-tra-f3 i').remove();
                }
            } else if (vop == 3) {
                alert('.: Recibo eliminado con exito :.');
                $('#sp-fl1 i').hide().data('url', '');
                $('#td-est-fl1').html('Pendiente').css('color', 'red');
                $('#sp-trash-fl1 i').hide();
                $("#fl-tra-f1").val("");
                $('#fl-tra-f1 i').remove();
                $('#fl-tra-f1').parent().children('span').html('');
            } else if (vop == 4) {
                alert('.: Licencia eliminado con exito :.');
                $('#sp-fl2 i').hide().data('url', '');
                $('#td-est-fl2').html('Pendiente').css('color', 'red');
                $('#sp-trash-fl2 i').hide();
                $("#fl-tra-f2").val("");
                $('#fl-tra-f2 i').remove();
                $('#fl-tra-f2').parent().children('span').html('');
            } else if (vop == 5) {
                $('#txt-fec-ing-expe').val("");

                $('#sp-fl1 i').hide().data('url', '');
                $('#td-est-fl1').html('Pendiente').css('color', 'red');
                $('#sp-trash-fl1 i').hide();
                $('#fl-tra-f1 i').remove();

                $('#txt-obs-ing-expe').val("");
                $('#txt-entre-licencia').val("");
                $('#txt-venc-licencia').val("");

                $('#sp-fl2 i').hide().data('url', '');
                $('#td-est-fl2').html('Pendiente').css('color', 'red');
                $('#sp-trash-fl2 i').hide();
                $('#fl-tra-f2 i').remove();

                $('#txt-monto-pto-final').val("");
                $('#txt-nroarchivo-final').val("");

                $('#sp-fl3 i').hide().data('url', '');
                $('#sp-trash-fl3 i').hide();
                $('#fl-tra-f3 i').remove();
            } else if (vop == 6) {
                alert('.: Archivo Presupuesto eliminado con exito :.');
                $('#sp-fl3 i').hide().data('url', '');
                $('#fl-tra-f3').data('doc', '');
                $('#sp-trash-fl3 i').hide();
                $("#fl-tra-f3").val("");
                $('#fl-tra-f3 i').remove();
                $('#fl-tra-f3').parent().children('span').html('');
            } else if (vop == 7) {
                if (a._a == null || a._a == '') {
                    $('#id-td-fec_ing_expe-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-fec_ing_expe-' + vid).html(a._a).css({ "text-decoration": "underline", "color": "#3498db" });
                    $('#id-td-fec_ing_expe-' + vid).html(a._a);
                }
                if (a._b == null || a._b == '') {
                    $('#id-td-reg-tramite-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-reg-tramite-' + vid).html('<img src="/Img/ipdf.png"  class="cls-img-td" / >');
                }
                if (a._c == null || a._c == '') {
                    $('#id-td-obs-tramite-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-obs-tramite-' + vid).html(a._c).css({ "text-decoration": "underline", "color": "#3498db" });
                    $('#id-td-obs-tramite-' + vid).html(a._c);
                }
                if (a._d == null || a._d == '') {
                    $('#id-td-fec_ent-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-fec_ent-licen-' + vid).html(a._d).css({ "text-decoration": "underline", "color": "#3498db" });
                    $('#id-td-fec_ent-licen-' + vid).html(a._d);
                }
                if (a._e == null || a._e == '') {
                    $('#id-td-fec_cadu-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-fec_cadu-licen-' + vid).html(a._e).css({ "text-decoration": "underline", "color": "#3498db" });
                    $('#id-td-fec_cadu-licen-' + vid).html(a._e);
                }
                if (a._f == null || a._f == '') {
                    $('#id-td-url-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-url-licen-' + vid).html('<img src="/Img/ipdf.png"  class="cls-img-td" / >');
                }
                if (a._h == null || a._h == '') {
                    $('#id-td-monto-pto-final-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    //$('#id-td-monto-pto-final-' + vid).html(a._h).css({ "text-decoration": "underline", "color": "#3498db" });
                    $('#id-td-monto-pto-final-' + vid).html(a._h);
                }
                if (a._i == null || a._i == '') {
                    if (a._j == null || a._j == '') {
                        $('#id-td-url-pto-final-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        $('#id-td-url-pto-final-' + vid).html('<img src="/Img/logoexcel.png"  class="cls-img-td" / >');
                    }
                } else {
                    if (a._j == null || a._j == '') {
                        $('#id-td-url-pto-final-' + vid).html('<span class="label label-success" style="font-size: 100% !Important;" title="' + a._i + '">' + a._i + '</span>');
                    } else {
                        $('#id-td-url-pto-final-' + vid).html('<img src="/Img/logoexcel.png"  class="cls-img-td" / ><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + a._i + '">' + a._i + '</span>');
                    }
                }
                $('#id-renovacion-' + vid).empty();
                if (a._e != null && a._e != '' && a._f != null && a._f != '') {
                    $('#id-renovacion-' + vid).empty();

                    $('#id-renovacion-' + vid).append('<a class="cls-p7-renovacion" style="text-decoration:underline;" data-id="' + vid + '" data-fec-ingreso-expe="' + a._a + '" data-url-rec-tramite="' + a._b + '" data-obs_tramite="' + a._c + '" data-fec-entre-licencia="' + a._d + '" data-fec-cadu-licencia="' + a._e + '" data-url-licencia="' + a._f + '" data-p3-monto-presupuesto-final="' + a._h + '" data-p3-codigo-presupuesto-final="' + a._i + '" data-url-presu-final="' + a._j + '">Renovar</a>')
                }

                alert('.: Archivo renovado con exito, los datos anteriores están en el Historico :.');
            }
        },
        error: function (xhr) {
            console.log('[Advertencia]<p7>: Problemas con el servicio.');
        }
    });
    //}
}
function fn_mdoc2(vid) {
    var _vurl = $('#' + vid + ' i').data('url');
    mostrarPDF(_vurl);
}

// P7 analista
$(document).on('click', '.cls-a-repfot-presu', function (e) {
    var _vop = $(this).data('op');

    var _vtitulo = "Carga presupuesto final";
    var _vbtn = "Presupuesto";
    if (_vop == 1) {
        _vtitulo = "Carga reporte fotografico"; $('#id-fl-repfot-presu').attr('accept', '.ppt,.pptx'); _vbtn = "Rep. Fotografico";

        $(".objrequeridprefinal").css("display", "none");
    } else {
        $('#id-fl-repfot-presu').attr('accept', '.xls,.xlsx');

        $(".objrequeridprefinal").css("display", "block");

        $('#id-hd-monto-cpf').val($(this).data('monto'));
        $('#id-hd-nroarchivo-cpf').val($(this).data('codigo'));
    }
    var _vdoc = $(this).data('doc');
    var _vid = $(this).data('id');
    $('#id-fl-repfot-presu').parent().children('span').html('');
    $('#id-fl-repfot-presu').val('');
    $('#id-md-p7-repfot-presu .modal-title').html(_vtitulo);
    $('#id-md-p7-repfot-presu').data('doc', _vdoc).data('id', _vid).data('op', _vop);
    //$('#id-md-p7-repfot-presu .modal-footer .pull-right').html('');

    $('#id-md-p7-repfot-presu .modal-content .modal-footer').remove();
    if (_vdoc != null && _vdoc != "") {
        //$('#id-md-p7-repfot-presu .modal-footer .pull-right').html('<a class="btn btn-primary" href="' + _vdoc + '"><i class="fa fa-cloud-download"></i> ' + _vbtn + '</a>');
        $('#id-md-p7-repfot-presu .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="' + _vdoc + '" ><i class="fa fa-cloud-download"></i> ' + _vbtn + '</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
    }

    $('#id-md-p7-repfot-presu').modal({ backdrop: 'static' });
});
$(document).on('change', '#id-fl-repfot-presu', function (e) {
    var _vdoc = $('#id-md-p7-repfot-presu').data('doc');
    var _vid = $('#id-md-p7-repfot-presu').data('id');
    var _vop = $('#id-md-p7-repfot-presu').data('op');
    fn_carga_archivo_repfot_presu('id-fl-repfot-presu', _vdoc, _vid, _vop);
});
function fn_ope_p7_LuckyAnalista(vid, vdoc, vop) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso7_LuckyAnalista',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: vdoc,
            __c: vop
        },
        success: function (a) {
            if (vop == 1) {
                //$('#id-a-repfot-' + vid).html('Editar');
                $('#id-a-repfot-' + vid).html("<img src='/Img/logoppt.png' >");
                $('#id-a-repfot-' + vid).data('doc', vdoc);
            } else if (vop == 2) {
                $('#id-a-presuf-' + vid).html("<img src='/Img/logoexcel.png' >");
                $('#id-a-presuf-' + vid).data('doc', vdoc);
            } else if (vop == 3) {
                $('#id-md-p7-repfot-presu .modal-content .modal-footer').remove();
                $('#id-a-repfot-' + vid).html('adjuntar');
                $('#id-a-repfot-' + vid).data('doc', null);
            } else if (vop == 4) {
                $('#id-md-p7-repfot-presu .modal-content .modal-footer').remove();
                $('#id-a-presuf-' + vid).html('adjuntar');
                $('#id-a-presuf-' + vid).data('doc', null);
            }
        },
        error: function (xhr) {
            console.log('[Advertencia]<p7>: Problemas con el servicio.');
        }
    });
}

// P8 trade
$(document).on('click', '.cls-sw-pre-imple i', function (e) {
    if ($(this).hasClass('cls-active') != true) {
        $(this).parent().children('i').removeClass('cls-active');
        $(this).addClass('cls-active');
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        if (vvalor == 'si') {
            fn_ope_p8_Trade(vid, 1);
            $('.id-a-mante-' + vid).show();
            //Aprobacion de solicitud
            //fn_p2_aprosolicitud(vid, 1);
        } else if (vvalor == 'no') {
            fn_ope_p8_Trade(vid, 0);
            //fn_modal_comentario(vid); // solicitud
        }
    }
});
function fn_ope_p8_Trade(vid, vop) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso8_Trade',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: "",
            __c: vop
        },
        success: function (a) {
            //alert(a);
            $('#id-sp-fec-vali-' + vid).html(a);
        },
        error: function (xhr) {
            console.log('[Advertencia]<p8>: Problemas con el servicio.');
        }
    });
}


//mantenimiento
$(document).on('click', '.cls-a-mante', function (e) {
    var _cg = $(this).data('cg');
    var _vid = $(this).data('id');
    var _pdv = $(this).data('nompdv');
    $('#id-md-mantenimiento').data('id', _vid);
    $('#id-md-mantenimiento').data('pdv', _pdv);

    $('#id-tab-grid-mante').removeClass('cls-btn-active');
    $('#id-tab-visi-mante').removeClass('cls-btn-active').addClass('cls-btn-active');

    $('#id-md-mantenimiento').modal({ backdrop: 'static' });
    $.ajax({
        beforeSend: function (xhr) { }, url: 'VMModal', data: { __a: _cg }, type: 'POST', dataType: 'html',
        success: function (response) { $('#dv-md-content-viman').replaceWith(response); },
        error: function (xhr) { alert('Algo salió mal, por favor intente de nuevo.'); }
    });

    $('#id-tb-mantenimiento').hide();
    $('#id-tb-visita').show();
});

$(document).on('click', '#id-md-mantenimiento .btn-cls-rep', function (e) {
    $('.cls-tab').hide();
    $('#id-md-mantenimiento .btn-cls-rep').removeClass('cls-btn-active');
    $(this).addClass('cls-btn-active');
    var _vtab = $(this).data('tab');
    $('#' + _vtab).show();
});
/* Inicio Gantt */
$(document).on('click', '.cls-a-gantt-fotomon', function (e) {
    $(this).parent().children('img').trigger('click');
    $(this).parent().children('img').show();
    $(this).hide();
});
$(document).on('click', '.cls-a-fechaentrega-fotomon', function (e) {
    $(this).parent().children('.dtp_fecha_entrega_foto').show();
    $(this).hide();
});
$(document).on('click', '.cls-a-fechaimplementacion-fotomon', function (e) {
    $(this).parent().children('.dtp_fecha_implementacion').show();
    $(this).hide();
});
$(document).on('click', '.cls-gantt-fotomon', function (e) {

    $('#id-md-Gantt .modal-body').empty();
    var _vid = $(this).data('id');
    var _vopcion = $(this).data('opcion');
    $('#id-md-Gantt').modal({ backdrop: 'static' });
    $('.cls-ft-' + _vid).show();
    $('#id-md-Gantt').on('shown.bs.modal', function (e) {
        $('#id-md-Gantt .modal-body').html('<iframe id="if_xplora" width="100%" src="GetEditGrantt?_a=' + _veq + '&_b=' + _vid + '&_c=' + _vopcion + '&_d=' + _vg_grupo + '&_e=' + _vip + '" style="height: 470px;"></iframe>');
    });

    $('#id-a-repfot-' + _vid).show();
    $('#id-a-presuf-' + _vid).show();
});

$(document).on('click', '.cls-img-visit', function () {
    var _vurl = $(this).data('url');
    $('#id-visi-img img').attr('src', _vurl);
    $('#id-visi-img').modal({ backdrop: 'static' });
});

$(document).on('click', '#id-btn-man-nuevo', function (e) {
    $('#id-new-mantenimiento .pull-left button').prop('disabled', false);
    $("#id-new-mantenimiento input").prop('disabled', false).val('');
    $('#id-new-mantenimiento').data('id', '');
    $('#id-img-mante').attr('src', '');
    $('#id-new-mantenimiento').modal({ backdrop: 'static' });
    $('#btn-man-editar').hide();
});
$(document).on('click', '', function (e) {
});
$(document).on('click', '#btn-man-grabar', function (e) {
    var _vid = $('#id-new-mantenimiento').data('id');
    fn_carga_archivo_addMan('id-man-new-carga', '', 1, fn_add_mantenimiento);
});
$(document).on('click', '#btn-man-enviar', function (e) {
    var _vid = $('#id-new-mantenimiento').data('id');
    fn_carga_archivo_addMan('id-man-new-carga', '', 2, fn_add_mantenimiento);
});
$(document).on('click', '#btn-man-editar', function (e) {

});
$(document).on('click', '#id-tab-grid-mante', function () {
    fn_consutar_grid_man();
});

function fn_add_mantenimiento(vfoto, vop) {
    var _vid = $('#id-md-mantenimiento').data('id');
    var _vdet = $('#id-new-mantenimiento').data('id');
    _vdet = (_vdet == null || _vdet == '0' ? '0' : _vdet)
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'RegNewMantenimiento',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _vid,
            __b: vfoto,
            __c: ($('#id-man-venta-soles').val() == null || $('#id-man-venta-soles').val() == "" ? "0" : $('#id-man-venta-soles').val()),
            __d: ($('#id-man-venta-toneladas').val() == null || $('#id-man-venta-toneladas').val() == "" ? "0" : $('#id-man-venta-toneladas').val()),
            __e: ($('#id-man-credito').val() == null || $('#id-man-credito').val() == "" ? "0" : $('#id-man-credito').val()),
            __f: ($('#id-man-deuda').val() == null || $('#id-man-deuda').val() == "" ? "0" : $('#id-man-deuda').val()),
            __g: vop,
            __h: vop,
            __i: _vdet
        },
        success: function (a) {
            fn_consutar_grid_man();
        },
        error: function (xhr) {
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
}

$(document).on('click', '.cls-ver-man', function (e) {
    var _vid = $(this).data('id');
    var _v1 = $(this).data('v1');
    var _v2 = $(this).data('v2');
    var _v3 = $(this).data('v3');
    var _v4 = $(this).data('v4');

    $('#id-new-mantenimiento').modal({ backdrop: 'static' });
    $('#id-man-venta-soles').val(_v1);
    $('#id-man-venta-toneladas').val(_v2);
    $('#id-man-credito').val(_v3);
    $('#id-man-deuda').val(_v4);
    $('#id-img-mante').attr('src', $(this).data('img'));
    $('#id-new-mantenimiento').data('id', _vid);
    $('#id-man-new-carga').val('');

    $('#id-new-mantenimiento .pull-left button').prop('disabled', true);
    if ($(this).data('estado') == "Enviado") { $("#btn-man-editar").prop('disabled', true); } else { $("#btn-man-editar").prop('disabled', false); }
    $("#id-new-mantenimiento input").prop('disabled', true);
});

$(document).on('click', '#btn-man-editar', function (e) {
    $("#id-new-mantenimiento input").prop('disabled', false);
    $('#id-new-mantenimiento .pull-left button').prop('disabled', false);
});

$(document).on('click', '.cls-sw-pre-man-soli i', function (e) {
    var _cg = $(this).data('cg');
    var _vid = $(this).data('id');
    if ($(this).hasClass('cls-active') != true) {
        $(this).parent().children('i').removeClass('cls-active');
        $(this).addClass('cls-active');
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        if (vvalor == 'si') {
            fn_pasos_mantenimiento(1, '', '', '', '', _cg, 1, _vid);
            $('#id-td-comen-soli-' + _vid).html('');
        } else if (vvalor == 'no') {
            $('#id-md-manten-comen').data('det', _vid);
            $("#id-man-comentario").val('').focus();
            $('#id-md-manten-comen').modal({ backdrop: 'static' });
        }
    }
});

function fn_pasos_mantenimiento(_vestado, _vcomen, _vdocu, _vordp, _vhes, _vcgrp, _vop, _vfila) {
    var _vid = $('#id-md-mantenimiento').data('id');
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'RegMatenimientoPasos',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _vfila,
            __b: _vestado, //Estado
            __c: _vcomen, //comentario
            __d: _vdocu,//documento
            __e: _vordp,//ord pago
            __f: _vhes,//hes
            __g: _vcgrp,//grupo,
            __h: _vop,// opcion
        },
        success: function (a) {

            if (_vcgrp == 2 && _vop == 1) {

                $('#id-td-man-fec-apro-' + _vfila).html(a);
            }
            if (_vcgrp == 3 && _vop == 1) {
                $('#md-man-presupuesto .modal-content .modal-footer').remove();
                $('#md-man-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="" ><i class="fa fa-cloud-download"></i> Cotización</a></div> </div>');
                $('#md-man-presupuesto .modal-content .modal-footer a').attr('href', a);
                $('.cls-a-coti-' + _vfila).html('Editar').data('doc', a);
            }
            if (_vcgrp == 2 && _vop == 2) {
                $('#id-td-ap-presu-' + _vfila).html(a);
            }
            if (_vcgrp == 2 && _vop == 3) {
                $('.cls-1-mante-' + _vfila).html(_vordp);
                $('.cls-2-mante-' + _vfila).html(_vhes);
                $('#id-md-archi-mante').modal('hide');
            }
            if (_vcgrp == 3 && _vop == 3) {
                $('#id-a-repfot-man-' + _vfila).html('Editar');
                $('#id-a-repfot-man-' + _vfila).data('doc', vdoc);
            }
            if (_vcgrp == 2 && _vop == 4) {
                $('#id-td-vali-fac-mante-' + _vfila).html(a);
            }
        },
        error: function (xhr) {
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
}


$(document).on('click', '.cls-man-comen-solici', function (e) {
    //  $('#id-md-manten-comen').modal({ backdrop: 'static' });
});
$(document).on('click', '#id-btn-man-comen', function (e) {
    if (String($("#id-man-comentario").val()).length > 0) {
        //var _vid = $('#id-md-apsolicitud-comen input[type="hidden"]').val();
        var _vtexto = $("#id-man-comentario").val();
        var _vid = $('#id-md-manten-comen').data('det');
        fn_pasos_mantenimiento(0, _vtexto, '', '', '', _vg_grupo, 1, _vid);
        $('#id-td-comen-soli-' + _vid).html(_vtexto);
        $('#id-md-manten-comen').modal('hide');
    } else {
        $('#id-md-manten-comen .cls-obligatorio').html('Para desaprobar la solicitud ingresar un comentario.');
    }
});
$(document).on('click', '.id-a-cotizacon', function () {
    $('#md-man-presupuesto .modal-content .modal-footer').remove();
    $('#md-man-presupuesto #id-cls-pre-presu').val('');
    $('#md-man-presupuesto i').remove();
    $('#id-cls-man-cotizacion').val('');
    $('#md-man-presupuesto .cls-eliminar').remove();
    var _vid = $(this).data('id');
    var _vdoc = $(this).data('doc');
    if (_vdoc != null && _vdoc != "") {
        $('#md-man-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="' + _vdoc + '" ><i class="fa fa-cloud-download"></i> Cotizacón</a></div> </div>');
    }
    $('#md-man-presupuesto').data('id', _vid);
    $('#md-man-presupuesto').modal({ backdrop: 'static' });
});
$(document).on('change', '#id-cls-man-cotizacion', function (e) {
    $(this).parent().children('span').html('<i class="fa fa-check-circle" style="margin-left:20px;"></i>');
    var _vid = $('#md-man-presupuesto').data('id');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);
    var _vdoc = $('#md-man-presupuesto .modal-content .modal-footer  a').attr('href');
    //fn_carga_archivo('id-cls-pre-presu', _vid, fn_reg_prepresu, _vdoc);
    fn_carg_arch_cotizacion('id-cls-man-cotizacion', _vdoc, _vid, fn_pasos_mantenimiento);
});

$(document).on('click', '.cls-sw-pre-man-presu i', function (e) {
    var _cg = $(this).data('cg');
    var _vid = $(this).data('id');
    if ($(this).hasClass('cls-active') != true) {
        $(this).parent().children('i').removeClass('cls-active');
        $(this).addClass('cls-active');
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        if (vvalor == 'si') {
            fn_pasos_mantenimiento(1, '', '', '', '', _cg, 2, _vid);
            $('#id-td-comen-presu-' + _vid).html('');
        } else if (vvalor == 'no') {
            $('#id-md-manten-comen-presu').data('det', _vid);
            $("#id-man-comentario-presu").val('').focus();
            $('#id-md-manten-comen-presu').modal({ backdrop: 'static' });
        }
    }
});
$(document).on('click', '#id-btn-man-comen-presu', function (e) {
    if (String($("#id-man-comentario-presu").val()).length > 0) {
        //var _vid = $('#id-md-apsolicitud-comen input[type="hidden"]').val();
        var _vtexto = $("#id-man-comentario-presu").val();
        var _vid = $('#id-md-manten-comen-presu').data('det');
        fn_pasos_mantenimiento(0, _vtexto, '', '', '', _vg_grupo, 2, _vid);
        $('#id-td-comen-presu-' + _vid).html(_vtexto);
        $('#id-md-manten-comen-presu').modal('hide');
    } else {
        $('#id-md-manten-comen-presu .cls-obligatorio').html('Para desaprobar la solicitud ingresar un comentario.');
    }
});


$(document).on('click', '.cls-mante-trade', function (e) {
    var _vid = $(this).data('id');
    $('#txt-ord-compra-mante').val('');
    $('#txt-Hes-mante').val('');
    ///////////////////////////
    $('#txt-ord-compra-mante').val($('.cls-mante-trade .cls-1-mante-' + _vid).html());
    $('#txt-Hes-mante').val($('.cls-mante-trade .cls-2-mante-' + _vid).html());

    $('#id-md-archi-mante').data('id', _vid);
    $('#id-md-archi-mante').modal({ backdrop: 'static' });
});
$(document).on('click', '#id-btn-archi-mante', function (e) {
    var _vid = $('#id-md-archi-mante').data('id');
    var _a = $('#txt-ord-compra-mante').val();
    _a = (_a == null ? "" : _a);
    var _b = $('#txt-Hes-mante').val();
    _b = (_b == null ? "" : _b);

    fn_pasos_mantenimiento('0', '', '', _a, _b, _vg_grupo, 3, _vid);
});

$(document).on('click', '.cls-gantt-imple-man', function (e) {

    $('#id-md-Gantt .modal-body').empty();
    var _vid = $(this).data('id');
    var _vopcion = $(this).data('opcion');
    $('#id-md-Gantt').modal({ backdrop: 'static' });

    $('#id-md-Gantt').on('shown.bs.modal', function (e) {
        $('#id-md-Gantt .modal-body').html('<iframe id="if_xplora" width="100%" src="GetEditGrantt?_a=' + _veq + '&_b=' + _vid + '&_c=' + _vopcion + '&_d=' + _vg_grupo + '&_e=' + _vip + '" style="height: 470px;"></iframe>');
    });
    $('#id-a-repfot-man-' + _vid).show();
    //$('#id-a-repfot-' + _vid).show();
    //$('#id-a-presuf-' + _vid).show();
});
$(document).on('click', '.cls-a-gantt-imple-man', function (e) {
    $(this).parent().children('img').trigger('click');
    $(this).parent().children('img').show();
    $(this).hide();

});


$(document).on('click', '.cls-a-repfot-man', function (e) {
    var _vop = $(this).data('op');

    // var _vtitulo = "Carga presupuesto final";
    //var _vbtn = "Presupuesto";
    //  if (_vop == 1) { _vtitulo = "Carga reporte fotografico"; $('#id-fl-repfot-presu').attr('accept', '.ppt,.pptx'); _vbtn = "Rep. Fotografico"; } else { $('#id-fl-repfot-presu').attr('accept', '.xls,.xlsx'); }
    var _vdoc = $(this).data('doc');
    var _vid = $(this).data('id');
    $('#id-fl-repfot-mante').parent().children('span').html('');
    $('#id-fl-repfot-mante').val('');
    //$('#id-md-repfot-mante .modal-title').html(_vtitulo);
    $('#id-md-repfot-mante').data('doc', _vdoc).data('id', _vid).data('op', _vop);
    $('#id-md-repfot-mante .modal-footer .pull-right').html('');
    if (_vdoc != null && _vdoc != "") {
        $('#id-md-repfot-mante .modal-footer .pull-right').html('<a class="btn btn-primary" href="' + _vdoc + '"><i class="fa fa-cloud-download"></i> Carga</a>');
    }

    $('#id-md-repfot-mante').modal({ backdrop: 'static' });
});
$(document).on('change', '#id-fl-repfot-mante', function (e) {
    var _vdoc = $('#id-md-repfot-mante').data('doc');
    var _vid = $('#id-md-repfot-mante').data('id');
    var _vop = $('#id-md-repfot-mante').data('op');
    fn_carg_arch_repfoto_mante('id-fl-repfot-mante', _vdoc, _vid, fn_pasos_mantenimiento);
    //fn_carga_archivo_repfot_presu('id-fl-repfot-mante', _vdoc, _vid, _vop);
});

$(document).on('click', '.cls-sw-fachada-man-vali i', function (e) {
    var _cg = $(this).data('cg');
    var _vid = $(this).data('id');
    if ($(this).hasClass('cls-active') != true) {
        $(this).parent().children('i').removeClass('cls-active');
        $(this).addClass('cls-active');
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        if (vvalor == 'si') {
            fn_pasos_mantenimiento(1, '', '', '', '', _cg, 4, _vid);

        } else if (vvalor == 'no') {
            fn_pasos_mantenimiento(0, '', '', '', '', _cg, 4, _vid);
        }
    }
});

/*
        $(document).on("dp.change", '.dtp_fecha_entrega_foto', function (e) {
            //alert($('#txt_fecha_entrega_foto_'+$(this).data('id')).val() + ' ' + $(this).data('id'));          
                $.ajax({
                    url: 'WF_Una_FechaEntrega_Fotomontaje',
                    dataType: "json",
                    data: {
                        __a: $(this).data('id'),
                        __b: $('#txt_fecha_entrega_foto_' + $(this).data('id')).val()
                    },
                    type: "POST",
                    success: function (response) {
                        if (response.CodStatus == 1) {
                            alert('.:: Fecha entrega actualizado con exito!!! ::.');
                        } else {
                            alert("Error inesperado, intentelo nuevamente.");
                        }
                    }
                });
        });
*/

$(document).on('click', '#btn-mp-grabar-presupuesto', function (e) {
    var vcppmonto = $("#id-hd-monto-cpp").val();
    var vcppnro = $("#id-hd-nroarchivo-cpp").val();

    if (String(replaceAll(vcppmonto, ' ', '')).length > 0 && String(replaceAll(vcppnro, ' ', '')).length > 0) {
        $('.objrequeridpre').removeClass('has-error');
        fn_reg_det_prepresu();
    } else {
        $('#id-hd-monto-cpp').val('').focus();
        $('.objrequeridpre').addClass('has-error');   //html('Para desaprobar la solicitud ingresar un comentario.');
    }
});

$(document).on('click', '#btn-mp-grabar-presupuesto-final', function (e) {
    var vcppmonto = $("#id-hd-monto-cpp-final").val();
    var vcppnro = $("#id-hd-nroarchivo-cpp-final").val

    if (String(replaceAll(vcppmonto, ' ', '')).length > 0 && String(replaceAll(vcppnro, ' ', '')).length > 0) {
        $('.objrequeridprefinal').removeClass('has-error');
        fn_reg_det_prepresu_final();
    } else {
        $('#id-hd-monto-cpf-final').val('').focus();
        $('.objrequeridprefinal').addClass('has-error');   //html('Para desaprobar la solicitud ingresar un comentario.');
    }
});

$(document).on('click', '#id-delete-grid', function (e) {
    $('#id_num_solicitud').val($(this).data('idregdrop'));
    $('#id-md-sm-delete').modal();
});

$(document).on('click', '#id-btn-aceptar', function (e) {
    //alert($('#id_num_solicitud').val());

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Eliminar_SolicitudVentas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: $('#id_num_solicitud').val(),
            __b: 0
        },
        success: function (a) {
            fn_consutar_grilla();
            $('#id-md-sm-delete').modal('hide');
        },
        error: function (xhr) {
            console.log('[Advertencia]<RemoveSolicitud>: Problemas con el servicio.');
        }
    });
});

/* Inicio utilitarios */
function fnr_uti_soloNumeros(e) {
    var key = window.Event ? e.which : e.keyCode
    //alert(e.target.id);
    return ((key >= 48 && key <= 57));
}
function fnr_uti_soloNumerosypunto(e) {
    var key = window.Event ? e.which : e.keyCode
    //alert(e.target.id);
    var texto = $('#' + e.target.id).val();
    if (texto.indexOf(".") == -1 && texto.length != 0) {
        return ((key >= 48 && key <= 57) || key == 46);
    } else {
        return ((key >= 48 && key <= 57));
    }
}
function fn_carga_archivo(id, vop, functioncalback, vdoc, vsw, vparametros) {
    if ($("#" + id).val()) {
        if (vdoc == null) {
            vdoc = "";
        }

        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                console.log(a);
                var _nomfoto = String(a);
                if (vsw == 1) {
                    functioncalback(vop, _nomfoto, vparametros);
                } else {
                    functioncalback(vop, _nomfoto);
                    $('#id-md-solicitud').modal('hide');
                }
            },
            'json'
        );

    } else {
        if (vdoc == null) {
            vdoc = "";
        }
        //alert('waltercito');
        functioncalback(vop, vdoc);
        //alert("No selecciono ningún archivo...");
    }
}
function fn_carga_archivo2(id, vop, functioncalback, vdoc, vsw, vparametros) {
    console.log('2');
    if ($("#" + id).val()) {
        if (vdoc == null) {
            vdoc = "";
        }
        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                var _nomfoto = String(a);
                functioncalback(vop, _nomfoto, vparametros);
            },
            'json'
        );
    } else {
        functioncalback(vop, '', vparametros);
    }
}
function fn_carga_archivo_tramite(id, vdoc) {
    console.log('3-' + vdoc);
    if ($("#" + id).val()) {
        if (vdoc == null) {
            vdoc = "";
        }
        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                var _nomfoto = String(a);
                $("#" + id).data('doc', _nomfoto);
                if (_nomfoto != null && _nomfoto != "") {
                    $('#' + id).parent().children('span').html('<i class="fa fa-check-circle" style="color:#70AD47;"></i>');

                    if (id == 'fl-tra-f1') {
                        //alert(".: Recibo Cargado");
                        //$('#sp-trash-fl1 i').show();
                    } else if (id == 'fl-tra-f2') {
                        //alert(".: Licencia Cargada");
                        //$('#sp-trash-fl2 i').show();
                    }
                }
            },
            'json'
        );

    } else {

        //alert("No selecciono ningún archivo...");
    }
}
function fn_carga_archivo_repfot_presu(id, vdoc, id_reg, vop) {
    console.log('4');
    if ($("#" + id).val()) {

        if (vdoc == null) { vdoc = ""; }

        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                var _nomfoto = String(a);
                if (_nomfoto != "0" && _nomfoto != "") {
                    fn_ope_p7_LuckyAnalista(id_reg, _nomfoto, vop);
                    $('#' + id).parent().children('span').html('<i class="fa fa-check-circle" style="color:#70AD47;"></i>');
                    var _btn = "Presupuesto";
                    if (vop == 1) { _btn = "Rep. Fotografico"; }
                    //$('#id-md-p7-repfot-presu .modal-footer .pull-right').html('<a class="btn btn-primary" href="' + _nomfoto + '"><i class="fa fa-cloud-download"></i> ' + _btn + '</a>');


                    $('#id-md-p7-repfot-presu .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="' + _nomfoto + '" ><i class="fa fa-cloud-download"></i> ' + _btn + '</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
                }
            },
            'json'
        );

    } else {

        //alert("No selecciono ningún archivo...");
    }
}
function fn_carga_archivo_visita(id, vdoc, fecha, estado, orden, callback) {
    console.log('5');
    if ($("#" + id).val()) {

        if (vdoc == null) { vdoc = ""; }

        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                var _nomfoto = String(a);
                callback(fecha, _nomfoto, estado, orden);
            },
            'json'
        );

    } else {
        alert("No selecciono ningún archivo...");
    }
}
function fn_carga_archivo_addMan(id, vdoc, vop, callback) {
    console.log('6');
    //   if ($("#" + id).val()) {
    //if ($("#" + id).val() == null) { } 
    if (vdoc == null) { vdoc = ""; }

    $('#' + id).upload(
        'Carga_Archivo',
        { _b: vdoc },
        function (a) {
            var _nomfoto = String(a);
            callback(_nomfoto, vop);
        },
        'json'
    );

    // } else {
    //   alert("No selecciono ningún archivo...");
    // }
}
function fn_carg_arch_cotizacion(id, vdoc, vfila, callback) {
    console.log('7');
    if ($("#" + id).val()) {
        if (vdoc == null) { vdoc = ""; }
        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                var _nomfoto = String(a);
                callback(0, '', _nomfoto, '', '', _vg_grupo, 1, vfila);
            },
            'json'
        );
    } else {
        alert("No selecciono ningún archivo...");
    }
}
function fn_carg_arch_repfoto_mante(id, vdoc, vfila, callback) {
    console.log('8');
    if ($("#" + id).val()) {
        if (vdoc == null) { vdoc = ""; }
        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                var _nomfoto = String(a);
                callback(0, '', _nomfoto, '', '', _vg_grupo, 3, vfila);
                $('#' + id).parent().children('span').html('<i class="fa fa-check-circle" style="color:#70AD47;"></i>');
                $('#id-md-repfot-mante .modal-footer .pull-right').html('<a class="btn btn-primary" href="' + _nomfoto + '"><i class="fa fa-cloud-download"></i> Carga</a>');
            },
            'json'
        );
    } else {
        alert("No selecciono ningún archivo...");
    }
}


function fn_regvistaman(vfecha, v_nomfoto, vestado, vorden) {
    $.ajax({
        beforeSend: function (xhr) { $('.cls-ms-visita-' + vorden).html('<i class="fa fa-cog fa-spin"></i> Cargando'); },
        url: 'Reg_Vista', type: 'POST', dataType: 'json',
        data: {
            __a: $('#id-md-mantenimiento').data('id'),
            __b: vfecha,
            __c: v_nomfoto,
            __d: vestado,
            __e: 0,
            __f: vorden,
            __g: 1
        },
        success: function (a) {
            $.each(a, function (k, v) {

                if (v._a != "0") {
                    $('.cls-ms-visita-' + vorden).html('<i class="fa fa-check-circle"></i>');
                }
            });
        },
        error: function (xhr) { console.log('[Advertencia]<visita>: Problemas con el servicio.'); }
    });
}
function replaceAll(text, busca, reemplaza) {
    while (text.toString().indexOf(busca) != -1) {
        text = text.toString().replace(busca, reemplaza);
    }
    return text;
}
function mostrarPDF(vdirdoc) {
    //window.location.href = vdirdoc;

    var w = 620;
    var h = 360;
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(vdirdoc, "pdf", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}
function fn_isDate(val) {
    var d = new Date(val);
    return !isNaN(d.valueOf());
}
$(document).on('click', '#id-leyen-grid', function () {
    $('#id-md-smf').modal();
});


$(document).on('click', '#id-md-carta-fotomont .cls-quita_doc', function (e) {
    var idreg = $(this).data('idreg');
    $.ajax({
        url: 'Eliminar_Archivo',
        type: "POST",
        dataType: "json",
        async: true,
        data: {
            __a: idreg,
            __b: "",
            __c: 0,
            __d: 3
        },
        success: function (response) {
            if (response == "1") {
                $('#id-md-carta-fotomont').modal('hide');
                fn_consutar_grilla();
            }

        }
    });
});

//Autor:wlopez
//Fecha:12/12/2016
$(document).on('click', '#id-md-p7-repfot-presu .cls-eliminar', function () {
    var _vid = $('#id-md-p7-repfot-presu').data('id');

    var _vop = $('#id-md-p7-repfot-presu').data('op');

    if (_vop == 1) {
        var opction = 3;
    } else {
        if (_vop == 2) {
            var opction = 4;
        }
    }

    _vid = (_vid == null || _vid == "" ? 0 : _vid);
    //var _nomfoto = null;
    fn_ope_p7_LuckyAnalista(_vid, '', opction);
    //fn_reg_repfoto_elimina(_vid, '')
});

$(document).on('click', '#id-md-p7-lucky-tramite .cls-eliminar', function () {
    var _vid = $('#id-md-p7-lucky-tramite').data('id');
    var _id = $(this).attr("id");
    if (_id == 'eli-fl-tra-f1') {
        var opc_arch = 3;
    } else if (_id == 'eli-fl-tra-f2') {
        var opc_arch = 4;
    } else if (_id == 'eli-fl-tra-f3') {
        var opc_arch = 6;
    }

    fn_ope_p7_LuckyTramite(_vid, opc_arch);
});

$(document).on('blur', '#id-md-ruc', function (e) {
    var text = $(this).val();
    text = text.replace("_", "");

    text = text.trim().length;
    if (text < 11 && text > 0) {
        //alert('Completar el campo');
        $('#id-md-ruc').focus();
    }
});

$(document).on('blur', '#id-md-celular', function (e) {
    var text = $(this).val();
    text = text.replace("_", "");

    text = text.trim().length;
    if (text < 11 && text > 0) {
        //alert('Completar el campo');
        $('#id-md-celular').focus();
    }
});

$(document).on('blur', '#id-md-telefono', function (e) {
    var text = $(this).val();
    text = text.replace("_", "");

    text = text.trim().length;
    if (text < 14 && text > 0) {
        //alert('Completar el campo');
        $('#id-md-telefono').focus();
    }
});

$(document).on('click', '#id-btn-resumen', function (e) {
    $('#id-md-resu-presu').modal({ backdrop: 'static' });
})

$(document).on('change', '.cls-tipotramite', function (e) {
    var _vid = $(this).data('id');
    var _a = this.value;
    _a = (_a == null ? "" : _a);

    fn_ope_p6_lucky_tipotramite(5, _a, _vid);
});

function fn_ope_p6_lucky_tipotramite(_va, _vb, _vc) {

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_Lucky_TipoTramite',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc
        },
        success: function (a) {
            alert("Registro actualizado con exito!");
        },
        error: function (xhr) {
            console.log('[Advertencia]<p3>: Problemas con el servicio.');
        }
    });
}

$(document).on('change', '.cls-tiempopermiso', function (e) {
    var _vid = $(this).data('id');
    var _a = this.value;
    _a = (_a == null ? "" : _a);

    fn_ope_p6_lucky_tiempopermiso(6, _a, _vid);
});

function fn_ope_p6_lucky_tiempopermiso(_va, _vb, _vc) {

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_Lucky_TipoPermiso',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc
        },
        success: function (a) {
            alert("Registro actualizado con exito!");
        },
        error: function (xhr) {
            console.log('[Advertencia]<p3>: Problemas con el servicio.');
        }
    });
}

$(document).on('change', '#id-md-solicitud input[type="file"]', function (e) {
    var sms = $(this).val();
    var vid = $('#id-md-solicitud').data('codigo');
    var vimg = $(this).data('img');
    var vhdd = $(this).data('hdd');
    var vorden = $(this).data('orden');

    var __id = $(this).data('id');
    $('#id-md-solicitud').data('codigo', __id);

    if (sms) {
        //$(this).parent().children('span').html('<button class="cls-eliminar-file btn btn-success btn-xs" data-orden="' + vorden + '"  data-img="' + vimg + '" data-hdd="' + vhdd + '" >Eliminar</button>');

        $('#id-' + vimg).upload(
               'Carga_Img_FotoSolicitud',
               { __a: vid, __b: vorden },
               function (a) {
                   var _nomfoto = String(a);
                   $('#id-md-solicitud .objrequeridsolicitud td .' + vimg).attr('src', Url.Xplora + _nomfoto);
                   $('#' + vhdd).val(_nomfoto);
                   //$("#foto" + vorden).css("visibility", "visible");
                   //$('.cls-img-' + vorden).css("display", "block");
                   //$('.cargar-foto' + vorden).css("display", "none");

                   //$('.cls-img-' + vorden).css("margin-left", "5px");
                   //$('.cls-img-' + vorden).css("margin-right", "5px");
               },
               'json'
        );
    } else {
        $(this).parent().children('span').html('');
    }
});
/*********************************************************************************/
function fnOnChangeFile() {
    if ($('div[data-solicitud-archivo]').length == 5) return;

    var oFile = ($('#FileImagenSolicitud'))[0].files[0];
    var form = $('#FormCargaImagenSolicitud')[0];
    var dataString = new FormData(form);

    $.ajax({
        url: Url.CargaFoto,
        beforeSend: function () {
            $('#lucky-loading').modal('show');
        },
        type: 'post',
        success: function (__s) {
            if (__s.estado == true) {
                $('#DivGaleriaSolicitud').append('<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" id="' + __s.id + '" data-solicitud-archivo="' + __s.archivo + '"><a href="javascript:"><img src="' + Url.Fachada + 'Img/' + __s.archivo + '" class="img-thumbnail" alt="' + __s.archivo + '" /></a><a href="javascript:" onclick="fnOnRemoveImage(\'' + __s.id + '\',\'' + __s.archivo + '\');" style="position: absolute; right: 10px; top: -5px;"><i class="fa fa-times" style="background-color: #B40404; padding: 5px; border-radius: 13px; color: white; font-size:10px;"></i></a></div>');
            } else {
                alert('Problemas al cargar la imagen.');
            }
        },
        dataType: 'json',
        error: function (__e) {
            console.error(__e);
            $('#lucky-loading').modal('hide');
        },
        complete: function () {
            $('#lucky-loading').modal('hide');
        },
        data: dataString,
        cache: false,
        contentType: false,
        processData: false
    });
}
function fnOnRemoveImage(id, archivo) {
    var RemoveDisabled = $('#' + id + ' a').attr('disabled');

    if (typeof RemoveDisabled !== typeof undefined && RemoveDisabled !== false) return;

    $.ajax({
        type: 'post',
        dataType: 'json',
        url: Url.EliminaFoto,
        data: { ArchivoNombre: archivo },
        beforeSend: function () {
            $('#lucky-loading').modal('show');
        },
        success: function (respuesta) {
            if (respuesta.Resultado == true) {
                $('#' + id).remove();
            }
        },
        complete: function () {
            $('#lucky-loading').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-loading').modal('hide');
        }
    });
}

$(document).on('click', '#btn-md-desactivar', function (e) {
    $('#id_num_solicitud_desactive').val($('#id-hd-codigo').val());
    $('#id-md-sm-desactive').modal();
});

$(document).on('click', '#id-btn-aceptar-desactive', function (e) {
    //alert($('#id_num_solicitud').val());

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Eliminar_SolicitudVentas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: $('#id_num_solicitud_desactive').val(),
            __b: 2
        },
        success: function (a) {
            $('#id-md-sm-desactive').modal('hide');
            $('#id-md-solicitud').modal('hide');
            fn_consutar_grilla();
        },
        error: function (xhr) {
            console.log('[Advertencia]<DesactiveSolicitud>: Problemas con el servicio.');
        }
    });
});

$(document).on('click', '#btn-md-activar', function (e) {
    $('#id_num_solicitud_active').val($('#id-hd-codigo').val());
    $('#id-md-sm-active').modal();
});
$(document).on('click', '#id-btn-aceptar-active', function (e) {
    //alert($('#id_num_solicitud').val());

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Eliminar_SolicitudVentas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: $('#id_num_solicitud_active').val(),
            __b: 1
        },
        success: function (a) {
            $('#id-md-sm-active').modal('hide');
            $('#id-md-solicitud').modal('hide');
            fn_consutar_grilla();
        },
        error: function (xhr) {
            console.log('[Advertencia]<ActiveSolicitud>: Problemas con el servicio.');
        }
    });
});

//historico renovacion
$(document).on('click', '.cls-hitorico-renovacion', function (e) {
    //$('#md-historico-renovacion .modal-content .modal-footer').remove();
    var _vid = $(this).data('id');
    var _a = ""; var _b = ""; var _c = ""; var _d = ""; var _e = ""; var _f = ""; var _g = ""; var _h = ""; var _i = "";
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'GetGrillaTramite',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _vid,
            __b: _a,
            __c: _b,
            __d: _c,
            __e: _d,
            __f: _e,
            __g: _f,
            __h: 8,
            __i: _g,
            __j: _h,
            __k: _i
        },
        success: function (response) {
            //console.log(response);
            if (response.Archivo != null && response.Archivo != "" && response.Archivo.length.toString() != "0") {
                $('#tb-wf-unacem-renovacion tbody').empty();

                var ruta_recibo = '';
                var ruta_licencia = '';
                var ruta_Presupuesto_Final = '';
                $.each(response.Archivo, function (key, value) {

                    if (value.Url_rec_tramite != '' && value.Url_rec_tramite != null) {
                        ruta_recibo = '<img src="/Img/ipdf.png" class="cls-img-td" onclick="mostrarPDF(\'' + value.Url_rec_tramite + '\');" />'
                    } else {
                        ruta_recibo = '';
                    }
                    if (value.Url_Licencia != '' && value.Url_Licencia != null) {
                        ruta_licencia = '<img src="/Img/ipdf.png" class="cls-img-td" onclick="mostrarPDF(\'' + value.Url_Licencia + '\');" />'
                    } else {
                        ruta_licencia = '';
                    }
                    if (value.Url_Presupuesto_Final != '' && value.Url_Presupuesto_Final != null) {
                        ruta_Presupuesto_Final = '<a href="' + value.Url_Presupuesto_Final + '"><img src="/Img/logoexcel.png" class="cls-img-td" /></a>'
                    } else {
                        ruta_Presupuesto_Final = '';
                    }

                    $("#tb-wf-unacem-renovacion tbody").append('<tr><td>' + value.Fec_Ingreso_expe +
                        '</td><td>' + ruta_recibo +
                        '</td><td>' + value.Obs_Tramite +
                        '</td><td>' + value.Fec_Entre_Licencia +
                        '</td><td>' + value.fec_cadu_licencia +
                        '</td><td>' + ruta_licencia +
                        '</td><td>' + value.Monto_Presupuesto_Final +
                        '</td><td>' + value.Codigo_Presupuesto_Final +
                        '</td><td>' + ruta_Presupuesto_Final +
                        '</td></tr>');
                });
            }
            else {
                $('#tb-wf-unacem-renovacion tbody').empty();
            }

        }
    });

    $('#md-historico-renovacion').data('id', _vid);
    $('#md-historico-renovacion').modal({ backdrop: 'static' });
});

$(document).on('click', '.cls-sw-preg-solicitud-fv i', function (e) {
    if ($(this).hasClass('cls-active') != true) {

        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        var vuser = $(this).data('user');
        var vop = $(this).data('op');
        var vcomentario = $(this).data('comentario');
        var vclase = $(this).data('clase');
        if (vvalor == 'si') {
            $(this).parent().children('i').removeClass('cls-active');
            $(this).addClass('cls-active');
            //Aprobacion de solicitud
            fn_p2_aprosolicitud(vid, 4, 2, '');

        } else if (vvalor == 'no') {
            if (vcomentario != '') {
                $(this).parent().children('i').removeClass('cls-active');
                $(this).addClass('cls-active');
                fn_p2_aprosolicitud(vid, 3, 2, '');
            } else {
                fn_modal_comentario(vid, vop, vuser, vcomentario, 1, vclase);
            }

            //fn_modal_comentario(vid, vop, vuser,''); // solicitud
        }
    }
});

$(document).on('click', '#id-btn-apsolicitud-fv-grabar', function (e) {
    //if (String(($("#id-ta-comentario").val()).trim()).length > 0) {
    //    var _vid = $('#id-md-apsolicitud-comen input[type="hidden"]').val();
    //    fn_p2_aprosolicitud(_vid,2);
    //    $('#id-md-apsolicitud-comen').modal('hide');
    //} else {
    //    $('#id-md-apsolicitud-comen #id-ta-comentario').val('').focus();
    //    //$('#id-md-apsolicitud-comen .cls-obligatorio').html('Para desaprobar la solicitud ingresar un comentario.');
    //    $('#id-md-apsolicitud-comen .cls-obligatorio').html('Para grabar primero ingresar un comentario.');
    //}

    var _vtipo = $("#id-tipo").val();
    var _vclase = $("#id-clase").val();
    var _vid = $('#id-md-apsolicitud-comen #id-numreg').val();
    var _texto = $("#id-ta-comentario").val();
    var _op = $("#id-op").val();

    if (_op == 1) {
        $(".cls-sw-preg-solicitud i").data('comentario', _texto);
    } else if (_op == 2) {
        $(".cls-sw-preg-fotomont i").data('comentario', _texto);
    } else if (_op == 3) {
        $(".cls-sw-preg-solicitud-fv i").data('comentario', _texto);
    }

    fn_p2_aprosolicitud(_vid, 5, _vtipo, _vclase);
    $('#id-md-apsolicitud-comen').modal('hide');

    //cls-obligatorio
});

$(document).on('change', '#cbo_anio_wf', function () {
    fnmes();
});

function fnmes() {
    var vanio = $('#cbo_anio_wf').val();
    if (vanio == 2016) {
        $('#segmento').hide();
        $('#canal').hide();
        $('#marca').show();
    }
}
