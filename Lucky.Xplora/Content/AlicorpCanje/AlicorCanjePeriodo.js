﻿var contenedor = [];
var fecha_inicial = '';
var fecha_cierre = '';
var valor_actual = 0;
var filtro = 1;
var status_save = 0;
var valor_combo = 0;
var valor_anterior = 0;

$(document).ready(function () {
    fnOnCheck();
    fnGenerarAnio();
});

function fnOnDetallePeriodo(valor_rpl_id) {
    var __parameter = [];
    var __cont = 1;
    var arrayBody = [],
        arrayHead = [],
        arrayHead2 = [];
    var tabla = $('#table-det-caramelos'),
        tablaBody = $('#table-det-caramelos tbody'),
        tablaHead = $('#table-det-caramelos thead');

    var tabla2 = $('#table-det-galletas'),
        tablaBody2 = $('#table-det-galletas tbody'),
        tablaHead2 = $('#table-det-galletas thead');

    if (filtro == 1) {
        valor_combo = $('#combo-anio').val();
    } else {
        valor_combo = 0;
    }

    __parameter.push(valor_combo);
    __parameter.push($('#input-bus-concurso').val());
    __parameter.push(valor_rpl_id);

    $.ajax({
        beforeSend: function () {
            $('.lucky-load').show();
            $('#md-det-periodo').modal();
        },
        url: 'WorkFlowPeriodoListar',
        type: 'post',
        dataType: 'json',
        data: { __a: 1, __b: __parameter.join(',') },
        success: function (__s) {
            contenedor = __s;
            console.log(contenedor);
            tablaBody.empty();
            tablaBody2.empty();

            $.each(contenedor, function (i, v) {

                $('#input-det-concurso').val(v._b);/******ID-Concurso*************/
                $('#input-det-descripcion').val(v._c);/*******Descripcion del Concurso***************/
                $('#input-det-fecha_ini').val(v._g);/************Fecha Inicio*********************/
                $('#input-det-fecha_cierre').val(v._h);/*********************Fecha de Cierre******************/
                $('#input-det-pre-dest').val(v._f);/***************Presupuesto destinado****************/
                $('#input-det-observacion').val(v._i);/***************Observacion****************/
                //    __cont = 1;
                //    arrayBody.push('<td ><img role="button" onclick="fnOnDetallePeriodo(' + v._b + ');" src="/Img/Alicorp-Canje/detalle-2.PNG" /></td>');//Vista
                //    arrayBody.push('<td nowrap>' + v._a + '</td>');//Item
                //    arrayBody.push('<td nowrap>' + v._b + '</td>');//Codigo de Concurso
                //    arrayBody.push('<td nowrap>' + v._c + '</td>');//Descripcion de Concurso

                //    for (x = 1; x <= 12; x++) {
                //        if (__cont >= v._d && __cont <= v._e) {
                //            arrayBody.push('<td nowrap style="background-color:#FCE95D"></td>');//Item
                //        } else {
                //            arrayBody.push('<td></td>');//Item
                //        }
                //        __cont = __cont + 1;
                //    }

                //    /**************Falta Generar Mayoristas y Codistribuidores*****************/

                //    tablaBody.append('<tr>' + arrayBody.join('') + '</tr>');
                //    arrayBody = [];



            });

            tablaHead.append('<tr>' + arrayHead.join('') + '</tr>');
            tablaHead.append('<tr>' + arrayHead2.join('') + '</tr>');
            arrayHead = [];
            arrayHead2 = [];

        },
        complete: function () {
            $('.lucky-load').hide();
        },
        error: function (__e) {
            console.error(__e);
            $('.lucky-load').hide();
        }
    });


};

function fnOnCheck() {

    $('#input-checkbox').click(function () {
        if ($(this).is(':checked')) {
            $('#input-inicio').attr('disabled', 'disabled');
            $('#input-cierra').attr('disabled', 'disabled');
            fecha_inicial = '@DateTime.Today.ToShortDateString()'
            fecha_cierre = '@DateTime.Today.Date.AddMonths(1).ToShortDateString()'
            valor_actual = 1;
        } else {
            $('#input-inicio').removeAttr('disabled');
            $('#input-cierra').removeAttr('disabled');
            fecha_inicial = $('#input-inicio').val();
            fecha_cierre = $('#input-cierra').val();
            valor_actual = 0;
        }
    });

};

function fnOnSave() {

    /****************Datos ya han sido guardados***************************/
    if (status_save == 1) {
        swal("Cancelado", "Limpiar y crear el nuevo periodo", "error");
        return;
    }

    /*************Verifica si los campos estan completos****************/
    if (valor_actual == 1) {
        if (($('#input-concurso').val() == "") || ($('#input-observacion').val() == "") || ($('#input-presupuesto').val() == "")) {
            swal("Cancelado", "Completar campos !!!", "error");
            return;
        }
    } else {
        if (($('#input-concurso').val() == "") || ($('#input-inicio').val() == "") || ($('#input-cierra').val() == "") || ($('#input-observacion').val() == "") || ($('#input-presupuesto').val() == "")) {
            swal("Cancelado", "Completar campos !!!", "error");
            return;
        } else {

            fecha_inicial = $('#input-inicio').val();
            fecha_cierre = $('#input-cierra').val();

        }
    }

    var __parameter = [];
    __parameter.push($('#input-concurso').val());
    __parameter.push(fecha_inicial);
    __parameter.push(fecha_cierre);
    __parameter.push($('#input-observacion').val());
    __parameter.push($('#input-presupuesto').val());
    __parameter.push('@ViewBag.name_user');
    __parameter.push('@DateTime.Today.ToShortDateString()');


    $.ajax({
        beforeSend: function () {
            $('.lucky-load').show();
        },
        url: 'WorkFlowPeriodoInsertar',
        type: 'post',
        /*dataType: 'json',*/
        data: { __a: 0, __b: __parameter.join(',') },
        success: function (__s) {
            contenedor = __s;
            console.log(contenedor);

            contenedor = contenedor.replace('"', '');
            var texto = contenedor;
            console.log(texto);
            var data = texto.split(",");

            console.log(data);
            $('#input-idconcurso').val(data[0].replace('"', ''));
            swal("Registrado!", "ID Concurso es : " + data[0], "success");

            var cadena = '';
            /*alert(fecha_inicial);*/
            cadena = '<div id="contenido-resumen" >' +
                    '<div class="form-group" style="padding:5px !important" >' +
                       'El Nombre del Concurso es : ' + $('#input-concurso').val() + '</br>' +
                      ' El Concurso empieza : ' + fecha_inicial + ' hasta el dia : ' + fecha_cierre + '' +
                      '</div>' +
                      '</div';

            $('#panel-resumen').append(cadena);

            var cadena2 = '';
            cadena2 = '<div id="contenido-status">' +
                        '<div class="form-group" style="padding:5px !important" >' +
                            '<label class="control-label">Meses : ' + data[1] + '</label>' + '</br>' +
                            '<label class="control-label">Días Calendario : ' + data[2] + '</label>' + '</br>' +
                            '<label class="control-label">Días Utiles : ' + data[3].replace('"', '') + '</label>' +
                        '</div>' +
                    '</div>';

            $('#panel-status').append(cadena2);

            status_save = 1;

        },
        complete: function () {
            $('.lucky-load').hide();
            fnOnListarPeriodo();
        },
        error: function (__e) {
            console.error(__e);
            $('.lucky-load').hide();
        }
    });
};


function fnOnClean() {
    $('#input-idconcurso').val("");
    $('#input-concurso').val("");
    $('#input-inicio').removeAttr('disabled');
    $('#input-cierra').removeAttr('disabled');
    $('#input-checkbox').removeAttr('checked');
    $('#input-inicio').val("");
    $('#input-cierra').val("");
    $('#input-observacion').val("");
    $('#input-presupuesto').val("");
    $("div").remove('#contenido-resumen');
    $("div").remove('#contenido-status');
    var fecha_inicial = "";
    var fecha_cierre = "";
    status_save = 0;
}

function fnOnFiltroAnio() {
    if (filtro == 1) {
        $("#img-filtro").removeAttr("src");
        $("#img-filtro").attr("src", "/Img/Alicorp-Canje/filter-2.png");
        $("#panel-anio").css({ "display": "none" });
        valor_anterior = valor_combo;
        valor_combo = 0;
        filtro = 0;
        //$('#combo-anio').attr('disabled', 'disabled');
        fnOnListarPeriodo();
    } else {
        $("#img-filtro").removeAttr("src");
        $("#img-filtro").attr("src", "/Img/Alicorp-Canje/filtro.png");
        $("#panel-anio").css({ "display": "block" });
        valor_combo = valor_anterior;
        ////$("#combo-anio").removeAttr("disabled");
        filtro = 1;
        fnOnListarPeriodo();
    }

}

function fnGenerarAnio() {
    var cadena = '';
    var anio = '@DateTime.Today.Year'
    cadena = cadena + '<div class="row">';
    anio = anio - 2;
    for (x = 1; x <= 3; x++) {

        if (x == 3) {
            cadena = cadena + '<div id="col-filtro-' + anio + '" class="col-md-4 div-anio-inactivo div-anio-activo" role="button" >' +
                                '<div  class="text-center">' +
                                    '<a id="ref-anio" href="javascript:fnOnSeleccionar(' + anio + ');" style="text-decoration:none;">' + anio + '</a>' +
                                '</div>' +
                                '<div id="col-barra-' + anio + '" class="div-barra-inactivo div-barra-activo"></div>' +
                              '</div>'
            valor_combo = anio;
            fnOnListarPeriodo();
        } else {
            cadena = cadena + '<div id="col-filtro-' + anio + '" class="col-md-4 div-anio-inactivo" role="button" >' +
                                '<div class="text-center">' +
                                    '<a id="ref-anio" href="javascript:fnOnSeleccionar(' + anio + ');" style="text-decoration:none;">' + anio + '</a>' +
                                '</div>' +
                                '<div id="col-barra-' + anio + '" class="div-barra-inactivo"></div>' +
                            '</div>'
        }
        anio = anio + 1;
    }

    cadena = cadena + '</div>';

    $('#panel-anio').append(cadena);

}

function fnOnSeleccionar(anio) {
    $("div").removeClass('div-anio-activo');
    $("div").removeClass('div-barra-activo');
    $('#col-filtro-' + anio + '').addClass('div-anio-activo');
    $('#col-barra-' + anio + '').addClass('div-barra-activo');
    valor_combo = anio;

    fnOnListarPeriodo();

}

function fnOnListarPeriodo() {
    var cadena = '';
    var __parameter = [];
    var __cont = 1;
    var group_anio = 0;
    var cont_col = 0;

    var arrayBody = [],
        arrayHead = [],
        arrayHead2 = [];

    var tabla = $('#table-concurso'),
        tablaBody = $('#table-concurso tbody'),
        tablaHead = $('#table-concurso thead');

    cadena = cadena + '<div id="div-input-anio">' + valor_combo + '</div>';
    $("div").remove('#div-input-anio');
    $('#div-panel-anio').append(cadena);

    __parameter.push(valor_combo);
    __parameter.push($('#input-bus-concurso').val());
    __parameter.push('');

    $.ajax({
        beforeSend: function () {
            $('.lucky-load').show();
        },
        url: 'WorkFlowPeriodoListar',
        type: 'post',
        dataType: 'json',
        data: { __a: 1, __b: __parameter.join(',') },
        success: function (__s) {
            contenedor = __s;
            console.log(contenedor);
            tablaBody.empty();

            $.each(contenedor, function (i, v) {

                var array_fecha = v._g.split("-")
                var anio = array_fecha[0];

                if (anio != group_anio && filtro != 1) {
                    arrayBody.push('<td class="td-padding"></td>')
                    tablaBody.append('<tr>' + arrayBody.join('') + '</tr>');
                    arrayBody = [];
                    arrayBody.push('<td class="text-center row-anio-' + (cont_col + 1) + ' " colspan="28">' + anio + '</td>');
                    group_anio = anio;
                    tablaBody.append('<tr >' + arrayBody.join('') + '</tr>');
                    arrayBody = [];
                } else {

                }

                var str1 = "" + v._b;
                var str2 = "'";
                var res = str2.concat(str1.concat(str2));
                __cont = 1;
                arrayBody.push('<td ><img role="button" onclick="fnOnDetallePeriodo(' + res + ');" src="/Img/Alicorp-Canje/detalle-2.PNG" /></td>');//Vista
                arrayBody.push('<td >' + v._a + '</td>');//Item
                arrayBody.push('<td >' + v._b + '</td>');//Codigo de Concurso
                arrayBody.push('<td >' + v._c + '</td>');//Descripcion de Concurso

                for (x = 1; x <= 12; x++) {/*Para recorrer los 12 meses*/
                    if (__cont >= v._d && __cont <= v._e) {
                        arrayBody.push('<td class="td-style"></td>');//Mes
                    } else {
                        arrayBody.push('<td></td>');//Mes
                    }
                    __cont = __cont + 1;
                }

                /**************Falta Generar Mayoristas y Codistribuidores*****************/

                tablaBody.append('<tr>' + arrayBody.join('') + '</tr>');
                arrayBody = [];



            });

            $('#table-concurso').bootstrapTable({

                //fixedColumns: true,
                //fixedNumber: 1

            });

        },
        complete: function () {
            $('.lucky-load').hide();

        },
        error: function (__e) {
            console.error(__e);
            $('.lucky-load').hide();
        }
    });
}

function fnOnClickDownloadPeriodo() {
    var parametro = [];

    parametro.push(valor_combo);
    parametro.push($('#input-bus-concurso').val());
    parametro.push('');

    $.ajax({
        type: 'post',
        beforeSend: function (a) {
            $('.lucky-load').modal('show');
        },
        url: 'WorkFlowPeriodoExportar',
        dataType: 'json',
        //contentType: 'application/json',
        data: { __a: 1, __b: parametro.join(',') },
        success: function (s) {
            /*console.log(s);*/
            window.open('/Temp/' + s.archivo, '_blank');
        },
        complete: function () {
            $('.lucky-load').modal('hide');
        },
        error: function (a) {
            $('.lucky-load').modal('hide');
        }
    });
}

function fnOnEdit() {
    /*$('#input-inicio').attr('disabled', 'disabled');*/
    $('#input-det-descripcion').removeAttr('disabled');
    $('#input-det-fecha_ini').removeAttr('disabled');
    $('#input-det-fecha_cierre').removeAttr('disabled');

    $('#input-det-pre-dest').removeAttr('disabled');
    //$('#input-det-monto-asig').removeAttr('disabled');
    //$('#input-det-saldo-disp').removeAttr('disabled');
    $('#input-det-observacion').removeAttr('disabled');

}


function fnOnModificar() {

    var __parameter = [];
    __parameter.push($('#input-det-concurso').val());
    __parameter.push($('#input-det-descripcion').val());
    __parameter.push($('#input-det-fecha_ini').val());
    __parameter.push($('#input-det-fecha_cierre').val());
    __parameter.push($('#input-det-observacion').val());
    __parameter.push($('#input-det-pre-dest').val());
    __parameter.push('@ViewBag.name_user');
    __parameter.push('@DateTime.Today.ToShortDateString()');

    $.ajax({
        beforeSend: function () {
            $('.lucky-load').show();
        },
        url: 'WorkFlowPeriodoInsertar',
        type: 'post',
        dataType: 'json',
        data: { __a: 2, __b: __parameter.join(',') },
        success: function (__s) {
            contenedor = __s;
            console.log(contenedor);
            $('#md-det-periodo').modal('hide');
            $('#input-det-descripcion').attr('disabled', 'disabled');
            $('#input-det-fecha_ini').attr('disabled', 'disabled');
            $('#input-det-fecha_cierre').attr('disabled', 'disabled');
            $('#input-det-pre-dest').attr('disabled', 'disabled');
            $('#input-det-observacion').attr('disabled', 'disabled');
            fnOnListarPeriodo();
            if (contenedor == "1") {
                swal("Actualizado!", "Se realizo correctamente.", "success")
            } else {
                swal("Cancelled", "Error durante la actulizacion campos !!!", "error");
            }
        },
        complete: function () {
            $('.lucky-load').hide();
        },
        error: function (__e) {
            console.error(__e);
            $('.lucky-load').hide();
        }
    });


}