﻿
/**********Formato de Moneda******************/
//formato_value(NaN); // "0.00"
//formato_value(0); // "0.00"
//formato_value(123456567.89); // "123'456.567,89"
//formato_value(-123456567.89); // "-123'456.567,89"
//formato_value(1234.56, 1); // "1.234,5"
//formato_value(1234.56, 1, [',', "'", '.']); // "1,234.5"

function formato_value(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || ['.', "'", ','];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}