﻿var ArrayFotomontaje = [];

$(function () {
    $('.nav.navbar-nav.navbar-right').prepend('<li class="dropdown toolbar-icon-bg demo-search-hidden mr5"><a href="#" data-toggle="dropdown" data-original-title="" title="Descargar"><span class="icon-bg" onclick="fndesunacem();"><i class="fa fa-database" ></i></span></a></li>');
    $('#HeadingBtnGroup').html('\
            <a class="btn" style="color: #000; cursor: auto; box-shadow: inset 0 0px 0px rgba(0,0,0,.125);">PERFIL: ' + Persona.GrupoNombre + '</a>\
            <a role="button" href="javascript:" class="btn btn-success" id="id-btn-nuevo"><i class="fa fa-plus-circle"></i>&nbsp;Nuevo</a>\
            <a role="button" href="javascript:" class="btn btn-info" onclick="fnOnClickRefresca();"><i class="fa fa-refresh"></i></a>\
            <a role="button" href="javascript:" class="btn btn-default" onclick="" id="id-btn-resumen">Presupuesto</a>\
            <a role="button" href="javascript:fnOnClickNavbar(\'navbar-vertical-filtro\',null)" class="btn btn-default"><i class="fa fa-search"></i></a>\
        ');

    if (Persona.Grupo != '1') {
        $('#id-btn-nuevo').hide();
    }
    if (Persona.Grupo != '3') {
        $('#id-md-fotomontaje .modal-footer .pull-left').remove();
    }

    if (Persona.Grupo == '1') {
        $('#HeadingH5Subtitulo').html('GESTION DE ALTAS NUEVAS');
        fnArrayPerfilVenta();
    } else if (Persona.Grupo == '2') {
        $('#HeadingH5Subtitulo').html('GESTION DE APROBACION DE ALTAS NUEVAS');
        fnArrayPerfilTrade();
    } else if (Persona.Grupo == '3') {
        $('#HeadingH5Subtitulo').html('GESTION DE ALTAS NUEVAS');
        fnArrayPerfilAnalis();
    } else if (Persona.Grupo == '4') {
        $('#HeadingH5Subtitulo').html('GESTION DE TRAMITES');
        fnArrayPerfilAnalisTramite();
    }

    $("#id-md-nom-pdv").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: 'Autocomplete',
                dataType: "json",
                type: 'POST',
                data: {
                    term : request.term + ',' + Campania.Idplanning,
                    opcion : 1,
                    tipo : 1
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength:3,
        //focus: function( event, ui ) {
        //    $( "#id-md-nom-pdv" ).val( ui.item.nom_pdv );
        //    return false;
        //},
        select: function (event, ui) {
            //event.preventDefault();
            $('#id-md-ruc').val(ui.item.num_RUC);
            $('#id-md-marca').val(ui.item.marca);
            $('#id-md-codigo').val(ui.item.cod_pdv);
            $('#id-md-celular').val(ui.item.num_celular);
            //$('#id-md-ruc').val(ui.item.id_tiposolicitud);
            $('#id-md-departamento').val(ui.item.departamento);
            $('#id-md-contacto').val(ui.item.contacto);
            $('#id-md-provincia').val(ui.item.provincia);
            $('#id-md-telefono').val(ui.item.telefono);
            $('#id-md-distrito').val(ui.item.distrito);
            $('#id-md-direccion').val(ui.item.direccion);
            $('#id-md-distribuidora').val(ui.item.distribuidora);
            $('#id-md-comentario').val(ui.item.comentarios_solicitud);
            //$('#id-md-ruc').val(ui.item.id_zona);
            $('#id-hd-codigo').val(ui.item.num_solicitud);

            $('#id-md-nom-pdv').val(ui.item.nom_pdv);

            var contador = $('input[name="gchktiposolic"]').length;

            var str = String(ui.item.id_tiposolicitud);
            var valores = "";
            posicionaux = 0;
            posicion = str.indexOf(",");
            if (posicion != -1) {
                do {
                    if (posicionaux == 0) {
                        if (valores == "") {
                            valores = parseInt(str.substring(0, posicion)) + ",";
                        }
                    } else {
                        valores += parseInt(str.substring(posicionaux + 1, posicion)) + ",";
                    }
                    posicionaux = posicion;
                    posicion = str.indexOf(",", posicion + 1);
                    if (posicion == -1) {
                        valores += parseInt(str.substring(posicionaux + 1, str.length)) + ",";
                    }
                } while (posicion != -1);
                var valores2 = valores.substring(0, valores.length - 1);
                var test = "[" + valores2 + "]";
            } else {
                var test = [parseInt(str)];
            }

            for (var i = 1; i <= contador; i++) {
                if (jQuery.inArray(i, test) != -1 || jQuery.inArray(i.toString(), test) != -1) {
                    $('input[name="gchktiposolic"]')[i - 1].checked = true;
                } else {
                    $('input[name="gchktiposolic"]')[i - 1].checked = false;
                }
            }
            return false;
                    
        }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append( "<div>" + item.nom_pdv + "<br>" + item.direccion + "</div>" )
            .appendTo( ul );
    };

    $("#id-md-ruc").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: 'Autocomplete',
                dataType: "json",
                type: 'POST',
                data: {
                    term: request.term + ',' + Campania.Idplanning,
                    opcion: 1,
                    tipo: 2
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        //focus: function (event, ui) {
        //    $("#id-md-ruc").val(ui.item.num_RUC);
        //    return false;
        //},
        select: function (event, ui) {
            //event.preventDefault();
            $('#id-md-nom-pdv').val(ui.item.nom_pdv);
            $('#id-md-ruc').val(ui.item.num_RUC);
            $('#id-md-marca').val(ui.item.marca);
            $('#id-md-codigo').val(ui.item.cod_pdv);
            $('#id-md-celular').val(ui.item.num_celular);
            //$('#id-md-ruc').val(ui.item.id_tiposolicitud);
            $('#id-md-departamento').val(ui.item.departamento);
            $('#id-md-contacto').val(ui.item.contacto);
            $('#id-md-provincia').val(ui.item.provincia);
            $('#id-md-telefono').val(ui.item.telefono);
            $('#id-md-distrito').val(ui.item.distrito);
            $('#id-md-direccion').val(ui.item.direccion);
            $('#id-md-distribuidora').val(ui.item.distribuidora);
            $('#id-md-comentario').val(ui.item.comentarios_solicitud);
            //$('#id-md-ruc').val(ui.item.id_zona);
            $('#id-hd-codigo').val(ui.item.num_solicitud);


            var contador = $('input[name="gchktiposolic"]').length;

            var str = String(ui.item.id_tiposolicitud);
            var valores = "";
            posicionaux = 0;
            posicion = str.indexOf(",");
            if (posicion != -1) {
                do {
                    if (posicionaux == 0) {
                        if (valores == "") {
                            valores = parseInt(str.substring(0, posicion)) + ",";
                        }
                    } else {
                        valores += parseInt(str.substring(posicionaux + 1, posicion)) + ",";
                    }
                    posicionaux = posicion;
                    posicion = str.indexOf(",", posicion + 1);
                    if (posicion == -1) {
                        valores += parseInt(str.substring(posicionaux + 1, str.length)) + ",";
                    }
                } while (posicion != -1);
                var valores2 = valores.substring(0, valores.length - 1);
                var test = "[" + valores2 + "]";
            } else {
                var test = [parseInt(str)];
            }

            for (var i = 1; i <= contador; i++) {
                if (jQuery.inArray(i, test) != -1 || jQuery.inArray(i.toString(), test) != -1) {
                    $('input[name="gchktiposolic"]')[i - 1].checked = true;
                } else {
                    $('input[name="gchktiposolic"]')[i - 1].checked = false;
                }
            }
            return false;
        }
    })
    .autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
            .append("<div>" + item.num_RUC + "<br>" + item.direccion + "</div>")
            .appendTo(ul);
    };
})

$(document).on('click', '#id-btn-resumen', function (e) {
    $('#id-md-resu-presu').modal('show');
})

/* Inicio boton nueva solcitud */
$(document).on('click', '#id-btn-nuevo', function (e) {
    fn_md_nueva_solicitud();
});
function fn_md_nueva_solicitud() {
    $("#btn-md-grabar").css("visibility", "visible");
    $("#btn-md-cancelar").css("visibility", "visible");
    $("#btn-md-enviar").css("visibility", "visible");
    $("#id-md-solicitud .panel-footer .pull-left button").prop('disabled', false);

    $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-marca').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-celular').removeClass('has-error2');
    $('.objrequeridsolicitud #div_tipos').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-departamento').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-contacto').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-provincia').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-distrito').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-direccion').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-distribuidora').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-zona').removeClass('has-error2');

    $('div[data-solicitud-archivo]').each(function () {
        $(this).remove();
    });

    $('#id-hd-codigo').val('0');
    //$('#id-md-solicitud').modal({ backdrop: 'static' });
    $("#id-md-solicitud .modal-footer .pull-left button").prop('disabled', false);
    $("#id-md-solicitud input").prop('disabled', false);

    $('#btn-md-editar').hide();
    $("#id-md-solicitud input").val('');
    $('.mask').inputmask();

    $('#id-img-solicitud-1').attr('src', Url.Xplora + 'Fachada/Img/foto_nodisponible.jpg');

    $('#id-md-nom-pdv').focus();

    $('#btn-group-general-solicitud button')
            .removeClass()
            .addClass('btn btn-default');
    $('#id-btn-implementacion')
        .removeClass()
        .addClass('btn btn-primary');
    $('#btn-group-general-solicitud button')
        .removeAttr('disabled');
    $('#auxbuton').val('1');

    $("#id-md-solicitud select").prop('disabled', false);
    $("#_solicitud").prop('disabled', false);
    $('input:checkbox').removeAttr('checked');
    $('#id-md-comentario').val('');
    document.getElementById('id-md-zona').selectedIndex = 0;
    document.getElementById('id-md-marca').selectedIndex = 0;
    $('#btn-md-desactivar').css("display", "none");
    $('#btn-md-activar').css("display", "none");

    $('#id-md-solicitud').modal({ backdrop: 'static' });
}
$(document).on('click', '#btn-md-grabar', function (e) {
    var vpdv = $('#id-md-nom-pdv').val()
            , vruc = $('#id-md-ruc').val()
            , vcodigo = $('#id-md-codigo').val()
            , vdireccion = $('#id-md-direccion').val()
            , vdistrito = $('#id-md-distrito').val()
            , vprovincia = $('#id-md-provincia').val()
            , vdepartamento = $('#id-md-departamento').val()
            , vcontacto = $('#id-md-contacto').val()
            , vnumtelef = $('#id-md-telefono').val()
            , vnumcel = $('#id-md-celular').val()
            , vdistribuidora = $('#id-md-distribuidora').val()
            , p1_codZona = $('#id-md-zona').val()
            , vmarca = $('#id-md-marca').val()
            , v_rutafoto1 = $('#ruta-foto-1').val()
            , v_tipo_gral_solicitud = $('#auxbuton').val();

    var _vtiposolic;
    $('input[name="gchktiposolic"]:checked').each(function () {
        _vtiposolic += $(this).data('cod-tiposolic') + ',';
    });
    if (_vtiposolic == "" || _vtiposolic == null) {
        _vtiposolic = "0"
    } else {
        _vtiposolic = _vtiposolic.substring(0, ((_vtiposolic.length) - 1));
        _vtiposolic = _vtiposolic.substring(9, _vtiposolic.length);
    }

    if (String(vpdv.replace(/ /g, "")).length > 0 &&
        String(vruc.replace(/ /g, "")).length > 0 &&
        String(vcodigo.replace(/ /g, "")).length > 0 &&
        ($('#DivGaleriaSolicitud').find('div').length > 1)) {

        $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');

        //$('.lucky-loading').modal('show');

        var ArrayFoto = [];
        $('div[data-solicitud-archivo]').each(function () {
            var SolicitudArchivo = $(this).data('solicitud-archivo');
            ArrayFoto.push(SolicitudArchivo);
        });
        $('#id-md-solicitud').modal('hide');
        fn_reg_solicitud(1, ArrayFoto.join(','));
    } else {
        if (String(vpdv.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-nom-pdv').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
        }
        if (String(vruc.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-ruc').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
        }
        if (String(vcodigo.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-codigo').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
        }

        if ($('#DivGaleriaSolicitud').find('div').length == 1) {
            alert("Debe ingresar por lo menos una imagen");
        }
    }
});
$(document).on('click', '#btn-md-enviar', function () {
    var vpdv = $('#id-md-nom-pdv').val()
           , vruc = $('#id-md-ruc').val()
           , vcodigo = $('#id-md-codigo').val()
           , vdireccion = $('#id-md-direccion').val()
           , vdistrito = $('#id-md-distrito').val()
           , vprovincia = $('#id-md-provincia').val()
           , vdepartamento = $('#id-md-departamento').val()
           , vcontacto = $('#id-md-contacto').val()
           , vnumtelef = $('#id-md-telefono').val()
           , vnumcel = $('#id-md-celular').val()
           , vdistribuidora = $('#id-md-distribuidora').val()
           , p1_codZona = $('#id-md-zona').val()
           , vmarca = $('#id-md-marca').val()
           , v_rutafoto1 = $('#ruta-foto-1').val();

    var _vtiposolic;
    $('input[name="gchktiposolic"]:checked').each(function () {
        _vtiposolic += $(this).data('cod-tiposolic') + ',';
    });
    if (_vtiposolic == "" || _vtiposolic == null) {
        _vtiposolic = "0"
    } else {
        _vtiposolic = _vtiposolic.substring(0, ((_vtiposolic.length) - 1));
        _vtiposolic = _vtiposolic.substring(9, _vtiposolic.length);
    }

    if (String(vpdv.replace(/ /g, "")).length > 0 &&
        String(vruc.replace(/ /g, "")).length > 0 &&
        String(vcodigo.replace(/ /g, "")).length > 0 &&
        String(vdireccion.replace(/ /g, "")).length > 0 &&
        String(vdistrito.replace(/ /g, "")).length > 0 &&
        String(vprovincia.replace(/ /g, "")).length > 0 &&
        String(vdepartamento.replace(/ /g, "")).length > 0 &&
        String(vcontacto.replace(/ /g, "")).length > 0 &&
        String(vnumcel.replace(/ /g, "")).length > 0 &&
        String(vdistribuidora.replace(/ /g, "")).length > 0 &&
        _vtiposolic != "0" &&
        p1_codZona != "0" &&
        vmarca != "0" &&
        ($('#DivGaleriaSolicitud').find('div').length > 1)
        ) {

        $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-marca').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-celular').removeClass('has-error2');
        $('.objrequeridsolicitud #div_tipos').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-departamento').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-contacto').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-provincia').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-distrito').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-direccion').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-distribuidora').removeClass('has-error2');
        $('.objrequeridsolicitud #id-md-zona').removeClass('has-error2');

        var ArrayFoto = [];
        $('div[data-solicitud-archivo]').each(function () {
            var SolicitudArchivo = $(this).data('solicitud-archivo');
            ArrayFoto.push(SolicitudArchivo);
        });
        fn_reg_solicitud(2, ArrayFoto.join(','));

        $('#id-md-solicitud').modal('hide');

    } else {
        if (String(vpdv.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-nom-pdv').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
        }
        if (String(vruc.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-ruc').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
        }
        if (vmarca == "0") {
            $('.objrequeridsolicitud #id-md-marca').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-marca').removeClass('has-error2');
        }
        if (String(vcodigo.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-codigo').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
        }
        if (String(vnumcel.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-celular').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-celular').removeClass('has-error2');
        }
        if (_vtiposolic == "0") {
            $('.objrequeridsolicitud #div_tipos').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #div_tipos').removeClass('has-error2');
        }
        if (String(vdepartamento.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-departamento').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-departamento').removeClass('has-error2');
        }
        if (String(vcontacto.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-contacto').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-contacto').removeClass('has-error2');
        }
        if (String(vprovincia.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-provincia').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-provincia').removeClass('has-error2');
        }
        if (String(vdistrito.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-distrito').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-distrito').removeClass('has-error2');
        }
        if (String(vdireccion.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-direccion').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-direccion').removeClass('has-error2');
        }
        if (String(vdistribuidora.replace(/ /g, "")).length == 0) {
            $('.objrequeridsolicitud #id-md-distribuidora').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-distribuidora').removeClass('has-error2');
        }
        if (p1_codZona == "0") {
            $('.objrequeridsolicitud #id-md-zona').addClass("has-error2");
        } else {
            $('.objrequeridsolicitud #id-md-zona').removeClass('has-error2');
        }

        if ($('#DivGaleriaSolicitud').find('div').length == 1) {
            alert("Debe ingresar por lo menos una imagen");
        }
    }
});
$(document).on('click', '#btn-md-editar', function (e) {
    $("#id-md-solicitud input").prop('disabled', false);
    $("#id-md-solicitud select").prop('disabled', false);
    $("#id-md-solicitud textarea").prop('disabled', false);
    $("#btn-md-grabar").css("visibility", "visible");
    $("#btn-md-cancelar").css("visibility", "visible");
    $("#btn-md-enviar").css("visibility", "visible");

    $('#btn-group-general-solicitud button').removeAttr('disabled');

    $("#id-md-solicitud .panel-footer .pull-left #btn-md-grabar").prop('disabled', false);
    $("#id-md-solicitud .panel-footer .pull-left #btn-md-cancelar").prop('disabled', false);
    $("#id-md-solicitud .panel-footer .pull-left #btn-md-enviar").prop('disabled', false);


    $("#btn-md-grabar").css("background-color", "#5AAF00");
    $("#btn-md-cancelar").css("background-color", "#7B0F84");
    $("#btn-md-enviar").css("background-color", "#009DC8");

    $('div[data-solicitud-archivo] a').each(function () {
        $(this).attr('disabled', false);
    });
});

$(document).on('click', 'div[data-solicitud-archivo] img', function (e) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [],
        vImage = new Image(),
        options = {
            history: false,
            focus: false,
            showAnimationDuration: 0,
            hideAnimationDuration: 0,
            shareEl: false
        };

    $.each($('div[data-solicitud-archivo] img'), function (i, v) {
        vImage.src = $(this).attr('src');
        __arrayFotos.push({ src: $(this).attr('src'), w: vImage.naturalWidth, h: vImage.naturalHeight });
    });

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
});

function fn_reg_solicitud(vop, foto) {
    var _vtiposolic;
    $('input[name="gchktiposolic"]:checked').each(function () {
        _vtiposolic += $(this).data('cod-tiposolic') + ',';
    });
    if (_vtiposolic == "" || _vtiposolic == null) {
        _vtiposolic = "0"
    } else {
        _vtiposolic = _vtiposolic.substring(0, ((_vtiposolic.length) - 1));
        _vtiposolic = _vtiposolic.substring(9, _vtiposolic.length);
    }
    $.ajax({
        beforeSend: function (xhr) {
            $('#lucky-load').modal('show');
        },
        url: 'AddSolicitud',
        type: 'POST',
        dataType: 'json',
        data: {
            vids: ($('#id-hd-codigo').val() == null || $('#id-hd-codigo').val() == "" ? 0 : $('#id-hd-codigo').val())
            , vpdv: $('#id-md-nom-pdv').val()
            , vruc: $('#id-md-ruc').val()
            , vcodigo: $('#id-md-codigo').val()
            , vdireccion: $('#id-md-direccion').val()
            , vdistrito: $('#id-md-distrito').val()
            , vprovincia: $('#id-md-provincia').val()
            , vdepartamento: $('#id-md-departamento').val()
            , vcontacto: $('#id-md-contacto').val()
            , vnumtelef: $('#id-md-telefono').val()
            , vnumcel: $('#id-md-celular').val()
            , vdistribuidora: $('#id-md-distribuidora').val()
            , vfoto: foto
            , vven_prom_soles: $('#id-md-venta-soles').val()
            , vven_prom_ton: $('#id-md-venta-toneladas').val()
            , vlinea_credito: $('#id-md-credito').val()
            , vdeu_actual: $('#id-md-deuda').val()
            , vop_grabado: vop
            , vop_opera: ($('#id-hd-codigo').val() == '0' || $('#id-hd-codigo').val() == null || $('#id-hd-codigo').val() == "" ? '1' : '2')
            , p1_codperson: $('#id-hd-person').val()
            , p1_codTipo: _vtiposolic
            , p1_codZona: $('#id-md-zona').val()
            , p1_comentarios: $('#id-md-comentario').val()
            , vmarca: $('#id-md-marca').val()
            , v_tipo_gral_solicitud: $('#auxbuton').val()
            , v_referencia: $('#id-md-referencia').val()
            , v_cod_equipo : Campania.Idplanning
        },
        success: function (a) {
            fnOnClickRefresca();

        },
        complete: function (a) {

            $('#lucky-load').modal('hide');
            //fn_md_nueva_solicitud();
        },
        error: function (xhr) {
            console.error('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });

}
function fn_confirmar_registro() {
    //$.pnotify({
    //    title: 'Confirmación',
    //    text: 'Solicitud registrada con exito!!!',
    //    type: 'success'
    //});
}

//CArga foto solicitud
function fnOnChangeFile() {
    if ($('div[data-solicitud-archivo]').length == 5) return;

    var oFile = ($('#FileImagenSolicitud'))[0].files[0];
    var form = $('#FormCargaImagenSolicitud')[0];
    var dataString = new FormData(form);

    $.ajax({
        url: Url.CargaFoto,
        beforeSend: function () {
            $('#lucky-loading').modal('show');
        },
        type: 'post',
        success: function (__s) {
            if (__s.estado == true) {
                $('#DivGaleriaSolicitud').append('<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" id="' + __s.id + '" data-solicitud-archivo="' + __s.archivo + '"><a href="javascript:"><img src="' + Url.Fachada + 'Img/' + __s.archivo + '" class="img-thumbnail" alt="' + __s.archivo + '" /></a><a href="javascript:" onclick="fnOnRemoveImage(\'' + __s.id + '\',\'' + __s.archivo + '\');" style="position: absolute; right: 10px; top: -5px;"><i class="fa fa-times" style="background-color: #B40404; padding: 5px; border-radius: 13px; color: white; font-size:10px;"></i></a></div>');
            } else {
                alert('Problemas al cargar la imagen.');
            }
        },
        dataType: 'json',
        error: function (__e) {
            console.error(__e);
            $('#lucky-loading').modal('hide');
        },
        complete: function () {
            $('#lucky-loading').modal('hide');
        },
        data: dataString,
        cache: false,
        contentType: false,
        processData: false
    });
}
//Funcion para elimianr la fto
function fnOnRemoveImage(id, archivo) {
    var RemoveDisabled = $('#' + id + ' a').attr('disabled');

    if (typeof RemoveDisabled !== typeof undefined && RemoveDisabled !== false) return;

    $.ajax({
        type: 'post',
        dataType: 'json',
        url: Url.EliminaFoto,
        data: { ArchivoNombre: archivo },
        beforeSend: function () {
            $('#lucky-loading').modal('show');
        },
        success: function (respuesta) {
            if (respuesta.Resultado == true) {
                $('#' + id).remove();
            }
        },
        complete: function () {
            $('#lucky-loading').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-loading').modal('hide');
        }
    });
}

// Ver detalle de datos
$(document).on('click', '.cls-ver-datos', function (e) {
    $('.objrequeridsolicitud #id-md-nom-pdv').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-ruc').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-marca').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-codigo').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-celular').removeClass('has-error2');
    $('.objrequeridsolicitud #div_tipos').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-departamento').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-contacto').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-provincia').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-distrito').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-direccion').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-distribuidora').removeClass('has-error2');
    $('.objrequeridsolicitud #id-md-zona').removeClass('has-error2');

    $('div[data-solicitud-archivo]').each(function () {
        $(this).remove();
    });

    $("#id-md-zona").children().removeAttr("selected");
    $('#id-md-zona').val($(this).data('p1zona'));

    $('#id-hd-codigo').val($(this).data('ids'));
    $('#id-md-nom-pdv').val($(this).data('pdv'));
    $('#id-md-ruc').val($(this).data('ruc'));

    /*
    if (($.trim(data)).length > 36) {
                        Title = 'title="' + ($.trim(data)) + '"';
                        Value = ($.trim(data)).substr(0, 33) + "...";
                    } else {
                        Title = '';
                        Value = $.trim(data);
                    }
    */
    if ($.trim($(this).data('cod')).length > 13) {
        $('#id-md-codigo').val($.trim($(this).data('cod')).substr(0, 10) + "...");
        $('#id-md-codigo').attr("title", $.trim($(this).data('cod')));
    } else {
        $('#id-md-codigo').val($(this).data('cod'));
        $('#id-md-codigo').removeAttr("title");
    }
    if ($.trim($(this).data('direc')).length > 35) {
        $('#id-md-direccion').val($.trim($(this).data('direc')).substr(0, 32) + "...");
        $('#id-md-direccion').attr("title", $.trim($(this).data('direc')));
    } else {
        $('#id-md-direccion').val($(this).data('direc'));
        $('#id-md-direccion').removeAttr("title");
    }
    $('#id-md-distrito').val($(this).data('dist'));
    $('#id-md-provincia').val($(this).data('prov'));
    $('#id-md-departamento').val($(this).data('dep'));
    $('#id-md-contacto').val($(this).data('cont'));
    $('#id-md-telefono').val($(this).data('telf'));
    $('#id-md-celular').val($(this).data('celu'));
    $('#id-md-distribuidora').val($(this).data('distrib'));
    $('#id-md-referencia').val($(this).data('referencia'));



    var ArrayFoto = [];
    $.each($(this).data('img-1').split(','), function (i, v) {
        var SolicitudArchivo = v.replace('/Fachada/Img/', '');
        var SolicitudArchivo2 = SolicitudArchivo.replace('Img_', '');
        var SolicitudArchivoId = SolicitudArchivo2.split('.', 1);
        if (SolicitudArchivoId != null && SolicitudArchivoId != '') {
            $('#DivGaleriaSolicitud').append('<div class="col-xs-12 col-sm-2 col-md-12 col-lg-2" id="' + SolicitudArchivoId + '" data-solicitud-archivo="' + SolicitudArchivo + '"><a href="javascript:"><img src="' + Url.Fachada + 'Img/' + SolicitudArchivo + '" class="img-thumbnail" alt="' + SolicitudArchivo + '" /></a><a href="javascript:" onclick="fnOnRemoveImage(\'' + SolicitudArchivoId + '\',\'' + SolicitudArchivo + '\');" style="position: absolute; right: 10px; top: -5px;"><i class="fa fa-times" style="background-color: #B40404; padding: 5px; border-radius: 13px; color: white; font-size:10px;"></i></a></div>');
        }

    });

    $('#id-md-venta-soles').val($(this).data('m1'));
    $('#id-md-venta-toneladas').val($(this).data('m2'));
    $('#id-md-credito').val($(this).data('m3'));
    $('#id-md-deuda').val($(this).data('m4'));

    $('#id-md-comentario').val($(this).data('p1comen'));

    $("#id-md-marca").children().removeAttr("selected");
    $('#id-md-marca').val($(this).data('marca'));
    $('#auxbuton').val($(this).data('tipo-gralsolicitud'));

    $('#btn-group-general-solicitud button')
        .removeClass()
        .addClass('btn btn-default');
    if ($(this).data('tipo-gralsolicitud') == 1) {
        $('#id-btn-implementacion')
            .removeClass()
            .addClass('btn btn-primary');
    } else if ($(this).data('tipo-gralsolicitud') == 2) {
        $('#id-btn-mantenimiento')
            .removeClass()
            .addClass('btn btn-primary');
    } else if ($(this).data('tipo-gralsolicitud') == 3) {
        $('#id-btn-tramite')
            .removeClass()
            .addClass('btn btn-primary');
    }

    var contador = $('input[name="gchktiposolic"]').length;

    var str = String($(this).data('p1tsolic'));

    var valores = "";
    posicionaux = 0;
    posicion = str.indexOf(",");
    if (posicion != -1) {
        do {
            if (posicionaux == 0) {
                if (valores == "") {
                    valores = parseInt(str.substring(0, posicion)) + ",";
                }
            } else {
                valores += parseInt(str.substring(posicionaux + 1, posicion)) + ",";
            }
            posicionaux = posicion;
            posicion = str.indexOf(",", posicion + 1);
            if (posicion == -1) {
                valores += parseInt(str.substring(posicionaux + 1, str.length)) + ",";
            }
        } while (posicion != -1);
        var valores2 = valores.substring(0, valores.length - 1);
        var test = "[" + valores2 + "]";
    } else {
        var test = [parseInt(str)];
    }

    for (var i = 1; i <= contador; i++) {
        if (jQuery.inArray(i, test) != -1 || jQuery.inArray(i.toString(), test) != -1) {
            $('input[name="gchktiposolic"]')[i - 1].checked = true;
        } else {
            $('input[name="gchktiposolic"]')[i - 1].checked = false;
        }
    }

    $('.mask').inputmask();
    $('#btn-md-editar').show();
    $("#btn-md-editar").prop('disabled', false);
    $("#ruta-foto-1").prop('disabled', false);

    $('div[data-solicitud-archivo] a').each(function () {
        $(this).attr('disabled', true);
    });

    if ($(this).data('estado') == "Enviado") {
        //$("#btn-md-editar").prop('disabled', true);
        $("#btn-md-editar").css('visibility', 'hidden');
        $("#btn-md-grabar").css('visibility', 'hidden');
        $("#btn-md-cancelar").css('visibility', 'hidden');
        $("#btn-md-enviar").css('visibility', 'hidden');
    } else {
        $("#btn-md-editar").css('visibility', 'visible');
    }
    if (Persona.Grupo == 2 || Persona.Grupo == 3) {
        if ($(this).data('estado-solicitud') == 1) {
            $('#btn-md-desactivar').css("visibility", "visible");
            $('#btn-md-activar').css("visibility", "hidden");
        } else if ($(this).data('estado-solicitud') == 2) {
            $('#btn-md-desactivar').css("visibility", "hidden");
            $('#btn-md-activar').css("visibility", "visible");
            $('#btn-md-activar').css("margin-right", "-140px");
        }
    } else {
        $('#btn-md-desactivar').css("visibility", "hidden");
        $('#btn-md-activar').css("visibility", "hidden");
    }
    fn_md_consultar_solicitud();
});
function fn_md_consultar_solicitud() {
    $("#btn-md-grabar").css("background-color", "#8DB95D");
    $("#btn-md-cancelar").css("background-color", "#806882");
    $("#btn-md-enviar").css("background-color", "#6BB3C6");


    $('#id-md-solicitud').modal({ backdrop: 'static' });
    $("#id-md-solicitud .panel-footer .pull-left button").prop('disabled', true);
    $("#id-md-solicitud input").prop('disabled', true);
    $("#id-md-solicitud select").prop('disabled', true);
    $("#id-md-solicitud textarea").prop('disabled', true);
    $("#btn-md-grabar").css("visibility", "hidden");
    $("#btn-md-cancelar").css("visibility", "hidden");
    $("#btn-md-enviar").css("visibility", "hidden");
    $("#_solicitud").prop('disabled', true);

    $('#btn-group-general-solicitud button').attr('disabled', 'disabled');
}
//Completar Ruc
$(document).on('blur', '#id-md-ruc', function (e) {
    var text = $(this).val();
    text = text.replace("_", "");

    text = text.trim().length;
    if (text < 11 && text > 0) {
        //alert('Completar el campo');
        $('#id-md-ruc').focus();
    }
});
//Completar Celular
$(document).on('blur', '#id-md-celular', function (e) {
    var text = $(this).val();
    text = text.replace("_", "");

    text = text.trim().length;
    if (text < 11 && text > 0) {
        //alert('Completar el campo');
        $('#id-md-celular').focus();
    }
});
//Completar Telefono
$(document).on('blur', '#id-md-telefono', function (e) {
    var text = $(this).val();
    text = text.replace("_", "");

    text = text.trim().length;
    if (text < 14 && text > 0) {
        //alert('Completar el campo');
        $('#id-md-telefono').focus();
    }
});

//Aprobacion de Trade(Aprobacion de solicitud)
$(document).on('click', '.cls-sw-preg-solicitud i', function (e) {
    if ($(this).hasClass('cls-active') != true) {
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        var vuser = $(this).data('user');
        var vop = $(this).data('op');
        var vcomentario = $(this).data('comentario');
        var vclase = $(this).data('clase');
        if (vvalor == 'si') {
            $(this).parent().children('i').removeClass('cls-active');
            $(this).addClass('cls-active');
            fn_p2_aprosolicitud(vid, 1, 2, '');

        } else if (vvalor == 'no') {
            if (vcomentario != '') {
                $(this).parent().children('i').removeClass('cls-active');
                $(this).addClass('cls-active');
                fn_p2_aprosolicitud(vid, 0, 2, '');
            } else {
                fn_modal_comentario(vid, vop, vuser, vcomentario, 1, vclase);
            }
        }
    }
});


//FUNCION Aprobacion de Trade(Aprobacion de solicitud)
function fn_p2_aprosolicitud(vid, vop, vtipo, vclase) {
    if (vclase != '') {
        $("." + vclase).parent().children('i').removeClass('cls-active');
        $("." + vclase).addClass('cls-active');
    }
    var _vestado = ""
    var _vcomentario = "";
    if (vop == 1) {
        _vestado = 1;
    } else if (vop == 0) {
        _vestado = 0;
    } else if (vop == 2) {
        _vestado = 2;
    }
    _vcomentario = $("#id-ta-comentario").val();
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'AproSolicitudp2',
        type: 'POST',
        dataType: 'json',
        data: {
            vnumero: vid
            , vestado: _vestado
            , vcomentario: _vcomentario
        },
        success: function (a) {
            $('#id-fec-aprosoli_' + vid).html(a._a);
            $('#id-comen-aprosoli_' + vid).html((String(a._b).length > 5 ? "<a>Ver</a>" : a._b));
            $('#id-comen-aprosoli_' + vid).data('comentario', a._b);
            if (vtipo == 1) {
                fn_p2_aprosolicitud(vid, 0, 2, '');
            }
            if (vop == 0) {
                fnOnClickRefresca();
            }
        },
        error: function (xhr) {
            console.error('[Advertencia]<AprobacionSolicitud>: Problemas con el servicio.');
        }
    });
}

//Modal Comentario
function fn_modal_comentario(vid, vop, vuser, vcomen, vtipo, vclase) {
    $("#id-md-apsolicitud-comen #id_tipo").val(vtipo);
    $('#id-md-apsolicitud-comen .pull-right .btn.btn-success').hide();

    $('#id-md-apsolicitud-comen .cls-obligatorio').html('');
    $('#id-md-apsolicitud-comen #id-numreg').val("");
    $('#id-md-apsolicitud-comen #id-tipo').val("");
    $('#id-md-apsolicitud-comen #id-clase').val("");
    $("#id-md-apsolicitud-comen #id-ta-comentario").val("");
    $("#id-md-apsolicitud-comen .cls-op").val("");
    $('#id-md-apsolicitud-comen #id-numreg').val(vid);
    $('#id-md-apsolicitud-comen #id-tipo').val(vtipo);
    $('#id-md-apsolicitud-comen #id-op').val(vop);
    $('#id-md-apsolicitud-comen #id-clase').val(vclase);
    if (vop == 1) {
        $("#id-md-apsolicitud-comen #id-ta-comentario").val(vcomen);
        if (vuser == 2) {
            $('#id-md-apsolicitud-comen #id-btn-apsolicitud-grabar').show();
            $("#id-md-apsolicitud-comen #id-ta-comentario").prop('disabled', false);
        } else {
            $("#id-md-apsolicitud-comen #id-ta-comentario").prop('disabled', true);
        }
    } else if (vop == 2) {
        $("#id-md-apsolicitud-comen #id-ta-comentario").val(vcomen);
        if (vuser == 1) {
            $('#id-md-apsolicitud-comen #id-btn-apfotomon-grabar').show();
            $("#id-md-apsolicitud-comen #id-ta-comentario").prop('disabled', false);
        } else {
            $("#id-md-apsolicitud-comen #id-ta-comentario").prop('disabled', true);
        }
    }

    $('#id-md-apsolicitud-comen').modal('show');
}

//Grabar comentario de aprobacion o desaprobacion de solicitud
$(document).on('click', '#id-btn-apsolicitud-grabar', function (e) {
    var _vtipo = $("#id-tipo").val();
    var _vclase = $("#id-clase").val();
    var _vid = $('#id-md-apsolicitud-comen #id-numreg').val();
    var _texto = $("#id-ta-comentario").val();
    var _op = $("#id-op").val();

    if (_op == 1) {
        $(".cls-sw-preg-solicitud i").data('comentario', _texto);
    } else if (_op == 2) {
        $(".cls-sw-preg-fotomont i").data('comentario', _texto);
    }

    fn_p2_aprosolicitud(_vid, 2, _vtipo, _vclase);
    $('#id-md-apsolicitud-comen').modal('hide');
});

//Click para abrir Modal de COnfirmacion eliminacion
$(document).on('click', '#id-delete-grid', function (e) {
    $('#id_num_solicitud').val($(this).data('idregdrop'));
    $('#id-md-sm-delete').modal();
});
//Click para grbar Eliminacion de Registro
$(document).on('click', '#id-btn-aceptar', function (e) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Eliminar_SolicitudVentas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: $('#id_num_solicitud').val(),
            __b: 0
        },
        success: function (a) {
            fnOnClickRefresca();
            $('#id-md-sm-delete').modal('hide');
        },
        error: function (xhr) {
            console.error('[Advertencia]<RemoveSolicitud>: Problemas con el servicio.');
        }
    });
});

//Abre el modal de Comentario
$(document).on('click', '.cls-td-comen-solicitud', function (e) {
    var _vcomen = $(this).data('comentario');
    var _vop = $(this).data('op');

    var _vid = $(this).data('id');
    var _vuser = $(this).data('user');
    if (_vop == 1) {

        if (_vuser == 2) {
            fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
        } else {
            if (_vcomen.length > 0) {
                fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
            }
        }
    } else if (_vop == 2) {
        if (_vuser == 1) {
            fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
        } else {
            if (_vcomen.length > 0) {
                fn_modal_comentario(_vid, _vop, _vuser, _vcomen, 2, '');
            }
        }
    }

});

//Actualiza Proveedor
$(document).on('change', '.cls-datos-p3', function (e) {
    var _vid = $(this).data('id');
    var _a = this.value;
    _a = (_a == null ? "" : _a);
    fn_ope_p3_lucky(4, _a, _vid);
});

//Funcion´para actualizar Proveedor
function fn_ope_p3_lucky(_va, _vb, _vc) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso3_Lucky',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc
        },
        success: function (a) {
            if (_va == 4) {
                alert("Registro actualizado con exito!");
            }
        },
        error: function (xhr) {
            console.error('[Advertencia]<p3>: Problemas con el servicio.');
        }
    });
}

//Abre el modal de Comentario para LuckyAnalis
$(document).on('click', '.cls-datos-p6', function (e) {
    var _vid = $(this).data('id');
    var _vcomen = $(this).data('comentario');
    var _code = $(this).data('cod');
    if (_vcomen.length > 0) {
        var _vcomen = $(this).data('comentario');
    } else {
        var _vcomen = "";
    }
    $('#txt-comen-p6').val(_vcomen);

    $('#id-md-p6-lucky').data('id', _vid);
    $('#id-md-p6-lucky').modal({ backdrop: 'static' });
    $("#id-md-p6-lucky #id-btn-p6-grabar").css('visibility', 'visible');
    if (_code != "3") {
        $("#id-md-p6-lucky #id-btn-p6-grabar").css('visibility', 'hidden');
    }
});
//Grabar Comentario  para Luckyanalis
$(document).on('click', '#id-btn-p6-grabar', function (e) {
    var _vid = $('#id-md-p6-lucky').data('id');
    var _a = $('#txt-comen-p6').val();
    _a = (_a == null ? "" : _a);
    fn_ope_p6_lucky(1, _a, _vid);
});

//Funcion para grabar el comentario para LuckyAnalis
function fn_ope_p6_lucky(_va, _vb, _vc) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_Lucky',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc
        },
        success: function (a) {
            if (_va == 1) {
                alert("Comentario guardado con exito!");
                $('.cls-datos-p6 .cls-1-' + _vc).html(_vb.length > 5 ? "<a>Ver</a>" : _vb);
                $('.cls-datos-p6-' + _vc).data('comentario', _vb);
                $('#id-md-p6-lucky').modal('hide');
            }
        },
        error: function (xhr) {
            console.error('[Advertencia]<p6>: Problemas con el servicio.');
        }
    });
}


//Abre el Modal de Fotomontaje
$(document).on('click', '.cls-item-fotomontaje', function () {
    var __id = $(this).data('id'),
        __npdv = $(this).data('npdv'),
        __doc = '',
        __comment = '';

    $('#id-download-word').hide();
    $('#id-md-fotomontaje input[type="file"]').val('');
    $('#id-md-fotomontaje').data('codigo', '');
    $('#id-md-fotomontaje').data('npdv', '');
    $("#id-cls-area-comment").val('');

    $('#id-md-fotomontaje').data('codigo', __id);
    $('#id-md-fotomontaje').data('npdv', __npdv);

    $.ajax({
        url: 'Consultar_Fotomontaje',
        type: 'post',
        dataType: 'json',
        async: true,
        data: { __a: __id },
        beforeSend: function () {
            $('#foto1').css('visibility', 'hidden');
            $('#foto2').css('visibility', 'hidden');
            $('#foto3').css('visibility', 'hidden');
            $('#foto4').css('visibility', 'hidden');

            $(".cls-img-1").attr('src', Url.Xplora + 'Fachada/Img/foto_nodisponible.jpg');
            $(".cls-img-2").attr('src', Url.Xplora + 'Fachada/Img/foto_nodisponible.jpg');
            $(".cls-img-3").attr('src', Url.Xplora + 'Fachada/Img/foto_nodisponible.jpg');
            $(".cls-img-4").attr('src', Url.Xplora + 'Fachada/Img/foto_nodisponible.jpg');

            $('#id-hd-img1').val('');
            $('#id-hd-img2').val('');
            $('#id-hd-img3').val('');
            $('#id-hd-img4').val('');

            if (Persona.Grupo != 3) {
                $('.cls-img-fotomontaje .btn-file').css('visibility', 'hidden');
            } else {
                $('.cls-img-fotomontaje .btn-file').css('visibility', 'visible');
            }

            ArrayFotomontaje = [];
        }, success: function (response) {
            var _vbodyhistoric = "";
            $.each(response, function (key, value) {
                if (value._g == "1") {
                    __doc = value._d;
                    __comment = value._f;
                    switch (value._b) {
                        case 1:
                            $('#id-hd-img1').val(value._c);
                            if (value._c != '') {
                                $(".cls-img-1").attr('src', Url.Xplora + value._c);
                                if (Persona.Grupo == 3) {
                                    $('#foto1').css("visibility", "visible");
                                }
                                $('.cargar-foto1').css("display", "none");

                                ArrayFotomontaje.push({ Orden: 1, Url: Url.Xplora + value._c });
                            } else if (value._c == '' || value._c == null) {
                                if (Persona.Grupo == 3) {
                                    $('.cargar-foto1').css("display", "block");
                                } else {
                                    $('.cargar-foto1').css("display", "none");
                                }
                            }
                            break;
                        case 2:
                            $('#id-hd-img2').val(value._c);
                            if (value._c != '') {
                                $(".cls-img-2").attr('src', Url.Xplora + value._c);
                                if (Persona.Grupo == 3) {
                                    $('#foto2').css("visibility", "visible");
                                }
                                $('.cargar-foto2').css("display", "none");

                                ArrayFotomontaje.push({ Orden: 2, Url: Url.Xplora + value._c });
                            } else if (value._c == '' || value._c == null) {
                                if (Persona.Grupo == 3) {
                                    $('.cargar-foto2').css("display", "block");
                                } else {
                                    $('.cargar-foto2').css("display", "none");
                                }
                            }
                            break;
                        case 3:
                            $('#id-hd-img3').val(value._c);
                            if (value._c != '') {
                                $(".cls-img-3").attr('src', Url.Xplora + value._c);
                                if (Persona.Grupo == 3) {
                                    $('#foto3').css("visibility", "visible");
                                }
                                $('.cargar-foto3').css("display", "none");
                                ArrayFotomontaje.push({ Orden: 3, Url: Url.Xplora + value._c });
                            } else if (value._c == '' || value._c == null) {
                                if (Persona.Grupo == 3) {
                                    $('.cargar-foto3').css("display", "block");
                                } else {
                                    $('.cargar-foto3').css("display", "none");
                                }
                            }
                            break;
                        case 4:
                            $('#id-hd-img4').val(value._c);
                            if (value._c != '') {
                                $(".cls-img-4").attr('src', Url.Xplora + value._c);
                                if (Persona.Grupo == 3) {
                                    $('#foto4').css("visibility", "visible");
                                }
                                $('.cargar-foto4').css("display", "none");
                                ArrayFotomontaje.push({ Orden: 4, Url: Url.Xplora + value._c });
                            } else if (value._c == '' || value._c == null) {
                                if (Persona.Grupo == 3) {
                                    $('.cargar-foto4').css("display", "block");
                                } else {
                                    $('.cargar-foto4').css("display", "none");
                                }
                            }
                            break;
                    }
                }
                else {

                    switch (value._b) {
                        case 1:
                            _vbodyhistoric += '<td><img style="width: 100px; height: 40px;" src="' + value._c + '" /></td>';
                            break;
                        case 2:
                            _vbodyhistoric += '<td><img style="width: 100px; height: 40px;" src="' + value._c + '" /></td>';
                            break;
                        case 3:
                            _vbodyhistoric += '<td><img style="width: 100px; height: 40px;" src="' + value._c + '" /></td>';
                            break;
                        case 4:
                            _vbodyhistoric += '<td><img style="width: 100px; height: 40px;" src="' + value._c + '" /></td>';
                            break;
                    }
                    //_vbodyhistoric+="</tr>"
                }
            });
            $("#tb_historialfoto tbody").append("<tr>" + _vbodyhistoric + "</tr>");

            $("#id-cls-area-comment").val(__comment);

            if (__doc != null && __doc != "") {
                $('#id-download-word').attr('href', Url.Xplora + 'Descarga/WFUnacem?vdoc=' + __doc);
                $('#id-download-word').show();
            }
        }, complete: function () {
            $('#id-md-fotomontaje').modal('show');
        }, error: function (e) {
            console.error(e);
        }
    });
});

/*Metodo para invalidar sin usuario es diferente a LuckyAnalis*/
$(document).on('click', '#id-md-fotomontaje input[type="file"]', function (e) {
    if (Persona.Grupo != 3) {
        return false;
    }
});
/*Metodo pra insertar imagenes al fotomontaje*/
$(document).on('change', '#id-md-fotomontaje input[type="file"]', function (e) {
    var sms = $(this).val();
    var vid = $('#id-md-fotomontaje').data('codigo');
    var vimg = $(this).data('img');
    var vhdd = $(this).data('hdd');
    var vorden = $(this).data('orden');
    if (sms) {
        $('#id-' + vimg).upload(
                'Carga_Img_FotoMon',
                { __a: vid, __b: vorden },
                function (a) {
                    var _nomfoto = String(a);
                    $('#id-md-fotomontaje .cls-img-fotomontaje td .' + vimg)
                        .removeAttr('src')
                        .attr('src', Url.Xplora + _nomfoto);
                    $('#' + vhdd).val(_nomfoto);
                    $("#foto" + vorden).css("visibility", "visible");
                    $('.cargar-foto' + vorden).css("display", "none");

                    $('.cls-img-' + vorden).css("margin-left", "5px");
                    $('.cls-img-' + vorden).css("margin-right", "5px");

                    fn_BlankFotoswipeFotomontaje();
                },
                'json'
        );
    } else {
        $(this).parent().children('span').html('');
    }
});

//Click para Eliminar el Fotomonatje
$(document).on('click', '.cls-eliminar-file', function (e) {
    var vorden = $(this).data('orden');
    var vhh = $(this).data('hdd');
    var vimg = $(this).data('img');
    $.ajax({
        url: 'Eliminar_Img_FotoMon',
        type: "POST",
        dataType: "json",
        async: true,
        data: {
            __a: $('#id-md-fotomontaje').data('codigo'),
            __b: vorden,
            __c: $('#' + vhh).val()
        },
        success: function (response) {
            if (response == "1") {
                //$("." + vimg).attr('src', '');
                $("." + vimg).attr('src', Url.Xplora + 'Fachada/Img/foto_nodisponible.jpg');

                $("#id-hd-img" + vorden).val('');

                fn_BlankFotoswipeFotomontaje();

            }
            $("#foto" + vorden).css("visibility", "hidden");
            //$('.cls-img-' + vorden).css("display", "none");
            $('.cargar-foto' + vorden).css("display", "block");

        }
    });
});

//Fooswipe Fotomontake
$(document).on('click', '#id-md-fotomontaje img', function (e) {
    //if (Persona.Grupo != 3) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [],
        vImage = new Image(),
        options = {
            history: false,
            focus: false,
            showAnimationDuration: 0,
            hideAnimationDuration: 0,
            shareEl: false
        };
        //console.log(ArrayFotomontaje);
        if (ArrayFotomontaje.length > 0) {
            $.each(ArrayFotomontaje, function (i, v) {
                vImage.src = v.Url;
                __arrayFotos.push({ src: v.Url, w: vImage.naturalWidth, h: vImage.naturalHeight });
            });

            var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
            gallery.init();
        } //else {
            //alert("no hay imagenes por mostrar")
        //}
    //}
});

//Genera Carta de Fotomontaje
$(document).on('click', '#id-btn-generar-word', function (e) {
    if (!$('#id-hd-img1').val() && !$('#id-hd-img2').val() && !$('#id-hd-img3').val() && !$('#id-hd-img4').val()) {
        alert(".:: Para generar el word tiene que seleccionar minimo una imagen ::.");
    } else {
        $('#id-download-word').hide();
        $.ajax({
            url: 'Generar_Documento',
            beforeSend: function (xhr) {
                $('#id-md-fotomontaje .cls-loading').html($('.cls-rep-loading1').html());
            },
            type: "POST",
            dataType: "json",
            async: true,
            data: {
                __a: $('#id-md-fotomontaje').data('npdv'),
                __b: (!$('#id-hd-img1').val() ? "" : $('#id-hd-img1').val()),
                __c: (!$('#id-hd-img2').val() ? "" : $('#id-hd-img2').val()),
                __d: (!$('#id-hd-img3').val() ? "" : $('#id-hd-img3').val()),
                __e: (!$('#id-hd-img4').val() ? "" : $('#id-hd-img4').val()),
                __f: $('#id-md-fotomontaje').data('codigo'),
                __g: (!$('#id-cls-area-comment').val() ? "" : $('#id-cls-area-comment').val())
            },
            success: function (response) {
                $('#id-md-fotomontaje .cls-loading').html('');
                $('#id-download-word').attr('href', Url.Xplora + 'Descarga/WFUnacem?vdoc=' + response).show();
            }
        });
    }
});

//Abre el modal para ingresar el presupuesto, el monto y el codigo
$(document).on('click', '.cls-a-pre-presu', function (e) {
    $('.objrequeridpre').removeClass('has-error');
    $('#id-hd-monto-cpp').val('');
    $('#id-hd-nroarchivo-cpp').val('');
    $('#id-hd-monto-cpp').val($(this).data('monto'));
    $('#id-hd-nroarchivo-cpp').val($(this).data('codigo'));

    //$('#md-pre-presupuesto .modal-content .modal-footer').remove();
    $('#md-pre-presupuesto #id-cls-pre-presu').val('');
    //$('#md-pre-presupuesto i').remove();
    //$('#md-pre-presupuesto .cls-eliminar').remove();
    var _vid = $(this).data('id');
    var _vdoc = $(this).data('doc');

    if (_vdoc != null && _vdoc != "" && _vdoc != '""') {
        //$('#md-pre-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="' + _vdoc + '" ><i class="fa fa-cloud-download"></i> Pre-Presupuesto</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
        $('#ModalPrePresupuestoDescarga').show();
        $('#ModalPrePresupuestoElimina').show();

        $('#ModalPrePresupuestoDescarga').attr('href', _vdoc);
    } else {
        $('#ModalPrePresupuestoDescarga').hide();
        $('#ModalPrePresupuestoElimina').hide();
    }
    $('#md-pre-presupuesto').data('id', _vid);
    $('#md-pre-presupuesto').modal({ backdrop: 'static' });
});
//Validacion antes de grabar
$(document).on('click', '#btn-mp-grabar-presupuesto', function (e) {
    var vcppmonto = $("#id-hd-monto-cpp").val();
    var vcppnro = $("#id-hd-nroarchivo-cpp").val();
    if (String(vcppmonto.replace(/ /g, "")).length > 0 && String(vcppnro.replace(/ /g, "")).length > 0) {
        $('.objrequeridpre').removeClass('has-error');
        fn_reg_det_prepresu();
    } else {
        $('#id-hd-monto-cpp').val('').focus();
        $('.objrequeridpre').addClass('has-error');   //html('Para desaprobar la solicitud ingresar un comentario.');
    }
});
//Grabar monto de presupuesto y codigo de presupuesto
function fn_reg_det_prepresu() {
    $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html());
    var _vstrcodigo = "0";
    var montocpp = $('#id-hd-monto-cpp').val();

    montocpp = montocpp.replace(/,/g, "");
    $.ajax({
        beforeSend: function (xhr) { $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'RegDetalle_PrePresupuesto',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: $('#md-pre-presupuesto').data('id'),
            __b: montocpp,
            __c: $('#id-hd-nroarchivo-cpp').val(),
            __d: _vstrcodigo
        },
        success: function (a) {
            $('#md-pre-presupuesto .cls-loading').html('');
            alert('Actualizado satisfactoriamente.');
            fnOnClickRefresca();
        },
        error: function (xhr) {
            console.error('[Advertencia]<RegDetalle_PrePresupuesto>: Problemas con el servicio.');
        }
    });
}
//Graba Archivo de Presupuesto y Graba Relacion entre PDV's y Presupuesto
$(document).on('change', '#md-pre-presupuesto #id-cls-pre-presu', function (e) {
    var _vdoc = $('#md-pre-presupuesto #ModalPrePresupuestoDescarga').attr('href');
    var _vid = $('#md-pre-presupuesto').data('id');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);

    fn_carga_archivo('id-cls-pre-presu', _vid, fn_reg_prepresu, _vdoc);

    var _vstrcodigo;
    $('input[name="gchkcodigo"]:checked').each(function () {
        _vstrcodigo += $(this).val() + ',';
    });
    if (_vstrcodigo == "" || _vstrcodigo == null) {
        _vstrcodigo = "0"
    } else {
        _vstrcodigo = _vstrcodigo.substring(0, ((_vstrcodigo.length) - 1));
        _vstrcodigo = _vstrcodigo.substring(9, _vstrcodigo.length);
    }
    fn_asoc_pdv_prepresu(_vstrcodigo, _vdoc);

});
//Metodos para grabar el presupuesto
function fn_reg_prepresu(vid, vdocu) {
    //$('#md-pre-presupuesto .modal-content .modal-footer').remove();
    $('#ModalPrePresupuestoDescarga').hide();
    $('#ModalPrePresupuestoElimina').hide();
    $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforeSend: function (xhr) { $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Reg_PrePresupuesto',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: vdocu
        }, success: function (a) {
            //$('#md-pre-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="" ><i class="fa fa-cloud-download"></i> Pre-Presupuesto</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
            $('#ModalPrePresupuestoDescarga').show();
            $('#ModalPrePresupuestoElimina').show();
            $('#md-pre-presupuesto .cls-loading').html('');
            $('#md-pre-presupuesto #ModalPrePresupuestoDescarga').attr('href', a);
            $('#md-pre-presupuesto #id-cls-pre-presu').val();
            $('.cls-pre-presu-' + vid).html('<img src="' + Url.Xplora + 'Img/logoexcel.png" >').data('doc', a);
        }, error: function (xhr) {
            console.error(xhr);
        }
    });
}
//Metodo para relacionar los PDV's con el Presupuesto
function fn_asoc_pdv_prepresu(vid, vdocu) {
    //$('#md-pre-presupuesto .modal-content .modal-footer').remove();
    $('#ModalPrePresupuestoDescarga').hide();
    $('#ModalPrePresupuestoElimina').hide();
    $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforeSend: function (xhr) { $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Asociacion_PrePresupuesto',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: vdocu
        },
        success: function (a) {
            //$('#md-pre-presupuesto .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="" ><i class="fa fa-cloud-download"></i> Pre-Presupuesto</a></div> </div>');
            $('#ModalPrePresupuestoDescarga').show();
            $('#ModalPrePresupuestoElimina').show();
            $('#md-pre-presupuesto .cls-loading').html('');
            $('#md-pre-presupuesto #ModalPrePresupuestoDescarga').attr('href', a);
            $('#md-pre-presupuesto #id-cls-pre-presu').val();
            $('.cls-pre-presu-' + vid).html('Editar').data('doc', a);
        },
        error: function (xhr) {
            console.log('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
}
//Eliminar archivo de Presupuesto
$(document).on('click', '#md-pre-presupuesto .cls-eliminar', function () {
    var _vid = $('#md-pre-presupuesto').data('id');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);

    fn_reg_prepresu_elimina(_vid, '')
    $('#md-pre-presupuesto #id-cls-pre-presu').val('');
    //$('#md-pre-presupuesto i').remove();
    //$('#md-pre-presupuesto .cls-eliminar').remove();
});
//Metodo para eliminar el archivo de Presupuesto
function fn_reg_prepresu_elimina(vid, vdocu) {
    //$('#md-pre-presupuesto .modal-content .modal-footer').remove();
    $('#ModalPrePresupuestoDescarga').hide();
    $('#ModalPrePresupuestoElimina').hide();
    $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforesend: function (xhr) { $('#md-pre-presupuesto .cls-loading').html($('.cls-rep-loading1').html()); },
        url: 'Reg_PrePresupuesto',
        type: 'post',
        datatype: 'json',
        data: {
            __a: vid,
            __b: vdocu
        },
        success: function (a) {
            $('#md-pre-presupuesto .cls-loading').html('');
            $('#md-pre-presupuesto #ModalPrePresupuestoDescarga').attr('href', a);
            $('#md-pre-presupuesto #id-cls-pre-presu').val();
            $('.cls-pre-presu-' + vid).html('Cargar').data('doc', a);
        },
        error: function (xhr) {
            console.log('[advertencia]<addsolicitud>: problemas con el servicio.');
        }
    });
}

//Aprobacion de Fotomontaje
$(document).on('click', '.cls-sw-preg-fotomont i', function (e) {
    if ($(this).hasClass('cls-active') != true) {
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        var vuser = $(this).data('user');
        var vop = $(this).data('op');
        var vcomentario = $(this).data('comentario');
        var vclase = $(this).data('clase');

        if (vvalor == 'si') {
            //Aprobacion de solicitud
            $(this).parent().children('i').removeClass('cls-active');
            $(this).addClass('cls-active');
            fn_ope_p4_ventas(vid, 1, '', 1, 2, '');
        } else if (vvalor == 'no') {
            if (vcomentario != '') {
                $(this).parent().children('i').removeClass('cls-active');
                $(this).addClass('cls-active');
                fn_ope_p4_ventas(vid, 0, '', 1, 2, '');
            } else {
                fn_modal_comentario(vid, vop, vuser, vcomentario, 1, vclase);
            }
        }
    }
});
//Check para Aprobar o Desaprobar el Fotomontaje
function fn_ope_p4_ventas(_va, _vb, _vc, _vd, _vtipo, _vclase) {
    if (_vclase != '') {
        $("." + _vclase).parent().children('i').removeClass('cls-active');
        $("." + _vclase).addClass('cls-active');
    }
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso4_Ventas',
        type: 'POST',
        dataType: 'json',
        data: { __a: _va, __b: _vb, __c: _vc, __d: _vd },
        success: function (a) {
            if (_vd == 1) {
                $('.cls-fec_apro-fotomon_' + _va).html(a._c);
                $('#id-comen-fotomontaje_' + _va).html((String(a._d).length > 5 ? "<a>Ver</a>" : a._d)).data('comentario', a._d);

                if (_vtipo == 1) {
                    fn_ope_p4_ventas(_va, 0, '', 1, 2, _vclase);
                }
            }
        },
        error: function (xhr) {
            console.error('[Advertencia]<AprobacionSolicitud>: Problemas con el servicio.');
        }
    });
}
//Metodo para Grabar el Comentario de Fotomontaje
$(document).on('click', '#id-btn-apfotomon-grabar', function (e) {
    var _vtipo = $("#id-tipo").val();
    var _vclase = $("#id-clase").val();
    var _vid = $('#id-md-apsolicitud-comen #id-numreg').val();
    var _texto = $("#id-ta-comentario").val();
    var _op = $("#id-op").val();

    if (_op == 1) {
        $(".cls-sw-preg-solicitud i").data('comentario', _texto);
    } else if (_op == 2) {
        $(".cls-sw-preg-fotomont i").data('comentario', _texto);
    }

    fn_ope_p4_ventas(_vid, 2, _texto, 1, _vtipo, _vclase);

    $('#id-md-apsolicitud-comen').modal('hide');

});

//Abre Modal para subir la Carta de Fotomontaje
$(document).on('click', '.cls-crg-carta', function (e) {
    //$('#id-md-carta-fotomont .modal-content .modal-footer').remove();
    $('#ModalCartaFotomontajeDescarga').hide();
    $('#ModalCartaFotomontajeElimina').hide();
    $('#id-md-carta-fotomont #id-file-up-carta-fotomon').val('');
    //$('#id-md-carta-fotomont i').remove();
    //$('#id-md-carta-fotomont .cls-eliminar').remove();
    var _vid = $(this).data('idreg');
    var _vdoc = $(this).data('doc');
    if (_vdoc != null && _vdoc != "") {
        //$('#id-md-carta-fotomont .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" onclick="mostrarPDF(\'' + Url.Xplora + _vdoc + '\');"  ><i class="fa fa-cloud-download"></i> Carta aceptación</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
        $('#ModalCartaFotomontajeDescarga').show();
        $('#ModalCartaFotomontajeElimina').show();
        $('#ModalCartaFotomontajeDescarga').attr('onclick', 'mostrarPDF(\'' + Url.Xplora + _vdoc + '\');');
        $('#id-md-carta-fotomont').data('doc', _vdoc);
    } else {
        $('#ModalCartaFotomontajeDescarga').hide();
        $('#ModalCartaFotomontajeElimina').hide();
    }
    $('#id-md-carta-fotomont').data('codigo', _vid);
    $('#id-md-carta-fotomont').modal({ backdrop: 'static' });
});
//Metodo al escoger la Carta de Fotomontaje
$(document).on('change', '#id-file-up-carta-fotomon', function (e) {
    var _vid = $('#id-md-carta-fotomont').data('codigo');
    _vid = (_vid == null || _vid == "" ? 0 : _vid);

    var _vdoc = e.target.files[0].toString();

    fn_carga_archivo('id-file-up-carta-fotomon', _vid, fn_reg_carta, _vdoc);
});
//Metodo para guardar la ruta del archivo en el servidor
function fn_reg_carta(vid, vdocu) {
    //$('#id-md-carta-fotomont .modal-content .modal-footer').remove();
    $('#ModalCartaFotomontajeDescarga').hide();
    $('#ModalCartaFotomontajeElimina').hide();
    $('#id-md-carta-fotomont .cls-loading').html($('.cls-rep-loading1').html());
    $.ajax({
        beforeSend: function (xhr) {
            $('#id-md-carta-fotomont .cls-loading').html($('.cls-rep-loading1').html());
        },
        url: 'Reg_Paso4_Ventas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: 1,
            __c: vdocu,
            __d: 2
        },
        success: function (a) {
            //$('#id-md-carta-fotomont .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" onclick="mostrarPDF(\'' + Url.Xplora + a._c + '\');" ><i class="fa fa-cloud-download"></i> Carta aprobación</a></div>  <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
            $('#ModalCartaFotomontajeDescarga').show();
            $('#ModalCartaFotomontajeElimina').show();
            $('#ModalCartaFotomontajeDescarga').attr('onclick', 'mostrarPDF(\'' + Url.Xplora + a._c + '\');');
            $('#id-md-carta-fotomont .cls-loading').html('');
            $('#id-md-carta-fotomont').data('doc', a._c);

            $('#id-md-carta-fotomont #id-cls-pre-presu').val();

            $('#id-crg-carta-' + vid).html('<img src="' + Url.Xplora + 'Img/ipdf.png" >').data('doc', a._c);
            fn_ope_p4_ventas(vid, 1, '', 1, 2, 'cls-fotosi-' + vid);
        },
        error: function (xhr) {
            console.error('[Advertencia]<addSolicitud>: Problemas con el servicio.');
        }
    });
}
//Metodo para Eliminar la Carta de Fotomontaje
$(document).on('click', '#id-md-carta-fotomont .cls-eliminar', function (e) {
    var _vid = $('#id-md-carta-fotomont').data('codigo');
    var _vdoc = $('#id-md-carta-fotomont').data('doc');
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso4_Ventas',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _vid,
            __b: 2,
            __c: _vdoc,
            __d: 2
        },
        success: function (a) {
            $('#id-file-up-carta-fotomon').parent().children('span').html('');
            //$('#id-md-carta-fotomont .modal-content .modal-footer').remove();
            $('#ModalCartaFotomontajeDescarga').hide();
            $('#ModalCartaFotomontajeElimina').hide();
            $('#id-md-carta-fotomont').data('doc', '');
            $('#id-md-carta-fotomont #id-file-up-carta-fotomon').val('');
            $('#id-crg-carta-' + _vid).html('Cargar').data('doc', '');
        },
        error: function (xhr) {
            console.error('[Advertencia]<Eli Car. Fotomon>: Problemas con el servicio.');
        }
    });
});

//Aprueba o Desaprueba Pre-Presupuesto
$(document).on('click', '.cls-sw-pre-presu i', function (e) {
    if ($(this).hasClass('cls-active') != true) {
        $(this).parent().children('i').removeClass('cls-active');
        $(this).addClass('cls-active');
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        if (vvalor == 'si') {
            fn_ope_p5_trade(vid, '', '', '', 1, 1);
        } else if (vvalor == 'no') {
            fn_ope_p5_trade(vid, '', '', '', 0, 1);
        }
    }
});
//Funcion para Aprobar o Desaprobar el Presupuesto E Ingrsar Orden de Compra, HES y comentario
function fn_ope_p5_trade(_va, _vb, _vc, _vd, _ve, _vf) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso5_Trade',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc, __d: _vd, __e: _ve, __f: _vf
        },
        success: function (a) {
            if (_vf == 1) {
                $('.cls-fec-apro-prepresu-' + _va).html(a);
            }
            if (_vf == 2) {
                if (($.trim(_vb)).length > 13) {
                    $('.cls-datos-p5 .cls-1-' + _va).parent().attr("title", ($.trim(_vb)));
                    $('.cls-datos-p5 .cls-1-' + _va).html(($.trim(_vb)).substr(0, 10) + "...");
                } else {
                    $('.cls-datos-p5 .cls-1-' + _va).parent().removeAttr("title");
                    $('.cls-datos-p5 .cls-1-' + _va).html($.trim(_vb));
                }

                if (($.trim(_vc)).length > 13) {
                    $('.cls-datos-p5 .cls-2-' + _va).parent().attr("title", ($.trim(_vc)));
                    $('.cls-datos-p5 .cls-2-' + _va).html(($.trim(_vc)).substr(0, 10) + "...");
                } else {
                    $('.cls-datos-p5 .cls-2-' + _va).parent().removeAttr("title");
                    $('.cls-datos-p5 .cls-2-' + _va).html($.trim(_vc));
                }

                if (($.trim(_vd)).length > 24) {
                    $('.cls-datos-p5 .cls-3-' + _va).parent().attr("title", ($.trim(_vd)));
                    $('.cls-datos-p5 .cls-3-' + _va).html(($.trim(_vd)).substr(0, 21) + "...");
                } else {
                    $('.cls-datos-p5 .cls-3-' + _va).parent().removeAttr("title");
                    $('.cls-datos-p5 .cls-3-' + _va).html($.trim(_vd));
                }

                $('.cls-datos-p5 .cls-1-' + _va).parent().data('oc', $.trim(_vb)).data('hes', $.trim(_vc)).data('comentario', $.trim(_vd));
                $('.cls-datos-p5 .cls-2-' + _va).parent().data('oc', $.trim(_vb)).data('hes', $.trim(_vc)).data('comentario', $.trim(_vd));
                $('.cls-datos-p5 .cls-3-' + _va).parent().data('oc', $.trim(_vb)).data('hes', $.trim(_vc)).data('comentario', $.trim(_vd));

                //$('.cls-datos-p5-OC-' + _va).data('oc', $.trim(_vb));
                //$('.cls-datos-p5-HES-' + _va).data('hes', $.trim(_vc));
                //$('.cls-datos-p5-coment' + _va).data('comentario', $.trim(_vd));

                $('#id-md-p5-trade').modal('hide');
            }
        },
        error: function (xhr) {
            console.error('[Advertencia]<p5>: Problemas con el servicio.');
        }
    });
}

//Abre Modal para Ingresar Orden de Compra, HES y Comentario
$(document).on('click', '.cls-datos-p5', function (e) {
    var _vid = $(this).data('id');
    var _comentario_trade = $(this).data('comentario');
    var _OC_trade = $(this).data('oc');
    var _HES_trade = $(this).data('hes');
    var _code_trade = $(this).data('cod');
    $('#txt-ord-compra').val('');
    $('#txt-Hes').val('');
    $('#txt-comen-p5').val('');

    //$('#txt-ord-compra').val($('.cls-datos-p5 .cls-1-' + _vid).html());
    //$('#txt-Hes').val($('.cls-datos-p5 .cls-2-' + _vid).html());
    $('#txt-ord-compra').val(_OC_trade);
    $('#txt-Hes').val(_HES_trade);
    $('#txt-comen-p5').val(_comentario_trade);

    $('#id-md-p5-trade').data('id', _vid);

    //$("#id-md-p5-trade #id-btn-p5-grabar").css('visibility', 'visible');
    //$('#id-md-p5-trade table td:eq(0)').css('visibility', 'visible');
    //$('#id-md-p5-trade table td:eq(1)').css('visibility', 'visible');
    //$('#id-md-p5-trade table td:eq(2)').css('visibility', 'visible');
    //$('#id-md-p5-trade table td:eq(3)').css('visibility', 'visible');

    $('#id-btn-p5-grabar').show();
    //$('#id-md-p5-trade .modal-content .modal-footer').show();

    $('#id-md-p5-trade #txt-ord-compra').prop('disabled', false);
    $('#id-md-p5-trade #txt-Hes').prop('disabled', false);
    $('#id-md-p5-trade #txt-comen-p5').prop('disabled', false);
    if (_code_trade == "2") {
        $('#id-md-p5-trade #txt-ord-compra').prop('disabled', true);
        $('#id-md-p5-trade #txt-Hes').prop('disabled', true);
        $('#id-md-p5-trade #txt-comen-p5').prop('disabled', true);

        //$('#id-md-p5-trade .modal-content .modal-footer').hide();
        $('#id-btn-p5-grabar').hide();
        //$('#id-md-p5-trade .modal-content .modal-footer').remove();

    }

    $('#id-md-p5-trade').modal({ backdrop: 'static' });
});
//Click para Grabar Orden de Compra, HES y Comentario
$(document).on('click', '#id-btn-p5-grabar', function (e) {
    var _vid = $('#id-md-p5-trade').data('id');
    var _a = $('#txt-ord-compra').val();
    _a = (_a == null ? "" : _a);
    var _b = $('#txt-Hes').val();
    _b = (_b == null ? "" : _b);
    var _c = $('#txt-comen-p5').val();
    _c = (_c == null ? "" : _c);
    fn_ope_p5_trade(_vid, _a, _b, _c, 0, 2);
});


//Modal para Status Documentario
$(document).on('click', '.cls-ver-docs', function (e) {
    $('.mask').inputmask();
    var _id = $(this).data('id');
    $('#id-md-st-doc-lucky').data('id', _id);
    $('.cls-cl-estado').html('').removeClass('cls-ok').removeClass('cls-pendiente').addClass('cls-pendiente').html('PENDIENTE');
    $('.mask').val('');
    $('.mask').inputmask();
    //$('#id-md-st-doc-lucky table td .pull-right i').hide().data('url', '');
    $('#id-md-st-doc-lucky table td div span i').hide().data('url', '');
    $('.cls-f-doc').val('');
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_LuckyAnalis',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _id, __b: '', __c: '', __d: 0, __e: 2, __f: ''
        },
        success: function (a) {
            var _vcount = 0,
                _vfechaprog,
                _varrayfechaprog = [];
            $.each(a, function (k, v) {
                if (v._c != null && v._c != "") {
                    $('.cls-estado-' + v._b).removeClass('cls-pendiente').addClass('cls-ok').html('OK');
                    $('#id-pdf-doc-' + v._b).show();
                    $('#id-pdf-doc-' + v._b).data('url', v._c);
                    $('#id-btn-doc-' + v._b).data('doc', v._c);
                    _vcount++;
                }
                if (v._e != '' && v._e != null) {

                    _varrayfechaprog = (v._e).split("/")
                    _vfechaprog = _varrayfechaprog[2] + '-' + _varrayfechaprog[1] + '-' + _varrayfechaprog[0];
                } else {
                    _varrayfechaprog = [];
                    _vfechaprog = '';
                }
                $('#dt-doc-' + v._b).val(_vfechaprog);

                $('#nom-doc-' + v._b).val(v._h);
            });

            var _vcolor = '';
            if (_vcount == 0) {
                _vcolor = '#F47173';
            } else if (_vcount > 0 && _vcount < 10) {
                _vcolor = '#FEDF78';
            } else if (_vcount == 10) {
                _vcolor = '#5DBB77';
            }

            $('.cls-ver-doc-' + _id + ' i').css('color', _vcolor);

            $('#id-md-st-doc-lucky').modal({ backdrop: 'static' });
        },
        error: function (xhr) {
            console.error('[Advertencia]<p6>: Problemas con el servicio.');
        }
    });
});

//Click para grabar Documentos en el Status Documentario
$(document).on('click', '.cls-cg-fl-doc', function (e) {
    var _orden = $(this).data('orden'),
        _id = 'id-fl-doc-' + _orden,
        _fecha = $('#dt-doc-' + _orden).val(),
        _Arrayfecha = [],
        _vfecha_final;
    if (_fecha != '') {
        _Arrayfecha = _fecha.split("-");
        _vfecha_final = _Arrayfecha[2] + '/' + _Arrayfecha[1] + '/' + _Arrayfecha[0];
    } else {
        _Arrayfecha = [];
        _vfecha_final = '';
    }
    alert(_vfecha_final);
    //con input date se envias asi yyyy-mm-dd
    //con input date debe enviarse asi dd/mm/yyyy
    var _nombre = $('#nom-doc-' + _orden).val();
    var id = $('#id-md-st-doc-lucky').data('id');
    var vdoc = $(this).data('doc');
    fn_carga_archivo2(_id, id, fn_ope_p6_LuckyAnalis, vdoc, 1, { "fecha": _vfecha_final, "orden": _orden, "op": 1, "nombre": _nombre });
});
//Funcion para Grabar los Documentos del Status Documentario
function fn_ope_p6_LuckyAnalis(_vid, _vfoto, _vparametros) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_LuckyAnalis',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _vid, __b: _vfoto, __c: _vparametros.fecha, __d: _vparametros.orden, __e: _vparametros.op, __f: _vparametros.nombre
        },
        success: function (a) {
            if (_vparametros.op == 1) {
                $.each(a, function (k, v) {
                    if (v._c != null && v._c != "") {
                        $('.cls-estado-' + v._b).removeClass('cls-pendiente').addClass('cls-ok').html('OK');
                        $('#id-pdf-doc-' + v._b).show();
                        $('#id-pdf-doc-' + v._b).data('url', v._c);
                        _vcount++;
                    } else {
                        $('.cls-estado-' + v._b).removeClass('cls-pendiente').removeClass('cls-ok').addClass('cls-pendiente').html('Pendiente');
                        $('#id-pdf-doc-' + v._b).data('url', '').hide();
                    }
                });

                var _vcount = 0;
                $("#id-md-st-doc-lucky .cls-cl-estado").each(function (index) {
                    if ($(this).html() == 'OK') { _vcount++; }
                });

                var _vcolor = '';
                if (_vcount == 0) { _vcolor = '#F47173'; }
                else if (_vcount > 0 && _vcount < 10) { _vcolor = '#FEDF78'; }
                else if (_vcount == 10) { _vcolor = '#5DBB77'; }

                $('.cls-ver-doc-' + _vid + ' i').css('color', _vcolor);
                alert('.:: Registro guardado con exito ::.');
            }
            if (_vparametros.op == 2) {
                alert(a);
            }
        },
        error: function (xhr) {
            console.log('[Advertencia]<p6>: Problemas con el servicio.');
        }
    });

}
//Muestra Pdf de Status Documentario
function fn_mdoc(vid) {
    var _vdoc = $('#' + vid).data('url');

    if (_vdoc == null || _vdoc.length == 0) return;

    mostrarPDF(_vdoc);
}
//Funcion para Guardar el Documento del Status Documentario en el Servidor
function fn_carga_archivo2(id, vop, functioncalback, vdoc, vsw, vparametros) {
    console.log('2');
    if ($("#" + id).val()) {
        if (vdoc == null) {
            vdoc = "";
        }
        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                var _nomfoto = String(a);
                functioncalback(vop, _nomfoto, vparametros);
            },
            'json'
        );
    } else {
        functioncalback(vop, '', vparametros);
    }
}

//Metodo al elegir el Tipo de Tramite
$(document).on('change', '.cls-tipotramite', function (e) {
    var _vid = $(this).data('id');
    var _a = this.value;
    _a = (_a == null ? "" : _a);

    fn_ope_p6_lucky_tipotramite(5, _a, _vid);
});
function fn_ope_p6_lucky_tipotramite(_va, _vb, _vc) {

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_Lucky_TipoTramite',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc
        },
        success: function (a) {
            alert("Registro actualizado con exito!");
        },
        error: function (xhr) {
            console.log('[Advertencia]<p3>: Problemas con el servicio.');
        }
    });
}

//Metodo al elegir el Tiempo de permiso
$(document).on('change', '.cls-tiempopermiso', function (e) {
    var _vid = $(this).data('id');
    var _a = this.value;
    _a = (_a == null ? "" : _a);

    fn_ope_p6_lucky_tiempopermiso(6, _a, _vid);
});
function fn_ope_p6_lucky_tiempopermiso(_va, _vb, _vc) {

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso6_Lucky_TipoPermiso',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _va, __b: _vb, __c: _vc
        },
        success: function (a) {
            alert("Registro actualizado con exito!");
        },
        error: function (xhr) {
            console.log('[Advertencia]<p3>: Problemas con el servicio.');
        }
    });
}

//Click para el ingreso de Documentos de LuckyTramite
$(document).on('click', '.cls-p7-tramite', function (e) {
    $('#txt-fec-ing-expe').val('');
    $('#txt-entre-licencia').val('');
    $('#txt-venc-licencia').val('');
    $('.mask').inputmask();
    $('#fl-tra-f1').data('doc', '').val('');
    $('#fl-tra-f2').data('doc', '').val('');
    var _id = $(this).data('id');
    $('#id-md-p7-lucky-tramite').data('id', _id);
    $('#id-md-p7-lucky-tramite').data('op', 1);
    $('#btn-grabar-tramite').html("Grabar");
    $('#id-md-p7-lucky-tramite').modal({ backdrop: 'static' });
    fn_ope_p7_LuckyTramite(_id, 2);
});
//Funcion para Mostrar los registro de LuckyTramite
function fn_ope_p7_LuckyTramite(vid, vop) {

    var _a = "",
        _b = "",
        _c = "",
        _d = "",
        _e = "",
        _f = "",
        _g = "",
        _h = "",
        _i = "",
        _ving_expe,
        _vArraying_expe = [],
        _ventre_licencia,
        _vArrayentre_licencia = [],
        _vvenc_licencia,
        _vArrayvenc_licencia = [];

    if (vop == 1 || vop == 7) {
        _a = $('#txt-fec-ing-expe').val();
        _b = $('#fl-tra-f1').data('doc');
        _c = $('#txt-obs-ing-expe').val();
        _d = $('#txt-entre-licencia').val();
        _e = $('#txt-venc-licencia').val();
        _f = $('#fl-tra-f2').data('doc');
        _g = $('#txt-monto-pto-final').val();
        _h = $('#txt-nroarchivo-final').val();
        _i = $('#fl-tra-f3').data('doc');

        //Fecha de Ingreso de Expediente
        if (_a != '' && _a != null) {
            _vArraying_expe = _a.split("-");
            _ving_expe = _vArraying_expe[2] + '/' + _vArraying_expe[1] + '/' + _vArraying_expe[0];
        } else {
            _vArraying_expe = [];
            _ving_expe = '';
        }
        //Fecha de Entrega de Permiso
        if (_d != '' && _d != null) {
            _vArrayentre_licencia = _d.split("-");
            _ventre_licencia = _vArrayentre_licencia[2] + '/' + _vArrayentre_licencia[1] + '/' + _vArrayentre_licencia[0];
        } else {
            _vArrayentre_licencia = [];
            _ventre_licencia = '';
        }
        //Fecha de Vencimiento de Permiso
        if (_e != '' && _e != null) {
            _vArrayvenc_licencia = _e.split("-");
            _vvenc_licencia = _vArrayvenc_licencia[2] + '/' + _vArrayvenc_licencia[1] + '/' + _vArrayvenc_licencia[0];
        } else {
            _vArrayvenc_licencia = [];
            _vvenc_licencia = '';
        }

        if (_g != '' && _g != 'undefined' && _g != null) {
            _g = _g.replace(",", "");
        }
    }
    //return false;

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso7_LuckyTramite',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            //__b: _a,
            __b: _ving_expe,
            __c: _b,
            __d: _c,
            //__e: _d,
            __e: _ventre_licencia,
            //__f: _e,
            __f: _vvenc_licencia,
            __g: _f,
            __h: vop,
            __i: _g,
            __j: _h,
            __k: _i
        },
        success: function (a) {
            if (a == null) return;

            _ving_expe = '',
            _vArraying_expe = [],
            _ventre_licencia = '',
            _vArrayentre_licencia = [],
            _vvenc_licencia = '',
            _vArrayvenc_licencia = []
            if (vop == 1) {
                if (a._a == null || a._a == '') {
                    $('#id-td-fec_ing_expe-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    if (a._b == null || a._b == '') {
                        $('#id-td-fec_ing_expe-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        $('#id-td-fec_ing_expe-' + vid).html(a._a);
                    }
                }
                if (a._b == null || a._b == '') {
                    $('#id-td-reg-tramite-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-reg-tramite-' + vid).html('<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" / >');
                }
                if (a._c == null || a._c == '') {
                    $('#id-td-obs-tramite-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-obs-tramite-' + vid).html(a._c);
                }
                if (a._d == null || a._d == '') {
                    $('#id-td-fec_ent-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    if (a._f == null || a._f == '') {
                        $('#id-td-fec_ent-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        $('#id-td-fec_ent-licen-' + vid).html(a._d);
                    }
                }
                if (a._e == null || a._e == '') {
                    $('#id-td-fec_cadu-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    if (a._f == null || a._f == '') {
                        $('#id-td-fec_cadu-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        alert(a._e)
                        $('#id-td-fec_cadu-licen-' + vid).html(a._e);
                    }
                }
                if (a._f == null || a._f == '') {
                    $('#id-td-url-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-url-licen-' + vid).html('<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" / >');
                }
                if (a._h == null || a._h == '') {
                    $('#id-td-monto-pto-final-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-monto-pto-final-' + vid).html(a._h);
                }
                if (a._i == null || a._i == '') {
                    if (a._j == null || a._j == '') {
                        $('#id-td-url-pto-final-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        $('#id-td-url-pto-final-' + vid).html('<img src="' + Url.Xplora + 'Img/logoexcel.png"  class="cls-img-td" / >');
                    }
                } else {
                    if (a._j == null || a._j == '') {
                        $('#id-td-url-pto-final-' + vid).html('<span class="label label-success" style="font-size: 100% !Important;" title="' + a._i + '">' + a._i + '</span>');
                    } else {
                        $('#id-td-url-pto-final-' + vid).html('<img src="' + Url.Xplora + 'Img/logoexcel.png"  class="cls-img-td" / ><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + a._i + '">' + a._i + '</span>');
                    }
                }
                if (a._e != null && a._e != '' && a._f != null && a._f != '') {
                    $('#id-renovacion-' + vid).empty();

                    $('#id-renovacion-' + vid).append('<a class="cls-p7-renovacion" style="text-decoration:underline;" data-id="' + vid + '" data-fec-ingreso-expe="' + a._a + '" data-url-rec-tramite="' + a._b + '" data-obs_tramite="' + a._c + '" data-fec-entre-licencia="' + a._d + '" data-fec-cadu-licencia="' + a._e + '" data-url-licencia="' + a._f + '" data-p3-monto-presupuesto-final="' + a._h + '" data-p3-codigo-presupuesto-final="' + a._i + '" data-url-presu-final="' + a._j + '">Renovar</a>')
                }
                alert('.:: Datos registrados con exito ::.');
            } else if (vop == 2) {
                if (a._a != '' && a._a != null) {
                    _vArraying_expe = a._a.split("/");
                    _ving_expe = _vArraying_expe[2] + '-' + _vArraying_expe[1] + '-' + _vArraying_expe[0];
                } else {
                    _vArraying_expe = [];
                    _ving_expe = '';
                }
                $('#txt-fec-ing-expe').val(_ving_expe);
                if (a._b != null && a._b != "") {
                    $('#sp-fl1').show().data('url', a._b);
                    $('#td-est-fl1').html('Cargado').css('color', 'blue');
                    $('#eli-fl-tra-f1').show();

                } else {
                    $('#sp-fl1').hide().data('url', '');
                    $('#td-est-fl1').html('Pendiente').css('color', 'red');
                    $('#eli-fl-tra-f1').hide();
                    $('#fl-tra-f1').hide();
                }
                $('#txt-obs-ing-expe').val(a._c);
                if (a._d != '' && a._d != null) {
                    _vArrayentre_licencia = a._d.split("/");
                    _ventre_licencia = _vArrayentre_licencia[2] + '-' + _vArrayentre_licencia[1] + '-' + _vArrayentre_licencia[0];
                } else {
                    _vArrayentre_licencia = [];
                    _ventre_licencia = '';
                }
                $('#txt-entre-licencia').val(_ventre_licencia);
                if (a._e != '' && a._e != null) {
                    _vArrayvenc_licencia = a._e.split("/");
                    _vvenc_licencia = _vArrayvenc_licencia[2] + '-' + _vArrayvenc_licencia[1] + '-' + _vArrayvenc_licencia[0];
                } else {
                    _vArrayvenc_licencia = [];
                    _vvenc_licencia = '';
                }
                $('#txt-venc-licencia').val(_vvenc_licencia);
                if (a._f != null && a._f != "") {
                    $('#sp-fl2').show().data('url', a._f);
                    $('#td-est-fl2').html('Cargado').css('color', 'blue');
                    $('#eli-fl-tra-f2').show();
                } else {
                    $('#sp-fl2').hide().data('url', '');
                    $('#td-est-fl2').html('Pendiente').css('color', 'red');
                    $('#eli-fl-tra-f2').hide();
                    $('#fl-tra-f2').hide();
                }
                $('#txt-monto-pto-final').val(a._h);
                $('#txt-nroarchivo-final').val(a._i);
                if (a._j != null && a._j != "") {
                    $('#sp-fl3').show().data('url', a._j);
                    $('#eli-fl-tra-f3').show();
                } else {
                    $('#sp-fl3').hide().data('url', '');
                    $('#eli-fl-tra-f3').hide();
                    $('#fl-tra-f3').hide();
                }
            } else if (vop == 3) {
                alert('.: Recibo eliminado con exito :.');
                $('#sp-fl1').hide().data('url', '');
                $('#td-est-fl1').html('Pendiente').css('color', 'red');
                $('#eli-fl-tra-f1').hide();
                $("#fl-tra-f1").val("");
                $('#fl-tra-f1').hide();
                $('#fl-tra-f1').parent().children('span').html('');
            } else if (vop == 4) {
                alert('.: Licencia eliminado con exito :.');
                $('#sp-fl2').hide().data('url', '');
                $('#td-est-fl2').html('Pendiente').css('color', 'red');
                $('#eli-fl-tra-f2').hide();
                $("#fl-tra-f2").val("");
                $('#fl-tra-f2').hide();
                $('#fl-tra-f2').parent().children('span').html('');

            } else if (vop == 5) {
                $('#txt-fec-ing-expe').val("");

                $('#sp-fl1').hide().data('url', '');
                $('#td-est-fl1').html('Pendiente').css('color', 'red');
                $('#eli-fl-tra-f1').hide();
                $('#fl-tra-f1').hide();

                $('#txt-obs-ing-expe').val("");
                $('#txt-entre-licencia').val("");
                $('#txt-venc-licencia').val("");

                $('#sp-fl2').hide().data('url', '');
                $('#td-est-fl2').html('Pendiente').css('color', 'red');
                $('#eli-fl-tra-f2').hide();
                $('#fl-tra-f2').hide();

                $('#txt-monto-pto-final').val("");
                $('#txt-nroarchivo-final').val("");

                $('#sp-fl3').hide().data('url', '');
                $('#eli-fl-tra-f3').hide();
                $('#fl-tra-f3').hide();
            } else if (vop == 6) {
                alert('.: Archivo Presupuesto eliminado con exito :.');
                $('#sp-fl3').hide().data('url', '');
                $('#eli-fl-tra-f3').hide();
                $("#fl-tra-f3").val("");
                $('#fl-tra-f3').hide();
                $('#fl-tra-f3').parent().children('span').html('');

            } else if (vop == 7) {
                if (a._a == null || a._a == '') {
                    $('#id-td-fec_ing_expe-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-fec_ing_expe-' + vid).html(a._a);
                }
                if (a._b == null || a._b == '') {
                    $('#id-td-reg-tramite-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-reg-tramite-' + vid).html('<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" / >');
                }
                if (a._c == null || a._c == '') {
                    $('#id-td-obs-tramite-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-obs-tramite-' + vid).html(a._c);
                }
                if (a._d == null || a._d == '') {
                    $('#id-td-fec_ent-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-fec_ent-licen-' + vid).html(a._d);
                }
                if (a._e == null || a._e == '') {
                    $('#id-td-fec_cadu-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-fec_cadu-licen-' + vid).html(a._e);
                }
                if (a._f == null || a._f == '') {
                    $('#id-td-url-licen-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-url-licen-' + vid).html('<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" / >');
                }
                if (a._h == null || a._h == '') {
                    $('#id-td-monto-pto-final-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                } else {
                    $('#id-td-monto-pto-final-' + vid).html(a._h);
                }
                if (a._i == null || a._i == '') {
                    if (a._j == null || a._j == '') {
                        $('#id-td-url-pto-final-' + vid).html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").css({ "text-decoration": "underline", "color": "#3498db" });
                    } else {
                        $('#id-td-url-pto-final-' + vid).html('<img src="' + Url.Xplora + 'Img/logoexcel.png"  class="cls-img-td" / >');
                    }
                } else {
                    if (a._j == null || a._j == '') {
                        $('#id-td-url-pto-final-' + vid).html('<span class="label label-success" style="font-size: 100% !Important;" title="' + a._i + '">' + a._i + '</span>');
                    } else {
                        $('#id-td-url-pto-final-' + vid).html('<img src="' + Url.Xplora + 'Img/logoexcel.png"  class="cls-img-td" / ><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + a._i + '">' + a._i + '</span>');
                    }
                }
                $('#id-renovacion-' + vid).empty();
                if (a._e != null && a._e != '' && a._f != null && a._f != '') {
                    $('#id-renovacion-' + vid).empty();

                    $('#id-renovacion-' + vid).append('<a class="cls-p7-renovacion" style="text-decoration:underline;" data-id="' + vid + '" data-fec-ingreso-expe="' + a._a + '" data-url-rec-tramite="' + a._b + '" data-obs_tramite="' + a._c + '" data-fec-entre-licencia="' + a._d + '" data-fec-cadu-licencia="' + a._e + '" data-url-licencia="' + a._f + '" data-p3-monto-presupuesto-final="' + a._h + '" data-p3-codigo-presupuesto-final="' + a._i + '" data-url-presu-final="' + a._j + '">Renovar</a>')
                }

                alert('.: Archivo renovado con exito, los datos anteriores están en el Historico :.');
            }
        },
        error: function (xhr) {
            console.error('[Advertencia]<p7>: Problemas con el servicio.');
        }
    });
}
//Metodo al cargar el Presupuesto Final de LuckyTramites
$(document).on('change', '.cls-tra-file', function (e) {
    var _vid = $(this).attr('id');
    var _vdoc = $(this).data('doc');
    fn_carga_archivo_tramite(_vid, _vdoc);
});
//Funcion para Cargar el Excel Presupuesto Final, PDF Recibo y PDF Permiso al servidor
function fn_carga_archivo_tramite(id, vdoc) {
    //console.log('3-' + vdoc);
    if ($("#" + id).val()) {
        if (vdoc == null) {
            vdoc = "";
        }
        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                var _nomfoto = String(a);
                $("#" + id).data('doc', _nomfoto);
                if (_nomfoto != null && _nomfoto != "") {
                    $('#' + id).parent().children('span').html('<i class="fa fa-check-circle" style="color:#70AD47;"></i>');

                    if (id == 'fl-tra-f1') {

                    } else if (id == 'fl-tra-f2') {

                    }
                }
            },
            'json'
        );

    } else {
        //alert("No selecciono ningún archivo...");
    }
}
//Funcion para ver el PDF del presupuesto Final de LuckyTramite
function fn_mdoc2(vid) {
    //var _vurl = $('#' + vid + ' i').data('url');
    var _vurl = $('#' + vid).data('url');
    mostrarPDF(_vurl);
}
//Click para Grabar los registros de LuckyTramite
$(document).on('click', '#btn-grabar-tramite', function (e) {
    var _vid = $('#id-md-p7-lucky-tramite').data('id'),
        _vop = $('#id-md-p7-lucky-tramite').data('op');
    if (_vop == 2) {
        fn_ope_p7_LuckyTramite(_vid, 7);
    } else if (_vop == 1) {
        fn_ope_p7_LuckyTramite(_vid, 1);
    }
});
//Click para eliminar archivos de LuckyTramite
$(document).on('click', '#id-md-p7-lucky-tramite .cls-eliminar', function () {
    var _vid = $('#id-md-p7-lucky-tramite').data('id');
    var _id = $(this).attr("id");
    if (_id == 'eli-fl-tra-f1') {
        var opc_arch = 3;
    } else if (_id == 'eli-fl-tra-f2') {
        var opc_arch = 4;
    } else if (_id == 'eli-fl-tra-f3') {
        var opc_arch = 6;
    }

    fn_ope_p7_LuckyTramite(_vid, opc_arch);
});

//Click para Levantar el Modal de LuckyTramite para Renovar
$(document).on('click', '.cls-p7-renovacion', function (e) {
    $('#txt-fec-ing-expe').val('');
    $('#txt-entre-licencia').val('');
    $('#txt-venc-licencia').val('');
    $('.mask').inputmask();
    $('#fl-tra-f1').data('doc', '').val('');
    $('#fl-tra-f2').data('doc', '').val('');
    $('#fl-tra-f3').data('doc', '').val('');

    $('#fl-tra-f3').parent().children('span').html('');
    $('#fl-tra-f2').parent().children('span').html('');
    $('#fl-tra-f1').parent().children('span').html('');

    var _id = $(this).data('id');

    $('#id-md-p7-lucky-tramite').data('id', _id);
    $('#id-md-p7-lucky-tramite').data('op', 2);
    $('#id-md-p7-lucky-tramite').data('fec_ingreso_expe', $(this).data('fec-ingreso-expe'));
    $('#id-md-p7-lucky-tramite').data('url_rec_tramite', $(this).data('url-rec-tramite'));
    $('#id-md-p7-lucky-tramite').data('obs_tramite', $(this).data('obs_tramite'));
    $('#id-md-p7-lucky-tramite').data('fec_entre_licencia', $(this).data('fec-entre-licencia'));
    $('#id-md-p7-lucky-tramite').data('fec_cadu_licencia', $(this).data('fec-cadu-licencia'));
    $('#id-md-p7-lucky-tramite').data('url_licencia', $(this).data('url-licencia'));
    $('#id-md-p7-lucky-tramite').data('p3_monto_presupuesto_final', $(this).data('p3-monto-presupuesto-final'));
    $('#id-md-p7-lucky-tramite').data('p3_codigo_presupuesto_final', $(this).data('p3-codigo-presupuesto-final'));
    $('#id-md-p7-lucky-tramite').data('url_presu_final', $(this).data('url-presu-final'));
    $('#btn-grabar-tramite').html("Renovar");
    $('#id-md-p7-lucky-tramite').modal({ backdrop: 'static' });

    fn_ope_p7_LuckyTramite(_id, 5);
});
//Click para Ver el Historico
$(document).on('click', '.cls-hitorico-renovacion', function (e) {
    //$('#md-historico-renovacion .modal-content .modal-footer').remove();
    var _vid = $(this).data('id');
    var _a = ""; var _b = ""; var _c = ""; var _d = ""; var _e = ""; var _f = ""; var _g = ""; var _h = ""; var _i = "";
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'GetGrillaTramite',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: _vid,
            __b: _a,
            __c: _b,
            __d: _c,
            __e: _d,
            __f: _e,
            __g: _f,
            __h: 8,
            __i: _g,
            __j: _h,
            __k: _i
        },
        success: function (response) {
            if (response.Archivo != null && response.Archivo != "" && response.Archivo.length.toString() != "0") {
                $('#tb-wf-unacem-renovacion tbody').empty();

                var ruta_recibo = '';
                var ruta_licencia = '';
                var ruta_Presupuesto_Final = '';
                $.each(response.Archivo, function (key, value) {

                    if (value.Url_rec_tramite != '' && value.Url_rec_tramite != null) {
                        ruta_recibo = '<img src="' + Url.Xplora + 'Img/ipdf.png" class="cls-img-td" onclick="mostrarPDF(\'' + value.Url_rec_tramite + '\');" />'
                    } else {
                        ruta_recibo = '';
                    }
                    if (value.Url_Licencia != '' && value.Url_Licencia != null) {
                        ruta_licencia = '<img src="' + Url.Xplora + 'Img/ipdf.png" class="cls-img-td" onclick="mostrarPDF(\'' + value.Url_Licencia + '\');" />'
                    } else {
                        ruta_licencia = '';
                    }
                    if (value.Url_Presupuesto_Final != '' && value.Url_Presupuesto_Final != null) {
                        ruta_Presupuesto_Final = '<a href="' + value.Url_Presupuesto_Final + '"><img src="' + Url.Xplora + 'Img/logoexcel.png" class="cls-img-td" /></a>'
                    } else {
                        ruta_Presupuesto_Final = '';
                    }

                    $("#tb-wf-unacem-renovacion tbody").append(
                        '<tr>\
                            <td class="text-center">' + value.Fec_Ingreso_expe + '</td>\
                            <td class="text-center">' + ruta_recibo + '</td>\
                            <td class="text-left">' + value.Obs_Tramite + '</td>\
                            <td class="text-center">' + value.Fec_Entre_Licencia + '</td>\
                            <td class="text-center">' + value.fec_cadu_licencia + '</td>\
                            <td class="text-center">' + ruta_licencia + '</td>\
                            <td class="text-right">' + value.Monto_Presupuesto_Final + '</td>\
                            <td class="text-center">' + value.Codigo_Presupuesto_Final + '</td>\
                            <td class="text-center">' + ruta_Presupuesto_Final + '</td>\
                        </tr>'
                        );
                });
            }
            else {
                $('#tb-wf-unacem-renovacion tbody').empty();
            }

        }
    });

    $('#md-historico-renovacion').data('id', _vid);
    $('#md-historico-renovacion').modal({ backdrop: 'static' });
});

//Click para Abrir Modal para Reporte Fotografico
$(document).on('click', '.cls-a-repfot-presu', function (e) {
    var _vop = $(this).data('op');

    var _vtitulo = "Carga presupuesto final";
    var _vbtn = "Presupuesto";
    if (_vop == 1) {
        _vtitulo = "Carga reporte fotografico"; $('#id-fl-repfot-presu').attr('accept', '.jpg,.png'); _vbtn = "Rep. Fotografico";

        $(".objrequeridprefinal").css("display", "none");
    } else {
        $('#id-fl-repfot-presu').attr('accept', '.xls,.xlsx');

        $(".objrequeridprefinal").css("display", "block");

        $('#id-hd-monto-cpf').val($(this).data('monto'));
        $('#id-hd-nroarchivo-cpf').val($(this).data('codigo'));
    }
    var _vdoc = $(this).data('doc');
    var _vid = $(this).data('id');
    $('#id-fl-repfot-presu').parent().children('span').html('');
    $('#id-fl-repfot-presu').val('');
    $('#id-md-p7-repfot-presu .panel-heading h5').html('<a href="javascript:"><i class="fa fa-cloud-upload"></i> ' + _vtitulo + '</a>');
    $('#id-md-p7-repfot-presu').data('doc', Url.Xplora + _vdoc).data('id', _vid).data('op', _vop);
    ////$('#id-md-p7-repfot-presu .modal-footer .pull-right').html('');

    //$('#id-md-p7-repfot-presu .modal-content .modal-footer').remove();
    $('#ModalReporteFotograficoDescarga').hide();
    $('#ModalReporteFotograficoElimina').hide();
    if (_vdoc != null && _vdoc != "") {
        //var ext = _vdoc.substr(-3);
        var n = _vdoc.lastIndexOf("/");
        var nomArch = _vdoc.substr(parseInt(n) + 1);
        ////$('#id-md-p7-repfot-presu .modal-footer .pull-right').html('<a class="btn btn-primary" href="' + _vdoc + '"><i class="fa fa-cloud-download"></i> ' + _vbtn + '</a>');
        //$('#id-md-p7-repfot-presu .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="' + _vdoc + '" ><i class="fa fa-cloud-download"></i> ' + _vbtn + '</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
        $('#ModalReporteFotograficoDescarga').show();
        $('#ModalReporteFotograficoElimina').show();
        $('#ModalReporteFotograficoDescarga').attr('href', Url.Xplora + _vdoc);

        $('#ModalReporteFotograficoDescarga').attr('download', nomArch);

        //$('#ModalReporteFotograficoDescarga').attr('target', '_blank');
    }

    $('#id-md-p7-repfot-presu').modal({ backdrop: 'static' });
});
//Metodo para cargar archivo del Reporte Fotografico
$(document).on('change', '#id-fl-repfot-presu', function (e) {
    var _vdoc = $('#id-md-p7-repfot-presu').data('doc');
    var _vid = $('#id-md-p7-repfot-presu').data('id');
    var _vop = $('#id-md-p7-repfot-presu').data('op');

    fn_carga_archivo_repfot_presu('id-fl-repfot-presu', _vdoc, _vid, _vop);
});
//Funcion para cargar el archivo de Reporte fotografico
function fn_carga_archivo_repfot_presu(id, vdoc, id_reg, vop) {
    console.log('4');
    if ($("#" + id).val()) {
        var namefile = $("#" + id).val();

        var ext = namefile.substr(-3);

        if (vdoc == null) { vdoc = ""; }

        if (ext == 'jpg' || ext == 'png') {
            $('#' + id).upload(
                'Carga_Archivo',
                { _b: vdoc },
                function (a) {
                    var _nomfoto = String(a);
                    if (_nomfoto != "0" && _nomfoto != "") {
                        fn_ope_p7_LuckyAnalista(id_reg, _nomfoto, vop);
                        $('#' + id).parent().children('span').html('<i class="fa fa-check-circle" style="color:#70AD47;"></i>');
                        var _btn = "Presupuesto";
                        if (vop == 1) { _btn = "Rep. Fotografico"; }
                        ////$('#id-md-p7-repfot-presu .modal-footer .pull-right').html('<a class="btn btn-primary" href="' + _nomfoto + '"><i class="fa fa-cloud-download"></i> ' + _btn + '</a>');

                        //$('#id-md-p7-repfot-presu .modal-content').append('<div class="modal-footer"><div class="pull-right"><a class="btn btn-primary" href="' + _nomfoto + '" ><i class="fa fa-cloud-download"></i> ' + _btn + '</a></div> <div class="pull-left"><a class="btn btn-primary cls-eliminar"><i class="fa fa-trash"></i> Eliminar</a></div></div>');
                        $('#ModalReporteFotograficoDescarga').show();
                        $('#ModalReporteFotograficoElimina').show();
                        $('#ModalReporteFotograficoDescarga').attr('href', Url.Xplora + _nomfoto);
                        var n = _nomfoto.lastIndexOf("/");
                        var nomArch = _nomfoto.substr(parseInt(n) + 1);

                        $('#ModalReporteFotograficoDescarga').attr('download', nomArch);
                    }
                },
                'json'
            );
        } else {
            alert('Solo se permiten formatos (.jpg) o (.png)');
            return false;
        }
        
    } else {

        //alert("No selecciono ningún archivo...");
    }
}
$(document).on('click', '#id-md-p7-repfot-presu .cls-eliminar', function () {
    var _vid = $('#id-md-p7-repfot-presu').data('id');

    var _vop = $('#id-md-p7-repfot-presu').data('op');

    if (_vop == 1) {
        var opction = 3;
    } else {
        if (_vop == 2) {
            var opction = 4;
        }
    }
    _vid = (_vid == null || _vid == "" ? 0 : _vid);
    //var _nomfoto = null;
    fn_ope_p7_LuckyAnalista(_vid, '', opction);
    //fn_reg_repfoto_elimina(_vid, '')
});
//Funcion para eliminar el Reporte Fotografico
function fn_ope_p7_LuckyAnalista(vid, vdoc, vop) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso7_LuckyAnalista',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: vdoc,
            __c: vop
        },
        success: function (a) {
            if (vop == 1) {
                //$('#id-a-repfot-' + vid).html('Editar');
                $('#id-a-repfot-' + vid).html('<i class="fa fa-picture-o fa-2x" aria-hidden="true"></i>');
                $('#id-a-repfot-' + vid).data('doc', vdoc);
            } else if (vop == 2) {
                $('#id-a-presuf-' + vid).html('<img src="' + Url.Xplora + 'Img/logoexcel.png" >');
                $('#id-a-presuf-' + vid).data('doc', vdoc);
            } else if (vop == 3) {
                //$('#id-md-p7-repfot-presu .modal-content .modal-footer').remove();
                $('#ModalReporteFotograficoDescarga').hide();
                $('#ModalReporteFotograficoElimina').hide();
                $('#id-a-repfot-' + vid).html('Adjuntar');
                $('#id-a-repfot-' + vid).data('doc', null);
            } else if (vop == 4) {
                //$('#id-md-p7-repfot-presu .modal-content .modal-footer').remove();
                $('#ModalReporteFotograficoDescarga').hide();
                $('#ModalReporteFotograficoElimina').hide();
                $('#id-a-presuf-' + vid).html('Adjuntar');
                $('#id-a-presuf-' + vid).data('doc', null);
            }
        },
        error: function (xhr) {
            console.error('[Advertencia]<p7>: Problemas con el servicio.');
        }
    });
}

//Click para Aprobar o Desaprobar la Validacion de Fachada
$(document).on('click', '.cls-sw-pre-imple i', function (e) {
    if ($(this).hasClass('cls-active') != true) {
        $(this).parent().children('i').removeClass('cls-active');
        $(this).addClass('cls-active');
        var vvalor = $(this).data('estado');
        var vid = $(this).data('id');
        if (vvalor == 'si') {
            fn_ope_p8_Trade(vid, 1);
            $('.id-a-mante-' + vid).show();
        } else if (vvalor == 'no') {
            fn_ope_p8_Trade(vid, 0);
        }
    }
});
//Funcion para aprobar o desaprobar la validacion de fachada
function fn_ope_p8_Trade(vid, vop) {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Reg_Paso8_Trade',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: vid,
            __b: "",
            __c: vop
        },
        success: function (a) {
            $('#id-sp-fec-vali-' + vid).html(a);
        },
        error: function (xhr) {
            console.log('[Advertencia]<p8>: Problemas con el servicio.');
        }
    });
}

//Metodo para Cargar Archivos al Servidor
function fn_carga_archivo(id, vop, functioncalback, vdoc, vsw, vparametros) {
    if ($("#" + id).val()) {
        if (vdoc == null) {
            vdoc = "";
        }

        $('#' + id).upload(
            'Carga_Archivo',
            { _b: vdoc },
            function (a) {
                console.log(a);
                var _nomfoto = String(a);
                if (vsw == 1) {
                    functioncalback(vop, _nomfoto, vparametros);
                } else {
                    functioncalback(vop, _nomfoto);
                    $('#id-md-solicitud').modal('hide');
                }
            },
            'json'
        );
    } else {
        if (vdoc == null) {
            vdoc = "";
        }
        functioncalback(vop, vdoc);
    }
}
//Popup para Visualizar el PDF
function mostrarPDF(vdirdoc) {
    var w = 620;
    var h = 360;
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(vdirdoc, "pdf", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

//Utilitarios
function fnr_uti_soloNumerosypunto(e) {
    var key = window.Event ? e.which : e.keyCode
    //alert(e.target.id);
    var texto = $('#' + e.target.id).val();
    if (texto.indexOf(".") == -1 && texto.length != 0) {
        return ((key >= 48 && key <= 57) || key == 46);
    } else {
        return ((key >= 48 && key <= 57));
    }
}

function fn_BlankFotoswipeFotomontaje() {
    ArrayFotomontaje = [];
    $.each($('#id-md-fotomontaje img'), function (i, v) {
        var DataOrden = $(v).data('orden'),
            DataSrc = $(v).attr('src');
        if (DataSrc.indexOf('foto_nodisponible.jpg') == -1) {
            ArrayFotomontaje.push({ Orden: DataOrden, Url: DataSrc });
        }
    });
}
/*********************************************************************************/
var ArrayDataTableColumnsDefs = [];

function fnArrayPerfilVenta() {
    var Title,
        Value,
        Style,
        Class,
        Result, ListaOpciones = [];
    ArrayDataTableColumnsDefs = [
        {
            targets: 0,
            data: '_cg',
            render: function (data, type, full, meta) {
                if (data == 1) {
                    Title = 'Implementacion';
                    Value = 'I';
                } else {
                    if (data == 2) {
                        Title = 'Mantenimiento';
                        Value = 'M';
                    } else {
                        if (data = 3) {
                            Title = 'Tramite';
                            Value = 'T';
                        } else {
                            Title = '';
                            Value = '';
                        }
                    }
                }
                return '<div class="cls-TipoSolicitud" title="' + Title + '"><span class="cls-texto">' + Value + '</span></div>';
            }
        },//Tipo de Solicitud
        {
            targets: 1,
            data: null,
            render: function (data, type, full, meta) {
                if (data._di < 10) {
                    Value = "WF_0000" + data._di;
                } else {
                    if (data._di >= 10 && data._di < 100) {
                        Value = "WF_000" + data._di;
                    }
                    else {
                        if (data._di >= 100 && data._di < 1000) {
                            Value = "WF_00" + data._di;
                        }
                        else {
                            if (data._di >= 1000 && data._di < 10000) {
                                Value = "WF_0" + data._di;
                            }
                            else {
                                if (data._di >= 10000) {
                                    Value = "WF_" + data._di;
                                }
                                else {
                                    Value = "";
                                }
                            }
                        }
                    }
                }
                return '<div style="text-align:center;" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Id del Registro
        {
            targets: 2,
            data: '_bj',
            render: function (data, type, full, meta) {
                if (data.length > 0) {
                    if (data.length > 14) {
                        Title = 'title="' + data + '"';
                        Value = (data.substr(0, 10)) + "...";
                    } else {
                        Title = '';
                        Value = data;
                    }
                } else {
                    Title = '';
                    Value = '--';
                }
                return '<div style="text-align:center;width:80px;" ' + Title + ' >' + Value + '</div>';
            }
        },//Solicitado Por
        {
            targets: 3,
            data: '_b',
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:60px;">' + data + '</div>';
            }
        },//Fecha de Solicitud
        {
            targets: 4,
            data: '_c',
            render: function (data, type, full, meta) {
                if (data.length > 0) {
                    if (data.length > 36) {
                        Title = 'title="' + data + '"';
                        Value = data.substr(0, 33) + "...";
                    } else {
                        Title = '';
                        Value = data;
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:left;width:195px;" ' + Title + ' >' + Value + '</div>';
            }
        },//Punto de Venta
        {
            targets: 5,
            data: '_f',
            render: function (data, type, full, meta) {
                if (($.trim(data)).length > 0) {
                    if (($.trim(data)).length > 36) {
                        Title = 'title="' + ($.trim(data)) + '"';
                        Value = ($.trim(data)).substr(0, 33) + "...";
                    } else {
                        Title = '';
                        Value = $.trim(data);
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:left;width:200px" ' + Title + ' >' + Value + '</div>';
            }
        },//Direccion
        {
            targets: 6,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:55px;"><a href="javascript:" class="cls-ver-datos" data-idreg="' + data._a + '" data-ids="' + data._a + '" data-pdv="' + data._c + '" data-ruc="' + data._d + '" data-cod="' + data._e + '" data-direc="' + data._f + '" data-dist="' + data._g + '" data-prov="' + data._h + '" data-dep="' + data._i + '" data-cont="' + data._j + '" data-telf="' + data._k + '" data-celu="' + data._l + '" data-distrib="' + data._m + '" data-img-1="' + data._n + '" data-m1="' + data._o + '" data-m2="' + data._p + '" data-m3="' + data._q + '" data-m4="' + data._r + '" data-estado="' + data._s + '" data-p1zona="' + data._bk + '" data-p1comen="' + data._bl + '" data-p1tsolic="' + data._bn + '" data-marca="' + data._bv + '" data-tipo-gralsolicitud="' + data._cg + '" data-estado-solicitud="' + data._ch + '" data-referencia="' + data._dj + '">Ver</a></div>';
            }
        },//Datos del PDV
        {
            targets: 7,
            data: null,
            render: function (data, type, full, meta) {
                if (data._s == 'Pendiente') {
                    Style = 'style="font-weight: bold; color:red;text-align:center;width:60px;"';
                } else {
                    Style = 'style="text-align:center;width:60px;"';
                }
                return '<div ' + Style + '>' + data._s + '</div>';
            }
        },//Estado Solicitud Ventas
        {
            targets: 8,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 9,
            data: null,
            render: function (data, type, full, meta) {
                switch (data._v) {
                    case "True":
                        Style = 'style="color:blue;font-weight:bold;text-align:center;width:74px;"';
                        Value = 'Aprobado';
                        break;
                    case "False":
                        Style = 'style="color:red;font-weight:bold;text-align:center;width:74px;"';
                        Value = 'Desaprobado';
                        break;
                    default:
                        Style = 'style="text-align:center;width:74px;"';
                        Value = '';
                }
                return '<div ' + Style + '>' + Value + '</div>';
            }
        },//Estado de Solicitud Trade
        {
            targets: 10,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;" id="id-fec-aprosoli_' + data._a + '" class="cls-coment-aprobacion" data-id="' + data._a + '">' + data._w + '</div>';
            }
        },//Fecha de Aprobacion de Solicitud
        {
            targets: 11,
            data: null,
            render: function (data, type, full, meta) {
                if ($.trim(data._x).length > 0) {
                    //Value = '<a href="javascript:">Ver</a>';
                    if (($.trim(data._x)).length > 27) {
                        Title = 'title="' + ($.trim(data._x)) + '"';
                        Value = ($.trim(data._x)).substr(0, 24) + "...";
                    } else {
                        Title = '';
                        Value = $.trim(data._x);
                    }
                } else {
                    Value = ' ';
                }
                return '<div style="text-align:center;width:150px;cursor:pointer;" ' + Title + ' class="cls-td-comen-solicitud" data-op="1" id="id-comen-aprosoli_' + data._a + '" data-id="' + data._a + '" data-user="' + Persona.Grupo + '" data-comentario="' + $.trim(data._x) + '">' + Value + '</div>';
            }
        },//Comentario de Fecha de aprobacion
        {
            targets: 12,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 13,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    //Url_Gantt_Foto
                    if (data._z != null && data._z != "") {
                        var VarFecha = '';
                        if (data._bm != null) {
                            var ArrayFecha = (data._bm).split('/');
                            VarFecha = ArrayFecha[2] + '-' + ArrayFecha[1] + '-' + ArrayFecha[0];
                        }
                        Value = '<input type="date" style="width: 158px !important;text-align:center;" class="form-control input-sm" value="' + VarFecha + '" disabled="disabled" />';
                    } else {
                        Value = '';
                    }
                }
                
                return '<div style="text-align:left;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Visita
        {
            targets: 14,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;"> </div>';
                } else {
                    if ($.trim(data._bs).length > 0) {
                        Class = 'class="cls-datos-p6 cls-datos-p6-' + data._a + '"';
                        //Value = '<a href="javascript:">Ver</a>';
                        if (($.trim(data._bs)).length > 27) {
                            Title = 'title="' + ($.trim(data._bs)) + '"';
                            Value = ($.trim(data._bs)).substr(0, 24) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._bs);
                        }
                    } else {
                        Class = 'class="cls-datos-p9"';
                        Value = '&nbsp;';
                    }
                    return '<div style="text-align:center;width:150px;cursor:pointer;" ' + Title + ' ' + Class + ' data-id="' + data._a + '" data-comentario="' + $.trim(data._bs) + '" data-cod="1"><span class="cls-1-' + data._a + '">' + Value + '</span></div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Comentario
        {
            targets: 15,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    //Url_Gantt_Foto-----------------------Url_Fotomontaje
                    if (data._z != null && data._z != "" && data._aa != null && data._aa != "") {
                        Value = '<i style="cursor:pointer;" data-id="' + data._a + '" data-npdv="' + data._c + '" data-doc="' + data._aa + '" class="fa fa-camera-retro fa-2x cls-item-fotomontaje"></i>';

                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fotomontaje
        {
            targets: 16,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    if (data._bo != null && data._bo != '') {
                        Value = data._bo;
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;width:50px;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Monto
        {
            targets: 17,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    //$(td).css({ "background-color": "#E6E6E6 !important" });
                    Class = 'col-separador-Tramite';
                } else {
                    Class = 'col-separador';
                }
                $(td).addClass(Class);
            }
        },//Separador
        {
            targets: 18,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Result = '<div style="text-align:center;width:80px;"></div>';
                } else {
                    if (data._z != null && data._z != "" && data._aa != null && data._aa != "") {
                        var _vactive = "";
                        var _vdesactive = "";
                        switch (data._ad) {
                            case "True":
                                _vactive = "cls-active";
                                break;
                            case "False":
                                _vdesactive = "cls-active";
                                break;
                        }
                        Result = '<div style="text-align:center;width:80px;" class="cls-sw-pregunta cls-sw-preg-fotomont"><i class="fa fa-check ' + _vactive + ' cls-fotosi-' + data._a + '" data-id="' + data._a + '" data-estado="si" data-user="' + Persona.Grupo + '" data-op="2" data-comentario="' + data._af + '" data-clase="cls-fotosi-' + data._a + '"></i><i class="fa fa-times ' + _vdesactive + ' cls-fotono-' + data._a + '" data-id="' + data._a + '" data-estado="no" data-user="' + Persona.Grupo + '" data-op="2" data-comentario="' + data._af + '" data-clase="cls-fotono-' + data._a + '"></i></div>'
                    } else {
                        Result = '<div style="text-align:center;width:80px;"></div>';
                    }
                }
                return Result;
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Aprobacion de Fotomontaje
        {
            targets: 19,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:60px;"> </div>';
                } else {
                    if (data._ae != null && data._ae != '') {
                        Value = data._ae;
                    } else {
                        Value = '';
                    }
                    return '<div style="text-align:center;width:60px;" class="cls-fec_apro-fotomon_' + data._a + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Aprobacion
        {
            targets: 20,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:150px;"> </div>';
                } else {
                    if ($.trim(data._af).length > 0) {
                        //Value = '<a href="javascript:">Ver</a>';
                        if (($.trim(data._af)).length > 27) {
                            Title = 'title="' + ($.trim(data._af)) + '"';
                            Value = ($.trim(data._af)).substr(0, 24) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._af);
                        }
                    } else {
                        Value = ' ';
                    }
                    return '<div style="text-align:center;width:150px;cursor:pointer;" ' + Title + ' class="cls-td-comen-solicitud" id="id-comen-fotomontaje_' + data._a + '" data-op="2" data-id="' + data._a + '" data-user="' + Persona.Grupo + '" data-comentario="' + $.trim(data._af) + '">' + Value + '</div>';
                }
                
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Comentario
        {
            targets: 21,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    //Url_Fotomontaje
                    if (data._aa != null && (data._aa).length > 0) {
                        //Url_Carta_Fotomontaje
                        if (data._ag != null && $.trim((data._ag).length) > 0) {
                            //Url_Carta = Url.Xplora + data._ag;
                            Url_Carta = data._ag;
                            ValorCarta = '<img src="' + Url.Xplora + 'Img/ipdf.png" >';
                        } else {
                            Url_Carta = '';
                            ValorCarta = 'Cargar';
                        }
                        Value = '<a href="javascript:" class="cls-adjuntar-datos cls-crg-carta" id="id-crg-carta-' + data._a + '" data-doc="' + Url_Carta + '" data-idreg="' + data._a + '" style="font-weight:bold;">' + ValorCarta + '</a>';
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;width:70px;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Carta de Aprobacion
        {
            targets: 22,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    //$(td).css({ "background-color": "#E6E6E6 !important" });
                    Class = 'col-separador-Tramite';
                } else {
                    Class = 'col-separador';
                }
                $(td).addClass(Class);
            }
        },//Separador
        {
            targets: 23,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    if (data._ai != null && data._ai != "") {
                        switch (data._ai) {
                            case "True":
                                Value = '<span style="color:blue;font-weight:bold;">Aprobado</span>';
                                break;
                            case "False":
                                Value = '<span style="color:red;font-weight:bold;">Desaprobado</span>';
                                break;
                        }
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Aprobacion De Pre-presupuesto
        {
            targets: 24,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:70px;"> </div>';
                } else {
                    if (data._aj != '' && data._aj != null) {
                        Value = data._aj;
                    } else {
                        Value = '';
                    }
                    return '<div style="text-align:center;width:70px;" class="cls-fec-apro-prepresu-' + data._a + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Aprobacion de Pre-presupuesto
        {
            targets: 25,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    //$(td).css({ "background-color": "#E6E6E6 !important" });
                    Class = 'col-separador-Tramite';
                } else {
                    Class = 'col-separador';
                }
                $(td).addClass(Class);
            }
        },//Separador
        {
            targets: 26,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    var VarFecha = '';
                    if (data._bt != null) {
                        var ArrayFecha = data._bt.split('/');
                        VarFecha = ArrayFecha[2] + '-' + ArrayFecha[1] + '-' + ArrayFecha[0];
                    }
                    if (data._bt != null && data._bt != "") {
                        Value = '<input type="date" class="form-control input-sm" value="' + VarFecha + '" disabled="disabled" />'
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:left;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Implementacion
        {
            targets: 27,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:90px;"> </div>';
            }
        },//Status Documentario
        {
            targets: 28,
            data: null,
            render: function (data, type, full, meta) {
                ListaOpciones = [];
                $.each(Lista.TipoTramite, function (i, v) {
                    ListaOpciones.push('<option value="' + v.a + '" ' + (data._ce == v.a ? 'selected="selected"' : '') + '>' + v.b + '</option>');
                });
                return '<select name="tipo_tramite" id="tipo_tramite_' + data._a + '" style="width:120px;" class="form-control input-sm" data-id="' + data._a + '" disabled>' + ListaOpciones.join('') + '</select>';
            }
        },//Tipo de Tramite
        {
            targets: 29,
            data: null,
            render: function (data, type, full, meta) {
                ListaOpciones = [];
                $.each(Lista.TiempoPermiso, function (i, v) {
                    ListaOpciones.push('<option value="' + v.a + '" ' + (data._cf == v.a ? 'selected="selected"' : '') + '>' + v.b + '</option>');
                });
                return '<select name="tiempo_permiso" id="tiempo_permiso_' + data._a + '" style="width:120px;" class="form-control input-sm" data-id="' + data._a + '" disabled>' + ListaOpciones.join('') + '</select>';
            }
        },//Tiempo de Permiso
        {
            targets: 30,
            data: null,
            render: function (data, type, full, meta) {
                //Aprobacion de Pre-presupuesto
                if (data._ai == "True") {
                    //Fec_ingre_expe ------- Url_Rec_Tramite_Muni
                    if (data._as != null && data._as != "" && data._as != "01/01/1900" && data._at != null && data._at != "") {
                        Value = data._as;
                    }
                    else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:100px;">' + Value + '</div>';
            }
        },//Fecha De Ingreso De Expediente O Carta
        {
            targets: 31,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    if (data._at != null && data._at != "") {
                        Value = '<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" onclick="mostrarPDF(\'' + data._at + '\');" >';
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:100px;">' + Value + '</div>';
            }
        },//Recibo de Tramite Municipal
        {
            targets: 32,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    if ((data._au).length > 0) {
                        if ((data._au).length > 36) {
                            Title = 'title="' + (data._au) + '"';
                            Value = (data._au).substr(0, 33) + '...';
                        } else {
                            Title = '';
                            Value = data._au;
                        }
                    } else {
                        Title = '';
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:center;width:200px;" ' + Title + '>' + Value + '</div>';
            }
        },//Observacion
        {
            targets: 33,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Fec_Entre_Licencia   ----------------------Url_Licencia
                    if (data._av != null && data._av != "" && data._av != "01/01/1900" && data._ax != null && data._ax != "") {
                        Value = data._av;
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:90px;">' + Value + '</div>';
            }
        },//Fecha de Entrega de Permiso
        {
            targets: 34,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Fec_Cadu_Licencia   ----------------------Url_Licencia
                    if (data._aw != null && data._aw != "" && data._aw != "01/01/1900" && data._ax != null && data._ax != "") {
                        Value = data._aw;
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:90px;">' + Value + '</div>';
            }
        },//Fecha de Caducidad de Permiso
        {
            targets: 35,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Url_Licencia
                    if (data._ax != null && data._ax != "") {
                        Value = '<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" onclick="mostrarPDF(\'' + data._ax + '\');">'
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:60px;">' + Value + '</div>';
            }
        },//Permiso
        {
            targets: 36,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //p3_monto_presupuesto_final
                    if (data._bw != null && data._bw != "") {
                        Value = data._bw;
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:80px;">' + Value + '</div>';
            }
        },//Monto
        {
            targets: 37,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Url_Presu_Final
                    if (data._az != "" && data._az != null) {
                        Value = '<a href="' + Url.Xplora + data._az + '" title="' + data._bx + '"><img src="' + Url.Xplora + 'Img/logoexcel.png" /></a><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + data._bx + '">' + data._bx + '</span>'
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:80px;">' + Value + '</div>';
            }
        },//Presupuesto de Tramite
        {
            targets: 38,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            }
        },//Renovacion de Tramites
        {
            targets: 39,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Historico
                    Value = '<a href="javascript:" class="cls-hitorico-renovacion" data-id="' + data._a + '">Ver Historico</a>';
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            }
        },//Historico Tramites
        {
            targets: 40,
            data: '_ay',
            render: function (data, type, full, meta) {
                if (data != null && data != "") {
                    var n = data.lastIndexOf('/');
                    var nom_archive=data.substr(parseInt(n) + 1)
                    //replace para borrar un (/) inicial que esta demas en la variable data
                    Value = '<a href="' + Url.Xplora + data.replace('/', '') + '" download="' + nom_archive + '"><i class="fa fa-picture-o fa-2x" aria-hidden="true"></i></a>';
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:70px;">' + Value + '</div>';
            }
        },//Reporte Fotografico
        {
            targets: 41,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 42,
            data: '_bb',
            render: function (data, type, full, meta) {
                if (data != null && data != "") {
                    switch (data) {
                        case "True":
                            Value = '<span style="color:blue;font-weight:bold;">Aprobado</span>';
                            break;
                        case "False":
                            Value = '<span style="color:red;font-weight:bold;">Desaprobado</span>';
                            break;
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            }
        },//Validacion de fachada
        {
            targets: 43,
            data: null,
            render: function (data, type, full, meta) {
                if (data._bc != null && data._bc != '') {
                    Value = data._bc;
                } else {
                    Value = ' ';
                }
                return '<div style="text-align:center;width:70px;" id="id-sp-fec-vali-' + data._a + '" >' + Value + '</div>';
            }
        }//Fecha de Validacion
    ]
}
function fnArrayPerfilTrade() {
    var Title,
        Value,
        Style,
        Class,
        Result, ListaOpciones = [];

    ArrayDataTableColumnsDefs = [
        {
            targets: 0,
            data: '_cg',
            render: function (data, type, full, meta) {
                if (data == 1) {
                    Title = 'Implementacion';
                    Value = 'I';
                } else {
                    if (data == 2) {
                        Title = 'Mantenimiento';
                        Value = 'M';
                    } else {
                        if (data = 3) {
                            Title = 'Tramite';
                            Value = 'T';
                        } else {
                            Title = '';
                            Value = '';
                        }
                    }
                }
                return '<div class="cls-TipoSolicitud" title="' + Title + '"><span class="cls-texto">' + Value + '</span></div>';
            }
        },//Tipo de Solicitud
        {
            targets: 1,
            data: null,
            render: function (data, type, full, meta) {
                if (data._di < 10) {
                    Value = "WF_0000" + data._di;
                } else {
                    if (data._di >= 10 && data._di < 100) {
                        Value = "WF_000" + data._di;
                    }
                    else {
                        if (data._di >= 100 && data._di < 1000) {
                            Value = "WF_00" + data._di;
                        }
                        else {
                            if (data._di >= 1000 && data._di < 10000) {
                                Value = "WF_0" + data._di;
                            }
                            else {
                                if (data._di >= 10000) {
                                    Value = "WF_" + data._di;
                                }
                                else {
                                    Value = "";
                                }
                            }
                        }
                    }
                }
                return '<div style="text-align:center;" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Id del Registro
        {
            targets: 2,
            data: '_bj',
            render: function (data, type, full, meta) {
                if (data.length > 0) {
                    if (data.length > 14) {
                        Title = 'title="' + data + '"';
                        Value = data.substr(0, 10) + "...";
                    } else {
                        Title = '';
                        Value = data;
                    }
                } else {
                    Title = '';
                    Value = '--';
                }
                return '<div style="text-align:center;width:80px;" ' + Title + ' >' + Value + '</div>';
            }
        },//Solicitado Por
        {
            targets: 3,
            data: '_b',
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:60px;">' + data + '</div>';
            }
        },//Fecha de Solicitud
        {
            targets: 4,
            data: '_c',
            render: function (data, type, full, meta) {
                if (data.length > 0) {
                    if (data.length > 36) {
                        Title = 'title="' + data + '"';
                        Value = data.substr(0, 33) + "...";
                    } else {
                        Title = '';
                        Value = data;
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:left;width:195px;" ' + Title + ' >' + Value + '</div>';
            }
        },//Punto de Venta
        {
            targets: 5,
            data: '_f',
            render: function (data, type, full, meta) {
                if (($.trim(data)).length > 0) {
                    if (($.trim(data)).length > 36) {
                        Title = 'title="' + ($.trim(data)) + '"';
                        Value = ($.trim(data)).substr(0, 33) + "...";
                    } else {
                        Title = '';
                        Value = $.trim(data);
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:left;width:200px" ' + Title + ' >' + Value + '</div>';
            }
        },//Direccion
        {
            targets: 6,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:55px;"><a href="javascript:" class="cls-ver-datos" data-idreg="' + data._a + '" data-ids="' + data._a + '" data-pdv="' + data._c + '" data-ruc="' + data._d + '" data-cod="' + data._e + '" data-direc="' + data._f + '" data-dist="' + data._g + '" data-prov="' + data._h + '" data-dep="' + data._i + '" data-cont="' + data._j + '" data-telf="' + data._k + '" data-celu="' + data._l + '" data-distrib="' + data._m + '" data-img-1="' + data._n + '" data-m1="' + data._o + '" data-m2="' + data._p + '" data-m3="' + data._q + '" data-m4="' + data._r + '" data-estado="' + data._s + '" data-p1zona="' + data._bk + '" data-p1comen="' + data._bl + '" data-p1tsolic="' + data._bn + '" data-marca="' + data._bv + '" data-tipo-gralsolicitud="' + data._cg + '" data-estado-solicitud="' + data._ch + '" data-referencia="' + data._dj + '">Ver</a></div>';
            }
        },//Datos del PDV
        {
            targets: 7,
            data: null,
            render: function (data, type, full, meta) {
                if (data._s == 'Pendiente') {
                    Style = 'style="font-weight: bold; color:red;text-align:center;width:60px;"';
                } else {
                    Style = 'style="text-align:center;width:60px;"';
                }
                return '<div ' + Style + '>' + data._s + '</div>';
            }
        },//Estado Solicitud Ventas
        {
            targets: 8,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:50px;"><a href="javascript:" id="id-delete-grid" data-idregdrop="' + data._a + '"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a><div>';
            }
        },//Eliminar Registro
        {
            targets: 9,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 10,
            data: null,
            render: function (data, type, full, meta) {
                var _vactive = "";
                var _vdesactive = "";
                switch (data._v) {
                    case "True":
                        _vactive = "cls-active";
                        break;
                    case "False":
                        _vdesactive = "cls-active";
                        break;
                }
                return '<div style="text-align:center;width:80px;" class="cls-sw-pregunta cls-sw-preg-solicitud"><i class="fa fa-check ' + _vactive + ' cls-solsi-' + data._a + '" data-id="' + data._a + '" data-estado="si" data-user="' + Persona.Grupo + '" data-op="1" data-comentario="' + $.trim(data._x) + '" data-clase="cls-solsi-' + data._a + '"></i><i class="fa fa-times ' + _vdesactive + ' cls-fotono-' + data._a + '" data-id="' + data._a + '" data-estado="no" data-user="' + Persona.Grupo + '" data-op="1" data-comentario="' + $.trim(data._x) + '" data-clase="cls-solsi-' + data._a + '"></i></div>'
            }
        },//Aprobacion de Solicitud
        {
            targets: 11,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;" id="id-fec-aprosoli_' + data._a + '" class="cls-coment-aprobacion" data-id="' + data._a + '">' + data._w + '</div>';
            }
        },//Fecha de Aprobacion de Solicitud
        {
            targets: 12,
            data: null,
            render: function (data, type, full, meta) {
                if ($.trim(data._x).length > 0) {
                    //Value = '<a href="javascript:">Ver</a>';
                    if (($.trim(data._x)).length > 27) {
                        Title = 'title="' + ($.trim(data._x)) + '"';
                        Value = ($.trim(data._x)).substr(0, 24) + "...";
                    } else {
                        Title = '';
                        Value = $.trim(data._x);
                    }
                } else {
                    Value = '<a href="javascript:">Click!</a>';
                }
                return '<div style="text-align:center;width:150px;cursor:pointer;" ' + Title + ' class="cls-td-comen-solicitud" data-op="1" id="id-comen-aprosoli_' + data._a + '" data-id="' + data._a + '" data-user="' + Persona.Grupo + '" data-comentario="' + $.trim(data._x) + '">' + Value + '</div>';
            }
        },//Comentario de Fecha de aprobacion
        {
            targets: 13,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 14,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) { 
                    Value = '';
                } else {
                    //Url_Gantt_Foto
                    if (data._z != null && data._z != "") {
                        var VarFecha = '';
                        if (data._bm != null) {
                            var ArrayFecha = (data._bm).split('/');
                            VarFecha = ArrayFecha[2] + '-' + ArrayFecha[1] + '-' + ArrayFecha[0];
                        }
                        Value = '<input type="date" style="width: 158px !important;text-align:center;" class="form-control input-sm" value="' + VarFecha + '" disabled="disabled" />';
                    } else {
                        Value = '';
                    }
                }
                
                return '<div style="text-align:left;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Visita
        {
            targets: 15,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;"> </div>';
                } else {
                    if ($.trim(data._bs).length > 0) {
                        Class = 'class="cls-datos-p6 cls-datos-p6-' + data._a + '"';
                        //Value = '<a href="javascript:">Ver</a>';
                        if (($.trim(data._bs)).length > 27) {
                            Title = 'title="' + ($.trim(data._bs)) + '"';
                            Value = ($.trim(data._bs)).substr(0, 24) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._bs);
                        }
                    } else {
                        Class = 'class="cls-datos-p9"';
                        Value = ' ';
                    }
                    return '<div style="text-align:center;width:150px;cursor:pointer;" ' + Title + ' ' + Class + ' data-id="' + data._a + '" data-comentario="' + $.trim(data._bs) + '" data-cod="1"><span class="cls-1-' + data._a + '">' + Value + '</span></div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Comentario
        {
            targets: 16,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    //Url_Gantt_Foto-----------------------Url_Fotomontaje
                    if (data._z != null && data._z != "" && data._aa != null && data._aa != "") {
                        Value = '<i style="cursor:pointer;" data-id="' + data._a + '" data-npdv="' + data._c + '" data-doc="' + data._aa + '" class="fa fa-camera-retro fa-2x cls-item-fotomontaje"></i>';

                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fotomontaje
        {
            targets: 17,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    if (data._bo != null && data._bo != '') {
                        Value = data._bo;
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;width:50px;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Monto
        {
            targets: 18,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    if (data._ab != "" && data._ab != null) {
                        Value = '<a href="' + Url.Xplora + data._ab + '" title="' + data._bp + '"><img src="' + Url.Xplora + 'Img/logoexcel.png" /></a><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + data._bp + '">' + data._bp + '</span>';
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;width:60px;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Pre-presupuesto
        {
            targets: 19,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    //$(td).css({ "background-color": "#E6E6E6 !important" });
                    Class = 'col-separador-Tramite';
                } else {
                    Class = 'col-separador';
                }
                $(td).addClass(Class);
            }
        },//Separador
        {
            targets: 20,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    if (data._ad != null && data._ad != "") {
                        switch (data._ad) {
                            case "True":
                                Value = '<span style="color:blue;font-weight:bold;">Aprobado</span>';
                                break;
                            case "False":
                                Value = '<span style="color:red;font-weight:bold;">Desaprobado</span>';
                                break;
                        }
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Aprobacion de Fotomontaje
        {
            targets: 21,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:60px;"> </div>';
                } else {
                    if (data._ae != null && data._ae != '') {
                        Value = data._ae;
                    } else {
                        Value = '';
                    }
                    return '<div style="text-align:center;width:60px;" class="cls-fec_apro-fotomon_' + data._a + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Aprobacion
        {
            targets: 22,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:150px;"> </div>';
                } else {
                    if ($.trim(data._af).length > 0) {
                        //Value = '<a href="javascript:">Ver</a>';
                        if (($.trim(data._af)).length > 27) {
                            Title = 'title="' + ($.trim(data._af)) + '"';
                            Value = ($.trim(data._af)).substr(0, 24) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._af);
                        }
                    } else {
                        Value = ' ';
                    }
                    return '<div style="text-align:center;width:150px;cursor:pointer;" ' + Title + ' class="cls-td-comen-solicitud" id="id-comen-fotomontaje_' + data._a + '" data-op="2" data-id="' + data._a + '" data-user="' + Persona.Grupo + '" data-comentario="' + $.trim(data._af) + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Comentario
        {
            targets: 23,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    //Url_Fotomontaje
                    if (data._aa != null && (data._aa).length > 0) {
                        //Url_Carta_Fotomontaje
                        if (data._ag != "" && $.trim(data._ag).length > 0) {
                            Value = '<img src="' + Url.Xplora + 'Img/ipdf.png" class="cls-img-td" style="cursor:pointer;" onclick="mostrarPDF(\'' + $.trim(data._ag) + '\');" >'
                        } else {
                            Value = '';
                        }
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;width:70px">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Carta de Aprobacion
        {
            targets: 24,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    //$(td).css({ "background-color": "#E6E6E6 !important" });
                    Class = 'col-separador-Tramite';
                } else {
                    Class = 'col-separador';
                }
                $(td).addClass(Class);
            }
        },//Separador
        {
            targets: 25,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                    Class = '';
                    Title = '';
                } else {
                    //Url_Presupuesto
                    if (data._ab != null && data._ab != "") {
                        //Apro_pre_Presu
                        var _vactive;
                        var _vdesactive;
                        Class = 'class="cls-sw-pregunta cls-sw-pre-presu"';
                        switch (data._ai) {
                            case "True":
                                _vactive = "cls-active";
                                break;
                            case "False":
                                _vdesactive = "cls-active";
                                break;
                            default:
                                _vactive = "";
                                _vdesactive = "";
                        }
                        Title = '';
                        Value = '<i class="fa fa-check ' + _vactive + '" data-id="' + data._a + '" data-estado="si" ></i><i class="fa fa-times ' + _vdesactive + '" data-id="' + data._a + '" data-estado="no" ></i>'
                    } else {
                        Value = '';
                        Class = '';
                        Title = 'title="Ingresar un archivo de presupuesto"';
                    }
                }
                return '<div style="text-align:center;width:70px;height:20px;" ' + Title + ' ' + Class + '>' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Aprobacion De Pre-presupuesto
        {
            targets: 26,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:70px;"> </div>';
                } else {
                    if (data._aj != '' && data._aj != null) {
                        Value = data._aj;
                    } else {
                        Value = '';
                    }
                    return '<div style="text-align:center;width:70px;" class="cls-fec-apro-prepresu-' + data._a + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Aprobacion de Pre-presupuesto
        {
            targets: 27,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:80px;"> </div>'
                } else {
                    //Url_Presupuesto
                    if (data._ab != null && data._ab != "") {
                        Class = 'cls-datos-p5 cls-datos-p5-OC-' + data._a;
                    } else {
                        Class = '';
                    }
                    //Comentario de pre presupuesto
                    if ((data._am).length > 0) {
                        Aux = 'data-comentario="' + data._am + '" ';
                    } else {
                        Aux = 'data-comentario="" ';
                    }
                    //Orden de Compra
                    if (data._ak != '' && data._ak != null) {
                        Aux += 'data-oc="' + data._ak + '" ';
                        if (($.trim(data._ak)).length > 13) {
                            Title = 'title="' + ($.trim(data._ak)) + '"';
                            Value = ($.trim(data._ak)).substr(0, 10) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._ak);
                        }
                    } else {
                        Aux += 'data-oc="" ';
                        //Url_Presupuesto
                        if (data._ab != null && data._ab != "") {
                            Value = '<a href="javascript:">Click!</a>';
                            Title = '';
                        } else {
                            Value = ' ';
                            Title = 'title="Ingresar un archivo de presupuesto"';
                        }
                    }
                    //HES
                    if (data._al != '' && data._al != null) {
                        Aux += 'data-hes="' + data._al + '" ';
                    } else {
                        Aux += 'data-hes="" ';
                    }
                    return '<div style="text-align:center;width:80px;height:20px;cursor:pointer;" ' + Title + ' class="' + Class + '" data-id="' + data._a + '" ' + Aux + ' data-cod="1"><span class="cls-1-' + data._a + '" style="vertical-align: sub;">' + Value + '</span></div>'
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Orden de Compra
        {
            targets: 28,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:left;"> </div>';
                } else {
                    //Url_Presupuesto
                    if (data._ab != null && data._ab != "") {
                        var VarFecha = '';
                        if (data._df != null) {
                            var ArrayFecha = data._bm.split('/');
                            VarFecha = ArrayFecha[2] + '-' + ArrayFecha[1] + '-' + ArrayFecha[0];
                        }
                        if (data._df != '') {
                            Value = '<input type="date" id="txt_fecha_p5_trade_' + data._a + '" style="width: 158px !important;text-align:center;" class="form-control input-sm" value="' + VarFecha + '" onchange="fnOnChangeFechaPresupuesto(\'' + data._a + '\');" />';
                        } else {
                            Value = '<input type="date" id="txt_fecha_p5_trade_' + data._a + '" style="width: 158px !important;text-align:center;" class="form-control input-sm" value="" onchange="fnOnChangeFechaPresupuesto(\'' + data._a + '\');" />';
                        }
                    } else {
                        Value = '';
                    }
                    return '<div style="text-align:left;" class="dtp_fecha_p5_trade" data-opcion="1" title="Editar" data-ideq="" data-id="' + data._a + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha
        {
            targets: 29,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:80px;"> </div>';
                } else {
                    //Url_Presupuesto
                    if (data._ab != null && data._ab != "" && Persona.Grupo == 2) {
                        Class = 'cls-datos-p5 cls-datos-p5-HES-' + data._a
                    } else {
                        Class = '';
                    }
                    //Comentario de pre prsupuesto
                    if ((data._am).length > 0) {
                        Aux = 'data-comentario="' + data._am + '" ';
                    } else {
                        Aux = 'data-comentario="" ';
                    }
                    //Orden de Compra
                    if (data._ak != '' && data._ak != null) {
                        Aux += 'data-oc="' + data._ak + '" ';
                    } else {
                        Aux += 'data-oc="" ';
                    }
                    //HES
                    if (data._al != '' && data._al != null) {
                        Aux += 'data-hes="' + data._al + '" ';
                        if (($.trim(data._al)).length > 13) {
                            Title = 'title="' + ($.trim(data._al)) + '"';
                            Value = ($.trim(data._al)).substr(0, 10) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._al);
                        }
                    } else {
                        Aux += 'data-hes="" ';
                        //Url_Presupuesto
                        if (data._ab != null && data._ab != "") {
                            Value = '<a href="javascript:">Click!</a>';
                            Title = '';
                        } else {
                            Value = ' ';
                            Title = 'title="Ingresar un archivo de presupuesto"';
                        }
                    }
                    return '<div style="text-align:center;width:80px;height:20px;cursor:pointer;" ' + Title + ' class="' + Class + '" data-id="' + data._a + '" ' + Aux + ' data-cod="1"><span class="cls-2-' + data._a + '" style="vertical-align: sub;">' + Value + '</span></div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//H.E.S.
        {
            targets: 30,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:150px;"> </div>'
                } else {
                    //Url_Presupuesto
                    if (data._ab != null && data._ab != "") {
                        Class = 'cls-datos-p5 cls-datos-p5-coment-' + data._a
                    } else {
                        Class = '';
                    }
                    //Comentario de pre presupuesto
                    if ((data._am).length > 0) {
                        Aux = 'data-comentario="' + data._am + '" ';
                        if (($.trim(data._am)).length > 24) {
                            Title = 'title="' + ($.trim(data._am)) + '"';
                            Value = ($.trim(data._am)).substr(0, 21) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._am);
                        }
                    } else {
                        Aux = 'data-comentario="" ';
                        //Url_Presupuesto
                        if (data._ab != null && data._ab != "") {
                            Value = '<a href="javascript:">Click!</a>';
                            Title = '';
                        } else {
                            Value = ' ';
                            Title = 'title="Ingresar un archivo de presupuesto"';
                        }
                    }
                    //Orden de Compra
                    if (data._ak != '' && data._ak != null) {
                        Aux += 'data-oc="' + data._ak + '" ';
                    } else {
                        Aux += 'data-oc="" ';
                    }
                    //HES
                    if (data._al != '' && data._al != null) {
                        Aux += 'data-hes="' + data._al + '" ';
                    } else {
                        Aux += 'data-hes="" ';
                    }
                    return '<div style="text-align:center;width:150px;height:20px;cursor:pointer;" ' + Title + ' class="' + Class + '" data-id="' + data._a + '" ' + Aux + ' data-cod="1"><span class="cls-3-' + data._a + '" style="vertical-align: sub;">' + Value + '</span></div>'
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Comentario
        {
            targets: 31,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    //$(td).css({ "background-color": "#E6E6E6 !important" });
                    Class = 'col-separador-Tramite';
                } else {
                    Class = 'col-separador';
                }
                $(td).addClass(Class);
            }
        },//Separador
        {
            targets: 32,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    var VarFecha = '';
                    if (data._bt != null) {
                        var ArrayFecha = data._bt.split('/');
                        VarFecha = ArrayFecha[2] + '-' + ArrayFecha[1] + '-' + ArrayFecha[0];
                    }
                    if (data._bt != null && data._bt != "") {
                        Value = '<input type="date" class="form-control input-sm" value="' + VarFecha + '" disabled="disabled" />'
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:left;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Implementacion
        {
            targets: 33,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:90px;"> </div>';
            }
        },//Status Documentario
        {
            targets: 34,
            data: null,
            render: function (data, type, full, meta) {
                ListaOpciones = [];
                $.each(Lista.TipoTramite, function (i, v) {
                    ListaOpciones.push('<option value="' + v.a + '" ' + (data._ce == v.a ? 'selected="selected"' : '') + '>' + v.b + '</option>');
                });
                return '<select name="tipo_tramite" id="tipo_tramite_' + data._a + '" style="width:120px;" class="form-control input-sm" data-id="' + data._a + '" disabled>' + ListaOpciones.join('') + '</select>';
            }
        },//Tipo de Tramite
        {
            targets: 35,
            data: null,
            render: function (data, type, full, meta) {
                ListaOpciones = [];
                $.each(Lista.TiempoPermiso, function (i, v) {
                    ListaOpciones.push('<option value="' + v.a + '" ' + (data._cf == v.a ? 'selected="selected"' : '') + '>' + v.b + '</option>');
                });
                return '<select name="tiempo_permiso" id="tiempo_permiso_' + data._a + '" style="width:120px;" class="form-control input-sm" data-id="' + data._a + '" disabled>' + ListaOpciones.join('') + '</select>';
            }
        },//Tiempo de Permiso
        {
            targets: 36,
            data: null,
            render: function (data, type, full, meta) {
                //Aprobacion de Pre-presupuesto
                if (data._ai == "True") {
                    //Fec_ingre_expe ------- Url_Rec_Tramite_Muni
                    if (data._as != null && data._as != "" && data._as != "01/01/1900" && data._at != null && data._at != "") {
                        Value = data._as;
                    }
                    else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:100px;">' + Value + '</div>';
            }
        },//Fecha De Ingreso De Expediente O Carta
        {
            targets: 37,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    if (data._at != null && data._at != "") {
                        Value = '<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" onclick="mostrarPDF(\'' + data._at + '\');" >';
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:100px;">' + Value + '</div>';
            }
        },//Recibo de Tramite Municipal
        {
            targets: 38,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    if ((data._au).length > 0) {
                        if ((data._au).length > 36) {
                            Title = 'title="' + (data._au) + '"';
                            Value = (data._au).substr(0, 33) + '...';
                        } else {
                            Title = '';
                            Value = data._au;
                        }
                    } else {
                        Title = '';
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:center;width:200px;" ' + Title + '>' + Value + '</div>';
            }
        },//Observacion
        {
            targets: 39,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Fec_Entre_Licencia   ----------------------Url_Licencia
                    if (data._av != null && data._av != "" && data._av != "01/01/1900" && data._ax != null && data._ax != "") {
                        Value = data._av;
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:90px;">' + Value + '</div>';
            }
        },//Fecha de Entrega de Permiso
        {
            targets: 40,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Fec_Cadu_Licencia   ----------------------Url_Licencia
                    if (data._aw != null && data._aw != "" && data._aw != "01/01/1900" && data._ax != null && data._ax != "") {
                        Value = data._aw;
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:90px;">' + Value + '</div>';
            }
        },//Fecha de Caducidad de Permiso
        {
            targets: 41,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Url_Licencia
                    if (data._ax != null && data._ax != "") {
                        Value = '<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" onclick="mostrarPDF(\'' + data._ax + '\');">'
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:60px;">' + Value + '</div>';
            }
        },//Permiso
        {
            targets: 42,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //p3_monto_presupuesto_final
                    if (data._bw != null && data._bw != "") {
                        Value = data._bw;
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:80px;">' + Value + '</div>';
            }
        },//Monto
        {
            targets: 43,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Url_Presu_Final
                    if (data._az != "" && data._az != null) {
                        Value = '<a href="' + Url.Xplora + data._az + '" title="' + data._bx + '"><img src="' + Url.Xplora + 'Img/logoexcel.png" /></a><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + data._bx + '">' + data._bx + '</span>'
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:80px;">' + Value + '</div>';
            }
        },//Presupuesto de Tramite
        {
            targets: 44,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            }
        },//Renovacion de Tramites
        {
            targets: 45,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Historico
                    Value = '<a href="javascript:" class="cls-hitorico-renovacion" data-id="' + data._a + '">Ver Historico</a>';
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            }
        },//Historico Tramites
        {
            targets: 46,
            data: '_ay',
            render: function (data, type, full, meta) {
                if (data != null && data != "") {
                    var n = data.lastIndexOf('/');
                    var nom_archive = data.substr(parseInt(n) + 1)
                    //replace para borrar un (/) inicial que esta demas en la variable data
                    Value = '<a href="' + Url.Xplora + data.replace('/', '') + '" download="' + nom_archive + '"><i class="fa fa-picture-o fa-2x" aria-hidden="true"></i></a>';
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:70px;">' + Value + '</div>';
            }
        },//Reporte Fotografico
        {
            targets: 47,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 48,
            data: null,
            render: function (data, type, full, meta) {
                //Url_Report_Fotografico
                if (data._ay != null && data._ay != "") {
                    var _vactive;
                    var _vdesactive;
                    Class = 'class="cls-sw-pregunta cls-sw-pre-imple"';
                    //Vali_Fechada_Imple
                    switch (data._bb) {
                        case "True":
                            _vactive = "cls-active";
                            break;
                        case "False":
                            _vdesactive = "cls-active";
                            break;
                        default:
                            _vactive = "";
                            _vdesactive = "";
                    }
                    Value = '<i class="fa fa-check ' + _vactive + '" data-id="' + data._a + '" data-estado="si" ></i><i class="fa fa-times ' + _vdesactive + '" data-id="' + data._a + '" data-estado="no" ></i>'
                } else {
                    Value = ' ';
                    Class = '';
                }
                return '<div style="text-align:center;width:74px;" ' + Class + '>' + Value + '</div>';
            }
        },//Validacion de fachada
        {
            targets: 49,
            data: null,
            render: function (data, type, full, meta) {
                if (data._bc != null && data._bc != '') {
                    Value = data._bc;
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:70px;" id="id-sp-fec-vali-' + data._a + '" >' + Value + '</div>';
            }
        }//Fecha de Validacion
    ]
}
function fnArrayPerfilAnalis() {
    var Title,
        Value,
        Style,
        Class,
        Result, ListaOpciones = [];
    ArrayDataTableColumnsDefs = [
        {
            targets: 0,
            data: '_cg',
            render: function (data, type, full, meta) {
                if (data == 1) {
                    Title = 'Implementacion';
                    Value = 'I';
                } else {
                    if (data == 2) {
                        Title = 'Mantenimiento';
                        Value = 'M';
                    } else {
                        if (data = 3) {
                            Title = 'Tramite';
                            Value = 'T';
                        } else {
                            Title = '';
                            Value = '';
                        }
                    }
                }
                return '<div class="cls-TipoSolicitud" title="' + Title + '"><span class="cls-texto">' + Value + '</span></div>';
            }
        },//Tipo de Solicitud
        {
            targets: 1,
            data: null,
            render: function (data, type, full, meta) {
                if (data._di < 10) {
                    Value = "WF_0000" + data._di;
                } else {
                    if (data._di >= 10 && data._di < 100) {
                        Value = "WF_000" + data._di;
                    }
                    else {
                        if (data._di >= 100 && data._di < 1000) {
                            Value = "WF_00" + data._di;
                        }
                        else {
                            if (data._di >= 1000 && data._di < 10000) {
                                Value = "WF_0" + data._di;
                            }
                            else {
                                if (data._di >= 10000) {
                                    Value = "WF_" + data._di;
                                }
                                else {
                                    Value = "";
                                }
                            }
                        }
                    }
                }
                return '<div style="text-align:center;" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Id del Registro
        {
            targets: 2,
            data: '_bj',
            render: function (data, type, full, meta) {
                if (data.length > 0) {
                    if (data.length > 14) {
                        Title = 'title="' + data + '"';
                        Value = data.substr(0, 10) + "...";
                    } else {
                        Title = '';
                        Value = data;
                    }
                } else {
                    Title = '';
                    Value = '--';
                }
                return '<div style="text-align:center;width:80px;" ' + Title + ' >' + Value + '</div>';
            }
        },//Solicitado Por
        {
            targets: 3,
            data: '_b',
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:60px;">' + data + '</div>';
            }
        },//Fecha de Solicitud
        {
            targets: 4,
            data: '_c',
            render: function (data, type, full, meta) {
                if (data.length > 0) {
                    if (data.length > 36) {
                        Title = 'title="' + data + '"';
                        Value = data.substr(0, 33) + "...";
                    } else {
                        Title = '';
                        Value = data;
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:left;width:195px;" ' + Title + ' >' + Value + '</div>';
            }
        },//Punto de Venta
        {
            targets: 5,
            data: '_f',
            render: function (data, type, full, meta) {
                if (($.trim(data)).length > 0) {
                    if (($.trim(data)).length > 36) {
                        Title = 'title="' + ($.trim(data)) + '"';
                        Value = ($.trim(data)).substr(0, 33) + "...";
                    } else {
                        Title = '';
                        Value = $.trim(data);
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:left;width:200px" ' + Title + ' >' + Value + '</div>';
            }
        },//Direccion
        {
            targets: 6,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:55px;"><a href="javascript:" class="cls-ver-datos" data-idreg="' + data._a + '" data-ids="' + data._a + '" data-pdv="' + data._c + '" data-ruc="' + data._d + '" data-cod="' + data._e + '" data-direc="' + data._f + '" data-dist="' + data._g + '" data-prov="' + data._h + '" data-dep="' + data._i + '" data-cont="' + data._j + '" data-telf="' + data._k + '" data-celu="' + data._l + '" data-distrib="' + data._m + '" data-img-1="' + data._n + '" data-m1="' + data._o + '" data-m2="' + data._p + '" data-m3="' + data._q + '" data-m4="' + data._r + '" data-estado="' + data._s + '" data-p1zona="' + data._bk + '" data-p1comen="' + data._bl + '" data-p1tsolic="' + data._bn + '" data-marca="' + data._bv + '" data-tipo-gralsolicitud="' + data._cg + '" data-estado-solicitud="' + data._ch + '" data-referencia="' + data._dj + '">Ver</a></div>';
            }
        },//Datos del PDV
        {
            targets: 7,
            data: null,
            render: function (data, type, full, meta) {
                if (data._s == 'Pendiente') {
                    Style = 'style="font-weight: bold; color:red;text-align:center;width:60px;"';
                } else {
                    Style = 'style="text-align:center;width:60px;"';
                }
                return '<div ' + Style + '>' + data._s + '</div>';
            }
        },//Estado Solicitud Ventas
        {
            targets: 8,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 9,
            data: null,
            render: function (data, type, full, meta) {
                switch (data._v) {
                    case "True":
                        Style = 'style="color:blue;font-weight:bold;text-align:center;width:74px;"';
                        Value = 'Aprobado';
                        break;
                    case "False":
                        Style = 'style="color:red;font-weight:bold;text-align:center;width:74px;"';
                        Value = 'Desaprobado';
                        break;
                    default:
                        Style = 'style="text-align:center;width:74px;"';
                        Value = '';
                }
                return '<div ' + Style + '>' + Value + '</div>';
            }
        },//Aprobacion de Solicitud
        {
            targets: 10,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;" id="id-fec-aprosoli_' + data._a + '" class="cls-coment-aprobacion" data-id="' + data._a + '">' + data._w + '</div>';
            }
        },//Fecha de Aprobacion de Solicitud
        {
            targets: 11,
            data: null,
            render: function (data, type, full, meta) {
                if ($.trim(data._x).length > 0) {
                    //Value = '<a href="javascript:">Ver</a>';
                    if (($.trim(data._x)).length > 26) {
                        Title = 'title="' + ($.trim(data._x)) + '"';
                        Value = ($.trim(data._x)).substr(0, 23) + "...";
                    } else {
                        Title = '';
                        Value = $.trim(data._x);
                    }
                } else {
                    Value = ' ';
                }
                return '<div style="text-align:center;width:150px;" ' + Title + ' class="cls-td-comen-solicitud" data-op="1" id="id-comen-aprosoli_' + data._a + '" data-id="' + data._a + '" data-user="' + Persona.Grupo + '" data-comentario="' + $.trim(data._x) + '">' + Value + '</div>';
            }
        },//Comentario de Fecha de aprobacion
        {
            targets: 12,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 13,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:left;"> </div>';
                } else {
                    //Url_Gantt_Foto
                    if (data._z == "1") {
                        var VarFecha = '';
                        if (data._bm != null) {
                            var ArrayFecha = data._bm.split('/');
                            VarFecha = ArrayFecha[2] + '-' + ArrayFecha[1] + '-' + ArrayFecha[0];
                        }
                        Value = '<input type="date" id="txt_fecha_entrega_foto_' + data._a + '" style="width: 158px !important;text-align:center;" class="form-control input-sm" value="' + VarFecha + '" onchange="fnOnChangeFechaVisita(\'' + data._a + '\');" />';
                    } else {
                        Value = '<input type="date" id="txt_fecha_entrega_foto_' + data._a + '" style="width: 158px !important;text-align:center;" class="form-control input-sm" value="" onchange="fnOnChangeFechaVisita(\'' + data._a + '\');" />';
                    }
                    return '<div style="text-align:left;" class="dtp_fecha_entrega_foto" data-opcion="1" title="Editar" data-ideq="" data-id="' + data._a + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Visita
        {
            targets: 14,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="align:left;"> </div>';
                } else {
                    ListaOpciones = [];
                    $.each(Lista.Proveedor, function (i, v) {
                        ListaOpciones.push('<option value="' + v.a + '" ' + (data._bu == v.a ? 'selected="selected"' : '') + '>' + v.b + '</option>');
                    });
                    return '<select name="Provider_code" id="Provider_code_' + data._a + '" style="width:120px;" class="cls-datos-p3 form-control input-sm" data-id="' + data._a + '">' + ListaOpciones.join('') + '</select>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Proveedor
        {
            targets: 15,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;"> </div>';
                } else {
                    if ($.trim(data._bs).length > 0) {
                        //Value = '<a href="javascript:">Ver</a>';
                        if (($.trim(data._bs)).length > 27) {
                            Title = 'title="' + ($.trim(data._bs)) + '"';
                            Value = ($.trim(data._bs)).substr(0, 24) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._bs);
                        }
                    } else {
                        Value = '&nbsp;';
                    }
                    return '<div style="text-align:center;width:150px;cursor:pointer;" ' + Title + ' class="cls-datos-p6 cls-datos-p6-' + data._a + '" data-id="' + data._a + '" data-comentario="' + $.trim(data._bs) + '" data-cod="3"><span class="cls-1-' + data._a + '">' + Value + '</span></div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Comentario
        {
            targets: 16,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = ' ';
                } else {
                    //Url_Gantt_Foto
                    if (data._z != null && data._z != "") {
                        //Url_Fotomontaje
                        Style = 'style="cursor:pointer;"';
                    } else {
                        Style = 'style="display:none;"';
                    }
                    Value = '<i ' + Style + ' data-id="' + data._a + '" data-npdv="' + data._c + '" data-doc="' + data._aa + '" class="fa fa-camera-retro fa-2x cls-item-fotomontaje cls-ft-' + data._a + '"></i>';
                }
                return '<div style="text-align:center;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fotomontaje
        {
            targets: 17,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = ' ';
                } else {
                    if (data._bo != null && data._bo != '') {
                        Value = data._bo;
                    } else {
                        Value = ' ';
                    }
                }
                return '<div style="text-align:center;width:50px;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Monto
        {
            targets: 18,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center"> </div>';
                } else {
                    if (data._ab == "" || data._ab == null) {
                        Extra = 'data-doc=""';
                        Value = '<a href="javascript:" class="cls-a-pre-presu cls-pre-presu-' + data._a + '" ' + Extra + '  data-id="' + data._a + '" data-monto="' + data._bo + '" data-codigo="' + data._bp + '">Cargar</a>';
                    } else {
                        Extra = 'data-doc="' + data._ab + '"';
                        Value = '<a href="javascript:" class="cls-a-pre-presu cls-pre-presu-' + data._a + '" ' + Extra + '  data-id="' + data._a + '" data-monto="' + data._bo + '" data-codigo="' + data._bp + '"><img src="' + Url.Xplora + 'Img/logoexcel.png" /></a><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + data._bp + '">' + data._bp + '</span>';
                    }
                    return '<div style="text-align:center">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Pre-presupuesto
        {
            targets: 19,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    //$(td).css({ "background-color": "#E6E6E6 !important" });
                    Class = 'col-separador-Tramite';
                } else {
                    Class = 'col-separador';
                }
                $(td).addClass(Class);
            }
        },//Separador
        {
            targets: 20,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = ' ';
                } else {
                    if (data._ad != null && data._ad != "") {
                        switch (data._ad) {
                            case "True":
                                Value = '<span style="color:blue;font-weight:bold;">Aprobado</span>';
                                break;
                            case "False":
                                Value = '<span style="color:red;font-weight:bold;">Desaprobado</span>';
                                break;
                        }
                    } else {
                        Value = ' ';
                    }
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Aprobacion de Fotomontaje
        {
            targets: 21,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:60px;"> </div>';
                } else {
                    if (data._ae != null && data._ae != '') {
                        Value = data._ae;
                    } else {
                        Value = ' ';
                    }
                    return '<div style="text-align:center;width:60px;" class="cls-fec_apro-fotomon_' + data._a + '">' + Value + '</div>';
                }
                
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Aprobacion
        {
            targets: 22,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:150px;"> </div>';
                } else {
                    if ($.trim(data._af).length > 0) {
                        //Value = '<a href="javascript:">Ver</a>';
                        if (($.trim(data._af)).length > 27) {
                            Title = 'title="' + ($.trim(data._af)) + '"';
                            Value = ($.trim(data._af)).substr(0, 24) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._af);
                        }
                    } else {
                        Value = ' ';
                    }
                    return '<div style="text-align:center;width:150px;cursor:pointer;" ' + Title + ' class="cls-td-comen-solicitud" id="id-comen-fotomontaje_' + data._a + '" data-op="2" data-id="' + data._a + '" data-user="' + Persona.Grupo + '" data-comentario="' + $.trim(data._af) + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Comentario
        {
            targets: 23,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    //Url_Fotomontaje
                    if (data._aa != null && (data._aa).length > 0) {
                        //Url_Carta_Fotomontaje
                        if (data._ag != "" && $.trim(data._ag).length > 0) {
                            Value = '<img src="' + Url.Xplora + 'Img/ipdf.png" class="cls-img-td" style="cursor:pointer;" onclick="mostrarPDF(\'' + $.trim(data._ag) + '\');" >'
                        } else {
                            Value = '';
                        }
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;width:70px">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Carta de Aprobacion
        {
            targets: 24,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    //$(td).css({ "background-color": "#E6E6E6 !important" });
                    Class = 'col-separador-Tramite';
                } else {
                    Class = 'col-separador';
                }
                $(td).addClass(Class);
            }
        },//Separador
        {
            targets: 25,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                } else {
                    if (data._ai != null && data._ai != "") {
                        switch (data._ai) {
                            case "True":
                                Value = '<span style="color:blue;font-weight:bold;">Aprobado</span>';
                                break;
                            case "False":
                                Value = '<span style="color:red;font-weight:bold;">Desaprobado</span>';
                                break;
                        }
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Aprobacion De Pre-presupuesto
        {
            targets: 26,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    Value = '';
                    return '<div style="text-align:center;width:70px;"> </div>';
                } else {
                    if (data._aj != '' && data._aj != null) {
                        Value = data._aj;
                    } else {
                        Value = '';
                    }
                    return '<div style="text-align:center;width:70px;" class="cls-fec-apro-prepresu-' + data._a + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Aprobacion de Pre-presupuesto
        {
            targets: 27,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:80px;"> </div>';
                } else {
                    //Url_Presupuesto
                    if (data._ab != null && data._ab != "") {
                        Class = 'cls-datos-p5';
                    } else {
                        Class = '';
                    }
                    //Comentario de pre presupuesto
                    if ((data._am).length > 0) {
                        Aux = 'data-comentario="' + data._am + '" ';
                    } else {
                        Aux = 'data-comentario="" ';
                    }
                    //Orden de Compra
                    if (data._ak != '' && data._ak != null) {
                        Aux += 'data-oc="' + data._ak + '" ';
                        if (($.trim(data._ak)).length > 13) {
                            Title = 'title="' + ($.trim(data._ak)) + '"';
                            Value = ($.trim(data._ak)).substr(0, 10) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._ak);
                        }
                    } else {
                        Aux += 'data-oc="" ';
                        Value = '';
                    }
                    //HES
                    if (data._al != '' && data._al != null) {
                        Aux += 'data-hes="' + data._al + '" ';
                    } else {
                        Aux += 'data-hes="" ';
                    }
                    return '<div style="text-align:center;width:80px;height:20px;cursor:pointer;" ' + Title + ' class="' + Class + '" data-id="' + data._a + '" ' + Aux + ' data-cod="2"><span class="cls-1-' + data._a + '" style="vertical-align: sub;">' + Value + '</span></div>';
                }
                
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Orden de Compra
        {
            targets: 28,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:80px;"> </div>'
                } else {
                    //Url_Presupuesto
                    if (data._ab != null && data._ab != "") {
                        Class = 'cls-datos-p5';
                    } else {
                        Class = '';
                    }
                    //Comentario de pre prsupuesto
                    if ((data._am).length > 0) {
                        Aux = 'data-comentario="' + data._am + '" ';
                    } else {
                        Aux = 'data-comentario="" ';
                    }
                    //Orden de Compra
                    if (data._ak != '' && data._ak != null) {
                        Aux += 'data-oc="' + data._ak + '" ';
                    } else {
                        Aux += 'data-oc="" ';
                    }
                    //HES
                    if (data._al != '' && data._al != null) {
                        Aux += 'data-hes="' + data._al + '" ';
                        if (($.trim(data._al)).length > 13) {
                            Title = 'title="' + ($.trim(data._al)) + '"';
                            Value = ($.trim(data._al)).substr(0, 10) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._al);
                        }
                    } else {
                        Aux += 'data-hes="" ';
                        Value = '';
                    }
                    return '<div style="text-align:center;width:80px;height:20px;cursor:pointer;" ' + Title + ' class="' + Class + '" data-id="' + data._a + '" ' + Aux + ' data-cod="2"><span class="cls-2-' + data._a + '" style="vertical-align: sub;">' + Value + '</span></div>'
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//H.E.S.
        {
            targets: 29,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:center;width:150px;"> </div>'
                } else {
                    //Url_Presupuesto
                    if (data._ab != null && data._ab != "") {
                        Class = 'cls-datos-p5';
                    } else {
                        Class = '';
                    }
                    //Comentario de pre presupuesto
                    if ((data._am).length > 0) {
                        Aux = 'data-comentario="' + data._am + '" ';
                        if (($.trim(data._am)).length > 27) {
                            Title = 'title="' + ($.trim(data._am)) + '"';
                            Value = ($.trim(data._am)).substr(0, 24) + "...";
                        } else {
                            Title = '';
                            Value = $.trim(data._am);
                        }
                    } else {
                        Aux = 'data-comentario="" ';
                        Value = '';
                    }
                    //Orden de Compra
                    if (data._ak != '' && data._ak != null) {
                        Aux += 'data-oc="' + data._ak + '" ';
                    } else {
                        Aux += 'data-oc="" ';
                    }
                    //HES
                    if (data._al != '' && data._al != null) {
                        Aux += 'data-hes="' + data._al + '" ';
                    } else {
                        Aux += 'data-hes="" ';
                    }
                    return '<div style="text-align:center;width:150px;height:20px;cursor:pointer;" ' + Title + ' class="' + Class + '" data-id="' + data._a + '" ' + Aux + ' data-cod="2"><span class="cls-3-' + data._a + '" style="vertical-align: sub;">' + Value + '</span></div>'
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Comentario
        {
            targets: 30,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    //$(td).css({ "background-color": "#E6E6E6 !important" });
                    Class = 'col-separador-Tramite';
                } else {
                    Class = 'col-separador';
                }
                $(td).addClass(Class);
            }
        },//Separador
        {
            targets: 31,
            data: null,
            render: function (data, type, full, meta) {
                if (data._cg == 3) {
                    return '<div style="text-align:left;"> </div>';
                } else {
                    //Apro_Fotomontaje
                    if (data._ad == "True") {
                        var VarFecha = '';
                        if (data._bt != '' && data._bt != null) {
                            var ArrayFecha = data._bt.split('/');
                            VarFecha = ArrayFecha[2] + '-' + ArrayFecha[1] + '-' + ArrayFecha[0];
                        }
                        Value = '<input type="date" id="txt_fecha_implementacion_' + data._a + '" style="width: 158px !important;text-align:center;" class="form-control input-sm" value="' + VarFecha + '" onchange="fnOnChangeFechaImplementacion(\'' + data._a + '\');" />';
                    } else {
                        Value = ' ';
                    }
                    return '<div style="text-align:left;" class="dtp_fecha_implementacion" data-opcion="1" title="Editar" data-ideq="" data-id="' + data._a + '">' + Value + '</div>';
                }
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Implementacion
        {
            targets: 32,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:90px;"> </div>';
            }
        },//Status Documentario
        {
            targets: 33,
            data: null,
            render: function (data, type, full, meta) {
                ListaOpciones = [];
                $.each(Lista.TipoTramite, function (i, v) {
                    ListaOpciones.push('<option value="' + v.a + '" ' + (data._ce == v.a ? 'selected="selected"' : '') + '>' + v.b + '</option>');
                });
                return '<select name="tipo_tramite" id="tipo_tramite_' + data._a + '" style="width:120px;" class="form-control input-sm" data-id="' + data._a + '" disabled>' + ListaOpciones.join('') + '</select>';
            }
        },//Tipo de Tramite
        {
            targets: 34,
            data: null,
            render: function (data, type, full, meta) {
                ListaOpciones = [];
                $.each(Lista.TiempoPermiso, function (i, v) {
                    ListaOpciones.push('<option value="' + v.a + '" ' + (data._cf == v.a ? 'selected="selected"' : '') + '>' + v.b + '</option>');
                });
                return '<select name="tiempo_permiso" id="tiempo_permiso_' + data._a + '" style="width:120px;" class="form-control input-sm" data-id="' + data._a + '" disabled>' + ListaOpciones.join('') + '</select>';
            }
        },//Tiempo de Permiso
        {
            targets: 35,
            data: null,
            render: function (data, type, full, meta) {
                //Aprobacion de Pre-presupuesto
                if (data._ai == "True") {
                    //Fec_ingre_expe ------- Url_Rec_Tramite_Muni
                    if (data._as != null && data._as != "" && data._as != "01/01/1900" && data._at != null && data._at != "") {
                        Value = data._as;
                    }
                    else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:100px;">' + Value + '</div>';
            }
        },//Fecha De Ingreso De Expediente O Carta
        {
            targets: 36,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    if (data._at != null && data._at != "") {
                        Value = '<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" onclick="mostrarPDF(\'' + data._at + '\');" >';
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:100px;">' + Value + '</div>';
            }
        },//Recibo de Tramite Municipal
        {
            targets: 37,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    if ((data._au).length > 0) {
                        if ((data._au).length > 36) {
                            Title = 'title="' + (data._au) + '"';
                            Value = (data._au).substr(0, 33) + '...';
                        } else {
                            Title = '';
                            Value = data._au;
                        }
                    } else {
                        Title = '';
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:center;width:200px;" ' + Title + '>' + Value + '</div>';
            }
        },//Observacion
        {
            targets: 38,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Fec_Entre_Licencia   ----------------------Url_Licencia
                    if (data._av != null && data._av != "" && data._av != "01/01/1900" && data._ax != null && data._ax != "") {
                        Value = data._av;
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:90px;">' + Value + '</div>';
            }
        },//Fecha de Entrega de Permiso
        {
            targets: 39,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Fec_Cadu_Licencia   ----------------------Url_Licencia
                    if (data._aw != null && data._aw != "" && data._aw != "01/01/1900" && data._ax != null && data._ax != "") {
                        Value = data._aw;
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:90px;">' + Value + '</div>';
            }
        },//Fecha de Caducidad de Permiso
        {
            targets: 40,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Url_Licencia
                    if (data._ax != null && data._ax != "") {
                        Value = '<img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" onclick="mostrarPDF(\'' + data._ax + '\');">'
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:60px;">' + Value + '</div>';
            }
        },//Permiso
        {
            targets: 41,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //p3_monto_presupuesto_final
                    if (data._bw != null && data._bw != "") {
                        Value = data._bw;
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:80px;">' + Value + '</div>';
            }
        },//Monto
        {
            targets: 42,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Url_Presu_Final
                    if (data._az != "" && data._az != null) {
                        Value = '<a href="' + Url.Xplora + data._az + '" title="' + data._bx + '"><img src="' + Url.Xplora + 'Img/logoexcel.png" /></a><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + data._bx + '">' + data._bx + '</span>'
                    } else {
                        Value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:80px;">' + Value + '</div>';
            }
        },//Presupuesto de Tramite
        {
            targets: 43,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            }
        },//Renovacion de Tramites
        {
            targets: 44,
            data: null,
            render: function (data, type, full, meta) {
                if (data._ai == "True") {
                    //Historico
                    Value = '<a href="javascript:" class="cls-hitorico-renovacion" data-id="' + data._a + '">Ver Historico</a>';
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            }
        },//Historico Tramites
        {
            targets: 45,
            data: null,
            render: function (data, type, full, meta) {
                //Url_Report_Fotografico
                if (data._ay != null && data._ay != "" && data._bt != null && data._bt != "") {
                    Extra = 'data-doc="' + data._ay + '"';
                    Style = 'style="font-weight:bold;"';
                    Value = '<a href="javascript:" id="id-a-repfot-' + data._a + '" ' + Extra + ' data-id="' + data._a + '" class="cls-a-repfot-presu" data-op="1" ' + Style + '><i class="fa fa-picture-o fa-2x" aria-hidden="true"></i></a>';
                } else if (data._bt != null && data._bt != "" && (data._ay == null || data._ay == "")) {
                    Extra = 'data-doc=""';
                    Style = 'style="font-weight:bold;"';
                    Value = '<a href="javascript:" id="id-a-repfot-' + data._a + '" ' + Extra + ' data-id="' + data._a + '" class="cls-a-repfot-presu" data-op="1" ' + Style + '>Adjuntar</a>';
                } else {
                    Extra = 'data-doc=""';
                    Style = 'style="font-weight:bold;display:none;"';
                    Value = '<a href="javascript:" id="id-a-repfot-' + data._a + '" ' + Extra + ' data-id="' + data._a + '" class="cls-a-repfot-presu" data-op="1" ' + Style + '>Adjuntar</a>';
                }
                return '<div style="text-align:center;width:70px;">' + Value + '</div>';
            }
        },//Reporte Fotografico
        {
            targets: 46,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 47,
            data: '_bb',
            render: function (data, type, full, meta) {
                if (data != null && data != "") {
                    switch (data) {
                        case "True":
                            Value = '<span style="color:blue;font-weight:bold;">Aprobado</span>';
                            break;
                        case "False":
                            Value = '<span style="color:red;font-weight:bold;">Desaprobado</span>';
                            break;
                    }
                } else {
                    Value = '&nbsp;';
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            }
        },//Validacion de fachada
        {
            targets: 48,
            data: null,
            render: function (data, type, full, meta) {
                if (data._bc != null && data._bc != '') {
                    Value = data._bc;
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:70px;" id="id-sp-fec-vali-' + data._a + '" >' + Value + '</div>';
            }
        }//Fecha de Validacion
    ]
}
function fnArrayPerfilAnalisTramite() {
    var Title,
        Value,
        Style,
        Class,
        Result, ListaOpciones = [];
    ArrayDataTableColumnsDefs = [
        {
            targets: 0,
            data: '_cg',
            render: function (data, type, full, meta) {
                if (data == 1) {
                    Title = 'Implementacion';
                    Value = 'I';
                } else {
                    if (data == 2) {
                        Title = 'Mantenimiento';
                        Value = 'M';
                    } else {
                        if (data = 3) {
                            Title = 'Tramite';
                            Value = 'T';
                        } else {
                            Title = '';
                            Value = '';
                        }
                    }
                }
                return '<div class="cls-TipoSolicitud" title="' + Title + '"><span class="cls-texto">' + Value + '</span></div>';
            }
        },//Tipo de Solicitud
        {
            targets: 1,
            data: null,
            render: function (data, type, full, meta) {
                if (data._di < 10) {
                    Value = "WF_0000" + data._di;
                } else {
                    if (data._di >= 10 && data._di < 100) {
                        Value = "WF_000" + data._di;
                    }
                    else {
                        if (data._di >= 100 && data._di < 1000) {
                            Value = "WF_00" + data._di;
                        }
                        else {
                            if (data._di >= 1000 && data._di < 10000) {
                                Value = "WF_0" + data._di;
                            }
                            else {
                                if (data._di >= 10000) {
                                    Value = "WF_" + data._di;
                                }
                                else {
                                    Value = "";
                                }
                            }
                        }
                    }
                }
                return '<div style="text-align:center;" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Id del Registro
        {
            targets: 2,
            data: '_bj',
            render: function (data, type, full, meta) {
                if (data.length > 0) {
                    if (data.length > 14) {
                        Title = 'title="' + data + '"';
                        Value = data.substr(0, 10) + "...";
                    } else {
                        Title = '';
                        Value = data;
                    }
                } else {
                    Title = '';
                    Value = '--';
                }
                return '<div style="text-align:center;width:80px;" ' + Title + ' >' + Value + '</div>';
            }
        },//Solicitado Por
        {
            targets: 3,
            data: '_b',
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:60px;">' + data + '</div>';
            }
        },//Fecha de Solicitud
        {
            targets: 4,
            data: '_c',
            render: function (data, type, full, meta) {
                if (data.length > 0) {
                    if (data.length > 36) {
                        Title = 'title="' + data + '"';
                        Value = data.substr(0, 33) + "...";
                    } else {
                        Title = '';
                        Value = data;
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:left;width:195px;" ' + Title + ' >' + Value + '</div>';
            }
        },//Punto de Venta
        {
            targets: 5,
            data: '_f',
            render: function (data, type, full, meta) {
                if (($.trim(data)).length > 0) {
                    if (($.trim(data)).length > 36) {
                        Title = 'title="' + ($.trim(data)) + '"';
                        Value = ($.trim(data)).substr(0, 33) + "...";
                    } else {
                        Title = '';
                        Value = $.trim(data);
                    }
                } else {
                    Title = '';
                    Value = '';
                }
                return '<div style="text-align:left;width:200px" ' + Title + ' >' + Value + '</div>';
            }
        },//Direccion
        {
            targets: 6,
            data: null,
            render: function (data, type, full, meta) {
                return '<div style="text-align:center;width:55px;"><a href="javascript:" class="cls-ver-datos" data-idreg="' + data._a + '" data-ids="' + data._a + '" data-pdv="' + data._c + '" data-ruc="' + data._d + '" data-cod="' + data._e + '" data-direc="' + data._f + '" data-dist="' + data._g + '" data-prov="' + data._h + '" data-dep="' + data._i + '" data-cont="' + data._j + '" data-telf="' + data._k + '" data-celu="' + data._l + '" data-distrib="' + data._m + '" data-img-1="' + data._n + '" data-m1="' + data._o + '" data-m2="' + data._p + '" data-m3="' + data._q + '" data-m4="' + data._r + '" data-estado="' + data._s + '" data-p1zona="' + data._bk + '" data-p1comen="' + data._bl + '" data-p1tsolic="' + data._bn + '" data-marca="' + data._bv + '" data-tipo-gralsolicitud="' + data._cg + '" data-estado-solicitud="' + data._ch + '" data-referencia="' + data._dj + '">Ver</a></div>';
            }
        },//Datos del PDV
        {
            targets: 7,
            data: null,
            render: function (data, type, full, meta) {
                if (data._s == 'Pendiente') {
                    Style = 'style="font-weight: bold; color:red;text-align:center;width:60px;"';
                } else {
                    Style = 'style="text-align:center;width:60px;"';
                }
                return '<div ' + Style + '>' + data._s + '</div>';
            }
        },//Estado Solicitud Ventas
        {
            targets: 8,
            data: null,
            render: function (data, type, full, meta) {
                return '';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                $(td).addClass('col-separador');
            }
        },//Separador
        {
            targets: 9,
            data: null,
            render: function (data, type, full, meta) {
                var VarFecha = '';
                if (data._cg == 3) {
                    Value = '';
                } else {
                    if (data._bt != null) {
                        var ArrayFecha = data._bt.split('/');
                        VarFecha = ArrayFecha[2] + '-' + ArrayFecha[1] + '-' + ArrayFecha[0];
                    }
                    if (data._bt != null && data._bt != "") {
                        Value = '<input type="date" class="form-control input-sm" value="' + VarFecha + '" disabled="disabled" />'
                    } else {
                        Value = '';
                    }
                }
                return '<div style="text-align:left;">' + Value + '</div>';
            },
            createdCell: function (td, cellData, rowData, row, col) {
                if (rowData._cg == 3) {
                    $(td).css('background-color', '#E6E6E6 !important');
                }
            }
        },//Fecha de Implementacion
        {
            targets: 10,
            data: null,
            render: function (data, type, full, meta) {
                //Apro_pre_Presu
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    Value = '<a href="javascript:" class="cls-ver-docs cls-ver-doc-' + data._a + '" data-id="' + data._a + '" style="font-weight:bold;">Ver</a>';
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:90px;">' + Value + '</div>';
            }
        },//Status Documentario
        {
            targets: 11,
            data: null,
            render: function (data, type, full, meta) {
                ListaOpciones = [];
                $.each(Lista.TipoTramite, function (i, v) {
                    ListaOpciones.push('<option value="' + v.a + '" ' + (data._ce == v.a ? 'selected="selected"' : '') + '>' + v.b + '</option>');
                });
                return '<select name="tipo_tramite" id="tipo_tramite_' + data._a + '" style="width:120px;" class="cls-tipotramite form-control input-sm" data-id="' + data._a + '">' + ListaOpciones.join('') + '</select>';
            }
        },//Tipo de Tramite
        {
            targets: 12,
            data: null,
            render: function (data, type, full, meta) {
                ListaOpciones = [];
                $.each(Lista.TiempoPermiso, function (i, v) {
                    ListaOpciones.push('<option value="' + v.a + '" ' + (data._cf == v.a ? 'selected="selected"' : '') + '>' + v.b + '</option>');
                });
                return '<select name="tiempo_permiso" id="tiempo_permiso_' + data._a + '" style="width:120px;" class="cls-tiempopermiso form-control input-sm" data-id="' + data._a + '">' + ListaOpciones.join('') + '</select>';
            }
        },//Tiempo de Permiso
        {
            targets: 13,
            data: null,
            render: function (data, type, full, meta) {
                //Aprobacion de Pre-presupuesto
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    //Fec_ingre_expe ------- Url_Rec_Tramite_Muni
                    if (data._as != null && data._as != "" && data._as != "01/01/1900" && data._at != null && data._at != "") {
                        Value = '<span id="id-td-fec_ing_expe-' + data._a + '">' + data._as + '</span>';
                    } else {
                        Value = '<span id="id-td-fec_ing_expe-' + data._a + '"><a href="javascript:">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span>';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:100px;" class="cls-p7-tramite" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Fecha De Ingreso De Expediente O Carta
        {
            targets: 14,
            data: null,
            render: function (data, type, full, meta) {
                //Aprobacion de Pre-presupuesto
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    //Url_Rec_Tramite_Muni
                    if (data._at != null && data._at != "") {
                        Value = '<span id="id-td-reg-tramite-' + data._a + '"><img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" /></span>';
                    } else {
                        Value = '<span id="id-td-reg-tramite-' + data._a + '"><a href="javascript:">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span>';
                    }
                } else {
                    Value = ' ';
                }
                return '<div style="text-align:center;width:100px;"  class="cls-p7-tramite" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Recibo de Tramite Municipal
        {
            targets: 15,
            data: null,
            render: function (data, type, full, meta) {
                //Aprobacion de Pre-presupuesto
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    //Obs_Tramite
                    if (data._au != null && data._au != "") {
                        Value = '<span id="id-td-obs-tramite-' + data._a + '">' + data._au + '</span>';
                    } else {
                        Value = '<span id="id-td-obs-tramite-' + data._a + '"><a href="javascript:">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span>';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:200px;" class="cls-p7-tramite" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Observacion
        {
            targets: 16,
            data: null,
            render: function (data, type, full, meta) {
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    //Fec_Entre_Licencia   ----------------------Url_Licencia
                    if (data._av != null && data._av != "" && data._av != "01/01/1900" && data._ax != null && data._ax != "") {
                        Value = '<span id="id-td-fec_ent-licen-' + data._a + '">' + data._av + '</span>';
                    } else {
                        Value = '<span id="id-td-fec_ent-licen-' + data._a + '"><a href="javascript:">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span>';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:90px;" class="cls-p7-tramite" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Fecha de Entrega de Permiso
        {
            targets: 17,
            data: null,
            render: function (data, type, full, meta) {
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    //Fec_Cadu_Licencia   ----------------------Url_Licencia
                    if (data._aw != null && data._aw != "" && data._aw != "01/01/1900" && data._ax != null && data._ax != "") {
                        Value = '<span id="id-td-fec_cadu-licen-' + data._a + '" >' + data._aw + '</span>';
                    } else {
                        Value = '<span id="id-td-fec_cadu-licen-' + data._a + '"><a href="javascript:">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span>';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:90px;" class="cls-p7-tramite" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Fecha de Caducidad de Permiso
        {
            targets: 18,
            data: null,
            render: function (data, type, full, meta) {
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    //Url_Licencia
                    if (data._ax != null && data._ax != "") {
                        Value = '<span id="id-td-url-licen-' + data._a + '" ><img src="' + Url.Xplora + 'Img/ipdf.png"  class="cls-img-td" /></span>'
                    } else {
                        Value = '<span id="id-td-url-licen-' + data._a + '" ><a href="javascript:">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span>';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:60px;" class="cls-p7-tramite" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Permiso
        {
            targets: 19,
            data: null,
            render: function (data, type, full, meta) {
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    //p3_monto_presupuesto_final
                    if (data._bw != null && data._bw != "") {
                        Value = '<span id="id-td-monto-pto-final-' + data._a + '" >' + data._bw + '</span>';
                    } else {
                        Value = '<span id="id-td-monto-pto-final-' + data._a + '" ><a href="javascript:">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span>';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:80px;" class="cls-p7-tramite" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Monto
        {
            targets: 20,
            data: null,
            render: function (data, type, full, meta) {
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    //p3_codigo_presupuesto_final
                    if (data._bx == null || data._bx == "") {
                        //Url_Presu_Final
                        if (data._az == null || data._az == "") {
                            Value = '<span id="id-td-url-pto-final-' + data._a + '" ><a href="javascript:">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span>';
                        } else {
                            Value = '<span id="id-td-url-pto-final-' + data._a + '" ><img src="' + Url.Xplora + 'Img/logoexcel.png" /></span>';
                        }
                    } else {
                        if (data._az == null || data._az == "") {
                            Value = '<span id="id-td-url-pto-final-' + data._a + '" ><span class="label label-success" style="font-size: 100% !Important;" title="' + data._bx + '">' + data._bx + '</span></span>';
                        } else {
                            Value = '<span id="id-td-url-pto-final-' + data._a + '" ><img src="' + Url.Xplora + 'Img/logoexcel.png" /><br /><span class="label label-success" style="font-size: 100% !Important;" title="' + data._bx + '">' + data._bx + '</span></span>';
                        }
                    }
                } else {
                    Value = '';
                }

                return '<div style="text-align:center;width:80px;" class="cls-p7-tramite" data-id="' + data._a + '">' + Value + '</div>';
            }
        },//Presupuesto de Tramite
        {
            targets: 21,
            data: null,
            render: function (data, type, full, meta) {
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    //Fec_Cadu_Licencia-----------------Url_Licencia
                    if (data._aw != null && data._aw != "" && data._aw != "01/01/1900" && data._ax != null && data._ax != "") {
                        Value = '<a href="javascript:" class="cls-p7-renovacion" data-id="' + data._a + '" data-fec-ingreso-expe="' + data._as + '" data-url-rec-tramite="' + data._at + '" data-obs_tramite="' + data._au + '" data-fec-entre-licencia="' + data._av + '" data-fec-cadu-licencia="' + data._aw + '" data-url-licencia="' + data._ax + '" data-p3-monto-presupuesto-final="' + data._bw + '" data-p3-codigo-presupuesto-final="' + data._bx + '" data-url-presu-final="' + data._az + '">Renovar</a>'
                    } else {
                        Value = '';
                    }
                } else {
                    Value = '';
                }
                return '<div style="text-align:center" id="id-renovacion-' + data._a + '">' + Value + '</div>';
            }
        },//Renovacion de Tramites
        {
            targets: 22,
            data: null,
            render: function (data, type, full, meta) {
                if ((data._cg != 3 && data._ai == "True") || (data._cg == 3)) {
                    Value = '<a href="javascript:" class="cls-hitorico-renovacion" data-id="' + data._a + '">Ver Historico</a>';
                } else {
                    Value = '';
                }
                return '<div style="text-align:center;width:74px;">' + Value + '</div>';
            }
        }//Historico Tramites
    ]
}
function fnConsultarResumenPresupuesto() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#tb-wf-resumen tbody').empty();
        },
        url: 'GetResumenPresuGrilla',
        type: 'POST',
        dataType: 'json',
        async: true,
        data: { id_planning: Campania.Idplanning }, success: function (s) {
            if (s == null) return;

            $.each(s.Archivo, function (key, value) {
                $("#tb-wf-resumen tbody").append('<tr>\
                            <td>' + value.Tipo_Solicitud + '</td>\
                            <td class="text-center">' + value.Monto_Soles + '</td>\
                            <td class="text-center">' + value.Porc_Soles + '</td>\
                            <td class="text-center">' + value.Cantidad_Solicitud + '</td>\
                            <td class="text-center">' + value.Porc_Cantidad_Solicitud + '</td>\
                        </tr>');
            });
        },
        complete: function (c) { }, error: function (e) {
            console.error(e);
        }
    });
}
function fnOnChangeFechaVisita(id) {
    var fecha = $('#txt_fecha_entrega_foto_' + id).val();
    var Arrayfecha, fecha_final;
    if (fecha != '') {
        Arrayfecha = fecha.split('-');
        fecha_final = Arrayfecha[2] + '/' + Arrayfecha[1] + '/' + Arrayfecha[0];

        $.ajax({
            url: 'wf_una_fechaentrega_fotomontaje',
            type: 'POST',
            datatype: "json",
            data: {
                __a: id,
                __b: fecha_final
            },
            type: "post",
            success: function (response) {
                if (response.CodStatus == 1) {
                    alert('.:: fecha entrega actualizado con exito!!! ::.');
                } else {
                    alert("error inesperado, intentelo nuevamente.");
                }
            }
        });
    } else {
        return false;
    }
}
function fnOnChangeFechaPresupuesto(id) {
    var fecha = $('#txt_fecha_p5_trade_' + id).val();
    var Arrayfecha, fecha_final;
    if (fecha != '') {
        Arrayfecha = fecha.split('-');
        fecha_final = Arrayfecha[2] + '/' + Arrayfecha[1] + '/' + Arrayfecha[0];

        $.ajax({
            url: 'WF_Una_FechaImplementacion',
            type: 'POST',
            dataType: "json",
            data: {
                __a: 7,
                __b: fecha_final,
                __c: id
            },
            type: "POST",
            success: function (response) {
                alert('.:: Fecha actualizada con exito!!! ::.');
            }
        });
    } else {
        return false;
    }
}
function fnOnChangeFechaImplementacion(id) {
    var fecha = $('#txt_fecha_implementacion_' + id).val();
    var Arrayfecha, fecha_final;
    if (fecha != '') {
        Arrayfecha = fecha.split('-');
        fecha_final = Arrayfecha[2] + '/' + Arrayfecha[1] + '/' + Arrayfecha[0];
        $.ajax({
            url: 'WF_Una_FechaImplementacion',
            dataType: "json",
            data: {
                __a: 3,
                __b: fecha_final,
                __c: id
            },
            type: "POST",
            success: function (response) {
                alert('.:: Fecha de implementacion actualizado con exito!!! ::.');
            }
        });
    } else {
        return false;
    }
}
function fnOnClickGeneralSolicitud(objeto) {
    var AuxButon = $(objeto).data('code');

    $('#btn-group-general-solicitud button')
        .removeClass()
        .addClass('btn btn-default');

    $(objeto)
        .removeClass()
        .addClass('btn btn-primary');

    $('#auxbuton').val(AuxButon);
}
function fnOnLoadTableFachada(data) {
    return {
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        ordering: false,
        info: false,
        searching: false,
        data: data,
        columnDefs: ArrayDataTableColumnsDefs,
        fixedColumns: {
            leftColumns: 5
        }
    };
}
function fndesunacem() {
    var Filtro = [],
        _vfechaventas,
        _vfechafotos,
        _vfechapresupuestos,
        _vArrayfechaventas,
        _vArrayfechafotos,
        _vArrayfechapresupuestos,
        cbo_mes_prepresupuesto,
        cbo_anio_prepresupuesto;

    _vfechaventas = $('#txt_fecha_apro_ventas').val();
    _vfechafotos = $('#txt_fecha_apro_foto').val();

    cbo_mes_prepresupuesto = $('#cbo_mes_wf').val();
    cbo_anio_prepresupuesto = $('#cbo_anio_wf').val();

    if (cbo_mes_prepresupuesto > 0 && cbo_mes_prepresupuesto < 13) {
        _vfechapresupuestos = "01/" + cbo_mes_prepresupuesto + "/" + cbo_anio_prepresupuesto;
    } else {
        _vfechapresupuestos = "";
    }

    if (_vfechaventas == "") {
        _vfechaventas = "0";
        _vArrayfechaventas = [];
    } else {
        _vArrayfechaventas = _vfechaventas.split("-");
        _vfechaventas = _vArrayfechaventas[2] + "/" + _vArrayfechaventas[1] + "/" + _vArrayfechaventas[0];
    }
    if (_vfechafotos == "") {
        _vfechafotos = "0";
    } else {
        _vArrayfechafotos = _vfechafotos.split("-");
        _vfechafotos = _vArrayfechafotos[2] + "/" + _vArrayfechafotos[1] + "/" + _vArrayfechafotos[0];
    }
    if (_vfechapresupuestos == "") {
        _vfechapresupuestos = "0";
    }

    $.ajax({
        async: false,
        beforeSend: function (xhr) {
            $('#lucky-load').modal('show');
        },
        url: 'WFExcel',
        type: 'POST',
        data: {
            __a: $('#cbo_zona_wf').val(),
            __b: _vfechaventas,
            __c: _vfechafotos,
            //__d: $('#cod_ditribuidora_wf').val(),
            __d: '0',
            __e: $('#cod_usuario_wf').val(),
            __f: _vfechapresupuestos,
            __i: Campania.Idplanning,
            __g: $('#cod_estado_solicitud_wf').val(),
            __h: $('#cod_marca').val(),
            __j: Persona.Grupo
        },
        success: function (response) {
            $('#lucky-load').modal('hide');
            if (response.Archivo === '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(Url.Temp + response.Archivo);
            }
        },
        error: function (xhr) {
            $('#lucky-load').modal('hide');
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}