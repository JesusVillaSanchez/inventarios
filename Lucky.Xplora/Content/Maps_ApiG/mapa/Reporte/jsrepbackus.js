﻿/* Inicio : Filtros iniciales */
//191
function funct_periodo() {

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "3"
            , __b: _g_veq + ",191," + $("#cbo_anio").val() + "," + $("#cbo_mes").val()
            , __c: "cbo_periodo"
        },
        success: function (response) {
            $('#cbo_periodo').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });

}

$(document).on('change', '#cbo_categoria_right', function (e) {
    if (_v_g_report_popup == 'Presencia Producto') {
        fnPresenciaSkuPanel();
    }
    if (_v_g_report_popup == 'Presencia Visibilidad') {
        fnMarca();
    }
    if (_v_g_report_popup == 'Tracking de Precio') {
        fnTrackingPrecio();
    }
    if (_v_g_report_popup == "Share Of Display") {
        if ($(this).val() == '10066') {
            fnFiltroSod('1', '');
        } else {
            $('#cbo_nivel_right').parent().parent().hide();
        }
    }
    if (_v_g_report_popup == 'Stock Out') {
        fnStockOut();
    }
    if (_v_g_report_popup == 'Actividad Competencia') {
        fnActividadCompetenciaPanel();
    }
});
$(document).on('click', '#btn_consultar_sod', function (e) {
    fnShareOfDisplay();
    $('#myModal_SOD').modal({
        keyboard: false
        , backdrop: 'static'
    });
});
$(document).on('click', '#btn_consultar_repoperativo', function (e) {
    $('#reporte1 .cls-descargar').hide();
    fnStockOutRepOperativo();
});
$(document).on('click', '#btn_consultar_rangkingpdv', function (e) {
    $('#reporte2 .cls-descargar').hide();
    fnStockOutRepRangPDVOperativo();
});
$(document).on('click', '#btn_consultar_rangkingsku', function (e) {
    $('#reporte3 .cls-descargar').hide();
    fnStockOutRepRangSKUOperativo();
});
$(document).on('change', '#cbo_canal', function (e) {
    fnCadena();
});


function fnModalOperativo() {
    $('#reporte1 .cls-descargar').hide();
    $('#reporte1 .cls_table').html('');
    $('#reporte2 .cls-descargar').hide();
    $('#reporte2 .cls_table').html('');
    $('#reporte3 .cls-descargar').hide();
    $('#reporte3 .cls_table').html('');
    $('#myModal_Operativo').modal({
        keyboard: false
        , backdrop: 'static'
    });
    fnFiltrosOperativo(4, $('#cbo_periodo').val()); //Fechas
    fnFiltrosOperativo(5, ''); //Tipo Quiebre
}
$(function () {

    $('#cbo_fechas_ope').multiselect({
        buttonClass: 'btn btn btn-md',
        nonSelectedText: '-- Seleccione --',
        nSelectedText: ' seleccionados.',
        allSelectedText: '-- Todo --',
        numberDisplayed: 1,
        includeSelectAllOption: false,
        selectAllText: '-- Todo --',
        disableIfEmpty: true
    });
    //_removeMultiSelect('cbo_fechas_ope');





    //$.each($('#cbo_fechas_ope option:selected').map(function (a, i) { return i.value; }), function (i, v) {
    //    $propio.push(v);
    //});

});
/* Fin : Filtros iniciales */


function funct_reporte(velemento) {
    map.removeMarkers();
    $("#dv_rep_panel_filtros > div").hide();
    $("#sp_nom_reporte").html(velemento);
    if (velemento == "Cobertura") {
        $(".cls_filtro_rep").hide();
        fnCoberturaPanel();
    }
    if (velemento == "Presencia Producto") {
        $(".cls_filtro_rep").show();
        fnCategoria(163);
        fnPresenciaSkuPanel();
    }
    if (velemento == "Presencia Visibilidad") {
        $(".cls_filtro_rep").show();
        fnCategoria(66);
        fnMarca();
        fnPresenciaVisibilidadPanel();
    }
    if (velemento == "Tracking de Precio") {
        $(".cls_filtro_rep").show();
        fnCategoria(19);
        fnTrackingPrecio();
    }
    if (velemento == "Share Of Display") {
        $(".cls_filtro_rep").show();
        fnCategoria(19);
        $("#cbo_categoria_right option[value=0]").remove();
        fnFiltroSod('2', ''); //elemento
        $('#btn_consultar_sod').parent().parent().show();
        //fnTrackingPrecio();
    }
    if (velemento == 'Stock Out') {
        $(".cls_filtro_rep").show();
        fnCategoria(163);
        fnStockOut();
    }
    if (velemento == "Actividad Competencia") {
        //YR
        $(".cls_filtro_rep").show();
        fnCategoria(163);
        fnActividadCompetenciaPanel();
    }

}

/* Inicio   : Reportes Panel */
function fnCoberturaPanel() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'CoberturaBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPresenciaSkuPanel() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'PresenciaProductoBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPresenciaVisibilidadPanel() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'PresenciaVisibilidadBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vmarca: $('#cbo_marca_right').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnTrackingPrecio() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'TrackingPrecioBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnShareOfDisplay() {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'ShareOfDisplayBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _velemento: $('#cbo_tipo_elemento_right').val()
            , _vnivel: $('#cbo_nivel_right').val()
        },
        success: function (response) {
            if (response != null) {
                var _Categories = [];
                var _data_graf = [];
                var _data_item_graf = [];

                $.each(response, function (key, value) {
                    _Categories.push(value.b);
                });
                $.each(response, function (key, value) {
                    _data_item_graf.push(parseFloat(value.c));
                });
                $('#myModal_SOD .modal-body').append('<div style="width:63%;"></div>');
                $('#myModal_SOD .modal-body > div').highcharts({
                    chart: { height: 350,width: 840, type: 'column' },
                    credits: false, title: { text: null },
                    xAxis: { categories: _Categories, type: 'category', labels: { rotation: -60, style: { fontSize: '10px', fontFamily: 'Verdana, sans-serif' } } },
                    yAxis: { min: 0, title: { text: '' }, labels: { format: ' ' } },
                    legend: { enabled: false },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' +
                        '<td style="padding:0"><b>{point.y:.2f} %</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true, useHTML: true
                    },
                    plotOptions: { column: { pointPadding: 0.2, borderWidth: 0 }, series: { borderWidth: 0, dataLabels: { enabled: true, format: '{point.y:.2f} %' } } },
                    series: [{ data: _data_item_graf, dataLabels: { enabled: true, rotation: -60, color: 'black', align: 'center', x: 4, y: -30, style: { fontSize: '10px', fontFamily: 'Verdana, sans-serif' } } }]
                });
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });

}
function fnStockOut() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'StockOutBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

function fnStockOutRepOperativo() {
    var vfechas = '';
    $.each($('#cbo_fechas_ope option:selected').map(function (a, i) { return i.value; }), function (i, v) {
        vfechas += v + ',';
    });
    $.ajax({
        beforeSend: function (xhr) {
            $('#reporte1 .cls_table').html($("#dv_loading_general").html());
        },
        url: 'StockOutRepOpeBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vfechas: vfechas
            , _vtipoquiebre: $('#cbo_tipo_quiebre').val()
        },
        success: function (response) {
            $('#reporte1 .cls_table').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnStockOutRepRangPDVOperativo() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#reporte2 .cls_table').html($("#dv_loading_general").html());
        },
        url: 'StockOutRepRangPDVOpeBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vanio: $('#cbo_anio_ope').val()
            , _vcategoria: $('#cbo_categoria_ope').val()
            , _vtipoquiebre: $('#cbo_tipo_quiebre2').val()
        },
        success: function (response) {
            $('#reporte2 .cls_table').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnStockOutRepRangSKUOperativo() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#reporte3 .cls_table').html($("#dv_loading_general").html());
        },
        url: 'StockOutRepRangSKUOpeBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vanio: $('#cbo_anio_ope2').val()
           , _vtipoquiebre: $('#cbo_tipo_quiebre3').val()
           , _vcategoria: $('#cbo_categoria_ope2').val()
        },
        success: function (response) {
            $('#reporte3 .cls_table').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnActividadCompetenciaPanel() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'ActividadCompetenciaBK',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _vmpas_g_ubigeo.grupo
            , __b: _vmpas_g_ubigeo.codigo
            , __c: $('#cbo_cadena').val()
            , __d: $('#cbo_periodo').val()
            , __e: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}


/* Fin  : Reportes Panel */

/* Inicio   : Consulta PDV's */
function fnPdvCobertura(dato) {
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsCoberturaBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vopcobertura: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvPresenciaProducto(dato) {
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsPresenciaProductoBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });


}
function fnPdvPresenciaVisibilidad(dato) {
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsPresenciaVisibilidadBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vmarca: $('#cbo_marca_right').val()
            , _velemento: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvTrackingPrecio(dato) {
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsTrackingPrecioBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvStockOut(dato) {
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsStockOutBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin  : Consulta PDV's*/


/* Inicio   : Exportables */
function fnExportarCobertura(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarCoberturaBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vopcobertura: dato
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarPresenciaProducto(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarPresenciaProductoBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarPresenciaVisibilidad(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarPresenciaVisibilidadBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vmarca: $('#cbo_marca_right').val()
            , _velemento: dato
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarTrackingPrecio(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarTrackingPrecioBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarStockOut(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

function fnExportarStockOutOperativo() {
    fnLoading(2);
    var vfechas = '';
    $.each($('#cbo_fechas_ope option:selected').map(function (a, i) { return i.value; }), function (i, v) {
        vfechas += v + ',';
    });
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutOperativoBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vfechas: vfechas
            , _vtipoquiebre: $('#cbo_tipo_quiebre').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarStockOutRangPDVOperativo() {
    fnLoading(2);
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutOpeRangPDVBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vanio: $('#cbo_anio_ope').val()
            , _vcategoria: $('#cbo_categoria_ope').val()
            , _vtipoquiebre: $('#cbo_tipo_quiebre2').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarStockOutRangSKUOperativo() {
    fnLoading(2);
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutOpeRangSKUBackus',
        type: 'POST',
        data: {
            _vanio: $('#cbo_anio_ope2').val()
           , _vtipoquiebre: $('#cbo_tipo_quiebre3').val()
           , _vcategoria: $('#cbo_categoria_ope2').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

/* Fin  : Exportables*/

/* Inicio : Filtros Panel */

function fnCategoria(reporte) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaCategoria',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vequipo: _g_veq
            , _vreporte: reporte
        },
        success: function (response) {
            $('#cbo_categoria_right').parent().parent().show();
            $('#cbo_categoria_right').empty();
            $('#cbo_categoria_ope').empty();
            $('#cbo_categoria_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_categoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                if (reporte == 163) {
                    $('#cbo_categoria_ope').append('<option value="' + value.a + '" >' + value.b + '</option>');
                    $('#cbo_categoria_ope2').append('<option value="' + value.a + '" >' + value.b + '</option>');
                }
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnMarca() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaMarca',
        type: 'POST',
        dataType: 'Json',
        data: {
            _veq: _g_veq
            , _vcategoria: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#cbo_marca_right').parent().parent().show();
            $('#cbo_marca_right').empty();
            $('#cbo_marca_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_marca_right').append('<option value="' + value.a + '" >' + value.d + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnFiltroSod(op, parametro) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaFiltroSod',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vtipo: op
            , _vparametro: parametro
        },
        success: function (response) {
            if (op == "1") {
                $('#cbo_nivel_right').parent().parent().show();
                $('#cbo_nivel_right').empty();
                $.each(response, function (key, value) {
                    $('#cbo_nivel_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                });
            }
            if (op == "2") {
                $('#cbo_tipo_elemento_right').parent().parent().show();
                $('#cbo_tipo_elemento_right').empty();
                $.each(response, function (key, value) {
                    $('#cbo_tipo_elemento_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                });
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnFiltrosOperativo(op, parametro) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaFiltroSod',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vtipo: op
            , _vparametro: parametro
        },
        success: function (response) {
            if (op == 4) {
                _removeMultiSelect('cbo_fechas_ope');
                $.each(response, function (i, v) {
                    $('#cbo_fechas_ope').append('<option value="' + v.a + '" selected="selected">' + v.b + '</option>');
                });
                $('#cbo_fechas_ope').multiselect('rebuild');
            }
            if (op == 5) {
                $('#cbo_tipo_quiebre').empty();
                $.each(response, function (i, v) {
                    $('#cbo_tipo_quiebre').append('<option value="' + v.a + '" >' + v.b + '</option>');
                });
                $('#cbo_tipo_quiebre2').empty();
                $.each(response, function (i, v) {
                    $('#cbo_tipo_quiebre2').append('<option value="' + v.a + '" >' + v.b + '</option>');
                });
                $('#cbo_tipo_quiebre3').empty();
                $.each(response, function (i, v) {
                    $('#cbo_tipo_quiebre3').append('<option value="' + v.a + '" >' + v.b + '</option>');
                });
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnCadena() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaCadena',
        type: 'POST',
        dataType: 'Json',
        data: {
            _veq: _g_veq
            , _vcanal: $('#cbo_canal').val()
        },
        success: function (response) {
            $('#cbo_cadena').parent().parent().show();
            $('#cbo_cadena').empty();
            $.each(response, function (key, value) {
                $('#cbo_cadena').append('<option value="' + value.a + '" >' + value.b + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

/* Fin : Filtros Panel */


/* Inicio   : Reportes por PDV */
function fnReportesPdvs() {
    fnDetallePDV();
    fnFotoPDV(1);
    fnPresenciaProductoPDV();
    fnTrackingPrecioPDV();
    fnStockOutPDV();
    fnActividadCompetenciaPDV();
    fnRptFotograficoPDV();
}
function fnPresenciaProductoPDV() {
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_presenciasku").html($("#dv_loading_general").html())
        },
        url: 'PresenciaProductoBackusPDV',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: _g_vpdv
        },
        success: function (response) {
            $('#dv_content_presenciasku').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnTrackingPrecioPDV() {
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_preciosku").html($("#dv_loading_general").html())
        },
        url: 'TrackingPrecioBackusPDV',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: _g_vpdv
        },
        success: function (response) {
            $('#dv_content_preciosku').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnStockOutPDV() {
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_StockOut").html($("#dv_loading_general").html());
        },
        url: 'StockOutBackusPDV',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: _g_vpdv
        },
        success: function (response) {
            $('#dv_content_StockOut').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}


function fnActividadCompetenciaPDV() {
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_ActividadCompe").html('')
        },
        url: 'DetxPDVActividadCompetenciaBK',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _vmpas_g_ubigeo.grupo
            , __b: _vmpas_g_ubigeo.codigo
            , __c: $('#cbo_cadena').val()
            , __d: $('#cbo_periodo').val()
            , __e: $('#cbo_categoria_right').val()
            , __g: $('#cbo_canal').val()
            , __f: null
            , __h: _g_vpdv
        },
        success: function (response) {
            $('#dv_content_ActividadCompe').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnDetallePDV() {
    $.ajax({
        url: 'TrackingDetallePDV',
        type: 'POST',
        data: {
            vcodpdv: _g_vpdv,
            vperiodo: $('#cbo_periodo').val()
        },
        success: function (response) {
            fnLimpiarDetallePDV();
            if (response.Jdpdv != null) {
                $('.cls_panel_datos #pd_cod_pdv').text(response.Jdpdv.codPuntoVenta);
                $('.cls_panel_datos #pd_nom_pdv').text(response.Jdpdv.nombrePuntoVenta);
                $('.cls_panel_datos #pd_administrador').text(response.Jdpdv.nombreAdministrador);
                $('.cls_panel_datos #pd_direccion').text(response.Jdpdv.direccion);
                $('.cls_panel_datos #pd_zona').text(response.Jdpdv.sector);
                $('.cls_panel_datos #pd_distrito').text(response.Jdpdv.distrito);
                $('.cls_panel_datos #pd_email').text(response.Jdpdv.email);
                $('.cls_panel_datos #pd_cluster').text(response.Jdpdv.Cluster);
                $('.cls_panel_datos #pd_gestor').text(response.Jdpdv.nombreGestor);
                $('.cls_panel_datos #pd_ult_visita').text(response.Jdpdv.ultimaVisita);

            } else {
                fnLimpiarDetallePDV();
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnLimpiarDetallePDV() {
    $('.cls_panel_datos #pd_nom_pdv').text('');
    $('.cls_panel_datos #pd_cod_pdv').text('');
    $('.cls_panel_datos #pd_nom_pdv').text('');
    $('.cls_panel_datos #pd_administrador').text('');
    $('.cls_panel_datos #pd_direccion').text('');
    $('.cls_panel_datos #pd_zona').text('');
    $('.cls_panel_datos #pd_distrito').text('');
    $('.cls_panel_datos #pd_email').text('');
    $('.cls_panel_datos #pd_cluster').text('');
    $('.cls_panel_datos #pd_gestor').text('');
    $('.cls_panel_datos #pd_ult_visita').text('');
}
function fnFotoPDV(vcodtipo) {
    $.ajax({
        url: 'TrackingFotoPDV',
        type: 'POST',
        data: {
            vperiodo: $('#cbo_periodo').val(),
            vcodtipo: vcodtipo,
            vcodpdv: _g_vpdv
        },
        success: function (response) {
            if (response.Jfotopdv != null) {
                var vnum = 0;
                if (vcodtipo == 1) {
                    $.each(response.Jfotopdv, function (k, v) {
                        vnum++;
                        $('#dp_foto' + vnum).attr("src", String(v.nombreFoto));
                    });
                } else {

                }
            } else {

            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnRptFotograficoPDV() {
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_Fotografico").html('')
        },
        url: 'TrackingFotograficoPDV',
        type: 'POST',
        dataType: 'html',
        data: {
            vperiodo: $('#cbo_periodo').val(),
            vcodtipo: 2,
            vcodpdv: _g_vpdv
        },
        success: function (response) {
            $('#dv_content_Fotografico').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}


/* Fin  : Reportes por PDV */

/* Inicio : Utilitario 
    modificar aquí YR
*/
_vmarcadores = [];
_v_g_report_popup = null;
function fnPintarPDV(v_data) {
    map.removeMarkers();
    _vmarcadores = [];
    $.each(v_data, function (key, value) {
        map.addMarker({
            lat: parseFloat(value.c),
            lng: parseFloat(value.d),
            title: value.b,
            code: value.a,
            icon: ($('#cbo_canal').val() != "23" || $('#cbo_canal').val() != "24" || $('#cbo_canal').val() != "25") ? '/Img/iconmarkers/' + value.f + '.png' : '/Img/iconmarkers/logos/' + value.f + '.png',
            click: function (e) {
                $(".cls_op_menu_right").removeClass("active");
                $("body").removeClass("infobar-active").delay(200).queue(function () {
                    $('#myModal').modal({
                        keyboard: false
                      , backdrop: 'static'
                    });
                    $('#dv_tabs_button li').removeClass('active');
                    $('#dv_home').parent().addClass('active');
                    $('#dv_reportes_pdv_content div').removeClass('active');
                    $('#home3').addClass('active');
                    $(this).dequeue();
                }).delay(200).queue(function () {
                    _g_vpdv = e.code;
                    //funct_reportes_pdv();
                    fnReportesPdvs();
                    //map.removeMarkers();
                    $(this).dequeue();
                });
            }
        });

        if (key == (v_data.length - 1)) {
            fnLoading(2);
        }
    });
}
function fnLoading(_vop) {
    if (_vop == 1) {
        $('#MD_loading').modal({
            keyboard: false
            , backdrop: 'static'
        });
    } else {
        $('#MD_loading').modal('hide')
    }
}
/* Fin  : Utilitario */