﻿/* Inicio : Filtros iniciales */
//191
var idReporte = 58;
var totPDV = 0;//Cuenta la cantidad de PDV de May-Min
var productoSelec = "";
var totGreen = 0;
var totRed = 0;
var reporteSeleccionado = "";
var load = 0;
var listaPDVAASS=[];
function funct_valida_reporte() {
    if (idReporte == 0) {
        return false;
    } else {
        return true;
    }
}
function funct_asigna_reporte_filtro(velemento) {
    //debugger;
    
    if (velemento == "Cobertura") {
        idReporte = 58;
    }
    if (velemento == "Presencia Producto") {
        idReporte = 58;
    }
    if (velemento == "Presencia Visibilidad") {
        idReporte = 58;
    }
    if (velemento == "Tracking de Precio") {
        idReporte = 58;
    }
    if (velemento == "Share Of Shelf") {
        idReporte = 166;
    }
    if (velemento == "Actividad Competencia") {
        idReporte = 58;
    }
}

function funct_periodo() {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'FusionColgFiltrosJson',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: "5"
            , __b: _g_veq + "," + idReporte + "," + $("#cbo_anio").val() + "," + $("#cbo_mes").val()
            , __c: "cbo_periodo"
        },
        success: function (response) {
            $('#cbo_periodo').parent().parent().show();
            $('#cbo_periodo').empty();
            $.each(response, function (key, value) {
                $('#cbo_periodo').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });

}

function funct_categoriaFC(_vid, _vreporte, _vmarca, vrepreal) {
    $("#" + _vid).parent().parent().show();
    $.ajax({
        beforeSend: function (xhr) { },
        async: true,
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "5"
            , __b: _g_veq + "," + _vreporte
            , __c: _vid
        },
        success: function (response) {
            $("#" + _vid).replaceWith(response);
            $("#" + _vid).attr('data-id', _vreporte);
            $("#" + _vid).attr('data-marca', _vmarca);
            $("#" + _vid).attr('data-repreal', vrepreal);
            if (_vmarca != null && _vmarca != "") {
                funct_marca(_vmarca, _vreporte, $("#" + _vid).val(), vrepreal);
            }
            if (_vreporte == 23) {
                $("#cbo_pdv_foto_cat").trigger("change");
            }
            if (vrepreal == 21) {
                funct_segmento('cbo_segmento_right', 21);
            }
            if (_vid == "cbo_pdv_sod_cat") {
                funct_sod_x_pdv();
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_marcaFC(_vid, _vreporte, _vcategoria, vrepreal) {

    $("#" + _vid).parent().parent().show();
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "6"
            , __b: _g_veq + "," + _vreporte + "," + _vcategoria
            , __c: _vid
        },
        success: function (response) {
            $("#" + _vid).replaceWith(response);
            switch (_vid) {
                case 'cbo_pdv_pres_mar':
                    funct_pres_x_pdv();
                    break;
                case 'cbo_pdv_prec_mar':
                    funct_prec_x_pdv();
                    break;
                case 'cbo_marca_right':
                    if (vrepreal == 58) {
                        $("#" + _vid).attr('data-repreal', vrepreal);
                        funct_presencia_panel();
                    }
                    if (vrepreal == 19) {
                        $("#" + _vid).attr('data-repreal', vrepreal);
                        funct_precio_panel();
                    }

                    break;
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
$(document).ready(function () {
    $('#advanced-demo').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
});
$(document).on('click', '#btn_pdv_mapaSearch', function (e) {
    $("body").removeClass("infobar-active");
    if (funct_validar_selectPDV(1) == 0) {
        $("#btn_close_panel_reporte").trigger("click");
        funct_pdv_unicoSearch($("#txt_PDV").val());
    }
});

$(document).on('change', '#cbo_empresa_right', function (e) {
    //debugger;
    if (_v_g_report_popup == 'Presencia Visibilidad') {
        fnPresenciaVisibilidadPanel();
    }
    if (_v_g_report_popup == 'Tracking de Precio') {
        fnTrackingPrecio();
    }
});
$(document).on('change', '#cbo_subcategoria_right', function (e) {
    //debugger;
    if (_v_g_report_popup == 'Presencia Producto') {
        fnPresenciaSkuPanel();
    }
    if (_v_g_report_popup == 'Share Of Shelf') {
        fnShareOfShelf();
    }
    if (_v_g_report_popup == 'Presencia Visibilidad') {
        fnPresenciaVisibilidadPanel();
        if (_g_veq == "4111304112013")
            fnEmpresaAASS(140);
    }
    if (_v_g_report_popup == 'Tracking de Precio') {
        fnTrackingPrecio();
        if (_g_veq == "4111304112013")
            fnEmpresaAASS(19);
    }
});
$(document).on('change', '#cbo_categoria_right', function (e) {
    if (_v_g_report_popup == 'Presencia Producto') {
        fnPresenciaSkuPanel();
        fnMarca();
    }
    if (_v_g_report_popup == 'Presencia Visibilidad') {
        if (_g_veq == "4111304112013")
            fnEmpresa(140);
        else
            fnMarca();
    }
    if (_v_g_report_popup == 'Tracking de Precio') {
        fnMarca();
        fnTrackingPrecio();
    }
    if (_v_g_report_popup == 'Share Of Shelf') {
        fnShareOfShelf();
    }
    if (_v_g_report_popup == 'Actividad Competencia') {
        fnActividadCompetenciaPanel();
    }
});
$(document).on('change', '#cbo_marca_right', function (e) {
    if (_v_g_report_popup == 'Presencia Producto') {
        fnPresenciaSkuPanel();
        //fnMarca();
    }
    if (_v_g_report_popup == 'Presencia Visibilidad') {
        //fnMarca();
    }
    if (_v_g_report_popup == 'Tracking de Precio') {
        fnTrackingPrecio();
    }
    if (_v_g_report_popup == "Share Of Display") {
        if ($(this).val() == '10066') {
            fnFiltroSod('1', '');
        } else {
            $('#cbo_nivel_right').parent().parent().hide();
        }
    }
    if (_v_g_report_popup == 'Stock Out') {
        fnStockOut();
    }
    if (_v_g_report_popup == 'Actividad Competencia') {
        fnActividadCompetenciaPanel();
    }
});
$(document).on('click', '#btn_consultar_sod', function (e) {
    fnShareOfDisplay();
    $('#myModal_SOD').modal({
        keyboard: false
        , backdrop: 'static'
    });
});
$(document).on('click', '#btn_consultar_repoperativo', function (e) {
    $('#reporte1 .cls-descargar').hide();
    fnStockOutRepOperativo();
});
$(document).on('click', '#btn_consultar_rangkingpdv', function (e) {
    $('#reporte2 .cls-descargar').hide();
    fnStockOutRepRangPDVOperativo();
});
$(document).on('click', '#btn_consultar_rangkingsku', function (e) {
    $('#reporte3 .cls-descargar').hide();
    fnStockOutRepRangSKUOperativo();
});
$(document).on('change', '#cbo_canal', function (e) {
    location.href = '?_a=' + e.target.value;
    //debugger;
    //MAYORISTA: 813622482010 --1000
    //MINORISTA: 0133725102010 --1023
    //AASS: 4111304112013 --1241
    //FARMA IT: 0134226102010 --1242
    //FARMA DT: 0134126102010 --1243
    //BODEGAS: 012011092692011 --2008
    //console.log(e.target.value);
    //if (e.target.value == "813622482010" || e.target.value == "0133725102010" || e.target.value == "4111304112013" || e.target.value == "0134126102010") {
    //    if (e.target.value == "813622482010" || e.target.value == "0133725102010") {
    //        $("#label_cadena").html("Mercado");
    //    } else {
    //        $("#label_cadena").html("Cadena");
    //    }
    //    $("#div_cbo_cadena").show();
    //} else {
    //    $("#div_cbo_cadena").hide();
    //}
    //fnCadena();
});
$(document).on('change', '#cbo_mes', function (e) {
    if (!funct_valida_reporte()) {
        alert("Debe seleccionar un Reporte");
        $("#cbo_mes").val("0");
        return;
    }
    funct_periodo();
});
$(document).on('click', '#btn_close_panel_reporte', function (e) {
    $("#dv_content_leyenda").hide();
});
/* Filtros Reporte por PDV */
$(document).on('change', '#cbo_categoria_pressku_pdv', function (e) {
    fnMarcaPDV("#cbo_marca_pressku_pdv", "#cbo_categoria_pressku_pdv");
});
$(document).on('change', '#cbo_categoria_precio_pdv', function (e) {
    fnMarcaPDV("#cbo_marca_precio_pdv", "#cbo_categoria_precio_pdv");
});
$(document).on('change', '#cbo_categoria_precio_pdv', function (e) {
    fnMarcaPDV("#cbo_marca_precio_pdv", "#cbo_categoria_precio_pdv");
});
$(document).on('change', '#cbo_categoria_sos_pdv', function (e) {
    fnMarcaPDV("#cbo_marca_sos_pdv", "#cbo_categoria_sos_pdv");
});
$(document).on('change', '#cbo_categoria_actcomp_pdv', function (e) {
    fnMarcaPDV("#cbo_marca_actcomp_pdv", "#cbo_categoria_actcomp_pdv");
});
$(document).on('change', '#cbo_categoria_foto_pdv', function (e) {
    var categoria = $('#cbo_categoria_foto_pdv').val();
    fnRptFotograficoPDVCat(categoria);
});

$(document).keyup(function (ev) {
    if (ev.keyCode == 27)
        $("#myModalListPDV").modal('toggle');
});

function fnModalOperativo() {
    $('#reporte1 .cls-descargar').hide();
    $('#reporte1 .cls_table').html('');
    $('#reporte2 .cls-descargar').hide();
    $('#reporte2 .cls_table').html('');
    $('#reporte3 .cls-descargar').hide();
    $('#reporte3 .cls_table').html('');
    $('#myModal_Operativo').modal({
        keyboard: false
        , backdrop: 'static'
    });
    fnFiltrosOperativo(4, $('#cbo_periodo').val()); //Fechas
    fnFiltrosOperativo(5, ''); //Tipo Quiebre
}
$(function () {

    $('#cbo_fechas_ope').multiselect({
        buttonClass: 'btn btn btn-md',
        nonSelectedText: '-- Seleccione --',
        nSelectedText: ' seleccionados.',
        allSelectedText: '-- Todo --',
        numberDisplayed: 1,
        includeSelectAllOption: false,
        selectAllText: '-- Todo --',
        disableIfEmpty: true
    });
    //_removeMultiSelect('cbo_fechas_ope');

    //$.each($('#cbo_fechas_ope option:selected').map(function (a, i) { return i.value; }), function (i, v) {
    //    $propio.push(v);
    //});

});
/* Fin : Filtros iniciales */


function funct_reporte(velemento) {
    //debugger;
    reporteSeleccionado = velemento;
    map.removeMarkers();
    $("#dv_rep_panel_filtros > div").hide();
    $("#sp_nom_reporte").html(velemento);
    if (velemento == "Cobertura") {
        $(".cls_filtro_rep").hide();
        fnCoberturaPanel();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
        //if (_g_veq == "4111304112013") {//AASS
        //    $("#dv_content_leyenda").show();
        //    $("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Cluster Super<br /><img src="' + iconMarkers.yellow + '" />Cluster Express<br /><img src="' + iconMarkers.red + '" />Cluster Hiper');
        //}else
        //if (_g_veq == "813622482010" || _g_veq == "0133725102010") {//May-Min
        //    $("#dv_content_leyenda").show();
        //    $("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Cluster A<br /><img src="' + iconMarkers.GreenYellow + '" />Cluster A+<br /><img src="' + iconMarkers.yellow + '" />Cluster B <br /><img src="' + iconMarkers.red + '" />Cluster C');
        //} else{
        //    $("#dv_content_leyenda").show();
        //    $("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Cluster A<br /><img src="' + iconMarkers.yellow + '" />Cluster B <br /><img src="' + iconMarkers.red + '" />Cluster C');
        //}
    }
    if (velemento == "Presencia Producto") {
        $(".cls_filtro_rep").show();
        if (_g_veq == "4111304112013") {//AASS
            fnSubCategoria("58");
        } else {
            //fnCategoria(58);
            //fnMarca();
        }
        
        fnPresenciaSkuPanel();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
        //$("#dv_content_leyenda").show();
        //$("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Con Presencia<br /><img src="' + iconMarkers.red + '" />Sin Presencia');
    }
    if (velemento == "Presencia Visibilidad") {
        $(".cls_filtro_rep").show();
        if (_g_veq == "4111304112013") {//AASS
            fnSubCategoria("140");
            fnEmpresaAASS(140);
        } else {
            //fnCategoria(58);
            //fnMarca();            
        }
        //fnMarca();
        fnPresenciaVisibilidadPanel();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
        //$("#dv_content_leyenda").show();
        //$("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Con Presencia<br /><img src="' + iconMarkers.red + '" />Sin Presencia');
    }
    if (velemento == "Tracking de Precio") {
        $(".cls_filtro_rep").show();
        if (_g_veq == "4111304112013"){//AASS
            fnSubCategoria("58");
            fnEmpresaAASS(19);
        }
        //else
        //    fnCategoria(58);

        //fnMarca();
        fnTrackingPrecio();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
        //$("#dv_content_leyenda").show();
        //$("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Cumple Prec. Sug.<br /><img src="' + iconMarkers.red + '" />No cumple Prec. Sug.');
    }
    if (velemento == "Share Of Shelf") {
        $(".cls_filtro_rep").show();
        if (_g_veq == "4111304112013") {//AASS
            $("#cbo_subcategoria_right option[value=0]").remove();
            fnSubCategoriaSOS();
        } else {
            $("#cbo_categoria_right option[value=0]").remove();
            fnSubCategoria("166");
        }

        //$("#cbo_subcategoria_right option[value=0]").remove();
        //fnSubCategoria();
        
        fnShareOfShelf();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
    }
    if (velemento == "Actividad Competencia") {
        $(".cls_filtro_rep").show();
        //fnCategoria(25);//163
        //fnActividadCompetenciaPanel();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
    }

}

/* Inicio   : Reportes Panel */
function fnCoberturaPanel() {
    //debugger;
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'CoberturaColgate',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPresenciaSkuPanel() {
    var categoria = "0"; var marca = "0";
    if (_g_veq == "4111304112013") {
        categoria = $('#cbo_subcategoria_right').val()

    } else {
        //categoria = $('#cbo_categoria_right').val()
        //marca = $('#cbo_marca_right').val()
    }
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'PresenciaProductoColgate',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria:categoria
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: marca
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPresenciaVisibilidadPanel() {
    var marca = "0";
    var categoria = "0";
    if (_g_veq == "4111304112013") {
        categoria = $('#cbo_subcategoria_right').val();
        marca = $('#cbo_empresa_right').val();
    }
    //else
        //marca = $('#cbo_marca_right').val()
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'PresenciaVisibilidadColgate',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: categoria
            , _vmarca: marca
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnTrackingPrecio() {
    debugger;
    var marca = "0";
    var categoria = "0";
    if (_g_veq == "4111304112013") {
        categoria = $('#cbo_subcategoria_right').val();
        marca = $('#cbo_empresa_right').val();
    }
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'TrackingPrecioColgate',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: categoria
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: marca
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnShareOfDisplay() {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'ShareOfDisplayBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _velemento: $('#cbo_tipo_elemento_right').val()
            , _vnivel: $('#cbo_nivel_right').val()
        },
        success: function (response) {
            if (response != null) {
                var _Categories = [];
                var _data_graf = [];
                var _data_item_graf = [];

                $.each(response, function (key, value) {
                    _Categories.push(value.b);
                });
                $.each(response, function (key, value) {
                    _data_item_graf.push(parseFloat(value.c));
                });
                $('#myModal_SOD .modal-body').append('<div style="width:63%;"></div>');
                $('#myModal_SOD .modal-body > div').highcharts({
                    chart: { height: 350,width: 840, type: 'column' },
                    credits: false, title: { text: null },
                    xAxis: { categories: _Categories, type: 'category', labels: { rotation: -60, style: { fontSize: '10px', fontFamily: 'Verdana, sans-serif' } } },
                    yAxis: { min: 0, title: { text: '' }, labels: { format: ' ' } },
                    legend: { enabled: false },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' +
                        '<td style="padding:0"><b>{point.y:.2f} %</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true, useHTML: true
                    },
                    plotOptions: { column: { pointPadding: 0.2, borderWidth: 0 }, series: { borderWidth: 0, dataLabels: { enabled: true, format: '{point.y:.2f} %' } } },
                    series: [{ data: _data_item_graf, dataLabels: { enabled: true, rotation: -60, color: 'black', align: 'center', x: 4, y: -30, style: { fontSize: '10px', fontFamily: 'Verdana, sans-serif' } } }]
                });
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });

}
function fnStockOut() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'StockOutBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnStockOutRepOperativo() {
    var vfechas = '';
    $.each($('#cbo_fechas_ope option:selected').map(function (a, i) { return i.value; }), function (i, v) {
        vfechas += v + ',';
    });
    $.ajax({
        beforeSend: function (xhr) {
            $('#reporte1 .cls_table').html($("#dv_loading_general").html());
        },
        url: 'StockOutRepOpeBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vfechas: vfechas
            , _vtipoquiebre: $('#cbo_tipo_quiebre').val()
        },
        success: function (response) {
            $('#reporte1 .cls_table').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnStockOutRepRangPDVOperativo() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#reporte2 .cls_table').html($("#dv_loading_general").html());
        },
        url: 'StockOutRepRangPDVOpeBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vanio: $('#cbo_anio_ope').val()
            , _vcategoria: $('#cbo_categoria_ope').val()
            , _vtipoquiebre: $('#cbo_tipo_quiebre2').val()
        },
        success: function (response) {
            $('#reporte2 .cls_table').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnStockOutRepRangSKUOperativo() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#reporte3 .cls_table').html($("#dv_loading_general").html());
        },
        url: 'StockOutRepRangSKUOpeBackus',
        type: 'POST',
        dataType: 'html',
        data: {
            _vanio: $('#cbo_anio_ope2').val()
           , _vtipoquiebre: $('#cbo_tipo_quiebre3').val()
           , _vcategoria: $('#cbo_categoria_ope2').val()
        },
        success: function (response) {
            $('#reporte3 .cls_table').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnActividadCompetenciaPanel() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'ActividadCompetenciaColgate',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _vmpas_g_ubigeo.grupo
            , __b: _vmpas_g_ubigeo.codigo
            , __c: $('#cbo_cadena').val()
            , __d: $('#cbo_periodo').val()
            , __e: $('#cbo_categoria_right').val()
            , __f: $('#txt_PDV').val()
            , __g: $('#cbo_canal').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnShareOfShelf() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'ShareOfShelfColgate',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_subcategoria_right').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

/* Fin  : Reportes Panel */

/* Inicio   : Consulta PDV's */
function fnPdvCobertura(dato) {
    $('#txtSKU').html("");
    if (_g_veq == "4111304112013") {//AASS
        $("#dv_content_leyenda").show();
        $("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Cluster Super<br /><img src="' + iconMarkers.yellow + '" />Cluster Express<br /><img src="' + iconMarkers.red + '" />Cluster Hiper');
    } else {
        if (_g_veq == "813622482010" || _g_veq == "0133725102010") {//May-Min
            $("#dv_content_leyenda").show();
            $("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Cluster A<br /><img src="' + iconMarkers.GreenYellow + '" />Cluster A+<br /><img src="' + iconMarkers.yellow + '" />Cluster B <br /><img src="' + iconMarkers.red + '" />Cluster C');
        } else {
            $("#dv_content_leyenda").show();
            $("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Cluster A<br /><img src="' + iconMarkers.yellow + '" />Cluster B <br /><img src="' + iconMarkers.red + '" />Cluster C');
        }
    }
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsCoberturaColgate',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vopcobertura: dato
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvPresenciaProducto(dato, descProd) {
    productoSelec = descProd;
    $('#txtSKU').html("<strong>PRODUCTO : " + descProd + "</strong>");
    $("#dv_content_leyenda").show();
    $("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Con Presencia<br /><img src="' + iconMarkers.red + '" />Sin Presencia');
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsPresenciaProductoColgate',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });


}
function fnPdvPresenciaVisibilidad(dato, descProd) {
    $('#txtSKU').html("<strong>PRODUCTO : " + descProd + "</strong>");
    $("#dv_content_leyenda").show();
    $("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Con Presencia<br /><img src="' + iconMarkers.red + '" />Sin Presencia');
    var marca = "";
    if (_g_veq == "4111304112013")
        marca = $('#cbo_empresa_right').val()
    else
        marca = $('#cbo_marca_right').val()

    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsPresenciaVisibilidadColgate',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vmarca: marca
            , _velemento: dato
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvTrackingPrecio(dato, descProd) {
    $('#txtSKU').html("<strong>PRODUCTO : " + descProd + "</strong>");
    $("#dv_content_leyenda").show();
    $("#div_leyenda").html('<img src="' + iconMarkers.green + '" />Cumple Prec. Sug.<br /><img src="' + iconMarkers.red + '" />No cumple Prec. Sug.');
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsTrackingPrecioColgate',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
            , _vmarca: $('#cbo_marca_right').val()
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvStockOut(dato) {
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsStockOutBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvShareOfShelf(dato) {
    $('#txtSKU').html("");
    $("#dv_content_leyenda").hide();
    $("#div_leyenda").html("");
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsStockOutBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pdv_unicoSearch(_vpdv) {
    $('#txtSKU').html("");
    var textSearch = $('#advanced-demo').val();
    if (textSearch == "")
        $('#txt_PDV').val("");

    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsCoberturaColgate',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: "1"
            , _vubigeo: "589"
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vopcobertura: 2
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
                alert("Este PDV no registra visitas en el periodo seleccionado.");
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin  : Consulta PDV's*/


/* Inicio   : Exportables */
function fnExportarCobertura(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarCoberturaColgate',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vopcobertura: dato
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(downloadtemp.temp + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarPresenciaProducto(dato) {
    var categoria = "0"; var marca = "0";
    if (_g_veq == "4111304112013") {
        categoria = $('#cbo_subcategoria_right').val()  
    }
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarPresenciaProductoColgate',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: categoria
            , _vsku: dato
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(downloadtemp.temp + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarPresenciaProductoTodo() {
    if (_g_veq == "4111304112013") {

        fnExportarPresenciaProducto('0');
    } else {
        $.ajax({
            async: false,
            beforeSend: function (xhr) { fnLoading(1); },
            url: '../ColgateReporting/ExportaExcelConsolidadoMaps',
            type: 'POST',
            data: {
                planning: $('#cbo_canal').val(),
                periodo: $('#cbo_periodo').val(),
                cadena: $('#cbo_cadena').val(),
                pdv: "" //$('#txt_PDV').val()
            },
            success: function (response) {
                fnLoading(2);
                if (response.Archivo == '0') {
                    alert('Sin datos disponibles para descargar.');
                } else {
                    $.fileDownload(downloadtemp.temp + response.Archivo);
                }
            },
            error: function (xhr) {
                fnLoading(2);
                alert('Algo salió mal, por favor intente de nuevo.');
            }
        });
    }

}
function fnExportarPresenciaVisibilidad(dato) {
    var categoria = "0"; var marca = "0";
    if (_g_veq == "4111304112013") {
        categoria = $('#cbo_subcategoria_right').val()
        marca = $('#cbo_empresa_right').val()
    }
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarPresenciaVisibilidadColgate',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: categoria
            , _vmarca: marca
            , _velemento: dato
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(downloadtemp.temp + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarTrackingPrecio(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarTrackingPrecioColgate',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
            , _vmarca: $('#cbo_marca_right').val()
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(downloadtemp.temp + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarStockOut(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(downloadtemp.temp + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarShareOfShelf(dato) {
    console.log("Subcategoria",$('#cbo_subcategoria_right').val());
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarShareOfShelfColgate',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_subcategoria_right').val()
            , _vpdv: ""
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(downloadtemp.temp + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

function fnExportarStockOutOperativo() {
    fnLoading(2);
    var vfechas = '';
    $.each($('#cbo_fechas_ope option:selected').map(function (a, i) { return i.value; }), function (i, v) {
        vfechas += v + ',';
    });
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutOperativoBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vfechas: vfechas
            , _vtipoquiebre: $('#cbo_tipo_quiebre').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(downloadtemp.temp + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarStockOutRangPDVOperativo() {
    fnLoading(2);
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutOpeRangPDVBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vanio: $('#cbo_anio_ope').val()
            , _vcategoria: $('#cbo_categoria_ope').val()
            , _vtipoquiebre: $('#cbo_tipo_quiebre2').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(downloadtemp.temp + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarStockOutRangSKUOperativo() {
    fnLoading(2);
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutOpeRangSKUBackus',
        type: 'POST',
        data: {
            _vanio: $('#cbo_anio_ope2').val()
           , _vtipoquiebre: $('#cbo_tipo_quiebre3').val()
           , _vcategoria: $('#cbo_categoria_ope2').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(downloadtemp.temp + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

/* Fin  : Exportables*/

/* Inicio : Filtros Panel */

function fnCategoria(reporte) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaCategoria',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vequipo: _g_veq
            , _vreporte: reporte
        },
        success: function (response) {
            $('#cbo_categoria_right').parent().parent().show();
            $('#cbo_categoria_right').empty();
            $('#cbo_categoria_ope').empty();
            $('#cbo_categoria_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_categoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                if (reporte == 163) {
                    $('#cbo_categoria_ope').append('<option value="' + value.a + '" >' + value.b + '</option>');
                    $('#cbo_categoria_ope2').append('<option value="' + value.a + '" >' + value.b + '</option>');
                }
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnMarca() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaMarca',
        type: 'POST',
        dataType: 'Json',
        data: {
            _veq: _g_veq
            , _vcategoria: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#cbo_marca_right').parent().parent().show();
            $('#cbo_marca_right').empty();
            $('#cbo_marca_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_marca_right').append('<option value="' + value.a + '" >' + value.d + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnEmpresa(reporte) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaEmpresas',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq,
            __b: reporte
            , __c: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#cbo_empresa_right').parent().parent().show();
            $('#cbo_empresa_right').empty();
            $('#cbo_empresa_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                //debugger;
                $('#cbo_empresa_right').append('<option value="' + value.Text + '" >' + value.Value + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnFiltroSod(op, parametro) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaFiltroSod',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vtipo: op
            , _vparametro: parametro
        },
        success: function (response) {
            if (op == "1") {
                $('#cbo_nivel_right').parent().parent().show();
                $('#cbo_nivel_right').empty();
                $.each(response, function (key, value) {
                    $('#cbo_nivel_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                });
            }
            if (op == "2") {
                $('#cbo_tipo_elemento_right').parent().parent().show();
                $('#cbo_tipo_elemento_right').empty();
                $.each(response, function (key, value) {
                    $('#cbo_tipo_elemento_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                });
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnFiltrosOperativo(op, parametro) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaFiltroSod',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vtipo: op
            , _vparametro: parametro
        },
        success: function (response) {
            if (op == 4) {
                _removeMultiSelect('cbo_fechas_ope');
                $.each(response, function (i, v) {
                    $('#cbo_fechas_ope').append('<option value="' + v.a + '" selected="selected">' + v.b + '</option>');
                });
                $('#cbo_fechas_ope').multiselect('rebuild');
            }
            if (op == 5) {
                $('#cbo_tipo_quiebre').empty();
                $.each(response, function (i, v) {
                    $('#cbo_tipo_quiebre').append('<option value="' + v.a + '" >' + v.b + '</option>');
                });
                $('#cbo_tipo_quiebre2').empty();
                $.each(response, function (i, v) {
                    $('#cbo_tipo_quiebre2').append('<option value="' + v.a + '" >' + v.b + '</option>');
                });
                $('#cbo_tipo_quiebre3').empty();
                $.each(response, function (i, v) {
                    $('#cbo_tipo_quiebre3').append('<option value="' + v.a + '" >' + v.b + '</option>');
                });
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnCadena() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'FusionColgFiltrosJson',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: 2
            , __b: _g_veq //+","+$('#cbo_canal').val()
        },
        success: function (response) {
            //debugger;
            $('#cbo_cadena').parent().parent().show();
            $('#cbo_cadena').empty();
            //$('#cbo_cadena').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_cadena').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnCanal() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'FusionColgFiltros',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: "1",
            __b: _g_veq
        },
        success: function (response) {
            $('#cbo_canal').parent().parent().show();
            $('#cbo_canal').empty();
            $.each(response, function (key, value) {
                $('#cbo_canal').append('<option value="' + value.a + '" >' + value.b + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnSubCategoria(idReport) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'FusionColgFiltrosJson',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: "89"
            , __b: _g_veq + ',' + idReport
            //, __b: _g_veq+','+$('#cbo_periodo').val()
        },
        success: function (response) {
            debugger;
            $('#cbo_subcategoria_right').parent().parent().show();
            $('#cbo_subcategoria_right').empty();
            $('#cbo_subcategoria_right').empty();
            $('#cbo_subcategoria_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_subcategoria_right').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
                //if (reporte == 163) {
                //    $('#cbo_subcategoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                //    $('#cbo_subcategoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                //}
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnSubCategoriaSOS() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'FusionColgFiltrosJson',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: "89"
            , __b: _g_veq + ',166'
        },
        success: function (response) {
            debugger;
            $('#cbo_subcategoria_right').parent().parent().show();
            $('#cbo_subcategoria_right').empty();
            $('#cbo_subcategoria_right').empty();
            //$('#cbo_subcategoria_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_subcategoria_right').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
                //if (reporte == 163) {
                //    $('#cbo_subcategoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                //    $('#cbo_subcategoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                //}
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnEmpresaAASS(reporte) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaEmpresas',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq,
            __b: reporte
            , __c: $('#cbo_subcategoria_right').val()
        },
        success: function (response) {
            $('#cbo_empresa_right').parent().parent().show();
            $('#cbo_empresa_right').empty();
            $('#cbo_empresa_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                //debugger;
                $('#cbo_empresa_right').append('<option value="' + value.Text + '" >' + value.Value + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin : Filtros Panel */


/* Inicio   : Reportes por PDV */
function fnReportesPdvs() {
    fnDetallePDV();
    fnFotoPDV(1);
    fnPresenciaProductoPDV();
    fnPresenciaVisibilidadPDV();
    fnTrackingPrecioPDV();
    fnShareOfShelfPDV();
    //fnActividadCompetenciaPDV();
    fnRptFotograficoPDV();
}
function fnPresenciaProductoPDV() {
    //fnCategoriaPDV(58,"#cbo_categoria_pressku_pdv");
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_presenciasku").html($("#dv_loading_general").html())
        },
        url: 'PresenciaProductoColgatePDV',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            //, _vgrupo: _vmpas_g_ubigeo.grupo
            //, _vubigeo: _vmpas_g_ubigeo.codigo
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: _g_vpdv
            , _vmarca: $('#cbo_marca_pressku_pdv').val()
            , _vcategoria: $('#cbo_categoria_pressku_pdv').val()
        },
        success: function (response) {
            $('#dv_content_presenciasku').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPresenciaVisibilidadPDV() {
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_presenciavisib").html($("#dv_loading_general").html())
        },
        url: 'PresenciaVisibilidadColgatePDV',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: _g_vpdv
        },
        success: function (response) {
            $('#dv_content_presenciavisib').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnTrackingPrecioPDV() {
    //fnCategoriaPDV(58, "#cbo_categoria_precio_pdv");
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_preciosku").html($("#dv_loading_general").html())
        },
        url: 'TrackingPrecioColgatePDV',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: "1"//_vmpas_g_ubigeo.grupo
            , _vubigeo: "589"//_vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: _g_vpdv
        },
        success: function (response) {
            $('#dv_content_preciosku').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnStockOutPDV() {
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_StockOut").html($("#dv_loading_general").html());
        },
        url: 'StockOutBackusPDV',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: _g_vpdv
        },
        success: function (response) {
            $('#dv_content_StockOut').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnShareOfShelfPDV() {
    //fnCategoriaPDV(58, "#cbo_categoria_sos_pdv");
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_SOS").html($("#dv_loading_general").html());
        },
        url: 'ShareOfShelfColgatePDV',
        type: 'POST',
        dataType: 'html',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: "1"//_vmpas_g_ubigeo.grupo
            , _vubigeo: "589"//_vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: _g_vpdv
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vmarca: $('#cbo_marca_right').val()
        },
        success: function (response) {
            $('#dv_content_SOS').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnActividadCompetenciaPDV() {
    //fnCategoriaPDV(58, "#cbo_categoria_actcomp_pdv");
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_ActividadCompe").html('')
        },
        url: 'DetxPDVActividadCompetenciaFC',//'DetxPDVActividadCompetenciaBK',
        type: 'POST',
        dataType: 'html',
        data: {
            //__a: _vmpas_g_ubigeo.grupo
            //, __b: _vmpas_g_ubigeo.codigo
            __c: $('#cbo_cadena').val()
            , __d: $('#cbo_periodo').val()
            , __e: $('#cbo_categoria_right').val()
            , __g: $('#cbo_canal').val()
            , __f: null
            , __h: _g_vpdv
        },
        success: function (response) {
            $('#dv_content_ActividadCompe').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnDetallePDV() {
    $.ajax({
        url: 'TrackingDetallePDVColgate',
        type: 'POST',
        data: {
            vcodpdv: _g_vpdv,
            vperiodo: $('#cbo_periodo').val()
        },
        success: function (response) {
            fnLimpiarDetallePDV();
            if (response.Jdpdv != null) {
                $('.cls_panel_datos #pd_cod_pdv').text(response.Jdpdv.codPuntoVenta);
                $('.cls_panel_datos #pd_nom_pdv').text(response.Jdpdv.nombrePuntoVenta);
                $('.cls_panel_datos #pd_administrador').text(response.Jdpdv.nombreAdministrador);
                $('.cls_panel_datos #pd_direccion').text(response.Jdpdv.direccion);
                $('.cls_panel_datos #pd_zona').text(response.Jdpdv.sector);
                $('.cls_panel_datos #pd_distrito').text(response.Jdpdv.distrito);
                $('.cls_panel_datos #pd_email').text(response.Jdpdv.email);
                $('.cls_panel_datos #pd_cluster').text(response.Jdpdv.Cluster);
                $('.cls_panel_datos #pd_gestor').text(response.Jdpdv.nombreGestor);
                $('.cls_panel_datos #pd_ult_visita').text(response.Jdpdv.ultimaVisita);

            } else {
                fnLimpiarDetallePDV();
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnLimpiarDetallePDV() {
    $('.cls_panel_datos #pd_nom_pdv').text('');
    $('.cls_panel_datos #pd_cod_pdv').text('');
    $('.cls_panel_datos #pd_nom_pdv').text('');
    $('.cls_panel_datos #pd_administrador').text('');
    $('.cls_panel_datos #pd_direccion').text('');
    $('.cls_panel_datos #pd_zona').text('');
    $('.cls_panel_datos #pd_distrito').text('');
    $('.cls_panel_datos #pd_email').text('');
    $('.cls_panel_datos #pd_cluster').text('');
    $('.cls_panel_datos #pd_gestor').text('');
    $('.cls_panel_datos #pd_ult_visita').text('');
}
function fnFotoPDV(vcodtipo) {
    $.ajax({
        url: 'TrackingFotoPDVColgate',//TrackingFotograficoColgatePDV
        type: 'POST',
        data: {
            vperiodo: $('#cbo_periodo').val(),
            vcodtipo: vcodtipo,
            vcodpdv: _g_vpdv
        },
        success: function (response) {
            if (response.Jfotopdv != null) {
                var vnum = 0;
                if (vcodtipo == 1) {
                    $.each(response.Jfotopdv, function (k, v) {
                        vnum++;
                        $('#dp_foto' + vnum).attr("src", String(v.nombreFoto));
                    });
                } else {

                }
            } else {

            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnRptFotograficoPDV() {
    fnCategoriaPDV(23, "#cbo_categoria_foto_pdv");
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_Fotografico").html('')
        },
        url: 'TrackingFotograficoColgatePDV',
        type: 'POST',
        dataType: 'html',
        data: {
            vperiodo: $('#cbo_periodo').val(),
            vcodtipo: 2,
            vcodpdv: _g_vpdv,
            vcodcategoria: $('#cbo_categoria_foto_pdv').val()
        },
        success: function (response) {
            $('#dv_content_Fotografico').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnRptFotograficoPDVCat(codcat) {
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_Fotografico").html('')
        },
        url: 'TrackingFotograficoColgatePDV',
        type: 'POST',
        dataType: 'html',
        data: {
            vperiodo: $('#cbo_periodo').val(),
            vcodtipo: 2,
            vcodpdv: _g_vpdv,
            vcodcategoria: codcat
        },
        success: function (response) {
            $('#dv_content_Fotografico').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

/* Fin  : Reportes por PDV */

/* Filtros por PDV */
function fnCategoriaPDV(reporte,idElemento) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaCategoria',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vequipo: _g_veq
            , _vreporte: reporte
        },
        success: function (response) {
            $(idElemento).parent().parent().show();
            $(idElemento).empty();
            $(idElemento).append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $(idElemento).append('<option value="' + value.a + '" >' + value.b + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnMarcaPDV(idDivMarca, idElementoCat) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaMarca',
        type: 'POST',
        dataType: 'Json',
        data: {
            _veq: _g_veq
            , _vcategoria: $(idElementoCat).val()
        },
        success: function (response) {
            $(idDivMarca).parent().parent().show();
            $(idDivMarca).empty();
            $(idDivMarca).append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $(idDivMarca).append('<option value="' + value.a + '" >' + value.d + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin Filtros por PDV */

/* Inicio : Utilitario */
_vmarcadores = [];
_v_g_report_popup = null;
function fnPintarPDV(v_data) {
    map.removeMarkers();
    _vmarcadores = [];
    
    $.each(v_data, function (key, value) {
        //debugger;
        var color = "";
        var descripcion = "";
        if (_g_veq == "813622482010" || _g_veq == "0133725102010") {
            color = "green";
            descripcion = value.h;
        } else {
            color = value.e;
            //descripcion = value.f;
            descripcion = value.b;
        }

        map.addMarker({
            lat: parseFloat(value.c),
            lng: parseFloat(value.d),
            title: descripcion,
            code: value.a,
            desc: value.f,
            node: value.g,
            listaPDV: v_data,
            nomNode: value.h,
            icon: '~/xplora/Img/iconmarkers/' + color + '.png',
            click: function (e) {
                //Muestra la lista de PDV cuando es Mayorista y Minorista
                //debugger;
                totPDV = 0;
                totGreen = 0;
                if (e.desc == "group") {
                    $(".cls_op_menu_right").removeClass("active");
                    $("body").removeClass("infobar-active").delay(500).queue(function () {
                        $('#myModalListPDV').modal({
                            keyboard: false
                        , backdrop: 'static'
                        });
                        $(this).dequeue();
                    }).delay(500).queue(function () {
                        _g_vpdv = e.code;
                        $(this).dequeue();
                    });
                   
                    $('#trListaPDV').empty();
                    //var lista = e.listaPDV.filter(e.node);
                    $.each(e.listaPDV, function (key, value) {
                        if (e.node == value.g) {
                            $('#trListaPDV').append('<tr><td><img src="/xplora/Img/iconmarkers/' + value.e + '.png" /></td><td>' + value.a + '</td><td>' + value.b + '</td><td><button class=\"btn btn-xs\" onclick="fnMostrarReportePDV(\'' + value.a + '\')"><i class="fa fa-search"></i>  Ver  </button></td>');
                            totPDV = totPDV + 1;
                            if (value.e == "green")
                                totGreen = totGreen + 1;
                        }
                    });
                    if (reporteSeleccionado == "Presencia Producto") {
                        var porcPres = ((totGreen / totPDV) * 100).toFixed(2) + " %";
                        $('#NomNode').html("<div id='nameNodediv'>Mercado : " + e.nomNode + "<br />Presencia: " + porcPres + " <br />Producto: " + productoSelec + "</div>");
                    } else {
                        $('#NomNode').html("<div id='nameNodediv'>Mercado : " + e.nomNode + "</div>");
                    }
                } else {
                    fnLoading(1);
                    $(".cls_op_menu_right").removeClass("active");
                    $("body").removeClass("infobar-active").delay(500).queue(function () {
                        $('#myModal').modal({
                            keyboard: false
                          , backdrop: 'static'
                        });
                        $('#dv_tabs_button li').removeClass('active');
                        $('#dv_home').parent().addClass('active');
                        $('#dv_reportes_pdv_content div').removeClass('active');
                        $('#home3').addClass('active');
                        $(this).dequeue();
                    }).delay(500).queue(function () {
                        _g_vpdv = e.code;
                        //funct_reportes_pdv();
                        fnReportesPdvs();
                        //map.removeMarkers();
                        $(this).dequeue();
                        fnLoading(2);
                    });
                }
            }
        });

        if (key == (v_data.length - 1)) {
            fnLoading(2);
        }
    });
}
function fnLoading(_vop) {
    if (_vop == 1) {
        $('#MD_loading').modal({
            keyboard: false
            , backdrop: 'static'
        });
    } else {
        $('#MD_loading').modal('hide')
    }
}
/* Fin  : Utilitario */
/* Loading Page */
//fnCanal();

function fnMostrarReportePDV(cod_pdv) {
    fnLoading(1);
    $(".cls_op_menu_right").removeClass("active");
    $("body").removeClass("infobar-active").delay(500).queue(function () {
        $('#myModal').modal({
            keyboard: false
          , backdrop: 'static'
        });
        $('#dv_tabs_button li').removeClass('active');
        $('#dv_home').parent().addClass('active');
        $('#dv_reportes_pdv_content div').removeClass('active');
        $('#home3').addClass('active');
        $(this).dequeue();
    }).delay(500).queue(function () {
        _g_vpdv = cod_pdv;
        //funct_reportes_pdv();
        fnReportesPdvs();
        
        //map.removeMarkers();
        $(this).dequeue();
        fnLoading(2);
    });
}
function funct_validar_selectPDV(_vop) {
    debugger;
    var _vbody_alert = "";
    var sw = 0;
    //if (_vmpas_g_ubigeo == null) {
    //    _vbody_alert += "<li>Seleccionar capa para realizar la consulta...</li>";
    //    sw = 1;
    //}
    if ($("#cbo_anio").val() == 0 || $("#cbo_anio").val() == null) {
        _vbody_alert += "<li>Seleccionar el filtro año...</li>";
        sw = 1;
    }
    if ($("#cbo_mes").val() == 0 || $("#cbo_mes").val() == null) {
        _vbody_alert += "<li>Seleccionar el filtro mes...</li>";
        sw = 1;
    }
    if ($("#cbo_periodo").val() == null) {
        _vbody_alert += "<li>Seleccionar el filtro periodo...</li>";
        sw = 1;
    }
    if (_vop == 1) {
        if ($("#txt_PDV").val() == null || $("#txt_PDV").val() == "0" || $("#txt_PDV").val() == "") {
            _vbody_alert += "<li>Ingresar codigo de PDV...</li>";
            sw = 1;
        }
    }

    if (sw == 1) {
        $("#MD_alert_body").html(_vbody_alert);
        $("#MD_alert").modal();
    }

    return sw;
}

