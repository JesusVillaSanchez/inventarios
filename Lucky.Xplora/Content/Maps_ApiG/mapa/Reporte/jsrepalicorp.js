﻿/* Inicio : Filtros iniciales */
//191
var idReporte = 19;
var totPDV = 0;//Cuenta la cantidad de PDV de May-Min
var productoSelec = "";
var totGreen = 0;
var totRed = 0;
var reporteSeleccionado = "";
var load = 0;
var listaPDVAASS=[];
function funct_valida_reporte() {
    if (idReporte == 0) {
        return false;
    } else {
        return true;
    }
}
function funct_asigna_reporte_filtro(velemento) {
    //debugger;
    
    if (velemento == "Cobertura") {
        idReporte = 19;
    }
    if (velemento == "Tracking de Precio") {
        idReporte = 19;
    }
    if (velemento == "SOD") {
        idReporte = 19;
    }
    if (velemento == "Quiebre") {
        idReporte = 19;
    }
    if (velemento == "OSA") {
        idReporte = 19;
    }
}

function funct_periodo() {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'AlicorpAASSFiltrosJson',
        type: 'POST',
        dataType: 'json',
        data: {
            __a: "3"
            , __b: _g_veq + "," + idReporte + "," + $("#cbo_anio").val() + "," + $("#cbo_mes").val()
            , __c: "cbo_periodo"
        },
        success: function (response) {
            $('#cbo_periodo').parent().parent().show();
            $('#cbo_periodo').empty();
            $.each(response, function (key, value) {
                $('#cbo_periodo').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });

}

function funct_categoriaFC(_vid, _vreporte, _vmarca, vrepreal) {
    $("#" + _vid).parent().parent().show();
    $.ajax({
        beforeSend: function (xhr) { },
        async: true,
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "5"
            , __b: _g_veq + "," + _vreporte
            , __c: _vid
        },
        success: function (response) {
            $("#" + _vid).replaceWith(response);
            $("#" + _vid).attr('data-id', _vreporte);
            $("#" + _vid).attr('data-marca', _vmarca);
            $("#" + _vid).attr('data-repreal', vrepreal);
            if (_vmarca != null && _vmarca != "") {
                funct_marca(_vmarca, _vreporte, $("#" + _vid).val(), vrepreal);
            }
            if (_vreporte == 23) {
                $("#cbo_pdv_foto_cat").trigger("change");
            }
            if (vrepreal == 21) {
                funct_segmento('cbo_segmento_right', 21);
            }
            if (_vid == "cbo_pdv_sod_cat") {
                funct_sod_x_pdv();
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_marcaFC(_vid, _vreporte, _vcategoria, vrepreal) {

    $("#" + _vid).parent().parent().show();
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "6"
            , __b: _g_veq + "," + _vreporte + "," + _vcategoria
            , __c: _vid
        },
        success: function (response) {
            $("#" + _vid).replaceWith(response);
            switch (_vid) {
                case 'cbo_pdv_pres_mar':
                    funct_pres_x_pdv();
                    break;
                case 'cbo_pdv_prec_mar':
                    funct_prec_x_pdv();
                    break;
                case 'cbo_marca_right':
                    if (vrepreal == 58) {
                        $("#" + _vid).attr('data-repreal', vrepreal);
                        funct_presencia_panel();
                    }
                    if (vrepreal == 19) {
                        $("#" + _vid).attr('data-repreal', vrepreal);
                        funct_precio_panel();
                    }

                    break;
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
$(document).ready(function () {
    $('#advanced-demo').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
});
$(document).on('click', '#btn_pdv_mapaSearch', function (e) {
    $("body").removeClass("infobar-active");
    if (funct_validar_selectPDV(1) == 0) {
        $("#btn_close_panel_reporte").trigger("click");
        funct_pdv_unicoSearch($("#txt_PDV").val());
    }
});

$(document).on('change', '#cbo_categoria_right', function (e) {
    if (_v_g_report_popup == 'OSA') {
        fnMarca();
        fnOSAPanel();        
    }
    if (_v_g_report_popup == 'Tracking de Precio') {
        fnMarca();
        fnTrackingPrecio();
    }
    if (_v_g_report_popup == 'Quiebre') {
        fnStockOut();
    }
});
$(document).on('change', '#cbo_marca_right', function (e) {
    if (_v_g_report_popup == 'OSA') {
        fnOSAPanel();
    }
    if (_v_g_report_popup == 'Tracking de Precio') {
        fnTrackingPrecio();
    }
});
$(document).on('change', '#cbo_cluster_right', function (e) {
    if (_v_g_report_popup == 'OSA') {
        fnOSAPanel();
    }
    if (_v_g_report_popup == 'Share Of Display') {
        fnShareOfDisplay();
    }
    if (_v_g_report_popup == 'Quiebre') {
        fnStockOut();
    }
    if (_v_g_report_popup == 'Tracking de Precio') {
        fnTrackingPrecio();
    }
});
$(document).on('change', '#cbo_tiporesultado_right', function (e) {
    var tipo = $('#cbo_tiporesultado_right').val();
    if (_v_g_report_popup == 'OSA') {
        fnOSAPanel();
        if (tipo == "1" || tipo == "3")
            fnTipoCausa();
        else {
            $('#cbo_causal_right').empty();
            $('#cbo_causal_right').append('<option value="0" >Todos</option>');
            $('#cbo_causal_right').parent().parent().hide();
        }
    }
});
$(document).on('change', '#cbo_causal_right', function (e) {
    if (_v_g_report_popup == 'OSA') {
        fnOSAPanel();
    }
});
$(document).on('change', '#cbo_canal', function (e) {
    fnCadena();
});
$(document).on('click', '#btn_close_panel_reporte', function (e) {
    $("#dv_content_leyenda").hide();
});
/* Filtros Reporte por PDV */
$(function () {

    $('#cbo_fechas_ope').multiselect({
        buttonClass: 'btn btn btn-md',
        nonSelectedText: '-- Seleccione --',
        nSelectedText: ' seleccionados.',
        allSelectedText: '-- Todo --',
        numberDisplayed: 1,
        includeSelectAllOption: false,
        selectAllText: '-- Todo --',
        disableIfEmpty: true
    });
    //_removeMultiSelect('cbo_fechas_ope');

    //$.each($('#cbo_fechas_ope option:selected').map(function (a, i) { return i.value; }), function (i, v) {
    //    $propio.push(v);
    //});

});
/* Fin : Filtros iniciales */


function funct_reporte(velemento) {
    //debugger;
    reporteSeleccionado = velemento;
    map.removeMarkers();
    $("#dv_rep_panel_filtros > div").hide();
    $("#sp_nom_reporte").html(velemento);
    if (velemento == "Cobertura") {
        $(".cls_filtro_rep").hide();
        fnCoberturaPanel();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
    }
    if (velemento == "OSA") {
        $(".cls_filtro_rep").show();
        fnCategoria(19);
        fnMarca();
        fnTipoResultado();
        fnCluster();
        
        fnOSAPanel();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
    }
    if (velemento == "Quiebre") {
        $(".cls_filtro_rep").show();
        fnCategoria(19);
        fnCluster();

        fnStockOut();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
        //$("#dv_content_leyenda").show();
        //$("#div_leyenda").html('<img src="/Img/iconmarkers/green.png" />Con Presencia<br /><img src="/Img/iconmarkers/red.png" />Sin Presencia');
    }
    if (velemento == "Tracking de Precio") {
        $(".cls_filtro_rep").show();
        fnCategoria(19);
        fnMarca();
        fnCluster();
        fnTrackingPrecio();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
        //$("#dv_content_leyenda").show();
        //$("#div_leyenda").html('<img src="/Img/iconmarkers/green.png" />Cumple Prec. Sug.<br /><img src="/Img/iconmarkers/red.png" />No cumple Prec. Sug.');
    }
    if (velemento == "SOD") {
        $(".cls_filtro_rep").show();
        fnCluster();
        
        fnShareOfDisplay();
        $('#txtSKU').html("");
        $("#dv_content_leyenda").hide();
        $("#div_leyenda").html("");
    }

}

/* Inicio   : Reportes Panel */
function fnCoberturaPanel() {
    //debugger;
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'CoberturaAlicorpAASS',
        type: 'POST',
        dataType: 'html',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo en el panel de obertura.');
        }
    });
}
function fnOSAPanel() {
    var causal = $('#cbo_causal_right').val();
    if(causal=="" || causal== undefined || causal== null)
        causal = "0";

    //debugger;
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'OSA_AASS',
        type: 'POST',
        dataType: 'html',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: $('#cbo_marca_right').val()
            , _vcluster: $('#cbo_cluster_right').val()
            , _vcausal: causal
            , _vresultado: $('#cbo_tiporesultado_right').val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            console.log(xhr);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnTrackingPrecio() {
    var categoria = "0"; var marca = "0"; var cluster = "0"; var causal = "0"; var tresultado = "0";

    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'TrackingPrecioAli_AASS',
        type: 'POST',
        dataType: 'html',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: $('#cbo_marca_right').val()
            , _vcluster: $('#cbo_cluster_right').val()
            , _vcausal: causal
            , _vresultado: tresultado
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo en el Traking de precio');
        }
    });
}
function fnShareOfDisplay() {
    var categoria = "0"; var marca = "0"; var cluster = "0"; var causal = "0"; var tresultado = "0";
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'SOD_Ali_AASS',
        type: 'POST',
        dataType: 'html',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: categoria
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: marca
            , _vcluster: cluster
            , _vcausal: causal
            , _vresultado: tresultado
        },
        success: function (response) {
            if (response != null) {
                debugger;
                $('#dv_rep_panel_body').replaceWith(response);
                //$('.leyenda').show();
                //$('.leyenda-marcador').hide();
            }
        },
        error: function (xhr) {
            debugger;
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });

}
function fnStockOut() {
    var categoria = "0"; var marca = "0"; var cluster = "0"; var causal = "0"; var tresultado = "0";
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'Quiebre_Ali_AASS',
        type: 'POST',
        dataType: 'html',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: marca
            , _vcluster: $('#cbo_cluster_right').val()
            , _vcausal: causal
            , _vresultado: tresultado
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin  : Reportes Panel */

/* Inicio   : Consulta PDV's */
function fnPdvCobertura(dato) {
    //$('#txtSKU').html("");
    $("#dv_content_leyenda").hide();
    //$("#div_leyenda").html('<img src="/Img/iconmarkers/green.png" />Cluster A<br /><img src="/Img/iconmarkers/yellow.png" />Cluster B <br /><img src="/Img/iconmarkers/red.png" />Cluster C');
    
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsCoberturaAlicorpAASS',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vopcobertura: dato
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvTrackingPrecio(dato, descProd) {
    var categoria = "0"; var marca = "0"; var cluster = "0"; var causal = "0"; var tresultado = "0";
    //$('#txtSKU').html("<strong>PRODUCTO : " + descProd + "</strong>");
    $("#dv_content_leyenda").hide();
    //$("#div_leyenda").html('');
    //$("#div_leyenda").html('<img src="/Img/iconmarkers/green.png" />Cumple Prec. Sug.<br /><img src="/Img/iconmarkers/red.png" />No cumple Prec. Sug.');
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsTrackingPrecio_AASS',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: $('#cbo_marca_right').val()
            , _vcluster: $('#cbo_cluster_right').val()
            , _vcausal: causal
            , _vresultado: tresultado
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo. Precio');
        }
    });
}
function fnPdvOSA(descProd) {
    var causal = $('#cbo_causal_right').val();
    if (causal == "" || causal == undefined || causal == null)
        causal = "0";
    //productoSelec = descProd;
    //$('#txtSKU').html("<strong>PRODUCTO : " + descProd + "</strong>");
    $("#dv_content_leyenda").hide();
    //$("#div_leyenda").html('<img src="/Img/iconmarkers/green.png" />Con Presencia<br /><img src="/Img/iconmarkers/red.png" />Sin Presencia');
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'ListarPDVs_OSA_AASS',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: $('#cbo_marca_right').val()
            , _vcluster: $('#cbo_cluster_right').val()
            , _vcausal: causal
            , _vresultado: $('#cbo_tiporesultado_right').val()
            , _sku: descProd
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });


}

function fnPdvPresenciaVisibilidad(dato, descProd) {
    $('#txtSKU').html("<strong>PRODUCTO : " + descProd + "</strong>");
    $("#dv_content_leyenda").show();
    $("#div_leyenda").html('<img src="/Img/iconmarkers/green.png" />Con Presencia<br /><img src="/Img/iconmarkers/red.png" />Sin Presencia');
    var marca = "";
    if (_g_veq == "4111304112013")
        marca = $('#cbo_empresa_right').val()
    else
        marca = $('#cbo_marca_right').val()

    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsPresenciaVisibilidadColgate',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vmarca: marca
            , _velemento: dato
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvStockOut(dato) {
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsStockOutBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnPdvShareOfShelf(dato) {
    $('#txtSKU').html("");
    $("#dv_content_leyenda").hide();
    $("#div_leyenda").html("");
    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsStockOutBackus',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pdv_unicoSearch(_vpdv) {
    $('#txtSKU').html("");
    var textSearch = $('#advanced-demo').val();
    if (textSearch == "")
        $('#txt_PDV').val("");

    fnLoading(2);
    $.ajax({
        beforeSend: function (xhr) { fnLoading(1); map.removeMarkers(); },
        url: 'PDVsCoberturaColgate',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: "1"
            , _vubigeo: "589"
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vopcobertura: 2
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            if (response == null) {
                fnLoading(2);
                alert("Este PDV no registra visitas en el periodo seleccionado.");
            } else {
                fnPintarPDV(response);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin  : Consulta PDV's*/


/* Inicio   : Exportables */
function fnExportarCobertura(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarCoberturaAASS',
        type: 'POST',
        data: {
            _vequipo: _g_veq
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vopcobertura: dato
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo. Exportar cobertura');
        }
    });
}
function fnExportarTrackingPrecio() {
    console.log("Exportar Precio");
    var categoria = "0"; var marca = "0"; var cluster = "0"; var causal = "0"; var tresultado = "0";
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarTrackingPrecio_AASS',
        type: 'POST',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: $('#cbo_marca_right').val()
            , _vcluster: $('#cbo_cluster_right').val()
            , _vcausal: causal
            , _vresultado: tresultado
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo. Exportar Tacking de precio');
        }
    });
}
function fnExportarOSA(descProd) {
    var causal = $('#cbo_causal_right').val();
    if (causal == "" || causal == undefined || causal == null)
        causal = "0";
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'Exportar_OSA_AASS',
        type: 'POST',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vpdv: $('#txt_PDV').val()
            , _vmarca: $('#cbo_marca_right').val()
            , _vcluster: $('#cbo_cluster_right').val()
            , _vcausal: causal
            , _vresultado: $('#cbo_tiporesultado_right').val()
            , _sku: descProd
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

function fnExportarPresenciaProductoTodo() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: '../ColgateReporting/ExportaExcelConsolidadoMaps',
        type: 'POST',
        data: {
            planning: $('#cbo_canal').val(),
            periodo: $('#cbo_periodo').val(),
            cadena: $('#cbo_cadena').val(),
            pdv: "" //$('#txt_PDV').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarPresenciaVisibilidad(dato) {
    var categoria = "0"; var marca = "0";
    if (_g_veq == "4111304112013") {
        categoria = $('#cbo_subcategoria_right').val()
        marca = $('#cbo_empresa_right').val()
    }
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarPresenciaVisibilidadColgate',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: categoria
            , _vmarca: marca
            , _velemento: dato
            , _vpdv: $('#txt_PDV').val()
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarStockOut(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnExportarShareOfShelf(dato) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { fnLoading(1); },
        url: 'ExportarStockOutBackus',
        type: 'POST',
        data: {
            _vcanal: $('#cbo_canal').val()
            , _vgrupo: _vmpas_g_ubigeo.grupo
            , _vubigeo: _vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: $('#cbo_categoria_right').val()
            , _vsku: dato
        },
        success: function (response) {
            fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload('/Temp/' + response.Archivo);
            }
        },
        error: function (xhr) {
            fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

/* Fin  : Exportables*/

/* Inicio : Filtros Panel */

function fnCategoria(reporte) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaCategoria',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vequipo: _g_veq
            , _vreporte: reporte
        },
        success: function (response) {
            $('#cbo_categoria_right').parent().parent().show();
            $('#cbo_categoria_right').empty();
            $('#cbo_categoria_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                //$('#cbo_categoria_right').append('<option selected value="' + value.a + '" >' + value.b + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnMarca() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaMarca',
        type: 'POST',
        dataType: 'Json',
        data: {
            _veq: _g_veq
            , _vcategoria: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#cbo_marca_right').parent().parent().show();
            $('#cbo_marca_right').empty();
            $('#cbo_marca_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_marca_right').append('<option value="' + value.a + '" >' + value.d + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnEmpresa(reporte) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaEmpresas',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq,
            __b: reporte
            , __c: $('#cbo_categoria_right').val()
        },
        success: function (response) {
            $('#cbo_empresa_right').parent().parent().show();
            $('#cbo_empresa_right').empty();
            $('#cbo_empresa_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                //debugger;
                $('#cbo_empresa_right').append('<option value="' + value.Text + '" >' + value.Value + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnFiltroSod(op, parametro) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaFiltroSod',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vtipo: op
            , _vparametro: parametro
        },
        success: function (response) {
            if (op == "1") {
                $('#cbo_nivel_right').parent().parent().show();
                $('#cbo_nivel_right').empty();
                $.each(response, function (key, value) {
                    $('#cbo_nivel_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                });
            }
            if (op == "2") {
                $('#cbo_tipo_elemento_right').parent().parent().show();
                $('#cbo_tipo_elemento_right').empty();
                $.each(response, function (key, value) {
                    $('#cbo_tipo_elemento_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                });
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnFiltrosOperativo(op, parametro) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaFiltroSod',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vtipo: op
            , _vparametro: parametro
        },
        success: function (response) {
            if (op == 4) {
                _removeMultiSelect('cbo_fechas_ope');
                $.each(response, function (i, v) {
                    $('#cbo_fechas_ope').append('<option value="' + v.a + '" selected="selected">' + v.b + '</option>');
                });
                $('#cbo_fechas_ope').multiselect('rebuild');
            }
            if (op == 5) {
                $('#cbo_tipo_quiebre').empty();
                $.each(response, function (i, v) {
                    $('#cbo_tipo_quiebre').append('<option value="' + v.a + '" >' + v.b + '</option>');
                });
                $('#cbo_tipo_quiebre2').empty();
                $.each(response, function (i, v) {
                    $('#cbo_tipo_quiebre2').append('<option value="' + v.a + '" >' + v.b + '</option>');
                });
                $('#cbo_tipo_quiebre3').empty();
                $.each(response, function (i, v) {
                    $('#cbo_tipo_quiebre3').append('<option value="' + v.a + '" >' + v.b + '</option>');
                });
            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnCadena() {
    var canal = $("#cbo_canal").val();
    if (canal != "0") {
        $.ajax({
            async: false,
            beforeSend: function (xhr) { },
            url: 'AlicorpAASSFiltrosJson',
            type: 'POST',
            dataType: 'Json',
            data: {
                __a: 12
                , __b: $('#cbo_canal').val() + ",1562"
            },
            success: function (response) {
                $('#cbo_cadena').parent().parent().show();
                $('#cbo_cadena').empty();
                //$('#cbo_cadena').append('<option value="0" >Todos</option>');
                $.each(response, function (key, value) {
                    $('#cbo_cadena').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
                });
            },
            error: function (xhr) {
                alert('Algo salió mal, por favor intente de nuevo.');
            }
        });
    } else {
        $('#cbo_cadena').parent().parent().show();
        $('#cbo_cadena').empty();
        $('#cbo_cadena').append('<option value="0" >Todos</option>');
    }
}
function fnCanal() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'FusionColgFiltros',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: "1",
            __b: _g_veq
        },
        success: function (response) {
            $('#cbo_canal').parent().parent().show();
            $('#cbo_canal').empty();
            $.each(response, function (key, value) {
                $('#cbo_canal').append('<option value="' + value.a + '" >' + value.b + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnSubCategoria(idReport) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'FusionColgFiltrosJson',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: "89"
            , __b: _g_veq + ',' + idReport
            //, __b: _g_veq+','+$('#cbo_periodo').val()
        },
        success: function (response) {
            debugger;
            $('#cbo_subcategoria_right').parent().parent().show();
            $('#cbo_subcategoria_right').empty();
            $('#cbo_subcategoria_right').empty();
            $('#cbo_subcategoria_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_subcategoria_right').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
                //if (reporte == 163) {
                //    $('#cbo_subcategoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                //    $('#cbo_subcategoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                //}
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnSubCategoriaSOS() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'FusionColgFiltrosJson',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: "89"
            , __b: _g_veq + ',166'
        },
        success: function (response) {
            debugger;
            $('#cbo_subcategoria_right').parent().parent().show();
            $('#cbo_subcategoria_right').empty();
            $('#cbo_subcategoria_right').empty();
            //$('#cbo_subcategoria_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $('#cbo_subcategoria_right').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
                //if (reporte == 163) {
                //    $('#cbo_subcategoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                //    $('#cbo_subcategoria_right').append('<option value="' + value.a + '" >' + value.b + '</option>');
                //}
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnEmpresaAASS(reporte) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaEmpresas',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq,
            __b: reporte
            , __c: $('#cbo_subcategoria_right').val()
        },
        success: function (response) {
            $('#cbo_empresa_right').parent().parent().show();
            $('#cbo_empresa_right').empty();
            $('#cbo_empresa_right').append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                //debugger;
                $('#cbo_empresa_right').append('<option value="' + value.Text + '" >' + value.Value + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnTipoResultado() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'AlicorpAASSFiltrosJson',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: 13
            , __b: ''
        },
        success: function (response) {
            $('#cbo_tiporesultado_right').parent().parent().show();
            $('#cbo_tiporesultado_right').empty();
            $.each(response, function (key, value) {
                console.log(value);

                if (value.Value == 2)
                {
                    $('#cbo_tiporesultado_right').append('<option selected value="' + value.Value + '" >' + value.Text + '</option>');
                }
                else
                {
                    $('#cbo_tiporesultado_right').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
                } 
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnTipoCausa() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'AlicorpAASSFiltrosJson',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: 14
            , __b: ''
        },
        success: function (response) {
            $('#cbo_causal_right').parent().parent().show();
            $('#cbo_causal_right').empty();
            $.each(response, function (key, value) {
                $('#cbo_causal_right').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnCluster() {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'AlicorpAASSFiltrosJson',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: 15
            , __b: $('#cbo_periodo').val()
        },
        success: function (response) {
            $('#cbo_cluster_right').parent().parent().show();
            $('#cbo_cluster_right').empty();
            $.each(response, function (key, value) {
                $('#cbo_cluster_right').append('<option value="' + value.Value + '" >' + value.Text + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin : Filtros Panel */


/* Inicio   : Reportes por PDV */
function fnReportesPdvs() {
    fnDetallePDV();
    fnFotoPDV(1);
    fnTrackingPrecioPDV();
    fnOSAPDV();
    fnStockOutPDV();
    fnShareOfDisplayPDV();
    //fnShareOfShelfPDV();
    //fnRptFotograficoPDV();
}
function fnDetallePDV() {
    $.ajax({
        url: 'DetallePDV_Alicorp',
        type: 'POST',
        data: {
            vcodpdv: _g_vpdv,
            vperiodo: $('#cbo_periodo').val()
        },
        success: function (response) {
            fnLimpiarDetallePDV();
            if (response.Jdpdv != null) {
                $('.cls_panel_datos #pd_cod_pdv').text(response.Jdpdv.codPuntoVenta);
                $('.cls_panel_datos #pd_nom_pdv').text(response.Jdpdv.nombrePuntoVenta);
                $('.cls_panel_datos #pd_administrador').text(response.Jdpdv.nombreAdministrador);
                $('.cls_panel_datos #pd_direccion').text(response.Jdpdv.direccion);
                $('.cls_panel_datos #pd_zona').text(response.Jdpdv.sector);
                $('.cls_panel_datos #pd_distrito').text(response.Jdpdv.distrito);
                $('.cls_panel_datos #pd_email').text(response.Jdpdv.email);
                $('.cls_panel_datos #pd_cluster').text(response.Jdpdv.Cluster);
                $('.cls_panel_datos #pd_gestor').text(response.Jdpdv.nombreGestor);
                $('.cls_panel_datos #pd_ult_visita').text(response.Jdpdv.ultimaVisita);
                $('.cls_panel_datos #cant_gie').text(response.Jdpdv.Cant_Gie);
                $('.cls_panel_datos #clasificacion').text(response.Jdpdv.Desc_Clasificacion);
            
            } else {
                fnLimpiarDetallePDV();
            }
        },  
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnLimpiarDetallePDV() {
    $('.cls_panel_datos #pd_nom_pdv').text('');
    $('.cls_panel_datos #pd_cod_pdv').text('');
    $('.cls_panel_datos #pd_nom_pdv').text('');
    $('.cls_panel_datos #pd_administrador').text('');
    $('.cls_panel_datos #pd_direccion').text('');
    $('.cls_panel_datos #pd_zona').text('');
    $('.cls_panel_datos #pd_distrito').text('');
    $('.cls_panel_datos #pd_email').text('');
    $('.cls_panel_datos #pd_cluster').text('');
    $('.cls_panel_datos #pd_gestor').text('');
    $('.cls_panel_datos #pd_ult_visita').text('');
}
function fnFotoPDV(vcodtipo) {
    $.ajax({
        url: 'FotoPDVAlicorp',
        type: 'POST',
        data: {
            vperiodo: $('#cbo_periodo').val(),
            vcodtipo: vcodtipo,
            vcodpdv: _g_vpdv
        },
        success: function (response) {
            if (response.Jfotopdv != null) {
                var vnum = 0;
                if (vcodtipo == 1) {
                    $.each(response.Jfotopdv, function (k, v) {
                        vnum++;
                        $('#dp_foto' + vnum).attr("src", String(v.nombreFoto));
                    });
                } else {

                }
            } else {

            }
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnTrackingPrecioPDV() {
    //fnCategoriaPDV(58, "#cbo_categoria_precio_pdv");
    //_vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_preciosku").html($("#dv_loading_general").html())
        },
        url: 'TrackingPrecioPDV_AASS',
        type: 'POST',
        dataType: 'html',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: "1"//_vmpas_g_ubigeo.grupo
            , _vubigeo: "589"//_vmpas_g_ubigeo.codigo
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: '0'
            , _vpdv: _g_vpdv
            , _vmarca: '0'
        },
        success: function (response) {
            $('#dv_content_preciosku').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo. hey no sale');
        }
    });
}
function fnOSAPDV() {
    //fnCategoriaPDV(58, "#cbo_categoria_actcomp_pdv");
    var categoria = "0"; var marca = "0"; var cluster = "0"; var causal = "0"; var tresultado = "0";
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_osa").html('')
        },
        url: 'OSAPDV_AASS',
        type: 'POST',
        dataType: 'html',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: "1"
            , _vubigeo: "589"
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: '0'
            , _vpdv: _g_vpdv
            , _vmarca: '0'
            , _vcluster: cluster
            , _vcausal: causal
            , _vresultado: tresultado
        },
        success: function (response) {
            //console.log(response);
            $('#dv_content_osa').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnStockOutPDV() {
    var categoria = "0"; var marca = "0"; var cluster = "0"; var causal = "0"; var tresultado = "0";
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_Quiebre").html($("#dv_loading_general").html());
        },
        url: 'QuiebrePDV_Ali_AASS',
        type: 'POST',
        dataType: 'html',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: "1"
            , _vubigeo: "589"
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: '0'
            , _vpdv: _g_vpdv
            , _vmarca: marca
            , _vcluster: cluster
            , _vcausal: causal
            , _vresultado: tresultado
        },
        success: function (response) {
            $('#dv_content_Quiebre').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnShareOfDisplayPDV() {
    var categoria = "0"; var marca = "0"; var cluster = "0"; var causal = "0"; var tresultado = "0";
    $.ajax({
        beforeSend: function (xhr) {
            $("#dv_content_SOD").html($("#dv_loading_general").html());
        },
        url: 'SODPDV_Ali_AASS',
        type: 'POST',
        dataType: 'html',
        data: {
            _vequipo: _g_veq
            , _vcanal: $('#cbo_canal').val()
            , _vgrupo: "1"
            , _vubigeo: "589"
            , _vcadena: $('#cbo_cadena').val()
            , _vperiodo: $('#cbo_periodo').val()
            , _vcategoria: '0'
            , _vpdv: _g_vpdv
            , _vmarca: marca
            , _vcluster: cluster
            , _vcausal: causal
            , _vresultado: tresultado
        },
        success: function (response) {
            $('#dv_content_SOD').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}


function fnRptFotograficoPDV() {
    //fnCategoriaPDV(23, "#cbo_categoria_foto_pdv");
    //$.ajax({
    //    beforeSend: function (xhr) {
    //        $("#dv_content_Fotografico").html('')
    //    },
    //    url: 'TrackingFotograficoColgatePDV',
    //    type: 'POST',
    //    dataType: 'html',
    //    data: {
    //        vperiodo: $('#cbo_periodo').val(),
    //        vcodtipo: 2,
    //        vcodpdv: _g_vpdv,
    //        vcodcategoria: $('#cbo_categoria_foto_pdv').val()
    //    },
    //    success: function (response) {
    //        $('#dv_content_Fotografico').replaceWith(response);
    //    },
    //    error: function (xhr) {
    //        alert('Algo salió mal, por favor intente de nuevo.');
    //    }
    //});
}
function fnRptFotograficoPDVCat(codcat) {
    //$.ajax({
    //    beforeSend: function (xhr) {
    //        $("#dv_content_Fotografico").html('')
    //    },
    //    url: 'TrackingFotograficoColgatePDV',
    //    type: 'POST',
    //    dataType: 'html',
    //    data: {
    //        vperiodo: $('#cbo_periodo').val(),
    //        vcodtipo: 2,
    //        vcodpdv: _g_vpdv,
    //        vcodcategoria: codcat
    //    },
    //    success: function (response) {
    //        $('#dv_content_Fotografico').replaceWith(response);
    //    },
    //    error: function (xhr) {
    //        alert('Algo salió mal, por favor intente de nuevo.');
    //    }
    //});
}

/* Fin  : Reportes por PDV */

/* Filtros por PDV */
function fnCategoriaPDV(reporte,idElemento) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaCategoria',
        type: 'POST',
        dataType: 'Json',
        data: {
            _vequipo: _g_veq
            , _vreporte: reporte
        },
        success: function (response) {
            $(idElemento).parent().parent().show();
            $(idElemento).empty();
            $(idElemento).append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $(idElemento).append('<option value="' + value.a + '" >' + value.b + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnMarcaPDV(idDivMarca, idElementoCat) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) { },
        url: 'ListaMarca',
        type: 'POST',
        dataType: 'Json',
        data: {
            _veq: _g_veq
            , _vcategoria: $(idElementoCat).val()
        },
        success: function (response) {
            $(idDivMarca).parent().parent().show();
            $(idDivMarca).empty();
            $(idDivMarca).append('<option value="0" >Todos</option>');
            $.each(response, function (key, value) {
                $(idDivMarca).append('<option value="' + value.a + '" >' + value.d + '</option>');
            });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin Filtros por PDV */

/* Inicio : Utilitario */
_vmarcadores = [];
_v_g_report_popup = null;
function fnPintarPDV(v_data) {
    map.removeMarkers();
    _vmarcadores = [];
    
    $.each(v_data, function (key, value) {
        //debugger;
        var color = "";
        var descripcion = "";
        
        color = value.e;
        descripcion = value.b;
        

        map.addMarker({
            lat: parseFloat(value.c),
            lng: parseFloat(value.d),
            title: value.b,
            code: value.a,
            icon: '/Img/iconmarkers/' + value.e + '.png',
            click: function (e) {
                $(".cls_op_menu_right").removeClass("active");
                $("body").removeClass("infobar-active").delay(200).queue(function () {
                    $('#myModal').modal({
                        keyboard: false
                      , backdrop: 'static'
                    });
                    $('#dv_tabs_button li').removeClass('active');
                    $('#dv_home').parent().addClass('active');
                    $('#dv_reportes_pdv_content div').removeClass('active');
                    $('#home3').addClass('active');
                    $(this).dequeue();
                }).delay(200).queue(function () {
                    _g_vpdv = e.code;
                    //funct_reportes_pdv();
                    fnReportesPdvs();
                    //map.removeMarkers();
                    $(this).dequeue();
                });
                
            }
        });

        if (key == (v_data.length - 1)) {
            fnLoading(2);
        }
    });
}
function fnLoading(_vop) {
    if (_vop == 1) {
        $('#MD_loading').modal({
            keyboard: false
            , backdrop: 'static'
        });
    } else {
        $('#MD_loading').modal('hide')
    }
}
/* Fin  : Utilitario */
/* Loading Page */
//fnCanal();

function fnMostrarReportePDV(cod_pdv) {
    fnLoading(1);
    $(".cls_op_menu_right").removeClass("active");
    $("body").removeClass("infobar-active").delay(500).queue(function () {
        $('#myModal').modal({
            keyboard: false
          , backdrop: 'static'
        });
        $('#dv_tabs_button li').removeClass('active');
        $('#dv_home').parent().addClass('active');
        $('#dv_reportes_pdv_content div').removeClass('active');
        $('#home3').addClass('active');
        $(this).dequeue();
    }).delay(500).queue(function () {
        _g_vpdv = cod_pdv;
        //funct_reportes_pdv();
        fnReportesPdvs();
        
        //map.removeMarkers();
        $(this).dequeue();
        fnLoading(2);
    });
}
function funct_validar_selectPDV(_vop) {
    debugger;
    var _vbody_alert = "";
    var sw = 0;
    //if (_vmpas_g_ubigeo == null) {
    //    _vbody_alert += "<li>Seleccionar capa para realizar la consulta...</li>";
    //    sw = 1;
    //}
    if ($("#cbo_anio").val() == 0 || $("#cbo_anio").val() == null) {
        _vbody_alert += "<li>Seleccionar el filtro año...</li>";
        sw = 1;
    }
    if ($("#cbo_mes").val() == 0 || $("#cbo_mes").val() == null) {
        _vbody_alert += "<li>Seleccionar el filtro mes...</li>";
        sw = 1;
    }
    if ($("#cbo_periodo").val() == null) {
        _vbody_alert += "<li>Seleccionar el filtro periodo...</li>";
        sw = 1;
    }
    if (_vop == 1) {
        if ($("#txt_PDV").val() == null || $("#txt_PDV").val() == "0" || $("#txt_PDV").val() == "") {
            _vbody_alert += "<li>Ingresar codigo de PDV...</li>";
            sw = 1;
        }
    }

    if (sw == 1) {
        $("#MD_alert_body").html(_vbody_alert);
        $("#MD_alert").modal();
    }

    return sw;
}

