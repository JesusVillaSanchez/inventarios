﻿function funct_periodo() {

    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Filtros',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: "3"
            , __b: _g_veq + ",19," + $("#cbo_anio").val() + "," + $("#cbo_mes").val()
            , __c: "cbo_periodo"
        },
        success: function (response) {
            $('#cbo_periodo').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
_vmarcadores = [];
_v_g_report_popup = null;
function funct_reporte(velemento) {
    map.removeMarkers();

    //$('#div-cbo-unidad').hide();
    $("#dv_rep_panel_filtros > div").hide();

    //$("#sp_nom_reporte").html(velemento == 'Venta' ? 'Venta (A NIVEL NACIONAL)' : velemento);
    $("#sp_nom_reporte").html(velemento);

    if (velemento == "Cobertura")
    {
        //$(".cls_filtro_rep").hide();
        funct_cobertura_panel();
    }
    if (velemento == "Share Of Display")
    {
        
        //funct_categoria('cbo_categoria_right', 19, '', 21);        
        funct_segmento('cbo_segmento_right', 21);
        $("#dv_btn_filtros_rep").show();
        //funct_sod_panel();
        $('#dv_rep_panel_body').html('<h4>Seleccionar filtros</h4>');
    }
    if (velemento == "Actividad Competencia")
    {
        $("#dv_btn_filtros_rep").show();
        funct_tipo('cbo_tipo_right');
        funct_categoria('cbo_categoria_right', 19, '',66);
        funct_segmento('cbo_segmento_right',66);
        funct_actcompe_panel();
    }
    if (velemento == "Presencia de Producto (OSA)")
    {
        funct_segmento('cbo_segmento_right', 58);
        $("#dv_btn_filtros_rep").show(); 
        funct_categoria('cbo_categoria_right', 19, 'cbo_marca_right', 58);        
        //funct_presencia_panel();
    }
    if (velemento == "Tracking de Precio")
    {
        funct_segmento('cbo_segmento_right', 19);
        $("#dv_btn_filtros_rep").show();
        funct_categoria('cbo_categoria_right', 19, 'cbo_marca_right',19);        
        //funct_precio_panel();
    }

    if (velemento == "Presencia Visibilidad")
    {
        $("#dv_btn_filtros_rep").show();
        funct_categoria('cbo_categoria_right', 19, '', 78);
    }

    if (velemento == "Presencia Ventanas") {
        funct_segmento('cbo_segmento_right', 1001);
        //$(".cls_filtro_rep").show();
        fnVentanaPanel();
    }

    if (velemento == "Venta") {
        $('#div-cbo-unidad').show();
        $("#dv_btn_filtros_rep").show();

        funct_segmento('cbo_segmento_right', 17);
        funct_ventas_panel();
    }

    $('#dv_rep_panel_filtros').hide();
}

/* Inicio reportes panel */

function funct_cobertura_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'Cobertura',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: ""
            , __k: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
            $('.leyenda').hide();
            $('.leyenda-marcador').show();
            $('.leyenda-marcador-verde').text('Visitados');
            $('.leyenda-marcador-rojo').text('No visitados');
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_presencia_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'Presencia',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: ""
            , __i: $("#cbo_categoria_right").val()
            , __j: $("#cbo_marca_right").val()
            , __k: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __l: ""
            , __m: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
            $('.leyenda').hide();
            $('.leyenda-marcador').show();
            $('.leyenda-marcador-verde').text('Con presencia');
            $('.leyenda-marcador-rojo').text('Sin presencia');
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_precio_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'Precio',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()

            , __h: ""
            , __i: $("#cbo_categoria_right").val()
            , __j: $("#cbo_marca_right").val()
            , __k: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __l: ""
            , __m: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
            $('.leyenda').hide();
            $('.leyenda-marcador').show();
            $('.leyenda-marcador-verde').text('Cumple precio sugerido');
            $('.leyenda-marcador-rojo').text('No cumple precio sugerido');
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_sod_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        async: true,
        url: 'ShareOfDisplay',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: ""
            , __i: "0" //$("#cbo_categoria_right").val()
            , __j: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __k: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
            $('.leyenda').show();
            $('.leyenda-marcador').hide();
            $('.leyenda1').text('SOD - Share Of Display / ');
            $('.leyenda2').text('SOM - Share Of Market');
        },
        error: function (xhr) {
            $('#dv_rep_panel_body').html('<div id="dv_rep_panel_body" ></div>');
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_actcompe_panel()
{
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        url: 'ActividadCompetencia',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: ""
            , __i: ($("#cbo_categoria_right").val() == null || $("#cbo_categoria_right").val() == "" ? "0" : $("#cbo_categoria_right").val())
            , __j: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __k: ($("#cbo_tipo_right").val() == null || $("#cbo_tipo_right").val() == "" ? 0 : $("#cbo_tipo_right").val())
            , __l: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
            $('.leyenda').hide();
            $('.leyenda-marcador').hide();
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_visibilidad_panel() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        async: true,
        url: 'Pop',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: 0//$("#cbo_categoria_right").val()
            , __i: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 18 : $("#cbo_segmento_right").val())
            , __j: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
            $('.leyenda').hide();
            $('.leyenda-marcador').show();
            $('.leyenda-marcador-verde').text('Con presencia');
            $('.leyenda-marcador-rojo').text('Sin presencia');
        },
        error: function (xhr) {
            $('#dv_rep_panel_body').html('<div id="dv_rep_panel_body" ></div>');
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnVentanaPanel() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        async: true,
        url: 'Ventana',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: $("#cbo_tipogiro").val()
            , __i: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 18 : $("#cbo_segmento_right").val())
        },
        success: function (response) {
            $('#dv_rep_panel_body').replaceWith(response);
            $('.leyenda').hide();
            $('.leyenda-marcador').show();
            $('.leyenda-marcador-verde').text('Con exhibición');
            $('.leyenda-marcador-rojo').text('Sin exhibición');
        },
        error: function (xhr) {
            $('#dv_rep_panel_body').html('<div id="dv_rep_panel_body" ></div>');
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

//rcontreras 21-08-2015
function funct_ventas_panel() {
    $.ajax({
        beforeSend: function (xhr) {
            $('#dv_rep_panel_body').html($("#dv_loading_general").html());
        },
        async: true,
        url: 'Venta',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq,
            __b: _vmpas_g_ubigeo.grupo,
            __c: _vmpas_g_ubigeo.codigo,
            __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val()),
            __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val()),
            __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val()),
            __g: $("#cbo_giro").val(),
            __h: $("#cbo_tipogiro").val(),
            __i: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 18 : $("#cbo_segmento_right").val())
        },
        success: function (response) {
            $('#cbo-unidad').val(0);

            $('#dv_rep_panel_body').replaceWith(response);
            $('.leyenda-marcador').hide();
        },
        complete: function () {
            $('.hide-peso').show();
            $('.hide-moneda').hide();
        },
        error: function (xhr) {
            $('#dv_rep_panel_body').html('<div id="dv_rep_panel_body" ></div>');
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

/* Fin reportes panel */

function funct_reportes_pdv() {    
    funct_filtros_x_PDV();
    // Datos PDV
    funct_datos_pdv();
    //funct_sod_x_pdv();
    $('#dv_content_SOD').html("<center><h4>Seleccionar filtros</h4></center>");
    funct_actcompe_x_pdv();
    funct_foto_x_pdv();
    fnPopPdv();
    fnVentanaDetalle();
    //fnVentasDetalle();
}

/* Inicio reportes por PDV */
function funct_datos_pdv() {
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Pdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __c: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __d: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __e: _g_vpdv
        },
        success: function (response) {
            $("#dp_foto1").attr('src', _g_vurlimg + response._j[0]._c);
            $("#dp_foto2").attr('src', _g_vurlimg + response._j[1]._c);
            $("#pd_nom_pdv").html(response._a);
            $("#pd_direccion").html(response._b);
            $("#pd_distrito").html(response._c);
            $("#pd_ult_visita").html(response._d);
            $("#pd_nom_gie").html(response._e);
            // $("#pd_nom_comerciante").html();
            $("#pd_email").html(response._f);
            $("#pd_nom_segmento").html(response._g);
            $("#pd_nom_ciudad").html(response._h);
            $("#pd_nom_giro").html(response._i);

            $("#pd_cod_pdv").html(response._k);
            $("#pd_tgestion").html(response._l);

            $("#pd_tipo_giro").html(response._m);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_sod_x_pdv() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_SOD').html(""); },
        url: 'ShareOfDisplay_PDV',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
             , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: _g_vpdv
            , __i: $("#cbo_pdv_sod_cat").val()
            , __j: ""
            , __k: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_content_SOD').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_actcompe_x_pdv() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_ActividadCompe').html(""); },
        url: 'ActividadCompetenciaPDV',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: _g_vpdv
            , __i: ( $("#cbo_pdv_act_cat").val() == null || $("#cbo_pdv_act_cat").val() == "" ? "0" : $("#cbo_pdv_act_cat").val() )
            , __j: "0"
            , __k: ($("#cbo_pdv_act_tipo").val() == null || $("#cbo_pdv_act_tipo").val() == "" ? "0" : $("#cbo_pdv_act_tipo").val())
            //, __l: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_content_ActividadCompe').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pres_x_pdv() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_presenciasku').html(""); },
        url: 'PresenciaSku',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
             , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: _g_vpdv
            , __i: $("#cbo_pdv_pres_cat").val()
            , __j: $("#cbo_pdv_pres_mar").val()
            , __k: "0"
            , __l: ""
            , __m: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_content_presenciasku').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_prec_x_pdv() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_preciosku').html(""); },
        url: 'PrecioSku',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
             , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: _g_vpdv
            , __i: $("#cbo_pdv_prec_cat").val()
            , __j: $("#cbo_pdv_prec_mar").val()
            , __k: ""
            , __l: ""
            , __m: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#dv_content_preciosku').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_foto_x_pdv() {
    var sw = 0;

    //_g_vfotos = null;
    $.ajax({
        beforeSend: function (xhr) { },
        url: 'Fotografico',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __c: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __d: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __e: _g_vpdv
            , __f: 0
        },
        success: function (response) {
            //$("#dv_content_Fotografico").html("");
            //_g_vfotos = response;
            //var sw = 0;
            //$.each(response, function (key, value) {
            //    $("#dv_content_Fotografico").append("<div class='cls_item_fotografico'><img onclick='alert(" + _g_vurlimg + value._c + ")' src='" + _g_vurlimg + value._c + "' alt='...' class='img-thumbnail' /> <b>" + value._d + "</b> </div> ");
            //    sw += 1;
            //});
            //if (sw == 0) {
            //    $("#dv_content_Fotografico").html("<div class='md-col-12'><center><h5>Sin datos disponibles...</h5></div>");
            //}

            //    $.each(_g_vfotos, function (key, value) {
            //        if ($("#cbo_pdv_foto_cat").val() == value._a) {
            //            $('.galeriaFotografico').append("<a href=\"" + _g_vurlimg + value._c + "\" rel=\"prettyPhoto[galeriaFotografico]\" class=\"cls_item_fotografico\"><img src='" + _g_vurlimg + value._c + "' alt=\"" + value._d + "\" class='img-thumbnail' /> <b>" + value._d + "</b> </a> ");
            //            sw += 1;
            //        }
            //    });
            $('.galeriaFotografico').empty();

            $.each(response, function (key, value) {
                $('.galeriaFotografico').append('<a href="' + _g_vurlimg + value._c + '" rel="prettyPhoto[galeriaFotografico]" class="cls_item_fotografico"><img src="' + _g_vurlimg + value._c + '" alt="Categoria: ' + value._b + ' \\ Fecha: ' + value._d + '" class="img-thumbnail" /><b style=" font-size: 10px; ">' + value._b + '</b></a>');
                sw += 1;
            });

            //if (sw == 0) {
            //    $('.galeriaFotografico').html('<div class=\"cls_item_fotografico md-col-12\"><center><h5>Sin datos disponibles...</h5></div>');
            //}

            $('.galeriaFotografico:first a[rel^=\'prettyPhoto\']').prettyPhoto({ animation_speed: 'normal' });
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

function funt_zoomm_fotografico(img) {
    //$(document).on('click', '#btnFoto', function (e) {
    alert();
    alert(img);
    if (img != null && img != '' && img != "1") {
        //$("#dp_foto1").hide();
        $("#dp_foto1").attr('src', img);
        // $('#dp_foto1').append('<img class="cls_zoom_item" src="' + img + '"  style="height:80%;width:80%;cursor: url(../../static/img/zoom-out-32.png), auto;" />');
    }
    //if (img == "1") {
    //    $("#dp_foto1 .cls_zoom_item").remove();
    //    $("#dp_foto1 .cls_lista_foto").show();
    //}
}


function fnPopPdv() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_preciosku').html(""); },
        url: 'PopElemento',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
             , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: _g_vpdv
            , __i: 0//$('#cbo-visibilidad-categoria').val()
            , __j: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#div-content-visibilidad').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnVentanaDetalle() {
    $.ajax({
        beforeSend: function (xhr) { $('#dv_content_preciosku').html(""); },
        url: 'VentanaCategoria',
        type: 'POST',
        dataType: 'html',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
             , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: _g_vpdv
            , __i: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            $('#div-content-ventana').replaceWith(response);
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
/* Fin Reportes por PDV */

function funct_filtros_x_PDV() {
    // Presencia
    funct_categoria('cbo_pdv_pres_cat', 19, 'cbo_pdv_pres_mar');
    // Precio
    funct_categoria('cbo_pdv_prec_cat', 19, 'cbo_pdv_prec_mar');
    // Share of Display
    funct_categoria('cbo_pdv_sod_cat', 19, '');
    // Actividad Competencia
    funct_categoria('cbo_pdv_act_cat', 19, '');
    funct_tipo('cbo_pdv_act_tipo');
    //Fotografico
    //funct_categoria('cbo_pdv_foto_cat', 23, '');
    ////Elemento de visibilidad
    //funct_categoria('cbo-visibilidad-categoria', 19,'')

    //ventas
    funct_categoria('cbo_pdv_venta_cat', 23, '');
}

/*exporta excel*/
function func_excelcobertura(_vgrupo, _vsegmento) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) {
            //fnLoading(1);
            $('.lucky-loading').modal('show');
        },
        url: 'CoberturaPdvExcel',
        type: 'POST',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: "" //_vpdv
            , __i: _vgrupo
            , __j: _vsegmento
            , __k: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            //fnLoading(2);
            $('.lucky-loading').modal('hide');
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(link.Temp + response.Archivo);
            }
        },
        error: function (xhr) {
            //fnLoading(2);
            $('.lucky-loading').modal('hide');
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function func_excelpresencia(_vsku) {
    $.ajax({
        async: false,
        beforeSend: function (xhr) {
            //fnLoading(1);
            $('.lucky-loading').modal('show');
        },
        url: 'PresenciaPdvExcel',
        type: 'POST',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: ""
            , __i: $("#cbo_categoria_right").val()
            , __j: $("#cbo_marca_right").val()
            , __k: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __l: _vsku
            , __m: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            //fnLoading(2);
            if (response.Archivo == '0') {
                alert('Sin datos disponibles para descargar.');
            } else {
                $.fileDownload(link.Temp + response.Archivo);
            }
        },
        complete: function () {
            $('.lucky-loading').modal('hide');
        },
        error: function (xhr) {
            $('.lucky-loading').modal('hide');
            //fnLoading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

/* Inicio mostrar PDV en mapa */
function funct_pdv_cobertura(_vgrupo, _vsegmento) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); map.removeMarkers(); },
        url: 'CoberturaPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: "" //_vpdv
            , __i: _vgrupo
            , __j: _vsegmento
            , __k: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            if (response == null) {
                funct_loading(2);
            } else {
                funct_pintar_pdv(response);
            }
        },
        error: function (xhr) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pdv_presencia(_vsku) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); map.removeMarkers(); },
        url: 'PresenciaPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            ,__h : ""
            , __i: $("#cbo_categoria_right").val()
            , __j: $("#cbo_marca_right").val()
            , __k: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __l: _vsku
            , __m: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            if (response == null) {
                funct_loading(2);
            } else {
                funct_pintar_pdv(response);
            }
        },
        error: function (xhr) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pdv_precio(_vsku) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); map.removeMarkers(); },
        url: 'PrecioPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            ,__h : ""
            , __i: $("#cbo_categoria_right").val()
            , __j: $("#cbo_marca_right").val()
            , __k: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __l: _vsku
            , __m: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            if (response == null) {
                funct_loading(2);
            } else {
                funct_pintar_pdv(response);
            }
        },
        error: function (xhr) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pdv_unico(_vpdv) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); map.removeMarkers(); },
        url: 'CoberturaPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: _vpdv
            , __i: "0"
            , __j: "0"
            , __k: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            if (response == null || response.length <= 0) {
                funct_loading(2);
                alert("El codigo ingresado no contiene visita en el periodo seleccionado...");
            } else {
                funct_pintar_pdv(response);
            }
        },
        error: function (xhr) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funct_pdv_visibilidad(_elemento,_categoria) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); map.removeMarkers(); },
        url: 'PopPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: _categoria
            , __i: ($("#cbo_segmento_right").val() == null || $("#cbo_segmento_right").val() == "" ? 0 : $("#cbo_segmento_right").val())
            , __j: _elemento
            , __k: $("#cbo_tipogiro").val()
        },
        success: function (response) {
            if (response == null || response.length == 0) {
                funct_loading(2);
            } else {
                funct_pintar_pdv(response);
            }
        },
        error: function (xhr) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function fnVentanaPdv(__v) {
    funct_loading(2);
    $.ajax({
        beforeSend: function (xhr) { funct_loading(1); map.removeMarkers(); },
        url: 'VentanaPdv',
        type: 'POST',
        dataType: 'Json',
        data: {
            __a: _g_veq
            , __b: _vmpas_g_ubigeo.grupo
            , __c: _vmpas_g_ubigeo.codigo
            , __d: ($("#cbo_anio").val() == null || $("#cbo_anio").val() == "" ? "0" : $("#cbo_anio").val())
            , __e: ($("#cbo_mes").val() == null || $("#cbo_mes").val() == "" ? "0" : $("#cbo_mes").val())
            , __f: ($("#cbo_periodo").val() == null || $("#cbo_periodo").val() == "" ? "0" : $("#cbo_periodo").val())
            , __g: $("#cbo_giro").val()
            , __h: __v
            , __i: $("#cbo_tipogiro").val()
        },
        success: function (__s) {
            if (__s == null || __s.length == 0) {
                funct_loading(2);
            } else {
                funct_pintar_pdv(__s);
            }

            map.addControl({
                position: 'left_bottom', content: '<i class="fa fa-map-marker" style="color:#23AC4D;font-size:20px;" ></i> Con Exhibición <i class="fa fa-map-marker" style="color:#D9534F;font-size:20px;" ></i> Sin Exhibición',
                style: { margin: '5px', padding: '10px', border: 'solid 1px #717B87', background: '#fff' },
            });
        },
        error: function (__e) {
            funct_loading(2);
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

function funct_pintar_pdv(v_data) {
    map.removeMarkers();
    _vmarcadores = [];
    if (v_data == null) return;
    $.each(v_data, function (key, value) {
        map.addMarker({
            lat: parseFloat(value.c),
            lng: parseFloat(value.d),
            title: value.b,
            code: value.a,
            icon: link.Xplora + 'Img/iconmarkers/' + value.e + '.png',
            click: function (e) {
                $(".cls_op_menu_right").removeClass("active");
                $("body").removeClass("infobar-active").delay(200).queue(function () {
                    $('#myModal').modal({
                        keyboard: false
                      , backdrop: 'static'
                    });

                    
                    $('#dv_tabs_button li').removeClass('active');
                    $('#dv_home').parent().addClass('active');
                    $('#dv_reportes_pdv_content div').removeClass('active');
                    $('#home3').addClass('active');

                    $(this).dequeue();
                }).delay(200).queue(function () {
                    _g_vpdv = e.code;
                    funct_reportes_pdv();
                    //map.removeMarkers();
                    $(this).dequeue();
                });
            }
        });

        if (key == (v_data.length-1)) {
            funct_loading(2);
            //funct_centrar_mapa();
        }
    });
}
/* fin mostrar PDV en mapa */
function funct_loading(_vop) {
    if (_vop==1) {
        $('#MD_loading').modal({
            keyboard: false
            , backdrop: 'static'
        });
    }else{
        $('#MD_loading').modal('hide')
    }
}

function fnLoading(_vop) {
    if (_vop == 1) {
        //$('#MD_loading').modal({
        //    keyboard: false
        //    , backdrop: 'static'
        //});
        $('.lucky-loading').modal('show');
    } else {
        //$('#MD_loading').modal('hide');
        $('.lucky-loading').modal('hide');
    }
}