﻿var evaluacion = [];
var contador = 0;

$(function () {
    fnOnClickNavbar('navbar-vertical', false);
})

function fnOnClickBuscar() {
    var parametro = [],
        pdv = $('#select-pdv').val(),
        contenido = $('#table-arriendo > tbody');

    if (pdv == '0' || pdv.length == 0) {
        alert('Seleccione punto de venta.');
        return;
    }

    parametro.push(pdv);
    parametro.push(Persona.Id);

    $.ajax({
        beforeSend: function (b) {
            contenido.empty();
            evaluacion = [];

            $('#lucky-load').modal('show');
        },
        url: url.Arriendo,
        type: 'post',
        dataType: 'json',
        data: { __a: '7', __b: parametro.join(',') },
        success: function (s) {
            evaluacion = s;

            $.each(evaluacion, function (i, arriendo) {
                contenido.append('<tr data-indice="' + i + '" data-parametro="' + arriendo._a + ',' + arriendo._b + ',' + arriendo._c + ',' + arriendo._d + ',' + arriendo._e + ',' + arriendo._f + '"><td>' + arriendo._i + '</td><td>' + arriendo._f + '</td><td>' + arriendo._j + '</td><td class="text-center"><input  onclick="fnHideSelect(\'' + contador + '\');" id="si' + contador + '"  type="checkbox" ' + (arriendo._g == '1' ? 'checked="checked"' : '') + ' /></td><td>' + fnSelectOption(arriendo._h) + '</td></tr>');
            });
        },
        complete: function () {
            fnOnClickNavbar('navbar-vertical-filtro', false);

            $('#lucky-load').modal('hide');
            fnChangeHeight();
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickGraba() {
    var resultado = 0,
        total = $('#table-arriendo > tbody > tr').length;

    $('#table-arriendo > tbody > tr').each(function () {
        var arrayParametro = [];

        var indice = $(this).data('indice'),
            parametro = $(this).data('parametro'),
            select = $(this).children('td').children('select'),
            radio = $(this).children('td').children('input').is(':checked');

        arrayParametro.push(parametro);
        arrayParametro.push(radio == true ? '1' : '0');
        arrayParametro.push(select.val());

        $.ajax({
            beforeSend: function (b) {
                $('#lucky-load').modal('show');
            },
            url: url.Osa,
            type: 'post',
            dataType: 'json',
            data: { __a: '8', __b: arrayParametro.join(',') },
            success: function (s) {
                resultado += s;
            },
            complete: function () {
                if (total == resultado) {
                    alert('Grabo correctamente.');
                    $('#lucky-load').modal('hide');
                }
            },
            error: function (e) {
                console.error(e);
                $('#lucky-load').modal('hide');
            }
        });
    });
}
function fnSelectOption(_a) {
    var arrayOption = [];

    $.each(arrayMotivo, function (i, motivo) {
        arrayOption.push('<option  class="' + contador + '"  value="' + motivo._a + '" ' + (motivo._a == _a ? 'selected="selected"' : '') + '>' + motivo._b + '</option>');
    });
    contador++;
    return '<select id=' + contador + ' class="form-control">' + arrayOption.join('') + '</select>';
    
}

function fnHideSelect(_a) {

    var si = document.getElementById("si" + _a).checked;
    var id = parseInt(_a) + 1;
    if (si == true) {
        $('#' + id ).val('0');
        $('.' + _a).hide();
    }
    else if (si == false) {

        $('.' + _a).show();

    }
}