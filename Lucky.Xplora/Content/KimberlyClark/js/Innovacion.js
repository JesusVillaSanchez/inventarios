﻿var __offsetTopOrigen,
    __divClone,
    __offsetTopDestino;

$(function () {
    __offsetTopOrigen = $('#button-top-navegacion').offset().top - 60;
    __divClone = $('#button-top-navegacion > div').clone(true);
    __offsetTopDestino = $('#button-top-navegacion-hidden').append(__divClone);

    fnOnClickNavbar('navbar-vertical', false);
});

window.addEventListener('scroll', function () {
    var __scrollTop = $(this).scrollTop();

    if (__scrollTop >= __offsetTopOrigen && __offsetTopDestino.is(':hidden')) {
        __offsetTopDestino.show();
    }
    else if (__scrollTop < __offsetTopOrigen) {
        __offsetTopDestino.hide();
    }
}, true);

function fnOnClickCategoria(__a, __b) {
    var $filtro = [];

    $filtro.push(__a);

    $.ajax({
        beforeSend: function (__s) {
            $('.btn-categoria').removeClass('btn btn-primary');
            $('.btn-categoria').addClass('btn btn-default');
            $('.btn-categoria.btn-' + __b).addClass('btn btn-primary');

            $('.kimberly-hide').show();
            $('#lucky-load').modal('show');
        },
        url: '_Marketing',
        type: 'post',
        dataType: 'html',
        data: { __a: '10', __b: $filtro.join(',') },
        success: function (__s) {
            $('#marketing-contenedor').empty();
            $('#marketing-contenedor').append(__s);
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickFoto(__a, __b, __c, __d) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [],
        __array = [];

    if (__d.length > 0) {
        __array = __d.split("&");

        for (var i = 0; i < __array.length; i++) {
            __arrayFotos.push(
                    { src: __a + __array[i], w: __b, h: __c }
                );
        }
    } else {
        __arrayFotos.push(
                { src: __a, w: __b, h: __c }
            );
    }

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
};