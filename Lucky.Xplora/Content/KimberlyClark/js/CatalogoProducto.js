﻿var __offsetTopOrigen,
    __divClone,
    __offsetTopDestino;

$(function () {
    __offsetTopOrigen = $('#button-top-navegacion').offset().top - 60;
    __divClone = $('#button-top-navegacion div').clone(true);
    __offsetTopDestino = $('#button-top-navegacion-hidden').append(__divClone);

    fnOnClickNavbar('navbar-vertical', false);
});

window.addEventListener('scroll', function () {
    var __scrollTop = $(this).scrollTop();

    if (__scrollTop >= __offsetTopOrigen && __offsetTopDestino.is(':hidden')) {
        __offsetTopDestino.show();
    }
    else if (__scrollTop < __offsetTopOrigen) {
        __offsetTopDestino.hide();
    }
}, true);

function fnOnClickNavegador(__a) {
    var __OffsetTop,
        __Array = [{ panel: '#panel-1' }, { panel: '#panel-2' }, { panel: '#panel-3' }, { panel: '#panel-4' }];

    __OffsetTop = $(__Array[(__a == 1 || __a == 21 ? 0 : (__a == 2 ? 3 : (__a == 11 ? 1 : 2)))].panel).offset().top - 105;

    $('html, body').animate({ scrollTop: __OffsetTop + 'px' });
}
function fnOnClickFoto(__a) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];

    if (__a == 0) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Huggies/1.png', w: 1497, h: 1930 },
                { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Huggies/2.png', w: 1497, h: 1930 },
                { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Huggies/3.png', w: 1497, h: 1930 },
                { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Huggies/4.png', w: 1497, h: 1930 },
                { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Huggies/5.png', w: 1497, h: 1930 },
                { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Huggies/6.png', w: 1497, h: 1930 },
                { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Huggies/7.png', w: 1497, h: 1930 }
            );
    } else if (__a == 1) {
        __arrayFotos.push(
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Kotex/1.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Kotex/2.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Kotex/3.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Kotex/4.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Kotex/5.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Kotex/6.png', w: 1497, h: 1930 }
                );
    } else if (__a == 2) {
        __arrayFotos.push(
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Adulto/1.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Adulto/2.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Adulto/3.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Adulto/4.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Adulto/5.png', w: 1497, h: 1930 }
                );
    } else if (__a == 3) {
        __arrayFotos.push(
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Family/1.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Family/2.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Family/3.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Family/4.png', w: 1497, h: 1930 },
                    { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Tradicional_Family/5.png', w: 1497, h: 1930 }
                );
    }

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false,
        loop: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
};