﻿$(function () {
    fnOnClickNavbar('navbar-vertical', false);
});

function fnOnClickFoto(__a) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];

    if (__a == 0) {
        __arrayFotos.push(
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Precio_Condicion/1.png', w: 733, h: 197 }
            );
    }

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
};