﻿var visible = 0,
    arrayVisible = [];
var content = [];
var count = 0;
var cdistrib = 0;

$(function () {
    fnOnClickNavbar('navbar-vertical-filtro', false);
    fnOnClickNavbar('navbar-vertical', false);
    fnOnClickBuscar('cs');
});

function fnOnClickBuscar(__a) {
    var parametro = [],
        tbodyFila = [],
        theadFila1 = [],
        theadFila2 = [];
    var tabla = $('#table-objetivo'),
        tablaHead = $('#table-objetivo > thead'),
        tablaBody = $('#table-objetivo > tbody'),
        smallHeading = $('#small-heading');


    parametro.push($('#_anio').val());
    parametro.push($('#_mes').val());
    parametro.push('DTT');
    parametro.push(__a);
    //parametro.push(persona.nivel);
    parametro.push('Nivel_3');
    parametro.push(persona.valor);

    $.ajax({
        beforeSend: function (__s) {
            visible = 0;
            arrayVisible = [];

            tablaHead.empty();
            tablaBody.empty();
            smallHeading.empty();

            $('#a-cs').removeClass().addClass('btn btn-default btn-sm');
            $('#a-usd').removeClass().addClass('btn btn-default btn-sm');
            $('#a-gsu').removeClass().addClass('btn btn-default btn-sm');
            $('#a-niv').removeClass().addClass('btn btn-default btn-sm');
            $('#a-' + __a).removeClass().addClass('btn btn-primary btn-sm');

            $('#lucky-load').modal('show');
        },
        url: 'Objetivo',
        type: 'post',
        dataType: 'json',
        data: { __a:'8', __b: parametro.join(',') },
        success: function (__s) {
            content = __s;

            if (__s._b == null || __s._b.length == 0) {
                alert('Lo sentimos, no hay información para este mes.');
                return;
            }

            if (__s._a == null || ""){
                alert('Sin información.');
                return;
            }
            content = __s;

            if (__s._b.length > 0) smallHeading.html('(actualizado al ' + __s._b + ')');

            $.each(__s._a, function (i, agrupa3) {
                $.each(agrupa3._b, function (ii, agrupacion){ 
                    $.each(agrupacion._b, function (iii, grupo) {
                        $.each(grupo._b, function (iiii, gdetalle) {
                         

                            var variable = grupo._a;
                            var clase = "";

                            if (agrupa3._a == "column__gba") {
                                var clasevariable = 'row-detalle' + (count + 1);
                                var variable2 = 'row-detalle' + (count + 1);
                            }
                            else if (agrupacion._b.length == 1) {
                                 clasevariable = 'gba'
                            } else if (agrupacion._b.length > 1) {
                                clasevariable='gba'+i +' distribuidora'
                            }
                            
                            if (variable != "" || agrupa3._a == "column__gba") {
                                variable = 'row-detalle' + (count + 1);
                            }
                                else {
                                    clase = 'master' + grupo._a;
                                    variable = 'ejecutivo' + (cdistrib + 1);
                                }

                                tbodyFila.push('<td class="column-cabecera" onclick="fnOnClickHideRow(\'' + (clasevariable == 'gba' ? 'gba'+(i) : variable) + '\',\'' +variable2 + '\');">' + gdetalle._a + '</td>');

                                $.each(gdetalle._b, function (iv, iiv) {
                                    if (i == 0 && ii== 0 && iii== 0 && iiii==0) {
                                        arrayVisible.push(iv);

                                        theadFila1.push('<th class="column-clear column-hidden column-hidden-' + iv + '"></th>');
                                        theadFila1.push('<th colspan="3" class="text-center column-hidden column-hidden-' + iv + '">' + iiv._a + '</th>');
                                        theadFila2.push('<th class="column-clear column-hidden column-hidden-' + iv + '"></th>');
                                        theadFila2.push('<th class="column-detalle text-center column-hidden column-hidden-' + iv + '">F<br />' + (__a == 'USD' ? 'US$' : __a.toUpperCase()) + '</th>');
                                        theadFila2.push('<th class="column-detalle text-center column-hidden column-hidden-' + iv + '">' + (__a == 'niv' ? 'USD' : 'P') + '<br />' + (__a == 'USD' ? 'US$' : __a.toUpperCase()) + '</th>');
                                        theadFila2.push('<th class="column-detalle text-center column-hidden column-hidden-' + iv + '">A<br />' + (__a == 'USD' ? 'US$' : __a.toUpperCase()) + '</th>');
                                    }

                                    tbodyFila.push('<td class="column-clear column-hidden column-hidden-' + iv + '"></td>');
                                    tbodyFila.push('<td class="column-detalle text-center column-hidden column-hidden-' + iv + '">' + iiv._c + '</td>');
                                    tbodyFila.push('<td class="column-detalle text-center column-hidden column-hidden-' + iv + '">' + iiv._b + '</td>');
                                    tbodyFila.push('<td class="column-detalle text-center column-hidden column-hidden-' + iv + '">' + iiv._e + '&nbsp;' + iiv._d + '</td>');
                                });
                                var master = grupo._a;
                                if (iii == 0) cdistrib++;

                                tablaBody.append('<tr ' + (iiii == 0 ? (master == '' && clasevariable!='gba' ? 'id="dist"' :(clasevariable=='gba'?'id="gba"' :'' )) + 'class="row-grupo row-grupo' + (count + 1) + ' ' + (clasevariable == 'gba' ? clasevariable + 'm' + (i + 1) + ' gbaclass': clasevariable + (cdistrib)) + ' ' + (grupo._a != '' ? 'ejecutivo' + (cdistrib)+' ejec' : '') + '"' : 'class="row-detalle' + count + ' deta ' + clasevariable + i + '"') + '> ' + tbodyFila.join('') + '</tr>');
                                tbodyFila = [];
                                if (iiii == 0) count++;
                                if (persona.nivel == "Nivel_4") {
                                    $('.deta').hide();
                                }

                                if (persona.nivel == "Nivel_3") {
                                    $('.deta').hide();
                                    $('.gba0').hide();
                                    $('.gba1').hide();
                                    $('.gba2').hide();
                                    $('.gba3').hide();
                                    $('.ejec').hide();
                                    }
                                //$('.cab').hide();

                   
                        });
                    });
                });
            });

            tablaHead.append('<tr><th class="column-cabecera" style="text-align: center;">Perfil</th>' + theadFila1.join('') + '</tr>');
            tablaHead.append('<tr><th class="column-cabecera"></th>' + theadFila2.join('') + '</tr>');

            fnOnClickNavegador('i');
        },
        complete: function () {
            fnOnClickNavbar('navbar-vertical-filtro', false);
            $('#lucky-load').modal('hide');
         
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickNavegador(__a) {
    var lng = ((arrayVisible.length - 1) < 0 ? 0 : (arrayVisible.length - 1)),
        windowWidth = $(window).width();

    if (windowWidth > 767) return;

    if (__a == 'd') {
        visible = visible + 1;
    } else if (__a == 'i') {
        visible = visible - 1;
    }

    visible = (visible > lng ? lng : (visible < 0 ? 0 : visible));

    $('.column-hidden').hide();
    $('.column-hidden-' + visible).show();
}
function fnOnClickHideRow(_a, _b) {
    if ($('.' + _a).is(":visible")) {
        $('.' + _a).hide();
    } else {
        if (_a.substring(0, 3) == 'gba') {
            $('.' + _a).show();
            $('.ejec').hide();
            $('.deta').hide();
            return;
        }

        $('.' + _a).show();

        if (_a.substring(0, 11) != 'row-detalle') {
            $('.deta').hide();
        }
    }
}
function fnOnChangeAnio() {
    var parametro = [];

    parametro.push($('#_anio').val());

    $.ajax({
        beforeSend: function (__s) {
            fnRemoveOptionSelect('_mes');
            $('#lucky-load').modal('show');
        },
        url: 'Panel',
        type: 'post',
        dataType: 'json',
        data: { opcion: '1', parametro: parametro.join(',') },
        success: function (s) {
            if (s == null) return;

            fnInsertOptionSelect('_mes', s);
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}