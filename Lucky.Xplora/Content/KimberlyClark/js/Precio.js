﻿var __contenedor = [],
    tableOffset,
    $header,
    $fixedHeader;

$(function () {
    $('#div-parametro').hide();
    fnOnClickNavbar('navbar-vertical', false);
    fnOnResizeGrilla();
});

function fnOnClickCalculadora() {
    var $filtro = [],
        __tbdistcobertura = $('#tb-dist-cobertura'),
        __tbdistmayorista = $('#tb-dist-mayorista'),
        __tbdistfarma = $('#tb-dist-farma'),
        __tbminimarket = $('#tb-minimarket'),
        __tbmayorista = $('#tb-mayorista'),
        __tbbroker = $('#tb-broker'),
        __tbpanalera = $('#tb-panalera'),
        __tbmakro = $('#tb-makro'),
        __canal = $('#_canal').val(),
        __phasing = $('#_phasing').val(),
        __financiero = $('#_financiero').val(),
        __sellin = $('#_sellin').val(),
        __arriendo = $('#_arriendo').val(),
        __cobertura = $('#_cobertura').val();

    if (__canal == 0) {
        alert('Seleccione canal.');
        return;
    }

    $filtro.push($('#_anio').val());
    $filtro.push($('#_mes').val());
    $filtro.push($('#_categoria').val());
    $filtro.push(__canal);
    $filtro.push(__phasing.length == 0 ? 0 : __phasing);
    $filtro.push(__financiero.length == 0 ? 0 : __financiero);
    $filtro.push(__sellin.length == 0 ? 0 : __sellin);
    $filtro.push(__arriendo.length == 0 ? 0 : __arriendo);
    $filtro.push(__cobertura.length == 0 ? 0 : __cobertura);

    __tbdistcobertura.hide();
    __tbdistmayorista.hide();
    __tbdistfarma.hide();
    __tbminimarket.hide();
    __tbbroker.hide();
    __tbpanalera.hide();
    __tbmayorista.hide();
    __tbmakro.hide();

    $.ajax({
        beforeSend: function (__s) {
            $('#tb-dist-cobertura > tbody').empty();
            $('#tb-dist-mayorista > tbody').empty();
            $('#tb-dist-farma > tbody').empty();
            $('#tb-minimarket > tbody').empty();
            $('#tb-broker > tbody').empty();
            $('#tb-panalera > tbody').empty();
            $('#tb-mayorista > tbody').empty();
            $('#tb-makro > tbody').empty();
            $('#TableCabecera').empty();

            $('#lucky-load').modal('show');
        },
        url: 'PrecioCondicion',
        type: 'post',
        dataType: 'json',
        data: { __a: '6', __b: $filtro.join(',') },
        success: function (__s) {
            __contenedor = __s;

            if (__contenedor.length == 0) {
                alert('No existe información para estos filtros.');
                return;
            }

            if (__contenedor != null) {
                if (__canal == 6 || __canal == 22 || __canal == 23) {
                    $.each(__contenedor, function (i, v) {
                        $('#tb-dist-cobertura > tbody').append('<tr onclick="fnOnclickDetalle($(this));" data-index="' + i + '"><td class="hidden-xs hidden-sm">' + v._e + '</td><td class="text-center hidden-xs hidden-sm">' + v._f + '</td><td>' + v._g + '</td><td class="text-center hidden-xs hidden-sm">' + v._h + '</td><td class="text-center hidden-xs hidden-sm">' + v._i + '</td><td class="text-center hidden-xs hidden-sm">' + v._j + '</td><td class="text-center">' + v._k + '</td><td class="text-center hidden-xs hidden-sm">' + __phasing + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __financiero + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __sellin + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __cobertura + '%' + '</td><td class="text-center">' + v._l + '</td><td class="text-center">' + v._m + '</td><td class="text-center hidden-xs hidden-sm">' + v._p + '</td><td class="text-center">' + v._q + '</td><td class="hidden-xs hidden-sm"></td><td class="text-center hidden-xs hidden-sm">' + v._x + '</td></tr>');
                    });

                    __tbdistcobertura.show();
                } else if (__canal == 9) {
                    $.each(__contenedor, function (i, v) {
                        $('#tb-dist-mayorista > tbody').append('<tr onclick="fnOnclickDetalle($(this));" data-index="' + i + '"><td class="hidden-xs hidden-sm">' + v._e + '</td><td class="text-center hidden-xs hidden-sm">' + v._f + '</td><td>' + v._g + '</td><td class="text-center hidden-xs hidden-sm">' + v._h + '</td><td class="text-center hidden-xs hidden-sm">' + v._i + '</td><td class="text-center hidden-xs hidden-sm">' + v._j + '</td><td class="text-center">' + v._k + '</td><td class="text-center hidden-xs hidden-sm">' + __phasing + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __financiero + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __sellin + '%' + '</td><td class="text-center">' + v._l + '</td><td class="text-center">' + v._m + '</td><td class="text-center hidden-xs hidden-sm">' + v._p + '</td><td class="text-center">' + v._q + '</td><td class="hidden-xs hidden-sm"></td><td class="text-center hidden-xs hidden-sm">' + v._x + '</td></tr>');
                    });

                    __tbdistmayorista.show();
                } else if (__canal == 13) {
                    $.each(__contenedor, function (i, v) {
                        $('#tb-minimarket > tbody').append('<tr onclick="fnOnclickDetalle($(this));" data-index="' + i + '"><td class="hidden-xs hidden-sm">' + v._e + '</td><td class="text-center hidden-xs hidden-sm">' + v._f + '</td><td>' + v._g + '</td><td class="text-center hidden-xs hidden-sm">' + v._h + '</td><td class="text-center hidden-xs hidden-sm">' + v._i + '</td><td class="text-center hidden-xs hidden-sm">' + v._j + '</td><td class="text-center">' + v._k + '</td><td class="text-center hidden-xs hidden-sm">' + __financiero + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __arriendo + '%' + '</td><td class="text-center">' + v._l + '</td><td class="text-center">' + v._m + '</td><td class="text-center hidden-xs hidden-sm">' + v._p + '</td><td class="text-center">' + v._q + '</td><td class="hidden-xs hidden-sm"></td><td class="text-center hidden-xs hidden-sm">' + v._x + '</td></tr>');
                    });

                    __tbminimarket.show();
                } else if (__canal == 5) {
                    $.each(__contenedor, function (i, v) {
                        $('#tb-broker > tbody').append('<tr onclick="fnOnclickDetalle($(this));" data-index="' + i + '"><td class="hidden-xs hidden-sm">' + v._e + '</td><td class="text-center hidden-xs hidden-sm">' + v._f + '</td><td>' + v._g + '</td><td class="text-center hidden-xs hidden-sm">' + v._h + '</td><td class="text-center hidden-xs hidden-sm">' + v._i + '</td><td class="text-center hidden-xs hidden-sm">' + v._j + '</td><td class="text-center">' + v._k + '</td><td class="text-center hidden-xs hidden-sm">' + __phasing + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __financiero + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __sellin + '%' + '</td><td class="text-center">' + v._l + '</td><td class="text-center">' + v._n + '</td><td class="text-center">' + v._o + '</td><td class="text-center hidden-xs hidden-sm">' + v._t + '</td><td class="text-center hidden-xs hidden-sm">' + v._u + '</td><td class="text-center">' + v._v + '</td><td class="text-center">' + v._w + '</td><td class="hidden-xs hidden-sm"></td><td class="text-center hidden-xs hidden-sm">' + v._x + '</td></tr>');
                    });

                    __tbbroker.show();
                } else if (__canal == 16) {
                    $.each(__contenedor, function (i, v) {
                        $('#tb-panalera > tbody').append('<tr onclick="fnOnclickDetalle($(this));" data-index="' + i + '"><td class="hidden-xs hidden-sm">' + v._e + '</td><td class="text-center hidden-xs hidden-sm">' + v._f + '</td><td>' + v._g + '</td><td class="text-center hidden-xs hidden-sm">' + v._h + '</td><td class="text-center hidden-xs hidden-sm">' + v._i + '</td><td class="text-center hidden-xs hidden-sm">' + v._j + '</td><td class="text-center">' + v._k + '</td><td class="text-center hidden-xs hidden-sm">' + __financiero + '%' + '</td><td class="text-center">' + v._l + '</td><td class="text-center">' + v._m + '</td><td class="text-center hidden-xs hidden-sm">' + v._p + '</td><td class="text-center">' + v._q + '</td><td class="hidden-xs hidden-sm"></td><td class="text-center hidden-xs hidden-sm">' + v._x + '</td></tr>');
                    });

                    __tbpanalera.show();
                } else if (__canal == 15) {
                    $.each(__contenedor, function (i, v) {
                        $('#tb-mayorista > tbody').append('<tr onclick="fnOnclickDetalle($(this));" data-index="' + i + '"><td class="hidden-xs hidden-sm">' + v._e + '</td><td class="text-center hidden-xs hidden-sm">' + v._f + '</td><td>' + v._g + '</td><td class="text-center hidden-xs hidden-sm">' + v._h + '</td><td class="text-center hidden-xs hidden-sm">' + v._i + '</td><td class="text-center hidden-xs hidden-sm">' + v._j + '</td><td class="text-center">' + v._k + '</td><td class="text-center hidden-xs hidden-sm">' + __financiero + '%' + '</td><td class="text-center">' + v._l + '</td><td class="text-center">' + v._m + '</td><td class="text-center hidden-xs hidden-sm">' + v._r + '</td><td class="text-center">' + v._s + '</td><td class="hidden-xs hidden-sm"></td><td class="text-center hidden-xs hidden-sm">' + v._x + '</td></tr>');
                    });

                    __tbmayorista.show();
                } else if (__canal == 4) {
                    $.each(__contenedor, function (i, v) {
                        $('#tb-dist-farma > tbody').append('<tr onclick="fnOnclickDetalle($(this));" data-index="' + i + '"><td class="hidden-xs hidden-sm">' + v._e + '</td><td class="text-center hidden-xs hidden-sm">' + v._f + '</td><td>' + v._g + '</td><td class="text-center hidden-xs hidden-sm">' + v._h + '</td><td class="text-center hidden-xs hidden-sm">' + v._i + '</td><td class="text-center hidden-xs hidden-sm">' + v._j + '</td><td class="text-center">' + v._k + '</td><td class="text-center hidden-xs hidden-sm">' + v._y + '</td><td class="text-center hidden-xs hidden-sm">' + v._z + '</td><td class="text-center hidden-xs hidden-sm">' + __phasing + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __financiero + '%' + '</td><td class="text-center hidden-xs hidden-sm">' + __sellin + '%' + '</td><td class="text-center">' + v._l + '</td><td class="text-center">' + v._m + '</td><td class="text-center hidden-xs hidden-sm">' + v._p + '</td><td class="text-center">' + v._q + '</td><td class="hidden-xs hidden-sm"></td><td class="text-center hidden-xs hidden-sm">' + v._x + '</td></tr>');
                    });

                    __tbdistfarma.show();
                } else if (__canal == 24) {
                    $.each(__contenedor, function (i, v) {
                        $('#tb-makro > tbody').append('<tr onclick="fnOnclickDetalle($(this));" data-index="' + i + '"><td class="hidden-xs hidden-sm">' + v._e + '</td><td class="text-center hidden-xs hidden-sm">' + v._f + '</td><td>' + v._g + '</td><td class="text-center">' + v._ag + '</td><td class="text-center">' + v._h + '</td><td class="text-center">' + v._aa + '</td><td class="text-center hidden-xs hidden-sm">' + v._ab + '</td><td class="text-center hidden-xs hidden-sm">' + v._ac + '</td><td class="text-center hidden-xs hidden-sm">' + v._x + '</td><td class="text-center hidden-xs hidden-sm">' + v._ad + '</td><td class="text-center hidden-xs hidden-sm">' + v._ae + '</td><td class="text-center hidden-xs hidden-sm">' + v._af + '</td></tr>');
                    });

                    __tbmakro.show();
                }
            }
        },
        complete: function () {
            $('#div-parametro').hide();
            $('#div-tabla').show();
            $('#lucky-load').modal('hide');

            var TablaId = (__canal == 6 || __canal == 22 || __canal == 23 ? '#tb-dist-cobertura' : (__canal == 9 ? '#tb-dist-mayorista' : (__canal == 13 ? '#tb-minimarket' : (__canal == 5 ? '#tb-broker' : (__canal == 16 ? '#tb-panalera' : (__canal == 15 ? '#tb-mayorista' : (__canal == 4 ? '#tb-dist-farma' : (__canal == 24 ? '#tb-makro' : ''))))))));

            tableOffset = $(TablaId).offset().top;
            $header = $(TablaId + ' > thead').clone();
            $fixedHeader = $('#TableCabecera').append($header);

            $('#TableCabecera').width($(TablaId).width());
            fnRecalculaWidthTd();
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnclickDetalle(__this) {
    var $ancho = $('body').width(),
        $index = __this.data('index'),
        __detalle = [],
        __form = $('#modal-detalle > div > div > div > form'),
        __canal = $('#_canal').val();

    __form.empty

    if ($ancho < 1200) {
        __detalle.push(fnFormatoDetalle('Categoria', __contenedor[$index]._e));
        __detalle.push(fnFormatoDetalle('Material', __contenedor[$index]._f + ' - ' + __contenedor[$index]._g));
        __detalle.push(fnFormatoDetalle('Px Lista (sin IGV)', __contenedor[$index]._h));
        __detalle.push(fnFormatoDetalle('Px Lista (con IGV)', __contenedor[$index]._i));
        __detalle.push(fnFormatoDetalle('Dsct. Innov.', __contenedor[$index]._j));
        __detalle.push(fnFormatoDetalle('Px c/Dsct. Innovación', __contenedor[$index]._k));

        if (__canal == 6 || __canal == 22 || __canal == 23) {
            __detalle.push(fnFormatoDetalle('Phasing', $('#_phasing').val() + '%'));
            __detalle.push(fnFormatoDetalle('Dsct. Financiero', $('#_financiero').val() + '%'));
            __detalle.push(fnFormatoDetalle('Rebate Sell In', $('#_sellin').val() + '%'));
            __detalle.push(fnFormatoDetalle('Rebate Cobertura', $('#_cobertura').val() + '%'));
            __detalle.push(fnFormatoDetalle('Px Full', __contenedor[$index]._l));
            __detalle.push(fnFormatoDetalle('Reventa', __contenedor[$index]._m));
            __detalle.push(fnFormatoDetalle('Mg. Lista (Mark Up)', __contenedor[$index]._p));
            __detalle.push(fnFormatoDetalle('Mg. Full (Mark Up)', __contenedor[$index]._q));
        } else if (__canal == 9) {
            __detalle.push(fnFormatoDetalle('Phasing', $('#_phasing').val() + '%'));
            __detalle.push(fnFormatoDetalle('Dsct. Financiero', $('#_financiero').val() + '%'));
            __detalle.push(fnFormatoDetalle('Rebate Sell In', $('#_sellin').val() + '%'));
            __detalle.push(fnFormatoDetalle('Px Full', __contenedor[$index]._l));
            __detalle.push(fnFormatoDetalle('Reventa', __contenedor[$index]._m));
            __detalle.push(fnFormatoDetalle('Mg. Lista (Mark Up)', __contenedor[$index]._p));
            __detalle.push(fnFormatoDetalle('Mg. Full (Mark Up)', __contenedor[$index]._q));
        } else if (__canal == 13) {
            __detalle.push(fnFormatoDetalle('Dsct. Financiero', $('#_financiero').val() + '%'));
            __detalle.push(fnFormatoDetalle('Rebate Arri./Exhib.', $('#_arriendo').val() + '%'));
            __detalle.push(fnFormatoDetalle('Px Full', __contenedor[$index]._l));
            __detalle.push(fnFormatoDetalle('Reventa', __contenedor[$index]._m));
            __detalle.push(fnFormatoDetalle('Mg. Lista (Mark Up)', __contenedor[$index]._p));
            __detalle.push(fnFormatoDetalle('Mg. Full (Mark Up)', __contenedor[$index]._q));
        } else if (__canal == 5) {
            __detalle.push(fnFormatoDetalle('Phasing', $('#_phasing').val() + '%'));
            __detalle.push(fnFormatoDetalle('Dsct. Financiero', $('#_financiero').val() + '%'));
            __detalle.push(fnFormatoDetalle('Rebate Sell In', $('#_sellin').val() + '%'));
            __detalle.push(fnFormatoDetalle('Px Full', __contenedor[$index]._l));
            __detalle.push(fnFormatoDetalle('Reventa x Mayor', __contenedor[$index]._n));
            __detalle.push(fnFormatoDetalle('Reventa x Menor', __contenedor[$index]._o));
            __detalle.push(fnFormatoDetalle('Mg. Lista (Mark Down) x Mayor', __contenedor[$index]._t));
            __detalle.push(fnFormatoDetalle('Mg. Full (Mark Down) x Mayor', __contenedor[$index]._u));
            __detalle.push(fnFormatoDetalle('Mg. Lista (Mark Down) x Menor', __contenedor[$index]._v));
            __detalle.push(fnFormatoDetalle('Mg. Full (Mark Down) x Menor', __contenedor[$index]._w));
        } else if (__canal == 16) {
            __detalle.push(fnFormatoDetalle('Dsct. Financiero', $('#_financiero').val() + '%'));
            __detalle.push(fnFormatoDetalle('Px Full', __contenedor[$index]._l));
            __detalle.push(fnFormatoDetalle('Reventa', __contenedor[$index]._m));
            __detalle.push(fnFormatoDetalle('Mg. Lista (Mark Up)', __contenedor[$index]._p));
            __detalle.push(fnFormatoDetalle('Mg. Full (Mark Up)', __contenedor[$index]._q));
        } else if (__canal == 15) {
            __detalle.push(fnFormatoDetalle('Dsct. Financiero', $('#_financiero').val() + '%'));
            __detalle.push(fnFormatoDetalle('Px Full', __contenedor[$index]._l));
            __detalle.push(fnFormatoDetalle('Reventa', __contenedor[$index]._m));
            __detalle.push(fnFormatoDetalle('Mg. Lista (Mark Down)', __contenedor[$index]._r));
            __detalle.push(fnFormatoDetalle('Mg. Full (Mark Down)', __contenedor[$index]._s));
        } else if (__canal == 4) {
            __detalle.push(fnFormatoDetalle('Dsct. Minicadena', __contenedor[$index]._y));
            __detalle.push(fnFormatoDetalle('Px c/Dscto Innovacion + Minicadena', __contenedor[$index]._z));
            __detalle.push(fnFormatoDetalle('Phasing', $('#_phasing').val() + '%'));
            __detalle.push(fnFormatoDetalle('Dsct. Financiero', $('#_financiero').val() + '%'));
            __detalle.push(fnFormatoDetalle('Rebate Sell In', $('#_sellin').val() + '%'));
            __detalle.push(fnFormatoDetalle('Px Full', __contenedor[$index]._l));
            __detalle.push(fnFormatoDetalle('Reventa', __contenedor[$index]._m));
            __detalle.push(fnFormatoDetalle('Mg. Lista (Mark Up)', __contenedor[$index]._p));
            __detalle.push(fnFormatoDetalle('Mg. Full (Mark Up)', __contenedor[$index]._q));
        }

        __detalle.push(fnFormatoDetalle('PVP Sugerido', __contenedor[$index]._x));

        __form.html(__detalle.join(''));

        $('#modal-detalle').modal('show');
    }
}
function fnOnClickParametros() {
    var __canal = $('#_canal').val(),
        __rowPhasing = $('#row-phasing'),
        __rowFinanciero = $('#row-financiero'),
        __rowSellin = $('#row-sellin'),
        __rowArriendo = $('#row-arriendo'),
        __rowCobertura = $('#row-cobertura');

    if (__canal == 0) {
        alert('Seleccione canal.');
        return;
    }

    __rowPhasing.hide();
    __rowFinanciero.hide();
    __rowSellin.hide();
    __rowArriendo.hide();
    __rowCobertura.hide();

    if (__canal == 6 || __canal == 22 || __canal == 23) {
        __rowPhasing.show();
        __rowFinanciero.show();
        __rowSellin.show();
        __rowCobertura.show();
    } else if (__canal == 9) {
        __rowPhasing.show();
        __rowFinanciero.show();
        __rowSellin.show();
    } else if (__canal == 13) {
        __rowFinanciero.show();
        __rowArriendo.show();
    } else if (__canal == 5) {
        __rowPhasing.show();
        __rowFinanciero.show();
        __rowSellin.show();
    } else if (__canal == 16) {
        __rowFinanciero.show();
    } else if (__canal == 15) {
        __rowFinanciero.show();
    } else if (__canal == 4) {
        __rowPhasing.show();
        __rowFinanciero.show();
        __rowSellin.show();
    }

    $('#div-tabla').hide();
    $('#div-parametro').show();

    fnOnClickNavbar('navbar-vertical-filtro', null);
}
function fnFormatoDetalle(__a, __b) {
    return '<div class="form-group form-group-sm"><label>' + __a + '</label><p>' + __b + '</p></div>';
}
function fnCargaPorcentaje(__a, __b) {
    $('#' + __a).val(__b);
}
function fnOnChangeAnio() {
    var parametro = [];

    parametro.push($('#_anio').val());

    $.ajax({
        beforeSend: function (__s) {
            fnRemoveOptionSelect('_mes');
            $('#lucky-load').modal('show');
        },
        url: 'Panel',
        type: 'post',
        dataType: 'json',
        data: { opcion: '1', parametro: parametro.join(',') },
        success: function (s) {
            if (s == null) return;

            fnInsertOptionSelect('_mes', s);
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnRecalculaWidthTd() {
    var __canal = $('#_canal').val(),
        TableId = (__canal == 6 || __canal == 22 || __canal == 23 ? '#tb-dist-cobertura' : (__canal == 9 ? '#tb-dist-mayorista' : (__canal == 13 ? '#tb-minimarket' : (__canal == 5 ? '#tb-broker' : (__canal == 16 ? '#tb-panalera' : (__canal == 15 ? '#tb-mayorista' : (__canal == 4 ? '#tb-dist-farma' : (__canal == 24 ? '#tb-makro' : ''))))))));

    $(TableId + ' tbody tr:last').each(function (iTd, eTd) {
        var td = $(this).find('td');

        $('#TableCabecera thead tr th:nth-child(1)').width(td.eq(0).width());
        $('#TableCabecera thead tr th:nth-child(2)').width(td.eq(1).width());
        $('#TableCabecera thead tr th:nth-child(3)').width(td.eq(2).width());
        $('#TableCabecera thead tr th:nth-child(4)').width(td.eq(3).width());
        $('#TableCabecera thead tr th:nth-child(5)').width(td.eq(4).width());
        $('#TableCabecera thead tr th:nth-child(6)').width(td.eq(5).width());
        $('#TableCabecera thead tr th:nth-child(7)').width(td.eq(6).width());
        $('#TableCabecera thead tr th:nth-child(8)').width(td.eq(7).width());
        $('#TableCabecera thead tr th:nth-child(9)').width(td.eq(8).width());
        $('#TableCabecera thead tr th:nth-child(10)').width(td.eq(9).width());
        $('#TableCabecera thead tr th:nth-child(11)').width(td.eq(10).width());
    });

    //$('#yourtableid tr:last').attr('id');
    //$('#tb-broker thead').find('tr').each(function (i, el) {
    //    if (i == 0) {
    //        $(this).find('th').each(function (iTh, eTh) {
    //            //console.log(eTh);
    //            //console.log($(eTh).width());
    //            $('#tb-broker tbody td:nth-child(' + (iTh + 1) + ')').width(
    //                iTh == 11 ? 
    //                $(eTh).width()
    //                );
    //        });
    //        //$('#tb-broker tbody td:nth-child(0)').width(_0);
    //    }
    //});
}
function fnOnResizeGrilla() {
    var __canal = $('#_canal').val(),
        TableId = (__canal == 6 || __canal == 22 || __canal == 23 ? '#tb-dist-cobertura' : (__canal == 9 ? '#tb-dist-mayorista' : (__canal == 13 ? '#tb-minimarket' : (__canal == 5 ? '#tb-broker' : (__canal == 16 ? '#tb-panalera' : (__canal == 15 ? '#tb-mayorista' : (__canal == 4 ? '#tb-dist-farma' : (__canal == 24 ? '#tb-makro' : ''))))))));

    $('#div-tabla').height($(window).height() - $('#l-navbar-horizontal').height() - 100);
}

//$(window).bind('scroll', function () {
//    var offset = $(this).scrollTop();

//    fnRecalculaWidthTd();

//    if (offset >= tableOffset && $fixedHeader.is(':hidden')) {
//        $fixedHeader.show();
//    }
//    else if (offset < tableOffset) {
//        $fixedHeader.hide();
//    }
//});
$(window).resize(function () {
    fnOnResizeGrilla();
});