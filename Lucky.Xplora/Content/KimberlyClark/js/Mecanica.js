﻿$(function () {
    fnOnClickNavbar('navbar-vertical', false);
});

function fnOnClickFoto(__a) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];

    if (__a == 0) {
        __arrayFotos.push(
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Mecanica_Rebate/1.png', w: 720, h: 1040 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Mecanica_Rebate/2.png', w: 720, h: 1040 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Mecanica_Rebate/3.png', w: 720, h: 1040 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Mecanica_Rebate/4.png', w: 720, h: 1040 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Mecanica_Rebate/5.png', w: 720, h: 1040 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Mecanica_Rebate/6.png', w: 720, h: 1040 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Mecanica_Rebate/7.png', w: 720, h: 1040 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Mecanica_Rebate/8.png', w: 720, h: 1040 }
            );
    }

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
};