﻿var evaluacion = [];
var contador = 0;

$(function () {
    fnOnClickNavbar('navbar-vertical', false);
})

function fnOnClickBuscar() {
    var parametro = [],
        categoria = $('#select-categoria').val(),
        pdv = $('#select-pdv').val(),
        contenido = $('#table-osa > tbody');

    if (categoria == 0) {
        alert('Seleccione categoria.');
        return;
    }

    if (pdv == '0' || pdv.length == 0) {
        alert('Seleccione punto de venta.');
        return;
    }

    parametro.push(categoria);
    parametro.push(pdv);
    parametro.push(Persona.Id);

    $.ajax({
        beforeSend: function (b) {
            contenido.empty();
            evaluacion = [];

            $('#lucky-load').modal('show');
        },
        url: url.Osa,
        type: 'post',
        dataType: 'json',
        data: { __a: '5', __b: parametro.join(',') },
        success: function (s) {
            evaluacion = s;

            $.each(evaluacion, function (i, osa) {
                contenido.append('<tr data-indice="' + i + '" data-parametro="' + osa._a + ',' + osa._b + ',' + osa._c + ',' + osa._d + ',' + osa._e + ',' + osa._f + '"><td>' + osa._i + '</td><td class="text-center">' + osa._f + '</td><td class="text-center"><input  onclick="fnHideSelect(\'' + contador + '\');" id="si' + contador + '" type="checkbox" ' + (osa._g == '1' ? 'checked="checked"' : '') + ' /></td><td>' + fnSelectOption(osa._h) + '</td></tr>');
            });
        },
        complete: function () {
            fnOnClickNavbar('navbar-vertical-filtro', false);

            $('#lucky-load').modal('hide');
            fnChangeHeight();
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickGraba() {
    var resultado = 0,
        total = $('#table-osa > tbody > tr').length;

    $('#table-osa > tbody > tr').each(function () {
        var arrayParametro = [];

        var indice = $(this).data('indice'),
            parametro = $(this).data('parametro'),
            select = $(this).children('td').children('select'),
            radio = $(this).children('td').children('input').is(':checked');

        arrayParametro.push(parametro);
        arrayParametro.push(radio == true ? '1' : '0');
        arrayParametro.push(select.val());

        $.ajax({
            beforeSend: function (b) {
                $('#lucky-load').modal('show');
            },
            url: url.Osa,
            type: 'post',
            dataType: 'json',
            data: { __a: '6', __b: arrayParametro.join(',') },
            success: function (s) {
                resultado += s;
            },
            complete: function () {
                if (total == resultado) {
                    alert('Grabo correctamente.');
                    $('#lucky-load').modal('hide');
                }
            },
            error: function (e) {
                console.error(e);
                $('#lucky-load').modal('hide');
            }
        });
    });
}
function fnSelectOption(_a) {
    var arrayOption = [];

    $.each(arrayMotivo, function (i, motivo) {
        arrayOption.push('<option class="' + contador + '" value="' + motivo._a + '" ' + (motivo._a == _a ? 'selected="selected"' : '') + '>' + motivo._b + '</option>');
    });
    contador++;
    return '<select id=' + contador + ' class="form-control">' + arrayOption.join('') + '</select>';
}

function fnHideSelect(_a) {

    var si = document.getElementById("si" + _a).checked;
    var id = parseInt(_a) + 1;
    if (si == true) {
        $('#' + id).val('0');
        $('.' + _a).hide();
    }
    else if (si == false) {

        $('.' + _a).show();

    }
}

$(window).bind("scroll", function () {
    offset = $(this).scrollTop();

    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
        $('#table-evolutivo-fixed').css('width', $('#table-evolutivo').width());

        $('#table-evolutivo > tbody > tr:eq(1) td').each(function (e) {
            $('#table-evolutivo-fixed > thead > tr:eq(1) th:nth-child(' + (e + 1) + ')').css('width', $('#table-evolutivo > tbody > tr td:nth-child(' + (e + 1) + ')').width() + 10);
        });

        $fixedHeader.show();
    }
    else if (offset < tableOffset) {
        $fixedHeader.hide();
    }
});