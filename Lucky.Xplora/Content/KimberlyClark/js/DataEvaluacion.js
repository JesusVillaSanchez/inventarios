﻿var evaluacion = {};
var contador = 0;
$(function () {
    fnOnClickNavbar('navbar-vertical', false);
})

function fnOnClickBuscar() {
    var parametro = [],
        categoria = $('#select-categoria').val(),
        grupo = $('#select-grupo').val(),
        pdv = $('#select-pdv').val(),
        contenido = $('.page-content > .container-fluid');

    if (categoria == 0) {
        alert('Seleccione categoria.');
        return;
    }

    if (pdv == '0' || pdv.length == 0) {
        alert('Seleccione punto de venta.');
        return;
    }

    parametro.push(Persona.Id);
    parametro.push(categoria);
    parametro.push(pdv);
    parametro.push(grupo);

    $.ajax({
        beforeSend: function (b) {
            contenido.empty();
            evaluacion = {};

            $('#lucky-load').modal('show');
        },
        url: url.Evaluacion,
        type: 'post',
        dataType: 'json',
        data: { __a: '3', __b: parametro.join(',') },
        success: function (s) {
            if (s._e == null) return;
            evaluacion = s;

            $.each(evaluacion._e, function (i, grupo) {
                $.each(grupo._b, function (ii, pregunta) {
                    contenido.append(
                            '<div class="bs-callout bs-callout-info" data-indice="' + grupo._a.toLowerCase().replace(' ', '_') + '_' + ii + '" data-parametro="' + evaluacion._a + ',' + evaluacion._b + ',' + evaluacion._c + ',' + evaluacion._d + ',' + grupo._a + ',' + pregunta._a + '">' +
                            '<h4>' + pregunta._b + '</h4>' +
                            '<div class="checkbox"><label><input type="radio"  onclick="fnHideSelect(\''+contador +'\');" id="si'+contador +'" name="si_no_' + grupo._a.toLowerCase().replace(' ', '_') + '_' + ii + '" value="si" ' + (pregunta._c == '1' ? 'checked="checked"' : '') + '> Si</label></div>' +
                            '<div class="checkbox"><label><input type="radio" onclick="fnHideSelect(\'' + contador + '\');" id="no' + contador +'" name="si_no_' + grupo._a.toLowerCase().replace(' ', '_') + '_' + ii + '" value="no" ' + (pregunta._c == '0' ? 'checked="checked"' : '') + '> No</label></div>' +
                            fnSelectOption(pregunta._e) +
                            '</div>'
                        );
                });
            });
        },
        complete: function () {
            fnOnClickNavbar('navbar-vertical-filtro', false);
            $('#lucky-load').modal('hide');
            fnChangeHeight();
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickGraba() {
    var resultado = 0,
        total = $('.bs-callout').length;

    $('.bs-callout').each(function () {
        var radioVal = '',
            arrayParametro = [];

        var indice = $(this).data('indice'),
            parametro = $(this).data('parametro'),
            select = $(this).children('select'),
            radio = $('input[name=si_no_' + indice + ']:checked').val();

        if (radio == undefined) {
            radioVal = '';
        } else if(radio == 'si') {
            radioVal = '1';
        } else if (radio == 'no') {
            radioVal = '0';
        }

        arrayParametro.push(parametro);
        arrayParametro.push(select.val());
        arrayParametro.push(radioVal);

        $.ajax({
            beforeSend: function (b) {
                $('#lucky-load').modal('show');
            },
            url: url.Evaluacion,
            type: 'post',
            dataType: 'json',
            data: { __a: '4', __b: arrayParametro.join(',') },
            success: function (s) {
                resultado += s;
            },
            complete: function () {
                if (total == resultado) {
                    alert('Grabo correctamente.');
                    $('#lucky-load').modal('hide');
                }
            },
            error: function (e) {
                console.error(e);
                $('#lucky-load').modal('hide');
            }
        });
    });
}
function fnSelectOption(arrayMotivo) {
    var arrayOption = [],
        estado = false;

    $.each(arrayMotivo, function (i, motivo) {
        arrayOption.push('<option class="'+ contador +'" value="' + motivo._a + '" ' + (motivo._c == true ? 'selected="selected"' : '') + '>' + motivo._b + '</option>');

        if(motivo._c == true) estado = true;
    });
    contador++;
    return '<select  id=' + contador + ' class="form-control"><option value="0" ' + (estado == true ? '' : 'selected="selected"') + '>- Seleccione -</option>' + arrayOption.join('') + '</select>';
}

function fnHideSelect(_a) {
    var si = document.getElementById("si" + _a).checked;
    var id = parseInt(_a) + 1;
    if (si == true) {
        $('#' + id).val('0');
        $('.' + _a).hide();
    }
    else if (si == false) {
        
        $('.' + _a).show();

    }
}