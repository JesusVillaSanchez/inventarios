﻿$(function () {
    fnOnClickNavbar('navbar-vertical', false);
});

function fnOnClickFoto(__a) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];

    if (__a == 0) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_1_Manual/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_1_Manual/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_1_Manual/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_1_Manual/4.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_1_Manual/5.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_1_Manual/6.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_1_Manual/7.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_1_Manual/8.jpg', w: 720, h: 1040 }
            );
    } else if (__a == 1) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_2_Agenda_Evolucion/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_2_Agenda_Evolucion/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_2_Agenda_Evolucion/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_2_Agenda_Evolucion/4.jpg', w: 720, h: 1040 }
            );
    } else if (__a == 2) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_3_Objetivo_Mes/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_3_Objetivo_Mes/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_3_Objetivo_Mes/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_3_Objetivo_Mes/4.jpg', w: 720, h: 1040 }
            );
    } else if (__a == 3) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_4_Prioridad/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_4_Prioridad/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_4_Prioridad/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_4_Prioridad/4.jpg', w: 720, h: 1040 }
            );
    }
    else if (__a == 4) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_5_Catalogo_Productos/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_5_Catalogo_Productos/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_5_Catalogo_Productos/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_5_Catalogo_Productos/4.jpg', w: 720, h: 1040 }
            );
    }
    else if (__a == 5) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_6_Rebate_Sell_IN/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_6_Rebate_Sell_IN/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_6_Rebate_Sell_IN/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_6_Rebate_Sell_IN/4.jpg', w: 720, h: 1040 }
            );
    }
    else if (__a == 6) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_7_Actividades_Mes/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_7_Actividades_Mes/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_7_Actividades_Mes/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_7_Actividades_Mes/4.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_7_Actividades_Mes/5.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_7_Actividades_Mes/6.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_7_Actividades_Mes/7.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_7_Actividades_Mes/8.jpg', w: 720, h: 1040 }
            );
    }
    else if (__a == 7) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_8_Precios_Condiciones/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_8_Precios_Condiciones/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_8_Precios_Condiciones/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_8_Precios_Condiciones/4.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_8_Precios_Condiciones/5.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_8_Precios_Condiciones/6.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_8_Precios_Condiciones/7.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_8_Precios_Condiciones/8.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_8_Precios_Condiciones/9.jpg', w: 720, h: 1040 }
            );
    }
    else if (__a == 8) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_9_Foto_Exito/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_9_Foto_Exito/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_9_Foto_Exito/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_9_Foto_Exito/4.jpg', w: 720, h: 1040 }
            );
    }
    else if (__a == 9) {
        __arrayFotos.push(
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_10_Drivers_FFVV/1.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_10_Drivers_FFVV/2.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_10_Drivers_FFVV/3.jpg', w: 720, h: 1040 },
                { src: '/Foto/c1987/Media/Imagen/Manual/ManualKC_10_Drivers_FFVV/4.jpg', w: 720, h: 1040 }
            );
    }

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false,
        loop: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
};