﻿var __contenedor = [];

$(function () {
    fnOnClickNavbar('navbar-vertical', false);
});

function fnOnClickBuscar() {
    var __filtro = [],
        __arrayHead = [],
        __arrayBody = [],
        __table = $('#table-visita'),
        __head = $('#table-visita > thead'),
        __body = $('#table-visita > tbody');

    __contenedor = [];
    __filtro.push(com);
    __filtro.push($('#input-desde').val());
    __filtro.push($('#input-hasta').val());

    $.ajax({
        beforeSend: function (__s) {
            __head.empty();
            __body.empty();

            $('#lucky-load').modal('show');
        },
        url: 'Index',
        type: 'post',
        dataType: 'json',
        data: { __a: __filtro.join(',') },
        success: function (__s) {
            __contenedor = __s;

            $.each(__s, function (i, v) {
                if (i == 0) {
                    __arrayHead.push('<th class="text-center">USUARIOS</th>');
                }

                __arrayBody.push('<td>' + v._b + '</td>');

                $.each(v._e, function (ii, iv) {
                    if (i == 0) {
                        __arrayHead.push('<th class="text-center">' + iv._f + '</th>');
                    }

                    __arrayBody.push('<td class="text-center">' + iv._g + '</td>');
                });

                if (i == 0) {
                    __arrayHead.push('<th class="text-center">TOTAL</th>');
                }
                __arrayBody.push('<td class="text-center">' + v._d + '</td>');

                if (i == 0) {
                    __head.append('<tr>' + __arrayHead.join(',') + '</tr>');
                }
                __body.append('<tr>' + __arrayBody.join(',') + '</tr>');

                __arrayHead = [];
                __arrayBody = [];
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickDescarga() {
    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: 'Descarga',
        type: 'post',
        dataType: 'json',
        data: { __a: JSON.stringify(__contenedor) },
        success: function (__s) {
            $.fileDownload(url.Descarga + __s.Archivo);
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}