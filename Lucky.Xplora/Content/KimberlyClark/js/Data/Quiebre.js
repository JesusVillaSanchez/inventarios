﻿function fnOnClickDescarga() {
    var filtro = [];

    filtro.push(parametro.supervisor);
    filtro.push($('#input-fecha').val());
    filtro.push($('#select-gestor').val());
    filtro.push($('#select-categoria').val());

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: url.Quiebre,
        type: 'post',
        dataType: 'json',
        data: { __a: '2', __b: filtro.join(',') },
        success: function (__s) {
            console.log(__s);
            window.open(url.Descarga + __s._a, '_blank');
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnChangeCarga() {
    var resultado = [];
    var formData = new FormData(),
        inputFile = document.getElementById('archivo');

    formData.append('archivo', inputFile.files[0]);

    $.ajax({
        url: url.Upload,
        beforeSend: function () {
            $('#lucky-load').modal('show');
        },
        type: 'post',
        dataType: 'json',
        data: formData,
        success: function (s) {
            if (s._b == true) {
                resultado = s;
            } else {
                alert('Problemas al cargar el archivo.');
            }
        },
        complete: function () {
            if (resultado._b == true) {
                $.ajax({
                    beforeSend: function (__s) {
                        $('#lucky-load').modal('show');
                    },
                    url: url.Quiebre,
                    type: 'post',
                    dataType: 'json',
                    data: { __a: '3', __b: resultado._a },
                    success: function (__s) {
                        alert('Se cargaron ' + __s._a + ', verifique su informacion.');
                    },
                    complete: function () {
                        $('#lucky-load').modal('hide');
                    },
                    error: function (e) {
                        console.error(e);
                        $('#lucky-load').modal('hide');
                    }
                });
            }
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        },
        cache: false,
        contentType: false,
        processData: false
    });
}