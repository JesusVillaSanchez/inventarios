﻿var __offsetTopOrigen,
    __divClone,
    __offsetTopDestino;

$(function () {
    __offsetTopOrigen = $('#button-top-navegacion').offset().top - 60;
    __divClone = $('#button-top-navegacion > div').clone(true);
    __offsetTopDestino = $('#button-top-navegacion-hidden').append(__divClone);

    fnOnClickNavbar('navbar-vertical', false);
});

//window.onscroll = function () {

//}

window.addEventListener('scroll', function () {
    var __scrollTop = $(this).scrollTop();

    if (__scrollTop >= __offsetTopOrigen && __offsetTopDestino.is(':hidden')) {
        __offsetTopDestino.show();
    }
    else if (__scrollTop < __offsetTopOrigen) {
        __offsetTopDestino.hide();
    }
}, true);

function fnOnClickNavegador(__a) {
    var __OffsetTop;

    __OffsetTop = $(__a).offset().top - 105;

    $('html, body').animate({ scrollTop: __OffsetTop + 'px' });
}
function fnOnClickFoto(__a) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];

    if (__a == 0) {
        __arrayFotos.push(
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Evolucion_Peru/1.png', w: 1754, h: 2391 }
            );
    } else if (__a == 1) {
        __arrayFotos.push(
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Evolucion_Andina/1.png', w: 1754, h: 2391 }
            );
    }

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
};