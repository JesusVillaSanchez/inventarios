﻿var Contenedor = [];

$(function () {
    fnOnResizeGrilla();
    fnOnClickNavbar('navbar-vertical', null);
})

function fnOnChangeTipoCliente() {
    fnOnChangePdv();
}
function fnOnChangeOficina() {
    fnOnChangePdv();
}
function fnOnChangeCadena() {
    fnOnChangePdv();
}
function fnOnChangePdv() {
    var filtro = [];

    filtro.push(Persona.Campania);
    filtro.push(Persona.Id);
    filtro.push($('#select-oficina').val());
    filtro.push($('#select-cadena').val());

    $.ajax({
        beforeSend: function (__s) {
            $('#container-principal .page-content .container-fluid').empty();
            fnRemoveOptionSelect('select-pdv');

            $('#lucky-load').modal('show');
        },
        url: Url.Cuestionario,
        type: 'post',
        dataType: 'json',
        data: { o: '1', p: filtro.join(',') },
        success: function (s) {
            $('#select-pdv option').remove();
            $.each(s, function (i, v) {
                $('#select-pdv').append('<option value="' + v._a + '" ' + (v._c == 1 ? 'selected="selected"' : '') + '>' + v._b + '</option>');
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickBuscar() {
    var filtro = [],
        SelectTipoCliente = $('#select-tipocliente').val(),
        SelectOficina = $('#select-oficina').val(),
        SelectCadena = $('#select-cadena').val(),
        SelectPdv = $('#select-pdv').val();

    if (SelectPdv == '0') {
        alert('Seleccione punto de venta.');
        return;
    }

    filtro.push(Persona.Campania);
    filtro.push(Persona.Reporte);
    filtro.push(SelectTipoCliente);
    filtro.push(Persona.Id);
    filtro.push(SelectPdv);
    filtro.push(0);

    $.ajax({
        beforeSend: function (__s) {
            Contenedor = [];

            $('#container-principal .page-content .container-fluid').empty();
            $('#lucky-load').modal('show');
        },
        url: Url.Cuestionario,
        type: 'post',
        dataType: 'json',
        data: { o: '4', p: filtro.join(',') },
        success: function (s) {
            if (s == null) return;

            Contenedor = s;

            $.each(Contenedor._c, function (iElemento, vElemento) {
                var ArrayRespuesta = [],
                    ArrayPreguntaRespuesta = [],
                    ArrayOpcion = [];

                ArrayPreguntaRespuesta = vElemento._g.split('|');

                $.each(ArrayPreguntaRespuesta[0].split(','), function (iRespuesta, vRespuesta) {
                    $.each(Contenedor._a, function (iRespuestaDescripcion, vRespuestaDescripcion) {
                        if (vRespuesta == vRespuestaDescripcion._a && vRespuesta == 24) {
                            ArrayRespuesta.push('<div class="checkbox"><input id="Input' + iElemento + '" type ="number" class="form-control" placeholder="' + vRespuestaDescripcion._c + ' (O a 10)" value="' + (ArrayPreguntaRespuesta.length > 1 ? ArrayPreguntaRespuesta[2] : '') + '" /></div>');
                        } else if (vRespuesta == vRespuestaDescripcion._a) {
                            ArrayRespuesta.push(
                                '<div class="checkbox">\
                                    <label><input type="radio" onchange="fnActivar(' + iElemento + ',$(this));" name="indice-' + iElemento + '" value="' + vRespuestaDescripcion._a + '" ' + (ArrayPreguntaRespuesta.length > 1 && vRespuestaDescripcion._a == ArrayPreguntaRespuesta[1] ? 'checked="checked"' : '') + ' />' + vRespuestaDescripcion._c + '</label>\
                                  </div>'
                            );
                        }

                        if (vElemento._b == '58') {
                            if (iElemento == 19) {
                                console.log(vRespuesta);
                                console.log(vRespuestaDescripcion._a);
                            }
                            if (ArrayPreguntaRespuesta.length > 2 && ArrayPreguntaRespuesta[1] == 1 && vRespuesta == '1') {
                                if (vRespuestaDescripcion._d == 'Causales' && vRespuestaDescripcion._a == 23) {
                                    ArrayOpcion.push('<option value="' + vRespuestaDescripcion._a + '" selected="selected">' + vRespuestaDescripcion._c + '</option>');
                                }
                            } else if (ArrayPreguntaRespuesta.length > 2 && ArrayPreguntaRespuesta[1] == 2 && vRespuesta == '2') {
                                if (vRespuestaDescripcion._d == 'Causales' && vRespuestaDescripcion._a != 23) {
                                    ArrayOpcion.push('<option value="' + vRespuestaDescripcion._a + '" ' + (parseInt(ArrayPreguntaRespuesta[2]) == vRespuestaDescripcion._a ? 'selected="selected"' : '') + '>' + vRespuestaDescripcion._c + '</option>');
                                }
                            }
                        }
                    });
                });

                if (vElemento._b == '58' && vElemento._e != 'PDVCLUSTER-001') {
                    ArrayRespuesta.push('<select id="Select' + iElemento + '" class="form-control">' + ArrayOpcion.join('') + '</select>');
                }
                $('#container-principal .page-content .container-fluid').append('<div class="bs-callout bs-callout-info" data-indice="' + iElemento + '" data-parametro="" data-reporte="' + vElemento._b + '">' + '<h4>' + vElemento._f + '</h4>' + ArrayRespuesta.join('') + '</div>');
            });
        },
        complete: function () {
            fnOnClickNavbar('navbar-vertical-filtro', false);
            $('#lucky-load').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnActivar(indice, objeto) {
    var ArrayOpcion = [];

    $('#Select' + indice).empty();
    $.each(Contenedor._a, function (i, v) {
        if (v._d == 'Causales') {
            if (objeto.val() == 1) {
                if (v._a == 23) {
                 ArrayOpcion.push('<option value="' + v._a + '" selected="selected">' + v._c + '</option>');
                }
            } else if (objeto.val() == 2 && v._a != 23) {
                ArrayOpcion.push('<option value="' + v._a + '">' + v._c + '</option>');
            }
        }
    });

    $('#Select' + indice).append((objeto.val() == 2 ? '<option value="0" selected= "selected">- Seleccione -</option>' : '') + ArrayOpcion.join(''));
}
function fnOnClickBuscarMontoCompra() {
    var filtro = [],
        SelectTipoCliente = $('#select-tipocliente').val(),
        SelectOficina = $('#select-oficina').val(),
        SelectCadena = $('#select-cadena').val(),
        SelectPdv = $('#select-pdv').val();

    if (SelectPdv == '0') {
        alert('Seleccione punto de venta.');
        return;
    }

    filtro.push(Persona.Campania);
    filtro.push(Persona.Reporte);
    filtro.push(0);
    filtro.push(Persona.Id);
    filtro.push(SelectPdv);
    filtro.push(SelectOficina);

    $.ajax({
        beforeSend: function (__s) {
            Contenedor = [];
            $('#container-principal .page-content .container-fluid').empty();

            $('#lucky-load').modal('show');
        },
        url: Url.Cuestionario,
        type: 'post',
        dataType: 'json',
        data: { o: '4', p: filtro.join(',') },
        success: function (s) {
            if (s == null) return;

            Contenedor = s;

            $('#container-principal .page-content .container-fluid').append(
                '<table class="table table-bordered">\
                    <colgroup>\
                        <col class="text-center" />\
                        <col class="text-center" style="width: 90px;" />\
                        <col class="text-center" style="width: 90px;" />\
                    </colgroup>\
                    <thead>\
                        <tr>\
                            <th class="text-center">PADRINO/MADRINA</th>\
                            <th class="text-center">MONTO</th>\
                            <th class="text-center">FOTO</th>\
                        </tr>\
                    </thead>\
                    <tbody></tbody>\
                </table>'
            );

            $.each(Contenedor._c, function (iElemento, vElemento) {
                $('table tbody').append('<tr><td>' + vElemento._f + '</td><td><input class="form-control" type="number" data-indice="' + iElemento + '" value="' + vElemento._j + '" /></td><td><input type="hidden" id="foto-' + iElemento + '" value="' + vElemento._i + '" /><form id="Form-' + iElemento + '" enctype="multipart/form-data" method="post"><label class="btn btn-default btn-sm btn-block btn-file"><i class="fa fa-camera"></i><input type="file" name="archivo" style="display: none;" onchange="fnOnChangeFile(' + iElemento + ');"></label></form></td></tr>');//<button class="btn btn-default btn-sm btn-block"><i class="fa fa-camera"></i></button>
            });
        },
        complete: function () {
            fnOnClickNavbar('navbar-vertical-filtro', false);
            $('#lucky-load').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickGraba() {
    var Parametro = [],
        Estado = true;

    $.each($('.bs-callout.bs-callout-info'), function (i, v) {
        var Indice = $(v).data('indice'),
            Reporte = $(v).data('reporte'),
            CheckBoxValue = $('input[name="indice-' + Indice + '"]:checked').val(),
            SelectValue = $('#Select' + Indice).val(),
            InputValue = $('#Input' + Indice).val();

        if (Reporte == 58) {
            if (CheckBoxValue == undefined) {
                alert('Debe de marcar alguno de los elementos en lista.');
                Estado = false;
                return false;
            } else if (Indice != 0 && (SelectValue == undefined || SelectValue == 0)) {
                alert('Debe de seleccionar algun dato de las causales.');
                console.log(Indice);
                Estado = false;
                return false;
            }
        } else if (Reporte == 140) {
            console.log($('#Input' + Indice).val());
            if (Indice == 0 && CheckBoxValue == undefined) {
                alert('Debe de marcar alguno de los elementos en lista.');
                Estado = false;
                return false;
            } else if (Indice != 0 && InputValue.length == 0) {
                alert('Debe de ingresar cantidad de elementos mayores a 0.');
                Estado = false;
                return false;
            }
        }
    });

    if (Estado == true) {
        $.each($('.bs-callout.bs-callout-info'), function (i, v) {
            var Indice = $(v).data('indice'),
                CheckBoxValue = $('input[name="indice-' + Indice + '"]:checked').val(),
                SelectValue = $('#Select' + Indice).val(),
                InputValue = $('#Input' + Indice).val(),
                Objeto = Contenedor._c[Indice],
                filtro = [];

            filtro.push(Objeto._a);
            filtro.push(Objeto._b);
            filtro.push(Objeto._c);
            filtro.push(Persona.Id);
            filtro.push($('#select-pdv').val());
            filtro.push(Objeto._d);
            filtro.push(Objeto._e);
            if (Objeto._b == 58) {
                filtro.push(CheckBoxValue + (SelectValue == undefined ? '' : '|' + SelectValue));
            } else if (Objeto._b == 140) {
                filtro.push((Indice == 0 ? CheckBoxValue + '|0' : '0|' + InputValue));
            }
            filtro.push('');
            filtro.push('');

            Parametro.push(filtro.join(','));
        });

        $.ajax({
            beforeSend: function (__s) {
                $('#lucky-load').modal('show');
            },
            url: Url.Cuestionario,
            type: 'post',
            dataType: 'json',
            data: { o: '5', p: Parametro.join('&') },
            success: function (s) {
                if (s == null) return;

                alert('Se guardaron ' + s.Resultado + ' registro(s).')
            },
            complete: function () {
                $('#lucky-load').modal('hide');
            },
            error: function (error) {
                console.error(error);
                $('#lucky-load').modal('hide');
            }
        });
    }
}
function fnOnClickGrabaMontoCompra() {
    var Parametro = [];

    $.each($('input[type=number]'), function (i, v) {
        var Indice = $(v).data('indice'),
            Objeto = Contenedor._c[Indice],
            filtro = [];

        if ($(v).val().length > 0 || $('#foto-' + Indice).val().length > 0) {
            filtro.push(Objeto._a);
            filtro.push(Objeto._b);
            //filtro.push(Objeto._c);
            filtro.push($('#select-tipocliente').val());
            filtro.push(Persona.Id);
            filtro.push($('#select-pdv').val());
            filtro.push(Objeto._d);
            filtro.push(Objeto._e);
            filtro.push(0);
            filtro.push($(v).val());
            filtro.push($('#foto-' + Indice).val());

            Parametro.push(filtro.join(','));
        }
    });

    if (Parametro.length == 0) return;

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: Url.Cuestionario,
        type: 'post',
        dataType: 'json',
        data: { o: '5', p: Parametro.join('&') },
        success: function (s) {
            if (s == null) return;

            alert('Se guardaron ' + s.Resultado + ' registro(s).')
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnResizeGrilla() {
    $('#container-principal .page-content .container-fluid').height($(window).height() - ($('.navbar.navbar-default.navbar-fixed-top').height() + $('.page-heading').height() + 60));
}
function fnOnChangeFile(indice) {
    var form = $('#Form-' + indice)[0];
    var dataString = new FormData(form);

    dataString.append('TipoEnvio', 'PoseidonImagen');

    $.ajax({
        url: 'Upload',
        beforeSend: function () {
            $('#lucky-load').modal('show');
        },
        type: 'post',
        data: dataString,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (s) {
            if (s == null) return;

            if (s._b == true) {
                var ArrayFoto = ($('#foto-' + indice).val().length == 0 ? [] : $('#foto-' + indice).val().split('|'));
                ArrayFoto.push(s._a);

                $('#foto-' + indice).val(ArrayFoto.join('|'));
                alert('Imagen cargada correctamente.');
            } else {
                alert('Problemas al cargar la imagen.');
            }
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__e) {
            console.error(__e);
            $('#lucky-load').modal('hide');
        }
    });
}

$(window).resize(function () {
    fnOnResizeGrilla();
});