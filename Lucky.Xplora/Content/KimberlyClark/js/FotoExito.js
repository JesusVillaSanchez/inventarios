﻿$(function () {
    fnOnClickNavbar('navbar-vertical', false);
    fnOnClickNavbar('navbar-vertical-filtro', false);
});

function fnOnClickFoto(indice) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];

    if (indice == 1) {
        __arrayFotos.push(
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Foto_Exito/2017_Abril/CARTILLA_BROKER_Y_MAY_DIRECTO-1.png', w: 2167, h: 1027 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Foto_Exito/2017_Abril/CARTILLA_BROKER_Y_MAY_DIRECTO-2.png', w: 2167, h: 1027 }
            );
    } else if (indice == 2) {
        __arrayFotos.push(
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Foto_Exito/2017_Abril/CARTILLA_MAYORISTA_INDIRECTO-1.png', w: 2167, h: 1027 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Foto_Exito/2017_Abril/CARTILLA_MAYORISTA_INDIRECTO-2.png', w: 2167, h: 1027 }
            );
    } else if (indice == 3) {
        __arrayFotos.push(
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Foto_Exito/2017_Abril/CARTILLA_MINORISTA-1.png', w: 2167, h: 1027 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Foto_Exito/2017_Abril/CARTILLA_MINORISTA-2.png', w: 2167, h: 1027 }
            );
    } else if (indice == 4) {
        __arrayFotos.push(
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Foto_Exito/2017_Abril/CARTILLA_PAÑALERAS-1.png', w: 2167, h: 1027 },
            { src: '/Foto/c1987/Media/Imagen/Catalogo/CatalogoKC_Foto_Exito/2017_Abril/CARTILLA_PAÑALERAS-2.png', w: 2167, h: 1027 }
            );
    }

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
}