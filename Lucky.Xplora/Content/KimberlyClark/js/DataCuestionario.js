﻿$(function () {
    fnOnClickNavbar('navbar-vertical', false);
})

function fnOnChangeCampania() {
    var filtro = [],
        Contenedor = $('#container-principal .page-content .container-fluid');

    filtro.push($('#select-campania').val());

    $.ajax({
        beforeSend: function (__s) {
            Contenedor.empty();
            fnRemoveOptionSelect('select-cadena');
            $('#lucky-load').modal('show');
        },
        url: Url.Cuestionario,
        type: 'post',
        dataType: 'json',
        data: { __a: '13', __b: filtro.join(',') },
        success: function (response) {
            $('#select-cadena option').remove();
            $('#select-cadena').append('<option value="0" selected="selected">- Seleccione -</option>');
            $.each(response, function (i, v) {
                $('#select-cadena').append('<option value="' + v._a + '">' + v._b + '</option>');
            });
        },
        complete: function () {
            fnOnChangeCadena();
            $('#lucky-load').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnChangeCadena() {
    var filtro = [],
        Contenedor = $('#container-principal .page-content .container-fluid');

    filtro.push(Persona.Id);
    filtro.push($('#select-campania').val());
    filtro.push($('#select-cadena').val());

    $.ajax({
        beforeSend: function (__s) {
            Contenedor.empty();
            fnRemoveOptionSelect('select-pdv');
            $('#lucky-load').modal('show');
        },
        url: Url.Cuestionario,
        type: 'post',
        dataType: 'json',
        data: { __a: '9', __b: filtro.join(',') },
        success: function (response) {
            $('#select-pdv option').remove();
            $('#select-pdv').append('<option value="0" selected="selected">- Seleccione -</option>');
            $.each(response, function (i, v) {
                $('#select-pdv').append('<option value="' + v._a + '">' + v._b + '</option>');
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnChangePdv() {
    var Contenedor = $('#container-principal .page-content .container-fluid');

    Contenedor.empty();
}
function fnOnClickBuscar() {
    var filtro = [],
        SelectCampania = $('#select-campania').val(),
        SelectPdv = $('#select-pdv').val(),
        SelectCategoria = $('#select-categoria').val(),
        SelectCadena = $('#select-cadena').val(),
        Contenedor = $('#container-principal .page-content .container-fluid');

    if (SelectCampania == '0') {
        alert('Seleccione canal.');
        return;
    }

    if (SelectPdv == '0') {
        alert('Seleccione punto de venta.');
        return;
    }

    filtro.push(SelectCampania);
    filtro.push(SelectCategoria);
    filtro.push(Persona.Id);
    filtro.push(SelectPdv);

    $.ajax({
        beforeSend: function (__s) {
            Contenedor.empty();
            $('#lucky-load').modal('show');
        },
        url: Url.Cuestionario,
        type: 'post',
        dataType: 'json',
        data: { __a: '10', __b: filtro.join(',') },
        success: function (response) {
            $.each(response._b, function (iCategoria, vCategoria) {
                $.each(vCategoria._c, function (iGrupo, vGrupo) {
                    $.each(vGrupo._b, function (iPregunta, vPregunta) {
                        var ArrayRespuesta = [];
                        var ArrayPreguntaRespuesta = [];

                        ArrayPreguntaRespuesta = vPregunta._c.split('|')

                        $.each(ArrayPreguntaRespuesta[0].split(','), function (iRespuesta, vRespuesta) {
                            $.each(response._a, function (iRespuestaDescripcion, vRespuestaDescripcion) {
                                if (vRespuesta == vRespuestaDescripcion._a) {
                                    ArrayRespuesta.push('<div class="checkbox"><label><input type="radio" name="' + iCategoria + '_' + iGrupo + '_' + iPregunta + '" onclick="fnOnClickGraba(\'' + vCategoria._a + '\',\'' + vPregunta._a + '\',\'' + vRespuesta + '\');" id="" value="' + vRespuestaDescripcion._a + '" ' + (ArrayPreguntaRespuesta.length == 2 && vRespuestaDescripcion._a == ArrayPreguntaRespuesta[1] ? 'checked="checked"' : '') + ' />' + vRespuestaDescripcion._c + '</label></div>');
                                }
                            });
                        });

                        Contenedor.append(
                            '<div class="bs-callout bs-callout-info" data-indice=" data-parametro="">' + '<h4>' + vPregunta._b + '</h4>' + ArrayRespuesta.join('') + '</div>'
                        );
                    });
                });
            });
        },
        complete: function () {
            fnOnClickNavbar('navbar-vertical-filtro', false);
            $('#lucky-load').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickGraba(categoria, pregunta, respuesta) {
    var filtro = [];

    filtro.push($('#select-campania').val());
    filtro.push(Persona.Id);
    filtro.push($('#select-pdv').val());
    filtro.push(categoria);
    filtro.push(pregunta);
    filtro.push(respuesta);

    $.ajax({
        beforeSend: function (__s) {
            $('#lucky-load').modal('show');
        },
        url: Url.Cuestionario,
        type: 'post',
        dataType: 'json',
        data: { __a: '12', __b: filtro.join(',') },
        success: function (response) { },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (error) {
            console.error(error);
            $('#lucky-load').modal('hide');
        }
    });
}