﻿$(function () {
    fnOnClickNavbar('navbar-vertical', false);
    fnOnClickNavbar('navbar-vertical-filtro', false);

    fnOnClickBuscar(0);
})

function fnOnClickFoto(__a) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];

    __arrayFotos.push(
            { src: __a, w: 720, h: 960 }
        );

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false,
        loop: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
};
function fnOnClickBuscar(__a) {
    var $filtro = [],
        $div = $('#div-trade');

    $filtro.push($('#_anio').val());
    $filtro.push($('#_mes').val());
    $filtro.push(__a);

    $.ajax({
        beforeSend: function (__s) {
            $div.empty();
            $('#lucky-load').modal('show');
        },
        url: 'TradeMarketing',
        type: 'post',
        dataType: 'json',
        data: {
            __a: '4',
            __b: $filtro.join(',')
        },
        success: function (__s) {
            //$contenedor = __s;

            if (__s != null) {
                $.each(__s, function (i, v) {
                    $div.append('<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3"><div class="panel panel-default"><div class="panel-heading"><h5><a href="javascript:"><i class="fa fa-film"></i>&nbsp;' + v._e + '</a></h5></div><div class="panel-body"><a href="javascript:" onclick="fnOnClickFoto(\'/Foto/c1987/Media/' + v._f + '\')"><img src="/Foto/c1987/Media/' + v._f + '" class="img-responsive" /></a></div></div></div>');
                })
            }
        },
        complete: function () {
            //if ($contenedor != null && $contenedor.length > 0) {
            //    $table.show();
            //    $info.hide();
            //    $div.show();

            //    fnOnClickNavbar('navbar-vertical-filtro', false);
            //} else {
            //    $table.hide();
            //    $info.show();
            //    $div.hide();
            //}

            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);

            //$table.hide();
            //$info.show();
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnChangeAnio() {
    var parametro = [];

    parametro.push($('#_anio').val());

    $.ajax({
        beforeSend: function (__s) {
            fnRemoveOptionSelect('_mes');
            $('#lucky-load').modal('show');
        },
        url: 'Panel',
        type: 'post',
        dataType: 'json',
        data: { opcion: '1', parametro: parametro.join(',') },
        success: function (s) {
            if (s == null) return;

            fnInsertOptionSelect('_mes', s);
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (__s) {
            console.error(__s);
            $('#lucky-load').modal('hide');
        }
    });
}