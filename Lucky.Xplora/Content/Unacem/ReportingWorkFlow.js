﻿$(function () {
    fnOnClickNavbar('navbar-vertical', false);
    $('#table-Presupuestal').bootstrapTable();

    $('#DivHighChartsStatusFachada').highcharts({
        chart: { type: 'column', height: 200, spacingBottom: 0 },
        title: false,
        xAxis: { categories: [], labels: { style: { fontSize: '8px' } } },
        yAxis: { title: false, tickInterval: 1, gridLineColor: 'transparent', labels: { enabled: false }, stackLabels: { enabled: true, useHTML: true, formatter: function () { if (this.total != null) { return '<div style="background-color:gray !important;color:white;text-align:center;font-size:9px;width:25px;">' + this.total + '</div>'; } else { return '<div></div>'; } }, style: { fontWeight: 'bold', color: 'black' },y:-7 } },
        credits: { enabled: false },
        legend: { align: 'center', x: -30, verticalAlign: 'top', y: -18, itemStyle: { fontSize: '9px' } },
        tooltip: { headerFormat: '<b>{point.x}</b><br/>', pointFormat: '{series.name}: <b>{point.y}</b> ({point.percentage:.0f}%)' },
        plotOptions: { column: { stacking: 'percent', dataLabels: { enabled: true, color: 'black' }}}
    });

    $('#DivHighChartsStatusPermiso').highcharts({
        chart: { type: 'column', height: 200, spacingBottom: 0 },
        title: false,
        xAxis: { categories: [], labels: { style: { fontSize: '8px' } } },
        yAxis: { title: false, tickInterval: 1, gridLineColor: 'transparent', labels: { enabled: false }, stackLabels: { enabled: true, useHTML: true, formatter: function () { if (this.total != null) { return '<div style="background-color:gray !important;color:white;text-align:center;font-size:9px;width:25px;">' + this.total + '</div>'; } else { return '<div></div>'; } }, style: { fontWeight: 'bold', color: 'black' }, y: -7 } },
        credits: { enabled: false },
        legend: { align: 'center', x: -30, verticalAlign: 'top', y: -18, itemStyle: { fontSize: '9px' } },
        tooltip: { headerFormat: '<b>{point.x}</b><br/>', pointFormat: '{series.name}: <b>{point.y}</b> ({point.percentage:.0f}%)' },
        plotOptions: { column: { stacking: 'percent', dataLabels: { enabled: true, color: 'black' }}}
    });

    $('#DivHighChartsStatusPresupuestal').highcharts({
        chart: { zoomType: 'xy', height: 200, spacingBottom: 0 },
        title: false,
        subtitle: false,
        xAxis: [{ categories: [], crosshair: true, labels: { style: { fontSize: '8px' } } }],
        yAxis: [{ labels: { enabled: false }, gridLineColor: 'transparent', title: false }, { labels: { enabled: false }, gridLineColor: 'transparent', title: false }, { labels: { enabled: false }, gridLineColor: 'transparent', title: false }],
        credits: { enabled: false },
        tooltip: { shared: true, valuePrefix: 'S/. ' },
        legend: { align: 'center', x: -10, verticalAlign: 'top', y: -10, itemStyle: { fontSize: '9px' }},
        plotOptions: { spline: { dataLabels: { enabled: true, formatter: function () { return this.point.custom; } } }, column: { dataLabels: { enabled: true } } }
    });

    $('#sel_anio1').multiselect({ dropRight: true, buttonClass: 'btn btn-default btn-xs' });
    $('#sel_anio2').multiselect({ dropRight: true, buttonClass: 'btn btn-default btn-xs' });
    $('#sel_anio3').multiselect({ dropRight: true, buttonClass: 'btn btn-default btn-xs' });
    $('#sel_mes1').multiselect({ numberDisplayed: 1, dropRight: true, buttonWidth: '100px', includeSelectAllOption: true, allSelectedText: 'Total Mes', nonSelectedText: '- Meses -', buttonClass: 'btn btn-default btn-xs' }); $("#sel_mes1").multiselect('selectAll', false); $("#sel_mes1").multiselect('updateButtonText');
    $('#sel_mes2').multiselect({ numberDisplayed: 1, dropRight: true, buttonWidth: '100px', includeSelectAllOption: true, allSelectedText: 'Total Mes', nonSelectedText: '- Meses -', buttonClass: 'btn btn-default btn-xs' }); $("#sel_mes2").multiselect('selectAll', false); $("#sel_mes2").multiselect('updateButtonText');
    $('#sel_marca1').multiselect({ numberDisplayed: 1, dropRight: true, buttonWidth: '100px', includeSelectAllOption: true, nonSelectedText: '- Marcas -', allSelectedText: 'Total Marca', buttonClass: 'btn btn-default btn-xs' }); $("#sel_marca1").multiselect('selectAll', false); $("#sel_marca1").multiselect('updateButtonText');
    $('#sel_marca2').multiselect({ numberDisplayed: 1, dropRight: true, buttonWidth: '100px', includeSelectAllOption: true, nonSelectedText: '- Marcas -', allSelectedText: 'Total Marca', buttonClass: 'btn btn-default btn-xs' }); $("#sel_marca2").multiselect('selectAll', false); $("#sel_marca2").multiselect('updateButtonText');
    $('#sel_marca3').multiselect({ numberDisplayed: 1, dropRight: true, buttonWidth: '100px', includeSelectAllOption: true, nonSelectedText: '- Marcas -', allSelectedText: 'Total Marca', buttonClass: 'btn btn-default btn-xs' }); $("#sel_marca3").multiselect('selectAll', false); $("#sel_marca3").multiselect('updateButtonText');
    $('#sel_zona').multiselect({ numberDisplayed: 1, dropRight: true, maxHeight: 200, buttonWidth: '100px', includeSelectAllOption: true, nonSelectedText: '- Zonas -', allSelectedText: 'Total Zona', buttonClass: 'btn btn-default btn-xs' }); $("#sel_zona").multiselect('selectAll', false); $("#sel_zona").multiselect('updateButtonText');
    $('#sel_contacto1').multiselect({ numberDisplayed: 1, dropRight: true, buttonWidth: '100px', maxHeight: 200, includeSelectAllOption: true, nonSelectedText: '- Supervisor -', allSelectedText: 'Total Sup.', buttonClass: 'btn btn-default btn-xs' }); $("#sel_contacto1").multiselect('selectAll', false); $("#sel_contacto1").multiselect('updateButtonText');
    $('#sel_contacto2').multiselect({ numberDisplayed: 1, dropRight: true, buttonWidth: '100px', maxHeight: 200, includeSelectAllOption: true, nonSelectedText: '- Supervisor -', allSelectedText: 'Total Sup.', buttonClass: 'btn btn-default btn-xs' }); $("#sel_contacto2").multiselect('selectAll', false); $("#sel_contacto2").multiselect('updateButtonText');
    $('#sel_contacto3').multiselect({ numberDisplayed: 1, dropRight: true, buttonWidth: '100px', maxHeight: 200, includeSelectAllOption: true, nonSelectedText: '- Supervisor -', allSelectedText: 'Total Sup.', buttonClass: 'btn btn-default btn-xs' }); $("#sel_contacto3").multiselect('selectAll', false); $("#sel_contacto3").multiselect('updateButtonText');

    fnOnClickBuscarStatusFachada();
    fnOnClickBuscarStatusPermiso();
    fnOnClickBuscarStatusPresupuestal();
});

function fnOnClickBuscarStatusFachada() {
    var Filtro = [],
        StatusFachadaCategoria = [],
        StatusFachada = $('#DivHighChartsStatusFachada').highcharts(),
        ContactoCountTotal = $('#sel_contacto1 option').length,
        ContactoCountSeleccionado = $('#sel_contacto1 option:selected').length;

    if ($('#sel_anio1').val() == null) {
        alert('Seleccione año.');
        return;
    }

    if ($('#sel_mes1').val() == null) {
        alert('Seleccione mes.');
        return;
    }

    if ($('#sel_marca1').val() == null) {
        alert('Seleccione marca.');
        return;
    }

    if ($('#sel_contacto1').val() == null) {
        alert('Seleccione contacto.');
        return;
    }

    Filtro.push($('#sel_anio1').val());
    Filtro.push($('#sel_mes1').val());
    Filtro.push($('#sel_marca1').val());
    Filtro.push($('#sel_contacto1').val());
    Filtro.push(Campania.Idplanning);

    //if (ContactoCountTotal == ContactoCountSeleccionado) {
    //    Filtro.push('0');
    //} else {
    //    Filtro.push($('#sel_contacto1').val());
    //}

    $.ajax({
        beforeSend: function (__s) {
            while (StatusFachada.series.length > 0) {
                StatusFachada.series[0].remove();
            }

            StatusFachada.xAxis[0].setCategories(StatusFachadaCategoria);

            $('#lucky-load').modal('show');
        },
        url: 'Reporting',
        type: 'post',
        dataType: 'json',
        data: { opcion: '0', parametro: Filtro.join('|') },
        success: function (response) {
            if (response == null) return;

            $.each(response, function (i, v) {
                var ArrayZona = [];
                //var ArrayTooltip = [];

                $.each(v._d, function (iZona, vZona) {
                    if (i == 0) {
                        StatusFachadaCategoria.push(vZona._b);
                    }

                    if (vZona._c == 0) {
                        ArrayZona.push(null);
                    } else {
                        ArrayZona.push(vZona._c);
                    }
                    
                    //ArrayTooltip.push(vZona._d);
                    //ArrayZona.push(parseInt((vZona._d).replace("%","")));
                });


                StatusFachada.addSeries({ name: v._b, data: ArrayZona, color: v._c });
                //StatusFachada.addtooltip({ pointFormat: '{series.name}: {' + ArrayTooltip + '}' });

                //tooltip: { headerFormat: '<b>{point.x}</b><br/>', pointFormat: '{series.name}: {point.y}' },
            });

            StatusFachada.xAxis[0].setCategories(StatusFachadaCategoria);
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });

}
function fnOnClickBuscarStatusPermiso() {
    var Filtro = [],
        StatusPermisoCategoria = [],
        StatusPermiso = $('#DivHighChartsStatusPermiso').highcharts(),
        ContactoCountTotal = $('#sel_contacto2 option').length,
        ContactoCountSeleccionado = $('#sel_contacto2 option:selected').length;

    if ($('#sel_anio2').val() == null) {
        alert('Seleccione año.');
        return;
    }

    if ($('#sel_mes2').val() == null) {
        alert('Seleccione mes.');
        return;
    }

    if ($('#sel_marca2').val() == null) {
        alert('Seleccione marca.');
        return;
    }

    if ($('#sel_contacto2').val() == null) {
        alert('Seleccione contacto.');
        return;
    }

    Filtro.push($('#sel_anio2').val());
    Filtro.push($('#sel_mes2').val());
    Filtro.push($('#sel_marca2').val());
    Filtro.push($('#sel_contacto2').val());
    Filtro.push(Campania.Idplanning);

    //if (ContactoCountTotal == ContactoCountSeleccionado) {
    //    Filtro.push('0');
    //} else {
    //    Filtro.push($('#sel_contacto2').val());
    //}

    $.ajax({
        beforeSend: function (__s) {
            while (StatusPermiso.series.length > 0) {
                StatusPermiso.series[0].remove();
            }

            StatusPermiso.xAxis[0].setCategories(StatusPermisoCategoria);

            $('#lucky-load').modal('show');
        },
        url: 'Reporting',
        type: 'post',
        dataType: 'json',
        data: { opcion: '1', parametro: Filtro.join('|') },
        success: function (response) {
            if (response == null) return;

            $.each(response, function (i, v) {
                var ArrayZona = [];
                $.each(v._d, function (iZona, vZona) {
                    if (i == 0) {
                        StatusPermisoCategoria.push(vZona._b);
                    }
                    if (vZona._c == 0) {
                        ArrayZona.push(null);
                    } else {
                        ArrayZona.push(vZona._c);
                    }
                });
                StatusPermiso.addSeries({ name: v._b, data: ArrayZona, color: v._c });
            });
            StatusPermiso.xAxis[0].setCategories(StatusPermisoCategoria);
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickBuscarStatusPresupuestal() {
    var Filtro = [],
        StatusPresupuestalCategoria = [],
        ArrayInversion = [],
        ArrayInversionFachada = [],
        ArrayInversionAcumulada = [],
        StatusPresupuestal = $('#DivHighChartsStatusPresupuestal').highcharts(),
        ContactoCountTotal = $('#sel_contacto3 option').length,
        ContactoCountSeleccionado = $('#sel_contacto3 option:selected').length;

    if($('#sel_anio3').val() == null){
        alert('Seleccione año.');
        return;
    }

    if($('#sel_zona').val() == null){
        alert('Seleccione zona.');
        return;
    }

    if($('#sel_contacto3').val() == null){
        alert('Seleccione contacto.');
        return;
    }

    if($('#sel_marca3').val() == null){
        alert('Seleccione marca.');
        return;
    }

    Filtro.push($('#sel_anio3').val());
    Filtro.push($('#sel_marca3').val());
    Filtro.push($('#sel_zona').val());
    Filtro.push($('#sel_contacto3').val());
    Filtro.push(Campania.Idplanning);

    //if (ContactoCountTotal == ContactoCountSeleccionado) {
    //    Filtro.push('0');
    //} else {
    //    Filtro.push($('#sel_contacto3').val());
    //}

    $.ajax({
        beforeSend: function (__s) {
            while (StatusPresupuestal.series.length > 0) {
                StatusPresupuestal.series[0].remove();
            }

            StatusPresupuestal.xAxis[0].setCategories(StatusPresupuestalCategoria);

            $('#lucky-load').modal('show');
        },
        url: 'Reporting',
        type: 'post',
        dataType: 'json',
        data: { opcion: '2', parametro: Filtro.join('|') },
        success: function (response) {
            if (response == null) return;

            var re = /,/gi;
            $.each(response, function (i, v) {
                
                //ArrayInversion.push(parseFloat(v._f.replace(',', '')));
                //ArrayInversionFachada.push(parseFloat(v._h.replace(',', '')));
                //ArrayInversionAcumulada.push(parseFloat(v._i.replace(',', '')));
                if (v._c == 'TOTAL') {

                } else {
                    ArrayInversion.push({ y: Math.round(parseInt(v._f.replace(re, "")) / 1000), custom: formatearNumero(Math.round(parseInt(v._f.replace(re, "")) / 1000)) });
                    ArrayInversionFachada.push({ y: Math.round(parseInt(v._h.replace(re, "")) / 1000), custom: formatearNumero(Math.round(parseInt(v._h.replace(re, "")) / 1000)) });
                    ArrayInversionAcumulada.push({ y: Math.round(parseInt(v._i.replace(re, "")) / 1000), custom: formatearNumero(Math.round(parseInt(v._i.replace(re, "")) / 1000)) });


                    StatusPresupuestalCategoria.push(v._c);
                }
                
            });


            StatusPresupuestal.addSeries({ name: 'Inversion x Fachada', type: 'column', yAxis: 1, data: ArrayInversionFachada, color: '#C7C7C7' });
            StatusPresupuestal.addSeries({ name: 'Inversion', type: 'spline', yAxis: 2, data: ArrayInversion, color: '#3C82B4' });
            StatusPresupuestal.addSeries({ name: 'Inversion Acumulada', type: 'spline', yAxis: 2, data: ArrayInversionAcumulada, color: '#66FF33' });

            $('#table-Presupuestal').bootstrapTable('destroy');
            $('#table-Presupuestal').bootstrapTable({ data: response });

            StatusPresupuestal.xAxis[0].setCategories(StatusPresupuestalCategoria);
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (e) {
            console.error(e);
            $('#lucky-load').modal('hide');
        }
    });
}
function fnOnClickStatusFachada() {
    

    
}
function fnOnClickStatusPermiso() {
    

    
}