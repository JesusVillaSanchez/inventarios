﻿var _arraygral = [],
    __vindexg = 0;

$(function () {
    fn_consutar_grilla();
});
//$(document).on('keydown', '#select_op_ventas', function () {
//    fnventascatoption();
//});
$(document).keyup(function (event) {
    if (event.which == 27) {
        $('#id-md-fachada').modal('hide');
    }
});

function fnOnClickBusca() {
    fn_consutar_grilla();
}
function fnOnClickModal(indice,tipo) {
    if (indice < 0) return;
    if (_arraygral.length < (indice + 1)) return;

    __vindexg = indice;

    var _valores = _arraygral[indice];

    $('#id-md-fachada .fa-picture-o').hide();
    $('#id-md-fachada img').show();
    $('#id-md-fachada').modal({ backdrop: 'static', keyboard: true });

    if (Direccion.length > 35) {
        Direccion = Direccion.substring(1, 35);
    }

    if (_valores._cn.replace(/ /gi, "") != '') {
        $('#DescripcionZona').html(_valores._cn);
    } else {
        $('#DescripcionZona').html("Sin zona");
    }
    if (_valores._de.replace(/ /gi, "") != '') {
        $('#DescripcionUsuario').html(_valores._de);
    } else {
        $('#DescripcionUsuario').html("Sin Usuario");
    }
    if (_valores._c.replace(/ /gi, "") != '') {
        if ((_valores._c.replace(/ /gi, "")).length > 33) {
            $('#Nom_PDV').html((_valores._c).substring(0, 33));
            $('#Nom_PDV').attr("title", _valores._c);
        } else {
            $('#Nom_PDV').html(_valores._c);
            $('#Nom_PDV').removeAttr("title");
        }
    } else {
        $('#Nom_PDV').html("Sin Nombre de Punto de Venta");
        $('#Nom_PDV').removeAttr("title");
    }
    if (_valores._m.replace(/ /gi, "") != '') {
        $('#Distribuidora').html(_valores._m);
    } else {
        $('#Distribuidora').html("Sin Distribuidora");
    }
    if (_valores._g.replace(/ /gi, "") != '') {
        $('#Distrito').html(_valores._g);
    } else {
        $('#Distrito').html("Sin Distrito");
    }
    if (_valores._f.replace(/ /gi, "") != '') {
        if ((_valores._f.replace(/ /gi, "")).length > 33) {
            $('#Direccion').html((_valores._f).substring(0, 33));
            $('#Direccion').attr("title", _valores._f);
        } else {
            $('#Direccion').html(_valores._f);
            $('#Direccion').removeAttr("title");
        }
    } else {
        $('#Direccion').html("Sin Direccion");
        $('#Direccion').removeAttr("title");
    }
    if (_valores._co.replace(/ /gi, "") != '') {
        $('#DescripcionMarca').html(_valores._co);
    } else {
        $('#DescripcionMarca').html("Sin Marca");
    }
    if (_valores._cp.replace(/ /gi, "") != '') {
        $('#DescripcionTipoSolicitud').html(_valores._cp);
    } else {
        $('#DescripcionTipoSolicitud').html("Sin Tipo de Solicitud");
    }
    if (_valores._bl.replace(/ /gi, "") != '') {
        $('#P1_Comentarios').html(_valores._bl);
    } else {
        $('#P1_Comentarios').html("Sin comentarios");
    }
    if (_valores._au.replace(/ /gi, "") != '') {
        $('#Obs_Tramite').html(_valores._au);
    } else {
        $('#Obs_Tramite').html("Sin comentarios");
    }

    if (_valores._cu == 'CONCLUIDO') {
        $('#lblEstadoTramite1').css({ 'background-color': '#27313B' });//FONDO NEGRO
        $('#lblEstadoTramite2').css({ 'background-color': '#8EA2BC' });//FONDO PLOMO

        $('#lblEstadoTramite11').removeClass("btn-default btn-primary").addClass("btn-primary");
        $('#lblEstadoTramite22').removeClass("btn-default btn-primary").addClass("btn-default");

        $('#lblTipoTramite1').html("CON PERMISO");
        $('#lblTipoTramite2').html("CON CARTA");
        $('#lblTipoTramite11').val("CON PERMISO");
        $('#lblTipoTramite22').val("CON CARTA");
        if (_valores._ct == 'CON PERMISO') {
            $('#lblTipoTramite1').css({ 'background-color': '#27313B', 'padding': '8px' });
            $('#lblTipoTramite2').css({ 'background-color': '#8EA2BC' });
            $('#lblTipoTramite11').removeClass("btn-default btn-primary").addClass("btn-primary");
            $('#lblTipoTramite22').removeClass("btn-default btn-primary").addClass("btn-default");
        } else {
            if (_valores._ct == 'CON CARTA LR') {
                $('#lblTipoTramite1').css({ 'background-color': '#8EA2BC', 'padding': '8px' });
                $('#lblTipoTramite2').css({ 'background-color': '#27313B' });
                $('#lblTipoTramite11').removeClass("btn-default btn-primary").addClass("btn-default");
                $('#lblTipoTramite22').removeClass("btn-default btn-primary").addClass("btn-primary");
            } else {
                $('#lblTipoTramite1').css({ 'background-color': '#8EA2BC', 'padding': '8px' });
                $('#lblTipoTramite2').css({ 'background-color': '#27313B' });
                $('#lblTipoTramite11').removeClass("btn-default btn-primary").addClass("btn-default");
                $('#lblTipoTramite22').removeClass("btn-default btn-primary").addClass("btn-default");
            }
        }
    } else {
        if (_valores._cu == 'EN PROCESO') {
            $('#lblEstadoTramite1').css({ 'background-color': '#8EA2BC' });
            $('#lblEstadoTramite2').css({ 'background-color': '#27313B' });

            $('#lblEstadoTramite11').removeClass("btn-default btn-primary").addClass("btn-default");
            $('#lblEstadoTramite22').removeClass("btn-default btn-primary").addClass("btn-primary");

            $('#lblTipoTramite1').html("EN PROCESO DE PERMISO");
            $('#lblTipoTramite2').html("SIN CARTA");
            $('#lblTipoTramite11').val("EN PROCESO DE PERMISO");
            $('#lblTipoTramite22').val("SIN CARTA");
            if (_valores._ct == 'EN PROCESO DE PERMISO') {
                $('#lblTipoTramite1').css({ 'background-color': '#27313B', 'padding': '1px' });
                $('#lblTipoTramite2').css({ 'background-color': '#8EA2BC' });
                $('#lblTipoTramite11').removeClass("btn-default btn-primary").addClass("btn-primary");
                $('#lblTipoTramite22').removeClass("btn-default btn-primary").addClass("btn-default");
            } else {
                if (_valores._ct == 'SIN CARTA LR') {
                    $('#lblTipoTramite1').css({ 'background-color': '#8EA2BC', 'padding': '1px' });
                    $('#lblTipoTramite2').css({ 'background-color': '#27313B' });
                    $('#lblTipoTramite11').removeClass("btn-default btn-primary").addClass("btn-default");
                    $('#lblTipoTramite22').removeClass("btn-default btn-primary").addClass("btn-primary");
                } else {
                    $('#lblTipoTramite1').css({ 'background-color': '#8EA2BC', 'padding': '1px' });
                    $('#lblTipoTramite2').css({ 'background-color': '#27313B' });
                    $('#lblTipoTramite11').removeClass("btn-default btn-primary").addClass("btn-default");
                    $('#lblTipoTramite22').removeClass("btn-default btn-primary").addClass("btn-default");
                }
            }
        } else {
            $('#lblTipoTramite1').html("CON PERMISO");
            $('#lblTipoTramite2').html("CON CARTA");
            $('#lblTipoTramite11').val("CON PERMISO");
            $('#lblTipoTramite22').val("CON CARTA");
            $('#lblEstadoTramite1').css({ 'background-color': '#8EA2BC' });
            $('#lblEstadoTramite2').css({ 'background-color': '#8EA2BC' });

            $('#lblEstadoTramite11').removeClass("btn-default btn-primary").addClass("btn-default");
            $('#lblEstadoTramite22').removeClass("btn-default btn-primary").addClass("btn-default");



            $('#lblTipoTramite1').css({ 'background-color': '#8EA2BC', 'padding': '8px' });
            $('#lblTipoTramite2').css({ 'background-color': '#8EA2BC' });
            $('#lblTipoTramite11').removeClass("btn-default btn-primary").addClass("btn-default");
            $('#lblTipoTramite22').removeClass("btn-default btn-primary").addClass("btn-default");
        }
    }

    //if (tipo == 2) {
    $('.tbl-fachada .td-image1 img').removeAttr('src');
    $('.tbl-fachada .td-image2 img').removeAttr('src');
    //}
    

    //if (_valores._cw != '') {
    //    $('#tipo_foto1').html("SOLICITUD");
    //    $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image1 img').attr('src', 'https://www.xplora.com.pe:9096' + _valores._cw).css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });

    //} else {
    //    $('#tipo_foto1').html("SIN FOTO");
    //    $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image1 img').attr('src', '#').css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });
    //}
    //if (_valores._dc != '') {
    //    $('#tipo_foto2').html("REPORTE FOTOGRAFICO");
    //    $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image2 img').attr('src', 'https://www.xplora.com.pe:9096' + _valores._dc).css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });
    //} else {
    //    if (_valores._cz != '') {
    //        $('#tipo_foto2').html("FOTOMONTAJE");
    //        $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image2 img').attr('src', 'https://www.xplora.com.pe:9096' + _valores._cz).css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });
    //    } else {
    //        $('#tipo_foto2').html("SIN FOTO");
    //        $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image2 img').attr('src', '#').css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });
    //    }
    //}
    if (_valores._cw != '') {
        $('#tipo_foto1').html("SOLICITUD");
        $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image1 img').attr('src', _valores._cw).css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });

    } else {
        $('#tipo_foto1').html("SIN FOTO");
        $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image1 img').attr('src', '#').css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });
    }
    if (_valores._dc != '') {
        $('#tipo_foto2').html("REPORTE FOTOGRAFICO");
        $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image2 img').attr('src', _valores._dc).css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });
    } else {
        if (_valores._cz != '') {
            $('#tipo_foto2').html("FOTOMONTAJE");
            $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image2 img').attr('src', _valores._cz).css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });
        } else {
            $('#tipo_foto2').html("SIN FOTO");
            $('#id-md-fachada' + ' .tbl-fachada' + ' .td-image2 img').attr('src', '#').css({ 'width': '100%', 'height': '265px', 'opacity': '1' }).on("error", function () { $(this).css({ 'opacity': '0', 'display': 'none' }); $(this).parent().children('i').show(); });
        }
    }

    $("#Num_Solicitud").val(_valores._a);
}

function fn_consutar_grilla() {
    var parametro = [], data_parametros = [];
    //var _vfechaventas = $('#dtp_fecha_apro_ventas').val();
    var _vtxtfechaventas = $('#txt_fecha_apro_ventas').val();
    //var _vfechafotos = $('#dtp_fecha_apro_foto').val();
    var _vtxtfechafotos = $('#txt_fecha_apro_foto').val();

    var cbo_mes_prepresupuesto = $('#cbo_mes_wf').val();
    var cbo_anio_prepresupuesto = $('#cbo_anio_wf').val();

    if (cbo_mes_prepresupuesto > 0 && cbo_mes_prepresupuesto < 13) {
        var _vfechapresupuestos = "01/" + cbo_mes_prepresupuesto + "/" + cbo_anio_prepresupuesto;
    } else {
        var _vfechapresupuestos = "";
    }



    if (/*_vfechaventas == null ||*/ _vtxtfechaventas == "") {
        _vfechaventas = "0";
    }

    if (/*_vfechafotos == null ||*/ _vtxtfechafotos == "") {
        _vfechafotos = "0";
    }
    if (_vfechapresupuestos == null || _vfechapresupuestos == "") {
        _vfechapresupuestos = "0";
    }

    parametro.push('\'_w\':' + $('#cbo_zona_wf').val());
    parametro.push('\'_x\':\'' +_vfechaventas + '\'');
    parametro.push('\'_y\':\'' + _vfechafotos + '\'');
    //parametro.push('\'_z\':' + $('#cod_ditribuidora_wf').val());
    parametro.push('\'_z\':' + '0');
    parametro.push('\'_j\':' + $('#cod_usuario_wf').val());
    parametro.push('\'_ae\':\'' + _vfechapresupuestos + '\'');
    parametro.push('\'_ai\':' + '9020111072013');
    parametro.push('\'_c\':' + $('#cod_estado_solicitud_wf').val());
    parametro.push('\'_ap\':' + $('#cod_marca').val());
    
    $.ajax({
        beforeSend: function (xhr) {
            $("#div_container").empty();
            _arraygral = [];
            $('#lucky-load').modal('show');
        },
        url: 'Fotografico',
        type: 'post',
        dataType: 'json',
        data: {
            opcion: '0', parametro: '{' + parametro.join(',') + '}'
        },
        success: function (response) {
            if (response == null) return;

            _arraygral = response;
            var rutafoto = '';
            var fechafoto = '';
            var tipofoto = '';
            //var ruta_fotografico = '';
            //var ruta_fotomontaje = '';
            //var ruta_solicitud = '';
            //console.log(response);
            $.each(response, function (i, v) {
                //data_parametros.length = 0;
                //var image = new Image();
                //image.onload = function () {
                //    var width = this.naturalWidth,
                //        height = this.naturalHeight;

                    if (v._ay != '' && v._ay != null) {
                        tipofoto = 'REPORTE FOTOGRAFICO: ';
                        rutafoto = v._dc;
                        fechafoto = v._dd;
                        //fechafoto = v._ay.replace("/Fachada/Img/Img_", "");
                        //fechafoto = fechafoto.replace(".jpg", "");
                        //fechafoto = fechafoto.replace(".png", "");
                        //fechafoto = fechafoto.split("_");
                        //fechafoto = fechafoto[0];
                        //fechafoto = fechafoto.substring(0, 2) + "/" + fechafoto.substring(2, 4) + "/" + fechafoto.substring(4);
                    } else {
                        //si existe un fotomontaje
                        if (v._ci != '' && v._ci != null) {
                            tipofoto = 'FOTOMONTAJE: ';
                            //var foto = v._ci.split("|");
                            //var fotonum = foto[0].split(",");
                            //rutafoto = fotonum[0];
                            //fechafoto = fotonum[2];
                            //fechafoto = fechafoto.split(" ");
                            //fechafoto = fechafoto[0];
                            rutafoto = v._cz;
                            fechafoto = v._da;
                        } else {
                            //si existe foto solicitud
                            if (v._n != '' && v._n != null) {
                                tipofoto = 'SOLICITUD: ';
                                //var foto2 = v._n.split(",");
                                //rutafoto = foto2[0];
                                //fechafoto = v._b;
                                rutafoto = v._cw;
                                fechafoto = v._b;
                            } else {
                                rutafoto = '';
                                tipofoto = 'SIN FOTO';
                                fechafoto = '';
                            }
                        }
                    }
                    //ruta_fotografico = '';
                    //ruta_fotomontaje = '';
                    //ruta_solicitud = '';
                    //if (v._ay != '' && v._ay != null) {
                    //    ruta_fotografico = v._ay;
                    //}
                    //if (v._ci != '' && v._ci != null) {
                    //    var ruta_fotomontaje1 = v._ci.split("|");
                    //    ruta_fotomontaje1 = ruta_fotomontaje1[0].split(",");
                    //    ruta_fotomontaje = ruta_fotomontaje1[0];
                    //}
                    //if (v._n != '' && v._n != null) {
                    //    var foto3 = v._n.split(",");
                    //    ruta_solicitud = foto3[0];
                    //}

                    $("#div_container").append('<div class="cls-foto-item cls-el-' + v._a + ' col-xs-12 col-sm-6 col-md-2 col-lg-2"></div>');
                    

                    var re = /\'/gi;
                    var Nom_PDV = v._c.replace(re, "&#39;");
                    var Distribuidora = v._m.replace(re, "&#39;");
                    var Distrito = v._g.replace(re, "&#39;");
                    var Direccion = v._f.replace(re, "&#39;");
                    var Direccion = v._f.replace(re, "&#39;");
                    var P1_Comentarios = v._bl.replace(re, "&#39;");
                    var Obs_Tramite = v._au.replace(re, "&#39;");
                    var PrimerNombrePrimerApellidoUsuario = v._de.replace(re, "&#39;");
                    re = /\"/gi;
                    Nom_PDV = Nom_PDV.replace(re, "&#34;");
                    Distribuidora = Distribuidora.replace(re, "&#34;");
                    Distrito = Distrito.replace(re, "&#34;");
                    Direccion = Direccion.replace(re, "&#34;");
                    P1_Comentarios = P1_Comentarios.replace(re, "&#34;");
                    Obs_Tramite = Obs_Tramite.replace(re, "&#34;");
                    PrimerNombrePrimerApellidoUsuario = PrimerNombrePrimerApellidoUsuario.replace(re, "&#34;");
                    re = /,/gi;
                    Nom_PDV = Nom_PDV.replace(re, "&#44;");
                    Distribuidora = Distribuidora.replace(re, "&#44;");
                    Distrito = Distrito.replace(re, "&#44;");
                    Direccion = Direccion.replace(re, "&#44;");
                    P1_Comentarios = P1_Comentarios.replace(re, "&#44;");
                    Obs_Tramite = Obs_Tramite.replace(re, "&#44;");
                    PrimerNombrePrimerApellidoUsuario = PrimerNombrePrimerApellidoUsuario.replace(re, "&#44;");
                    re = /\n/gi;
                    Nom_PDV = Nom_PDV.replace(re, "&#13;");
                    Distribuidora = Distribuidora.replace(re, "&#13;");
                    Distrito = Distrito.replace(re, "&#13;");
                    Direccion = Direccion.replace(re, "&#13;");
                    P1_Comentarios = P1_Comentarios.replace(re, "&#13;");
                    Obs_Tramite = Obs_Tramite.replace(re, "&#13;");
                    PrimerNombrePrimerApellidoUsuario = PrimerNombrePrimerApellidoUsuario.replace(re, "&#13;");

                    $('.cls-el-' + v._a).html($('#dv-elemento').html());
                    $('.cls-el-' + v._a + ' .cls-pdv').html(v._c);
                    //$('.cls-el-' + v._a + ' img')
                    //    .attr('src', 'https://www.xplora.com.pe:9096' + rutafoto)
                    //    .on("error", function () {
                    //        $(this).css('opacity', '0');
                    //        $(this).parent().children('i').show();
                    //    });
                    $('.cls-el-' + v._a + ' img')
                        .attr('src', rutafoto)
                        .on("error", function () {
                            $(this).css('opacity', '0');
                            $(this).parent().children('i').show();
                        });
                    $('.cls-el-' + v._a + ' .cls-fecha').html(tipofoto + fechafoto);
                    //$('.cls-el-' + v._a + ' .cls-imagen').attr('data-parametros', '{ "Num_Solicitud": ' + v._a + ', "DescripcionZona": "' + v._cn + '", "PrimerNommbrePrimerApellidoUsuario": "' + v._cj + ' ' + v._cl + '", "Nom_PDV": "' + Nom_PDV + '", "Distribuidora": "' + Distribuidora + '", "Distrito": "' + Distrito + '", "Direccion": "' + Direccion + '", "DescripcionMarca": "' + v._co + '", "DescripcionTipoSolicitud": "' + v._cp + '", "ruta_solicitud": "' + ruta_solicitud + '", "ruta_fotomontaje": "' + ruta_fotomontaje + '", "ruta_fotografico": "' + ruta_fotografico + '", "DescripcionTipoTramite": "' + v._ct + '", "DescripcionEstadoTramite": "' + v._cu + '", "P1_Comentarios": "' + P1_Comentarios + '", "Obs_Tramite": "' + Obs_Tramite + '"' + ' }');
                    $('.cls-el-' + v._a + ' .cls-imagen').attr('data-idfoto', i);
                    $('.cls-el-' + v._a + ' .cls-imagen').attr('data-parametros', '{ "_a": "' + v._a + '", "_cn": "' + v._cn + '", "_de": "' + PrimerNombrePrimerApellidoUsuario + '", "_c": "' + Nom_PDV + '", "_m": "' + Distribuidora + '", "_g": "' + Distrito + '", "_f": "' + Direccion + '", "_co": "' + v._co + '", "_cp": "' + v._cp + '", "_cw": "' + v._cw + '", "_cz": "' + v._cz + '", "_dc": "' + v._dc + '", "_ct": "' + v._ct + '", "_cu": "' + v._cu + '", "_bl": "' + P1_Comentarios + '", "_au": "' + Obs_Tramite + '"' + ' }');
                    $('.cls-el-' + v._a + ' .cls-imagen')
                        .attr('id', v._a)
                        .attr('href', 'javascript:' + 'fnOnClickModal(' + i + ',1);');
                    $('.cls-el-' + v._a + ' .cls-fotografico').attr('value', v._a);
                    //$('.cls-el-' + v._a + ' .cls-imagen').attr('data-parametros', '{ "Num_Solicitud": ' + v._a + ', "DescripcionZona": "' + v._cn + '", "PrimerNommbrePrimerApellidoUsuario": "' + v._cj + ' ' + v._cl + '", "Nom_PDV": "' + Nom_PDV + '", "Distribuidora": "' + Distribuidora + '", "Distrito": "' + Distrito + '", "Direccion": "' + Direccion + '", "DescripcionMarca": "' + v._co + '", "DescripcionTipoSolicitud": "' + v._cp + '", "ruta_solicitud": "' + ruta_solicitud + '", "ruta_fotomontaje": "' + ruta_fotomontaje + '", "ruta_fotografico": "' + ruta_fotografico + '", "DescripcionTipoTramite": "' + v._ct + '", "DescripcionEstadoTramite": "' + v._cu + '"' + ' }');
                //};
                //image.src = 'https://www.xplora.com.pe:9096' + rutafoto;
            });
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
            $('#lucky-load').modal('hide');
        }
    });
}

function fnOnClickFoto(__a) {
    //alert(__a);
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];

    __arrayFotos.push(
        { src: 'https://www.xplora.com.pe:9096' + __a, w: 1100, h: 5000 }
        );

    var options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: false
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
}

$(document).on('click', '.cls-imagen', function () {
    toggleFullScreen(document.getElementById('id-md-fachada'));
});

function toggleFullScreen(elem) {
    
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}

$(document).on('click', '.dropdown-menu > li > a:eq(0)', function (e) {
    var jsonData = [],
        extArchivo = '.pptx';

    //$luckyBuscar = $(this).button('loading');

    $('.cls-foto-item .panel-body > input[type=checkbox]:checked').each(function () {
        jsonData.push($.parseJSON($('#' + $(this).val()).attr('data-parametros')));
    });

    if (jsonData.length >= 1) {
        $.ajax({
            beforeSend: function (xhr) {
                $('#lucky-load').modal('show');
            },
            url: 'DescargaArchivo',
            type: 'POST',
            dataType: 'json',
            data: {
                __a: JSON.stringify(jsonData),
                __b: extArchivo
            },
            success: function (data) {
                //console.log(data);
                //$.fileDownload('/Temp/' + data.Archivo + extArchivo);
                //$.fileDownload('@Url.Content("~/Temp/")' + data.Archivo + extArchivo);
                $.fileDownload(Url.Temp + data.Archivo + extArchivo);
            },
            complete: function (_complete) {
                $('#lucky-load').modal('hide');
            },
            error: function (xhr) {
                console.error('[Advertencia]<DescargaArchivo>: Problemas con el servicio.');
            }
        });
    } else {
        alert('¡¡¡Seleccione foto!!!');
    }
});
$(document).on('click', '.dropdown-menu > li > a:eq(1)', function (e) {
    var jsonData = [],
        extArchivo = '.pdf';

    //$luckyBuscar = $(this).button('loading');

    $('.cls-foto-item .panel-body > input[type=checkbox]:checked').each(function () {
        jsonData.push($.parseJSON($('#' + $(this).val()).attr('data-parametros')));
    });

    if (jsonData.length >= 1) {
        $.ajax({
            beforeSend: function (xhr) {
                $('#lucky-load').modal('show');
            },
            url: 'DescargaArchivo',
            type: 'POST',
            dataType: 'json',
            data: {
                __a: JSON.stringify(jsonData),
                __b: extArchivo
            },
            success: function (data) {
                //window.open('@Url.Content("~/Temp/")' + data.Archivo + extArchivo);
                window.open(Url.Temp + data.Archivo + extArchivo);
            },
            complete: function (_complete) {
                $('#lucky-load').modal('hide');
                //$luckyBuscar.button('reset');
            },
            error: function (xhr) {
                console.log('[Advertencia]<DescargaArchivo>: Problemas con el servicio.');
            }
        });
    } else {
        alert('¡¡¡Seleccione foto!!!');
    }
});