﻿$(function () {
    fnOnClickNavbar('navbar-vertical', false);

    var table = $('#table-tecnicos'),
        tableBody = $('#table-tecnicos > tbody');

    if (view != false) {
        $.ajax({
            beforeSend: function (__s) {
                tableBody.empty();
                $('#lucky-load').modal('show');
            },
            url: 'Tecnicos',
            type: 'post',
            dataType: 'json',
            data: { _a: '11' },
            success: function (__s) {
                var array = [];
                array = __s;
            
                table.bootstrapTable({ data: __s });
                table.bootstrapTable('resetView', { height: 435 });
            },
            complete: function () {
                $('#lucky-load').modal('hide');
            },
            error: function (__s) {
                console.error(__s);
                $('#lucky-load').modal('hide');
            }
        });
    }
});


function GuardaTecnico() {

    var formData = new FormData();
    
    var pNombre   = $('#Pnombre').val();
    var sNombre   = $('#Snombre').val();
    var pApellido = $('#Papellido').val();
    var sApellido = $('#Sapellido').val();
    var servicio =  $('#servicio').val();


    formData.append('pNombre', pNombre);
    formData.append('sNombre', sNombre);
    formData.append('pApellido', pApellido);
    formData.append('sApellido', sApellido);
    formData.append('servicio', servicio);


    var request = new XMLHttpRequest();
    request.addEventListener('load', transferComplete, false);
    request.open('post', url.GuardarTecnico);
    request.send(formData);

}

function transferComplete(e) {
    var json = $.parseJSON(e.target.response);
    if (json._a == 'correcto') {
   alert('Tecnico añadido.');
        window.location.href = url.AutomecatronicaTecnicos;
    }
    else {
        alert('No se pudo agregar al tecnico');
    }
}


function desactivarTecnico(_a) {

    var codTecnico = _a;
    var alerta = confirm('¿Esta seguro de desabilitar al técnico?')
    if (alerta == true) {

        $.ajax({
            url: url.DesabilitarTecnicos,
            type:'post',
            dataType:'json',
            data: {'_a':codTecnico } ,
            success: function (__a) {
                alert('Tecnico desabilitado.');
                window.location.href = url.AutomecatronicaTecnicos;

            }


        });
    }
    else{
        return false;
    }
}
