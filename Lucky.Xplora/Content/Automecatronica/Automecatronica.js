﻿$(function () {
    fnOnClickNavbar('navbar-vertical', false);

    var table = $('#table-operacion'),
        tableBody = $('#table-operacion > tbody');

    if (view != false) {
        $.ajax({
            beforeSend: function (__s) {
                tableBody.empty();
                $('#lucky-load').modal('show');
            },
            url: 'Automecatronica',
            type: 'post',
            dataType: 'json',
            data: { __a: '9', __b: persona.Perfil },
            success: function (__s) {
                table.bootstrapTable({ data: __s });
                table.bootstrapTable('resetView', { height: 435 });
            },
            complete: function () {
                $('#lucky-load').modal('hide');
            },
            error: function (__s) {
                console.error(__s);
                $('#lucky-load').modal('hide');
            }
        });
    }
})

function fnOnClickCelda(__v, __r, __i) {
    return '<a style="color:#000;" href="' + url.AutomecatronicaUpdate + '?a=' + __r._a + '">' + __v + '</a>';
}
function fnOnClickGuarda() {
    var formData = new FormData(),
        arrayServicio = [],
        arrayFotos = [],
        arrayFotos2 = [],
        arrayFotoPrincipal = [];

    var inicio = $('#InicioPro').val().toString('dd/MM/yyyy');
    var importe = $('#Importe').val();
    var costoP = $('#HrsPintura').val();
    var costoM = $('#HrsMecan').val();
    var costoC = $('#HrsChapa').val();
    var aprob = $("#FechaAprob").val();
    var aju = $("#FechaAju").val();
    var auda = $("#FechaAdua").val();
    var ingreso = $("#IngresoH").val();
    var oc = $("#EntregaOC").val();
    var solicitudRep = $("#fecha_solicitud_repues").val();
    var entregaRep = $("#fecha_entrega_repues").val();
    var entregaEstimada = $("#T-FechaEstimada").val();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = yyyy + '-' + mm + '-' + dd;

    if ($('#Placa').val() == "") {

        alert('Por favor introduzca la placa');
        return false;
    }

    if ($('#IngresoH').val() == "") {

        $('#IngresoH').val('');
        
    }

    formData.append('input-id', $('#input-id').val());
    formData.append('Placa', $('#Placa').val());
    formData.append('IngresoH', $('#IngresoH').val());
    formData.append('FechaAju', $('#FechaAju').val());
    formData.append('InicioPro', $('#InicioPro').val());
    formData.append('FechaAdua', $('#FechaAdua').val());
    formData.append('FechaAprob', $('#FechaAprob').val());
    formData.append('CotiRep', $('#CotiRep').val());
    formData.append('EntregaOC', $('#EntregaOC').val());
    formData.append('NroOrden', $('#NroOrden').val());
    formData.append('id_trabajo', $('#id_trabajo').val());
    formData.append('HrsChapa', $('#HrsChapa').val());
    formData.append('HrsPintura', $('#HrsPintura').val());
    formData.append('HrsArma', $('#HrsArma').val());
    formData.append('HrsMecan', $('#HrsMecan').val());
    formData.append('Importe', $('#Importe').val());
    formData.append('MontoChapa', $('#MontoChapa').val());
    formData.append('MontoPintura', $('#MontoPintura').val());
    formData.append('HHprepar', $('#HHprepar').val());
    formData.append('MontoMecan', $('#MontoMecan').val());
    formData.append('cbProceso', $('#cbProceso').val());
    formData.append('Ut-Armado', $('#Ut-Armado').val());
    formData.append('Ut-Mecan', $('#Ut-Mecan').val());
    formData.append('T-FechaEstimada', $('#T-FechaEstimada').val());
    formData.append('T-observ', $('#T-observ').val());
    formData.append('entrega-lima', $('#entrega-lima').val());
    formData.append('Asesor', $('#Asesor').val());
    formData.append('Sede', $('#Sede').val());
    formData.append('C-observ', $('#C-observ').val());
    formData.append('Paños', $('#Paños').val());
    formData.append('Repuestos', $('#Repuestos').val());
    formData.append('T-HrsChapa', $('#T-HrsChapa').val());
    formData.append('T-HrsPint', $('#T-HrsPint').val());
    formData.append('cia_seguro', $('#cia_seguro').val());
    formData.append('id_cliente', $('#id_cliente').val());
    formData.append('fecha_solicitud_repues', $('#fecha_solicitud_repues').val());
    formData.append('fecha_entrega_repues', $('#fecha_entrega_repues').val());
    formData.append('mat_pintura', $('#mat_pintura').val());
    formData.append('id_taller', $('#id_taller').val());

    $('select[data-servicio]').each(function () {
        var servicio = $(this).data('servicio');
        var tecnico = $(this).val();

        arrayServicio.push(servicio + '|' + tecnico);
    });
    $('div[data-galeria-ingreso]').each(function () {
        var fotoI = $(this).data('galeria-ingreso');

        arrayFotos.push(fotoI);
    });
    $('div[data-galeria-egreso]').each(function () {
        var fotoE = $(this).data('galeria-egreso');

        arrayFotos2.push(fotoE);
    });

    if (arrayFotos.length > 0) arrayFotoPrincipal.push('1|' + arrayFotos.join('&'));
    if (arrayFotos2.length > 0) arrayFotoPrincipal.push('2|' + arrayFotos2.join('&'));

    formData.append('data-servicio', arrayServicio.join('&'));
    formData.append('foto', arrayFotoPrincipal.join('%'));

    var request = new XMLHttpRequest();
    request.addEventListener('load', transferComplete, false);
    request.open('post', url.AutomecatronicaCrear);
    request.send(formData);
}
function fnOnChangeFile(__a) {
    var oFile = ($('#' + (__a == 'i' ? 'file-imagen-ingreso' : 'file-imagen-egreso')))[0].files[0];

    var form = $('#' + (__a == 'i' ? 'formCargaImagenIngreso' : 'formCargaImagenEgreso'))[0];
    var dataString = new FormData(form);

    $.ajax({
        url: url.AutomecatronicaCargaFoto,
        beforeSend: function () {
            $('#lucky-load').modal('show');
        },
        type: 'post',
        success: function (__s) {
            if (__s._b == true) {
                $('#' + (__a == 'i' ? 'div-galeria-ingreso' : 'div-galeria-egreso')).append('<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1" id="'+ __s._a.slice(0,-4) +'" data-galeria-' + (__a == 'i' ? 'ingreso' : 'egreso') + '="' + __s._a + '" ><a href="javascript:" onclick="fnOnClickFoto(\'' + (__a == 'i' ? 'ingreso' : 'egreso') + '\');"><img src="' + url.AutomecatronicaTempFoto + __s._a + '" class="img-thumbnail" alt="' + __s._a + '" /></a><a href="javascript:" onclick="fnOnRemoveImage(\'' + __s._a + '\');" style="position: absolute; right: 10px; top: -5px;"><i class="fa fa-times" style="background-color: #B40404; padding: 5px; border-radius: 13px; color: white; font-size:10px;"></i></a></div>');
                //alert('Imagen cargada correctamente.');
            } else {
                alert('Problemas al cargar la imagen.');
            }
        },
        dataType: 'json',
        error: function (__e) {
            console.error(__e);
            $('#lucky-load').modal('hide');
        },
        complete: function () {
            $('#lucky-load').modal('hide');
        },
        data: dataString,
        cache: false,
        contentType: false,
        processData: false
    });
}
function transferComplete(e) {
    var json = $.parseJSON(e.target.response);
    if (json._a == 'correcto') {

        window.location.href = url.Automecatronica;
    }
}
function fnOnClickFoto(__a) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
        __arrayFotos = [];


    $('div[data-galeria-' + __a + ']').each(function () {
        var fotoI = $(this).data('galeria-' + __a);

        var vImage = document.createElement('img');

        vImage.onload = function () { loaded = true; };
        vImage.src = url.AutomecatronicaTempFoto + fotoI;

        __arrayFotos.push({ src: url.AutomecatronicaTempFoto + fotoI, w: vImage.naturalWidth, h: vImage.naturalHeight });
    });

    var options = {
        history: false,
        focus: true,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        shareEl: true
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, __arrayFotos, options);
    gallery.init();
}
function transferComplete2(e) {

    var json = $.parseJSON(e.target.response);
    if (json._a == 'correcto') {

        $('#marca-placa').val($('#marca').val());
        $('#chasis-placa').val($('#nrochasis').val());
        $('#color-placa').val($('#color').val());
        $('#Placa').val($('#nroplaca').val());
        $('#modelo-placa').val($('#modelo').val());
        alert('Vehiculo registrado');
        $('#myModal').modal('hide');
        //window.location.href = url.AutomecatronicaVehiculo;
    }
}



function fnOnClikcGuardaVehiculo() {

    var _placa = $('#nroplaca').val();

    $.ajax({
        url: url.ValidaPlaca,
        type: 'post',
        dataType: 'json',
        data: { placa: _placa },
        success: function(_a){

            var valida = _a;

   
            var formData = new FormData();


            if (valida == 'existe') {

                alert(valida);
                alert('La placa ya existe')
                return false;
            }
            else if ($("#nroplaca").val() == "") {

                alert('Ingrese la placa del vehiculo');
                return false;
            }

            else if ($("#marca").val() == "") {

                alert('Ingrese una marca de vehiculo');
                return false;
            }

            formData.append('marca', $('#marca').val());
            formData.append('nrochasis', $('#nrochasis').val());
            formData.append('color', $('#color').val());
            formData.append('nroplaca', $('#nroplaca').val());
            formData.append('modelo', $('#modelo').val());

            var request = new XMLHttpRequest();
            request.addEventListener('load', transferComplete2, false);
            request.open('post', url.AutomecatronicaVehiculo);
            request.send(formData);

        }

    });
        



}
function fnLimpiarControles() {
    $('#nroplaca').val('');
    $('#color').val('');
    $('#marca').val('');
    $('#modelo').val('');
    $('#nrochasis').val('');
}

function fnOnRemoveImage(__a) {
    var parametro = [];
    var re = __a;
    re = re.slice(0, -4);

    $.ajax({
        type: 'post',
        url: 'Automecatronica',
        data: { __a: '8', __b: __a },
        success: function (a , b) {

            console.log(a);
            console.log(b)
            $('#' + re).remove();
           

        }
    });
}