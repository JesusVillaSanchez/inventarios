﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using Lucky.Xplora;
using Lucky.Xplora.ServiceMaps;
using Lucky.Xplora.ServiceCampania;
using Lucky.Xplora.ServiceOperativa;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora
{
    // Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
    // visite http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static Ges_MapsServiceClient _Servicio_Maps;
        public static Ges_CampaniaServiceClient _Servicio_Campania;
        public static Ges_OperativaServiceClient _Servicio_Operativa;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            _Servicio_Maps = new Ges_MapsServiceClient("BasicHttpBinding_IGes_MapsService");
            _Servicio_Campania = new Ges_CampaniaServiceClient("BasicHttpBinding_IGes_CampaniaService");
            _Servicio_Operativa = new Ges_OperativaServiceClient("BasicHttpBinding_IGes_OperativaService");
        }

        #region Utilitarios
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-01
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string _Serialize(object value)
        {
            return JsonConvert.SerializeObject(value, new JsonSerializerSettings() { MaxDepth = Int32.MaxValue });
        }

        /// <summary>
        ///  Autor: jlucero
        /// Fecha: 2014-12-01
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T _Deserialize<T>(string value)
        {
            T obj = Activator.CreateInstance<T>();
            obj = JsonConvert.DeserializeObject<T>(value);
            return obj;
        }

        /// <summary>
        ///  Autor: jlucero
        /// Fecha: 2014-12-01
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public string _Encrypt(string StringValue, string encryptionKey)
        {
            byte[] key = { };
            byte[] IV = { 0x32, 0x41, 0x54, 0x67, 0x73, 0x21, 0x47, 0x19 };
            MemoryStream ms = null;

            try
            {
                //string encryptionKey = "bd5ygNc8";
                key = Encoding.UTF8.GetBytes(encryptionKey);
                byte[] bytes = Encoding.UTF8.GetBytes(StringValue);
                DESCryptoServiceProvider dcp = new DESCryptoServiceProvider();
                ICryptoTransform ict = dcp.CreateEncryptor(key, IV);
                ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, ict, CryptoStreamMode.Write);
                cs.Write(bytes, 0, bytes.Length);
                cs.FlushFinalBlock();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            return Convert.ToBase64String(ms.ToArray());
        }

        /// <summary>
        ///  Autor: jlucero
        /// Fecha: 2014-12-01
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public string _Decrypt(string StringValue, string encryptionKey)
        {
            byte[] key = { };
            byte[] IV = { 0x32, 0x41, 0x54, 0x67, 0x73, 0x21, 0x47, 0x19 };
            MemoryStream ms = null;

            try
            {
                //string encryptionKey = "bd5ygNc8";
                key = Encoding.UTF8.GetBytes(encryptionKey);
                byte[] bytes = new byte[StringValue.Length];
                bytes = Convert.FromBase64String(StringValue);
                DESCryptoServiceProvider dcp = new DESCryptoServiceProvider();
                ICryptoTransform ict = dcp.CreateDecryptor(key, IV);
                ms = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream(ms, ict, CryptoStreamMode.Write);
                cryptoStream.Write(bytes, 0, bytes.Length);
                cryptoStream.FlushFinalBlock();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            Encoding en = Encoding.UTF8;
            return en.GetString(ms.ToArray());
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-08-29
        /// </summary>
        /// <param name="StringValue"></param>
        /// <returns></returns>
        public static string Replace(string StringValue) {
            return StringValue.Replace(",", "&#44;")
                .Replace("\"","&#34;");
        }
        #endregion
    }
}