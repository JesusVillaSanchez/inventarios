﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.DataAccess
{
    public static class InventarioDA
    {
        private static string DB = System.Configuration.ConfigurationManager.
        ConnectionStrings["ConectaDBGestionDocumental"].ConnectionString;

        //0 Success 1 Existe 2 Error
        public static int CrearUsuario(string nombre, string apellido,
            string email, int idRol, string usuario, string password)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_CREATE_USER");
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@Apellido", DbType.String, apellido);
                db.AddInParameter(cmd, "@Usuario", DbType.String, usuario);
                db.AddInParameter(cmd, "@Contrasena", DbType.String, password);
                db.AddInParameter(cmd, "@Email", DbType.String, email);
                db.AddInParameter(cmd, "@IdRol", DbType.Int32, idRol);

                DataSet ds = db.ExecuteDataSet(cmd);
                int rows = (int)ds.Tables[0].Rows[0]["Result"];
                return rows;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return 2;
            }
        }

        //0 Success 1 Existe 2 Error
        public static int ActualizarUsuario(int idUsuario, string nombre, string apellido,
            string email, int idRol, string usuario, string password, int estado)
        {
            try
            {
                bool estadoFinal = false;
                if (estado == 1)
                    estadoFinal = true;
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_UPDATE_USER");
                db.AddInParameter(cmd, "@IdUsuario", DbType.Int32, idUsuario);
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@Apellido", DbType.String, apellido);
                db.AddInParameter(cmd, "@Usuario", DbType.String, usuario);
                db.AddInParameter(cmd, "@Contrasena", DbType.String, password);
                db.AddInParameter(cmd, "@Email", DbType.String, email);
                db.AddInParameter(cmd, "@IdRol", DbType.Int32, idRol);
                db.AddInParameter(cmd, "@Estado", DbType.Boolean, estadoFinal);
                DataSet ds = db.ExecuteDataSet(cmd);
                int rows = (int)ds.Tables[0].Rows[0]["Result"];
                return rows;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return 2;
            }
        }


        public static Lucky.Xplora.Models.Inventario.UsuarioModel ObtenerPersona(string user, string pass)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_LOGIN");
                db.AddInParameter(cmd, "@USUARIO", DbType.String, user);
                db.AddInParameter(cmd, "@CONTRASENA", DbType.String, pass);

                DataSet ds = db.ExecuteDataSet(cmd);
                int rows = (int)ds.Tables[0].Rows[0]["Result"];
                if (rows > 0)
                {

                    var persona = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[1]);
                    List<Lucky.Xplora.Models.Inventario.UsuarioModel> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Lucky.Xplora.Models.Inventario.UsuarioModel>>(persona);
                    return p.ElementAt(0);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static List<Models.Inventario.UsuarioModel> ObtenerUsuarios()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_USERS");
              

                DataSet ds = db.ExecuteDataSet(cmd);
                var persona = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                List<Models.Inventario.UsuarioModel> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.Inventario.UsuarioModel>>(persona);
                return p;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static List<Models.Inventario.RolModel> ObtenerRoles()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_ROLES");


                DataSet ds = db.ExecuteDataSet(cmd);
                var persona = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                List<Models.Inventario.RolModel> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.Inventario.RolModel>>(persona);
                return p;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static List<Models.Inventario.CategoriaModel> ObtenerCategorias()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_CATEGORIA");


                DataSet ds = db.ExecuteDataSet(cmd);
                var persona = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                List<Models.Inventario.CategoriaModel> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.Inventario.CategoriaModel>>(persona);
                return p;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static List<Models.Inventario.ProductoModel> ObtenerProductos()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_PRODUCTOS");


                DataSet ds = db.ExecuteDataSet(cmd);
                var persona = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                List<Models.Inventario.ProductoModel> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.Inventario.ProductoModel>>(persona);
                return p;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static Models.Inventario.ProductoModel ObtenerProducto(int idProducto)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_PRODUCTO");
                db.AddInParameter(cmd, "@IdProducto", DbType.Int32, idProducto);

                DataSet ds = db.ExecuteDataSet(cmd);
                var persona = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                List<Models.Inventario.ProductoModel> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.Inventario.ProductoModel>>(persona);
                return p.ElementAt(0);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static List<Models.Inventario.InventarioModel> ObtenerInventarios()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_INVENTARIOS");

                DataSet ds = db.ExecuteDataSet(cmd);
                var persona = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                List<Models.Inventario.InventarioModel> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.Inventario.InventarioModel>>(persona);
                return p;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static List<Models.Inventario.InventarioCategoriaModel> ObtenerInventariosCategoria()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_INVENTARIOS_CATEGORIA");

                DataSet ds = db.ExecuteDataSet(cmd);
                var persona = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                List<Models.Inventario.InventarioCategoriaModel> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.Inventario.InventarioCategoriaModel>>(persona);
                return p;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }


        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(
            prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        //0 Success 
        public static int CrearProducto(string nombre, string descripcion,
            int idCategoria, int stock, float precio)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_CREATE_PRODUCTO");
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@Descripcion", DbType.String, descripcion);
                db.AddInParameter(cmd, "@IdCategoria", DbType.Int32, idCategoria);
                db.AddInParameter(cmd, "@Stock", DbType.Int32, stock);
                db.AddInParameter(cmd, "@Precio", DbType.Single, precio);

                DataSet ds = db.ExecuteDataSet(cmd);
                int rows = (int)ds.Tables[0].Rows[0]["Result"];
                return rows;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return 2;
            }
        }

        //0 Success 
        public static int CrearInventario(int idProducto, int ingreso,int salida, int stock, string descripcion, int tipo)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_CREATE_INVENTARIO");
                db.AddInParameter(cmd, "@IdProducto", DbType.Int32, idProducto);
                db.AddInParameter(cmd, "@Ingreso", DbType.Int32, ingreso);
                db.AddInParameter(cmd, "@Salida", DbType.Int32, salida);
                db.AddInParameter(cmd, "@Stock", DbType.Int32, stock);
                db.AddInParameter(cmd, "@Descripcion", DbType.String, descripcion);
                db.AddInParameter(cmd, "@Tipo", DbType.Int32, tipo);

                DataSet ds = db.ExecuteDataSet(cmd);
                int rows = (int)ds.Tables[0].Rows[0]["Result"];
                return rows;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return 2;
            }
        }
    }
}