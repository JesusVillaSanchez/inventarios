﻿using Lucky.Xplora.Models.GestionDocumentalWeb;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.DataAccess
{
    public static class UsuarioDA
    {
        private static string DB = System.Configuration.ConfigurationManager.
        ConnectionStrings["ConectaDBGestionDocumental"].ConnectionString;

        public static int CrearEmpresa(string nombre, string descripcion, string direccion, string ciudad,
            string telefono1,string telefono2)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_CREATE_EMPRESA");
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@Descripcion", DbType.String, descripcion);
                db.AddInParameter(cmd, "@Direccion", DbType.String, direccion);
                db.AddInParameter(cmd, "@Ciudad", DbType.String, ciudad);
                db.AddInParameter(cmd, "@Telefono1", DbType.String, telefono1);
                db.AddInParameter(cmd, "@Telefono2", DbType.String, telefono2);

                DataSet ds = db.ExecuteDataSet(cmd);
                //0=Ok, <0 = Existe
                int result = Int32.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }

        public static int ActualizarEmpresa(Int64 idEmpresa,string nombre, string descripcion)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_UPDATE_EMPRESA");
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@Descripcion", DbType.String, descripcion);

                DataSet ds = db.ExecuteDataSet(cmd);
                //0=Ok, <0 = Existe
                int result = Int32.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }

        public static int CrearArea(string nombre, string descripcion)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_CREATE_AREA");
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@Descripcion", DbType.String, descripcion);

                DataSet ds = db.ExecuteDataSet(cmd);
                //0=Ok, <0 = Existe
                int result = Int32.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }

        public static int CrearUsuario(PersonaUsuario persona, string dni, int estadocivil, string direccion,
            string telefono, string telefonoemergencia)
        {
            try
            {
                string user = persona.Usuario;
                string pass = persona.Password;
                string nombre = persona.Nombre;
                string apePaterno = persona.ApellidoPaterno;
                string apeMaterno = persona.ApellidoMaterno;
                int idRol = persona.IdRol;
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[WEB].SP_CREATE_USER");
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@ApePaterno", DbType.String, apePaterno);
                db.AddInParameter(cmd, "@ApeMaterno", DbType.String, apeMaterno);
                db.AddInParameter(cmd, "@IdRol", DbType.Int32, idRol);
                db.AddInParameter(cmd, "@Usuario", DbType.String, user);
                db.AddInParameter(cmd, "@Password", DbType.String, pass);
                db.AddInParameter(cmd, "@Dni", DbType.String, dni);
                db.AddInParameter(cmd, "@IdEstadoCivil", DbType.Int32, estadocivil);
                db.AddInParameter(cmd, "@Direccion", DbType.String, direccion);
                db.AddInParameter(cmd, "@Telefono", DbType.String, telefono);
                db.AddInParameter(cmd, "@TelefEmergencia", DbType.String, telefonoemergencia);

                DataSet ds = db.ExecuteDataSet(cmd);
                //0=Ok, <0 = Existe
                int result = Int32.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }

        public static int ActualizarUsuario(PersonaUsuario persona, string dni, int estadocivil, string direccion,
            string telefono, string telefonoemergencia)
        {
            try
            {
                Int64 id = persona.IdUsuario;
                string user = persona.Usuario;
                string pass = persona.Password;
                string nombre = persona.Nombre;
                string apePaterno = persona.ApellidoPaterno;
                string apeMaterno = persona.ApellidoMaterno;
                int idRol = persona.IdRol;
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[WEB].SP_UPDATE_USER");
                db.AddInParameter(cmd, "@IdUser", DbType.Int64, id);
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@Paterno", DbType.String, apePaterno);
                db.AddInParameter(cmd, "@Materno", DbType.String, apeMaterno);
                db.AddInParameter(cmd, "@IdRol", DbType.Int32, idRol);
                db.AddInParameter(cmd, "@Usuario", DbType.String, user);
                db.AddInParameter(cmd, "@Password", DbType.String, pass);
                db.AddInParameter(cmd, "@Dni", DbType.String, dni);
                db.AddInParameter(cmd, "@IdEstadoCivil", DbType.Int32, estadocivil);
                db.AddInParameter(cmd, "@Direccion", DbType.String, direccion);
                db.AddInParameter(cmd, "@Telefono", DbType.String, telefono);
                db.AddInParameter(cmd, "@TelefEmergencia", DbType.String, telefonoemergencia);


                DataSet ds = db.ExecuteDataSet(cmd);
                //0=Ok, <0 = Existe
                int result = Int32.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }

        public static PersonaUsuario ObtenerPersona(string user, string pass) {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].[WEB.SP_LOGIN]");
                db.AddInParameter(cmd, "@USUARIO", DbType.String, user);
                db.AddInParameter(cmd, "@PASSWORD", DbType.String, pass);

                DataSet ds = db.ExecuteDataSet(cmd);
                int rows = (int)ds.Tables[0].Rows[0]["Rows"];
                if (rows > 0)
                {
                    
                var persona = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                List<PersonaUsuario> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PersonaUsuario>>(persona);
                    return p.ElementAt(0);
                }
                else {
                    return null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }      
        }

        public static bool IsNullOrEmpty(string str)
        {
            if (str == null)
                return true;
            if (str.Count() == 0)
                return true;
            return false;
        }

        public static List<Rol> ObtenerRoles()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[WEB].SP_GET_ROLES");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds!=null)
                {

                    var roles = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Rol> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Rol>>(roles);
                    return p;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<PrioridadProyecto> ObtenerPrioridades()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_PRIORIDAD");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var roles = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<PrioridadProyecto> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PrioridadProyecto>>(roles);
                    return p;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static DataTable ObtenerUsuariosDt()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_USERS");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<PersonaUsuario> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PersonaUsuario>>(users);

                   
                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<PersonaUsuario> ObtenerUsuarios()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_USERS");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<PersonaUsuario> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PersonaUsuario>>(users);


                    return (p);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

       

        public static List<Tarea> ObtenerTareas()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_TAREAS");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Tarea> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Tarea>>(users);


                    return (p);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<Area> ObtenerAreas()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_AREAS");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Area> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(users);


                    return (p);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<Empresa> ObtenerEmpresas()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_EMPRESAS");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Empresa> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Empresa>>(users);


                    return (p);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<EstadoCivilUsuario> ObtenerEstadoCivil()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_ESTADO_CIVIL");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<EstadoCivilUsuario> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EstadoCivilUsuario>>(users);


                    return (p);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static DataTable ObtenerTareasDt()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_TAREAS");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Tarea> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Tarea>>(users);


                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static DataTable ObtenerAreasDt()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_AREAS");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Area> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(users);


                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static DataTable ObtenerEmpresasDt()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_EMPRESAS");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {

                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Empresa> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Empresa>>(users);


                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static int CrearProyecto(Int64 idusuario, string nombre, string descripcion, int tarea, int area, int empresa,
            int prioridad, string fechaInicio, string fechaFin)
        {
            try
            {
                DateTime Inicio = DateTime.Parse(fechaInicio);
                DateTime Fin = DateTime.Parse(fechaFin);
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_CREATE_PROYECTO");
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@Descripcion", DbType.String, descripcion);
                db.AddInParameter(cmd, "@IdUsuario", DbType.Int64, idusuario);
                db.AddInParameter(cmd, "@IdTarea", DbType.Int32, tarea);
                db.AddInParameter(cmd, "@IdArea", DbType.Int32, area);
                db.AddInParameter(cmd, "@IdEmpresa", DbType.Int64, empresa);
                db.AddInParameter(cmd, "@IdPrioridad", DbType.Int32, prioridad);
                db.AddInParameter(cmd, "@FecInicio", DbType.DateTime, Inicio);
                db.AddInParameter(cmd, "@FecFin", DbType.DateTime, Fin);
                DataSet ds = db.ExecuteDataSet(cmd);
                //0=Ok, <0 = Existe
                int result = Int32.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }

        public static DataTable ObtenerTrabajosDt()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_TRABAJOS");

                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Trabajo> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Trabajo>>(users);
                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static DataTable ObtenerTrabajosUsuarioDt(Int64 IdUsuario)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_TRABAJO_USUARIO");
                db.AddInParameter(cmd, "@IdUsuario", DbType.String, IdUsuario);
                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Trabajo> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Trabajo>>(users);
                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static DataTable ObtenerTrabajoIdDt(int IdTrabajo)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_TRABAJO_ID");
                db.AddInParameter(cmd, "@IdTrabajo", DbType.Int32, IdTrabajo);
                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Trabajo> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Trabajo>>(users);
                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static Trabajo ObtenerTrabajoId(int IdTrabajo)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_TRABAJO_ID");
                db.AddInParameter(cmd, "@IdTrabajo", DbType.Int32, IdTrabajo);
                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Trabajo> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Trabajo>>(users);
                    return (p.ElementAt(0));
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        #region Proyecto
        public static int EliminarDocumentoProyecto(int idproyecto)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_DELETE_DOCUMENTO_TRABAJO");
                db.AddInParameter(cmd, "@IdTrabajo", DbType.Int32, idproyecto);
                
                DataSet ds = db.ExecuteDataSet(cmd);
                
                return 0;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }

        public static int ActualizarProyecto(ProyectoRequest proyecto)
        {
            try
            {
                int idTrabajo = proyecto.IdTrabajo;
                int idTarea = proyecto.IdTarea;
                int idArea = proyecto.IdArea;
                Int64 idUsuario = proyecto.IdUsuario;
                Int64 idEmpresa = proyecto.IdEmpresa;
                string nombre = proyecto.TrabajoNombre;
                string descripcion = proyecto.TrabajoDescripcion;
                int idPrioridad = proyecto.IdPrioridad;
                DateTime? fechaInicio = proyecto.FechaInicio;
                DateTime? fechaFin = proyecto.FechaFin;
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_UPDATE_PROYECTO");
                db.AddInParameter(cmd, "@IdTrabajo", DbType.Int32, idTrabajo);
                db.AddInParameter(cmd, "@Descripcion", DbType.String, descripcion);
                db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                db.AddInParameter(cmd, "@IdUsuario", DbType.Int64, idUsuario);
                db.AddInParameter(cmd, "@IdTarea", DbType.Int32, idTarea);
                db.AddInParameter(cmd, "@IdArea", DbType.Int32, idArea);
                db.AddInParameter(cmd, "@IdEmpresa", DbType.Int64, idEmpresa);
                db.AddInParameter(cmd, "@IdPrioridad", DbType.Int32, idPrioridad);
                db.AddInParameter(cmd, "@FechaInicio", DbType.DateTime, fechaInicio);
                db.AddInParameter(cmd, "@FechaFin", DbType.DateTime, fechaFin);
                DataSet ds = db.ExecuteDataSet(cmd);
                int result = Int32.Parse(ds.Tables[0].Rows[0]["Result"].ToString());
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }

        public static void DocumentoProyecto(ProyectoRequest proyecto)
        {
            try
            {
                List<DocumentosRequest> list = proyecto.documentos;
                int idTrabajo = proyecto.IdTrabajo;
                foreach (var obj in list) {
                    SqlDatabase db = new SqlDatabase(DB);
                    string nombre = obj.nombreDocumento;
                    string descripcion = obj.descripcionDocumento;
                    string path = obj.nombreArchivo;
                    DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_CREATE_DOCUMENTO");
                    db.AddInParameter(cmd, "@IdTrabajo", DbType.Int32, idTrabajo);
                    db.AddInParameter(cmd, "@Nombre", DbType.String, nombre);
                    db.AddInParameter(cmd, "@Descripcion", DbType.String, descripcion);
                    db.AddInParameter(cmd, "@Path", DbType.String, path);
                    DataSet ds = db.ExecuteDataSet(cmd);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static int EliminarProyecto(int idproyecto)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_DELETE_PROYECTO");
                db.AddInParameter(cmd, "@IdTrabajo", DbType.Int32, idproyecto);

                DataSet ds = db.ExecuteDataSet(cmd);

                return 0;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }

        public static int EliminarUsuario(long idusuario)
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_DELETE_PROYECTO");
                db.AddInParameter(cmd, "@IdTrabajo", DbType.Int32, idusuario);

                DataSet ds = db.ExecuteDataSet(cmd);

                return 0;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 2;
            }
        }


        public static List<TrabajoResponseReporte1> ObtenerTrabajosDocumentosReporte1()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_TRABAJOS");
                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Trabajo> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Trabajo>>(users);
                    List<TrabajoResponseReporte1> list = new List<TrabajoResponseReporte1>();
                    for (int i = 0; i < p.Count; i++)
                    {
                        TrabajoResponseReporte1 response = new TrabajoResponseReporte1();
                        response.IdTrabajo = p.ElementAt(i).IdTrabajo;
                        response.IdUsuario = p.ElementAt(i).IdUsuario;
                        response.IdTarea = p.ElementAt(i).IdTarea;
                        response.IdEmpresa = p.ElementAt(i).IdEmpresa;
                        response.IdArea = p.ElementAt(i).IdArea;
                        response.Tarea = p.ElementAt(i).Tarea;
                        response.Area = p.ElementAt(i).Area;
                        response.Empresa = p.ElementAt(i).Empresa;
                        response.TrabajoNombre = p.ElementAt(i).TrabajoNombre;
                        response.TrabajoDescripcion = p.ElementAt(i).TrabajoDescripcion;
                        response.IdPrioridad = p.ElementAt(i).IdPrioridad;
                        response.Prioridad = p.ElementAt(i).Prioridad;
                        response.FechaRegistro = p.ElementAt(i).FechaRegistro;
                        response.FechaInicio = p.ElementAt(i).FechaInicio;
                        response.FechaFin = p.ElementAt(i).FechaFin;
                        DbCommand cmd1 = db.GetStoredProcCommand("[dbo].SP_GET_DOCUMENTOS_TRABAJO");
                        db.AddInParameter(cmd1, "@IdTrabajo", DbType.Int32, response.IdTrabajo);
                        DataSet ds1 = db.ExecuteDataSet(cmd1);
                        var docs = Newtonsoft.Json.JsonConvert.SerializeObject(ds1.Tables[0]);
                        List<DocumentosResponse> docsRes =
                            Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentosResponse>>(docs);
                        
                        if (docsRes == null || docsRes.Count == 0)
                        {
                            response.DB = 0;/*Cantidad de documentos*/
                            response.DL = 0;/*Cantidad de documentos con Archivos*/
                            response.PL = (0);
                        }
                        else {
                            int countDocs = docsRes.Count;
                            int countDocsArchivos = 0;
                            foreach (DocumentosResponse doc in docsRes)
                            {
                                if (doc.Archivo != null && doc.Archivo != "")
                                    countDocsArchivos++;
                            }
                            response.DB = countDocs;/*Cantidad de documentos*/
                            response.DL = countDocsArchivos;/*Cantidad de documentos con Archivos*/
                            response.PL = (countDocsArchivos / countDocs);
                            response.Documentos = docsRes;
                        }

                        
                        list.Add(response);
                    }
                    return (list);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<TrabajoResponseReporte2> ObtenerTrabajosDocumentosReporte2()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_TRABAJOS");
                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Trabajo> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Trabajo>>(users);
                    List<TrabajoResponseReporte2> list = new List<TrabajoResponseReporte2>();
                    for (int i = 0; i < p.Count; i++)
                    {
                        TrabajoResponseReporte2 response = new TrabajoResponseReporte2();
                        response.IdTrabajo = p.ElementAt(i).IdTrabajo;
                        response.IdUsuario = p.ElementAt(i).IdUsuario;
                        response.IdTarea = p.ElementAt(i).IdTarea;
                        response.IdEmpresa = p.ElementAt(i).IdEmpresa;
                        response.IdArea = p.ElementAt(i).IdArea;
                        response.Tarea = p.ElementAt(i).Tarea;
                        response.Area = p.ElementAt(i).Area;
                        response.Empresa = p.ElementAt(i).Empresa;
                        response.TrabajoNombre = p.ElementAt(i).TrabajoNombre;
                        response.TrabajoDescripcion = p.ElementAt(i).TrabajoDescripcion;
                        response.IdPrioridad = p.ElementAt(i).IdPrioridad;
                        response.Prioridad = p.ElementAt(i).Prioridad;
                        response.FechaRegistro = p.ElementAt(i).FechaRegistro;
                        response.FechaInicio = p.ElementAt(i).FechaInicio;
                        response.FechaFin = p.ElementAt(i).FechaFin;
                        DbCommand cmd1 = db.GetStoredProcCommand("[dbo].SP_GET_DOCUMENTOS_TRABAJO");
                        db.AddInParameter(cmd1, "@IdTrabajo", DbType.Int32, response.IdTrabajo);
                        DataSet ds1 = db.ExecuteDataSet(cmd1);
                        var docs = Newtonsoft.Json.JsonConvert.SerializeObject(ds1.Tables[0]);
                        List<DocumentosResponse> docsRes =
                            Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentosResponse>>(docs);
                        if (docsRes == null || docsRes.Count == 0)
                        {
                            response.UD = 0;/*Cantidad de documentos con descripcion*/
                            response.UT = 0;/*Cantidad de documentos totales*/
                            response.PD = (0);/*UD/UT*/
                        }
                        else {
                            int countDocs = docsRes.Count;
                            int countDocsDescripcion = 0;
                            foreach (DocumentosResponse doc in docsRes)
                            {
                                if (doc.Descripcion != null && doc.Descripcion != "")
                                    countDocsDescripcion++;
                            }
                            response.UD = countDocsDescripcion;/*Cantidad de documentos con descripcion*/
                            response.UT = countDocs;/*Cantidad de documentos totales*/
                            response.PD = (countDocsDescripcion / countDocs);/*UD/UT*/
                            response.Documentos = docsRes;
                        }
                        list.Add(response);
                    }
                    return (list);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<TrabajoResponse> ObtenerTrabajosDocumentos()
        {
            try
            {
                SqlDatabase db = new SqlDatabase(DB);
                DbCommand cmd = db.GetStoredProcCommand("[dbo].SP_GET_TRABAJOS");
                DataSet ds = db.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    var users = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
                    List<Trabajo> p = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Trabajo>>(users);
                    List<TrabajoResponse> list = new List<TrabajoResponse>();
                    for (int i = 0; i < p.Count; i++)
                    {
                        TrabajoResponse response = new TrabajoResponse();
                        response.IdTrabajo = p.ElementAt(i).IdTrabajo;
                        response.IdUsuario = p.ElementAt(i).IdUsuario;
                        response.IdTarea = p.ElementAt(i).IdTarea;
                        response.IdEmpresa = p.ElementAt(i).IdEmpresa;
                        response.IdArea = p.ElementAt(i).IdArea;
                        response.Tarea = p.ElementAt(i).Tarea;
                        response.Area = p.ElementAt(i).Area;
                        response.Empresa = p.ElementAt(i).Empresa;
                        response.TrabajoNombre = p.ElementAt(i).TrabajoNombre;
                        response.TrabajoDescripcion = p.ElementAt(i).TrabajoDescripcion;
                        response.IdPrioridad = p.ElementAt(i).IdPrioridad;
                        response.Prioridad = p.ElementAt(i).Prioridad;
                        response.FechaRegistro = p.ElementAt(i).FechaRegistro;
                        response.FechaInicio = p.ElementAt(i).FechaInicio;
                        response.FechaFin = p.ElementAt(i).FechaFin;
                        DbCommand cmd1 = db.GetStoredProcCommand("[dbo].SP_GET_DOCUMENTOS_TRABAJO");
                        db.AddInParameter(cmd1, "@IdTrabajo", DbType.Int32, response.IdTrabajo);
                        DataSet ds1 = db.ExecuteDataSet(cmd1);
                        var docs = Newtonsoft.Json.JsonConvert.SerializeObject(ds1.Tables[0]);
                        List<DocumentosResponse> docsRes =
                            Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentosResponse>>(docs);
                        int countDocs = docsRes.Count;
                        response.CantidadDocumentos = countDocs;
                        response.Documentos = docsRes;
                        list.Add(response);
                    }
                    return (list);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        #endregion
    }
}