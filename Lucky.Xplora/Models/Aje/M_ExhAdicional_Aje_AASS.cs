﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Models.Aje
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public class AjeExhibicionAdicional : AAjeExhibicionAdicional
    {
        [JsonProperty("_a")]
        public string Valorizado { get; set; }

        [JsonProperty("_b")]
        public string Cantidad { get; set; }

        [JsonProperty("_c")]
        public string TotalPdv { get; set; }

        [JsonProperty("_d")]
        public string PdvElemento { get; set; }

        [JsonProperty("_e")]
        public List<AjeExhibicionAdicionalParticipacionMaterial> ParticipacionMaterial { get; set; }

        [JsonProperty("_f")]
        public List<AjeExhibicionAdicionalParticipacionEvolutivoMes> EvolutivoMes { get; set; }

        [JsonProperty("_g")]
        public List<AjeExhibicionAdicionalTarifario> TarifarioPeriodo { get; set; }

        [JsonProperty("_h")]
        public List<AjeExhibicionAdicionalDetalle> Detalle { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public class AjeExhibicionAdicionalParticipacionMaterial
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string mat_cantidad { get; set; }

        [JsonProperty("_d")]
        public string mat_cantidad_porcentaje { get; set; }

        [JsonProperty("_e")]
        public string mat_valorizado { get; set; }

        [JsonProperty("_f")]
        public string mat_valorizado_porcentaje { get; set; }

        [JsonProperty("_g")]
        public List<AjeExhibicionAdicionalParticipacionMaterialMarca> marca { get; set; }

        [JsonProperty("_h")]
        public List<AjeExhibicionAdicionalParticipacionEvolutivoMes> evolutivo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public class AjeExhibicionAdicionalParticipacionMaterialMarca
    {
        [JsonProperty("_a")]
        public int mar_id { get; set; }

        [JsonProperty("_b")]
        public string mar_descripcion { get; set; }

        [JsonProperty("_c")]
        public string mar_cantidad { get; set; }

        [JsonProperty("_d")]
        public string mar_cantidad_porcentaje { get; set; }

        [JsonProperty("_e")]
        public string mar_valorizado { get; set; }

        [JsonProperty("_f")]
        public string mar_valorizado_porcentaje { get; set; }

        [JsonProperty("_g")]
        public string color { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public class AjeExhibicionAdicionalParticipacionEvolutivoMes
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string mes { get; set; }

        [JsonProperty("_c")]
        public string valorizado { get; set; }

        [JsonProperty("_d")]
        public string cantidad { get; set; }

        [JsonProperty("_e")]
        public string pdv_total { get; set; }

        [JsonProperty("_f")]
        public string pdv_elemento { get; set; }

        [JsonProperty("_g")]
        public string cant_marca { get; set; }
        [JsonProperty("_h")]
        public List<HenkelExhibicionAdicionalvalor> valor { get; set; }
    }
    public class HenkelExhibicionAdicionalvalor
    {
        [JsonProperty("_a")]
        public string cadena { get; set; }
        [JsonProperty("_b")]
        public string valor { get; set; }
    }
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public class AjeExhibicionAdicionalTarifario
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string plazavea_valor { get; set; }

        [JsonProperty("_d")]
        public string tottus_valor { get; set; }

        [JsonProperty("_e")]
        public string metro_valor { get; set; }

        [JsonProperty("_f")]
        public string wong_valor { get; set; }

        [JsonProperty("_g")]
        public string vivanda_valor { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public class AjeExhibicionAdicionalDetalle
    {
        [JsonProperty("_a")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_b")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<AjeExhibicionAdicionalDetalleMaterial> DetalleMaterial { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public class AjeExhibicionAdicionalDetalleMaterial : AAjeExhibicionAdicional
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string foto { get; set; }

        [JsonProperty("_d")]
        public string existe { get; set; }

        [JsonProperty("_e")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_f")]
        public string pdv_descripcion { get; set; }
        
        [JsonProperty("_g")]
        public string cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public abstract class AAjeExhibicionAdicional
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-07-24
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public AjeExhibicionAdicional ExhibicionAdicional(E_Parametros_Aje_AASS oRq)
        {
            return MvcApplication._Deserialize<ResponseAjeExhibicionAdicional>(
                    MvcApplication._Servicio_Operativa.AjeExhibicionAdicional(
                        MvcApplication._Serialize(new RequestAjeExhibicionAdicional() { oParametros = oRq })
                    )
                ).Objeto;
        }

        public AjeExhibicionAdicional AlicorpExhibicionAdicional(E_Parametros_Aje_AASS oRq)
        {
            return MvcApplication._Deserialize<ResponseAjeExhibicionAdicional>(
                    MvcApplication._Servicio_Operativa.NWAlicorpExhibicionAdicional(
                        MvcApplication._Serialize(new RequestAjeExhibicionAdicional() { oParametros = oRq })
                    )
                ).Objeto;
        }
        //viernes 2
        //public List<AjeExhibicionAdicionalDetalleMaterial> Exportar_Detalle_Reporting_Exhibicion_AASS(E_Parametros_Aje_AASS oRq)
        //{
        //    Response_AjeReporting_Detalle_Exhibicion_AASS oRp = MvcApplication._Deserialize<Response_AjeReporting_Detalle_Exhibicion_AASS>(
        //            MvcApplication._Servicio_Operativa.Aje_Exportar_Excel_Detalle_Excibicion(
        //                MvcApplication._Serialize(new RequestAjeExhibicionAdicional() { oParametros = oRq })
        //            )
        //        );
        //    return oRp.Objeto;
        //}

        public List<AjeExhibicionAdicionalParticipacionMaterial> Excel_Exportar_exhibiciones_2(E_Parametros_Aje_AASS request)
        {
            Reponse_Nw_excel_Exportar_exhibiciones oRp = MvcApplication._Deserialize<Reponse_Nw_excel_Exportar_exhibiciones>(
                MvcApplication._Servicio_Operativa.Nw_excel_Exportar_exhibiciones_2(
                MvcApplication._Serialize(request)
                )
               );
            return oRp.Objeto;
        }

    }

    #region Request & Response
    /// <summary>
    /// Autor: Ayala
    /// Fecha: 2016-07-24
    /// </summary>
    public class Response_AjeReporting_Detalle_Exhibicion_AASS
    {
        [JsonProperty("_a")]
        public List<AjeExhibicionAdicionalDetalleMaterial> Objeto { get; set; }
    }
    #endregion
    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public class RequestAjeExhibicionAdicional
    {
        [JsonProperty("a")]
        public E_Parametros_Aje_AASS oParametros {get; set;}
    }


    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-07-24
    /// </summary>
    public class ResponseAjeExhibicionAdicional
    {
        [JsonProperty("_a")]
        public AjeExhibicionAdicional Objeto { get; set; }
    }
    #endregion

    //public class M_ExhAdicional_Aje_AASS : M_ExhAdicional_Aje_AASS_service
    //{
    //}

    //public class M_ExhAdicional_Aje_AASS_service
    //{
    //    public E_ExhAdicional_AASS ExhAdicional_AASS(E_Parametros_Aje_AASS oParametros)
    //    {
    //        Response_ExhAdicional_Aje_AASS oRp;
    //        Request_ExhAdicional_Aje_AASS oRequest = new Request_ExhAdicional_Aje_AASS();
    //        oRequest.parametros = oParametros;
    //        try
    //        {
    //            oRp = MvcApplication._Deserialize<Response_ExhAdicional_Aje_AASS>(
    //                    MvcApplication._Servicio_Operativa.NwRepStd_Consultar_ExhAdicional_AASS(
    //                        MvcApplication._Serialize(oRequest)
    //                    )
    //                );
    //            return oRp.Response;
    //        }catch(Exception ex){
    //            return null;
    //        }
            
    //    }

    //    public E_ExhAdicional_AASS ExhAdicional_Tarifario_AASS(E_Parametros_Aje_AASS oParametros)
    //    {
    //        Response_ExhAd_Tarifario_Aje_AASS oRp;
    //        Request_ExhAdicional_Aje_AASS oRequest = new Request_ExhAdicional_Aje_AASS();
    //        E_ExhAdicional_AASS Objeto = new E_ExhAdicional_AASS();
    //        oRequest.parametros = oParametros;
    //        try
    //        {
    //            oRp = MvcApplication._Deserialize<Response_ExhAd_Tarifario_Aje_AASS>(
    //                    MvcApplication._Servicio_Operativa.NwRepStd_Consultar_ExhAd_Tarifario_AASS(
    //                        MvcApplication._Serialize(oRequest)
    //                    )
    //                );
    //            Objeto.ListTarifarioMaterial = oRp.Response;
    //            return Objeto;
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }

    //    }

    //    public E_ExhAdicional_AASS ExhAdicional_Detalle_AASS(E_Parametros_Aje_AASS oParametros)
    //    {
    //        Response_ExhAd_Detalle_Aje_AASS oRp;
    //        Request_ExhAdicional_Aje_AASS oRequest = new Request_ExhAdicional_Aje_AASS();
    //        oRequest.parametros = oParametros;
    //        try
    //        {
    //            oRp = MvcApplication._Deserialize<Response_ExhAd_Detalle_Aje_AASS>(
    //                    MvcApplication._Servicio_Operativa.NwRepStd_Consultar_ExhAd_Detalle_AASS(
    //                        MvcApplication._Serialize(oRequest)
    //                    )
    //                );
    //            return oRp.Response;
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }

    //    public List<E_ExhAdicional_Evolutivo_AASS> ExhAdicional_Evolutivo_AASS(E_Parametros_Aje_AASS oParametros)
    //    {
    //        Response_ExhAd_Evolutivo_Aje_AASS oRp;
    //        Request_ExhAdicional_Aje_AASS oRequest = new Request_ExhAdicional_Aje_AASS();
    //        oRequest.parametros = oParametros;
    //        try
    //        {
    //            oRp = MvcApplication._Deserialize<Response_ExhAd_Evolutivo_Aje_AASS>(
    //                    MvcApplication._Servicio_Operativa.NwRepStd_Consultar_ExhAd_Evolutivo_AASS(
    //                        MvcApplication._Serialize(oRequest)
    //                    )
    //                );
    //            return oRp.Response;
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }

    //    public List<E_Excel_Exhibicion_AASS> Excel_Exhibicion(E_Parametros_Aje_AASS request)
    //    {
    //        //Response_ExhAd_Excel_Aje_AASS oRp = MvcApplication._Deserialize<Response_ExhAd_Excel_Aje_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Excel_Exhibicion_AASS(MvcApplication._Serialize(request)));

    //        //return oRp.Objeto;
    //        Response_ExhAd_Excel_Aje_AASS oRp;
    //        Request_ExhAdicional_Aje_AASS oRequest = new Request_ExhAdicional_Aje_AASS();
    //        oRequest.parametros = request;
    //        try
    //        {
    //            oRp = MvcApplication._Deserialize<Response_ExhAd_Excel_Aje_AASS>(
    //                    MvcApplication._Servicio_Operativa.NwRepStd_Excel_Exhibicion_AASS(
    //                        MvcApplication._Serialize(oRequest)
    //                    )
    //                );
    //            return oRp.Objeto;
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }
    //}

    public class E_ExhAdicional_Hijo
    {
        [JsonProperty("a")]
        public string Id { get; set; }
        [JsonProperty("b")]
        public string Valor { get; set; }
        [JsonProperty("c")]
        public string Foto { get; set; }
    }

    public class E_Parametros_Aje_AASS
    {
        [JsonProperty("a")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("b")]
        public string Cod_SubCanal { get; set; }
        [JsonProperty("c")]
        public string Cod_Cadena { get; set; }
        [JsonProperty("d")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("e")]
        public string Cod_Empresa { get; set; }
        [JsonProperty("f")]
        public string Cod_Marca { get; set; }
        [JsonProperty("g")]
        public int Periodo { get; set; }
        [JsonProperty("h")]
        public string Cod_Zona { get; set; }
        [JsonProperty("i")]
        public string Cod_Distrito { get; set; }
        [JsonProperty("j")]
        public string Cod_PDV { get; set; }
        [JsonProperty("k")]
        public string segmento { get; set; }
        [JsonProperty("l")]
        public string cod_sku { get; set; }
        [JsonProperty("m")]
        public string Anio { get; set; }
        [JsonProperty("n")]
        public string Mes { get; set; }
        [JsonProperty("o")]
        public int cod_reporte { get; set; }
        [JsonProperty("p")]
        public string Cod_Ubieo { get; set; }
        [JsonProperty("q")]
        public string Cod_Cluster { get; set; }
        [JsonProperty("r")]
        public string Cod_Elemento { get; set; }
        [JsonProperty("s")]
        public string Parametros { get; set; }
        [JsonProperty("t")]
        public int opcion { get; set; }
    }







    public class E_ExhAdicional_AASS
    {
        [JsonProperty("_a")]
        public String ExhValorizado { get; set; }
        [JsonProperty("_b")]
        public String CantElement { get; set; }
        [JsonProperty("_c")]
        public String AlcancePDV { get; set; }
        [JsonProperty("_d")]
        public String pdvElement { get; set; }
        [JsonProperty("_e")]
        public List<E_ExhAdicional_Material_AASS> ListTarifarioMaterial { get; set; }
        [JsonProperty("_f")]
        public List<E_Participacion_ExhAdicional_AASS> ParticipacionExhAd { get; set; }
        [JsonProperty("_g")]
        public String Cod_Material { get; set; }
        [JsonProperty("_h")]
        public String Desc_Material { get; set; }
        [JsonProperty("_i")]
        public List<E_ExhAdicional_Detalle_AASS> ListExhAdDetalle { get; set; }
    }

    public class E_ExhAdicional_Detalle_AASS
    {
        [JsonProperty("_a")]
        public String Cod_PDV { get; set; }
        [JsonProperty("_b")]
        public String PDV_Name { get; set; }
        [JsonProperty("_c")]
        public String Cod_Cadena { get; set; }
        [JsonProperty("_d")]
        public String Desc_Material { get; set; }
        [JsonProperty("_e")]
        public String valor { get; set; }
        [JsonProperty("_f")]
        public int presencia { get; set; }
        [JsonProperty("_g")]
        public String Cod_Material { get; set; }
        [JsonProperty("_h")]
        public List<E_ExhAdicional_Hijo> Hijo { get; set; }
        [JsonProperty("_i")]
        public String Foto { get; set; }
    }

    public class E_Participacion_ExhAdicional_AASS
    {
        [JsonProperty("_a")]
        public String CodMaterial { get; set; }
        [JsonProperty("_b")]
        public String DescMaterial { get; set; }
        [JsonProperty("_c")]
        public String cant_element { get; set; }
        [JsonProperty("_d")]
        public String cant_porc { get; set; }
        [JsonProperty("_e")]
        public String cant_total { get; set; }
        [JsonProperty("_f")]
        public String valor { get; set; }
        [JsonProperty("_g")]
        public String valor_porc { get; set; }
        [JsonProperty("_h")]
        public String valor_total { get; set; }
    }

    public class E_ExhAdicional_Material_AASS
    {
        [JsonProperty("_a")]
        public String CodMaterial { get; set; }
        [JsonProperty("_b")]
        public String DescMaterial { get; set; }
        [JsonProperty("_c")]
        public String TipoMaterial { get; set; }
        [JsonProperty("_d")]
        public String valor { get; set; }
        [JsonProperty("_e")]
        public String cod_cadena { get; set; }
        [JsonProperty("_f")]
        public String cadena { get; set; }
        [JsonProperty("_h")]
        public List<E_ExhAdicional_Hijo> Hijo { get; set; }
    }

    public class Reponse_Nw_excel_Exportar_exhibiciones 
    {
        [JsonProperty("_a")]
        public List<AjeExhibicionAdicionalParticipacionMaterial> Objeto { get; set; }
    }
  
    //public class E_ExhAdicional_Evolutivo_AASS
    //{
    //    [JsonProperty("_a")]
    //    public int Id { get; set; }
    //    [JsonProperty("_b")]
    //    public String Mes { get; set; }
    //    [JsonProperty("_c")]
    //    public String Valor { get; set; }
    //    [JsonProperty("_d")]
    //    public String PDV_Total { get; set; }
    //    [JsonProperty("_e")]
    //    public String PDV_Element { get; set; }
    //    [JsonProperty("_f")]
    //    public String Cant_Marca { get; set; }
    //    [JsonProperty("_g")]
    //    public String Elemento { get; set; }
    //}

    //public class E_Excel_Exhibicion_AASS
    //{
    //    [JsonProperty("_a")]
    //    public String Cadena { get; set; }
    //    [JsonProperty("_b")]
    //    public String Cod_PDV { get; set; }
    //    [JsonProperty("_c")]
    //    public String PDV { get; set; }
    //    [JsonProperty("_d")]
    //    public String Empresa { get; set; }
    //    [JsonProperty("_e")]
    //    public String Presentacion { get; set; }
    //    [JsonProperty("_f")]
    //    public String Categoria { get; set; }
    //    [JsonProperty("_g")]
    //    public String Marca { get; set; }
    //    [JsonProperty("_h")]
    //    public String Cod_Material { get; set; }
    //    [JsonProperty("_i")]
    //    public String Material { get; set; }
    //    [JsonProperty("_j")]
    //    public String P_Regular { get; set; }
    //    [JsonProperty("_k")]
    //    public String Periodo { get; set; }
    //}

    //public class Request_ExhAdicional_Aje_AASS
    //{
    //    [JsonProperty("a")]
    //    public E_Parametros_Aje_AASS parametros { get; set; }
    //}

    //public class Response_ExhAdicional_Aje_AASS
    //{
    //    [JsonProperty("_a")]
    //    public E_ExhAdicional_AASS Response { get; set; }
    //}
    //public class Response_ExhAd_Tarifario_Aje_AASS
    //{
    //    [JsonProperty("_a")]
    //    public List<E_ExhAdicional_Material_AASS> Response { get; set; }
    //}
    //public class Response_ExhAd_Detalle_Aje_AASS
    //{
    //    [JsonProperty("_a")]
    //    public E_ExhAdicional_AASS Response { get; set; }
    //}
    //public class Response_ExhAd_Evolutivo_Aje_AASS
    //{
    //    [JsonProperty("_a")]
    //    public List<E_ExhAdicional_Evolutivo_AASS> Response { get; set; }
    //}
    //public class Response_ExhAd_Excel_Aje_AASS
    //{
    //    [JsonProperty("_a")]
    //    public List<E_Excel_Exhibicion_AASS> Objeto { get; set; }
    //}


 }