﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Reporting;

namespace Lucky.Xplora.Models
{
    public class M_Combo : NW_Combo_service
    {
        [JsonProperty("a")]
        public string Value { get; set; }

        [JsonProperty("b")]
        public string Text { get; set; }
    }

    public abstract class NW_Combo_service
    {
        public List<M_Combo> Listar_combos(Request_NW_Reporting oRq)
        {
            Response_SOD_Filtros oRp;

            oRp = MvcApplication._Deserialize<Response_SOD_Filtros>(
                    MvcApplication._Servicio_Operativa.NW_RP_Consul_Filtros(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
        public List<M_Combo> Filtros_Tablero(Request_BI_Tablero_Rep_Venta oRq)
        {
            Response_SOD_Filtros oRp;

            oRp = MvcApplication._Deserialize<Response_SOD_Filtros>(
                    MvcApplication._Servicio_Operativa.BL_BI_Tablero_Filtros(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public class Response_SOD_Filtros
    {
        [JsonProperty("a")]
        public List<M_Combo> Response { get; set; }
    }
}