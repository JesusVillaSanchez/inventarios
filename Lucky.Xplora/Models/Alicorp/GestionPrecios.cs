﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Alicorp
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-26
    /// </summary>
    public class AlicorpGestionPrecios
    {
        [JsonProperty("_a")]
        public AlicorpGestionPreciosParametro Parametros { get; set; }

        [JsonProperty("_b")]
        public AlicorpGestionPreciosSelecciona Selecciona { get; set; }

        [JsonProperty("_c")]
        public AlicorpGestionPreciosModelo Modelo { get; set; }

        [JsonProperty("_d")]
        public List<AlicorpGestionPreciosRankingFases> Ranking { get; set; }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-26
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<AlicorpGestionPreciosSelecciona> Filtro(AlicorpGestionPreciosParametro p) {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosSelecciona>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-29
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<AlicorpGestionPreciosModelo> Precio(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosModelo>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Precio;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-30
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public int GuardaEnvia(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionGuardaEnvia>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Resultado;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-09
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public AlicorpGestionPreciosPanel Panel(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosPanel>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-12
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public AlicorpGestionPreciosPanelFase FaseStatus(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosPanelFase>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-12
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public AlicorpGestionPreciosPersonaDatos PersonaDatos(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosPersonaDatos>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-14
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<AlicorpGestionPreciosRankingFases> RankingAtrasados(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosRankingFases>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-14
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<AlicorpGestionPreciosVisorStatusNegocio> VisorStatus(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosVisorStatusNegocio>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-14
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public int BulkCopy(List<AlicorpGestionPreciosModelo> p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionGuardaEnvia>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPreciosBulkCopy(MvcApplication._Serialize(p))
                ).Resultado;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-20
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public int Guarda(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionGuardaEnvia>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Resultado;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-03
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<AlicorpGestionPreciosStatus> Status(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosStatus>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-04-19
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<AlicorpGestionPreciosDescuento> Descuentos(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosDescuento>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-04-26
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public string DescargaVariable(AlicorpGestionPreciosParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpGestionPreciosTexto>(
                    MvcApplication._Servicio_Operativa.AlicorpGestionPrecios(MvcApplication._Serialize(p))
                ).Texto;
        }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-26
    /// </summary>
    public class AlicorpGestionPreciosParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }

        [JsonProperty("_c")]
        public string comentario { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-26
    /// </summary>
    public class AlicorpGestionPreciosSelecciona
    {
        [JsonProperty("_a")]
        public string SelCodigo { get; set; }

        [JsonProperty("_b")]
        public string SelDescripcion { get; set; }

        [JsonProperty("_c")]
        public int Seleccionado { get; set; }

        [JsonProperty("_d")]
        public string SelGrupo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-29
    /// </summary>
    public class AlicorpGestionPreciosModelo
    {
        [JsonProperty("_a")]
        public int TrpId { get; set; }

        [JsonProperty("_b")]
        public int RplId { get; set; }

        [JsonProperty("_c")]
        public int OfiId { get; set; }

        [JsonProperty("_d")]
        public int OfiPerId { get; set; }

        [JsonProperty("_e")]
        public int CatId { get; set; }

        [JsonProperty("_f")]
        public string CatDescripcion { get; set; }

        [JsonProperty("_g")]
        public int CatPerId { get; set; }

        [JsonProperty("_h")]
        public int ComId { get; set; }

        [JsonProperty("_i")]
        public string ComDescripcion { get; set; }

        [JsonProperty("_j")]
        public int MarId { get; set; }

        [JsonProperty("_k")]
        public string MarDescripcion { get; set; }

        [JsonProperty("_l")]
        public string ProCodigo { get; set; }

        [JsonProperty("_m")]
        public string ProDescripcion { get; set; }

        [JsonProperty("_n")]
        public float? PrecioMpm { get; set; }

        [JsonProperty("_o")]
        public float? PrecioDexMinorista { get; set; }

        [JsonProperty("_p")]
        public float? PrecioRvtMin { get; set; }

        [JsonProperty("_q")]
        public float? PrecioRvtMay { get; set; }

        [JsonProperty("_r")]
        public string Chat { get; set; }

        [JsonProperty("_s")]
        public int FasId { get; set; }

        [JsonProperty("_t")]
        public int Estado { get; set; }

        [JsonProperty("_u")]
        public string OfiDescripcion { get; set; }

        [JsonProperty("_v")]
        public float? MargenMayorista { get; set; }

        [JsonProperty("_w")]
        public float? PrecioMpmAnterior { get; set; }

        [JsonProperty("_x")]
        public float? PrecioRvtMayAnterior { get; set; }

        [JsonProperty("_y")]
        public string VarPrecioMpm { get; set; }

        [JsonProperty("_z")]
        public string VarPrecioRvtMay { get; set; }

        [JsonProperty("_aa")]
        public string VarPrecioDexMinorista { get; set; }

        [JsonProperty("_ab")]
        public string VarMargenMayorista { get; set; }

        [JsonProperty("_ac")]
        public int GcaId { get; set; }

        [JsonProperty("_ad")]
        public string GcaDescripcion { get; set; }

        [JsonProperty("_ae")]
        public float? PrecioDexMinimayorista { get; set; }

        [JsonProperty("_af")]
        public float? BrechaMinimayorista { get; set; }

        [JsonProperty("_ag")]
        public string VarBrechaMinimayorista { get; set; }

        [JsonProperty("_ah")]
        public float? BrechaMinorista { get; set; }

        [JsonProperty("_ai")]
        public string VarBrechaMinorista { get; set; }

        [JsonProperty("_aj")]
        public float? PrecioPvp { get; set; }

        [JsonProperty("_ak")]
        public float? MargenMinorista { get; set; }

        [JsonProperty("_al")]
        public string VarMargenMinorista { get; set; }

        [JsonProperty("_am")]
        public float? MargenTotalComercio { get; set; }

        [JsonProperty("_an")]
        public string VarMargenTotalComercio { get; set; }

        [JsonProperty("_ao")]
        public float? RpaCosto { get; set; }

        [JsonProperty("_ap")]
        public float? RpaPvp { get; set; }

        [JsonProperty("_aq")]
        public List<AlicorpGestionPreciosModeloChat> ListaChat { get; set; }

        [JsonProperty("_ar")]
        public string Archivo { get; set; }

        [JsonProperty("_as")]
        public string VarPrecioDexMinimayorista { get; set; }

        [JsonProperty("_at")]
        public string VarPrecioPvp { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-06
    /// </summary>
    public class AlicorpGestionPreciosModeloChat
    {
        [JsonProperty("_a")]
        public int Orden { get; set; }

        [JsonProperty("_b")]
        public int RplId { get; set; }

        [JsonProperty("_c")]
        public string RplDescripcion { get; set; }

        [JsonProperty("_d")]
        public string Chat { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-09
    /// </summary>
    public class AlicorpGestionPreciosPanel
    {
        [JsonProperty("_a")]
        public List<AlicorpGestionPreciosPanelCronograma> Cronograma { get; set; }

        [JsonProperty("_b")]
        public AlicorpGestionPreciosPanelFase Fase { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpGestionPreciosFaseInforacion> FaseInformacion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-09
    /// </summary>
    public class AlicorpGestionPreciosPanelCronograma
    {
        [JsonProperty("_a")]
        public int FasId { get; set; }

        [JsonProperty("_b")]
        public string FasDescripcion { get; set; }

        [JsonProperty("_c")]
        public int Estado { get; set; }

        [JsonProperty("_d")]
        public List<AlicorpGestionPreciosPanelCronogramaFecha> Fecha { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-09
    /// </summary>
    public class AlicorpGestionPreciosPanelCronogramaFecha
    {
        [JsonProperty("_a")]
        public DateTime Fecha { get; set; }

        [JsonProperty("_b")]
        public string FechaFormato { get; set; }

        [JsonProperty("_c")]
        public string Color { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-09
    /// </summary>
    public class AlicorpGestionPreciosPanelFase
    {
        [JsonProperty("_a")]
        public List<AlicorpGestionPreciosPanelFaseModelo> Fase1 { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpGestionPreciosPanelFaseModelo> Fase2 { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpGestionPreciosPanelFaseModelo> Fase3 { get; set; }

        [JsonProperty("_d")]
        public List<AlicorpGestionPreciosPanelFaseModelo> Fase4 { get; set; }

        [JsonProperty("_e")]
        public List<AlicorpGestionPreciosPanelFaseModelo> RankingLider { get; set; }

        [JsonProperty("_f")]
        public List<AlicorpGestionPreciosPanelFaseModelo> RankingTrade { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-09
    /// </summary>
    public class AlicorpGestionPreciosPanelFaseModelo
    {
        [JsonProperty("_a")]
        public int FasId { get; set; }

        [JsonProperty("_b")]
        public int OfiId { get; set; }

        [JsonProperty("_c")]
        public string OfiDescripcion { get; set; }

        [JsonProperty("_d")]
        public int CatId { get; set; }

        [JsonProperty("_e")]
        public string CatDescripcion { get; set; }

        [JsonProperty("_f")]
        public int PerId { get; set; }

        [JsonProperty("_g")]
        public string PerNombre { get; set; }

        [JsonProperty("_h")]
        public string PerUsuario { get; set; }

        [JsonProperty("_i")]
        public string Valor { get; set; }

        [JsonProperty("_j")]
        public string Cantidad { get; set; }

        [JsonProperty("_k")]
        public string Dias { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-12
    /// </summary>
    public class AlicorpGestionPreciosPersonaDatos
    {
        [JsonProperty("_a")]
        public int GcaId { get; set; }

        [JsonProperty("_b")]
        public string Gca { get; set; }

        [JsonProperty("_c")]
        public int PerId { get; set; }

        [JsonProperty("_d")]
        public string PerUsuario { get; set; }

        [JsonProperty("_e")]
        public string PerContraseña { get; set; }

        [JsonProperty("_f")]
        public string PerEmail { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-14
    /// </summary>
    public class AlicorpGestionPreciosRankingFases
    {
        [JsonProperty("_a")]
        public int FasId { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpGestionPreciosRankingFasesEquipo> Equipo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-14
    /// </summary>
    public class AlicorpGestionPreciosRankingFasesEquipo
    {
        [JsonProperty("_a")]
        public int OfiId { get; set; }

        [JsonProperty("_b")]
        public string OfiDescripcion { get; set; }

        [JsonProperty("_c")]
        public int CatId { get; set; }

        [JsonProperty("_d")]
        public string CatDescripcion { get; set; }

        [JsonProperty("_e")]
        public int PerId { get; set; }

        [JsonProperty("_f")]
        public string PerNombre { get; set; }

        [JsonProperty("_g")]
        public List<AlicorpGestionPreciosRankingFasesEquipoGrupo> Grupo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-14
    /// </summary>
    public class AlicorpGestionPreciosRankingFasesEquipoGrupo
    {
        [JsonProperty("_a")]
        public string Grupo { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpGestionPreciosRankingFasesEquipoGrupoPeriodo> Periodo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-14
    /// </summary>
    public class AlicorpGestionPreciosRankingFasesEquipoGrupoPeriodo
    {
        [JsonProperty("_a")]
        public int RplOrden { get; set; }

        [JsonProperty("_b")]
        public int RplId { get; set; }

        [JsonProperty("_c")]
        public string RplDescripcion { get; set; }

        [JsonProperty("_d")]
        public string Valor { get; set; }

        [JsonProperty("_e")]
        public int RplPeriodo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-15
    /// </summary>
    public class AlicorpGestionPreciosVisorStatusNegocio
    {
        [JsonProperty("_a")]
        public int GcaId { get; set; }

        [JsonProperty("_b")]
        public string GcaDescripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpGestionPreciosVisorStatusNegocioPeriodo> Periodo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-15
    /// </summary>
    public class AlicorpGestionPreciosVisorStatusNegocioPeriodo
    {
        [JsonProperty("_a")]
        public int RplId { get; set; }

        [JsonProperty("_b")]
        public string RplDescripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpGestionPreciosVisorStatusNegocioPeriodoFase> Fase { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-15
    /// </summary>
    public class AlicorpGestionPreciosVisorStatusNegocioPeriodoFase
    {
        [JsonProperty("_a")]
        public int FasId { get; set; }

        [JsonProperty("_b")]
        public string FasDescripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpGestionPreciosVisorStatusNegocioPeriodoFaseColor> Color { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-15
    /// </summary>
    public class AlicorpGestionPreciosVisorStatusNegocioPeriodoFaseColor
    {
        [JsonProperty("_a")]
        public string Color { get; set; }

        [JsonProperty("_b")]
        public string Cantidad { get; set; }

        [JsonProperty("_c")]
        public string Comentario { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-03-03
    /// </summary>
    public class AlicorpGestionPreciosStatus
    {
        [JsonProperty("_a")]
        public int RplId { get; set; }

        [JsonProperty("_b")]
        public string RplDescripcion { get; set; }

        [JsonProperty("_c")]
        public int GcaId { get; set; }

        [JsonProperty("_d")]
        public int FasId { get; set; }

        [JsonProperty("_e")]
        public string FasDescripcion { get; set; }

        [JsonProperty("_f")]
        public int OfiId { get; set; }

        [JsonProperty("_g")]
        public string OfiDescripcion { get; set; }

        [JsonProperty("_h")]
        public int CatId { get; set; }

        [JsonProperty("_i")]
        public string CatDescripcion { get; set; }

        [JsonProperty("_j")]
        public int PerId { get; set; }

        [JsonProperty("_k")]
        public string PerNombre { get; set; }

        [JsonProperty("_l")]
        public string PerUsuario { get; set; }

        [JsonProperty("_m")]
        public string Valor { get; set; }

        [JsonProperty("_n")]
        public string Cantidad { get; set; }

        [JsonProperty("_o")]
        public string Dias { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-04-19
    /// </summary>
    public class AlicorpGestionPreciosDescuento
    {
        [JsonProperty("_a")]
        public int TrpId { get; set; }

        [JsonProperty("_b")]
        public float? PrecioMpm { get; set; }

        [JsonProperty("_c")]
        public float? DescuentoContado { get; set; }

        [JsonProperty("_d")]
        public float? DescuentoLinealidad { get; set; }

        [JsonProperty("_e")]
        public float? DescuentoRebate { get; set; }

        [JsonProperty("_f")]
        public float? DescuentoBonificacion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-04-20
    /// </summary>
    public class AlicorpGestionPreciosFaseInforacion
    {
        [JsonProperty("_a")]
        public int RplId { get; set; }

        [JsonProperty("_b")]
        public int GcaId { get; set; }

        [JsonProperty("_c")]
        public int FasId { get; set; }

        [JsonProperty("_d")]
        public string FasDescripcion { get; set; }

        [JsonProperty("_e")]
        public string FechaInicio { get; set; }

        [JsonProperty("_f")]
        public string FechaFin { get; set; }

        [JsonProperty("_g")]
        public int Estado { get; set; }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-26
    /// </summary>
    public class ResponseAlicorpGestionPreciosSelecciona
    {
        [JsonProperty("_a")]
        public List<AlicorpGestionPreciosSelecciona> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-29
    /// </summary>
    public class ResponseAlicorpGestionPreciosModelo
    {
        [JsonProperty("_a")]
        public List<AlicorpGestionPreciosModelo> Precio { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-30
    /// </summary>
    public class ResponseAlicorpGestionGuardaEnvia
    {
        [JsonProperty("_a")]
        public int Resultado { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-30
    /// </summary>
    public class ResponseAlicorpGestionPreciosPanel
    {
        [JsonProperty("_a")]
        public AlicorpGestionPreciosPanel Objeto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-12
    /// </summary>
    public class ResponseAlicorpGestionPreciosPanelFase
    {
        [JsonProperty("_a")]
        public AlicorpGestionPreciosPanelFase Objeto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-12
    /// </summary>
    public class ResponseAlicorpGestionPreciosPersonaDatos
    {
        [JsonProperty("_a")]
        public AlicorpGestionPreciosPersonaDatos Objeto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-14
    /// </summary>
    public class ResponseAlicorpGestionPreciosRankingFases
    {
        [JsonProperty("_a")]
        public List<AlicorpGestionPreciosRankingFases> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-14
    /// </summary>
    public class ResponseAlicorpGestionPreciosVisorStatusNegocio
    {
        [JsonProperty("_a")]
        public List<AlicorpGestionPreciosVisorStatusNegocio> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-03-03
    /// </summary>
    public class ResponseAlicorpGestionPreciosStatus
    {
        [JsonProperty("_a")]
        public List<AlicorpGestionPreciosStatus> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-04-19
    /// </summary>
    public class ResponseAlicorpGestionPreciosDescuento
    {
        [JsonProperty("_a")]
        public List<AlicorpGestionPreciosDescuento> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-04-26
    /// </summary>
    public class ResponseAlicorpGestionPreciosTexto
    {
        [JsonProperty("_a")]
        public string Texto { get; set; }
    }
    #endregion
}