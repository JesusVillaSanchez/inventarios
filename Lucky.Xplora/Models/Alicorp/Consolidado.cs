﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Alicorp
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-01-08
    /// </summary>
    public class FiltrosConsolidadoAlicorpMayorista {
        [JsonProperty("__a")]
        public string fecha_inicio { get; set; }

        [JsonProperty("__b")]
        public string fecha_fin { get; set; }

        [JsonProperty("__c")]
        public int oficina { get; set; }

        [JsonProperty("__d")]
        public string departamento { get; set; }

        [JsonProperty("__e")]
        public int nodocomercial { get; set; }

        [JsonProperty("__f")]
        public string pdv { get; set; }

        [JsonProperty("__g")]
        public int categoria { get; set; }

        [JsonProperty("__h")]
        public int marca { get; set; }

        [JsonProperty("__i")]
        public int supervisor { get; set; }

        [JsonProperty("__j")]
        public int gie { get; set; }

        [JsonProperty("__k")]
        public int estado { get; set; }
    }
}