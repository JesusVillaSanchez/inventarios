﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;



namespace Lucky.Xplora.Models.Alicorp.AASS
{
    #region Precio
    public class Precio : APrecio 
    {

    }

    public abstract class APrecio
    {
        /// <summary>
        /// autor:dsanchez
        /// fecha:2016-09-15
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<AlicorpAASSReportingLista> Lista(AlicorpAASSReportingParametro parametro)
        {
            return MvcApplication._Deserialize<ResponseAlicorpAASSReportingLista>(
                    MvcApplication._Servicio_Operativa.AlicorpAASSReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }
        public List<AlicorpAASSReportingPrecioCompania>ListaCompania(AlicorpAASSReportingParametro parametro) {
            return MvcApplication._Deserialize<ResponseAlicorpAASSPrecioReportingCompania>(
                    MvcApplication._Servicio_Operativa.AlicorpAASSReporting(
                        MvcApplication._Serialize(parametro)
                    )

                ).ListaCompania;
        }


    }
    /// <summary>
    /// autor:dsanchez
    /// fecha:2016-09-12
    /// </summary>
    public class AlicorpAASSReportingParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }
        [JsonProperty("_b")]
        public string parametro { get; set; }
    }

    /// <summary>
    /// autor:dsanchez
    /// fecha:2016-09-13
    /// </summary>
    public class AlicorpAASSReportingLista
    {
        [JsonProperty("_a")]
        public string codigo { get; set; }

        [JsonProperty("_b")]
        public string valor { get; set; }
    }

    /// <summary>
    /// autor:dsanchez 
    /// fecha:29/09/2016
    /// </summary>
    public class AlicorpAASSReportingPrecioCompania
    {   [JsonProperty("_a")]
        public int com_id { get; set; }

        [JsonProperty("_b")]
        public string com_descripcion { get; set;}

        [JsonProperty("_c")]
        public List<AlicorpAASSReportingPrecioCompaniaCategoria> categoria { get; set; }
    }

    /// <summary>
    /// autor:dsanchez
    /// fecha:29/09/2016
    /// </summary>
    public class AlicorpAASSReportingPrecioCompaniaCategoria
    {   [JsonProperty("_a")]
        public int cat_id { get; set;}
        
        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }
        
        [JsonProperty("_c")]
        public List<AlicorpAASSReportingPrecioCompaniaCategoriaMarca> marca { get; set; }
    }

    /// <summary>
    /// autor:dsanchez
    /// fecha:29/09/2016
    /// </summary>
    public class AlicorpAASSReportingPrecioCompaniaCategoriaMarca
    {   [JsonProperty("_a")]
        public int mar_id { get; set;}

        [JsonProperty("_b")]
        public string mar_descripcion { get; set;}

        [JsonProperty("_c")]
        public List<AlicorpAASSReportingPrecioCompaniaCategoriaMarcaProducto> producto { get; set; }
    }

    /// <summary>
    /// autor:dsanchez
    /// fecha:29/09/2016
    /// </summary>
    public class AlicorpAASSReportingPrecioCompaniaCategoriaMarcaProducto
    {   [JsonProperty("_a")]
        public string pro_sku { get; set;}
        
        [JsonProperty("_b")]
        public string pro_descripcion { get; set; }
        
        [JsonProperty("_c")]
        public List<AlicorpAASSReportingPrecioCompaniaCategoriaMarcaProductoPeriodo> periodo { get; set; }
    }

    /// <summary>
    /// autor:dsanchez
    /// fecha:29/09/2016
    /// </summary>
    public class AlicorpAASSReportingPrecioCompaniaCategoriaMarcaProductoPeriodo
    {  
        [JsonProperty("_a")]
        public int rpl_id { get; set; }
        
        [JsonProperty("_b")]
        public string rpl_descripcion { get; set; }
        
        [JsonProperty("_c")]
        public List<AlicorpAASSReportingPrecioCompaniaCategoriaMarcaProductoPeriodoCadena> cadena { get; set; }
    }

    /// <summary>
    /// autor:dsanchez
    /// fecha:29/09/2016
    /// </summary>
    public class AlicorpAASSReportingPrecioCompaniaCategoriaMarcaProductoPeriodoCadena
    {
        [JsonProperty("_a")]
        public int cad_id { get; set; }
        
        [JsonProperty("_b")]
        public string cad_descripcion { get; set; }
        
        [JsonProperty("_c")]
        public string precio_regular { get; set; }
        
        [JsonProperty("_d")]
        public string precio_oferta { get; set; }
        
        [JsonProperty("_e")]
        public string margen { get; set; }
    }

    /// <summary>
    /// autor:dsanchez
    /// fecha:2016-09-14
    /// </summary>
    public class ResponseAlicorpAASSReportingLista
    {
        [JsonProperty("_a")]
        public List<AlicorpAASSReportingLista> Lista { get; set; }
    }

    /// <summary>
    /// autor:dsanchez
    /// fecha:29/09/2016
    /// </summary>
    public class ResponseAlicorpAASSPrecioReportingCompania{
        
        [JsonProperty("_a")]
        public List<AlicorpAASSReportingPrecioCompania> ListaCompania{get;set;}
    }

    #endregion

    #region OSA

    /// <summary>
    /// autor:wlopez
    /// fecha:30/09/2016
    /// </summary>
    /// <param name="parametro"></param>
    /// <returns></returns>
    public class OSA : AOSA
    {

    }

    /// <summary>
    /// autor:wlopez
    /// fecha:30/09/2016
    /// </summary>
    /// <param name="parametro"></param>
    /// <returns></returns>
    public abstract class AOSA
    {
        public List<AlicorpAASSReportingLista> Lista(AlicorpAASSReportingParametro parametro)
        {
            return MvcApplication._Deserialize<ResponseAlicorpAASSReportingLista>(
                    MvcApplication._Servicio_Operativa.AlicorpAASSReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }
    }
    #endregion
}