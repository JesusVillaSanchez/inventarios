﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Alicorp
{
    public class AlicorpInteligenciaComercial : AReporteIntelComercial
    {
        [JsonProperty("_a")]
        public int id_rcompe { get; set; }

        [JsonProperty("_b")]
        public string id_planning { get; set; }

        [JsonProperty("_c")]
        public int company_id { get; set; }

        [JsonProperty("_d")]
        public string pdv { get; set; }

        [JsonProperty("_e")]
        public string fec_reg_cel { get; set; }

        [JsonProperty("_f")]
        public string fec_reg_bd { get; set; }

        [JsonProperty("_g")]
        public string product_category { get; set; }

        [JsonProperty("_h")]
        public string oficina { get; set; }

        [JsonProperty("_i")]
        public string cod_canal { get; set; }

        [JsonProperty("_j")]
        public string mercado { get; set; }

        [JsonProperty("_k")]
        public string empresa { get; set; }

        [JsonProperty("_l")]
        public string marca { get; set; }

        [JsonProperty("_m")]
        public string actividad { get; set; }

        [JsonProperty("_n")]
        public string observacion { get; set; }

        [JsonProperty("_o")]
        public string nombre_foto { get; set; }

        [JsonProperty("_p")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_q")]
        public string can_descripcion { get; set; }

        [JsonProperty("_r")]
        public string empresa_foto { get; set; }

        [JsonProperty("_s")]
        public string marca_foto { get; set; }

        [JsonProperty("_z")]
        public bool valida { get; set; }

        [JsonProperty("_t")]
        public string descripcion_actividad { get; set; }

        [JsonProperty("_u")]
        public string precio_competencia { get; set; }

        [JsonProperty("_v")]
        public string precio_alicorp { get; set; }

        [JsonProperty("_w")]
        public string distrito { get; set; }
        [JsonProperty("_x")]
        public E_AlicorpIntelComercial_detalle Detalle { get; set; }
        [JsonProperty("_aa")]
        public string cod_oficina { get; set; }
    }
    public class E_AlicorpIntelComercial_detalle
    {
        [JsonProperty("_a")]
        public int Cod_empresa { get; set; }
        [JsonProperty("_b")]
        public int Cod_marca { get; set; }
        [JsonProperty("_c")]
        public string Cod_actividad { get; set; }
        [JsonProperty("_d")]
        public string Cod_distritito { get; set; }
        [JsonProperty("_e")]
        public string Sku_compt { get; set; }
        [JsonProperty("_f")]
        public string PVP_compt { get; set; }
        [JsonProperty("_g")]
        public string PR_compt { get; set; }
        [JsonProperty("_h")]
        public string PC_compt { get; set; }
        [JsonProperty("_i")]
        public string MPM_compt { get; set; }
        [JsonProperty("_j")]
        public string SKU_alicorp { get; set; }
        [JsonProperty("_k")]
        public string PVP_alicorp { get; set; }
        [JsonProperty("_l")]
        public string PR_alicorp { get; set; }
        [JsonProperty("_m")]
        public string PC_alicorp { get; set; }
        [JsonProperty("_n")]
        public string MPM_alicorp { get; set; }
        [JsonProperty("_o")]
        public string Cod_Mercado { get; set; }
    }

    public class AlicorpIntelComercialParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }

    public abstract class AReporteIntelComercial
    {

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public int Mensaje(Request_Alicorp_Inteligencia_Mensaje oRq)
        {
            Response_Alicorp_Inteligencia_Actualiza oRp;

            oRp = MvcApplication._Deserialize<Response_Alicorp_Inteligencia_Actualiza>(
                    MvcApplication._Servicio_Operativa.Alicorp_Inteligencia_Mensaje(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Respuesta;
        }

        public List<M_Combo> GetFiltros(Request_Alicorp_InteligenciaComer_Filtro oRq)
        {
            Response_Alicorp_InteligenciaComer_Filtro oRp;
            oRp = MvcApplication._Deserialize<Response_Alicorp_InteligenciaComer_Filtro>(
                MvcApplication._Servicio_Operativa.Alicorp_InteligenciaComer_Filtro(MvcApplication._Serialize(oRq)));
            return oRp.Obj;
        }

        public Object Proyecto5_Mantenimiento(Request_Proyecto5_Parametros oRq)
        {
            Response_Proyecto5_Mantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_Proyecto5_Mantenimiento>(
                MvcApplication._Servicio_Operativa.Proyecto5_Mantenimiento(MvcApplication._Serialize(oRq)));

            return oRp.objeto;

        }

    }

    #region Request & Response

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 06-04-2015
    /// Descripcion: Reporte Inteligencia Comercial - Alicorp
    /// </summary>
    public class Request_ReporteAlicorpInteligenciaComercial
    {
        [JsonProperty("_a")]
        public int compania { get; set; }

        [JsonProperty("_b")]
        public int categoria { get; set; }

        [JsonProperty("_c")]
        public int canal { get; set; }

        [JsonProperty("_d")]
        public int periodo { get; set; }

        [JsonProperty("_e")]
        public int status { get; set; }
    }
    public class Response_ReporteAlicorpInteligenciaComercial
    {
        [JsonProperty("_a")]
        public List<AlicorpInteligenciaComercial> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-08
    /// </summary>
    public class Request_Alicorp_Inteligencia_Actualiza
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string oficina { get; set; }

        [JsonProperty("_c")]
        public string mercado { get; set; }

        [JsonProperty("_d")]
        public string empresa { get; set; }

        [JsonProperty("_e")]
        public string marca { get; set; }

        [JsonProperty("_f")]
        public string actividad { get; set; }

        [JsonProperty("_g")]
        public int valida { get; set; }

        [JsonProperty("_h")]
        public string foto { get; set; }

        [JsonProperty("_i")]
        public int opcion { get; set; }

        [JsonProperty("_j")]
        public string comentario { get; set; }
    }
    public class Response_Alicorp_Inteligencia_Actualiza
    {
        [JsonProperty("_a")]
        public int Respuesta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-08
    /// </summary>
    public class Request_Alicorp_Inteligencia_Mensaje
    {
        [JsonProperty("_a")]
        public List<AlicorpInteligenciaComercial> Lista { get; set; }
    }
    //Filtro
    public class Request_Alicorp_InteligenciaComer_Filtro
    {
        [JsonProperty("_a")]
        public int op { get; set; }
        [JsonProperty("_b")]
        public string parametros { get; set; }
    }
    public class Response_Alicorp_InteligenciaComer_Filtro
    {
        [JsonProperty("_a")]
        public List<M_Combo> Obj { get; set; }
    }

    public class Request_AlicorpIntelComercialParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }

    public class Response_AlicorpIntelComercialMoficar
    {
        [JsonProperty("_a")]
        public int Respuesta { get; set; }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-04-26
    /// </summary>
    public class Request_Proyecto5_Parametros
    {
        [JsonProperty("_a")]
        public int Opcion { get; set; }
        [JsonProperty("_b")]
        public string Parametros { get; set; }
    }
    public class Response_Proyecto5_Mantenimiento
    {
        [JsonProperty("_a")]
        public Object objeto { get; set; }
    }

    #endregion
}