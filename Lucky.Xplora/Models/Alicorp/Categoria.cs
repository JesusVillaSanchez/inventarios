﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Alicorp
{

    public class Categoria: ACategoria
    {
        [JsonProperty("a")]
        public int cat_id { get; set; }

        [JsonProperty("b")]
        public string cat_descripcion { get; set; }
    }


    public abstract class ACategoria {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 16-03-2015
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        //public List<Categoria> Lista(Request_Lista_Categorias oRq) {
        //    Response_Lista_Categorias oRp;

        //    oRp = MvcApplication._Deserialize<Response_Lista_Categorias>(
        //            MvcApplication._Servicio_Campania.Bl_Lista_Categorias(
        //                MvcApplication._Serialize(oRq)
        //            )
        //        );

        //    return oRp.Lista;
        //}
    }

    #region Request & Response

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 16-03-2015
    /// </summary>
    public class Request_Lista_Categorias
    {
        [JsonProperty("_a")]
        public int compania { get; set; }
    }
    public class Response_Lista_Categorias
    {
        [JsonProperty("_a")]
        public List<Categoria> Lista { get; set; }
    }

    #endregion
}
