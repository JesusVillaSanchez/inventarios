﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Alicorp
{
    public class ReporteSKUFamilias : AReporteSKUFamilias
    {
        [JsonProperty("_zz")]
        public int corre { get; set; }
        [JsonProperty("_a")]
        public int oro_id { get; set; }
        [JsonProperty("_b")]
        public int ord_id { get; set; }
        [JsonProperty("_c")]
        public string ofi_descripcion { get; set; }
        [JsonProperty("_d")]
        public string pdv_codigo { get; set; }
        [JsonProperty("_e")]
        public string pdv_razonsocial { get; set; }
        [JsonProperty("_f")]
        public string cat_descripcion { get; set; }
        [JsonProperty("_g")]
        public string mar_descripcion { get; set; }
        [JsonProperty("_h")]
        public string fam_descripcion { get; set; }
        [JsonProperty("_i")]
        public string stock_fin_ton { get; set; }
        [JsonProperty("_j")]
        public string prom_Ventas { get; set; }
        [JsonProperty("_k")]
        public string dias_giro { get; set; }
        [JsonProperty("_l")]
        public string obs_descripcion { get; set; }
        [JsonProperty("_m")]
        public bool Validado { get; set; }
        [JsonProperty("_n")]
        public bool validCliente { get; set; }
        [JsonProperty("_o")]
        public string obs_id { get; set; }

        [JsonProperty("_p")]
        public string sell_out { get; set; }
        [JsonProperty("_q")]
        public string cant_dias { get; set; }
        [JsonProperty("_r")]
        public string sell_in { get; set; }
        [JsonProperty("_s")]
        public string gie_dsc { get; set; }
        [JsonProperty("_t")]
        public string stock_inicial { get; set; }
        [JsonProperty("_u")]
        public string stock_final { get; set; }
   }

    public class Pla_MObservacion
    {
        [JsonProperty("a")]
        public string pmo_id { get; set; }

        [JsonProperty("b")]
        public string pmo_descripcion { get; set; }
    }

    public abstract class AReporteSKUFamilias {
        public List<ReporteSKUFamilias> Lista(Request_ReporteSKUFamilias oRq) {
            Response_ReporteSKUFamilias oRp;

            oRp = MvcApplication._Deserialize<Response_ReporteSKUFamilias>(
                    MvcApplication._Servicio_Operativa.Alicorp_Mayorista_SKU_Familia(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }

        public List<ReporteSKUFamilias> ListaMarca(Request_ReporteSKUFamilias oRq)
        {
            Response_ReporteSKUFamilias oRp;

            oRp = MvcApplication._Deserialize<Response_ReporteSKUFamilias>(
                    MvcApplication._Servicio_Operativa.Alicorp_Mayorista_Marca(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }


        public List<ReporteSKUFamilias> ListaCategoria(Request_ReporteSKUFamilias oRq)
        {
            Response_ReporteSKUFamilias oRp;

            oRp = MvcApplication._Deserialize<Response_ReporteSKUFamilias>(
                    MvcApplication._Servicio_Operativa.Alicorp_Mayorista_Categoria(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }

        public List<ReporteSKUFamilias> ListaVistaAnalista(Request_ReporteSKUFamilias oRq)
        {
            Response_ReporteSKUFamilias oRp;

            oRp = MvcApplication._Deserialize<Response_ReporteSKUFamilias>(
                    MvcApplication._Servicio_Operativa.Alicorp_Mayorista_Vista_Analista(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }


        public int ResponseSkuFamilia(Valida_Request oRq) {
            Valida_Response oRp;

            oRp = MvcApplication._Deserialize<Valida_Response>(
                    MvcApplication._Servicio_Operativa.Alicorp_Mayorista_Analista_ActualizaCheck(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Respuesta;
        }


        public int ResponseAnalista(Valida_Request oRq)
        {
            Valida_Response oRp;

            oRp = MvcApplication._Deserialize<Valida_Response>(
                    MvcApplication._Servicio_Operativa.Alicorp_Mayorista_Cliente_ActualizaCheck(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Respuesta;
        }


        public int ResponseUpdateObs(Alicorp_Mayor_Actualiza_Request oRq)
        {
            Alicorp_Mayor_Actualiza_Response oRp;

            oRp = MvcApplication._Deserialize<Alicorp_Mayor_Actualiza_Response>(
                    MvcApplication._Servicio_Operativa.Alicorp_Mayorista_SKU_Familia_Actualiza(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.iRespuesta;
        }

    }
 

    #region Request & Response

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 16-03-2015
    /// </summary>
    public class Request_ReporteSKUFamilias
    {
        [JsonProperty("_a")]
        public int anio { get; set; }
        [JsonProperty("_b")]
        public int mes { get; set; }
        [JsonProperty("_c")]
        public int oficina { get; set; }
        [JsonProperty("_d")]
        public string categoria { get; set; }
        [JsonProperty("_e")]
        public int periodo { get; set; }
    }
    public class Response_ReporteSKUFamilias 
    {
        [JsonProperty("_a")]
        public List<ReporteSKUFamilias> Lista { get; set; }
    }

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 18-03-2015
    /// Combo observación
    /// </summary>
    public class Pla_MObservacion_Request
    {
        [JsonProperty("a")]
        public string planning { get; set; }

        [JsonProperty("b")]
        public int report { get; set; }
    }

    public class Pla_MObservacion_Response
    {
        [JsonProperty("a")]
        public List<Pla_MObservacion> Lista { get; set; }
    }


    /// <summary>
    /// Fecha: 18-03-2015
    /// Creado: rcontreras
    /// </summary>
    /// 
    public class Alicorp_Mayor_Actualiza_Request
    {

        [JsonProperty("_a")]
        public int id_cab { get; set; }

        [JsonProperty("_b")]
        public string id_obs { get; set; }

        [JsonProperty("_c")]
        public string desc_obs { get; set; }

        [JsonProperty("_d")]
        public bool vali { get; set; }

        [JsonProperty("_e")]
        public string user_modif { get; set; }

        [JsonProperty("_f")]
        public DateTime date_modif { get; set; }

        [JsonProperty("_x")]
        public string valida { get; set; }

        [JsonProperty("_y")]
        public string invalida { get; set; }


    }
    public class Alicorp_Mayor_Actualiza_Response
    {
        [JsonProperty("a")]
        public int iRespuesta { get; set; }
    }



    public class Valida_Request
    {
        [JsonProperty("a")]
        public string valida { get; set; }

        [JsonProperty("b")]
        public string invalida { get; set; }

        [JsonProperty("c")]
        public string usuario { get; set; }
    }
    public class Valida_Response
    {
        [JsonProperty("a")]
        public int Respuesta { get; set; }
    }


    #endregion
}