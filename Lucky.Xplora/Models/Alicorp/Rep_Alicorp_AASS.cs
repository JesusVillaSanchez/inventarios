﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models.NwRepStdAASS;

namespace Lucky.Xplora.Models.Alicorp
{
    public class Rep_Alicorp_AASS
    {
        public E_Precio_AASS Consulta_Reporte(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Precio oRp = MvcApplication._Deserialize<Response_NWRepStd_Precio>(MvcApplication._Servicio_Operativa.NwRepAlicorp_Consultar_PreciosAASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<E_Excel_Precio_AASS> Excel_Precio(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Excel_Precio oRp = MvcApplication._Deserialize
           <Response_NWRepStd_Excel_Precio>(MvcApplication._Servicio_Operativa.NwRepStd_Excel_Precios_Alicorp_AASS
                (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }

        public List<E_Excel_Precio_AASS> Excel_Quiebrealicorp(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Excel_Precio oRp = MvcApplication._Deserialize
           <Response_NWRepStd_Excel_Precio>(MvcApplication._Servicio_Operativa.NwRepStd_Excel_Quiebre_Alicorp_AASS
                (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }

        public List<E_Osa_Alicorp_AASS_Cadena> Consulta_OSA(Request_NWRepStd_General request)
        {
            Response_NWRepAlicorp_OSA_AASS oRp = MvcApplication._Deserialize
           <Response_NWRepAlicorp_OSA_AASS>(MvcApplication._Servicio_Operativa.NwRepAlicorp_Consultar_OSAAASS
                (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public E_new_Spot_Alicorp_AASS Consulta_SPOT(Request_NWRepStd_General request)
        {
            Response_NWRepAlicorp_SPOT_AASS oRp = MvcApplication._Deserialize
           <Response_NWRepAlicorp_SPOT_AASS>(MvcApplication._Servicio_Operativa.NwRepAlicorp_Consultar_SPOT_AASS
                (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<M_Combo> Consulta_Filtro(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Filtro oRp = MvcApplication._Deserialize
           <Response_NWRepStd_Filtro>(MvcApplication._Servicio_Operativa.NwRepAlicorp_Filtro_AASS
                (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<E_Spot_Alicorp_AASS_Excel> Excel_SPOT(Request_NWRepStd_General request)
        {
            Response_NWRepAlicorp_Excel_SPOT_AASS oRp = MvcApplication._Deserialize
           <Response_NWRepAlicorp_Excel_SPOT_AASS>(MvcApplication._Servicio_Operativa.NwRepAlicorp_Excel_SPOT_AASS
                (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<E_Osa_Alicorp_AASS_Cuasal> Causal_OSA(Request_NWRepStd_General request)
        {
            Response_NWRepAlicorp_OSA_Causal_AASS oRp = MvcApplication._Deserialize
           <Response_NWRepAlicorp_OSA_Causal_AASS>(MvcApplication._Servicio_Operativa.NwRepAlicorp_Causal_OSAAASS
                (MvcApplication._Serialize(request)));
            return oRp.Objeto;
        }
        public List<E_Osa_Alicorp_AASS_Cuasal_OOS> Causal_OSA_OOS(Request_NWRepStd_General request)
        {
            Response_NWRepAlicorp_OSA_Causal_OOS_AASS oRp = MvcApplication._Deserialize
           <Response_NWRepAlicorp_OSA_Causal_OOS_AASS>(MvcApplication._Servicio_Operativa.NwRepAlicorp_Causal_OOS_OSAAASS
                (MvcApplication._Serialize(request)));
            return oRp.Objeto;
        }
    }

    #region Request - Response
    public class Response_NWRepAlicorp_OSA_AASS
    {
        [JsonProperty("_a")]
        public List<E_Osa_Alicorp_AASS_Cadena> Objeto { get; set; }
    }
    public class Response_NWRepAlicorp_SPOT_AASS 
    {
        [JsonProperty("_a")]
        public E_new_Spot_Alicorp_AASS Objeto { get; set; }
    }
    public class Response_NWRepAlicorp_Excel_SPOT_AASS
    {
        [JsonProperty("_a")]
        public List<E_Spot_Alicorp_AASS_Excel> Objeto { get; set; }
    }
    public class Response_NWRepAlicorp_OSA_Causal_AASS
    {
        [JsonProperty("_a")]
        public List<E_Osa_Alicorp_AASS_Cuasal> Objeto { get; set; }
    }
    public class Response_NWRepAlicorp_OSA_Causal_OOS_AASS
    {             
        [JsonProperty("_a")]
        public List<E_Osa_Alicorp_AASS_Cuasal_OOS> Objeto { get; set; }
    }
    #endregion

    #region Entidades
    public class E_Osa_Alicorp_AASS_Cadena
    {
        [JsonProperty("_a")]
        public int cod_cadena { get; set; }
        [JsonProperty("_b")]
        public E_Osa_Alicorp_AASS_grafico grafico1 { get; set; }
        [JsonProperty("_c")]
        public E_Osa_Alicorp_AASS_grafico grafico2 { get; set; }
        [JsonProperty("_d")]
        public E_Osa_Alicorp_AASS_grafico grafico3 { get; set; }
        [JsonProperty("_e")]
        public E_Osa_Alicorp_AASS_grafico grafico4 { get; set; }
        [JsonProperty("_f")]
        public List<E_Osa_Alicor_AASS_Grafico_General> grafico5 { get; set; }
    }
    public class E_Osa_Alicor_AASS_Grafico_General
    {
        [JsonProperty("_a")]
        public String Cadena { get; set; }
        [JsonProperty("_b")]
        public String Oos { get; set; }
        [JsonProperty("_c")]
        public String Void { get; set; }
        [JsonProperty("_d")]
        public String Osa { get; set; }

    }
    public class E_Osa_Alicorp_AASS_grafico
    {
        [JsonProperty("_a")]
        public List<String> categorias { get; set; }
        [JsonProperty("_b")]
        public List<String> OSA { get; set; }
        [JsonProperty("_c")]
        public List<String> VOID { get; set; }
        [JsonProperty("_d")]
        public List<String> OOS { get; set; }
        [JsonProperty("_e")]
        public List<String> ConM { get; set; }
        [JsonProperty("_f")]
        public List<String> SinM { get; set; }
        [JsonProperty("_g")]
        public List<E_Osa_Alicorp_AASS_grafico_anios> Anios { get; set; }

    }
    public class E_Osa_Alicorp_AASS_grafico_anios
    {
        [JsonProperty("_a")]
        public String nombre { get; set; }
        [JsonProperty("_b")]
        public List<String> valores { get; set; }
    }
    public class E_new_Spot_Alicorp_AASS
    {
        [JsonProperty("_a")]
        public List<E_Spot_Alicorp_AASS_gr1> grafico1 { get; set; }
        [JsonProperty("_b")]
        public List<E_Spot_Alicorp_AASS_gr2> grafico2 { get; set; }
        [JsonProperty("_c")]
        public List<E_Spot_Alicorp_AASS_gr2> grafico3 { get; set; }
    }
    public class E_Spot_Alicorp_AASS_gr1
    {
        [JsonProperty("_a")]
        public int cod_empresa { get; set; }
        [JsonProperty("_b")]
        public string nom_empresa { get; set; }
        [JsonProperty("_c")]
        public int cantidad { get; set; }
        [JsonProperty("_d")]
        public int total { get; set; }
        [JsonProperty("_e")]
        public string porcentaje { get; set; }
        [JsonProperty("_f")]
        public string color { get; set; }
    }
    public class E_Spot_Alicorp_AASS_gr2
    {
        [JsonProperty("_a")]
        public int actividad { get; set; }
        [JsonProperty("_b")]
        public List<String> categorias { get; set; }
        [JsonProperty("_c")]
        public List<E_Spot_Alicorp_AASS_serie> valores { get; set; }
    }
    public class E_Spot_Alicorp_AASS_serie
    {
        [JsonProperty("_a")]
        public string name { get; set; }
        [JsonProperty("_b")]
        public List<String> data { get; set; }
        [JsonProperty("_c")]
        public string color { get; set; }
    }
    public class E_Spot_Alicorp_AASS_Excel
    {
        [JsonProperty("_a")]
        public int anio { get; set; }
        [JsonProperty("_b")]
        public string mes { get; set; }
        [JsonProperty("_c")]
        public string semana { get; set; }
        [JsonProperty("_d")]
        public string cadena { get; set; }
        [JsonProperty("_e")]
        public string empresa { get; set; }
        [JsonProperty("_f")]
        public string categoria { get; set; }
        [JsonProperty("_g")]
        public string familia { get; set; }
        [JsonProperty("_h")]
        public string formato { get; set; }
        [JsonProperty("_i")]
        public string descuento { get; set; }
        [JsonProperty("_j")]
        public string precio_normal { get; set; }
        [JsonProperty("_k")]
        public string precio_oferta { get; set; }
    }
    public class E_Osa_Alicorp_AASS_Cuasal
    {
        [JsonProperty("_a")]
        public int semana { get; set; }
        [JsonProperty("_b")]
        public int Total { get; set; }
        [JsonProperty("_c")]
        public String causal_1 { get; set; }
        [JsonProperty("_d")]
        public String causal_2 { get; set; }
        [JsonProperty("_e")]
        public String causal_3 { get; set; }
        [JsonProperty("_f")]
        public String causal_total { get; set; }
    }
    public class E_Osa_Alicorp_AASS_Cuasal_OOS
    {
        [JsonProperty("_a")]
        public String Causal { get; set; }
        [JsonProperty("_b")]
        public String Valor { get; set; }
        [JsonProperty("_c")]
        public String Tipo { get; set; }
        [JsonProperty("_d")]
        public String Color { get; set; }
    }
    #endregion

    #region Carga de informacion
    /// <summary>
    /// Autor: pabrigo
    /// Fecha: 2017-01-20
    /// </summary>
    public class AlicorpAASSCargaEEAA
    {
        [JsonProperty("_a")]
        public string PdvCodigo { get; set; }

        [JsonProperty("_b")]
        public int CatId { get; set; }

        [JsonProperty("_c")]
        public int MarId { get; set; }

        [JsonProperty("_d")]
        public DateTime Fecha { get; set; }

        [JsonProperty("_e")]
        public string TmaCodigo { get; set; }

        [JsonProperty("_f")]
        public int MatId { get; set; }

        [JsonProperty("_g")]
        public int Cantidad { get; set; }

        [JsonProperty("_h")]
        public string Archivo { get; set; }

        [JsonProperty("_i")]
        public int UsuId { get; set; }

        [JsonProperty("_j")]
        public string UsuUsuario { get; set; }
    }

    /// <summary>
    /// Autor: pabrigo
    /// Fecha: 2017-01-20
    /// </summary>
    public class AlicorpAASSCargaOSA
    {

        [JsonProperty("_a")]
        public string cod_equipo { get; set; }

        [JsonProperty("_b")]
        public int cod_oficina { get; set; }

        [JsonProperty("_c")]
        public DateTime fecha { get; set; }

        [JsonProperty("_d")]
        public int anio { get; set; }

        [JsonProperty("_e")]
        public int mes { get; set; }

        [JsonProperty("_f")]
        public int semana { get; set; }

        [JsonProperty("_g")]
        public int cadena { get; set; }

        [JsonProperty("_h")]
        public string cod_categoria { get; set; }

        // [JsonProperty("_i")]
        //public int cod_marca { get; set; }

        [JsonProperty("_j")]
        public string Tiporesultado { get; set; }

        [JsonProperty("_k")]
        public string Causal { get; set; }

        [JsonProperty("_l")]
        public string cluster { get; set; }

        [JsonProperty("_m")]
        public string medicion { get; set; }

        [JsonProperty("_n")]
        public int estado { get; set; }

        [JsonProperty("_o")]
        public string marca { get; set; }

        [JsonProperty("_p")]
        public string archivo { get; set; }

        [JsonProperty("_q")]
        public int UsuId { get; set; }

        [JsonProperty("_r")]
        public string UsuUsuario { get; set; }
   
    }

    /// <summary>
    /// Autor: pabrigo
    /// Fecha: 2017-01-20
    /// </summary>
    public class AlicorpAASSCargaSPOT
    {

        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public int mes { get; set; }

        [JsonProperty("_c")]
        public int semana { get; set; }

        [JsonProperty("_d")]
        public int periodo { get; set; }

        [JsonProperty("_e")]
        public int cod_cadena { get; set; }

        [JsonProperty("_f")]
        public int cod_empresa { get; set; }

        [JsonProperty("_g")]
        public string nom_empresa { get; set; }

        [JsonProperty("_h")]
        public string cod_categoria { get; set; }

        [JsonProperty("_i")]
        public string familia { get; set; }

        [JsonProperty("_j")]
        public string precio_normal { get; set; }

        [JsonProperty("_k")]
        public string precio_oferta { get; set; } 

        [JsonProperty("_l")]
        public string formato_promo { get; set; }

        [JsonProperty("_m")]
        public string vs { get; set; }

        [JsonProperty("_n")]
        public int cod_formato { get; set; }

        [JsonProperty("_o")]
        public string medicion { get; set; }

        [JsonProperty("_p")]
        public string Archivo { get; set; }

        [JsonProperty("_q")]
        public int UsuId { get; set; }

        [JsonProperty("_r")]
        public string UsuUsuario { get; set; }

    }

    /// <summary>
    /// Autor: pabrigo
    /// Fecha: 2017-01-25
    /// </summary>
    public class AlicorpAASSCargaEEAA_Modulo {
        /// <summary>
        /// Autor: pabrigo
        /// Fecha: 2017-01-24
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public AlicorpAASSCargaRespuesta  Bulk(List<AlicorpAASSCargaEEAA> parametro)
        {
            return MvcApplication._Deserialize<ResponseAlicorpAASSCargaEEAA>(
                        MvcApplication._Servicio_Operativa.AlicorpAASSCargaEEAABulk(MvcApplication._Serialize(parametro))
                    ).objeto;
        }

        /// <summary>
        /// Autor: pabrigo
        /// Fecha: 2017-01-24
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public AlicorpAASSCargaRespuesta Bulkosa(List<AlicorpAASSCargaOSA> parametro)
        {
            return MvcApplication._Deserialize<ResponseAlicorpAASSCargaOSA>(
                        MvcApplication._Servicio_Operativa.AlicorpAASSCargaOSABulk(MvcApplication._Serialize(parametro))
                    ).objeto;
        }

        /// <summary>
        /// Autor: pabrigo
        /// Fecha: 2017-01-24
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public AlicorpAASSCargaRespuesta Bulkspot(List<AlicorpAASSCargaSPOT> parametro)
        {
            return MvcApplication._Deserialize<ResponseAlicorpAASSCargaSPOT>(
                        MvcApplication._Servicio_Operativa.AlicorpAASSCargaSPOTBulk(MvcApplication._Serialize(parametro))
                    ).objeto;
        }
    }

    #region Request&Response
    /// <summary>
    /// Autor: pabrigo
    /// Fecha: 2017-01-25
    /// </summary>
    public class ResponseAlicorpAASSCargaOSA
    {
        [JsonProperty("_a")]
        public AlicorpAASSCargaRespuesta objeto { get; set; }
    }

    /// <summary>
    /// Autor: pabrigo
    /// Fecha: 2017-01-25
    /// </summary>
    public class ResponseAlicorpAASSCargaEEAA
    {
        [JsonProperty("_a")]
        public AlicorpAASSCargaRespuesta objeto { get; set; }
    }

    /// <summary>
    /// Autor: pabrigo
    /// Fecha: 2017-01-25
    /// </summary>
    public class ResponseAlicorpAASSCargaSPOT
    {
        [JsonProperty("_a")]
        public AlicorpAASSCargaRespuesta objeto { get; set; }
    }

    public class AlicorpAASSCargaRespuesta
    {
        [JsonProperty("_a")]
        public int codigo { get; set; }
        [JsonProperty("_b")]
        public string descripcion { get; set; }
        [JsonProperty("_c")]
        public List<AlicorpAASSCargaRespuestaDet> Resultado { get; set; }
        [JsonProperty("_d")]
        public int Reporte { get; set; }
        [JsonProperty("_e")]
        public string Nom_Reporte { get; set; }

    }

    public class AlicorpAASSCargaRespuestaDet
    {
        [JsonProperty("_a")]
        public string Desripcion { get; set; }
        [JsonProperty("_b")]
        public string Cantidad { get; set; }

    }


    #endregion
    #endregion
}