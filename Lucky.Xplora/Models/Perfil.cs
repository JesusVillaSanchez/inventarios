﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Perfil : APerfil
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string per_codigo { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string per_descripcion { get; set; }
    }

    public abstract class APerfil {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Perfil> Lista(MapMultiGPS_Request oRq)
        {
            List<Perfil> oLs = new List<Perfil>();
            MapMultiGPS_Filtro_Response oRp;

            oRp = MvcApplication._Deserialize<MapMultiGPS_Filtro_Response>(
                    MvcApplication._Servicio_Maps.Consul_Filtro_MultiGPS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            /*obtiene resultado y lo llena en listado de campanias*/
            if (oRp.Lista != null && oRp.Lista.Count > 0)
            {
                foreach (M_Combo oBj in oRp.Lista)
                {
                    oLs.Add(new Perfil()
                    {
                        per_codigo = oBj.Value,
                        per_descripcion = oBj.Text
                    });
                }
            }

            return oLs;
        }

        /// <summary>
        /// Autor: gruiz
        /// Fecha: 2016-02-16
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Perfil> Lista(Request_GetListTipoPerfil oRq)
        {
            Response_GetListTipoPerfil oRp = MvcApplication._Deserialize<Response_GetListTipoPerfil>(
                    MvcApplication._Servicio_Campania.GetListTipoPerfil_Company(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }

    }

    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-02-16
    /// </summary>
    public class Request_GetListTipoPerfil
    {
        [JsonProperty("_a")]
        public int company { get; set; }
    }

    public class Response_GetListTipoPerfil 
    {
        [JsonProperty("_a")]
        public List<Perfil> Lista { get; set; }
    }
}