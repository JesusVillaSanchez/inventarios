﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Publicacion
{
    public class Encarte : AEncarte
    {
        [JsonProperty("_a")]
        public int ctl_id { get; set; }

        [JsonProperty("_b")]
        public string nco_nombre { get; set; }

        [JsonProperty("_c")]
        public string ctl_vigencia { get; set; }

        [JsonProperty("_d")]
        public string mes_descripcion { get; set; }

        [JsonProperty("_e")]
        public string img_catalogo { get; set; }

        [JsonProperty("_f")]
        public List<Pagina> pagina { get; set; }

        [JsonProperty("_g")]
        public string ctl_descripcion { get; set; }

        [JsonProperty("_h")]
        public string cod_catalogo { get; set;}
    }

    public class Pagina
    {
        [JsonProperty("_a")]
        public int ctp_orden { get; set; }

        [JsonProperty("_b")]
        public string img_ruta { get; set; }
    }

    public abstract class AEncarte {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-31
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Encarte> Lista(Request_Encarte oRq)
        {
            Response_Encarte oRp;

            oRp = MvcApplication._Deserialize<Response_Encarte>(
                MvcApplication._Servicio_Operativa.Encarte(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }


        public int UpdateCatalogo(UpdateCatalago_Request a)
        {
            var oRp = new CL_UpdateCatalogo_Response();

            oRp = MvcApplication._Deserialize<CL_UpdateCatalogo_Response>(
                MvcApplication._Servicio_Operativa.Serv_UpdateCatalogo(MvcApplication._Serialize(a))
            );

            return oRp.resultado;
            //return 0;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-05
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public int Inserta_Catalogo(Request_Inserta_Catalogo oRq) {
            Response_Inserta_Catalogo oRp;

            oRp = MvcApplication._Deserialize<Response_Inserta_Catalogo>(
                MvcApplication._Servicio_Operativa.Inserta_Catalogo(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.cantidad;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-05
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public int Inserta_Pagina(Request_Inserta_Pagina oRq)
        {
            Response_Inserta_Catalogo oRp;

            oRp = MvcApplication._Deserialize<Response_Inserta_Catalogo>(
                MvcApplication._Servicio_Operativa.Inserta_Pagina(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.cantidad;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-03-31
    /// </summary>
    public class Request_Encarte
    {
        [JsonProperty("_a")]
        public int compania { get; set; }

        [JsonProperty("_b")]
        public string campania { get; set; }

        [JsonProperty("_c")]
        public int tipo_canal { get; set; }

        [JsonProperty("_d")]
        public int cadena { get; set; }

        [JsonProperty("_e")]
        public int anio { get; set; }

        [JsonProperty("_f")]
        public int mes { get; set; }
    }
    public class Response_Encarte
    {
        [JsonProperty("_a")]
        public List<Encarte> Lista { get; set; }
    }

    public class UpdateCatalago_Request
    {
        [JsonProperty("a")]
        public string id_catalogo { get; set; }
    }

    public class CL_UpdateCatalogo_Response
    {
        [JsonProperty("a")]
        public int resultado { get; set; }
    }


    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-03
    /// </summary>
    public class Request_Inserta_Catalogo
    {
        [JsonProperty("_a")]
        public string catalogo_nombre { get; set; }

        [JsonProperty("_b")]
        public string catalogo_imagen { get; set; }

        [JsonProperty("_c")]
        public int tipo_canal { get; set; }

        [JsonProperty("_d")]
        public int cadena { get; set; }

        [JsonProperty("_e")]
        public int anio { get; set; }

        [JsonProperty("_f")]
        public int mes { get; set; }

        [JsonProperty("_g")]
        public string catalogo_direccion { get; set; }

        [JsonProperty("_h")]
        public string campania { get; set; }
    }
    public class Response_Inserta_Catalogo
    {
        [JsonProperty("_a")]
        public Int32 cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-03
    /// </summary>
    public class Request_Inserta_Pagina
    {
        [JsonProperty("_a")]
        public int catalogo_id { get; set; }

        [JsonProperty("_b")]
        public int pagina_orden { get; set; }

        [JsonProperty("_c")]
        public string pagina_imagen { get; set; }
    }

    #endregion
}