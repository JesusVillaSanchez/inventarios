﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Mes : AMes
    {
        [JsonProperty("a")]
        public int mes_id { get; set; }

        [JsonProperty("b")]
        public string mes_descripcion { get; set; }
    }

    public abstract class AMes {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-27
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Mes> Lista(Request_GetMes_Campania_Mes oRq) {
            Response_GetMes_Campania_Mes oRp;

            oRp = MvcApplication._Deserialize<Response_GetMes_Campania_Mes>(
                MvcApplication._Servicio_Campania.GetMes_Campania_Mes(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: 
        /// Fecha:
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Mes> Lista(M_Request_Mes_Por_Planning_Reports_Anio oRq)
        {
            M_Response_Mes_Por_Planning_Reports_Anio oRp;

            oRp = MvcApplication._Deserialize<M_Response_Mes_Por_Planning_Reports_Anio>(
                MvcApplication._Servicio_Campania.Mes_Por_Planning_Reports_Anio(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-10
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Mes> Lista(Request_GetMes_Campania_Reporte_Anio_Formato_TipoPerfil oRq)
        {
            Response_GetMes_Campania_Reporte_Anio_Formato_TipoPerfil oRp;

            oRp = MvcApplication._Deserialize<Response_GetMes_Campania_Reporte_Anio_Formato_TipoPerfil>(
                MvcApplication._Servicio_Campania.GetMes_Campania_Reporte_Anio_Formato_TipoPerfil(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
        

        /// <summary>
        /// Autor: gruiz
        /// Fecha: 2016-02-15
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Mes> Lista(Request_GetMes_por_anio oRq)
        {
            Response_GetMes_por_anio oRp;

            oRp = MvcApplication._Deserialize<Response_GetMes_por_anio>(
                MvcApplication._Servicio_Campania.GetMes_x_anio(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Fecha: 2015-02-27
    /// Autor: jlucero
    /// </summary>
    public class Request_GetMes_Campania_Mes
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_c")]
        public int anio { get; set; }
    }
    public class Response_GetMes_Campania_Mes
    {
        [JsonProperty("_a")]
        public List<Mes> Lista { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class M_Request_Mes_Por_Planning_Reports_Anio
    {
        [JsonProperty("a")]
        public string id_planning { get; set; }

        [JsonProperty("b")]
        public int id_report { get; set; }

        [JsonProperty("c")]
        public int anio { get; set; }
    }
    public class M_Response_Mes_Por_Planning_Reports_Anio
    {
        [JsonProperty("a")]
        public List<Mes> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-10
    /// </summary>
    public class Request_GetMes_Campania_Reporte_Anio_Formato_TipoPerfil
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int reporte { get; set; }

        [JsonProperty("_c")]
        public int anio { get; set; }

        [JsonProperty("_d")]
        public int formato { get; set; }

        [JsonProperty("_e")]
        public int tipo_perfil { get; set; }
    }
    public class Response_GetMes_Campania_Reporte_Anio_Formato_TipoPerfil
    {
        [JsonProperty("_a")]
        public List<Mes> Lista { get; set; }
    }



    /// <summary>
    /// Fecha: 2016-02-15
    /// Autor: gruiz
    /// </summary>
    public class Request_GetMes_por_anio
    {
        [JsonProperty("_a")]
        public int anio { get; set; }
    }
    public class Response_GetMes_por_anio
    {
        [JsonProperty("_a")]
        public List<Mes> Lista { get; set; }
    }

    #endregion
}