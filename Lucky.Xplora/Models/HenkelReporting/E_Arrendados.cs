﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.HenkelReporting
{
    public class E_Arrendados : AE_Arrendados
    {
        [JsonProperty("_a")]
        public int periodo { get; set; }

        [JsonProperty("_b")]
        public string nom_periodo { get; set; }

        [JsonProperty("_c")]
        public int oficina { get; set; }

        [JsonProperty("_d")]
        public string nom_oficina { get; set; }

        [JsonProperty("_e")]
        public int cantarren { get; set; }

        [JsonProperty("_f")]
        public int cant { get; set; }

        [JsonProperty("_g")]
        public string porc_arrend { get; set; }
    }


    public abstract class AE_Arrendados
    {
        public List<E_Arrendados> Lista(Request_ElemVisbArrendados oRq)
        {
            Response_ElemVisbArrendados oRp = MvcApplication._Deserialize<Response_ElemVisbArrendados>(
                                   MvcApplication._Servicio_Operativa.GetElemVisbArrendados(
                                           MvcApplication._Serialize(oRq))

                   );

            return oRp.lista;
        }
    }

    #region << Request & Response
    public class Request_ElemVisbArrendados
    {
        [JsonProperty("_a")]
        public string id_planning { get; set; }

        [JsonProperty("_b")]
        public int periodo { get; set; }

        [JsonProperty("_c")]
        public int categoria { get; set; }

        [JsonProperty("_d")]
        public int cadena { get; set; }

        [JsonProperty("_e")]
        public int agruptipomat { get; set; }

        [JsonProperty("_f")]
        public int agrupmat { get; set; }
    }
    public class Response_ElemVisbArrendados
    {
        [JsonProperty("_a")]
        public List<E_Arrendados> lista { get; set; }

    }
    #endregion
}