﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.HenkelReporting
{
    public class E_AgrupTipoMat : AE_AgrupTipoMat
    {
        [JsonProperty("_a")]
        public int cod_agruptipomat { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }
    


    }


    public abstract class AE_AgrupTipoMat
    {
        public List<E_AgrupTipoMat> Lista(Request_AgrupTipoMat oRq)
        {
            Response_AgrupTipoMat oRp = MvcApplication._Deserialize<Response_AgrupTipoMat>(
                                   MvcApplication._Servicio_Operativa.GetAgrupTipoMat(
                                           MvcApplication._Serialize(oRq))

                   );

            return oRp.lista;
        }
   
    }

    #region Request & Response
    public class Request_AgrupTipoMat
    {
         [JsonProperty("_a")]
        public string id_planning { get; set; }
    }
    public class Response_AgrupTipoMat
    {
        [JsonProperty("_a")]
        public List<E_AgrupTipoMat> lista { get; set; }
    }
    #endregion
}