﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.HenkelReporting
{
    public class E_SOS
    {
        public List<Henkel_AASS_SOS_Resumen> SOS_Resumen(Request_Henkel_SOS oRq)
        {
            Response_Henkel_SOS_Resumen oRp;

            oRp = MvcApplication._Deserialize<Response_Henkel_SOS_Resumen>(MvcApplication._Servicio_Operativa.HenkelAASS_SOS_Resumen(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<Henkel_AASS_SOS_Evolutivo> SOS_Evolutivo(Request_Henkel_SOS oRq)
        {
            Response_Henkel_SOS_Evolutivo oRp;

            oRp = MvcApplication._Deserialize<Response_Henkel_SOS_Evolutivo>(MvcApplication._Servicio_Operativa.HenkelAASS_SOS_Evolutivo(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<Henkel_AASS_SOS_Evolutivo_Marca> SOS_Evolutivo_Marca(Request_Henkel_SOS oRq)
        {
            Response_Henkel_SOS_Evolutivo_Marca oRp;

            oRp = MvcApplication._Deserialize<Response_Henkel_SOS_Evolutivo_Marca>(MvcApplication._Servicio_Operativa.HenkelAASS_SOS_Evolutivo_Marca(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public Henkel_AASS_SOS_BD SOS_BD(Request_Henkel_SOS oRq)
        {
            Response_Henkel_SOS_BD oRp;

            oRp = MvcApplication._Deserialize<Response_Henkel_SOS_BD>(MvcApplication._Servicio_Operativa.HenkelAASS_SOS_BD(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
    }


    #region <<< Entidades >>>
    public class Henkel_AASS_SOS_Resumen
    {
        [JsonProperty("_a")]
        public int Cod_Grafico { get; set; }
        [JsonProperty("_b")]
        public int Cod_Cadena { get; set; }
        [JsonProperty("_c")]
        public string Nom_Cadena { get; set; }
        [JsonProperty("_d")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("_e")]
        public string Nom_Empresa { get; set; }
        [JsonProperty("_f")]
        public string Valor { get; set; }
    }
    public class Henkel_AASS_SOS_Evolutivo
    {
        [JsonProperty("_a")]
        public int Cod_Mes { get; set; }
        [JsonProperty("_b")]
        public string Nom_Mes { get; set; }
        [JsonProperty("_c")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("_d")]
        public string Nom_Empresa { get; set; }
        [JsonProperty("_e")]
        public string Valor { get; set; }
    }
    public class Henkel_AASS_SOS_Evolutivo_Marca
    {
        [JsonProperty("_a")]
        public int Cod_Mes { get; set; }
        [JsonProperty("_b")]
        public string Nom_Mes { get; set; }
        [JsonProperty("_c")]
        public int Cod_Marca { get; set; }
        [JsonProperty("_d")]
        public string Nom_Marca { get; set; }
        [JsonProperty("_e")]
        public string Valor { get; set; }
    }
    public class Henkel_AASS_SOS_BD
    {
        [JsonProperty("_a")]
        public List<string> Empresas { get; set; }
        [JsonProperty("_b")]
        public List<Henkel_AASS_SOS_table> tabla { get; set; }
    }
    public class Henkel_AASS_SOS_table
    {
        [JsonProperty("_a")]
        public string Cod_PDV { get; set; }
        [JsonProperty("_b")]
        public string Nom_PDV { get; set; }
        [JsonProperty("_c")]
        public int Cod_Oficina { get; set; }
        [JsonProperty("_d")]
        public string Nom_Oficina { get; set; }
        [JsonProperty("_e")]
        public string Negocio { get; set; }
        [JsonProperty("_f")]
        public List<string> valores { get; set; }
    }

    #endregion
    #region Request - Response
    public class Request_Henkel_SOS
    {
        [JsonProperty("_a")]
        public int Cod_Perfil { get; set; }
        [JsonProperty("_b")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("_c")]
        public int Cod_Periodo { get; set; }
        [JsonProperty("_d")]
        public int Cod_Categoria { get; set; }
        [JsonProperty("_e")]
        public int Cod_Cadena { get; set; }
        [JsonProperty("_f")]
        public int Lectura { get; set; }
        [JsonProperty("_g")]
        public int Cod_Oficina { get; set; }
        [JsonProperty("_h")]
        public int Cod_Anio { get; set; }
        [JsonProperty("_i")]
        public int Cod_Mes { get; set; }
    }
    public class Response_Henkel_SOS_Resumen
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_SOS_Resumen> Response { get; set; }
    }
    public class Response_Henkel_SOS_Evolutivo
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_SOS_Evolutivo> Response { get; set; }
    }
    public class Response_Henkel_SOS_Evolutivo_Marca
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_SOS_Evolutivo_Marca> Response { get; set; }
    }
    public class Response_Henkel_SOS_BD
    {
        [JsonProperty("_a")]
        public Henkel_AASS_SOS_BD Response { get; set; }
    }
    
    #endregion
}