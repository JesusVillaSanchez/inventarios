﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.HenkelReporting
{
    public class E_DetalleEfectividad
    {
        public DetalleEfectividad DetailEffectiveness(Request_GetEfectividad oRq)
        {
            Response_GetEfectividad oRp;

            oRp = MvcApplication._Deserialize<Response_GetEfectividad>(MvcApplication._Servicio_Operativa.GetEfectividad_Arrendados(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
    }


    #region Entidad
    public class DetalleEfectividad
    {
        [JsonProperty("_a")]
        public List<string> Cabecera { get; set; }

        [JsonProperty("_b")]
        public List<GrillaDetalleEfectividad> grilla { get; set; }

    }

    public class GrillaDetalleEfectividad
    {
        [JsonProperty("_a")]
        public int cod_material { get; set; }

        [JsonProperty("_b")]
        public string nom_material { get; set; }

        [JsonProperty("_c")]
        public string obj { get; set; }

        [JsonProperty("_d")]
        public string real { get; set; }

        [JsonProperty("_e")]
        public string efectividad { get; set; }

        [JsonProperty("_f")]
        public List<string> valores { get; set; }
    }

    #endregion


    #region Request & Response
    public class Request_GetEfectividad
    {

        [JsonProperty("_a")]
        public string id_planning { get; set; }

        [JsonProperty("_b")]
        public int periodo { get; set; }

        [JsonProperty("_c")]
        public int categoria { get; set; }

        [JsonProperty("_d")]
        public int agruptipomat { get; set; }

    }
    public class Response_GetEfectividad
    {
        [JsonProperty("_a")]
        public DetalleEfectividad Response { get; set; }
    }

    #endregion

}