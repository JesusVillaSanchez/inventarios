﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.HenkelReporting
{
    #region << ElementosResumen & Total>>
    
    public class BI_ElementosVisb : ABI_ElementosVisb
    {
        [JsonProperty("_a")]
        public string cod_grafico { get; set; }

        [JsonProperty("_b")]
        public int company_id { get; set; }

        [JsonProperty("_c")]
        public string company_desc { get; set; }

        [JsonProperty("_d")]
        public int nodocomercial_id { get; set; }

        [JsonProperty("_e")]
        public string nodocomercial_desc { get; set; }

        [JsonProperty("_f")]
        public string valor { get; set; }

        [JsonProperty("_g")]
        public int total { get; set; }


        [JsonProperty("_h")]
        public int mes_id { get; set; }

        [JsonProperty("_i")]
        public string mes_desc { get; set; }

        [JsonProperty("_j")]
        public int semana { get; set; }


    }


    public abstract class ABI_ElementosVisb
    {

        public List<BI_ElementosVisb> Lista(Request_GetElementosVisbResum oRq)
        {

            Response_GetElementosVisbResum oRp = MvcApplication._Deserialize<Response_GetElementosVisbResum>(
                                   MvcApplication._Servicio_Operativa.GetElmVisbResum(
                                           MvcApplication._Serialize(oRq))

                   );

            return oRp.lista;
        }
        public List<BI_ElementosVisb> ListaTotal(Request_GetElementosVisbResumTotal oRqt)
        {
            Response_GetElementosVisbResumTotal oRpt = MvcApplication._Deserialize<Response_GetElementosVisbResumTotal>(
                                  MvcApplication._Servicio_Operativa.GetElmVisbResumTotal(
                                          MvcApplication._Serialize(oRqt))

                  );

            return oRpt.lista;

        }
        public List<BI_ElementosVisb> ListaEvol(Request_GetElementosVisbEvolutivoCiuCat oRq)
        {
            Response_GetElementosVisbEvolutivoCiuCat oRpt = MvcApplication._Deserialize<Response_GetElementosVisbEvolutivoCiuCat>(
                                             MvcApplication._Servicio_Operativa.GetElemVisbEvolutivoCiuCat(
                                                     MvcApplication._Serialize(oRq))

                             );

            return oRpt.listaEvol;

        }
        public List<BI_ElementosVisb> ListaCompa(Request_GetElementosVisbComparativo oRq)
        {
            Response_GetElementosVisbComparativo oRpt = MvcApplication._Deserialize<Response_GetElementosVisbComparativo>(
                                                        MvcApplication._Servicio_Operativa.GetElemVisbComparativo(
                                                                MvcApplication._Serialize(oRq))

                                        );

            return oRpt.listaCompa;
        }
    }

    #region Request & Response
    public class Request_GetElementosVisbResum
    {
        [JsonProperty("_a")]
        public string id_planning { get; set; }

        [JsonProperty("_b")]
        public int periodo { get; set; }

        [JsonProperty("_c")]
        public int ciudad { get; set; }

        [JsonProperty("_d")]
        public int categoria { get; set; }

        [JsonProperty("_e")]
        public int agruptipomat { get; set; }

        [JsonProperty("_f")]
        public int lectura { get; set; }
    }
    public class Response_GetElementosVisbResum
    {
         [JsonProperty("_a")]
        public List<BI_ElementosVisb> lista { get; set; }
    }


    public class Request_GetElementosVisbResumTotal
    {
        [JsonProperty("_a")]
        public string id_planning { get; set; }

        [JsonProperty("_b")]
        public int anio { get; set; }

        [JsonProperty("_c")]
        public int mes { get; set; }

        [JsonProperty("_d")]
        public int ciudad { get; set; }

        [JsonProperty("_e")]
        public int categoria { get; set; }

        [JsonProperty("_f")]
        public int agruptipomat { get; set; }

        [JsonProperty("_g")]
        public int lectura { get; set; } 
    }
    public class Response_GetElementosVisbResumTotal
    {
        [JsonProperty("_a")]
        public List<BI_ElementosVisb> lista { get; set; }
    }

    public class Request_GetElementosVisbEvolutivoCiuCat
    {
        [JsonProperty("_a")]
        public string id_planning { get; set; }

        [JsonProperty("_b")]
        public int anio { get; set; }

        [JsonProperty("_c")]
        public int mes { get; set; }

        [JsonProperty("_d")]
        public int ciudad { get; set; }

        [JsonProperty("_e")]
        public int categoria { get; set; }

        [JsonProperty("_f")]
        public int cadena { get; set; }

        [JsonProperty("_g")]
        public int agruptipomat { get; set; }

        [JsonProperty("_h")]
        public int agrupmat { get; set; }

        [JsonProperty("_i")]
        public int lectura { get; set; }
        [JsonProperty("_j")]
        public int periodo { get; set; }
    }
    public class Response_GetElementosVisbEvolutivoCiuCat
    {
        [JsonProperty("_a")]
        public List<BI_ElementosVisb> listaEvol { get; set; }
    }

    public class Request_GetElementosVisbComparativo
    {
        [JsonProperty("_a")]
        public string id_planning { get; set; }

        [JsonProperty("_b")]
        public int periodo { get; set; }


        [JsonProperty("_c")]
        public int ciudad { get; set; }

        [JsonProperty("_d")]
        public int categoria { get; set; }

        [JsonProperty("_e")]
        public int agruptipomat { get; set; }

        [JsonProperty("_f")]
        public int agrupmat { get; set; }
    }
    public class Response_GetElementosVisbComparativo
    {
        [JsonProperty("_a")]
        public List<BI_ElementosVisb> listaCompa { get; set; }
    }
    #endregion

    #endregion


}