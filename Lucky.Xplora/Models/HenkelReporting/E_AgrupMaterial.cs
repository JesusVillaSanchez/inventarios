﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;


namespace Lucky.Xplora.Models.HenkelReporting
{
    public class E_AgrupMaterial : AE_AgrupMaterial
    {
        [JsonProperty("_a")]
        public int codAgruptipoMat { get; set; }
        [JsonProperty("_b")]
        public string desctipomat { get; set; }
        [JsonProperty("_c")]
        public int codAgrupMat { get; set; }
        [JsonProperty("_d")]
        public string descmat { get; set; }

    }

    public abstract class AE_AgrupMaterial
    {
        public List<E_AgrupMaterial> Lista(Request_AgrupMat oRq)
        {
            Response_AgrupMat oRp = MvcApplication._Deserialize<Response_AgrupMat>(
                                   MvcApplication._Servicio_Operativa.GetAgrupMat(
                                           MvcApplication._Serialize(oRq))

                   );

            return oRp.lista;
        }

    }

    #region Request & Response
    public class Request_AgrupMat
    {
        [JsonProperty("_a")]
        public string id_planning { get; set; }
        [JsonProperty("_b")]
        public int agruptipomat { get; set; }
    }
    public class Response_AgrupMat
    {
        [JsonProperty("_a")]
        public List<E_AgrupMaterial> lista { get; set; }
    }


    #endregion

}