﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.HenkelReporting
{

    #region OOS

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-15
    /// </summary>
    public class Henkel_AASS_OOS : AHenkel_AASS_OOS
    {
        [JsonProperty("_a")]
        public int gru_id { get; set; }

        [JsonProperty("_b")]
        public string gru_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<Henkel_AASS_OOS_Periodo> periodo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-21
    /// </summary>
    public class Henkel_AASS_OOS_Periodo
    {
        [JsonProperty("_a")]
        public int rpl_orden { get; set; }

        [JsonProperty("_b")]
        public int rpl_id { get; set; }

        [JsonProperty("_c")]
        public string rpl_descripcion { get; set; }

        [JsonProperty("_d")]
        public decimal cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-18
    /// </summary>
    public class Henkel_AASS_OOS_Evolutivo
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_OOS_Evolutivo_Periodo> periodo { get; set; }

        [JsonProperty("_b")]
        public List<Henkel_AASS_OOS_Evolutivo_Producto> producto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-18
    /// </summary>
    public class Henkel_AASS_OOS_Evolutivo_Periodo
    {
        [JsonProperty("_a")]
        public int rpl_orden { get; set; }

        [JsonProperty("_b")]
        public int rpl_id { get; set; }

        [JsonProperty("_c")]
        public DateTime rpl_fecha_inicio { get; set; }

        [JsonProperty("_d")]
        public DateTime rpl_fecha_fin { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-18
    /// </summary>
    public class Henkel_AASS_OOS_Evolutivo_Producto
    {
        [JsonProperty("_a")]
        public string pro_sku { get; set; }

        [JsonProperty("_b")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_c")]
        public decimal col_6 { get; set; }

        [JsonProperty("_d")]
        public decimal col_5 { get; set; }

        [JsonProperty("_e")]
        public decimal col_4 { get; set; }

        [JsonProperty("_f")]
        public decimal col_3 { get; set; }

        [JsonProperty("_g")]
        public decimal col_2 { get; set; }

        [JsonProperty("_h")]
        public decimal col_1 { get; set; }

        [JsonProperty("_i")]
        public decimal col_tottus { get; set; }

        [JsonProperty("_j")]
        public decimal col_wong { get; set; }

        [JsonProperty("_k")]
        public decimal col_plaza { get; set; }

        [JsonProperty("_l")]
        public decimal col_metro { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-22
    /// </summary>
    public class Henkel_AASS_OOS_Filtro
    {
        [DefaultValue(false)]
        public bool categoria { get; set; }

        [DefaultValue(false)]
        public bool marca { get; set; }

        [DefaultValue(false)]
        public bool cadena { get; set; }

        public string campania { get; set; }

        [DefaultValue(0)]
        public int tipo_perfil { get; set; }

        [DefaultValue(false)]
        public bool oficina { get; set; }

        [DefaultValue(0)]
        public int reporte { get; set; }
        
        [DefaultValue(false)]
        public bool lectura { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-23
    /// </summary>
    public class Henkel_AASS_OOS_BD
    {
        [JsonProperty("_a")]
        public string fecha { get; set; }

        [JsonProperty("_b")]
        public int ofi_id { get; set; }

        [JsonProperty("_c")]
        public string ofi_descripcion { get; set; }

        [JsonProperty("_d")]
        public int cad_id { get; set; }

        [JsonProperty("_e")]
        public string cad_descripcion { get; set; }

        [JsonProperty("_f")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_g")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_h")]
        public int cat_id { get; set; }

        [JsonProperty("_i")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_j")]
        public int mar_id { get; set; }

        [JsonProperty("_k")]
        public string mar_descripcion { get; set; }

        [JsonProperty("_l")]
        public string pro_sku { get; set; }

        [JsonProperty("_m")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_n")]
        public string tqu_id { get; set; }

        [JsonProperty("_o")]
        public string tqu_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-16
    /// </summary>
    public abstract class AHenkel_AASS_OOS {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-16
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Henkel_AASS_OOS> Lista(Request_HenkelAASS_Parametros oRq) {
            Response_HenkelAASS_OOS oRp = MvcApplication._Deserialize<Response_HenkelAASS_OOS>(
                    MvcApplication._Servicio_Operativa.HenkelAASS_OOS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-18
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Henkel_AASS_OOS_Evolutivo Objeto(Request_HenkelAASS_Parametros oRq)
        {
            Response_HenkelAASS_OOS_Evolutivo oRp = MvcApplication._Deserialize<Response_HenkelAASS_OOS_Evolutivo>(
                    MvcApplication._Servicio_Operativa.HenkelAASS_OOS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-23
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Henkel_AASS_OOS_BD> Base(Request_HenkelAASS_Parametros oRq)
        {
            Response_HenkelAASS_OOS_BD oRp = MvcApplication._Deserialize<Response_HenkelAASS_OOS_BD>(
                    MvcApplication._Servicio_Operativa.HenkelAASS_OOS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-16
    /// </summary>
    public class Request_HenkelAASS_Parametros
    {
        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public int mes { get; set; }

        [JsonProperty("_c")]
        public int periodo { get; set; }

        [JsonProperty("_d")]
        public int categoria { get; set; }

        [JsonProperty("_e")]
        public int marca { get; set; }

        [JsonProperty("_f")]
        public int cadena { get; set; }

        [JsonProperty("_g")]
        public int opcion { get; set; }

        [JsonProperty("_h")]
        public int oficina { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-16
    /// </summary>
    public class Response_HenkelAASS_OOS
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_OOS> Lista { get; set; }
    }
    public class Response_HenkelAASS_OOS_Evolutivo
    {
        [JsonProperty("_a")]
        public Henkel_AASS_OOS_Evolutivo Objeto { get; set; }
    }
    public class Response_HenkelAASS_OOS_BD 
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_OOS_BD> Lista { get; set; }
    }
    #endregion

    #endregion

    #region Precio

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-22
    /// </summary>
    public class Henkel_AASS_Precios : AHenkel_AASS_Precios
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_Precios_Cab> lstPrecio_cab { get; set; }
        [JsonProperty("_b")]
        public List<Henkel_AASS_Precios_Detalle> lstPrecio_det { get; set; }
    }

    public class Henkel_AASS_Precios_HBO : AHenkel_AASS_Precios_HBO
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_Precios_Cab> lstPrecio_cab { get; set; }
        [JsonProperty("_b")]
        public List<Henkel_AASS_Precios_Detalle_HBO> lstPrecio_det { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-22
    /// </summary>
    public class Henkel_AASS_Precios_Cab
    {
        [JsonProperty("_a")]
        public string cab_cod { get; set; }
        [JsonProperty("_b")]
        public string cab_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-22
    /// </summary>
    public class Henkel_AASS_Precios_Detalle
    {
        [JsonProperty("_a")]
        public string family_name { get; set; }
        [JsonProperty("_b")]
        public string company_name { get; set; }
        [JsonProperty("_c")]
        public string product_cod { get; set; }
        [JsonProperty("_d")]
        public string product_name { get; set; }
        [JsonProperty("_e")]
        public string preg_periodo1 { get; set; }
        [JsonProperty("_f")]
        public string pofe_periodo1 { get; set; }
        [JsonProperty("_g")]
        public string preg_periodo2 { get; set; }
        [JsonProperty("_h")]
        public string pofe_periodo2 { get; set; }
        [JsonProperty("_i")]
        public string preg_periodo3 { get; set; }
        [JsonProperty("_j")]
        public string pofe_periodo3 { get; set; }
        [JsonProperty("_k")]
        public string preg_periodo4 { get; set; }
        [JsonProperty("_l")]
        public string pofe_periodo4 { get; set; }
        [JsonProperty("_m")]
        public string cadena1 { get; set; }
        [JsonProperty("_n")]
        public string cadena2 { get; set; }
        [JsonProperty("_ñ")]
        public string cadena3 { get; set; }
        [JsonProperty("_o")]
        public string cadena4 { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-22
    /// </summary>
    public class Henkel_AASS_Precios_Detalle_HBO
    {
        [JsonProperty("_a")]
        public string subcategory_name { get; set; }
        [JsonProperty("_b")]
        public string family_name { get; set; }
        [JsonProperty("_c")]
        public string company_name { get; set; }
        [JsonProperty("_d")]
        public string product_cod { get; set; }
        [JsonProperty("_e")]
        public string product_name { get; set; }
        [JsonProperty("_f")]
        public string preg_periodo1 { get; set; }
        [JsonProperty("_g")]
        public string pofe_periodo1 { get; set; }
        [JsonProperty("_h")]
        public string preg_periodo2 { get; set; }
        [JsonProperty("_i")]
        public string pofe_periodo2 { get; set; }
        [JsonProperty("_j")]
        public string preg_periodo3 { get; set; }
        [JsonProperty("_k")]
        public string pofe_periodo3 { get; set; }
        [JsonProperty("_l")]
        public string preg_periodo4 { get; set; }
        [JsonProperty("_m")]
        public string pofe_periodo4 { get; set; }
        [JsonProperty("_n")]
        public string cadena1 { get; set; }
        [JsonProperty("_ñ")]
        public string cadena2 { get; set; }
        [JsonProperty("_o")]
        public string cadena3 { get; set; }
        [JsonProperty("_p")]
        public string cadena4 { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015.09-22
    /// </summary>
    public class Henkel_AASS_Precios_Request
    {
        [JsonProperty("_a")]
        public int cod_ciudad { get; set; }
        [JsonProperty("_b")]
        public int cod_cadena { get; set; }
        [JsonProperty("_c")]
        public int cod_periodo { get; set; }
    }

    public abstract class AHenkel_AASS_Precios
    {
        public Henkel_AASS_Precios Lista_HenkelAASS_Precio(Request_HenkelAASS_Precios oRq)
        {
            Response_HenkelAASS_Precios oRp = MvcApplication._Deserialize<Response_HenkelAASS_Precios>(
                    MvcApplication._Servicio_Operativa.BHenkelAASS_Precios(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class AHenkel_AASS_Precios_HBO
    {
        public Henkel_AASS_Precios_HBO Lista_HenkelAASS_Precio_HBO(Request_HenkelAASS_Precios oRq)
        {
            Response_HenkelAASS_Precios_HBO oRp = MvcApplication._Deserialize<Response_HenkelAASS_Precios_HBO>(
                    MvcApplication._Servicio_Operativa.BHenkelAASS_Precios_HBO(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }
    

    #region Request - Response

    public class Request_HenkelAASS_Precios {
        [JsonProperty("_a")]
        public Henkel_AASS_Precios_Request oParametros { get; set; }
    }

    public class Response_HenkelAASS_Precios
    {
        [JsonProperty("_a")]
        public Henkel_AASS_Precios Lista { get; set; }
    }
    public class Response_HenkelAASS_Precios_HBO
    {
        [JsonProperty("_a")]
        public Henkel_AASS_Precios_HBO Lista { get; set; }
    }
    #endregion


    #endregion
    
    #region Incidencia

    public class Henkel_AASS_Incidencia_Resumen : AHenkel_AASS_Incidencia_Resumen
    {
        [JsonProperty("_a")]
        public List<Evo_Incidencia_Resuelto> lstincidenciaresuelto { get; set; }
        [JsonProperty("_b")]
        public Evo_Incidencia_Resuelto_Grilla lstincidenciatabla { get; set; }
    }

    public class Henkel_AASS_Incidencia_Detalle : AHenkel_AASS_Incidencia_Detalle
    {
        [JsonProperty("_a")]
        public string fecha { get; set; }
        [JsonProperty("_b")]
        public string ciudad { get; set; }
        [JsonProperty("_c")]
        public string cadena { get; set; }
        [JsonProperty("_d")]
        public string tienda { get; set; }
        [JsonProperty("_e")]
        public string actividad { get; set; }
        [JsonProperty("_f")]
        public string supervisor { get; set; }
        [JsonProperty("_g")]
        public string tipoincidencia { get; set; }
        [JsonProperty("_h")]
        public string solucion { get; set; }
    }

    public class Evo_Incidencia_Resuelto
    {
        [JsonProperty("_a")]
        public string fecha { get; set; }
        [JsonProperty("_b")]
        public string rpl_id { get; set; }
        [JsonProperty("_c")]
        public string cod { get; set; }
        [JsonProperty("_d")]
        public string porcentaje { get; set; }
    }

    public class Evo_Incidencia_Resuelto_Grilla
    {
        [JsonProperty("_a")]
        public List<Evo_Incidencia_Resuelto_Cabecera> olsh { get; set; }
        [JsonProperty("_b")]
        public List<Evo_Incidencia_Resuelto_Detalle> olsd { get; set; }
    }

    public class Evo_Incidencia_Resuelto_Cabecera
    {
        [JsonProperty("_a")]
        public string cod_periodo { get; set; }
        [JsonProperty("_b")]
        public string periodo { get; set; }
    }

    public class Evo_Incidencia_Resuelto_Detalle
    {
        [JsonProperty("_a")]
        public string descripcion { get; set; }
        [JsonProperty("_b")]
        public string periodo1 { get; set; }
        [JsonProperty("_c")]
        public string periodo2 { get; set; }
        [JsonProperty("_d")]
        public string periodo3 { get; set; }
        [JsonProperty("_e")]
        public string periodo4 { get; set; }
        [JsonProperty("_f")]
        public string periodo5 { get; set; }
    }


    public abstract class AHenkel_AASS_Incidencia_Resumen {
        public Henkel_AASS_Incidencia_Resumen Lista_HenkelAASS_Incidencia(Request_HenkelAASS_Incidencia oRq)
        {
            Response_HenkelAASS_Incidencia oRp = MvcApplication._Deserialize<Response_HenkelAASS_Incidencia>(
                    MvcApplication._Servicio_Operativa.BHenkelAASS_Incidencia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class AHenkel_AASS_Incidencia_Detalle {
        public List<Henkel_AASS_Incidencia_Detalle> Lista_HenkelAASS_IncidenciaDetalle(Request_HenkelAASS_Incidencia oRq)
        {
            Response_HenkelAASS_Incidencia_Detalle oRp = MvcApplication._Deserialize<Response_HenkelAASS_Incidencia_Detalle>(
                    MvcApplication._Servicio_Operativa.BHenkelAASS_IncidenciaDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }
    

    #region << Request Response>>

    public class Henkel_AASS_Incidencia_Request
    {
        [JsonProperty("_a")]
        public int cod_periodo { get; set; }
        [JsonProperty("_b")]
        public int cod_actividad { get; set; }
        [JsonProperty("_c")]
        public int cod_cadena { get; set; }
        [JsonProperty("_d")]
        public int cod_oficina { get; set; }
    }

    public class Request_HenkelAASS_Incidencia
    {
        [JsonProperty("_a")]
        public Henkel_AASS_Incidencia_Request oParametros { get; set; }
    }

    public class Response_HenkelAASS_Incidencia
    {
        [JsonProperty("_a")]
        public Henkel_AASS_Incidencia_Resumen Lista { get; set; }
    }

    public class Response_HenkelAASS_Incidencia_Detalle
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_Incidencia_Detalle> Lista { get; set; }
    }

    #endregion

    #endregion

    #region Exhibicion
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-25
    /// </summary>
    public class Henkel_AASS_Exhibicion_Grafico : AHenkel_AASS_Exhibicion_Grafico
    {
        [JsonProperty("_a")]
        public string gru_codigo { get; set; }

        [JsonProperty("_b")]
        public string gru_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<Henkel_AASS_Exhibicion_Grafico_Detalle> detalle { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-25
    /// </summary>
    public class Henkel_AASS_Exhibicion_Grafico_Detalle
    {
        [JsonProperty("_a")]
        public int det_id { get; set; }

        [JsonProperty("_b")]
        public string det_descripcion { get; set; }

        [JsonProperty("_c")]
        public decimal tarifa { get; set; }

        [JsonProperty("_d")]
        public int cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-25
    /// </summary>
    public class Henkel_AASS_Exhibicion_Tarifario
    {
        [JsonProperty("_a")]
        public int gru_id { get; set; }

        [JsonProperty("_b")]
        public string gru_descripcion { get; set; }

        [JsonProperty("_c")]
        public string mat_codigo { get; set; }

        [JsonProperty("_d")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_e")]
        public decimal metro { get; set; }

        [JsonProperty("_f")]
        public decimal wong { get; set; }

        [JsonProperty("_g")]
        public decimal plaza { get; set; }

        [JsonProperty("_h")]
        public decimal tottus { get; set; }
    }

    public class Henkel_AASS_Exhibicion_BD
    {
        [JsonProperty("_a")]
        public string ean { get; set; }
        [JsonProperty("_b")]
        public string lima_provincia { get; set; }
        [JsonProperty("_c")]
        public string ciudad { get; set; }
        [JsonProperty("_d")]
        public string cadena { get; set; }
        [JsonProperty("_e")]
        public string tiendas { get; set; }
        [JsonProperty("_f")]
        public string categoria { get; set; }
        [JsonProperty("_g")]
        public string empresa { get; set; }
        [JsonProperty("_h")]
        public string gru_descripcion { get; set; }
        [JsonProperty("_i")]
        public string elemento { get; set; }
        [JsonProperty("_j")]
        public string cantidad { get; set; }

    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-25
    /// </summary>
    public class AHenkel_AASS_Exhibicion_Grafico {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Henkel_AASS_Exhibicion_Grafico> Grafico(Request_HenkelAASS_Parametros oRq)
        {
            Response_Henkel_AASS_Exhibicion_Grafico oRp = MvcApplication._Deserialize<Response_Henkel_AASS_Exhibicion_Grafico>(
                    MvcApplication._Servicio_Operativa.HenkelAASS_Exhibicion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Henkel_AASS_Exhibicion_Tarifario> Tarifario(Request_HenkelAASS_Parametros oRq)
        {
            Response_Henkel_AASS_Exhibicion_Tarifario oRp = MvcApplication._Deserialize<Response_Henkel_AASS_Exhibicion_Tarifario>(
                    MvcApplication._Servicio_Operativa.HenkelAASS_Exhibicion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-25
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Henkel_AASS_Exhibicion_BD> BD(Request_HenkelAASS_Parametros oRq)
        {
            Response_Henkel_AASS_Exhibicion_BD oRp = MvcApplication._Deserialize<Response_Henkel_AASS_Exhibicion_BD>(
                    MvcApplication._Servicio_Operativa.HenkelAASS_Exhibicion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-25
    /// </summary>
    public class Response_Henkel_AASS_Exhibicion_Grafico
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_Exhibicion_Grafico> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-25
    /// </summary>
    public class Response_Henkel_AASS_Exhibicion_Tarifario
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_Exhibicion_Tarifario> Lista { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-25
    /// </summary>
    public class Response_Henkel_AASS_Exhibicion_BD
    {
        [JsonProperty("_a")]
        public List<Henkel_AASS_Exhibicion_BD> Lista { get; set; }
    }

    #endregion
    #endregion
}
