﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;


namespace Lucky.Xplora.Models.HenkelReporting
{
    public class Actividades : AActividades
    {
        [JsonProperty("_a")]
        public string oficina { get; set; }

        [JsonProperty("_b")]
        public string fecha { get; set; }

        [JsonProperty("_c")]
        public string empresa { get; set; }

        [JsonProperty("_d")]
        public string marca { get; set; }

        [JsonProperty("_e")]
        public string actividad { get; set; }

        [JsonProperty("_f")]
        public string descripcion { get; set; }

        [JsonProperty("_g")]
        public string categoria { get; set; }

        [JsonProperty("_h")]
        public string foto { get; set; }

        [JsonProperty("_i")]
        public string alcance { get; set; }

        [JsonProperty("_j")]
        public string vigencia { get; set; }

        [JsonProperty("_k")]
        public string codigoactv { get; set; }

        [JsonProperty("_l")]
        public string precio { get; set; }

        [JsonProperty("_m")]
        public string gramaje { get; set; }

        [JsonProperty("_n")]
        public string impulso { get; set; }

        [JsonProperty("_o")]
        public string pop { get; set; }

        [JsonProperty("_p")]
        public string comentarios { get; set; }

        [JsonProperty("_q")]
        public string cadena { get; set; }

    }

    

        public abstract class AActividades{
        /// <summary>
        /// Autor:curiarte
        /// Fecha: 2015-08-27
        /// </summary>
        
            public List<Actividades> Lista(Request_GetActividades oRq)
            {
                Response_GetActividades oRp = MvcApplication._Deserialize<Response_GetActividades>(
                                    MvcApplication._Servicio_Campania.GetActividades(
                                            MvcApplication._Serialize(oRq))

                    );

                return oRp.lista;
            }



        }


        #region Request & Response
        /// <summary>
        /// Autor:curiarte
        /// Fecha: 2015-08-27
        /// </summary>
        public class Request_GetActividades
        {

            [JsonProperty("_a")]
            public string id_planning { get; set; }

            [JsonProperty("_b")]
            public int anio { get; set; }

            [JsonProperty("_c")]
            public int mes { get; set; }

            [JsonProperty("_d")]
            public int cadena { get; set; }

            [JsonProperty("_e")]
            public string categoria { get; set; }

        }
        public class Response_GetActividades
        {
            [JsonProperty("_a")]
            public List<Actividades> lista { get; set; }
        }
        #endregion





}