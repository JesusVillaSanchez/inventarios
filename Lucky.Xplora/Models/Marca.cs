﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Marca : AMarca
    {
        [JsonProperty("a")]
        public int mar_id { get; set; }

        [JsonProperty("d")]
        public string mar_descripcion { get; set; }
    }

    public abstract class AMarca {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-17
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Marca> Lista(Request_GetMarca_Opcion_Campania_Categoria oRq)
        {
            Response_GetMarca_Opcion_Campania_Categoria oRp;

            oRp = MvcApplication._Deserialize<Response_GetMarca_Opcion_Campania_Categoria>(
                    MvcApplication._Servicio_Campania.GetMarca_Opcion_Campania_Categoria(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-23
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Marca> Lista(Request_GetMarca_Campania_Reporte_Categoria_ProductoFamilia oRq)
        {
            Response_GetMarca_Campania_Reporte_Categoria_ProductoFamilia oRp;

            oRp = MvcApplication._Deserialize<Response_GetMarca_Campania_Reporte_Categoria_ProductoFamilia>(
                    MvcApplication._Servicio_Campania.GetMarca_Campania_Reporte_Categoria_ProductoFamilia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-06-01
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Marca> ListaBackus(Request_GetMarca_Campania_Reporte_Categoria_ProductoFamiliaBackus oRq)
        {
            Response_GetMarca_Campania_Reporte_Categoria_ProductoFamiliaBackus oRp;

            oRp = MvcApplication._Deserialize<Response_GetMarca_Campania_Reporte_Categoria_ProductoFamiliaBackus>(
                    MvcApplication._Servicio_Campania.GetMarca_Campania_Reporte_Categoria_ProductoFamilia_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-04-24
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Marca> Lista(Request_GetMarca_Opcion_Categoria oRq)
        {
            Response_GetMarca_Opcion_Campania_Categoria oRp;

            oRp = MvcApplication._Deserialize<Response_GetMarca_Opcion_Campania_Categoria>(
                    MvcApplication._Servicio_Campania.GetMarca_Opcion_Categoria(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-17
    /// </summary>
    public class Request_GetMarca_Opcion_Campania_Categoria
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string campania { get; set; }

        [JsonProperty("_c")]
        public int categoria { get; set; }
    }
    public class Response_GetMarca_Opcion_Campania_Categoria
    {
        [JsonProperty("_a")]
        public List<Marca> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 20104-23
    /// </summary>
    public class Request_GetMarca_Campania_Reporte_Categoria_ProductoFamilia
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int reporte { get; set; }

        [JsonProperty("_c")]
        public int categoria { get; set; }

        [JsonProperty("_d")]
        public string producto_familia { get; set; }
    }
    public class Response_GetMarca_Campania_Reporte_Categoria_ProductoFamilia
    {
        [JsonProperty("_a")]
        public List<Marca> Lista { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-04-24
    /// </summary>
    public class Request_GetMarca_Opcion_Categoria
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string categoria { get; set; }
    }

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 2015-06-01
    /// </summary>
    public class Request_GetMarca_Campania_Reporte_Categoria_ProductoFamiliaBackus
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public string reporte { get; set; }

        [JsonProperty("_c")]
        public int categoria { get; set; }

        [JsonProperty("_d")]
        public string producto_familia { get; set; }
    }
    public class Response_GetMarca_Campania_Reporte_Categoria_ProductoFamiliaBackus
    {
        [JsonProperty("_a")]
        public List<Marca> Lista { get; set; }
    }

    #endregion
}