﻿using Lucky.Xplora.Models.Administrador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Utilitario
    {
        public static void registrar_visita(string _b) {
            if (_b != null && _b != "")
            {
                if (HttpContext.Current.Session["Vista_repetida"] == null || !((string)HttpContext.Current.Session["Vista_repetida"]).Equals(_b))
                {
                    HttpContext.Current.Session["Vista_repetida"] = _b;
                    Persona session = (Persona)HttpContext.Current.Session["Session_Login"];
                    new Insert_Visita_Persona_Response().Inserta_Visita_Modulo(
                        new Insert_Visita_Persona_Request()
                        {
                            person_id = session.Person_id,
                            modulo_id = _b,
                            company_id = session.Company_id.ToString(),
                            fecha_ingreso = DateTime.Today.ToString("dd/MM/yyyy"),
                            visitas = 1,
                            createBy = session.name_user
                        }
                    );
                }
            }
        }

        #region Auditoria
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-03-28
        /// </summary>
        /// <param name="ruta"></param>
        /// <param name="tipo_carga"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public bool AuditoriaUpDownLoad(string ruta, AuditoriaLoad tipo_carga, string usuario)
        {
            ResponseInsertaAuditoriaUpDownLoad oRp = MvcApplication._Deserialize<ResponseInsertaAuditoriaUpDownLoad>(
                    MvcApplication._Servicio_Operativa.AuditoriaUpDownLoad(MvcApplication._Serialize(new RequestParametrosAuditoriaUpDownLoad()
                    { 
                        aud_ruta = ruta, 
                        aud_tipo_carga = tipo_carga, 
                        aud_usuario = usuario }))
                );

            return oRp.respuesta;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-03-28
        /// </summary>
        public enum AuditoriaLoad { 
            Upload = 1,
            Download = 2
        }
        #endregion

        #region Response & Request
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-03-28
        /// </summary>
        public class RequestParametrosAuditoriaUpDownLoad
        {
            [JsonProperty("_a")]
            public string aud_ruta { get; set; }

            [JsonProperty("_b")]
            public AuditoriaLoad aud_tipo_carga { get; set; }

            [JsonProperty("_c")]
            public string aud_usuario { get; set; }
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-03-28
        /// </summary>
        public class ResponseInsertaAuditoriaUpDownLoad
        {
            [JsonProperty("_a")]
            public bool respuesta { get; set; }
        }
        #endregion
    }
}