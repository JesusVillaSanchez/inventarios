﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Campania : ACampania
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string cam_codigo { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string cam_descripcion { get; set; }
    }

    public abstract class ACampania {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Campania> Lista(MapMultiGPS_Request oRq)
        {
            List<Campania> oLs = new List<Campania>();
            MapMultiGPS_Filtro_Response oRp;

            oRp = MvcApplication._Deserialize<MapMultiGPS_Filtro_Response>(
                    MvcApplication._Servicio_Maps.Consul_Filtro_MultiGPS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            /*obtiene resultado y lo llena en listado de campanias*/
            foreach (M_Combo oBj in oRp.Lista) {
                oLs.Add(new Campania() {
                    cam_codigo = oBj.Value,
                    cam_descripcion = oBj.Text
                });
            }

            return oLs;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Campania> Lista(Request_GetCampania_Opcion oRq)
        {
            List<Campania> oLs = new List<Campania>();
            Response_GetCampania_Opcion oRp;

            oRp = MvcApplication._Deserialize<Response_GetCampania_Opcion>(
                    MvcApplication._Servicio_Campania.GetCampania_Opcion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            /*obtiene resultado y lo llena en listado de campanias*/
            foreach (ECampania oBj in oRp.Lista)
            {
                oLs.Add(new Campania()
                {
                    cam_codigo = oBj.cam_codigo,
                    cam_descripcion = oBj.cam_descripcion
                });
            }

            return oLs;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-26
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<E_Campania> Lista(Obtener_Campania_por_idCampania_Request oRq)
        {
            Obtener_Campania_por_idCampania_Response oRp;

            oRp = MvcApplication._Deserialize<Obtener_Campania_por_idCampania_Response>(
                    MvcApplication._Servicio_Campania.Obtener_Campanias_por_idCampania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-12-13
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Campania Objeto(Obtener_Campania_por_idCampania_Request oRq)
        {
            Campania oCampania = new Campania();
            Obtener_Campania_por_idCampania_Response oRp;

            oRp = MvcApplication._Deserialize<Obtener_Campania_por_idCampania_Response>(
                    MvcApplication._Servicio_Campania.Obtener_Campanias_por_idCampania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            foreach (E_Campania vCampania in oRp.Lista) 
            {
                if (oRp.Lista.First() == vCampania)
                {
                    oCampania.cam_codigo = vCampania.Id_planning;
                    oCampania.cam_descripcion = vCampania.Planning_Name;
                }
            }

            return oCampania;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-01
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Campania> Lista(Request_GetCampania_Compania oRq)
        {
            List<Campania> oLs = new List<Campania>();
            Response_GetCampania_Compania oRp;

            oRp = MvcApplication._Deserialize<Response_GetCampania_Compania>(
                    MvcApplication._Servicio_Campania.GetCampania_Compania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            /*obtiene resultado y lo llena en listado de campanias*/
            foreach (ECampania oBj in oRp.Lista)
            {
                oLs.Add(new Campania()
                {
                    cam_codigo = oBj.cam_codigo,
                    cam_descripcion = oBj.cam_descripcion
                });
            }

            return oLs;
        }
    }

    #region Request & Response

    public class E_parametros_MultiGPS
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string Cod_Pais { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string Cod_Equipo { get; set; }

        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string Cod_Ciudad { get; set; }

        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string Cod_Cargo { get; set; }

        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public int Cod_Usuario { get; set; }

        [JsonProperty("f", NullValueHandling = NullValueHandling.Ignore)]
        public string Fecha_Inicio { get; set; }

        [JsonProperty("g", NullValueHandling = NullValueHandling.Ignore)]
        public string Fecha_Fin { get; set; }

        [JsonProperty("h", NullValueHandling = NullValueHandling.Ignore)]
        public int tipo { get; set; }

        [JsonProperty("i", NullValueHandling = NullValueHandling.Ignore)]
        public string Empresa { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguez
    /// Fecha: 2015-11-03
    /// Desc.: Descripcion de Compañia
    /// </summary>
    public class E_Campania
    {
        [JsonIgnore]
        public int Cod_Strategy { get; set; }
        [JsonIgnore]
        public string Email_Contac { get; set; }
        [JsonIgnore]
        public int Id_designFor { get; set; }
        [JsonProperty("a")]
        public string Id_planning { get; set; }
        [JsonIgnore]
        public string Name_Contact { get; set; }
        [JsonIgnore]
        public string Planning_ActivityFormats { get; set; }
        [JsonIgnore]
        public string Planning_AreaInvolved { get; set; }
        [JsonIgnore]
        public string Planning_Budget { get; set; }
        [JsonIgnore]
        public string Planning_CodChannel { get; set; }
        [JsonIgnore]
        public string Planning_CreateBy { get; set; }
        [JsonIgnore]
        public DateTime Planning_DateBy { get; set; }
        [JsonIgnore]
        public DateTime Planning_DateFinreport { get; set; }
        [JsonIgnore]
        public DateTime Planning_DateModiBy { get; set; }
        [JsonIgnore]
        public DateTime Planning_DateRepSoli { get; set; }
        [JsonIgnore]
        public string Planning_DevelopmentActivityReport { get; set; }
        [JsonIgnore]
        public DateTime Planning_EndActivity { get; set; }
        [JsonIgnore]
        public bool Planning_FormaCompe { get; set; }
        [JsonIgnore]
        public string Planning_ModiBy { get; set; }
        [JsonProperty("b")]
        public string Planning_Name { get; set; }
        [JsonIgnore]
        public int Planning_Person_Eje { get; set; }
        [JsonIgnore]
        public DateTime Planning_PreproduDateEnd { get; set; }
        [JsonIgnore]
        public DateTime Planning_PreproduDateini { get; set; }
        [JsonIgnore]
        public string Planning_ProjectDuration { get; set; }
        [JsonIgnore]
        public string Planning_ReportAditional { get; set; }
        [JsonIgnore]
        public DateTime Planning_StartActivity { get; set; }
        [JsonIgnore]
        public bool Planning_Status { get; set; }
        [JsonIgnore]
        public string Planning_Vigen { get; set; }
        [JsonIgnore]
        public int Status_id { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-14
    /// Desc.: Homologando entidades
    /// </summary>
    public class ECampania
    {
        [JsonProperty("a")]
        public string cam_codigo { get; set; }

        [JsonProperty("b")]
        public string cam_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-07
    /// </summary>
    public class MapMultiGPS_Request
    {
        [JsonProperty("a")]
        public E_parametros_MultiGPS parametros { get; set; }
    }
    public class MapMultiGPS_Filtro_Response
    {
        [JsonProperty("a")]
        public List<M_Combo> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-13
    /// </summary>
    public class Request_GetCampania_Opcion
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Include)]
        public int opcion { get; set; }
    }
    public class Response_GetCampania_Opcion
    {
        [JsonProperty("_a")]
        public List<ECampania> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-26
    /// </summary>
    public class Obtener_Campania_por_idCampania_Request
    {
        [JsonProperty("a")]
        public string campania { get; set; }
    }
    public class Obtener_Campania_por_idCampania_Response
    {
        [JsonProperty("a")]
        //public List<Campania> Lista { get; set; }
        public List<E_Campania> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-03-01
    /// </summary>
    public class Request_GetCampania_Compania
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Include)]
        public int compania { get; set; }
    }
    public class Response_GetCampania_Compania
    {
        [JsonProperty("_a")]
        public List<ECampania> Lista { get; set; }
    }

    #endregion
}