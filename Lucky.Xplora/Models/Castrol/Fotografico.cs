﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Unacem
{
    #region Fotografico
        public class UnacemFotografico : AUnacemFotografico
        {
            /// <summary>
            /// Autor: wlopez
            /// Fecha: 2017-01-26
            /// </summary>
            /// <param name="Parametro"></param>
            /// <returns></returns>
            public override List<E_Fachada_Unacem> Consulta_Solicitud_Fotografico(UnacemFotograficoParametro Parametro)
            {
                Response_FW_Consulta_solicitud oRp = MvcApplication._Deserialize<Response_FW_Consulta_solicitud>
                (MvcApplication._Servicio_Operativa.FW_Una_consulta_solicitud(MvcApplication._Serialize(Parametro)));

                return oRp.Obj;
            }
        }

        public class UnacemFotograficoParametro
        {
            [JsonProperty("_a")]
            public int opcion { get; set; }

            [JsonProperty("_b")]
            public string parametro { get; set; }
        }

        public abstract class AUnacemFotografico
        {
            /// <summary>
            /// Autor: wlopez
            /// Fecha: 2017-01-26
            /// </summary>
            /// <param name="Parametro"></param>
            /// <returns></returns>
            public abstract List<E_Fachada_Unacem> Consulta_Solicitud_Fotografico(UnacemFotograficoParametro Parametro);

        }
    #endregion
    
}