﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class ElementoVisibilidad_Colgate
    {

        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string id_ReportsPlanning { get; set; }
        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public char id { get; set; }
        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string grupo { get; set; }
        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string compania { get; set; }
        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public int cantElementos { get; set; }
        [JsonProperty("f", NullValueHandling = NullValueHandling.Ignore)]
        public string nomElemento { get; set; }

    }


    //public abstract class AElementoVisibilidad_Colgate
    //{
    //    public List<ElementoVisibilidad_Colgate> Lista(Request_Consulta_Elementos_Visibilidad_Colgate_Reports oRq)
    //    {
    //        Response_Consulta_Elementos_Visibilidad_Colgate_Reports oRp;

    //        oRp = MvcApplication._Deserialize<Response_Consulta_Elementos_Visibilidad_Colgate_Reports>(
    //            MvcApplication._Servicio_Operativa.Consulta_Elementos_Visibilidad_Colgate (
    //                MvcApplication._Serialize(oRq)
    //            )
    //        );

    //        return oRp.Lista;
    //    }
    //}


    #region Request & Response

    public class Request_Consulta_Elementos_Visibilidad_Colgate_Reports
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string planning { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public int cliente { get; set; }

        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public int periodo { get; set; }

        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public int oficina { get; set; }

        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public int sector { get; set; }

        [JsonProperty("f", NullValueHandling = NullValueHandling.Ignore)]
        public int mercado { get; set; }

    }
    public class Response_Consulta_Elementos_Visibilidad_Colgate_Reports
    {
        [JsonProperty("a")]
        public List<ElementoVisibilidad_Colgate> Lista { get; set; }
    }

    #endregion


}