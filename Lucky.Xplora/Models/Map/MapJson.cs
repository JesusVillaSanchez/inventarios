﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Map
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-18
    /// </summary>
    public class MapJson : AMapJson
    {
        [JsonProperty("group_code")]
        public int grupo_codigo { get; set; }

        [JsonProperty("group_name")]
        public string grupo_descripcion { get; set; }

        [JsonProperty("type")]
        public string tipo { get; set; }

        [JsonProperty("features")]
        public List<MapJsonCaracteristica> caracteristicas { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-18
    /// </summary>
    public class MapJsonCaracteristica
    {
        [JsonProperty("type")]
        public string tipo { get; set; }

        [JsonProperty("geometry")]
        public MapJsonGeometria geometria { get; set; }

        [JsonProperty("properties")]
        public MapJsonPropiedad propiedades { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-18
    /// </summary>
    public class MapJsonGeometria
    {
        [JsonProperty("type")]
        public string tipo { get; set; }

        [JsonProperty("coordinates")]
        public Object coordenada { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-18
    /// </summary>
    public class MapJsonPropiedad
    {
        [JsonProperty("group")]
        public int grupo { get; set; }

        [JsonProperty("name")]
        public string nombre { get; set; }

        [JsonProperty("code")]
        public string ubigeo { get; set; }
    }

    public abstract class AMapJson {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-21
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public MapJson Objeto(Request_MapJson oRq)
        {
            Response_MapJson oRp;

            oRp = MvcApplication._Deserialize<Response_MapJson>(
                    MvcApplication._Servicio_Maps.MapJson(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-18
    /// </summary>
    public class Request_MapJson
    {
        [JsonProperty("_a")]
        public int compania { get; set; }

        [JsonProperty("_b")]
        public int canal { get; set; }

        [JsonProperty("_c")]
        public int grupo { get; set; }
    }
    public class Response_MapJson
    {
        [JsonProperty("_a")]
        public MapJson Objeto { get; set; }
    }

    #endregion
}