﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lucky.Xplora.Models.Map
{
    public class LuckyFusionColgate
    {
        public List<E_Anio> obtener_Anios()
        {
            Listar_Anios_Response oRp;
            oRp = MvcApplication._Deserialize<Listar_Anios_Response>(MvcApplication._Servicio_Campania.Listar_Anios());
            return oRp.oListaAnios;
        }
        public List<E_Cob_MapFC_tabla> Cobertura(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_Cobertura_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_Cobertura_Response>(
                    MvcApplication._Servicio_Maps.Consultar_CoberturaMapsColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_Cobertura;
        }
        public List<M_Presencia_MapFC> PresenciaProducto(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_Presencia_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_Presencia_Response>(
                    MvcApplication._Servicio_Maps.Consul_Presencia_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oPresencia;
        }
        public List<Presencia_Elemento_VisibilidadFC> PresenciaVisibilidad(Request_Elem_Visi_MapFC oRq)
        {
            Response_Elem_Visi_MapFC oRp;
            oRp = MvcApplication._Deserialize<Response_Elem_Visi_MapFC>(
                    MvcApplication._Servicio_Maps.BL_Elem_Visi_MapFC(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
        public List<E_PuntoVentaMapa> PDVsPresenciaVisibilidad(Request_Elem_Visi_Export_MapFC oRq)
        {
            Mps_FC_PDVS_Response oRp;

            oRp = MvcApplication._Deserialize<Mps_FC_PDVS_Response>(
                    MvcApplication._Servicio_Maps.BL_Elem_Visi_PDV_MapFC(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oReporte_PDVS;
        }
        public List<Presencia_Elemento_Visibilidad_ExportaFC> Exportar_PresenciaVisibilidad(Request_Elem_Visi_Export_MapFC oRq)
        {
            Response_Elem_Visi_Export_MapFC oRp;

            oRp = MvcApplication._Deserialize<Response_Elem_Visi_Export_MapFC>(
                    MvcApplication._Servicio_Maps.BL_Elem_Visi_Export_MapFC(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
        public List<Presencia_Elemento_VisibilidadFC> PresenciaVisibilidadPDV(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Response_Elem_Visi_MapFC oRp;
            oRp = MvcApplication._Deserialize<Response_Elem_Visi_MapFC>(
                    MvcApplication._Servicio_Maps.BL_RepElemVisiPDV_MapFC(MvcApplication._Serialize(oRequest)));
            return oRp.Lista;
        }


        public List<E_Actividad_CompeMapFC> ActividadCompeMapFC(Request_ActividadCompetenciaFC oRq)
        {
            Mps_FC_ActiviCompetencia_Response oRp;

            oRp = MvcApplication._Deserialize<Mps_FC_ActiviCompetencia_Response>(
                    MvcApplication._Servicio_Maps.Consul_ActivCompe_MpColgate(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oReporte_ActCompe;
        }
        public List<E_Detalle_Activi_CompeMapFC> DetalleActividadCompeMapFC(Request_ActividadCompetenciaFC oRq)
        {
            Mps_FC_DetalleActiviCompet_Response oRp;

            oRp = MvcApplication._Deserialize<Mps_FC_DetalleActiviCompet_Response>(
                    MvcApplication._Servicio_Maps.Consul_DetActivCompe_MpColgate(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oRep_DetalleActCompe;
        }
        public List<E_PuntoVentaMapa> PDVsCobertura(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Consultar_PDVS_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Exportar_Cobertura_MapFC> Exportar_cobertura(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_Export_Cobertura_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_Export_Cobertura_Response>(
                    MvcApplication._Servicio_Maps.Exportar_Cobertura_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oCobertura;
        }
        public E_PuntoVentaDatosMapa DetallePDV(Obtener_DatosPuntosVentaMapa_Request oRequest)
        {
            Obtener_DatosPuntosVentaMapa_Response oRp;
            oRp = MvcApplication._Deserialize<Obtener_DatosPuntosVentaMapa_Response>(MvcApplication._Servicio_Maps.Consul_DatosPDV_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.ptoVenta;
        }

        /// <summary>
        /// Autor: pccopa
        /// Fecha: 01/12/2015
        /// </summary>
        /// <param name="oRequest"></param>
        /// <returns></returns>
        public List<E_FotoFC> FotoPDV(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_Fotos_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_Fotos_Response>(MvcApplication._Servicio_Maps.Consul_Foto_pdv_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_Foto;
        }
        public List<E_Tracking_MapFC_tabla> TrackingPrecioPDV(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_TrackingPDVCatg_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_TrackingPDVCatg_Response>(
                    MvcApplication._Servicio_Maps.Consul_TrackingporPDV_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte;
        }
        public List<M_Tracking_precio_MapFC> TrackingPrecio(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_Tracking_precios_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_Tracking_precios_Response>(
                    MvcApplication._Servicio_Maps.Consul_TrackingPrecio_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_TrackingPrecio;
        }
        public List<E_PuntoVentaMapa> PDVsTrackingPrecio(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Consul_PDVS_TrackPrec_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Exportar_Tracking_MapFC> Exportar_Tracking_precio(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_Export_TrackingPrecio_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_Export_TrackingPrecio_Response>(
                    MvcApplication._Servicio_Maps.Exportar_TrackingPrecio_MpColgate(
                        MvcApplication._Serialize(oRequest)
                    )
                );
            return oRp.oTracking;
        }

        public List<M_Presencia_MapFC> PresenciaProductoPDV(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_Presencia_x_PDV_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_Presencia_x_PDV_Response>(
                    MvcApplication._Servicio_Maps.Consul_Presencia_x_PDV_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oPres_PDV;
        }
        public List<E_PuntoVentaMapa> PDVsPresenciaProducto(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Consul_PDVS_Presencia_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Export_Presencia_MapFC> Exportar_PresenciaProducto(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_Export_Presencia_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_Export_Presencia_Response>(
                    MvcApplication._Servicio_Maps.Exportar_Presencia_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oExport_Pres;
        }

        //SOS
        public List<M_ReporteSOS_MapFC> ShareOfShelf(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_SOS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_SOS_Response>(MvcApplication._Servicio_Maps.Consul_SOS_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_SOS;
        }
        public List<M_ReporteSOS_MapFC> ShareOfShelfPDV(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_SOS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_SOS_Response>(MvcApplication._Servicio_Maps.Consul_SOS_PDV_MpColgate(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_SOS;
        }

        public List<E_PuntoVentaMapa> Lista_PDV_Periodo(Consultar_RepMapsFCGeneral_Request oRequest)
        {
            Mps_FC_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Lista_PDV_Periodo(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }

    }
    public class Resquest_New_XPL_MapsFusionColg
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string campania { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int grupo { get; set; }

        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string ubigeo { get; set; }

        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int anio { get; set; }

        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int mes { get; set; }

        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public int periodo { get; set; }

        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public int giro { get; set; }

        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public string pdv { get; set; }

        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public int cluster { get; set; }

        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public int segmento { get; set; }

        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public int categoria { get; set; }

        [JsonProperty("_l", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_tipoactividad { get; set; }

        [JsonProperty("_m", NullValueHandling = NullValueHandling.Ignore)]
        public string producto { get; set; }

        [JsonProperty("_n", NullValueHandling = NullValueHandling.Ignore)]
        public int marca { get; set; }

        [JsonProperty("_o", NullValueHandling = NullValueHandling.Ignore)]
        public int opcion { get; set; }

        [JsonProperty("_p", NullValueHandling = NullValueHandling.Ignore)]
        public string parametros { get; set; }

        [JsonProperty("_q", NullValueHandling = NullValueHandling.Ignore)]
        public int elemento { get; set; }

        [JsonProperty("_r", NullValueHandling = NullValueHandling.Ignore)]
        public int tipoGiro { get; set; }
    }

    public class Resquest_ListaEmpresa
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string campania { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int reporte_id { get; set; }

        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_categoria { get; set; }
    }

    #region Filtros

    public class Response_New_XPL_Maps_FusionColg_Filtros
    {
        [JsonProperty("_a")]
        public List<M_Combo> Lista { get; set; }
    }
    public class New_XplMapsFsColg_Filtro_Service
    {
        public List<M_Combo> Filtro(Resquest_New_XPL_MapsFusionColg oRq)
        {
            Response_New_XPL_Maps_FusionColg_Filtros oRp;

            oRp = MvcApplication._Deserialize<Response_New_XPL_Maps_FusionColg_Filtros>(
                    MvcApplication._Servicio_Maps.GetFusionColgateFiltros(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }

        public List<M_Combo> ListarEmpresas(Resquest_ListaEmpresa oRq)
        {
            Response_New_XPL_Maps_FusionColg_Filtros oRp;

            oRp = MvcApplication._Deserialize<Response_New_XPL_Maps_FusionColg_Filtros>(
                    MvcApplication._Servicio_Campania.GetEmpresa_CampReportCat(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
    }

    #endregion
    #region <<< Request y Response >>>
    public class E_Parametros_MapFC
    {
        [JsonProperty("a")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("b")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("c")]
        public String Anio { get; set; }
        [JsonProperty("d")]
        public String Mes { get; set; }
        [JsonProperty("e")]
        public int Periodo { get; set; }
        [JsonProperty("f")]
        public int cod_reporte { get; set; }
        [JsonProperty("g")]
        public String Cod_Ubieo { get; set; }
        [JsonProperty("h")]
        public int Cod_Tipo { get; set; }
        [JsonProperty("i")]
        public String Cod_Categoria { get; set; }
        [JsonProperty("j")]
        public int Cod_Cadena { get; set; }
        [JsonProperty("k")]
        public int Cod_Cluster { get; set; }
        [JsonProperty("l")]
        public int Tipo_General { get; set; }
        [JsonProperty("m")]
        public string Cod_Servicio { get; set; }
        [JsonProperty("n")]
        public string Cod_Canal { get; set; }
        [JsonProperty("o")]
        public string Cod_PDV { get; set; }
        [JsonProperty("p")]
        public int Id_Reportsplanning { get; set; }
        [JsonProperty("q")]
        public string Desc_Segmento { get; set; }
        [JsonProperty("r")]
        public string cod_pais { get; set; }
        [JsonProperty("s")]
        public string cod_sku { get; set; }
        [JsonProperty("t")]
        public string actividad { get; set; }
        [JsonProperty("u")]
        public int Tipo_Exhi { get; set; }
        [JsonProperty("v")]
        public int Cod_Elemento { get; set; }
        [JsonProperty("w")]
        public string Cod_Nivel { get; set; }
        [JsonProperty("x")]
        public string Parametros { get; set; }
        [JsonProperty("y")]
        public string Cod_Marca { get; set; }
        [JsonProperty("z")]
        public int opcion { get; set; }
        [JsonProperty("aa")]
        public string segmento { get; set; }
        [JsonProperty("ab")]
        public string giro { get; set; }
    }
    public class Consultar_RepMapsFCGeneral_Request
    {
        [JsonProperty("a")]
        public E_Parametros_MapFC oParametros { get; set; }
    }
    public class Mps_FC_Cobertura_Response
    {
        [JsonProperty("a")]
        public List<E_Cob_MapFC_tabla> oReporte_Cobertura { get; set; }
    }
    public class Mps_FC_PDVS_Response
    {
        [JsonProperty("a")]
        public List<E_PuntoVentaMapa> oReporte_PDVS { get; set; }
    }
    public class Mps_FC_Export_Cobertura_Response
    {
        [JsonProperty("a")]
        public List<E_Exportar_Cobertura_MapFC> oCobertura { get; set; }
    }
    //public class Mps_FC_Fotos_Response
    //{
    //    [JsonProperty("a")]
    //    public List<E_Foto> oReporte_Foto { get; set; }
    //}
    public class Mps_FC_Tracking_precios_Response
    {
        [JsonProperty("a")]
        public List<M_Tracking_precio_MapFC> oReporte_TrackingPrecio { get; set; }
    }
    public class Mps_FC_TrackingPDVCatg_Response
    {
        [JsonProperty("a")]
        public List<E_Tracking_MapFC_tabla> oReporte { get; set; }
    }
    public class Mps_FC_Export_TrackingPrecio_Response
    {
        [JsonProperty("a")]
        public List<E_Exportar_Tracking_MapFC> oTracking { get; set; }
    }
    public class Mps_FC_Presencia_Response
    {
        [JsonProperty("a")]
        public List<M_Presencia_MapFC> oPresencia { get; set; }
    }
    public class Mps_FC_Export_Presencia_Response
    {
        [JsonProperty("a")]
        public List<E_Export_Presencia_MapFC> oExport_Pres { get; set; }
    }
    public class Mps_FC_Presencia_x_PDV_Response
    {
        [JsonProperty("a")]
        public List<M_Presencia_MapFC> oPres_PDV { get; set; }
        //public List<E_Presencia_Padre_MapFC> oPres_PDV { get; set; }
    }
    public class Mps_FC_SOS_Response
    {
        [JsonProperty("a")]
        public List<M_ReporteSOS_MapFC> oReporte_SOS { get; set; }
    }
    public class Response_Elem_Visi_MapFC
    {
        [JsonProperty("a")]
        public List<Presencia_Elemento_VisibilidadFC> Lista { get; set; }
    }
    public class Request_Elem_Visi_Export_MapFC
    {
        [JsonProperty("a")]
        public Parametros_Presencia_Elemento_VisibilidadFC oParametros { get; set; }
    }
    public class Response_Elem_Visi_Export_MapFC
    {
        [JsonProperty("a")]
        public List<Presencia_Elemento_Visibilidad_ExportaFC> Lista { get; set; }
    }
    public class Request_ActividadCompetenciaFC
    {
        [JsonProperty("a")]
        public E_Parametros_MapFC oParametros { get; set; }
    }
    public class Mps_FC_ActiviCompetencia_Response
    {
        [JsonProperty("a")]
        public List<E_Actividad_CompeMapFC> oReporte_ActCompe { get; set; }
    }
    public class Mps_FC_DetalleActiviCompet_Response
    {
        [JsonProperty("a")]
        public List<E_Detalle_Activi_CompeMapFC> oRep_DetalleActCompe { get; set; }
    }
    public class Mps_FC_Fotos_Response
    {
        [JsonProperty("a")]
        public List<E_FotoFC> oReporte_Foto { get; set; }
    }
    #endregion


    #region  <<< Cobertura >>>
    public class E_Cob_MapFC_tabla
    {
        [JsonProperty("a")]
        public int Cod_Cluster { get; set; }
        [JsonProperty("b")]
        public string Item { get; set; }
        [JsonProperty("c")]
        public string Cantidad { get; set; }
        [JsonProperty("d")]
        public string Peso { get; set; }
        [JsonProperty("e")]
        public string Representatividad { get; set; }
        [JsonProperty("f")]
        public List<E_Cob_MapFC_tabla_hijo> hijo { get; set; }
        [JsonProperty("g")]
        public int tipo { get; set; }
    }
    public class E_Cob_MapFC_tabla_hijo
    {
        [JsonProperty("a")]
        public int Cod_Cluster { get; set; }
        [JsonProperty("b")]
        public string Item { get; set; }
        [JsonProperty("c")]
        public string Cantidad { get; set; }
        [JsonProperty("d")]
        public string Peso { get; set; }
        [JsonProperty("e")]
        public string Representatividad { get; set; }
        [JsonProperty("f")]
        public int tipo { get; set; }

    }
    public class E_Exportar_Cobertura_MapFC
    {
        [JsonProperty("a")]
        public string Cod_PDV { get; set; }
        [JsonProperty("b")]
        public string Nombre_PDV { get; set; }
        [JsonProperty("c")]
        public string Raz_Social { get; set; }
        [JsonProperty("d")]
        public string Direccion { get; set; }
        [JsonProperty("e")]
        public string Cadena { get; set; }
        [JsonProperty("f")]
        public string Pais { get; set; }
        [JsonProperty("g")]
        public string Departamento { get; set; }
        [JsonProperty("h")]
        public string Provincia { get; set; }
        [JsonProperty("i")]
        public string Distrito { get; set; }
        [JsonProperty("j")]
        public string Anio { get; set; }
        [JsonProperty("k")]
        public string Mes { get; set; }
        [JsonProperty("l")]
        public string Semana { get; set; }
        [JsonProperty("m")]
        public string Cluster { get; set; }

    }
    #endregion
    #region <<< Tracking de Precio >>>
    public class M_Tracking_precio_MapFC
    {
        [JsonProperty("a")]
        public string cod_categoria { get; set; }
        [JsonProperty("b")]
        public string nom_categoria { get; set; }
        [JsonProperty("c")]
        public string cod_sku { get; set; }
        [JsonProperty("d")]
        public string nom_sku { get; set; }
        [JsonProperty("e")]
        public decimal? prec_reventa { get; set; }
        [JsonProperty("f")]
        public decimal? prec_PDV { get; set; }
        [JsonProperty("g")]
        public decimal? prec_Costo { get; set; }
        [JsonProperty("h")]
        public int id { get; set; }
        [JsonProperty("i")]
        public decimal? prec_moda { get; set; }
        [JsonProperty("j")]
        public decimal? prec_max { get; set; }
        [JsonProperty("k")]
        public decimal? prec_min { get; set; }
        [JsonProperty("l")]
        public decimal? prec_promedio { get; set; }

    }
    public class E_Exportar_Tracking_MapFC
    {
        [JsonProperty("a")]
        public string Cod_PDV { get; set; }
        [JsonProperty("b")]
        public string Nombre_PDV { get; set; }
        [JsonProperty("c")]
        public string Direccion { get; set; }
        [JsonProperty("d")]
        public string Cadena { get; set; }
        [JsonProperty("e")]
        public string Sector { get; set; }
        [JsonProperty("f")]
        public string Pais { get; set; }
        [JsonProperty("g")]
        public string Departamento { get; set; }
        [JsonProperty("h")]
        public string Provincia { get; set; }
        [JsonProperty("i")]
        public string Distrito { get; set; }
        [JsonProperty("j")]
        public string Categoria { get; set; }
        [JsonProperty("k")]
        public string Marca { get; set; }
        [JsonProperty("l")]
        public string SKU { get; set; }
        [JsonProperty("m")]
        public string Nombre_Producto { get; set; }
        [JsonProperty("n")]
        public string Precio_Reventa { get; set; }
        [JsonProperty("o")]
        public string Precio_PVP { get; set; }
        [JsonProperty("p")]
        public string Precio_Costo { get; set; }
        [JsonProperty("q")]
        public string Fecha_reg_celda { get; set; }
        [JsonProperty("r")]
        public string Anio { get; set; }
        [JsonProperty("s")]
        public string Mes { get; set; }
        [JsonProperty("t")]
        public string Semana { get; set; }
        [JsonProperty("u")]
        public string Presencia { get; set; }
        [JsonProperty("v")]
        public string Precio_Sugerido { get; set; }
    }
    public class E_Tracking_MapFC_tabla
    {
        [JsonProperty("a")]
        public string cod_categoria { get; set; }
        [JsonProperty("b")]
        public string nom_categoria { get; set; }
        [JsonProperty("c")]
        public int id { get; set; }
        [JsonProperty("d")]
        public List<E_Tracking_MapFC_tabla_hijo> hijo { get; set; }
    }
    public class E_Tracking_MapFC_tabla_hijo
    {
        [JsonProperty("a")]
        public string cod_sku { get; set; }
        [JsonProperty("b")]
        public string nom_sku { get; set; }
        [JsonProperty("c")]
        public decimal? prec_reventa { get; set; }
        [JsonProperty("d")]
        public decimal? prec_PDV { get; set; }
        [JsonProperty("e")]
        public decimal? prec_Costo { get; set; }
        [JsonProperty("f")]
        public string nom_marca { get; set; }
    }
    #endregion
    #region <<< Presencia Producto >>>
    public class M_Presencia_MapFC
    {
        [JsonProperty("a")]
        public string Tipo { get; set; }
        [JsonProperty("b")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("c")]
        public string Categoria { get; set; }
        [JsonProperty("d")]
        public string Cod_SKU { get; set; }
        [JsonProperty("e")]
        public string Producto { get; set; }
        [JsonProperty("f")]
        public int Cantidad { get; set; }
        [JsonProperty("g")]
        public decimal? Porcentaje { get; set; }
        [JsonProperty("h")]
        public int Porcentaje_Int { get; set; }
        [JsonProperty("i")]
        public int Orden { get; set; }
        [JsonProperty("j")]
        public int Presencia { get; set; }
        [JsonProperty("k")]
        public string Marca { get; set; }
    }
    public class E_Export_Presencia_MapFC
    {
        [JsonProperty("a")]
        public string Cod_PDV { get; set; }
        [JsonProperty("b")]
        public string Nombre_PDV { get; set; }
        [JsonProperty("c")]
        public string Raz_Social { get; set; }
        [JsonProperty("d")]
        public string Direccion { get; set; }
        [JsonProperty("e")]
        public string Cadena { get; set; }
        [JsonProperty("f")]
        public string Sector { get; set; }
        [JsonProperty("g")]
        public string Pais { get; set; }
        [JsonProperty("h")]
        public string Departamento { get; set; }
        [JsonProperty("i")]
        public string Provincia { get; set; }
        [JsonProperty("j")]
        public string Distrito { get; set; }
        [JsonProperty("k")]
        public string Categoria { get; set; }
        [JsonProperty("l")]
        public string Marca { get; set; }
        [JsonProperty("m")]
        public string Cod_SKU { get; set; }
        [JsonProperty("n")]
        public string Producto { get; set; }
        [JsonProperty("o")]
        public string Anio { get; set; }
        [JsonProperty("p")]
        public string Mes { get; set; }
        [JsonProperty("q")]
        public string Semana { get; set; }
        [JsonProperty("r")]
        public string Presencia { get; set; }
    }
    public class E_Presencia_Padre_MapFC
    {
        [JsonProperty("a")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("b")]
        public string Categoria { get; set; }
        public List<E_Presencia_Hijo_MapFC> Hijo { get; set; }
    }
    public class E_Presencia_Hijo_MapFC
    {
        [JsonProperty("d")]
        public string Cod_Item { get; set; }
        [JsonProperty("e")]
        public string Item { get; set; }
        [JsonProperty("f")]
        public string Valor { get; set; }
    }
    #endregion
    #region <<< Presencia Visibilidad >>>
    public class Request_Elem_Visi_MapFC
    {
        [JsonProperty("a")]
        public string TipoUbigeo { get; set; }
        [JsonProperty("b")]
        public string cod_ubieo { get; set; }
        [JsonProperty("c")]
        public string cod_pais { get; set; }
        [JsonProperty("d")]
        public string cod_cadena { get; set; }
        [JsonProperty("e")]
        public string cod_perido { get; set; }
        [JsonProperty("f")]
        public string cod_categoria { get; set; }
        [JsonProperty("g")]
        public string cod_marca { get; set; }
        [JsonProperty("h")]
        public string cod_tipo_canal { get; set; }
        [JsonProperty("i")]
        public string cod_Elemento { get; set; }
        [JsonProperty("j")]
        public int opcion { get; set; }
        [JsonProperty("k")]
        public string cod_pdv { get; set; }
        [JsonProperty("l")]
        public string cod_sku { get; set; }
    }
    public class Presencia_Elemento_VisibilidadFC
    {
        [JsonProperty("a")]
        public string cod_Elemento { get; set; }

        [JsonProperty("b")]
        public string Elemento { get; set; }

        [JsonProperty("c")]
        public string PDV_Totales { get; set; }

        [JsonProperty("d")]
        public string PDV_Con_Presencia { get; set; }

        [JsonProperty("e")]
        public string Presencia { get; set; }
        [JsonProperty("f")]
        public string Cantidad { get; set; }
    }
    public class Parametros_Presencia_Elemento_VisibilidadFC
    {
        [JsonProperty("a")]
        public string TipoUbigeo { get; set; }
        [JsonProperty("b")]
        public string cod_ubieo { get; set; }
        [JsonProperty("c")]
        public string cod_pais { get; set; }
        [JsonProperty("d")]
        public string cadena { get; set; }
        [JsonProperty("e")]
        public string id_reportsplanning { get; set; }
        [JsonProperty("f")]
        public string cod_categoria { get; set; }
        [JsonProperty("g")]
        public string cod_marca { get; set; }
        [JsonProperty("h")]
        public string cod_tipo_canal { get; set; }
        [JsonProperty("i")]
        public string cod_Elemento { get; set; }
        [JsonProperty("j")]
        public int opcion { get; set; }
        [JsonProperty("k")]
        public string cod_pdv { get; set; }
        [JsonProperty("l")]
        public string cod_sku { get; set; }
        [JsonProperty("m")]
        public string cod_equipo { get; set; }
    }
    public class Presencia_Elemento_Visibilidad_ExportaFC
    {
        [JsonProperty("a")]
        public string cod_pdv { get; set; }
        [JsonProperty("b")]
        public string nom_pdv { get; set; }
        [JsonProperty("c")]
        public string raz_social { get; set; }
        [JsonProperty("d")]
        public string direccion { get; set; }
        [JsonProperty("e")]
        public string nom_cadena { get; set; }
        [JsonProperty("f")]
        public string Sector { get; set; }
        [JsonProperty("g")]
        public string Name_Country { get; set; }
        [JsonProperty("h")]
        public string Name_dpto { get; set; }
        [JsonProperty("i")]
        public string Name_City { get; set; }
        [JsonProperty("j")]
        public string District { get; set; }
        [JsonProperty("k")]
        public string fec_reg_cel { get; set; }
        [JsonProperty("l")]
        public string nom_categoria { get; set; }
        [JsonProperty("m")]
        public string cod_material { get; set; }
        [JsonProperty("n")]
        public string nom_material { get; set; }
        [JsonProperty("ñ")]
        public string cantidad { get; set; }
        [JsonProperty("o")]
        public string anio { get; set; }
        [JsonProperty("p")]
        public string mes { get; set; }
        [JsonProperty("q")]
        public string presencia { get; set; }

    }
    #endregion
    #region <<Actividades>>

    public class E_Actividad_CompeMapFC
    {
        [JsonProperty("a")]
        public int cod_marca { get; set; }
        [JsonProperty("b")]
        public string marca { get; set; }
        [JsonProperty("c")]
        public string cod_act { get; set; }
        [JsonProperty("d")]
        public string actividad { get; set; }
    }

    public class E_Detalle_Activi_CompeMapFC
    {
        [JsonProperty("a")]
        public string cod_pdv { get; set; }
        [JsonProperty("b")]
        public string nom_pdv { get; set; }
        [JsonProperty("c")]
        public string cadena { get; set; }
        [JsonProperty("d")]
        public string empresa { get; set; }
        [JsonProperty("e")]
        public string marca { get; set; }
        [JsonProperty("f")]
        public string actividad { get; set; }
        [JsonProperty("g")]
        public string grupo_obj { get; set; }
        [JsonProperty("h")]
        public string inicio_prom { get; set; }
        [JsonProperty("i")]
        public int cant_per { get; set; }
        [JsonProperty("j")]
        public string mecanica { get; set; }
        [JsonProperty("k")]
        public string material { get; set; }
        [JsonProperty("l")]
        public string foto { get; set; }
        [JsonProperty("m")]
        public string comentario { get; set; }
        [JsonProperty("n")]
        public string categoria { get; set; }
    }

    #endregion
    #region <<< Share of Shelf >>>
    public class M_ReporteSOS_MapFC
    {
        [JsonProperty("a")]
        public int Cod_Marca { get; set; }
        [JsonProperty("b")]
        public string Nom_Marca { get; set; }
        [JsonProperty("c")]
        public string Porcentaje { get; set; }
        [JsonProperty("d")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("e")]
        public string Nom_Categoria { get; set; }
    }
 
    #endregion

    public class E_FotoFC
    {
        [JsonProperty("a")]
        public string nombreFoto { get; set; }
        [JsonProperty("b")]
        public int Id { get; set; }
        [JsonProperty("c")]
        public string Descripcion { get; set; }
        [JsonProperty("d")]
        public string Periodo { get; set; }
        [JsonProperty("e")]
        public string CabPeriodo { get; set; }
        [JsonProperty("f")]
        public string id_productCategory { get; set; }
        [JsonProperty("g")]
        public string Product_Category { get; set; }
        [JsonProperty("h")]
        public string fechaFoto { get; set; }
    }
}
