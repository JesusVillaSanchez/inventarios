﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Map
{
    #region RQ Lucky 264 v1.0

    #region Cobertura

    public class AlicorpMinoristaCobertura : AAlicorpMinoristaCobertura
    {
        [JsonProperty("_a")]
        public int cob_id { get; set; }

        [JsonProperty("_b")]
        public string cob_descripcion { get; set; }

        [JsonProperty("_c")]
        public int cob_total { get; set; }

        [JsonProperty("_d")]
        public int cob_porcentaje { get; set; }

        [JsonProperty("_e")]
        public List<AlicorpMinoristaCoberturaDetalle> cob_detalle { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-10
    /// </summary>
    public class AlicorpMinoristaExportaPopElemento
    {
        [JsonProperty("_a")]
        public string ultima_visita { get; set; }

        [JsonProperty("_b")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_c")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_d")]
        public string distrito { get; set; }

        [JsonProperty("_e")]
        public string ciudad { get; set; }

        [JsonProperty("_f")]
        public string giro { get; set; }

        [JsonProperty("_g")]
        public string gie { get; set; }

        [JsonProperty("_h")]
        public string instalado { get; set; }

        [JsonProperty("_i")]
        public string visita_antes { get; set; }

        [JsonProperty("_j")]
        public string visita_despues { get; set; }

    }

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 2015-09-11
    /// </summary>
    public class AlicorpMinoristaCoberturaExcel : AAlicorpMinoristaCoberturaExcel
    {
        [JsonProperty("_a")]
        public String fech_ini { get; set; }

        [JsonProperty("_b")]
        public String pdv_cod { get; set; }

        [JsonProperty("_c")]
        public String pdv_desc { get; set; }

        [JsonProperty("_d")]
        public String distribuidora { get; set; }

        [JsonProperty("_e")]
        public string distrito { get; set; }

        [JsonProperty("_f")]
        public String ciudad { get; set; }

        [JsonProperty("_g")]
        public String giro { get; set; }

        [JsonProperty("_h")]
        public String seg_descripcion { get; set; }

        [JsonProperty("_i")]
        public String nom_gie { get; set; }

    }

    public class AlicorpMinoristaCoberturaDetalle
    {
        [JsonProperty("_a")]
        public int tcp_id { get; set; }

        [JsonProperty("_b")]
        public string tcp_descripcion { get; set; }

        [JsonProperty("_c")]
        public int tcp_cantidad { get; set; }

        [JsonProperty("_d")]
        public int tcp_porcentaje { get; set; }

        [JsonProperty("_e")]
        public List<AlicorpMinoristaMotivoDetalle> motivo { get; set; }
    }

    public class AlicorpMinoristaMotivoDetalle
    {
        [JsonProperty("_a")]
        public int tcp_id { get; set; }

        [JsonProperty("_b")]
        public string mot_descripcion { get; set; }

        [JsonProperty("_c")]
        public int mot_cantidad { get; set; }

        [JsonProperty("_d")]
        public int cob_id { get; set; }
    }

    public abstract class AAlicorpMinoristaCoberturaExcel {

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-09-11
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaCoberturaExcel> PdvExcel(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaCoberturaPdvExcel oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaCoberturaPdvExcel>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaCoberturaPdvExcel(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

    }

    public abstract class AAlicorpMinoristaCobertura
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-26
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaCobertura> Cobertura(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaCobertura oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaCobertura>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaCobertura(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-26
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<PuntoVentaMapa> Pdv(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaCoberturaPdv oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaCoberturaPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaCoberturaPdv(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #endregion

    #region Presencia

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-10
    /// </summary>
    public class AlicorpMinoristaPresenciaPdv : AAlicorpMinoristaPresencia
    {
        [JsonProperty("_a")]
        public int cantidad { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpMinoristaPresenciaPdvCategoria> categoria { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpMinoristaPresenciaSemaforo> semaforo { get; set; }

        [JsonProperty("_d")]
        public decimal porcentaje_promedio { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-10
    /// </summary>
    public class AlicorpMinoristaPresenciaPdvCategoria
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpMinoristaPresenciaPdvSku> sku { get; set; }

        [JsonProperty("_d")]
        public int cantidad { get; set; }

        [JsonProperty("_e")]
        public decimal porcentaje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-03
    /// </summary>
    public class AlicorpMinoristaPresenciaPdvSku
    {
        [JsonProperty("_a")]
        public string pro_codigo { get; set; }

        [JsonProperty("_b")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_c")]
        public int cantidad { get; set; }

        [JsonProperty("_d")]
        public decimal porcentaje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-10
    /// </summary>
    public class AlicorpMinoristaPresenciaSkuCategoria
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpMinoristaPresenciaSku> sku { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class AlicorpMinoristaPresenciaSku
    {
        [JsonProperty("_a")]
        public string pro_codigo { get; set; }

        [JsonProperty("_b")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_c")]
        public int pro_presencia { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-11-03
    /// </summary>
    public class AlicorpMinoristaPresenciaPdvExcel 
    {
        [JsonProperty("_a")]
        public string fec_ini { get; set; }

        [JsonProperty("_b")]
        public string pdv_cod { get; set; }

        [JsonProperty("_c")]
        public string pdv_desc { get; set; }

        [JsonProperty("_d")]
        public string distrito { get; set; }

        [JsonProperty("_e")]
        public string oficina { get; set; }

        [JsonProperty("_f")]
        public string giro { get; set; }

        [JsonProperty("_g")]
        public string gie { get; set; }

        [JsonProperty("_h")]
        public string segmento { get; set; }

        [JsonProperty("_i")]
        public string categoria { get; set; }

        [JsonProperty("_j")]
        public string marca { get; set; }

        [JsonProperty("_k")]
        public string presencia { get; set; }

        [JsonProperty("_l")]
        public string sku { get; set; }

        [JsonProperty("_ll")]
        public string producto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-09
    /// </summary>
    public class AlicorpMinoristaPresenciaSemaforo
    {
        [JsonProperty("_a")]
        public int sem_id { get; set; }

        [JsonProperty("_b")]
        public int sem_cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-04
    /// </summary>
    public abstract class AAlicorpMinoristaPresencia {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public AlicorpMinoristaPresenciaPdv Presencia(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaPresencia oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPresencia>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPresencia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<PuntoVentaMapa> Pdv(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaCoberturaPdv oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaCoberturaPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPresencia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaPresenciaSkuCategoria> Sku(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaPresenciaSku oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPresenciaSku>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPresencia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-09-11
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaPresenciaPdvExcel> Descarga(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaPresenciaExcel oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPresenciaExcel>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPresencia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #endregion

    #region Precio

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-10
    /// </summary>
    public class AlicorpMinoristaPrecioPdv : AAlicorpMinoristaPrecio
    {
        [JsonProperty("_a")]
        public int cantidad { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpMinoristaPrecioPdvCategoria> categoria { get; set; }

        [JsonProperty("_c")]
        public decimal porcentaje_promedio { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-10
    /// </summary>
    public class AlicorpMinoristaPrecioPdvCategoria
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpMinoristaPrecioPdvSku> sku { get; set; }

        [JsonProperty("_d")]
        public decimal porcentaje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-03
    /// </summary>
    public class AlicorpMinoristaPrecioPdvSku
    {
        [JsonProperty("_a")]
        public string pro_codigo { get; set; }

        [JsonProperty("_b")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_c")]
        public decimal precio_sugerido { get; set; }

        [JsonProperty("_d")]
        public int cantidad { get; set; }

        [JsonProperty("_e")]
        public decimal porcentaje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-10
    /// </summary>
    public class AlicorpMinoristaPrecioSkuCategoria
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpMinoristaPrecioSku> sku { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class AlicorpMinoristaPrecioSku
    {
        [JsonProperty("_a")]
        public string pro_codigo { get; set; }

        [JsonProperty("_b")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_c")]
        public decimal precio_sugerido { get; set; }

        [JsonProperty("_d")]
        public int pvp { get; set; }
    }

    /// <summary>
    /// Autor: yroriguezs
    /// Fecha: 2015-09-10
    /// </summary>
    public class AlicorpMinoristaExportaPrecio
    {

        [JsonProperty("_a")]
        public string ultima_visita { get; set; }

        [JsonProperty("_b")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_c")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_d")]
        public string distrito { get; set; }

        [JsonProperty("_e")]
        public string ciudad { get; set; }

        [JsonProperty("_f")]
        public string giro { get; set; }

        [JsonProperty("_g")]
        public string gie { get; set; }

        [JsonProperty("_h")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_i")]
        public string Name_Brand { get; set; }

        [JsonProperty("_j")]
        public string precio_sugerido { get; set; }

        [JsonProperty("_k")]
        public string cumple { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-04
    /// </summary>
    public abstract class AAlicorpMinoristaPrecio {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public AlicorpMinoristaPrecioPdv Precio(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaPrecio oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPrecio>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPrecio(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<PuntoVentaMapa> Pdv(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaCoberturaPdv oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaCoberturaPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPrecio(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaPrecioSkuCategoria> Sku(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaPrecioSku oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPrecioSku>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPrecio(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaExportaPrecio> ExportaPrecio(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaExportaPrecioSku oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaExportaPrecioSku>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPrecio(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

    }

    #endregion

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-26
    /// </summary>
    public class Resquest_AlicorpMinoristaCobertura
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string campania { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int grupo { get; set; }

        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string ubigeo { get; set; }

        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int anio { get; set; }

        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int mes { get; set; }

        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public int periodo { get; set; }

        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public int giro { get; set; }

        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public string pdv { get; set; }

        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public int cluster { get; set; }

        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public int segmento { get; set; }

        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public int categoria { get; set; }

        [JsonProperty("_l", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_tipoactividad { get; set; }

        [JsonProperty("_m", NullValueHandling = NullValueHandling.Ignore)]
        public string producto { get; set; }

        [JsonProperty("_n", NullValueHandling = NullValueHandling.Ignore)]
        public int marca { get; set; }

        [JsonProperty("_o", NullValueHandling = NullValueHandling.Ignore)]
        public int opcion { get; set; }

        [JsonProperty("_p", NullValueHandling = NullValueHandling.Ignore)]
        public string parametros { get; set; }

        [JsonProperty("_q", NullValueHandling = NullValueHandling.Ignore)]
        public int elemento { get; set; }

        [JsonProperty("_r", NullValueHandling = NullValueHandling.Ignore)]
        public int tipoGiro { get; set; }

        //YR
        /*
        [JsonProperty("_r", NullValueHandling = NullValueHandling.Ignore)]
        public int cadena { get; set; }
        [JsonProperty("_s", NullValueHandling = NullValueHandling.Ignore)]
        public string pais { get; set; }
        */
    }
   
    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 27/08/15
    /// </summary>
    public class Request_ActividadCompetenciaBK
    {
        [JsonProperty("a")]
        public E_Parametros_MapBK oParametros { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-26
    /// </summary>
    public class Response_AlicorpMinoristaCobertura
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaCobertura> Lista { get; set; }
    }
    public class Response_AlicorpMinoristaCoberturaPdv
    {
        [JsonProperty("_a")]
        public List<PuntoVentaMapa> Lista { get; set; }
    }

    public class Response_AlicorpMinoristaCoberturaPdvExcel
    {
        [JsonProperty("_a")]
            public List<AlicorpMinoristaCoberturaExcel> Lista { get; set; }
    }

    public class Response_AlicorpMinoristaPresenciaExcel
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaPresenciaPdvExcel> Lista { get; set; }
    }
    

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-10
    /// </summary>
    public class Response_AlicorpMinoristaExportaPopElemento
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaExportaPopElemento> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-04
    /// </summary>
    public class Response_AlicorpMinoristaPresencia
    {
        [JsonProperty("_a")]
        public AlicorpMinoristaPresenciaPdv Objeto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-04
    /// </summary>
    public class Response_AlicorpMinoristaPrecio
    {
        [JsonProperty("_a")]
        public AlicorpMinoristaPrecioPdv Objeto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class Response_AlicorpMinoristaPresenciaSku
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaPresenciaSkuCategoria> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class Response_AlicorpMinoristaPrecioSku
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaPrecioSkuCategoria> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class Response_AlicorpMinoristaPdv
    {
        [JsonProperty("_a")]
        public AlicorpMinoristaPdv Objeto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class Response_AlicorpMinoristaFotografico
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaFotografico> Lista { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-10
    /// </summary>
    public class Response_AlicorpMinoristaExportaPrecioSku
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaExportaPrecio> Lista { get; set; }
    }


    #endregion

    #region SOD
    public class AlicorpMinoristaSod
    {
        [JsonProperty("_a")]
        public decimal porcentaje_promedio { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpMinoristaSodCategoria> categoria { get; set; }
    }

    public class AlicorpMinoristaSodCategoria : AAlicorpMinoristaReporteSOD
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public int sod_alicorp { get; set; }

        [JsonProperty("_d")]
        public int sod_competencia { get; set; }

        [JsonProperty("_e")]
        public int som_alicorp { get; set; }

        [JsonProperty("_f")]
        public int som_competencia { get; set; }

        [JsonProperty("_g")]
        public List<AlicorpMinoristaSodMarca> marca { get; set; }

        [JsonProperty("_h")]
        public int muestra { get; set; }
    }

    public class AlicorpMinoristaSodMarca
    {
        [JsonProperty("_a")]
        public int mar_id { get; set; }

        [JsonProperty("_b")]
        public string mar_descripcion { get; set; }

        [JsonProperty("_c")]
        public int sod { get; set; }

        [JsonProperty("_d")]
        public int som { get; set; }
    }

    /// <summary>
    /// GF - 27/05/2015
    /// Trabaja Reporte SOD General y por pdv
    /// </summary>
    public class ReporteSOD
    {
        [JsonProperty("_a")]
        public String cod_categoria { get; set; }
        [JsonProperty("_b")]
        public String nom_categoria { get; set; }
        [JsonProperty("_c")]
        public String cod_marca { get; set; }
        [JsonProperty("_d")]
        public String nom_marca { get; set; }
        [JsonProperty("_e")]
        public String porcentaje { get; set; }
    }

    public class AlicorpMinoristaExportaSod
    {
        [JsonProperty("_a")]
        public string id { get; set; }

        [JsonProperty("_b")]
        public string ultima_visita { get; set; }

        [JsonProperty("_c")]
        public string pdv_cod { get; set; }

        [JsonProperty("_d")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_e")]
        public string distrito { get; set; }

        [JsonProperty("_f")]
        public string ciudad { get; set; }

        [JsonProperty("_g")]
        public string giro { get; set; }

        [JsonProperty("_h")]
        public string gie { get; set; }

        [JsonProperty("_i")]
        public string categoria { get; set; }

        [JsonProperty("_j")]
        public string marca { get; set; }

        [JsonProperty("_k")]
        public string competencia { get; set; }

        [JsonProperty("_l")]
        public string alicorp { get; set; }

    }

    #region resonse

    public class Response_AlicorpMinoristaSOD 
    {
        [JsonProperty("_a")]
        public AlicorpMinoristaSod Objeto { get; set; }
    }
    public class Response_AlicorpMinoristaSODPdv
    {
        [JsonProperty("_a")]
        public List<ReporteSOD> Lista { get; set; }
    }

    public class Response_AlicorpMinoristaExportaSOD
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaExportaSod> Lista { get; set; }
    }

    #endregion

    public abstract class AAlicorpMinoristaReporteSOD
    {
        public AlicorpMinoristaSod SOD(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaSOD oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaSOD>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaSOD(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<ReporteSOD> SODPdv(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaSODPdv oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaSODPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaSODPdv(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaExportaSod> ExportaSOD(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaExportaSOD oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaExportaSOD>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaExportaSOD(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

    }

    #endregion
    
    #region <<Actividades>>

    public class ReporteActividadesMenor : AReporteActividadesMenor
    {

        [JsonProperty("_a")]
        public string cod_marca { get; set; }
        [JsonProperty("_b")]
        public string nom_marca { get; set; }
        [JsonProperty("_c")]
        public string actividad_desc { get; set; }
    }

    public class ReporteActividadesMenor_Pdv
    {

        [JsonProperty("_a")]
        public string desc_tipoactividad { get; set; }
        [JsonProperty("_b")]
        public string desc_aunto { get; set; }
        [JsonProperty("_c")]
        public string fecha_ini { get; set; }
        [JsonProperty("_d")]
        public string fecha_fin { get; set; }
        [JsonProperty("_e")]
        public string desc_empresa { get; set; }
        [JsonProperty("_f")]
        public string desc_categoria { get; set; }
        [JsonProperty("_g")]
        public string desc_marca { get; set; }
        [JsonProperty("_h")]
        public string desc_producto { get; set; }
        [JsonProperty("_i")]
        public string desc_mecanica { get; set; }
        [JsonProperty("_j")]
        public string desc_tipopromocion { get; set; }
        [JsonProperty("_k")]
        public string desc_materialapoyo { get; set; }
        [JsonProperty("_l")]
        public string desc_publicoObjetivo { get; set; }
        [JsonProperty("_m")]
        public string prec_distribuidor { get; set; }
        [JsonProperty("_n")]
        public string prec_sugerido { get; set; }
        [JsonProperty("_o")]
        public string prec_reventa { get; set; }
        [JsonProperty("_p")]
        public string desc_tipoexhibicion { get; set; }
        [JsonProperty("_q")]
        public string desc_productoBonif { get; set; }
        [JsonProperty("_r")]
        public string desc_oficina { get; set; }
        [JsonProperty("_s")]
        public string desc_sector { get; set; }
        [JsonProperty("_t")]
        public string desc_distrito { get; set; }
        [JsonProperty("_u")]
        public string desc_cadena { get; set; }
        [JsonProperty("_v")]
        public string desc_puntoventa { get; set; }
        [JsonProperty("_w")]
        public string desc_comentario { get; set; }
    }

    public class E_Actividad_CompeMapBK    {        
        [JsonProperty("a")]
        public int cod_marca { get; set; }
        [JsonProperty("b")]
        public string marca { get; set; }
        [JsonProperty("c")]
        public string cod_act { get; set; }
        [JsonProperty("d")]
        public string actividad { get; set; }
    }

    public class E_Detalle_Activi_CompeMapBK
    {
        [JsonProperty("a")]
        public string cod_pdv { get; set; }
        [JsonProperty("b")]
        public string nom_pdv { get; set; }
        [JsonProperty("c")]
        public string cadena { get; set; }
        [JsonProperty("d")]
        public string empresa { get; set; }
        [JsonProperty("e")]
        public string marca { get; set; }
        [JsonProperty("f")]
        public string actividad { get; set; }
        [JsonProperty("g")]
        public string grupo_obj { get; set; }
        [JsonProperty("h")]
        public string inicio_prom { get; set; }
        [JsonProperty("i")]
        public int cant_per { get; set; }
        [JsonProperty("j")]
        public string mecanica { get; set; }
        [JsonProperty("k")]
        public string material { get; set; }
        [JsonProperty("l")]
        public string foto { get; set; }
        [JsonProperty("m")]
        public string comentario { get; set; }
        [JsonProperty("n")]
        public string categoria { get; set; }
    }

    public class Response_AlicorpMinoristaActividades 
    {
        [JsonProperty("_a")]
        public List<ReporteActividadesMenor> Lista { get; set; }
    }

    public class Response_AlicorpMinoristaActividadesPdv 
    {
        [JsonProperty("_a")]
        public List<ReporteActividadesMenor_Pdv> Lista { get; set; }
    }

    public class Mps_BK_ActiviCompetencia_Response
    {
        [JsonProperty("a")]
        public List<E_Actividad_CompeMapBK> oReporte_ActCompe { get; set; }
    }

    public class Mps_BK_DetalleActiviCompet_Response
    {
        [JsonProperty("a")]
        public List<E_Detalle_Activi_CompeMapBK> oRep_DetalleActCompe { get; set; }
    }

    public abstract class AReporteActividadesMenor
    {
        public List<ReporteActividadesMenor> Actividad(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaActividades oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaActividades>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaActividades(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
        public List<ReporteActividadesMenor_Pdv> Actividad_x_PDV(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaActividadesPdv oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaActividadesPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaActividadesPdv(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 27/08/15
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<E_Actividad_CompeMapBK> ActividadCompeMapBK(Request_ActividadCompetenciaBK oRq)
        {
            Mps_BK_ActiviCompetencia_Response oRp;

            oRp = MvcApplication._Deserialize<Mps_BK_ActiviCompetencia_Response>(
                    MvcApplication._Servicio_Maps.Consul_ActivCompe_MpBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oReporte_ActCompe;
        }
        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 27/08/15
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<E_Detalle_Activi_CompeMapBK> DetalleActividadCompeMapBK(Request_ActividadCompetenciaBK oRq)
        {
            Mps_BK_DetalleActiviCompet_Response oRp;

            oRp = MvcApplication._Deserialize<Mps_BK_DetalleActiviCompet_Response>(
                    MvcApplication._Servicio_Maps.Consul_DetActivCompe_MpBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oRep_DetalleActCompe;
        }
    }

    #endregion

    #region << Filtros >>>


    public class Response_AlicorpMinoristaFiltros
    {
        [JsonProperty("_a")]
        public List<M_Combo> Lista { get; set; }
    }

    public class ReporteAliMenor_Filtro_Service
    {
        public List<M_Combo> Filtro(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaFiltros oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaFiltros>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaFiltros(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }
    #endregion

    #region Pdv

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class AlicorpMinoristaPdv : AAlicorpMinoristaPdv
    {
        [JsonProperty("_a")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_b")]
        public string pdv_direccion { get; set; }

        [JsonProperty("_c")]
        public string dis_descripcion { get; set; }

        [JsonProperty("_d")]
        public string fecha { get; set; }

        [JsonProperty("_e")]
        public string per_nombre { get; set; }

        [JsonProperty("_f")]
        public string pdv_email { get; set; }

        [JsonProperty("_g")]
        public string tcp_descripcion { get; set; }

        [JsonProperty("_h")]
        public string ofi_descripcion { get; set; }

        [JsonProperty("_i")]
        public string nct_descripcion { get; set; }

        [JsonProperty("_j")]
        public List<AlicorpMinoristaPdvFoto> foto { get; set; }

        [JsonProperty("_k")]
        public string pdv_cod { get; set; }

        [JsonProperty("_l")]
        public string gestion { get; set; }

        [JsonProperty("_m")]
        public string tipoGiro { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class AlicorpMinoristaPdvFoto
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string foto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public abstract class AAlicorpMinoristaPdv {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public AlicorpMinoristaPdv Pdv(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaPdv oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPdv(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
        }
    }

    #endregion

    #region Fotografico

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class AlicorpMinoristaFotografico : AAlicorpMinoristaFotografico
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string foto { get; set; }

        [JsonProperty("_d")]
        public string fecha { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public abstract class AAlicorpMinoristaFotografico {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaFotografico> Lista(Resquest_AlicorpMinoristaCobertura oRq) {
            Response_AlicorpMinoristaFotografico oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaFotografico>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaFotografico(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;

        }
    }

    #endregion

    #region POP

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-10
    /// </summary>
    public class AlicorpMinoristaPopPdv : AAlicorpMinoristaPopPdv
    {
        [JsonProperty("_a")]
        public int cantidad { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpMinoristaPopPdvCategoria> categoria { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpMinoristaPopSemaforo> semaforo { get; set; }

        [JsonProperty("_d")]
        public decimal porcentaje_promedio { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-10
    /// </summary>
    public class AlicorpMinoristaPopPdvCategoria
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpMinoristaPopPdvElemento> pop { get; set; }

        [JsonProperty("_d")]
        public decimal porcentaje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-03
    /// </summary>
    public class AlicorpMinoristaPopPdvElemento
    {
        [JsonProperty("_a")]
        public int ele_id { get; set; }

        [JsonProperty("_b")]
        public string ele_descripcion { get; set; }

        [JsonProperty("_c")]
        public int instalado { get; set; }

        [JsonProperty("_d")]
        public decimal porcentaje_instalado { get; set; }

        [JsonProperty("_e")]
        public int visita_antes { get; set; }

        [JsonProperty("_f")]
        public decimal porcentaje_visita_antes { get; set; }

        [JsonProperty("_g")]
        public int visita_despues { get; set; }

        [JsonProperty("_h")]
        public decimal porcentaje_visita_despues { get; set; }

        //[JsonProperty("_c")]
        //public int cantidad { get; set; }

        //[JsonProperty("_d")]
        //public decimal porcentaje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-10
    /// </summary>
    public class AlicorpMinoristaPopElementoCategoria
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpMinoristaPopElemento> pop { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-06-08
    /// </summary>
    public class AlicorpMinoristaPopElemento
    {
        [JsonProperty("_a")]
        public int ele_id { get; set; }

        [JsonProperty("_b")]
        public string ele_descripcion { get; set; }

        [JsonProperty("_c")]
        public int instalado { get; set; }

        [JsonProperty("_d")]
        public int visita_antes { get; set; }

        [JsonProperty("_e")]
        public int visita_despues { get; set; }

        //    [JsonProperty("_c")]
        //    public int presencia { get; set; }
    }

    public abstract class AAlicorpMinoristaPopPdv
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public AlicorpMinoristaPopPdv Pop(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaPop oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPop>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPop(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<PuntoVentaMapa> Pdv(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaCoberturaPdv oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaCoberturaPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPop(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaPopElementoCategoria> Elemento(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaPopElemento oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPopElemento>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPop(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaExportaPopElemento> ExportaPop(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaExportaPopElemento oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaExportaPopElemento>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaPop(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-09
    /// </summary>
    public class AlicorpMinoristaPopSemaforo
    {
        [JsonProperty("_a")]
        public int sem_id { get; set; }

        [JsonProperty("_b")]
        public int sem_cantidad { get; set; }
    }

    #region Request & Response

    public class Response_AlicorpMinoristaPop
    {
        [JsonProperty("_a")]
        public AlicorpMinoristaPopPdv Objeto { get; set; }
    }
    public class Response_AlicorpMinoristaPopElemento
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaPopElementoCategoria> Lista { get; set; }
    }

    #endregion

    ////public class AlicorpMinoristaReportePOP : AAlicorpMinoristaReportePOP
    ////{
    ////    [JsonProperty("_a")]
    ////    public int cod_pdv { get; set; }
    ////    [JsonProperty("_b")]
    ////    public int tot_pdv { get; set; }
    ////    [JsonProperty("_c")]
    ////    public String cod_elemento { get; set; }
    ////    [JsonProperty("_d")]
    ////    public string nom_elemento { get; set; }
    ////    [JsonProperty("_e")]
    ////    public int presencia { get; set; }
    ////    [JsonProperty("_f")]
    ////    public int porcentaje { get; set; }
    ////}

    ///// <summary>
    ///// Autor: rcontreras
    ///// Fecha: 2015-07-14
    ///// </summary>
    //public class AlicorpMinoristaPOP : AAlicorpMinoristaReportePOP
    //{
    //    [JsonProperty("_a")]
    //    public int cod_pdv { get; set; }

    //    [JsonProperty("_b")]
    //    public int tot_pdv { get; set; }

    //    [JsonProperty("_c")]
    //    public List<AlicorpMinoristaPOPCategoria> categoria { get; set; }
    //}

    ///// <summary>
    ///// Autor: rcontreras
    ///// Fecha: 2015-07-14
    ///// </summary>
    //public class AlicorpMinoristaPOPCategoria
    //{
    //    [JsonProperty("_a")]
    //    public int cat_id { get; set; }

    //    [JsonProperty("_b")]
    //    public string cat_descripcion { get; set; }

    //    [JsonProperty("_c")]
    //    public List<AlicorpMinoristaPOPPdvElemento> elemento { get; set; }
    //}

    ///// <summary>
    ///// Autor: rcontreras
    ///// Fecha: 2015-07-14
    ///// </summary>
    //public class AlicorpMinoristaPOPPdvElemento
    //{
    //    [JsonProperty("_a")]
    //    public String cod_elemento { get; set; }
    //    [JsonProperty("_b")]
    //    public string nom_elemento { get; set; }
    //    [JsonProperty("_c")]
    //    public int presencia { get; set; }
    //    [JsonProperty("_d")]
    //    public int porcentaje { get; set; }
    //}

    //#region response

    //public class Response_AlicorpMinoristaPOP
    //{
    //    [JsonProperty("_a")]
    //    public List<AlicorpMinoristaPOPCategoria> Lista { get; set; }
    //}

    //#endregion

    //public abstract class AAlicorpMinoristaReportePOP
    //{
    //    public List<AlicorpMinoristaPOPCategoria> POP(Resquest_AlicorpMinoristaCobertura oRq)
    //    {
    //        Response_AlicorpMinoristaPOP oRp;

    //        oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPOP>(
    //                MvcApplication._Servicio_Maps.AlicorpMinoristaPOP(
    //                    MvcApplication._Serialize(oRq)
    //                )
    //            );

    //        return oRp.Lista;
    //    }

    //    public List<AlicorpMinoristaPOPCategoria> POP_Presencia(Resquest_AlicorpMinoristaCobertura oRq)
    //    {
    //        Response_AlicorpMinoristaPOP oRp;

    //        oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaPOP>(
    //                MvcApplication._Servicio_Maps.AlicorpMinoristaPOPPresencia(
    //                    MvcApplication._Serialize(oRq)
    //                )
    //            );

    //        return oRp.Lista;
    //    }
    //}

    #endregion

    #region Ventana

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-28
    /// </summary>
    public class AlicorpMinoristaVentanaPdv : AAlicorpMinoristaVentanaPdv
    {
        [JsonProperty("_a")]
        public int cantidad { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpMinoristaVentanaPdvCategoria> categoria { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpMinoristaVentanaSemaforo> semaforo { get; set; }

        [JsonProperty("_d")]
        public decimal porcentaje_promedio { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-28
    /// </summary>
    public class AlicorpMinoristaVentanaPdvCategoria
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public int instalado { get; set; }

        [JsonProperty("_d")]
        public decimal porcentaje_instalado { get; set; }

        [JsonProperty("_e")]
        public int instalado_correctamente { get; set; }

        [JsonProperty("_f")]
        public decimal porcentaje_instalado_correctamente { get; set; }

        [JsonProperty("_g")]
        public int presencia { get; set; }

        [JsonProperty("_h")]
        public int observacion { get; set; }

        [JsonProperty("_i")]
        public List<AlicorpMinoristaVentanaPdvCategoriaObservacion> observaciones { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-11-24
    /// </summary>
    public class AlicorpMinoristaVentanaPdvCategoriaObservacion
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string obs_codigo { get; set; }

        [JsonProperty("_c")]
        public string obs_descripcion { get; set; }

        [JsonProperty("_d")]
        public int cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-28
    /// </summary>
    public class AlicorpMinoristaVentanaCategoria
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public int instalado { get; set; }

        [JsonProperty("_d")]
        public int instalado_correctamente { get; set; }

        [JsonProperty("_e")]
        public string obs_abreviatura { get; set; }
    }

    public class AlicorpMinoristaExportaVentana
    {
        [JsonProperty("_a")]
        public string ultima_visita { get; set; }

        [JsonProperty("_b")]
        public string pvd_codigo { get; set; }

        [JsonProperty("_c")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_d")]
        public string distrito { get; set; }

        [JsonProperty("_e")]
        public string ciudad { get; set; }

        [JsonProperty("_f")]
        public string giro { get; set; }

        [JsonProperty("_g")]
        public string gie { get; set; }

        [JsonProperty("_h")]
        public string segmento { get; set; }

        [JsonProperty("_i")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_j")]
        public string instalados { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-09
    /// </summary>
    public class AlicorpMinoristaVentanaSemaforo
    {
        [JsonProperty("_a")]
        public int sem_id { get; set; }

        [JsonProperty("_b")]
        public int sem_cantidad { get; set; }
    }

    public abstract class AAlicorpMinoristaVentanaPdv {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-07-28
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public AlicorpMinoristaVentanaPdv Ventana(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaVentana oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaVentana>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaVentana(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-07-28
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<PuntoVentaMapa> Pdv(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaCoberturaPdv oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaCoberturaPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaVentana(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-07-28
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaVentanaCategoria> Categoria(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaVetanaPdv oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaVetanaPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaVentana(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 09/09/15
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpMinoristaExportaVentana> ExportaVentana(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaExportaVetana oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaExportaVetana>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaVentana(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-28
    /// </summary>
    public class Response_AlicorpMinoristaVentana
    {
        [JsonProperty("_a")]
        public AlicorpMinoristaVentanaPdv Objeto { get; set; }
    }
    public class Response_AlicorpMinoristaVetanaPdv
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaVentanaCategoria> Lista { get; set; }
    }
    public class Response_AlicorpMinoristaExportaVetana
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaExportaVentana> Lista { get; set; }
    }

    #endregion

    #endregion

    #region Venta
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-10
    /// </summary>
    public class AlicorpMinoristaVenta : AAlicorpMinoristaVenta
    {
        [JsonProperty("_a")]
        public List<AlicorpMinoristaVentaAnio> anio { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpMinoristaVentaCategoria> categoria { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-10
    /// </summary>
    public class AlicorpMinoristaVentaAnio
    {
        [JsonProperty("_a")]
        public int anio { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-10
    /// </summary>
    public class AlicorpMinoristaVentaCategoria
    {
        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public decimal actual_moneda { get; set; }

        [JsonProperty("_d")]
        public decimal actual_peso { get; set; }

        [JsonProperty("_e")]
        public decimal anterior_moneda { get; set; }

        [JsonProperty("_f")]
        public decimal anterior_peso { get; set; }

        [JsonProperty("_g")]
        public decimal variacion_moneda { get; set; }

        [JsonProperty("_h")]
        public decimal variacion_peso { get; set; }

        [JsonProperty("_i")]
        public decimal diferencia_moneda { get; set; }

        [JsonProperty("_j")]
        public decimal diferencia_peso { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-10
    /// </summary>
    public abstract class AAlicorpMinoristaVenta
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public AlicorpMinoristaVenta Venta(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaVenta oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaVenta>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaVenta(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<PuntoVentaMapa> Pdv(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaCoberturaPdv oRp;

            oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaCoberturaPdv>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaVenta(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public AlicorpMinoristaVenta Detalle(Resquest_AlicorpMinoristaCobertura oRq)
        {
            Response_AlicorpMinoristaVenta oRp = MvcApplication._Deserialize<Response_AlicorpMinoristaVenta>(
                    MvcApplication._Servicio_Maps.AlicorpMinoristaVenta(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-10
    /// </summary>
    public class Response_AlicorpMinoristaVenta
    {
        [JsonProperty("_a")]
        public AlicorpMinoristaVenta Objeto { get; set; }
    }
    #endregion
    #endregion

    #endregion
}