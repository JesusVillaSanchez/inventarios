﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lucky.Xplora.Models.Map
{
    public class LuckyAlicorp
    {
        public List<E_Anio> obtener_Anios()
        {
            Listar_Anios_Response oRp;
            oRp = MvcApplication._Deserialize<Listar_Anios_Response>(MvcApplication._Servicio_Campania.Listar_Anios());
            return oRp.oListaAnios;
        }
        public List<E_Cob_MapAlicorp_tabla> Cobertura(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            Mps_Alicorp_Cobertura_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_Alicorp_Cobertura_Response>(
                    MvcApplication._Servicio_Maps.Consultar_CoberturaMapsAli_AASS(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_Cobertura;
        }
        public List<E_PuntoVentaMapa> PDVsCobertura(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            MapsAli_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<MapsAli_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Consultar_Cobertura_PDVS_MapsAli_AASS(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Exportar_Cobertura_MapAlicorp> Exportar_cobertura(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            Mps_Alicorp_Export_Cobertura_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_Alicorp_Export_Cobertura_Response>(
                    MvcApplication._Servicio_Maps.Exportar_Cobertura_MapsAli_AASS(MvcApplication._Serialize(oRequest)));
            return oRp.oExportarCobertura;
        }

        public List<M_OSA_MapAli> OSA(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            MpsAli_OSA_Response oRp;
            oRp = MvcApplication._Deserialize<MpsAli_OSA_Response>(
                    MvcApplication._Servicio_Maps.Consultar_OSA_MapsAli(MvcApplication._Serialize(oRequest)));
            return oRp.oOSA;
        }
        public List<E_PuntoVentaMapa> PDVsOSA(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            MapsAli_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<MapsAli_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Listar_PDVS_OSA_MapsAli(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Export_OSA_MapAli> Exportar_OSA(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            MapsAli_Export_OSA_Response oRp;
            oRp = MvcApplication._Deserialize<MapsAli_Export_OSA_Response>(
                    MvcApplication._Servicio_Maps.PDV_Exportar_OSA_MapsAli(
                        MvcApplication._Serialize(oRequest)
                    )
                );
            return oRp.oOSA;
        }

        public List<M_Tracking_precio_MapAli_AASS> TrackingPrecio(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            MapsAli_Tracking_precios_Response oRp;
            oRp = MvcApplication._Deserialize<MapsAli_Tracking_precios_Response>(
                    MvcApplication._Servicio_Maps.Consultar_TrackingPrecio_MapsAli(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_TrackingPrecio;
        }
        public List<E_PuntoVentaMapa> PDVsTrackingPrecio(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            MapsAli_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<MapsAli_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Listar_PDVS_TrackPrec_MapsAli(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Excel_Tracking_Precio_MapAli> Exportar_Tracking_precio(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            MapsAli_Export_TrackingPrecio_Response oRp;
            oRp = MvcApplication._Deserialize<MapsAli_Export_TrackingPrecio_Response>(
                    MvcApplication._Servicio_Maps.PDV_Exportar_TrackingPrecio_MapsAli(
                        MvcApplication._Serialize(oRequest)
                    )
                );
            return oRp.oTracking;
        }


        public List<M_SOD_MapAli_AASS> SOD(Consultar_RepMapsAlicorpGeneral_Request oRq)
        {
            MapsAli_SOD_Response oRp;

            oRp = MvcApplication._Deserialize<MapsAli_SOD_Response>(
                    MvcApplication._Servicio_Maps.Consultar_SOD_MapsAli(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oReporte_SOD;
        }
        public List<M_Quiebre_MapAli_AASS> Quiebre(Consultar_RepMapsAlicorpGeneral_Request oRq)
        {
            MapsAli_Quiebre_Response oRp;

            oRp = MvcApplication._Deserialize<MapsAli_Quiebre_Response>(
                    MvcApplication._Servicio_Maps.Consultar_Quiebre_MapsAli(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oReporte_Quiebre;
        }


        public E_PuntoVentaDatosMapa DetallePDV(Obtener_DatosPuntosVentaMapa_Request oRequest)
        {
            Obtener_DatosPuntosVentaMapa_Response oRp;
            oRp = MvcApplication._Deserialize<Obtener_DatosPuntosVentaMapa_Response>(MvcApplication._Servicio_Maps.Consultar_DatosPDV_MpAlicorp(MvcApplication._Serialize(oRequest)));
            return oRp.ptoVenta;
        }
        public List<E_FotoFC> FotoPDV(Consultar_RepMapsAlicorpGeneral_Request oRequest)
        {
            Mps_FC_Fotos_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_FC_Fotos_Response>(MvcApplication._Servicio_Maps.Consultar_Foto_pdv_MpAlicorp(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_Foto;
        }

    }

    public class Resquest_New_XPL_MapsAlicorp
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string campania { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int grupo { get; set; }

        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string ubigeo { get; set; }

        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int anio { get; set; }

        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int mes { get; set; }

        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public int periodo { get; set; }

        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public int giro { get; set; }

        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public string pdv { get; set; }

        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public int cluster { get; set; }

        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public int segmento { get; set; }

        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public int categoria { get; set; }

        [JsonProperty("_l", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_tipoactividad { get; set; }

        [JsonProperty("_m", NullValueHandling = NullValueHandling.Ignore)]
        public string producto { get; set; }

        [JsonProperty("_n", NullValueHandling = NullValueHandling.Ignore)]
        public int marca { get; set; }

        [JsonProperty("_o", NullValueHandling = NullValueHandling.Ignore)]
        public int opcion { get; set; }

        [JsonProperty("_p", NullValueHandling = NullValueHandling.Ignore)]
        public string parametros { get; set; }

        [JsonProperty("_q", NullValueHandling = NullValueHandling.Ignore)]
        public int elemento { get; set; }

        [JsonProperty("_r", NullValueHandling = NullValueHandling.Ignore)]
        public int tipoGiro { get; set; }
    }

    #region Filtros

    public class Response_New_XPL_Maps_Alicorp_Filtros
    {
        [JsonProperty("_a")]
        public List<M_Combo> Lista { get; set; }
    }
    public class New_XplMapsAlicorp_Filtro_Service
    {
        public List<M_Combo> Filtro(Resquest_New_XPL_MapsAlicorp oRq)
        {
            Response_New_XPL_Maps_Alicorp_Filtros oRp;

            oRp = MvcApplication._Deserialize<Response_New_XPL_Maps_Alicorp_Filtros>(
                    MvcApplication._Servicio_Maps.AlicorpAASSFiltros(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
    }

    #endregion
    #region <<< Request y Response >>>
    public class E_Parametros_MapAlicorp
    {
        [JsonProperty("a")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("b")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("c")]
        public String Anio { get; set; }
        [JsonProperty("d")]
        public String Mes { get; set; }
        [JsonProperty("e")]
        public int Periodo { get; set; }
        [JsonProperty("f")]
        public int cod_reporte { get; set; }
        [JsonProperty("g")]
        public String Cod_Ubieo { get; set; }
        [JsonProperty("h")]
        public int Cod_Tipo { get; set; }
        [JsonProperty("i")]
        public String Cod_Categoria { get; set; }
        [JsonProperty("j")]
        public int Cod_Cadena { get; set; }
        [JsonProperty("k")]
        public int Cod_Cluster { get; set; }
        [JsonProperty("l")]
        public int Tipo_General { get; set; }
        [JsonProperty("m")]
        public string Cod_Servicio { get; set; }
        [JsonProperty("n")]
        public string Cod_Canal { get; set; }
        [JsonProperty("o")]
        public string Cod_PDV { get; set; }
        [JsonProperty("p")]
        public int Id_Reportsplanning { get; set; }
        [JsonProperty("q")]
        public string Desc_Segmento { get; set; }
        [JsonProperty("r")]
        public string cod_pais { get; set; }
        [JsonProperty("s")]
        public string cod_sku { get; set; }
        [JsonProperty("t")]
        public string actividad { get; set; }
        [JsonProperty("u")]
        public int Tipo_Exhi { get; set; }
        [JsonProperty("v")]
        public int Cod_Elemento { get; set; }
        [JsonProperty("w")]
        public string Cod_Nivel { get; set; }
        [JsonProperty("x")]
        public string Parametros { get; set; }
        [JsonProperty("y")]
        public string Cod_Marca { get; set; }
        [JsonProperty("z")]
        public int opcion { get; set; }
        [JsonProperty("aa")]
        public string segmento { get; set; }
        [JsonProperty("ab")]
        public string giro { get; set; }
        [JsonProperty("ac")]
        public string cod_causal { get; set; }
        [JsonProperty("ad")]
        public string cod_tiporesultado { get; set; }
        [JsonProperty("ae")]
        public string cod_cluster { get; set; }
    }
    public class MapsAli_PDVS_Response
    {
        [JsonProperty("a")]
        public List<E_PuntoVentaMapa> oReporte_PDVS { get; set; }
    }
    #endregion

    #region  <<< Cobertura >>>
    public class Consultar_RepMapsAlicorpGeneral_Request
    {
        [JsonProperty("a")]
        public E_Parametros_MapAlicorp oParametros { get; set; }
    }
    public class Mps_Alicorp_Cobertura_Response
    {
        [JsonProperty("a")]
        public List<E_Cob_MapAlicorp_tabla> oReporte_Cobertura { get; set; }
    }
    public class E_Cob_MapAlicorp_tabla
    {
        [JsonProperty("a")]
        public int Cod_Cluster { get; set; }
        [JsonProperty("b")]
        public string Item { get; set; }
        [JsonProperty("c")]
        public string Cantidad { get; set; }
        [JsonProperty("d")]
        public string Peso { get; set; }
        [JsonProperty("e")]
        public string Representatividad { get; set; }
        [JsonProperty("f")]
        public List<E_Cob_MapAlicorp_tabla_hijo> hijo { get; set; }
        [JsonProperty("g")]
        public int tipo { get; set; }
    }
    public class E_Cob_MapAlicorp_tabla_hijo
    {
        [JsonProperty("a")]
        public int Cod_Cluster { get; set; }
        [JsonProperty("b")]
        public string Item { get; set; }
        [JsonProperty("c")]
        public string Cantidad { get; set; }
        [JsonProperty("d")]
        public string Peso { get; set; }
        [JsonProperty("e")]
        public string Representatividad { get; set; }
        [JsonProperty("f")]
        public int tipo { get; set; }

    }
    public class E_Exportar_Cobertura_MapAlicorp
    {
        [JsonProperty("a")]
        public string Cod_PDV { get; set; }
        [JsonProperty("b")]
        public string Nombre_PDV { get; set; }
        [JsonProperty("c")]
        public string Raz_Social { get; set; }
        [JsonProperty("d")]
        public string Direccion { get; set; }
        [JsonProperty("e")]
        public string Cadena { get; set; }
        [JsonProperty("f")]
        public string Pais { get; set; }
        [JsonProperty("g")]
        public string Departamento { get; set; }
        [JsonProperty("h")]
        public string Provincia { get; set; }
        [JsonProperty("i")]
        public string Distrito { get; set; }
        [JsonProperty("j")]
        public string Anio { get; set; }
        [JsonProperty("k")]
        public string Mes { get; set; }
        [JsonProperty("l")]
        public string Semana { get; set; }
        [JsonProperty("m")]
        public string Cluster { get; set; }

    }
    public class Mps_Alicorp_Export_Cobertura_Response
    {
        [JsonProperty("a")]
        public List<E_Exportar_Cobertura_MapAlicorp> oExportarCobertura { get; set; }
    }
    #endregion
    #region <<< OSA >>>
        public class MpsAli_OSA_Response
        {
            [JsonProperty("a")]
            public List<M_OSA_MapAli> oOSA { get; set; }
        }
        public class M_OSA_MapAli
        {
            [JsonProperty("a")]
            public string Tipo { get; set; }
            [JsonProperty("b")]
            public string Cod_Categoria { get; set; }
            [JsonProperty("c")]
            public string Categoria { get; set; }
            [JsonProperty("d")]
            public string Cod_SKU { get; set; }
            [JsonProperty("e")]
            public string Producto { get; set; }
            [JsonProperty("f")]
            public int Cantidad { get; set; }
            [JsonProperty("g")]
            public decimal? Porcentaje { get; set; }
            [JsonProperty("h")]
            public int Porcentaje_Int { get; set; }
            [JsonProperty("i")]
            public int Orden { get; set; }
            [JsonProperty("j")]
            public int Presencia { get; set; }
            [JsonProperty("k")]
            public string Marca { get; set; }
        }
        public class MapsAli_Export_OSA_Response
        {
            [JsonProperty("a")]
            public List<E_Export_OSA_MapAli> oOSA { get; set; }
        }
        public class E_Export_OSA_MapAli
        {
            [JsonProperty("a")]
            public string Cod_PDV { get; set; }
            [JsonProperty("b")]
            public string Nombre_PDV { get; set; }
            [JsonProperty("c")]
            public string Raz_Social { get; set; }
            [JsonProperty("d")]
            public string Direccion { get; set; }
            [JsonProperty("e")]
            public string Cadena { get; set; }
            [JsonProperty("f")]
            public string Sector { get; set; }
            [JsonProperty("g")]
            public string Pais { get; set; }
            [JsonProperty("h")]
            public string Departamento { get; set; }
            [JsonProperty("i")]
            public string Provincia { get; set; }
            [JsonProperty("j")]
            public string Distrito { get; set; }
            [JsonProperty("k")]
            public string Categoria { get; set; }
            [JsonProperty("l")]
            public string Marca { get; set; }
            [JsonProperty("m")]
            public string Cod_SKU { get; set; }
            [JsonProperty("n")]
            public string Producto { get; set; }
            [JsonProperty("o")]
            public string Anio { get; set; }
            [JsonProperty("p")]
            public string Mes { get; set; }
            [JsonProperty("q")]
            public string Semana { get; set; }
            [JsonProperty("r")]
            public string Presencia { get; set; }
            [JsonProperty("s")]
            public string causal { get; set; }
            [JsonProperty("t")]
            public string tipo_resultado { get; set; }
            [JsonProperty("u")]
            public string Cluster { get; set; }
        }
        public class E_OSA_Padre_MapAli
        {
            [JsonProperty("a")]
            public string Cod_Categoria { get; set; }
            [JsonProperty("b")]
            public string Categoria { get; set; }
            public List<E_OSA_Hijo_MapAli> Hijo { get; set; }
        }
        public class E_OSA_Hijo_MapAli
        {
            [JsonProperty("d")]
            public string Cod_Item { get; set; }
            [JsonProperty("e")]
            public string Item { get; set; }
            [JsonProperty("f")]
            public string Valor { get; set; }
        }
    #endregion
    #region <<< Tracking Precio >>>
        public class MapsAli_Tracking_precios_Response
        {
            [JsonProperty("a")]
            public List<M_Tracking_precio_MapAli_AASS> oReporte_TrackingPrecio { get; set; }
        }
        public class MapsAli_Export_TrackingPrecio_Response
        {
            [JsonProperty("a")]
            public List<E_Excel_Tracking_Precio_MapAli> oTracking { get; set; }
        }
        public class M_Tracking_precio_MapAli_AASS
        {
            [JsonProperty("a")]
            public string cod_categoria { get; set; }
            [JsonProperty("b")]
            public string nom_categoria { get; set; }
            [JsonProperty("c")]
            public string cod_sku { get; set; }
            [JsonProperty("d")]
            public string nom_sku { get; set; }
            [JsonProperty("e")]
            public decimal? prec_reventa { get; set; }
            [JsonProperty("f")]
            public decimal? prec_PDV { get; set; }
            [JsonProperty("g")]
            public decimal? prec_Costo { get; set; }
            [JsonProperty("h")]
            public int id { get; set; }
            [JsonProperty("i")]
            public decimal? prec_moda { get; set; }
            [JsonProperty("j")]
            public decimal? prec_max { get; set; }
            [JsonProperty("k")]
            public decimal? prec_min { get; set; }
            [JsonProperty("l")]
            public decimal? prec_promedio { get; set; }
            [JsonProperty("o")]
            public int cant_pdv { get; set; }

        }
        public class E_Excel_Tracking_Precio_MapAli
        {
            [JsonProperty("a")]
            public string Item { get; set; }
            [JsonProperty("b")]
            public string Cod_PDV { get; set; }
            [JsonProperty("c")]
            public string Nom_PDV { get; set; }
            [JsonProperty("d")]
            public string Direccion { get; set; }
            [JsonProperty("e")]
            public string Cod_Cadena { get; set; }
            [JsonProperty("f")]
            public string Nom_Cadena { get; set; }
            [JsonProperty("g")]
            public string Cod_Pais { get; set; }
            [JsonProperty("h")]
            public string Nom_Pais { get; set; }
            [JsonProperty("i")]
            public string cod_Departamento { get; set; }
            [JsonProperty("j")]
            public string Nom_Departamento { get; set; }
            [JsonProperty("k")]
            public string Cod_Provincia { get; set; }
            [JsonProperty("l")]
            public string Nom_Provincia { get; set; }
            [JsonProperty("m")]
            public string Cod_Distrito { get; set; }
            [JsonProperty("n")]
            public string Nom_Distrito { get; set; }
            [JsonProperty("o")]
            public string Cod_Sector { get; set; }
            [JsonProperty("p")]
            public string Nom_Sector { get; set; }
            [JsonProperty("q")]
            public string Anio { get; set; }
            [JsonProperty("r")]
            public string Mes { get; set; }
            [JsonProperty("s")]
            public string Semana { get; set; }
            [JsonProperty("t")]
            public string Fecha_Reg_Celda { get; set; }
            [JsonProperty("u")]
            public string Cod_Categoria { get; set; }
            [JsonProperty("v")]
            public string Nom_Categoria { get; set; }
            [JsonProperty("w")]
            public string Cod_Marca { get; set; }
            [JsonProperty("x")]
            public string Nom_Marca { get; set; }
            [JsonProperty("y")]
            public string Cod_SKU { get; set; }
            [JsonProperty("z")]
            public string Nom_Producto { get; set; }
            [JsonProperty("aa")]
            public string Precio_Regular { get; set; }
            [JsonProperty("ab")]
            public string Precio_Oferta { get; set; }
            [JsonProperty("ac")]
            public string Precio_PDV { get; set; }
            [JsonProperty("ad")]
            public string latitud { get; set; }
            [JsonProperty("ae")]
            public string longitud { get; set; }
            [JsonProperty("af")]
            public string color { get; set; }
        }
    #endregion
    #region <<< SOD >>>
        public class MapsAli_SOD_Response
        {
            [JsonProperty("a")]
            public List<M_SOD_MapAli_AASS> oReporte_SOD { get; set; }
        }
        public class M_SOD_MapAli_AASS
        {
            [JsonProperty("_a")]
            public int cat_id { get; set; }

            [JsonProperty("_b")]
            public string cat_descripcion { get; set; }

            [JsonProperty("_c")]
            public int sod_alicorp { get; set; }

            [JsonProperty("_d")]
            public int sod_competencia { get; set; }

            [JsonProperty("_e")]
            public int objetivo { get; set; }
        }
    #endregion
    #region <<< Quiebre >>>
        public class MapsAli_Quiebre_Response
        {
            [JsonProperty("a")]
            public List<M_Quiebre_MapAli_AASS> oReporte_Quiebre { get; set; }
        }
        public class M_Quiebre_MapAli_AASS
        {
            [JsonProperty("a")]
            public int Cod_Marca { get; set; }
            [JsonProperty("b")]
            public string Nom_Marca { get; set; }
            [JsonProperty("c")]
            public string Porcentaje { get; set; }
            [JsonProperty("d")]
            public string Cod_Categoria { get; set; }
            [JsonProperty("e")]
            public string Nom_Categoria { get; set; }
        }
    #endregion
}
