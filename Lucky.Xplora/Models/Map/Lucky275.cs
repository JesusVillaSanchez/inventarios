﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Map
{
    //public class Lucky275 { }
    #region Lucky 275 v1.0

    public class Resquest_New_XPL_SG_GPS
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string parametros { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int opcion { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public int vgie { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int vsupe { get; set; }
        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public string vequipo { get; set; }
        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public string vfec_inicio { get; set; }
        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public string vfec_fin { get; set; }
        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public int vcadena { get; set; }
        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public int vcanal { get; set; }
        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public string vciudad { get; set; }
        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public int vtipoperfil { get; set; }
        [JsonProperty("_l", NullValueHandling = NullValueHandling.Ignore)]
        public int vprograma { get; set; }
        [JsonProperty("_m", NullValueHandling = NullValueHandling.Ignore)]
        public int vgiro { get; set; }
        [JsonProperty("_n", NullValueHandling = NullValueHandling.Ignore)]
        public int vdex { get; set; }
    }

    public class E_Recorrido_Gie : AE_Tb_Recorrido_Gie_Excel
    {
        [JsonProperty("_a")]
        public int Cod_Correlativo { get; set; }
        [JsonProperty("_b")]
        public int Ids { get; set; }
        [JsonProperty("_c")]
        public string Nom_Supervisor { get; set; }
        [JsonProperty("_d")]
        public string Nom_Gie { get; set; }
        [JsonProperty("_e")]
        public string Latitud { get; set; }
        [JsonProperty("_f")]
        public string Longitud { get; set; }
        [JsonProperty("_g")]
        public string Fecha_Inicio { get; set; }
        [JsonProperty("_h")]
        public string Hora_Incio { get; set; }
        [JsonProperty("_i")]
        public string Fecha_Fin { get; set; }
        [JsonProperty("_j")]
        public string Hora_Fin { get; set; }
        [JsonProperty("_k")]
        public string Gestion { get; set; }
        [JsonProperty("_l")]
        public string Nom_PDV { get; set; }
        [JsonProperty("_m")]
        public string Estado { get; set; }
        [JsonProperty("_n")]
        public string No_Visita { get; set; }
        [JsonProperty("_o")]
        public int Orden_General { get; set; }
        [JsonProperty("_p")]
        public string pdv_cod { get; set; }
        [JsonProperty("_q")]
        public string Direccion { get; set; }
        [JsonProperty("_r")]
        public string Programa { get; set; }
        [JsonProperty("_s")]
        public string Dex { get; set; }
        [JsonProperty("_t")]
        public string Latitud_Fin { get; set; }
        [JsonProperty("_u")]
        public string Longitud_Fin { get; set; }

        [JsonProperty("_v")]
        public string Descripcion_GPS { get; set; }
    } 

    #region Filtros

    public class Response_New_XPL_SG_GPS_Filtros
    {
        [JsonProperty("_a")]
        public List<M_Combo> Lista { get; set; }
    }
    public class New_XplSgGps_Filtro_Service
    {
        public List<M_Combo> Filtro(Resquest_New_XPL_SG_GPS oRq)
        {
            Response_New_XPL_SG_GPS_Filtros oRp;

            oRp = MvcApplication._Deserialize<Response_New_XPL_SG_GPS_Filtros>(
                    MvcApplication._Servicio_Maps.Get_New_XPL_SG_GPS_Filtros(
                        MvcApplication._Serialize(oRq)
                    )
                );
            if (oRp.Lista.Count == 0) { oRp.Lista.Add(new M_Combo() { Value="0",Text=".::Todos::." }); }
            
            return oRp.Lista;
        }
    }
    
    #endregion

    #region Recorrido Gestor

    public class E_Tb_Recorrido_Gie : AE_Tb_Recorrido_Gie
    {
        [JsonProperty("_a")]
        public string Nom_Supervisor { get; set; }
        [JsonProperty("_b")]
        public List<E_Tb_Row_Recorrido_Gie> Fila { get; set; }
    }
    public class E_Tb_Row_Recorrido_Gie
    {
        [JsonProperty("_a")]
        public string Nom_Gie { get; set; }
        [JsonProperty("_b")]
        public List<E_Tb_Row_Det_Recorrido_Gie> Fila { get; set; }
        [JsonProperty("_c")]
        public int Cod_Gie { get; set; }
    }
    public class E_Tb_Row_Det_Recorrido_Gie
    {
        [JsonProperty("_a")]
        public int Item { get; set; }
        [JsonProperty("_b")]
        public string Latitud { get; set; }
        [JsonProperty("_c")]
        public string Longitud { get; set; }
        [JsonProperty("_d")]
        public string Fecha_Inicio { get; set; }
        [JsonProperty("_e")]
        public string Hora_Incio { get; set; }
        [JsonProperty("_f")]
        public string Fecha_Fin { get; set; }
        [JsonProperty("_g")]
        public string Hora_Fin { get; set; }
        [JsonProperty("_h")]
        public string Gestion { get; set; }
        [JsonProperty("_i")]
        public string Nom_PDV { get; set; }
        [JsonProperty("_j")]
        public string Estado { get; set; }
        [JsonProperty("_k")]
        public string Motivo { get; set; }
        [JsonProperty("_l")]
        public int cod_general { get; set; }
        [JsonProperty("_m")]
        public string direccion { get; set; }
        [JsonProperty("_n")]
        public string Pdv_Cod { get; set; }
        [JsonProperty("_p")]
        public string pdv_cod { get; set; }
        [JsonProperty("_o")]
        //public List<string> Fotos { get; set; }
        public List<E_Tb_Row_Det_Recorrido_Gie_Foto> Fotos { get; set; }
        [JsonProperty("_q")]
        public string cadena { get; set; }
        [JsonProperty("_r")]
        public string Descripcion_GPS { get; set; }
        
    }
    public class E_Tb_Row_Det_Recorrido_Gie_Foto
    {
        [JsonProperty("_a")]
        public string foto { get; set; }

        [JsonProperty("_b")]
        public string fecha { get; set; }

        [JsonProperty("_c")]
        public string hora { get; set; }
    }

    public class Response_New_SG_GPS_RecorridoGie
    {
        [JsonProperty("_a")]
        public E_Tb_Recorrido_Gie obj { get; set; }
    }

    /// <summary>
    /// Creado por : rcontreras
    /// Fecha       : 27/07/2015
    /// </summary>
    public class Response_New_SG_GPS_RecorridoGie_Excel
    {
        [JsonProperty("_a")]
        public List<E_Recorrido_Gie> lista { get; set; }
    }

    public abstract class AE_Tb_Recorrido_Gie
    {

        public E_Tb_Recorrido_Gie Recorrido(Resquest_New_XPL_SG_GPS oRq)
        {
            Response_New_SG_GPS_RecorridoGie oRp = MvcApplication._Deserialize<Response_New_SG_GPS_RecorridoGie>(
                    MvcApplication._Servicio_Maps.Get_New_XPL_SG_GPS_Recorrido_Gie(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.obj;
        }
    }

    public abstract class AE_Tb_Recorrido_Gie_Excel
    {
        public List<E_Recorrido_Gie> downloadExcel(Resquest_New_XPL_SG_GPS oRq)
        {
            Response_New_SG_GPS_RecorridoGie_Excel oRp = MvcApplication._Deserialize<Response_New_SG_GPS_RecorridoGie_Excel>(
                    MvcApplication._Servicio_Maps.Get_New_XPL_SG_GPS_Recorrido_Gie_Excel(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.lista;
        }

        public List<E_Recorrido_Gie> downloadExcelxSuper(Resquest_New_XPL_SG_GPS oRq)
        {
            Response_New_SG_GPS_RecorridoGie_Excel oRp = MvcApplication._Deserialize<Response_New_SG_GPS_RecorridoGie_Excel>(
                    MvcApplication._Servicio_Maps.Get_New_XPL_SG_GPS_Recorrido_Gie_Excel_x_Super(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.lista;
        }
    }

    #endregion

    #endregion

    #region Seguimiento v2.0
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-09
    /// </summary>
    public class Seguimiento : ASeguimiento { }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-11
    /// </summary>
    public class SeguimientoGpsSupervisor
    {
        [JsonProperty("_a")]
        public int sup_id { get; set; }

        [JsonProperty("_b")]
        public string sup_nombre { get; set; }

        [JsonProperty("_c")]
        public List<SeguimientoGpsSupervisorGie> gie { get; set; }
    }

    #region Seguimiento_v2_Cantidad_Relevo
    /// <summary>
    /// Autor: yCueva
    /// Fecha: 2017-03-13
    /// </summary>
    public class Seguimiento_v2_Cantidad_Relevo
    {
        [JsonProperty("_a")]
        public int gestor { get; set; }

        [JsonProperty("_b")]
        public string pdv { get; set; }

        [JsonProperty("_c")]
        public string estado { get; set; }

        [JsonProperty("_d")]
        public string fecha { get; set; }

        [JsonProperty("_e")]
        public int precio { get; set; }

        [JsonProperty("_f")]
        public int presencia { get; set; }

        [JsonProperty("_g")]
        public int coditt { get; set; }

        [JsonProperty("_h")]
        public int foto { get; set; }
        [JsonProperty("_i")]
        public int visibilidad { get; set; }
        [JsonProperty("_j")]
        public int impulso { get; set; }
        [JsonProperty("_k")]
        public int stockout { get; set; }
        [JsonProperty("_l")]
        public int participacion { get; set; }
    }
    #endregion

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-11
    /// </summary>
    /// 

    public class SeguimientoDescargaExcel
    {
        [JsonProperty("_aa")]
        public int correlativo { get; set; }

        [JsonProperty("_a")]
        public string fec_inicio { get; set; }

        [JsonProperty("_b")]
        public string hora_inicio { get; set; }

        [JsonProperty("_c")]
        public string fec_fin { get; set; }

        [JsonProperty("_d")]
        public string hora_fin { get; set; }

        [JsonProperty("_e")]
        public int gestion { get; set; }

        [JsonProperty("_f")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_g")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_h")]
        public string pdv_direccion { get; set; }

        [JsonProperty("_i")]
        public string latitud_pdv { get; set; }

        [JsonProperty("_j")]
        public string longitud_pdv { get; set; }

        [JsonProperty("_k")]
        public string sup_nombre { get; set; }

        [JsonProperty("_l)")]
        public string gestor {get;set;}

        [JsonProperty("_m")]
        public string distancia { get; set; }

        [JsonProperty("_n")]
        public string estado { get; set; }

        [JsonProperty("_o")]
        public string motivo { get; set; }

    }

    public class SeguimientoGpsSupervisorGie
    {
        [JsonProperty("_a")]
        public int gie_id { get; set; }

        [JsonProperty("_b")]
        public string gie_nombre { get; set; }

        [JsonProperty("_c")]
        public List<SeguimientoGpsSupervisorGiePdv> pdv { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-11
    /// </summary>
    public class SeguimientoGpsSupervisorGiePdv
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_c")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_d")]
        public string direccion { get; set; }

        [JsonProperty("_e")]
        public string programa { get; set; }

        [JsonProperty("_f")]
        public string dex_descripcion { get; set; }

        [JsonProperty("_g")]
        public string fecha_inicio { get; set; }

        [JsonProperty("_h")]
        public string hora_inicio { get; set; }

        [JsonProperty("_i")]
        public string fecha_fin { get; set; }

        [JsonProperty("_j")]
        public string hora_fin { get; set; }

        [JsonProperty("_k")]
        public string latitud_inicio { get; set; }

        [JsonProperty("_l")]
        public string longitud_inicio { get; set; }

        [JsonProperty("_m")]
        public int gestion { get; set; }

        [JsonProperty("_n")]
        public string estado { get; set; }

        [JsonProperty("_o")]
        public string no_visita { get; set; }

        [JsonProperty("_p")]
        public List<SeguimientoGpsSupervisorGiePdvFoto> fotografico { get; set; }

        [JsonProperty("_q")]
        public string distancia { get; set; }

        [JsonProperty("_r")]
        public string latitud_pdv { get; set; }

        [JsonProperty("_s")]
        public string longitud_pdv { get; set; }

        [JsonProperty("_t")]
        public string foto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-11
    /// </summary>
    public class SeguimientoGpsSupervisorGiePdvFoto
    {
        [JsonProperty("_a")]
        public Int64 fot_id { get; set; }

        [JsonProperty("_b")]
        public string foto { get; set; }

        [JsonProperty("_c")]
        public string fecha { get; set; }

        [JsonProperty("_d")]
        public string hora { get; set; }

        [JsonProperty("_e")]
        public int validado { get; set; }

        [JsonProperty("_f")]
        public int CatId { get; set; }

        [JsonProperty("_g")]
        public string CatDescripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-24
    /// </summary>
    public class SeguimientoGpsValidaFoto
    {
        [JsonProperty("_a")]
        public int cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-09
    /// </summary>
    public class Parametro
    {
        public Parametro() { }

        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-09
    /// </summary>
    public class Listado
    {
        public Listado() { }

        [JsonProperty("_a")]
        public string valor { get; set; }

        [JsonProperty("_b")]
        public string texto { get; set; }

        [JsonProperty("_c")]
        public bool seleccion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-09
    /// </summary>
    public abstract class ASeguimiento
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-09
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Listado> Lista(Parametro oRq)
        {
            return MvcApplication._Deserialize<ResponseSeguimientoLista>(
                    MvcApplication._Servicio_Maps.Seguimiento(
                        MvcApplication._Serialize(new RequestSeguimientoParametro() { Parametro = oRq })
                    )
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-12
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<SeguimientoGpsSupervisor> Seguimiento(Parametro oRq)
        {
            return MvcApplication._Deserialize<ResponseSeguimientoGpsSupervisorLista>(
                    MvcApplication._Servicio_Maps.Seguimiento(
                        MvcApplication._Serialize(new RequestSeguimientoParametro() { Parametro = oRq })
                    )
                ).Lista;
        }

        #region Seguimiento_v2_Cantidad_Relevo
        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-03-13
        /// </summary>
        public List<Seguimiento_v2_Cantidad_Relevo> l_Seguimiento_v2_Cantidad_Relevo(Parametro oRq)
        {
            return MvcApplication._Deserialize<ResponseSeguimiento_v2_Cantidad_Relevo>(
                    MvcApplication._Servicio_Maps.Seguimiento_v2_Cantidad_Relevo(
                        MvcApplication._Serialize(new RequestSeguimientoParametro() { Parametro = oRq })
                    )
                ).Lista;
        }
        #endregion

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-23
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public SeguimientoGpsValidaFoto Valida(Parametro oRq)
        {
            return MvcApplication._Deserialize<ResponseSeguimientoGpsValidaFoto>(
                    MvcApplication._Servicio_Maps.Seguimiento(
                        MvcApplication._Serialize(new RequestSeguimientoParametro() { Parametro = oRq })
                    )
                ).valida;
        }
    }



    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-09
    /// </summary>
    public class RequestSeguimientoParametro
    {
        [JsonProperty("_a")]
        public Parametro Parametro { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-09
    /// </summary>
    public class ResponseSeguimientoLista
    {
        [JsonProperty("_a")]
        public List<Listado> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-09
    /// </summary>
    public class ResponseSeguimientoGpsSupervisorLista
    {
        [JsonProperty("_a")]
        public List<SeguimientoGpsSupervisor> Lista { get; set; }
    }

    /// <summary>
    /// Autor: yCueva
    /// Fecha: 2017-03-13
    /// </summary>
    public class ResponseSeguimiento_v2_Cantidad_Relevo
    {
        [JsonProperty("_a")]
        public List<Seguimiento_v2_Cantidad_Relevo> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-24
    /// </summary>
    public class ResponseSeguimientoGpsValidaFoto
    {
        [JsonProperty("_a")]
        public SeguimientoGpsValidaFoto valida { get; set; }
    }
    #endregion

    
    #endregion
}