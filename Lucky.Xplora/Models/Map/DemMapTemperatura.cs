﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Map
{
    public class DemMapTemperatura
    {
        public List<E_Mtemperatura> Mapas(Request_Maps_Temperatura oRq)
        {
            Response_Maps_Temperatura oRp;

            oRp = MvcApplication._Deserialize<Response_Maps_Temperatura>(
                    MvcApplication._Servicio_Maps.fnMapasTemperatura(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.mapas;
        }
    }

    #region Entity
    public class E_Mtemperatura
    {
        [JsonProperty("_a")]
        public int Cod_Periodo { get; set; }
        [JsonProperty("_b")]
        public List<E_Mtemperatura_coordenadas> Lista { get; set; }
    }
    public class E_Mtemperatura_coordenadas
    {
        [JsonProperty("_a")]
        public String Cod_Pdv { get; set; }
        [JsonProperty("_b")]
        public String Pdv_Name { get; set; }
        [JsonProperty("_c")]
        public String Direccion { get; set; }
        [JsonProperty("_d")]
        public String Latitud { get; set; }
        [JsonProperty("_e")]
        public String Longitud { get; set; }
    }
    #endregion
    #region Request - Response
    public class Request_Maps_Temperatura
    {
        [JsonProperty("_a")]
        public string vperiodo { get; set; }
        [JsonProperty("_b")]
        public string vproducto { get; set; }
        [JsonProperty("_c")]
        public string vcategoria { get; set; }
        [JsonProperty("_d")]
        public int vmarca { get; set; }
    }
    public class Response_Maps_Temperatura 
    {
        [JsonProperty("_a")]
        public List<E_Mtemperatura> mapas { get; set; }

    }

#endregion

}