﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Map
{
    public class List_Reportes : AList_Reportes
    {
        [JsonProperty("_a")]
        public int Codigo { get; set; }
        [JsonProperty("_b")]
        public string Nom_Reporte { get; set; }
        [JsonProperty("_c")]
        public string Nom_Corto { get; set; }
        [JsonProperty("_d")]
        public string Color { get; set; }
        [JsonProperty("_e")]
        public string Icon { get; set; }
    }
    public abstract class AList_Reportes
    {
        /*public List<List_Reportes> Lista(Resquest_Maps_List_Reportes oRq)
        {
            Response_Maps_List_Reportes oRp;

            oRp = MvcApplication._Deserialize<Response_Maps_List_Reportes>(
                    MvcApplication._Servicio_Maps.Maps_List_Reportes_Equipo(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }*/
    }
    public class Resquest_Maps_List_Reportes
    {
        [JsonProperty("_a")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("_b")]
        public int Cod_Proyecto { get; set; }
    }
    public class Response_Maps_List_Reportes
    {
        [JsonProperty("_a")]
        public List<List_Reportes> Obj { get; set; }

    }

}