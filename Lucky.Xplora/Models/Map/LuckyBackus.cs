﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace Lucky.Xplora.Models.Map
{
    /// <summary>
    /// Autor   : Jsulla
    /// Fecha   : 24/08/2015
    /// </summary>
    public class LuckyBackus
    {
        public List<E_Cob_MapBK_tabla> Cobertura(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Cobertura_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Cobertura_Response>(
                    MvcApplication._Servicio_Maps.Consultar_coberturaMpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_Cobertura;
        }
        public List<E_PuntoVentaMapa> PDVsCobertura(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Consultar_PDVS_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Exportar_Cobertura_MapBK> Exportar_cobertura(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Export_Cobertura_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Export_Cobertura_Response>(
                    MvcApplication._Servicio_Maps.Exportar_Cobertura_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oCobertura;
        }
        public List<M_Presencia_MapBK> PresenciaProducto(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Presencia_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Presencia_Response>(
                    MvcApplication._Servicio_Maps.Consul_Presencia_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oPresencia;          
        }
        public List<E_PuntoVentaMapa> PDVsPresenciaProducto(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Consul_PDVS_Presencia_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Export_Presencia_MapBK> Exportar_PresenciaProducto(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Export_Presencia_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Export_Presencia_Response>(
                    MvcApplication._Servicio_Maps.Exportar_Presencia_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oExport_Pres;       
        }
        public List<E_Presencia_Padre_MapBK> PresenciaProductoPDV(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Presencia_x_PDV_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Presencia_x_PDV_Response>(
                    MvcApplication._Servicio_Maps.Consul_Presencia_x_PDV_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oPres_PDV;
        }
        public List<Presencia_Elemento_Visibilidad> PresenciaVisibilidad(Request_Elem_Visi_MapBK oRq)
        {
            Response_Elem_Visi_MapBK oRp;
            oRp = MvcApplication._Deserialize<Response_Elem_Visi_MapBK>(
                    MvcApplication._Servicio_Maps.BL_Elem_Visi_MapBK(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
        public List<Presencia_Elemento_Visibilidad_Exporta> Exportar_PresenciaVisibilidad(Request_Elem_Visi_Export_MapBK oRq)
        {
            Response_Elem_Visi_Export_MapBK oRp;

            oRp = MvcApplication._Deserialize<Response_Elem_Visi_Export_MapBK>(
                    MvcApplication._Servicio_Maps.BL_Elem_Visi_Export_MapBK(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
        public List<E_PuntoVentaMapa> PDVsPresenciaVisibilidad(Request_Elem_Visi_Export_MapBK oRq)
        {
            Mps_BK_PDVS_Response oRp;

            oRp = MvcApplication._Deserialize<Mps_BK_PDVS_Response>(
                    MvcApplication._Servicio_Maps.BL_Elem_Visi_PDV_MapBK(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oReporte_PDVS;
        }
        public List<M_Tracking_precio_MapBK> TrackingPrecio(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Tracking_precios_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Tracking_precios_Response>(
                    MvcApplication._Servicio_Maps.Consul_TrackingPrecio_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_TrackingPrecio;
        }
        public List<E_PuntoVentaMapa> PDVsTrackingPrecio(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Consul_PDVS_TrackPrec_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Exportar_Tracking_MapBK> Exportar_Tracking_precio(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Export_TrackingPrecio_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Export_TrackingPrecio_Response>(
                    MvcApplication._Servicio_Maps.Exportar_TrackingPrecio_MpBackus(
                        MvcApplication._Serialize(oRequest)
                    )
                );
            return oRp.oTracking;        
        }
        public List<E_Tracking_MapBK_tabla> TrackingPrecioPDV(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_TrackingPDVCatg_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_TrackingPDVCatg_Response>(
                    MvcApplication._Servicio_Maps.Consul_TrackingporPDV_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte;
        }
        public List<M_Combo> ListaFitroSod(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Filtros_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Filtros_Response>(
                    MvcApplication._Servicio_Maps.Consul_Filtros_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oFiltros;
        }
        public List<M_ReporteSOD_MapBK> ShareOfDisplay(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_SOD_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_SOD_Response>(MvcApplication._Servicio_Maps.Consul_SOD_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_SOD;
        }
        public List<M_Stock_out_MapBK> StockOut(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Stock_out_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Stock_out_Response>(MvcApplication._Servicio_Maps.Consul_Stock_Out_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_Stock_out;
        }
        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 28/08/15
        /// </summary>
        /// <param name="oRequest"></param>
        /// <returns></returns>
        public List<E_PuntoVentaMapa> PDVsStockOut(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_PDVS_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_PDVS_Response>(
                    MvcApplication._Servicio_Maps.Consul_PDVS_Stock_Out_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_PDVS;
        }
        public List<E_Export_Stock_Out_MapBK> Exportar_Stock_Out(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Export_Stock_Out_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Export_Stock_Out_Response>(
                    MvcApplication._Servicio_Maps.Exportar_Stock_Out_MpBackus(
                        MvcApplication._Serialize(oRequest)
                    )
                );
            return oRp.oStock_Out;  
        }
        public List<E_Tracking_MapBK_tabla> StockOutPDV(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Stock_x_PDV_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Stock_x_PDV_Response>(MvcApplication._Servicio_Maps.Consul_Stock_Out_x_PDV_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_StockPDV;
        }
        public List<E_Reportes_StockOut_MapBK> StockOutRepOperativo(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_RepOpe_StockOut_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_RepOpe_StockOut_Response>(MvcApplication._Servicio_Maps.Consul_RepOpe_StockOut_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oRepOpe_StockOut;
        }
        public List<E_Reportes_RangPDV_StockOut_MapBK> StockOutRepRangPDVOperativo(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_RangPDV_StockOut_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_RangPDV_StockOut_Response>(MvcApplication._Servicio_Maps.Consul_RangPDV_StockOut_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oRangPDV_StockOut;
        }
        public List<Ranking_Sku> StockOutRepRangSKUOperativo(Request_RankingSku_Anio_TipoQuiebre_Categoria oRequest)
        {
            Response_RankingSku_Anio_TipoQuiebre_Categoria oRp;
            oRp = MvcApplication._Deserialize<Response_RankingSku_Anio_TipoQuiebre_Categoria>(MvcApplication._Servicio_Maps.RankingSku_Anio_TipoQuiebre_Categoria(MvcApplication._Serialize(oRequest)));
            return oRp.Lista;
        }
        public List<E_Anio> obtener_Anios()
        {
            Listar_Anios_Response oRp;
            oRp = MvcApplication._Deserialize<Listar_Anios_Response>(MvcApplication._Servicio_Campania.Listar_Anios());
            return oRp.oListaAnios;         
        }
        public E_PuntoVentaDatosMapa DetallePDV(Obtener_DatosPuntosVentaMapa_Request oRequest)
        {
            Obtener_DatosPuntosVentaMapa_Response oRp;
            oRp = MvcApplication._Deserialize<Obtener_DatosPuntosVentaMapa_Response>(MvcApplication._Servicio_Maps.Consul_DatosPDV_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.ptoVenta;
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 28/08/15
        /// </summary>
        /// <param name="oRequest"></param>
        /// <returns></returns>
        public List<E_Foto> FotoPDV(Consultar_RepMapsBKGeneral_Request oRequest)
        {
            Mps_BK_Fotos_Response oRp;
            oRp = MvcApplication._Deserialize<Mps_BK_Fotos_Response>(MvcApplication._Servicio_Maps.Consul_Foto_pdv_MpBackus(MvcApplication._Serialize(oRequest)));
            return oRp.oReporte_Foto;
        }
    }
    #region  <<< Cobertura >>>
    public class E_Cob_MapBK_tabla
    {
        [JsonProperty("a")]
        public int Cod_Cluster { get; set; }
        [JsonProperty("b")]
        public string Item { get; set; }
        [JsonProperty("c")]
        public string Cantidad { get; set; }
        [JsonProperty("d")]
        public string Peso { get; set; }
        [JsonProperty("e")]
        public string Representatividad { get; set; }
        [JsonProperty("f")]
        public List<E_Cob_MapBK_tabla_hijo> hijo { get; set; }
        [JsonProperty("g")]
        public int tipo { get; set; }
    }
    public class E_Cob_MapBK_tabla_hijo
    {
        [JsonProperty("a")]
        public int Cod_Cluster { get; set; }
        [JsonProperty("b")]
        public string Item { get; set; }
        [JsonProperty("c")]
        public string Cantidad { get; set; }
        [JsonProperty("d")]
        public string Peso { get; set; }
        [JsonProperty("e")]
        public string Representatividad { get; set; }
        [JsonProperty("f")]
        public int tipo { get; set; }

    }
    public class E_Exportar_Cobertura_MapBK
    {
        [JsonProperty("a")]
        public string Cod_PDV { get; set; }
        [JsonProperty("b")]
        public string Nombre_PDV { get; set; }
        [JsonProperty("c")]
        public string Raz_Social { get; set; }
        [JsonProperty("d")]
        public string Direccion { get; set; }
        [JsonProperty("e")]
        public string Cadena { get; set; }
        [JsonProperty("f")]
        public string Pais { get; set; }
        [JsonProperty("g")]
        public string Departamento { get; set; }
        [JsonProperty("h")]
        public string Provincia { get; set; }
        [JsonProperty("i")]
        public string Distrito { get; set; }
        [JsonProperty("j")]
        public string Anio { get; set; }
        [JsonProperty("k")]
        public string Mes { get; set; }
        [JsonProperty("l")]
        public string Semana { get; set; }
        [JsonProperty("m")]
        public string Cluster { get; set; }

    }


    #endregion
    #region <<< PDV >>>
    public class E_PuntoVentaMapa
    {
        [JsonProperty("a")]
        public string codPuntoVenta { get; set; }
        [JsonProperty("e")]
        public string color { get; set; }
        [JsonProperty("c")]
        public string latitud { get; set; }
        [JsonProperty("d")]
        public string longitud { get; set; }
        [JsonProperty("b")]
        public string nombrePuntoVenta { get; set; }
        [JsonProperty("f")]
        public string segmento { get; set; }
        [JsonProperty("g")]
        public string node { get; set; }
        [JsonProperty("h")]
        public string nom_node { get; set; }
    }

    #endregion
    #region <<< Presencia Producto >>>
    public class M_Presencia_MapBK
    {
        [JsonProperty("a")]
        public string Tipo { get; set; }
        [JsonProperty("b")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("c")]
        public string Categoria { get; set; }
        [JsonProperty("d")]
        public string Cod_SKU { get; set; }
        [JsonProperty("e")]
        public string Producto { get; set; }
        [JsonProperty("f")]
        public int Cantidad { get; set; }
        [JsonProperty("g")]
        public decimal? Porcentaje { get; set; }
        [JsonProperty("h")]
        public int Porcentaje_Int { get; set; }
    }
    public class E_Export_Presencia_MapBK
    {
        [JsonProperty("a")]
        public string Cod_PDV { get; set; }
        [JsonProperty("b")]
        public string Nombre_PDV { get; set; }
        [JsonProperty("c")]
        public string Raz_Social { get; set; }
        [JsonProperty("d")]
        public string Direccion { get; set; }
        [JsonProperty("e")]
        public string Cadena { get; set; }
        [JsonProperty("f")]
        public string Sector { get; set; }
        [JsonProperty("g")]
        public string Pais { get; set; }
        [JsonProperty("h")]
        public string Departamento { get; set; }
        [JsonProperty("i")]
        public string Provincia { get; set; }
        [JsonProperty("j")]
        public string Distrito { get; set; }
        [JsonProperty("k")]
        public string Categoria { get; set; }
        [JsonProperty("l")]
        public string Marca { get; set; }
        [JsonProperty("m")]
        public string Cod_SKU { get; set; }
        [JsonProperty("n")]
        public string Producto { get; set; }
        [JsonProperty("o")]
        public string Anio { get; set; }
        [JsonProperty("p")]
        public string Mes { get; set; }
        [JsonProperty("q")]
        public string Semana { get; set; }
    }
    public class E_Presencia_Padre_MapBK
    {
        [JsonProperty("a")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("b")]
        public string Categoria { get; set; }
        public List<E_Presencia_Hijo_MapBK> Hijo { get; set; }
    }
    public class E_Presencia_Hijo_MapBK
    {
        [JsonProperty("d")]
        public string Cod_Item { get; set; }
        [JsonProperty("e")]
        public string Item { get; set; }
        [JsonProperty("f")]
        public string Valor { get; set; }
    }
    #endregion
    #region <<< Presencia Visibilidad >>>
        public class Request_Elem_Visi_MapBK
        {
            [JsonProperty("a")]
            public string TipoUbigeo { get; set; }
            [JsonProperty("b")]
            public string cod_ubieo { get; set; }
            [JsonProperty("c")]
            public string cod_pais { get; set; }
            [JsonProperty("d")]
            public string cod_cadena { get; set; }
            [JsonProperty("e")]
            public string cod_perido { get; set; }
            [JsonProperty("f")]
            public string cod_categoria { get; set; }
            [JsonProperty("g")]
            public string cod_marca { get; set; }
            [JsonProperty("h")]
            public string cod_tipo_canal { get; set; }
        }
        public class Presencia_Elemento_Visibilidad
        {
            [JsonProperty("a")]
            public string cod_Elemento { get; set; }

            [JsonProperty("b")]
            public string Elemento { get; set; }

            [JsonProperty("c")]
            public string PDV_Totales { get; set; }

            [JsonProperty("d")]
            public string PDV_Con_Presencia { get; set; }

            [JsonProperty("e")]
            public string Presencia { get; set; }
        }
        public class Parametros_Presencia_Elemento_Visibilidad
        {
            [JsonProperty("a")]
            public string TipoUbigeo { get; set; }
            [JsonProperty("b")]
            public string cod_ubieo { get; set; }
            [JsonProperty("c")]
            public string cod_pais { get; set; }
            [JsonProperty("d")]
            public string cadena { get; set; }
            [JsonProperty("e")]
            public string id_reportsplanning { get; set; }
            [JsonProperty("f")]
            public string cod_categoria { get; set; }
            [JsonProperty("g")]
            public string cod_marca { get; set; }
            [JsonProperty("h")]
            public string cod_tipo_canal { get; set; }
            [JsonProperty("i")]
            public string cod_Elemento { get; set; }
            [JsonProperty("j")]
            public int opcion { get; set; }
        }
        public class Presencia_Elemento_Visibilidad_Exporta
        {
            [JsonProperty("a")]
            public string cod_pdv { get; set; }
            [JsonProperty("b")]
            public string nom_pdv { get; set; }
            [JsonProperty("c")]
            public string raz_social { get; set; }
            [JsonProperty("d")]
            public string direccion { get; set; }
            [JsonProperty("e")]
            public string nom_cadena { get; set; }
            [JsonProperty("f")]
            public string Sector { get; set; }
            [JsonProperty("g")]
            public string Name_Country { get; set; }
            [JsonProperty("h")]
            public string Name_dpto { get; set; }
            [JsonProperty("i")]
            public string Name_City { get; set; }
            [JsonProperty("j")]
            public string District { get; set; }
            [JsonProperty("k")]
            public string fec_reg_cel { get; set; }
            [JsonProperty("l")]
            public string nom_categoria { get; set; }
            [JsonProperty("m")]
            public string cod_material { get; set; }
            [JsonProperty("n")]
            public string nom_material { get; set; }
            [JsonProperty("ñ")]
            public string cantidad { get; set; }
            [JsonProperty("o")]
            public string anio { get; set; }
            [JsonProperty("p")]
            public string mes { get; set; }

        }
    #endregion
    #region <<< Tracking de Precio >>>
        public class M_Tracking_precio_MapBK
        {
            [JsonProperty("a")]
            public string cod_categoria { get; set; }
            [JsonProperty("b")]
            public string nom_categoria { get; set; }
            [JsonProperty("c")]
            public string cod_sku { get; set; }
            [JsonProperty("d")]
            public string nom_sku { get; set; }
            [JsonProperty("e")]
            public decimal? prec_reventa { get; set; }
            [JsonProperty("f")]
            public decimal? prec_PDV { get; set; }
            [JsonProperty("g")]
            public decimal? prec_Costo { get; set; }
            [JsonProperty("h")]
            public int id { get; set; }

        }
        public class E_Exportar_Tracking_MapBK
        {
            [JsonProperty("a")]
            public string Cod_PDV { get; set; }
            [JsonProperty("b")]
            public string Nombre_PDV { get; set; }
            [JsonProperty("c")]
            public string Direccion { get; set; }
            [JsonProperty("d")]
            public string Cadena { get; set; }
            [JsonProperty("e")]
            public string Sector { get; set; }
            [JsonProperty("f")]
            public string Pais { get; set; }
            [JsonProperty("g")]
            public string Departamento { get; set; }
            [JsonProperty("h")]
            public string Provincia { get; set; }
            [JsonProperty("i")]
            public string Distrito { get; set; }
            [JsonProperty("j")]
            public string Categoria { get; set; }
            [JsonProperty("k")]
            public string Marca { get; set; }
            [JsonProperty("l")]
            public string SKU { get; set; }
            [JsonProperty("m")]
            public string Nombre_Producto { get; set; }
            [JsonProperty("n")]
            public string Precio_Reventa { get; set; }
            [JsonProperty("o")]
            public string Precio_PVP { get; set; }
            [JsonProperty("p")]
            public string Precio_Costo { get; set; }
            [JsonProperty("q")]
            public string Fecha_reg_celda { get; set; }
            [JsonProperty("r")]
            public string Anio { get; set; }
            [JsonProperty("s")]
            public string Mes { get; set; }
            [JsonProperty("t")]
            public string Semana { get; set; }
        }
        public class E_Tracking_MapBK_tabla
        {
            [JsonProperty("a")]
            public string cod_categoria { get; set; }
            [JsonProperty("b")]
            public string nom_categoria { get; set; }
            [JsonProperty("c")]
            public int id { get; set; }
            [JsonProperty("d")]
            public List<E_Tracking_MapBK_tabla_hijo> hijo { get; set; }
        }
        public class E_Tracking_MapBK_tabla_hijo
        {
            [JsonProperty("a")]
            public string cod_sku { get; set; }
            [JsonProperty("b")]
            public string nom_sku { get; set; }
            [JsonProperty("c")]
            public decimal? prec_reventa { get; set; }
            [JsonProperty("d")]
            public decimal? prec_PDV { get; set; }
            [JsonProperty("e")]
            public decimal? prec_Costo { get; set; }

        }
    #endregion
    #region <<< Share of Display >>>
        public class M_ReporteSOD_MapBK
        {
            [JsonProperty("a")]
            public int Cod_Marca { get; set; }
            [JsonProperty("b")]
            public string Nom_Marca { get; set; }
            [JsonProperty("c")]
            public string Porcentaje { get; set; }
        } 
    #endregion
    #region <<< Stock Out >>>
        public class M_Stock_out_MapBK
        {
            [JsonProperty("a")]
            public string Cod_Categoria { get; set; }
            [JsonProperty("b")]
            public string Categoria { get; set; }
            [JsonProperty("c")]
            public string Cod_SKU { get; set; }
            [JsonProperty("d")]
            public string Producto { get; set; }
            [JsonProperty("e")]
            public int Cantidad { get; set; }
            [JsonProperty("f")]
            public decimal? Porcentaje { get; set; }
            [JsonProperty("g")]
            public int Porcentaje_Int { get; set; }
        }
        public class E_Export_Stock_Out_MapBK
        {
            [JsonProperty("a")]
            public string Cod_PDV { get; set; }
            [JsonProperty("b")]
            public string Nombre_PDV { get; set; }
            [JsonProperty("c")]
            public string Raz_Social { get; set; }
            [JsonProperty("d")]
            public string Direccion { get; set; }
            [JsonProperty("e")]
            public string Cadena { get; set; }
            [JsonProperty("f")]
            public string Sector { get; set; }
            [JsonProperty("g")]
            public string Pais { get; set; }
            [JsonProperty("h")]
            public string Departamento { get; set; }
            [JsonProperty("i")]
            public string Provincia { get; set; }
            [JsonProperty("j")]
            public string Distrito { get; set; }
            [JsonProperty("k")]
            public string Fec_reg_cel { get; set; }
            [JsonProperty("l")]
            public string Categoria { get; set; }
            [JsonProperty("m")]
            public string Marca { get; set; }
            [JsonProperty("n")]
            public string Cod_SKU { get; set; }
            [JsonProperty("o")]
            public string Producto { get; set; }
            [JsonProperty("p")]
            public int Quiebre { get; set; }
            [JsonProperty("q")]
            public string Anio { get; set; }
            [JsonProperty("r")]
            public string Mes { get; set; }
            [JsonProperty("s")]
            public string Semana { get; set; }
        }
        public class E_Reportes_StockOut_MapBK
        {
            [JsonProperty("a")]
            public string Fecha { get; set; }
            [JsonProperty("b")]
            public string Canal { get; set; }
            [JsonProperty("c")]
            public string Oficina { get; set; }
            [JsonProperty("d")]
            public string Cadena { get; set; }
            [JsonProperty("e")]
            public string Tienda { get; set; }
            [JsonProperty("f")]
            public string Empresa { get; set; }
            [JsonProperty("g")]
            public string Categoria { get; set; }
            [JsonProperty("h")]
            public string Nivel { get; set; }
            [JsonProperty("i")]
            public string Marca { get; set; }
            [JsonProperty("j")]
            public string Producto { get; set; }
            [JsonProperty("k")]
            public string Tipo_Quiebre { get; set; }

        }
        public class E_Reportes_RangPDV_StockOut_MapBK
        {
            [JsonProperty("a")]
            public string Canal { get; set; }
            [JsonProperty("b")]
            public string Oficina { get; set; }
            [JsonProperty("c")]
            public string Cadena { get; set; }
            [JsonProperty("d")]
            public string Nom_PDV { get; set; }
            [JsonProperty("e")]
            public string Ene { get; set; }
            [JsonProperty("f")]
            public string Feb { get; set; }
            [JsonProperty("g")]
            public string Mar { get; set; }
            [JsonProperty("h")]
            public string Abr { get; set; }
            [JsonProperty("i")]
            public string May { get; set; }
            [JsonProperty("j")]
            public string Jun { get; set; }
            [JsonProperty("k")]
            public string Jul { get; set; }
            [JsonProperty("l")]
            public string Ago { get; set; }
            [JsonProperty("m")]
            public string Sep { get; set; }
            [JsonProperty("n")]
            public string Oct { get; set; }
            [JsonProperty("o")]
            public string Nov { get; set; }
            [JsonProperty("p")]
            public string Dic { get; set; }
            [JsonProperty("q")]
            public string Total { get; set; }

        }
        public class Ranking_Sku
        {
            [JsonProperty("_a")]
            public string sku_codigo { get; set; }

            [JsonProperty("_b")]
            public string sku_descripcion { get; set; }

            [JsonProperty("_c")]
            public string enero { get; set; }

            [JsonProperty("_d")]
            public string febrero { get; set; }

            [JsonProperty("_e")]
            public string marzo { get; set; }

            [JsonProperty("_f")]
            public string abril { get; set; }

            [JsonProperty("_g")]
            public string mayo { get; set; }

            [JsonProperty("_h")]
            public string junio { get; set; }

            [JsonProperty("_i")]
            public string julio { get; set; }

            [JsonProperty("_j")]
            public string agosto { get; set; }

            [JsonProperty("_k")]
            public string septiembre { get; set; }

            [JsonProperty("_l")]
            public string octubre { get; set; }

            [JsonProperty("_m")]
            public string noviembre { get; set; }

            [JsonProperty("_n")]
            public string diciembre { get; set; }

            [JsonProperty("_o")]
            public string total { get; set; }
        }    
        public class E_Anio
        {
            [JsonProperty("a")]
            public int anio { get; set; }

            [JsonProperty("b")]
            public string id_planning { get; set; }
        }
    #endregion
        #region <<< Request y Response >>>
        public class E_Parametros_MapBK
    {
        [JsonProperty("a")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("b")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("c")]
        public String Anio { get; set; }
        [JsonProperty("d")]
        public String Mes { get; set; }
        [JsonProperty("e")]
        public int Periodo { get; set; }
        [JsonProperty("f")]
        public int cod_reporte { get; set; }
        [JsonProperty("g")]
        public String Cod_Ubieo { get; set; }
        [JsonProperty("h")]
        public int Cod_Tipo { get; set; }
        [JsonProperty("i")]
        public String Cod_Categoria { get; set; }
        [JsonProperty("j")]
        public int Cod_Cadena { get; set; }
        [JsonProperty("k")]
        public int Cod_Cluster { get; set; }
        [JsonProperty("l")]
        public int Tipo_General { get; set; }
        [JsonProperty("m")]
        public string Cod_Servicio { get; set; }
        [JsonProperty("n")]
        public string Cod_Canal { get; set; }
        [JsonProperty("o")]
        public string Cod_PDV { get; set; }
        [JsonProperty("p")]
        public int Id_Reportsplanning { get; set; }
        [JsonProperty("q")]
        public string Desc_Segmento { get; set; }
        [JsonProperty("r")]
        public string cod_pais { get; set; }
        [JsonProperty("s")]
        public string cod_sku { get; set; }
        [JsonProperty("t")]
        public string actividad { get; set; }
        [JsonProperty("u")]
        public int Tipo_Exhi { get; set; }
        [JsonProperty("v")]
        public int Cod_Elemento { get; set; }
        [JsonProperty("w")]
        public string Cod_Nivel { get; set; }
        [JsonProperty("x")]
        public string Parametros { get; set; }
    }
        public class Consultar_RepMapsBKGeneral_Request
        {
            [JsonProperty("a")]
            public E_Parametros_MapBK oParametros { get; set; }
        }
        public class Mps_BK_Cobertura_Response
        {
            [JsonProperty("a")]
            public List<E_Cob_MapBK_tabla> oReporte_Cobertura { get; set; }
        }
        public class Mps_BK_PDVS_Response
        {
            [JsonProperty("a")]
            public List<E_PuntoVentaMapa> oReporte_PDVS { get; set; }
        }
        public class Mps_BK_Export_Cobertura_Response
        {
            [JsonProperty("a")]
            public List<E_Exportar_Cobertura_MapBK> oCobertura { get; set; }
        }
        public class Mps_BK_Presencia_Response
        {
            [JsonProperty("a")]
            public List<M_Presencia_MapBK> oPresencia { get; set; }
        }
        public class Mps_BK_Export_Presencia_Response
        {
            [JsonProperty("a")]
            public List<E_Export_Presencia_MapBK> oExport_Pres { get; set; }
        }
        public class Mps_BK_Presencia_x_PDV_Response
        {
            [JsonProperty("a")]
            public List<E_Presencia_Padre_MapBK> oPres_PDV { get; set; }
        }
        public class Response_Elem_Visi_MapBK
        {
            [JsonProperty("a")]
            public List<Presencia_Elemento_Visibilidad> Lista { get; set; }
        }
        public class Request_Elem_Visi_Export_MapBK
        {
            [JsonProperty("a")]
            public Parametros_Presencia_Elemento_Visibilidad oParametros { get; set; }
        }
        public class Response_Elem_Visi_Export_MapBK
        {
            [JsonProperty("a")]
            public List<Presencia_Elemento_Visibilidad_Exporta> Lista { get; set; }
        }
        public class Mps_BK_Tracking_precios_Response 
        {
            [JsonProperty("a")]
            public List<M_Tracking_precio_MapBK> oReporte_TrackingPrecio { get; set; }
        }
        public class Mps_BK_Export_TrackingPrecio_Response
        {
            [JsonProperty("a")]
            public List<E_Exportar_Tracking_MapBK> oTracking { get; set; }
        }
        public class Mps_BK_TrackingPDVCatg_Response
        {
            [JsonProperty("a")]
            public List<E_Tracking_MapBK_tabla> oReporte { get; set; }
        }
        public class Mps_BK_Filtros_Response
        {
            [JsonProperty("a")]
            public List<M_Combo> oFiltros { get; set; }
        }
        public class Mps_BK_SOD_Response
        {
            [JsonProperty("a")]
            public List<M_ReporteSOD_MapBK> oReporte_SOD { get; set; }
        }
        public class Mps_BK_Stock_out_Response
        {
            [JsonProperty("a")]
            public List<M_Stock_out_MapBK> oReporte_Stock_out { get; set; }
        }
        public class Mps_BK_Export_Stock_Out_Response
        {
            [JsonProperty("a")]
            public List<E_Export_Stock_Out_MapBK> oStock_Out { get; set; }
        }
        public class Mps_BK_Stock_x_PDV_Response
        {
            [JsonProperty("a")]
            public List<E_Tracking_MapBK_tabla> oReporte_StockPDV { get; set; }
        }
        public class Mps_BK_RepOpe_StockOut_Response
        {
            [JsonProperty("a")]
            public List<E_Reportes_StockOut_MapBK> oRepOpe_StockOut { get; set; }
        }
        public class Mps_BK_RangPDV_StockOut_Response
        {
            [JsonProperty("a")]
            public List<E_Reportes_RangPDV_StockOut_MapBK> oRangPDV_StockOut { get; set; }
        }

        public class Listar_Anios_Response 
        {
            [JsonProperty("a")]
            public List<E_Anio> oListaAnios { get; set; }
        }
        public class Response_RankingSku_Anio_TipoQuiebre_Categoria
        {
            [JsonProperty("_a")]
            public List<Ranking_Sku> Lista { get; set; }
        }
        public class Request_RankingSku_Anio_TipoQuiebre_Categoria
        {
            [JsonProperty("_a")]
            public string anio { get; set; }

            [JsonProperty("_b")]
            public string tipo_quiebre { get; set; }

            [JsonProperty("_c")]
            public string categoria { get; set; }
        }
    
    //YR
    public class E_Foto
    {
        [JsonProperty("a")]
        public string nombreFoto { get; set; }
        [JsonProperty("b")]
        public int Id { get; set; }
        [JsonProperty("c")]
        public string Descripcion { get; set; }
        [JsonProperty("d")]
        public string Periodo { get; set; }
        [JsonProperty("e")]
        public string CabPeriodo { get; set; }
        [JsonProperty("f")]
        public string id_productCategory { get; set; }
        [JsonProperty("g")]
        public string Product_Category { get; set; }
    }
    public class E_PuntoVentaDatosMapa
    {
        [JsonProperty("a")]
        public string codPuntoVenta { get; set; }
        [JsonProperty("b")]
        public string nombrePuntoVenta { get; set; }
        [JsonProperty("c")]
        public string direccion { get; set; }
        [JsonProperty("d")]
        public string distrito { get; set; }
        [JsonProperty("e")]
        public string sector { get; set; }
        [JsonProperty("f")]
        public string ultimaVisita { get; set; }
        [JsonProperty("g")]
        public string nombreGestor { get; set; }
        [JsonProperty("h")]
        public string nombreAdministrador { get; set; }
        [JsonProperty("i")]
        public string email { get; set; }
        [JsonProperty("j")]
        public string rutaVendedor { get; set; }
        [JsonProperty("k")]
        public string nombreVendedor { get; set; }
        [JsonProperty("l")]
        public string zona { get; set; }
        [JsonProperty("m")]
        public string cumpleanios { get; set; }
        [JsonProperty("n")]
        public string horaAtencion { get; set; }
        [JsonProperty("o")]
        public string numeroLocales { get; set; }
        [JsonProperty("p")]
        public int estado { get; set; }
        [JsonProperty("q")]
        public List<E_Foto> listFotos { get; set; }
        [JsonProperty("r")]
        public string Dex1 { get; set; }
        [JsonProperty("s")]
        public string Dex2 { get; set; }
        [JsonProperty("t")]
        public string CodItt1 { get; set; }
        [JsonProperty("u")]
        public string CodItt2 { get; set; }
        [JsonProperty("w")]
        public string Cluster { get; set; }
        [JsonProperty("x")]
        public string Cant_Gie { get; set; }
        [JsonProperty("y")]
        public string Desc_Clasificacion { get; set; }
    }
    public class Obtener_DatosPuntosVentaMapa_Request
    {
        [JsonProperty("a")]
        public string codPtoVenta { get; set; }

        [JsonProperty("b")]
        public string reportsPlanning { get; set; }
    }
    public class Obtener_DatosPuntosVentaMapa_Response
    {
        [JsonProperty("a")]
        public E_PuntoVentaDatosMapa ptoVenta { get; set; }
    }
    public class Mps_BK_Fotos_Response
    {
        [JsonProperty("a")]
        public List<E_Foto> oReporte_Foto { get; set; }
    }

    #endregion
}