﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Map
{
    public class Cadena : ACadena
    {
        [JsonProperty("a")]
        public int Cod_Cadena { get; set; }

        [JsonProperty("b")]
        public string Nombre_Cadena { get; set; }
    }
    public abstract class ACadena
    {
        /// <summary>
        /// Autor   : Jsulla
        /// Fecha   : 24/08/2015
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Cadena> Lista(Request_GetCadena_Campania_TipoCanal oRq)
        {
            Response_GetCadena_Campania_TipoCanal oRp;

            oRp = MvcApplication._Deserialize<Response_GetCadena_Campania_TipoCanal>(
                    MvcApplication._Servicio_Campania.GetCadena_Campania_TipoCanal(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

    }
    public class Request_GetCadena_Campania_TipoCanal
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int tipocanal { get; set; }
    }
    public class Response_GetCadena_Campania_TipoCanal
    {
        [JsonProperty("_a")]
        public List<Cadena> Lista { get; set; }
    }
}