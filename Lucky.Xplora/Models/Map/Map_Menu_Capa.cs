﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Map
{
    public class Map_Menu_Capa : AMap_Menu_Capa
    {
        [JsonProperty("a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("b")]
        public string Nom_Capa { get; set; }
        [JsonProperty("c")]
        public string img_Capa { get; set; }
        [JsonProperty("d")]
        public string id_corre { get; set; }
    }

    public class Request_Map_Menu_Capa
    {
        [JsonProperty("_a")]
        public int compania { get; set; }
        [JsonProperty("_b")]
        public int canal { get; set; }
    }
    public class Response_Map_Menu_Capa
    {
        [JsonProperty("_a")]
        public List<Map_Menu_Capa> Objeto { get; set; }
    }

    public abstract class AMap_Menu_Capa
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-21
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Map_Menu_Capa> Objeto(Request_Map_Menu_Capa oRq)
        {
            Response_Map_Menu_Capa oRp;

            oRp = MvcApplication._Deserialize<Response_Map_Menu_Capa>(
                    MvcApplication._Servicio_Maps.Map_Menu_Capa(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

}