﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace Lucky.Xplora.Models.Map
{
    public class Tipo_Canal : ATipo_Canal
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int tca_id { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string tca_descripcion { get; set; }
    }
    public abstract class ATipo_Canal
    {
        public List<Tipo_Canal> Lista(Request_GetTipoCanal_Campania oRq)
        {
            Response_GetTipoCanal_Campania oRp;

            oRp = MvcApplication._Deserialize<Response_GetTipoCanal_Campania>(
                    MvcApplication._Servicio_Campania.GetTipoCanal_Campania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }
    #region Request & Response
    /// <summary>
    /// Autor   : Jsulla
    /// Fecha   : 24/08/2015
    /// </summary>
    public class Request_GetTipoCanal_Campania
    {
        [JsonProperty("_a")]
        public string campania { get; set; }
    }
    public class Response_GetTipoCanal_Campania
    {
        [JsonProperty("a")]
        public List<Tipo_Canal> Lista { get; set; }
    }

    #endregion
}