﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusDataMercaderista
{
    public class Producto : AProducto
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_prod { get; set;}

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string des_prod { get; set; }
    }

    public abstract class AProducto {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 19-05-2015
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>    
        public List<Producto> Lista(Request_GetProducto oRq) {

            Response_GetProducto oRp;

            oRp = MvcApplication._Deserialize<Response_GetProducto>(
                MvcApplication._Servicio_Campania.GetProducto_Compania_Categoria_ProductoFamilia_Marca(
                        MvcApplication._Serialize(oRq)
                    )
            );
            return oRp.Lista;
        }
    }


    #region Rwquest & Response
    ///<sumary>
    ///Autor: rcontreras
    ///Fecha: 19-05-2015
    ///</sumary>

    public class Request_GetProducto {
        [JsonProperty("_a")]
        public string cliente {get; set;}
        [JsonProperty("_b")]
        public string categoria { get; set; }
        [JsonProperty("_c")]
        public string subCategoria { get; set; }
        [JsonProperty("_d")]
        public string marca { get; set; }
    }

    public class Response_GetProducto {
        [JsonProperty("_a")]
        public List<Producto> Lista { get; set; }
    }


    #endregion
}