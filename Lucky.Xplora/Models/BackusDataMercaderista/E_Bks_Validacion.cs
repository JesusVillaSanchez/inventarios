﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusDataMercaderista
{
    public class E_Bks_Validacion : M_Module_ValAnalista
    {
        [JsonProperty("_a")]
        public int validados { get; set; }
    }

    public abstract class M_Module_ValAnalista
    {
        public int UpdateValPrecio(Request_ValidacionPrecioBackus oRq)
        {
            Response_ValidacionPrecioBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionPrecioBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionPrecioBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaPrecio(Request_InValidacionPrecioBackus oRq)
        {
            Response_InValidacionPrecioBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionPrecioBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionPrecioBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaQuiebre(Request_ValidacionQuiebreBackus oRq)
        {
            Response_ValidacionQuiebreBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionQuiebreBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionQuiebreBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaQuiebre(Request_InValidacionQuiebreBackus oRq)
        {
            Response_InValidacionQuiebreBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionQuiebreBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionQuiebreBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaStock(Request_ValidacionStockBackus oRq)
        {
            Response_ValidacionStockBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionStockBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionStockBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaStock(Request_InValidacionStockBackus oRq)
        {
            Response_InValidacionStockBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionStockBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionStockBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaVisibilidad(Request_ValidacionVisibilidadBackus oRq)
        {
            Response_ValidacionVisibilidadBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionVisibilidadBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionVisibilidadBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaVisibilidad(Request_InValidacionVisibilidadBackus oRq)
        {
            Response_InValidacionVisibilidadBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionVisibilidadBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionVisibilidadBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaParticipacion(Request_ValidacionParticipacionBackus oRq)
        {
            Response_ValidacionParticipacionBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionParticipacionBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionParticipacionBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaParticipacion(Request_InValidacionParticipacionBackus oRq)
        {
            Response_InValidacionParticipacionBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionParticipacionBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionParticipacionBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaActividad(Request_ValidacionActividadBackus oRq)
        {
            Response_ValidacionActividadBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionActividadBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionActividadBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaActividad(Request_InValidacionActividadBackus oRq)
        {
            Response_InValidacionActividadBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionActividadBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionActividadBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaVencProducto(Request_ValidacionVencProductoBackus oRq)
        {
            Response_ValidacionVencProductoBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionVencProductoBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionVencProductoBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaVencProducto(Request_InValidacionVencProductoBackus oRq)
        {
            Response_InValidacionVencProductoBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionVencProductoBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionVencProductoBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaCodPDV(Request_ValidacionCodPDVBackus oRq)
        {
            Response_ValidacionCodPDVBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionCodPDVBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionCodPDVBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaCodPDV(Request_InValidacionCodPDVBackus oRq)
        {
            Response_InValidacionCodPDVBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionCodPDVBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionCodPDVBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaVideo(Request_ValidacionVideoBackus oRq)
        {
            Response_ValidacionVideoBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionVideoBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionVideoBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaVideo(Request_InValidacionVideoBackus oRq)
        {
            Response_InValidacionVideoBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionVideoBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionVideoBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaFotografico(Request_ValidacionFotograficoBackus oRq)
        {
            Response_ValidacionFotograficoBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionFotograficoBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionFotograficoBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaFotografico(Request_InValidacionFotograficoBackus oRq)
        {
            Response_InValidacionFotograficoBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionFotograficoBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionFotograficoBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaVenta(Request_ValidacionVentaBackus oRq)
        {
            Response_ValidacionVentaBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionVentaBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionVentaBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaVenta(Request_InValidacionVentaBackus oRq)
        {
            Response_InValidacionVentaBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionVentaBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionVentaBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }

        public int ValidaComentario(Request_ValidacionComentarioBackus oRq)
        {
            Response_ValidacionComentarioBackus oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionComentarioBackus>(
                    MvcApplication._Servicio_Operativa.ValidacionComentarioBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantValidados;
        }
        public int InvalidaComentario(Request_InValidacionComentarioBackus oRq)
        {
            Response_InValidacionComentarioBackus oRp;

            oRp = MvcApplication._Deserialize<Response_InValidacionComentarioBackus>(
                    MvcApplication._Servicio_Operativa.InValidacionComentarioBackus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantInValidados;
        }


        public int UpdateFechas(Request_UpdateFechaBackusDM oRq)
        {
            Response_UpdateFechaBackusDM oRp;

            oRp = MvcApplication._Deserialize<Response_UpdateFechaBackusDM>(
                    MvcApplication._Servicio_Operativa.UpdateFechaBackusDM(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantUpdate;
        }

    }

    #region REQUEST & REGION
    #region PRECIO
    public class Request_ValidacionPrecioBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionPrecioBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionPrecioBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionPrecioBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdatePrecioBackus
    {
        [JsonProperty("_a")]
        public string codigoDet { get; set; }

        [JsonProperty("_b")]
        public string precio { get; set; }

        [JsonProperty("_c")]
        public bool validado { get; set; }

        [JsonProperty("_d")]
        public string user_modif { get; set; }

        [JsonProperty("_e")]
        public string precioPublico { get; set; }

        [JsonProperty("_f")]
        public DateTime fechCel { get; set; }

        [JsonProperty("_g")]
        public int tipoOferta { get; set; }

    }
    public class Response_UpdatePrecioBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }

    #endregion

    #region QUIEBRE
    public class Request_ValidacionQuiebreBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionQuiebreBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionQuiebreBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionQuiebreBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdateQuiebreBackus
    {
        [JsonProperty("_a")]
        public string codigoDet { get; set; }

        [JsonProperty("_b")]
        public int quiebre { get; set; }

        [JsonProperty("_c")]
        public bool validado { get; set; }

        [JsonProperty("_d")]
        public string user_modif { get; set; }

        [JsonProperty("_e")]
        public DateTime fechCel { get; set; }
    }
    public class Response_UpdateQuiebreBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }
    #endregion

    #region STOCK
    public class Request_ValidacionStockBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionStockBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionStockBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionStockBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdateStockBackus
    {
        [JsonProperty("_a")]
        public string codigoDet { get; set; }

        [JsonProperty("_b")]
        public int cantidad { get; set; }

        [JsonProperty("_c")]
        public bool validado { get; set; }

        [JsonProperty("_d")]
        public string user_modif { get; set; }

        [JsonProperty("_e")]
        public DateTime fechCel { get; set; }
    }
    public class Response_UpdateStockBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }
    #endregion

    #region VISIBILIDAD
    public class Request_ValidacionVisibilidadBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionVisibilidadBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionVisibilidadBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionVisibilidadBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }
    public class Request_UpdateVisibilidadBackus
    {
        [JsonProperty("_a")]
        public string codigoDet { get; set; }

        [JsonProperty("_b")]
        public int cantidad { get; set; }

        [JsonProperty("_c")]
        public bool validado { get; set; }

        [JsonProperty("_d")]
        public string user_modif { get; set; }

        [JsonProperty("_e")]
        public DateTime fechCel { get; set; }

        [JsonProperty("_f")]
        public int codTipo { get; set; }

        [JsonProperty("_g")]
        public int codTipoA { get; set; }
    }
    public class Response_UpdateVisibilidadBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }
    #endregion

    #region PARTICIPACION
    public class Request_ValidacionParticipacionBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionParticipacionBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionParticipacionBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionParticipacionBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdateParticipacionBackus
    {
        [JsonProperty("_a")]
        public string codigoDet { get; set; }

        [JsonProperty("_b")]
        public int cantidad { get; set; }

        [JsonProperty("_c")]
        public bool validado { get; set; }

        [JsonProperty("_d")]
        public string user_modif { get; set; }

        [JsonProperty("_e")]
        public DateTime fechCel { get; set; }
    }
    public class Response_UpdateParticipacionBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }

    #endregion

    #region ACTIVIDAD
    public class Request_ValidacionActividadBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionActividadBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionActividadBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionActividadBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdateActividadBackus
    {
        [JsonProperty("_a")]
        public string codigoDet { get; set; }

        [JsonProperty("_b")]
        public int cantidad { get; set; }

        [JsonProperty("_c")]
        public bool validado { get; set; }

        [JsonProperty("_d")]
        public string user_modif { get; set; }

        [JsonProperty("_e")]
        public DateTime fechCel { get; set; }
    }
    public class Response_UpdateActividadBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }

    #endregion

    #region VENCIMIENTO DE PRODUCTO
    public class Request_ValidacionVencProductoBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionVencProductoBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionVencProductoBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionVencProductoBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdateVencProductoBackus
    {
        [JsonProperty("_a")]
        public int codigoDet { get; set; }

        [JsonProperty("_b")]
        public int cantidad { get; set; }

        [JsonProperty("_c")]
        public DateTime fechaVencimiento { get; set; }

        [JsonProperty("_d")]
        public bool validado { get; set; }

        [JsonProperty("_e")]
        public string user_modif { get; set; }

        [JsonProperty("_f")]
        public DateTime fechCel { get; set; }
    }
    public class Response_UpdateVencProductoBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }
    #endregion

    #region COD PDV
    public class Request_ValidacionCodPDVBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionCodPDVBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionCodPDVBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionCodPDVBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }
    #endregion

    #region VIDEO
    public class Request_ValidacionVideoBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionVideoBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionVideoBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionVideoBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdateVideoBackus
    {
        [JsonProperty("_a")]
        public string codigoDet { get; set; }

        [JsonProperty("_b")]
        public string comentario { get; set; }

        [JsonProperty("_c")]
        public bool validado { get; set; }

        [JsonProperty("_d")]
        public string user_modif { get; set; }

        [JsonProperty("_e")]
        public DateTime fechCel { get; set; }
    }
    public class Response_UpdateVideoBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }
    #endregion

    #region FOTOGRAFICO
    public class Request_ValidacionFotograficoBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionFotograficoBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionFotograficoBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionFotograficoBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdateFotograficoBackus
    {
        [JsonProperty("_a")]
        public string codigoDet { get; set; }

        [JsonProperty("_b")]
        public string comentario { get; set; }

        [JsonProperty("_c")]
        public bool validado { get; set; }

        [JsonProperty("_d")]
        public string user_modif { get; set; }

        [JsonProperty("_e")]
        public DateTime fechCel { get; set; }
    }
    public class Response_UpdateFotograficoBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }
    #endregion

    #region VENTA
    public class Request_ValidacionVentaBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionVentaBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionVentaBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionVentaBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdateVentaBackus
    {
        [JsonProperty("_a")]
        public int codigoDet { get; set; }

        [JsonProperty("_b")]
        public int cantidad { get; set; }

        [JsonProperty("_c")]
        public string tipo { get; set; }

        [JsonProperty("_d")]
        public bool validado { get; set; }

        [JsonProperty("_e")]
        public string user_modif { get; set; }

        [JsonProperty("_f")]
        public DateTime fechCel { get; set; }
    }
    public class Response_UpdateVentaBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }
    #endregion

    #region COMENTARIO
    public class Request_ValidacionComentarioBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string validado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_ValidacionComentarioBackus
    {
        [JsonProperty("_a")]
        public int cantValidados { get; set; }
    }

    public class Request_InValidacionComentarioBackus
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string invalidado { get; set; }

        [JsonProperty("_c")]
        public string usuario { get; set; }
    }
    public class Response_InValidacionComentarioBackus
    {
        [JsonProperty("_a")]
        public int cantInValidados { get; set; }
    }

    public class Request_UpdateComentarioBackus
    {
        [JsonProperty("_a")]
        public string codigoDet { get; set; }

        [JsonProperty("_b")]
        public string comentario { get; set; }

        [JsonProperty("_c")]
        public bool validado { get; set; }

        [JsonProperty("_d")]
        public string user_modif { get; set; }

        [JsonProperty("_e")]
        public DateTime fechCel { get; set; }
    }
    public class Response_UpdateComentarioBackus
    {
        [JsonProperty("_a")]
        public int iRespuesta { get; set; }
    }
    #endregion


    ///<summary>
    ///Autor: rcontreras
    ///Fecha: 06-07-2015
    ///Description: actualiza fechas
    ///
    public class Request_UpdateFechaBackusDM
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }

        [JsonProperty("_b")]
        public string codigo { get; set; }

        [JsonProperty("_c")]
        public DateTime fechCel { get; set; }

        [JsonProperty("_d")]
        public string usuario { get; set; }
    }
    public class Response_UpdateFechaBackusDM
    {
        [JsonProperty("_a")]
        public int cantUpdate { get; set; }
    }

    #endregion

}