﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusDataMercaderista
{
    public class E_Bks_Parametros
    {
        [JsonProperty("_a")]
        public int id_reporte { get; set; }
        [JsonProperty("_b")]
        public int id_tipo_canal { get; set; }
        [JsonProperty("_c")]
        public string fec_inicio { get; set; }
        [JsonProperty("_d")]
        public string fec_fin { get; set; }
        [JsonProperty("_e")]
        public int cadena { get; set; }
        [JsonProperty("_f")]
        public int categoria { get; set; }
        [JsonProperty("_g")]
        public string nivel { get; set; }
        [JsonProperty("_h")]
        public int marca { get; set; }
        [JsonProperty("_i")]
        public string producto { get; set; }
        [JsonProperty("_j")]
        public string empresa { get; set; }
    }
    #region Request - Response
    public class Request_E_Bks_Parametros
    {
        [JsonProperty("_a")]
        public E_Bks_Parametros oParametros { get; set; }
    }
    #endregion
}