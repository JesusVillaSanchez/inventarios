﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusDataMercaderista
{
    public class E_Bks_Quiebre : AE_Lista_Bks_Quiebre
    {
        [JsonProperty("_a")]
        public string cod_det { get; set; }
        [JsonProperty("_b")]
        public string des_canal { get; set; }
        [JsonProperty("_c")]
        public string fec_cel { get; set; }
        [JsonProperty("_d")]
        public string fec_bd { get; set; }
        [JsonProperty("_e")]
        public string des_ciudad { get; set; }
        [JsonProperty("_f")]
        public string des_distrito { get; set; }
        [JsonProperty("_g")]
        public string des_cono { get; set; }
        [JsonProperty("_h")]
        public string des_perfil { get; set; }
        [JsonProperty("_i")]
        public string des_gie_nom { get; set; }
        [JsonProperty("_j")]
        public string des_gie_use { get; set; }
        [JsonProperty("_k")]
        public string des_supervisor { get; set; }
        [JsonProperty("_l")]
        public string des_cadena { get; set; }
        [JsonProperty("_m")]
        public string cod_pdv_backus { get; set; }
        [JsonProperty("_n")]
        public string des_pdv { get; set; }
        [JsonProperty("_o")]
        public string des_categoria { get; set; }
        [JsonProperty("_p")]
        public string des_nivel { get; set; }
        [JsonProperty("_q")]
        public string des_marca { get; set; }
        [JsonProperty("_r")]
        public string des_producto { get; set; }
        [JsonProperty("_s")]
        public int quiebre1 { get; set; }

        [JsonProperty("_x")]
        public string quiebre1_desc { get; set; }

        [JsonProperty("_t")]
        public string comentario { get; set; }
        [JsonProperty("_u")]
        public string des_foto { get; set; }
        [JsonProperty("_v")]
        public string modificado_por { get; set; }
        [JsonProperty("_w")]
        public bool validado { get; set; }
    }


    public class Combo_Quiebre_Bks
    {
        [JsonProperty("_a")]
        public string cod_obs { get; set; }
        [JsonProperty("_b")]
        public string des_obs { get; set; }
    }


    public abstract class AE_Lista_Bks_Quiebre
    {
        public List<E_Bks_Quiebre> Lista(Request_E_Bks_Parametros oRq)
        {
            Response_Listar_Reporte_Quiebre_BackusDM oRp;
            oRp = MvcApplication._Deserialize<Response_Listar_Reporte_Quiebre_BackusDM>(
                    MvcApplication._Servicio_Operativa.Listar_Reporte_Quiebre_BackusDM(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
    }

    #region request & response
    public class Response_Listar_Reporte_Quiebre_BackusDM
    {
        [JsonProperty("_a")]
        public List<E_Bks_Quiebre> Lista { get; set; }
    }

    public class Combo_Quiebre_Bks_Request
    {
        [JsonProperty("_a")]
        public int reporte { get; set; }
    }

    public class Combo_Quiebre_Bks_Response
    {
        [JsonProperty("_a")]
        public List<Combo_Quiebre_Bks> Lista { get; set; }
    }

    #endregion
}