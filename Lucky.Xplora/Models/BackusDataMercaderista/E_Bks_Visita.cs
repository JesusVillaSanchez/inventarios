﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusDataMercaderista
{
    public class E_Bks_Visita : AE_Lista_Bks_Visita
    {
        [JsonProperty("_a")]
        public string cod_cab { get; set; }
        [JsonProperty("_b")]
        public string des_canal { get; set; }
        [JsonProperty("_c")]
        public string fecha { get; set; }
        [JsonProperty("_e")]
        public string des_ciudad { get; set; }
        [JsonProperty("_f")]
        public string des_distrito { get; set; }
        [JsonProperty("_g")]
        public string des_cono { get; set; }
        [JsonProperty("_h")]
        public string des_perfil { get; set; }
        [JsonProperty("_i")]
        public string des_gie_nom { get; set; }
        [JsonProperty("_j")]
        public string des_gie_use { get; set; }
        [JsonProperty("_k")]
        public string des_supervisor { get; set; }
        [JsonProperty("_l")]
        public string des_cadena { get; set; }
        [JsonProperty("_m")]
        public string cod_pdv_backus { get; set; }
        [JsonProperty("_n")]
        public string des_pdv { get; set; }
        [JsonProperty("_o")]
        public string cod_novisita { get; set; }
        [JsonProperty("_p")]
        public string des_novista { get; set; }
        [JsonProperty("_q")]
        public string fec_reg_ini { get; set; }
        [JsonProperty("_r")]
        public string latitud_ini { get; set; }
        [JsonProperty("_s")]
        public string longitud_ini { get; set; }
        [JsonProperty("_t")]
        public string fec_reg_fin { get; set; }
        [JsonProperty("_u")]
        public string latitud_fin { get; set; }
        [JsonProperty("_v")]
        public string longitud_fin { get; set; }
        [JsonProperty("_w")]
        public string modificado_por { get; set; }
    }

    public abstract class AE_Lista_Bks_Visita
    {
        public List<E_Bks_Visita> Lista(Request_E_Bks_Parametros oRq)
        {
            Response_Listar_Reporte_Visita_BackusDM oRp;
            oRp = MvcApplication._Deserialize<Response_Listar_Reporte_Visita_BackusDM>(
                    MvcApplication._Servicio_Operativa.Listar_Reporte_Visita_BackusDM(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
    }

    #region request & response
    public class Response_Listar_Reporte_Visita_BackusDM
    {
        [JsonProperty("_a")]
        public List<E_Bks_Visita> Lista { get; set; }
    }
    #endregion
}