﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusDataMercaderista
{
    public class E_Bks_Actividad : AE_Lista_Bks_Actividad
    {
        [JsonProperty("_a")]
        public string cod_cab { get; set; }
        [JsonProperty("_b")]
        public string des_canal { get; set; }
        [JsonProperty("_c")]
        public string fec_cel { get; set; }
        [JsonProperty("_d")]
        public string fec_bd { get; set; }
        [JsonProperty("_e")]
        public string des_ciudad { get; set; }
        [JsonProperty("_f")]
        public string des_distrito { get; set; }
        [JsonProperty("_g")]
        public string des_cono { get; set; }
        [JsonProperty("_h")]
        public string des_perfil { get; set; }
        [JsonProperty("_i")]
        public string des_gie_nom { get; set; }
        [JsonProperty("_j")]
        public string des_gie_use { get; set; }
        [JsonProperty("_k")]
        public string des_supervisor { get; set; }
        [JsonProperty("_l")]
        public string des_cadena { get; set; }
        [JsonProperty("_m")]
        public string cod_pdv_backus { get; set; }
        [JsonProperty("_n")]
        public string des_pdv { get; set; }
        [JsonProperty("_o")]
        public string des_categoria { get; set; }
        [JsonProperty("_p")]
        public string des_nivel { get; set; }
        [JsonProperty("_q")]
        public string des_marca { get; set; }
        [JsonProperty("_r")]
        public string des_producto { get; set; }
        [JsonProperty("_s")]
        public string cod_objetivo { get; set; }
        [JsonProperty("_t")]
        public string des_objetivo { get; set; }
        [JsonProperty("_u")]
        public string cod_actividad { get; set; }
        [JsonProperty("_v")]
        public string des_actividad { get; set; }
        [JsonProperty("_w")]
        public int cantidad { get; set; }
        [JsonProperty("_x")]
        public string des_mecanica { get; set; }
        [JsonProperty("_y")]
        public string fec_inicio_act { get; set; }
        [JsonProperty("_z")]
        public string fec_fin_act { get; set; }
        [JsonProperty("_aa")]
        public string cod_material { get; set; }
        [JsonProperty("_ab")]
        public string des_material { get; set; }
        [JsonProperty("_ac")]
        public decimal precio_regular { get; set; }
        [JsonProperty("_ad")]
        public decimal precio_oferta { get; set; }
        [JsonProperty("_ae")]
        public string modificado_por { get; set; }
        [JsonProperty("_af")]
        public bool validado { get; set; }
    }

    public abstract class AE_Lista_Bks_Actividad
    {
        public List<E_Bks_Actividad> Lista(Request_E_Bks_Parametros oRq)
        {
            Response_Listar_Reporte_Actividad_BackusDM oRp;
            oRp = MvcApplication._Deserialize<Response_Listar_Reporte_Actividad_BackusDM>(
                    MvcApplication._Servicio_Operativa.Listar_Reporte_Actividad_BackusDM(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
    }

    #region request & response
    public class Response_Listar_Reporte_Actividad_BackusDM
    {
        [JsonProperty("_a")]
        public List<E_Bks_Actividad> Lista { get; set; }
    }
    #endregion

}