﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Periodo : APeriodo
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public int rpl_id { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string rpl_descripcion { get; set; }
    }

    public abstract class APeriodo {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-11
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Periodo> Lista(Request_Lista_Periodo_Colgate_Reports oRq)
        {
            Response_Lista_Periodo_Colgate_Reports oRp;

            oRp = MvcApplication._Deserialize<Response_Lista_Periodo_Colgate_Reports>(
                MvcApplication._Servicio_Campania.Lista_Periodo_Colgate(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-27
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Periodo> Lista(Periodo_Por_Planning_Reports_Anio_Mes_Request oRq)
        {
            Periodo_Por_Planning_Reports_Anio_Mes_Response oRp;

            oRp = MvcApplication._Deserialize<Periodo_Por_Planning_Reports_Anio_Mes_Response>(
                MvcApplication._Servicio_Campania.Periodo_Por_Planning_Reports_Anio_Mes(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-20
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Periodo> Lista(Request_GetPeriodo_Anio_Campania_TipoPerfil oRq)
        {
            Response_GetPeriodo_Anio_Campania_TipoPerfil oRp;

            oRp = MvcApplication._Deserialize<Response_GetPeriodo_Anio_Campania_TipoPerfil>(
                MvcApplication._Servicio_Campania.GetPeriodo_Anio_Campania_TipoPerfil(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-23
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Periodo> Lista(Request_GetPeriodo_Campania_Reporte_Anio_Mes_Formato oRq)
        {
            Response_GetPeriodo_Campania_Reporte_Anio_Mes_Formato oRp;

            oRp = MvcApplication._Deserialize<Response_GetPeriodo_Campania_Reporte_Anio_Mes_Formato>(
                MvcApplication._Servicio_Campania.GetPeriodo_Campania_Reporte_Anio_Mes_Formato(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-29
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Periodo> Lista(Request_GetPeriodo_Campania_Reporte_Anio_Mes_Formato_TipoPerfil oRq)
        {
            Response_GetPeriodo_Campania_Reporte_Anio_Mes_Formato oRp;

            oRp = MvcApplication._Deserialize<Response_GetPeriodo_Campania_Reporte_Anio_Mes_Formato>(
                MvcApplication._Servicio_Campania.GetPeriodo_Campania_Reporte_Anio_Mes_Formato_TipoPerfil(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 2015-02-11
    /// </summary>
    public class Request_Lista_Periodo_Colgate_Reports
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public int anio { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string campania { get; set; }
    }
    public class Response_Lista_Periodo_Colgate_Reports
    {
        [JsonProperty("a")]
        public List<Periodo> Lista { get; set; }
    }

    /// <summary>
    /// Fecha: 2015-02-27
    /// Autor: jlucero
    /// </summary>
    public class Periodo_Por_Planning_Reports_Anio_Mes_Request
    {
        [JsonProperty("a")]
        public string campania { get; set; }

        [JsonProperty("b")]
        public int reporte { get; set; }

        [JsonProperty("c")]
        public int anio { get; set; }

        [JsonProperty("d")]
        public int mes { get; set; }
    }
    public class Periodo_Por_Planning_Reports_Anio_Mes_Response
    {
        [JsonProperty("a")]
        public List<Periodo> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-03-20
    /// </summary>
    public class Request_GetPeriodo_Anio_Campania_TipoPerfil
    {
        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public string campania { get; set; }

        [JsonProperty("_c")]
        public int tipoperfil { get; set; }
    }
    public class Response_GetPeriodo_Anio_Campania_TipoPerfil
    {
        [JsonProperty("_a")]
        public List<Periodo> Lista { get; set; }
    }

    /// <summary>
    /// Fecha: 2015-04-23
    /// Autor: jlucero
    /// </summary>
    public class Request_GetPeriodo_Campania_Reporte_Anio_Mes_Formato
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int reporte { get; set; }

        [JsonProperty("_c")]
        public int anio { get; set; }

        [JsonProperty("_d")]
        public int mes { get; set; }

        [JsonProperty("_e")]
        public int formato { get; set; }
    }
    public class Response_GetPeriodo_Campania_Reporte_Anio_Mes_Formato
    {
        [JsonProperty("_a")]
        public List<Periodo> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-29
    /// </summary>
    public class Request_GetPeriodo_Campania_Reporte_Anio_Mes_Formato_TipoPerfil
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int reporte { get; set; }

        [JsonProperty("_c")]
        public int anio { get; set; }

        [JsonProperty("_d")]
        public int mes { get; set; }

        [JsonProperty("_e")]
        public int formato { get; set; }

        [JsonProperty("_g")]
        public int tipo_perfil { get; set; }
    }

    #endregion
}