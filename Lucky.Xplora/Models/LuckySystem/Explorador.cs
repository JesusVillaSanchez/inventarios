﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.Models.LuckySystem
{
    public class Explorador
    {
        public List<Folder> _Folder;
        public List<Archivo> _Archivo;
        public string _Query;

        public Explorador(List<Folder> _ListFolder, List<Archivo> _ListArchivo, string _QueryString)
        {
            _Folder = _ListFolder;
            _Archivo = _ListArchivo;
            _Query = _QueryString;
        }
    }

    public class Folder
    {
        public string Nombre { get; set; }
        public DateTime Fecha { get; set; }
    }

    public class Archivo
    {
        public string NombreExtension { get; set; }
        public string Nombre { get; set; }
        public string Extension { get; set; }
        public string Tamaño { get; set; }
        public DateTime Fecha { get; set; }
    }
}