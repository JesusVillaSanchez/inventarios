﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-16
    /// </summary>
    public class NodoComercial : ANodoComercial
    {
        [JsonProperty("a")]
        public int nco_id { get; set; }

        [JsonProperty("b")]
        public string nco_descripcion { get; set; }
    }

    /*public class E_Cadena : BNodoComercial {
        [JsonProperty("a")]
        public int Cod_Cadena { get; set; }

        [JsonProperty("b")]
        public string Nombre_Cadena { get; set; }
    }*/

    public abstract class ANodoComercial
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-16
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<NodoComercial> Lista(Request_GetCadena_Campania_Oficina_Departamento oRq)
        {
            Response_GetCadena_Campania_Oficina_Departamento oRp;

            oRp = MvcApplication._Deserialize<Response_GetCadena_Campania_Oficina_Departamento>(
                    MvcApplication._Servicio_Campania.GetCadena_Campania_Oficina_Departamento(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-12
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<NodoComercial> Lista(Request_GetCadena_Compania_Oficina_Sector oRq)
        {
            Response_GetCadena_Campania_Oficina_Departamento oRp;

            oRp = MvcApplication._Deserialize<Response_GetCadena_Campania_Oficina_Departamento>(
                    MvcApplication._Servicio_Campania.GetCadena_Compania_Oficina_Sector(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-12
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<NodoComercial> Lista(Request_GetCadena_Campania_Compania_Oficina_Sector oRq)
        {
            Response_GetCadena_Campania_Oficina_Departamento oRp;

            oRp = MvcApplication._Deserialize<Response_GetCadena_Campania_Oficina_Departamento>(
                    MvcApplication._Servicio_Campania.GetCadena_Campania_Compania_Oficina_Sector(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-27
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<NodoComercial> Lista(NodeCommercial_Por_Planning_Request oRq)
        {
            NodeCommercial_Por_Planning_Response oRp;

            oRp = MvcApplication._Deserialize<NodeCommercial_Por_Planning_Response>(
                    MvcApplication._Servicio_Campania.NodeCommercial_Por_Planning(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-06
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<NodoComercial> Lista(Request_GetCadena_Campania_TipoCanal oRq)
        {
            Response_GetCadena_Campania_TipoCanal oRp;

            oRp = MvcApplication._Deserialize<Response_GetCadena_Campania_TipoCanal>(
                    MvcApplication._Servicio_Campania.GetCadena_Campania_TipoCanal(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 30/03/15
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<NodoComercial> Lista(Request_E_Cadena oRq)
        {
            Response_E_Cadena oRp;

            oRp = MvcApplication._Deserialize<Response_E_Cadena>(
                    MvcApplication._Servicio_Campania.GetCadena_Opcion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-16
    /// </summary>
    public class Request_GetCadena_Campania_Oficina_Departamento
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int oficina { get; set; }

        [JsonProperty("_c")]
        public string departamento { get; set; }
    }
    public class Response_GetCadena_Campania_Oficina_Departamento
    {
        [JsonProperty("_a")]
        public List<NodoComercial> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-12
    /// </summary>
    public class Request_GetCadena_Compania_Oficina_Sector
    {
        [JsonProperty("_a")]
        public int compania { get; set; }

        [JsonProperty("_b")]
        public int oficina { get; set; }

        [JsonProperty("_c")]
        public int sector { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-24
    /// </summary>
    public class Request_GetCadena_Campania_Compania_Oficina_Sector
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int compania { get; set; }

        [JsonProperty("_c")]
        public int oficina { get; set; }

        [JsonProperty("_d")]
        public int sector { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-27
    /// </summary>
    public class NodeCommercial_Por_Planning_Request
    {
        [JsonProperty("_a")]
        public string campania { get; set; }
    }
    public class NodeCommercial_Por_Planning_Response
    {
        [JsonProperty("_a")]
        public List<NodoComercial> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-03-06
    /// </summary>
    public class Request_GetCadena_Campania_TipoCanal
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int tipocanal { get; set; }
    }
    public class Response_GetCadena_Campania_TipoCanal
    {
        [JsonProperty("_a")]
        public List<NodoComercial> Lista { get; set; }
    }

    public class Request_E_Cadena
    {
        [JsonProperty("_a")]
        public string opcion { get; set; }
    }
    public class Response_E_Cadena
    {
        [JsonProperty("a")]
        public List<NodoComercial> Lista { get; set; }
    }
    #endregion
}