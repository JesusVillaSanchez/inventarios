﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Lucky.Xplora.Models.Automecatronica
{
    #region Guillermo
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-06-04
    /// </summary>
    public class Auto : AAuto
    {
        [JsonProperty("_a")]
        public int id_auto { get; set; }
        [JsonProperty("_b")]
        public string placa { get; set; }
        [JsonProperty("_c")]
        public string marca { get; set; }
        [JsonProperty("_d")]
        public string modelo { get; set; }
        [JsonProperty("_e")]
        public string color { get; set; }
        [JsonProperty("_f")]
        public string fec_recep { get; set; }
        [JsonProperty("_g")]
        public string fec_taller { get; set; }
        [JsonProperty("_h")]
        public string descrip_trab { get; set; }
        [JsonProperty("_i")]
        public string proceso_actual { get; set; }
        [JsonProperty("_j")]
        public string fec_esti_entrega { get; set; }
        [JsonProperty("_k")]
        public string observacion { get; set; }
        [JsonProperty("_l")]
        public string asesor { get; set; }
        [JsonProperty("_m")]
        public string sede { get; set; }
        [JsonProperty("_n")]
        public string permanencia { get; set; }
        [JsonProperty("_o")]
        public string importe_total { get; set; }
        [JsonProperty("_p")]
        public string estado_revision { get; set; }
        [JsonProperty("_q")]
        public string encargado { get; set; }
        [JsonProperty("_r")]
        public string aseguradora { get; set; }
        [JsonProperty("_s")]
        public int id_cliente { get; set; }
        [JsonProperty("_t")]
        public int id_category { get; set; }
        [JsonProperty("_u")]
        public int id_modelo { get; set; }
        [JsonProperty("_v")]
        public int id_color { get; set; }
        [JsonProperty("_w")]
        public int id_aseguradora { get; set; }
        [JsonProperty("_x")]
        public string fecha_huachipa { get; set; }
        [JsonProperty("_y")]
        public string fecha_en_taller { get; set; }
        [JsonProperty("_z")]
        public int id_estado { get; set; }
        [JsonProperty("_aa")]
        public string fecha_entrega { get; set; }
        [JsonProperty("_ab")]
        public string acti_actual { get; set; }
        [JsonProperty("_ac")]
        public string fecha_acti_actual { get; set; }
        [JsonProperty("_ad")]
        public int id_tecnico { get; set; }
        [JsonProperty("_ae")]
        public string nombre_tecnico { get; set; }
        [JsonProperty("_af")]
        public string sede_tecnico { get; set; }
        [JsonProperty("_ag")]
        public string telefono_tecnico { get; set; }
        [JsonProperty("_ah")]
        public int id_asesor { get; set; }
        [JsonProperty("_ai")]
        public string nombre_asesor { get; set; }
        [JsonProperty("_aj")]
        public string sede_asesor { get; set; }
        [JsonProperty("_ak")]
        public string telefono_asesor { get; set; }
        [JsonProperty("_al")]
        public int id_persona_asesor { get; set; }
        [JsonProperty("_am")]
        public int id_actividad_actual { get; set; }
        [JsonProperty("_an")]
        public Boolean en_taller { get; set; }
    }
    
    public class ComboFiltro: AComboFiltro
    {
        [JsonProperty("_a")]
        public int id { get; set; }
        [JsonProperty("_b")]
        public string nombre { get; set; }
        [JsonProperty("_c")]
        public string valor { get; set; }        
    }

    public class Valorizacion_Orden
    {
        [JsonProperty("_a")]
        public int id_autoactividad { get; set; }
        [JsonProperty("_b")]
        public string id_actividad { get; set; }
        [JsonProperty("_c")]
        public string costo { get; set; }
        [JsonProperty("_d")]
        public int id_auto { get; set; }
        [JsonProperty("_e")]
        public string estado { get; set; }
        [JsonProperty("_f")]
        public string fecha { get; set; }
        [JsonProperty("_g")]
        public string actual { get; set; }
        [JsonProperty("_h")]
        public string nombre_actividad { get; set; }
        [JsonProperty("_i")]
        public string valorizacion { get; set; }
        [JsonProperty("_j")]
        public string ajuste { get; set; }
        [JsonProperty("_k")]
        public string fecha_ajuste { get; set; }

        [JsonProperty("_l")]
        public bool csajuste { get; set; }
        [JsonProperty("_m")]
        public string costo_ajuste { get; set; }
        [JsonProperty("_n")]
        public string total_val { get; set; }
    }

    public class Auto_Orden
    {
        [JsonProperty("_a")]
        public int id_orden { get; set; }
        [JsonProperty("_b")]
        public int id_auto { get; set; }
        [JsonProperty("_c")]
        public string fecha_oc { get; set; }
        [JsonProperty("_d")]
        public string oc { get; set; }
        [JsonProperty("_e")]
        public string costo { get; set; }
        [JsonProperty("_f")]
        public string referencia { get; set; }
    }
    
    public class Auto_Foto 
    { 
        [JsonProperty("_a")]
        public int id_foto { get; set; }
        [JsonProperty("_b")]
        public int id_auto { get; set; }
        [JsonProperty("_c")]
        public string foto { get; set; }
        [JsonProperty("_d")]
        public Boolean ingreso { get; set; }
        [JsonProperty("_e")]
        public string base64 { get; set; }
        [JsonProperty("_f")]
        public string tipo { get; set; }
    }

    public abstract class AAuto
    {
        /// <summary>
        /// Autor: gruiz
        /// Fecha: 2016-04-06
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Auto> Lista(Request_AutoMantenimiento oRq)
        {
            Response_AutoMantenimiento_Listado oRp = MvcApplication._Deserialize<Response_AutoMantenimiento_Listado>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoListar(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }

        public int Registro(Request_AutoMantenimiento_Registro oRq)
        {
            Response_AutoMantenimiento_Registro oRp = MvcApplication._Deserialize<Response_AutoMantenimiento_Registro>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoRegistrar(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }

        public int Registro_Valorizacion(Request_Valorizacion oRq)
        {
            Response_Valorizacion oRp = MvcApplication._Deserialize<Response_Valorizacion>(
                        MvcApplication._Servicio_Operativa.RegistrarValorizacion(
                            MvcApplication._Serialize(oRq)
                        )
                    );

                return oRp.Obj;
        }

        public List<Valorizacion_Orden> Obtener_Val_Orden(Request_ObtenerValOrd oRq)
        {
            Response_ObtenerValOrd oRp = MvcApplication._Deserialize<Response_ObtenerValOrd>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoListar_valorden(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }

        public int eliminar_Val_Orden(Request_ObtenerValOrd oRq)
        {
            Response_eliminarValOrd oRp = MvcApplication._Deserialize<Response_eliminarValOrd>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoEliminar_valorden(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }

        public int eliminar_Registro(Request_ObtenerRegistro oRq)
        {
            Response_AutoMantenimiento_Registro oRp = MvcApplication._Deserialize<Response_AutoMantenimiento_Registro>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoEliminar_registro(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }

        public int Registro_Foto(Request_Auto_Foto oRq)
        {
            Response_Auto_Foto oRp = MvcApplication._Deserialize<Response_Auto_Foto>(
                        MvcApplication._Servicio_Operativa.AutoMantenimientoRegistrarFoto(
                            MvcApplication._Serialize(oRq)
                        )
                    );
                return oRp.Obj;
        }

        public List<Auto_Foto> Obtener_Fotos(Request_Auto_Foto oRq)
        {
            Response_Obtener_Foto oRp = MvcApplication._Deserialize<Response_Obtener_Foto>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoObtener_Fotos(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Obj;
        }

        public int eliminar_Foto(Request_Auto_Foto oRq)
        {
            Response_eliminarValOrd oRp = MvcApplication._Deserialize<Response_eliminarValOrd>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoEliminar_Foto(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }

        public int Registro_Orden(Request_Orden oRq)
        {
            Response_Valorizacion oRp = MvcApplication._Deserialize<Response_Valorizacion>(
                        MvcApplication._Servicio_Operativa.RegistrarOrden(
                            MvcApplication._Serialize(oRq)
                        )
                    );

                return oRp.Obj;
        }


        public List<Auto_Orden> Obtener_Orden(Request_Orden oRq)
        {
            Response_AutoOrden oRp = MvcApplication._Deserialize<Response_AutoOrden>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoListar_Orden(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }

        public int eliminar_Orden(Request_Orden oRq)
        {
            Response_eliminarValOrd oRp = MvcApplication._Deserialize<Response_eliminarValOrd>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoEliminar_Orden(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }

        public int Actualizar_archivo_val(Request_Valorizacion oRq)
        {
            Response_Auto_Foto oRp = MvcApplication._Deserialize<Response_Auto_Foto>(
                        MvcApplication._Servicio_Operativa.AutoMantenimientoRegistrarFileVal(
                            MvcApplication._Serialize(oRq)
                        )
                    );
            return oRp.Obj;
        }
        public int Actualizar_archivo_orden(Request_Valorizacion oRq)
        {
            Response_Auto_Foto oRp = MvcApplication._Deserialize<Response_Auto_Foto>(
                        MvcApplication._Servicio_Operativa.AutoMantenimientoRegistrarFileOrden(
                            MvcApplication._Serialize(oRq)
                        )
                    );
            return oRp.Obj;
        }
    }

    public abstract class AComboFiltro{
        /// <summary>
        /// Autor: gruiz
        /// Fecha: 2016-04-11
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<ComboFiltro> Lista(Request_comboFiltro oRq)
        {
            Response_comboFiltro_Listado oRp = MvcApplication._Deserialize<Response_comboFiltro_Listado>(
                    MvcApplication._Servicio_Operativa.AutoMantenimientoFiltro(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Obj;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-06
    /// </summary>
    public class Request_AutoMantenimiento
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string placa { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int asesor { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public int cliente { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int estado { get; set; }                
    }
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-06
    /// </summary>
    public class Response_AutoMantenimiento_Listado
    {
        [JsonProperty("_a")]
        public List<Auto> Obj { get; set; }
    }


    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-11
    /// </summary>
    public class Request_comboFiltro 
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int condicion { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int id_company { get; set; }
        [JsonProperty("_C", NullValueHandling = NullValueHandling.Ignore)]
        public int dato { get; set; }
    }
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-11
    /// </summary>
    public class Response_comboFiltro_Listado
    {
        [JsonProperty("_a")]
        public List<ComboFiltro> Obj { get; set; }
    }
    
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-18
    /// </summary>
    public class Request_Valorizacion
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int id_val { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int id_actividad { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string costo { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int id_auto { get; set; }
        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public string fecha { get; set; }
        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public Boolean actual { get; set; }
        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public string descrip_trab { get; set; }
        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public string valorizacion { get; set; }
        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public string ajust { get; set; }
        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public string fec_ajuste { get; set; }
        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public bool tipo { get; set; }

        [JsonProperty("_l", NullValueHandling = NullValueHandling.Ignore)]
        public bool csajuste { get; set; }
        [JsonProperty("_m", NullValueHandling = NullValueHandling.Ignore)]
        public string costo_ajuste { get; set; }
        [JsonProperty("_n", NullValueHandling = NullValueHandling.Ignore)]
        public string total_val { get; set; }

    }
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-18
    /// </summary>
    public class Response_Valorizacion
    {
        [JsonProperty("_a")]
        public int Obj { get; set; }
    }
    
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-14
    /// </summary>
    public class Request_AutoMantenimiento_Registro
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int id_auto { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string placa { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public int cliente { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int marca { get; set; }
        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int modelo { get; set; }
        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public int color { get; set; }
        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public string aseguradora { get; set; }
        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public string fec_recep { get; set; }
        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public string fec_taller { get; set; }
        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public int id_fotos_inicio { get; set; }
        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public int id_valorizacion { get; set; }
        [JsonProperty("_l", NullValueHandling = NullValueHandling.Ignore)]
        public int id_orden_compra { get; set; }
        [JsonProperty("_m", NullValueHandling = NullValueHandling.Ignore)]
        public string descripcion_trabajo { get; set; }
        [JsonProperty("_n", NullValueHandling = NullValueHandling.Ignore)]
        public int estado { get; set; }
        [JsonProperty("_o", NullValueHandling = NullValueHandling.Ignore)]
        public string fecha_estimada_entrega { get; set; }
        [JsonProperty("_p", NullValueHandling = NullValueHandling.Ignore)]
        public int proceso_actual { get; set; }
        [JsonProperty("_q", NullValueHandling = NullValueHandling.Ignore)]
        public string tecnico { get; set; }
        [JsonProperty("_r", NullValueHandling = NullValueHandling.Ignore)]
        public string sede_t { get; set; }
        [JsonProperty("_s", NullValueHandling = NullValueHandling.Ignore)]
        public string telefono_t { get; set; }
        [JsonProperty("_t", NullValueHandling = NullValueHandling.Ignore)]
        public string asesor { get; set; }
        [JsonProperty("_u", NullValueHandling = NullValueHandling.Ignore)]
        public string sede_a { get; set; }
        [JsonProperty("_v", NullValueHandling = NullValueHandling.Ignore)]
        public string telefono_a { get; set; }
        [JsonProperty("_w", NullValueHandling = NullValueHandling.Ignore)]
        public int id_fotos_final { get; set; }
        [JsonProperty("_x", NullValueHandling = NullValueHandling.Ignore)]
        public string fecha_proc_act { get; set; }
        [JsonProperty("_y", NullValueHandling = NullValueHandling.Ignore)]
        public string observacion { get; set; }
        [JsonProperty("_ab", NullValueHandling = NullValueHandling.Ignore)]
        public Boolean en_taller { get; set; }
    }
    
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-14
    /// </summary>
    public class Response_AutoMantenimiento_Registro
    {
        [JsonProperty("_a")]
        public int Obj { get; set; }
    }
        
    public class Request_ObtenerRegistro
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int id_auto { get; set; }
    }

    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-14
    /// </summary>
    public class Request_ObtenerValOrd
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int id_auto { get; set; }        
    }
        
    
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-14
    /// </summary>
    public class Response_ObtenerValOrd
    {
        [JsonProperty("_a")]
        public List<Valorizacion_Orden> Obj { get; set; }
    }

    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-20
    /// </summary>
    public class Response_eliminarValOrd
    {
        [JsonProperty("_a")]
        public int Obj { get; set; }
    }
    
    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-21
    /// </summary>
    public class Request_Auto_Foto
    {
        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int id_foto { get; set; }
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int id_auto { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string foto { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public bool ingreso { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public string tipo { get; set; }
    }

    public class Response_Auto_Foto
    {
        [JsonProperty("_a")]
        public int Obj { get; set; }
    }
    
    public class Response_Obtener_Foto
    {
        [JsonProperty("_a")]
        public List<Auto_Foto> Obj { get; set; }
    }


    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-27
    /// </summary>
    public class Request_Orden
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int id_orden { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int id_auto { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string fecha_oc { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public string oc { get; set; }
        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public string costo { get; set; }
        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public string referencia { get; set; }
    }

    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-18
    /// </summary>
    public class Response_Orden
    {
        [JsonProperty("_a")]
        public int Obj { get; set; }
    }

    /// <summary>
    /// Autor: gruiz
    /// Fecha: 2016-04-18
    /// </summary>
    public class Response_AutoOrden
    {
        [JsonProperty("_a")]
        public List<Auto_Orden> Obj { get; set; }
    }

    #endregion
    #endregion

    #region Aldo
    /// <summary>
    /// Autor: adarrigo
    /// Fecha: 2016-08-12
    /// </summary>
    public class Automecatronica : AAutomecatronica
    {
        [JsonProperty("_a")]
        public string id_Op { get; set; }

        [JsonProperty("_b")]
        public string placa_Op { get; set; }

        [JsonProperty("_c")]
        public string fecha_Ing { get; set; }

        [JsonProperty("_d")]
        public string fecha_Aju { get; set; }

        [JsonProperty("_e")]
        public string fecha_Inicio { get; set; }

        [JsonProperty("_f")]
        public string fecha_Audatex { get; set; }

        [JsonProperty("_g")]
        public string fecha_Aprob { get; set; }

        [JsonProperty("_h")]
        public string fecha_Cot_Rep { get; set; }

        [JsonProperty("_i")]
        public string fecha_Entrega_Op { get; set; }

        [JsonProperty("_j")]
        public Int64 nro_Orden { get; set; }

        [JsonProperty("_k")]
        public string valor_trab { get; set; }

        [JsonProperty("_l")]
        public double valor_hor_chapa { get; set; }

        [JsonProperty("_m")]
        public double valor_hor_pint { get; set; }

        [JsonProperty("n")]
        public double valor_hor_arma { get; set; }

        [JsonProperty("_o")]
        public double valor_hor_mecan { get; set; }

        [JsonProperty("_p")]
        public double valor_impor { get; set; }

        [JsonProperty("_q")]
        public double valor_monto_chapa { get; set; }

        [JsonProperty("_r")]
        public double valor_monto_pint { get; set; }

        [JsonProperty("_s")]
        public double valor_hh_prepar { get; set; }

        [JsonProperty("_t")]
        public double valor_monto_mecan { get; set; }

        [JsonProperty("_u")]
        public string valor_proceso { get; set; }

        [JsonProperty("_v")]
        public double valor_ut_armado { get; set; }

        [JsonProperty("_w")]
        public double valor_ut_mecan { get; set; }

        [JsonProperty("_x")]
        public string valor_fecha_entrega { get; set; }

        [JsonProperty("_y")]
        public string valor_observ { get; set; }

        [JsonProperty("_z")]
        public string time_perm_taller { get; set; }

        [JsonProperty("_aa")]
        public string time_dias_aprob { get; set; }

        [JsonProperty("_ab")]
        public string time_entrega_lima { get; set; }

        [JsonProperty("_ac")]
        public string time_pptpo { get; set; }

        [JsonProperty("_ad")]
        public string time_entrega_oc { get; set; }

        [JsonProperty("_ae")]
        public int time_pptpo_repue { get; set; }

        [JsonProperty("_af")]
        public string cont_asesor { get; set; }

        [JsonProperty("_ag")]
        public string cont_sede { get; set; }

        [JsonProperty("_ah")]
        public string cont_observ { get; set; }

        [JsonProperty("_ai")]
        public string ve_marca { get; set; }

        [JsonProperty("_aj")]
        public string ve_color { get; set; }

        [JsonProperty("_ak")]
        public string ve_modelo { get; set; }

        [JsonProperty("_al")]
        public Int64 orden { get; set; }

        [JsonProperty("_am")]
        public string time_entrega_repue { get; set; }

        [JsonProperty("_an")]
        public int id_tecnico { get; set; }

        [JsonProperty("_añ")]
        public int id_tecnico2 { get; set; }

        [JsonProperty("_ao")]
        public int id_tecnico3 { get; set; }

        [JsonProperty("_ap")]
        public int id_tecnico4 { get; set; }

        [JsonProperty("_aq")]
        public int id_tecnico5 { get; set; }

        [JsonProperty("_ar")]
        public int id_tecnico6 { get; set; }

        [JsonProperty("_as")]
        public int cont_id_sede { get; set; }

        [JsonProperty("_at")]
        public int valor_id_proceso { get; set; }

        [JsonProperty("_au")]
        public double paños { get; set; }

        [JsonProperty("_av")]
        public string valor_repuestos { get; set; }

        [JsonProperty("_aw")]
        public double thoras_chapa { get; set; }

        [JsonProperty("_ay")]
        public double thoras_pintura { get; set; }

        [JsonProperty("_az")]
        public int cia_seguro { get; set; }

        //[JsonProperty("_ba")]
        //public string foto { get; set; }

        [JsonProperty("_bb")]
        public int id_tipo_foto { get; set; }

        [JsonProperty("_bc")]
        public List<AutomecatronicaTecnico> tecnico { get; set; }

        [JsonProperty("_bd")]
        public List<AutomecatronicaFoto> foto { get; set; }

        [JsonProperty("_be")]
        public int id_trabajo { get; set; }

        [JsonProperty("_bf")]
        public int id_taller { get; set; }

        [JsonProperty("_bg")]
        public int id_cliente { get; set; }

        [JsonProperty("_bh")]
        public string fecha_solic_repues { get; set; }

        [JsonProperty("_bi")]
        public string fecha_entrega_repues { get; set; }

        [JsonProperty("_bj")]
        public double margen_total { get; set; }

        [JsonProperty("_bk")]
        public double costo_mecanica { get; set; }

        [JsonProperty("_bl")]
        public double costo_chapa { get; set; }

        [JsonProperty("_bm")]
        public double costo_pintura { get; set; }

        [JsonProperty("_bn")]
        public string mat_pintura { get; set; }

        [JsonProperty("_bñ")]
        public string dias_para_entrega { get; set; }

        [JsonProperty("_bo")]
        public string nom_taller { get; set; }

        [JsonProperty("_bp")]
        public string nom_trabajo { get; set; }

        [JsonProperty("_bq")]
        public string nom_cliente { get; set; }

        [JsonProperty("_br")]
        public string nom_tecnico { get; set; }

        [JsonProperty("_bs")]
        public string desc_tecnico { get; set; }

        [JsonProperty("_bt")]
        public string nrochasis { get; set; }
    }

    public class Calculos
    {
        /// <summary>
        /// Inschcape
        /// 14/12/16
        /// </summary>
        public decimal montoSeguroChapaI {get;set;}

        public decimal montoSeguroPinturaI {get; set;}

        public decimal montoSeguroMecanicaI {get; set;}

        public decimal precioChapaI {get; set;}

        public decimal precioPañosI {get; set;}

        public decimal precioPinturaI {get; set;}

        public decimal precioMecanicaI {get ; set;}

        public decimal porcentajeChapaI {get; set;}

        public decimal porcentajeArmadoI { get; set;}

        public decimal formulaPinturaI { get; set; }

        /// <summary>
        /// Lima Autos
        /// 14/12/16
        /// </summary>
        public decimal montoSeguroChapaL { get; set; }

        public decimal montoSeguroPinturaL { get; set; }

        public decimal montoSeguroMecanicaL { get; set; }

        public decimal precioChapaL { get; set; }

        public decimal precioPañosL { get; set; }

        public decimal precioPinturaL { get; set; }

        public decimal precioMecanicaL { get; set; }

        public decimal porcentajeChapaL { get; set; }

        public decimal porcentajeArmadoL { get; set; }

        public decimal formulaPinturaL { get; set; }

    }

    /// <summary>
    /// Autor: Aldo DArrigo
    /// Fecha: 2016-12-12
    /// </summary>
    public class ListaTecnicosXservicio
    {

        [JsonProperty("_a")]
        public int id_tecnico { get; set; }

        [JsonProperty("_b")]
        public string nombres_tecnico { get; set; }

        [JsonProperty("_c")]
        public int id_servicio { get; set; }

        [JsonProperty("_d")]
        public string desripcion { get; set; }

        [JsonProperty("_e")]
        public string input { get; set; }

    }

    public class Vehiculo : AAutomecatronica
    {
        public string label { get; set; }

        public string modelo { get; set; }

        public string marca { get; set; }

        public string  color { get; set; }

        public string nrochasis { get; set; }


    }

    /// <summary>
    /// Autor: adarrigo
    /// Fecha: 2016-08-12
    /// </summary>
    public abstract class AAutomecatronica
    {
        /// <summary>
        /// Autor: adarrigo
        /// Fecha: 2016-08-12
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<AutomecatronicaLista> Lista(AutomecatronicaParametro parametro) {
            return MvcApplication._Deserialize<ResponseAutomecatronicaLista>(
                    MvcApplication._Servicio_Operativa.Automecatronica(
                        MvcApplication._Serialize(parametro)
                    )
                ).lista;
        }


        /// <summary>
        /// Autor: adarrigo
        /// Fecha: 2016-08-12
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<AutomecatronicaListaTecnicos> ListaTecnicos(AutomecatronicaParametro parametro)
        {
            return MvcApplication._Deserialize<ResponseAutomecatronicaListaTecnicos>(
                    MvcApplication._Servicio_Operativa.Automecatronica(
                        MvcApplication._Serialize(parametro)
                    )
                ).lista;
        }


        /// <summary>
        /// Autor: adarrigo
        /// Fecha: 2016-12-12
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<ListaTecnicosXservicio> ListaTecnicosXservicio(AutomecatronicaParametro parametro)
        {
            return MvcApplication._Deserialize<ResponseListaTecnicosXservicio>(
                    MvcApplication._Servicio_Operativa.Automecatronica(
                        MvcApplication._Serialize(parametro)
                    )
                ).listaTecnicosXservicio;
        }

        /// <summary>
        /// Autor: adarrigo
        /// Fecha: 2016-08-12
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public string GeneraId(AutomecatronicaParametro parametro)
        {
            return MvcApplication._Deserialize<ResponseAutomecatronica>(
                    MvcApplication._Servicio_Operativa.Automecatronica(
                        MvcApplication._Serialize(parametro)
                    )
                ).Objeto.id_Op;
        }

        /// <summary>
        /// Autor: adarrigo
        /// Fecha: 2016-19-12
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        /// 
        public List<Automecatronica> ListaAutomecatronica(AutomecatronicaParametro parametro)
        {
            return MvcApplication._Deserialize<ResponseAutomecatronicaListaOperacion>(
                        MvcApplication._Servicio_Operativa.Automecatronica(
                            MvcApplication._Serialize(parametro))
                    ).objL;
        }

        /// <summary>
        /// Autor: adarrigo
        /// Fecha: 2016-19-12
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        /// 
        public List<Vehiculo> ListaVehiculo(AutomecatronicaParametro parametro)
        {
            return MvcApplication._Deserialize<ResponseAutomecatronicaListaVehiculo>(
                        MvcApplication._Servicio_Operativa.Automecatronica(
                            MvcApplication._Serialize(parametro))
                    ).objL;
        }

        /// <summary>
        /// Autor:adarrigo
        /// Fecha:23/08/2016
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public Int32 InsertUpdate(AutomecatronicaParametro parametro)
        {
            return MvcApplication._Deserialize<ResponseAutomecatronicaInsertUpdate>(
                    MvcApplication._Servicio_Operativa.Automecatronica(
                        MvcApplication._Serialize(parametro)
                    )
                ).Respuesta;
        }

        /// <summary>
        /// Autor: adarrigo
        /// Fecha: 2016-08-12
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public Automecatronica Operacion(AutomecatronicaParametro parametro)
        {
            return MvcApplication._Deserialize<ResponseAutomecatronica>(
                    MvcApplication._Servicio_Operativa.Automecatronica(
                        MvcApplication._Serialize(parametro)
                    )
                ).Objeto;
        }
    }

    /// <summary>
    /// Autor: Aldo DArrigo
    /// Fecha: 2016-08-12
    /// </summary>
    public class AutomecatronicaParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }


    /// <summary>
    /// Autor: Aldo DArrigo
    /// Fecha: 2016-08-12
    /// </summary>
    public class AutomecatronicaLista
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string texto { get; set; }

    
    }
    /// <summary>
    /// Autor: Aldo DArrigo
    /// Fecha: 2016-08-12
    /// </summary>
    public class AutomecatronicaListaTecnicos
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string texto { get; set; }

        [JsonProperty("_c")]
        public int servicio { get; set; }
    }

    /// <summary>
    /// Autor: Aldo DArrigo
    /// Fecha: 2016-08-29
    /// </summary>
    public class AutomecatronicaTecnico
    {
        [JsonProperty("_a")]
        public int id_servicio { get; set; }

        [JsonProperty("_b")]
        public int id_tecnico { get; set; }

        [JsonProperty("_c")]
        public string ser_descripcion { get; set; }

        [JsonProperty("_d")]
        public string tec_nomnbre { get; set; }
    }

    /// <summary>
    /// Autor: Aldo DArrigo
    /// Fecha: 2016-09-01
    /// </summary>
    public class AutomecatronicaFoto
    {
        [JsonProperty("_a")]
        public string foto { get; set; }
        [JsonProperty("_b")]
        public int tipo_foto { get; set; }
    }

    public class ResponseListaTecnicosXservicio
    {
        [JsonProperty("_a")]
        public List<ListaTecnicosXservicio> listaTecnicosXservicio { get; set; }
    }

    /// <summary>
    /// Autor: adarrigo
    /// Fecha: 2016-08-12
    /// </summary>
    public class ResponseAutomecatronicaLista
    {
        [JsonProperty("_a")]
        public List<AutomecatronicaLista> lista { get;set; }
    }

    /// <summary>
    /// Autor: adarrigo
    /// Fecha: 2016-08-12
    /// </summary>
    public class ResponseAutomecatronicaListaTecnicos
    {
        [JsonProperty("_a")]
        public List<AutomecatronicaListaTecnicos> lista { get; set; }
    }

    /// <summary>
    /// Autor: adarrigo
    /// Fecha: 2016-08-12
    /// </summary>
    public class ResponseAutomecatronica
    {
        [JsonProperty("_a")]
        public Automecatronica Objeto { get; set; }
    }

    /// <summary>
    /// Autor: adarrigo
    /// Fecha: 2016-19-12
    /// </summary>
    public class ResponseAutomecatronicaListaOperacion{
    
        [JsonProperty("_a")]
        public List<Automecatronica> objL { get; set; }

    }

    /// <summary>
    /// Autor: adarrigo
    /// Fecha: 2016-19-12
    /// </summary>
    public class ResponseAutomecatronicaListaVehiculo
    {

        [JsonProperty("_a")]
        public List<Vehiculo> objL { get; set; }

    }

    /// <summary>
    /// Autor: adarrigo
    /// Fecha: 2016-19-12
    /// </summary>
    public class ResponseAutomecatronicaInsertUpdate
    {
        [JsonProperty("_a")]
        public int Respuesta { get; set; }
    }
    #endregion
}