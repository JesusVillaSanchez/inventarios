﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-16
    /// </summary>
    public class Departamento : ADepartamento
    {
        [JsonProperty("a")]
        public string dep_id { get; set; }

        [JsonProperty("b")]
        public string dep_descripcion { get; set; }
    }

    public abstract class ADepartamento {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-16
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Departamento> Lista(Request_Departamento_Por_Compania_Campania_Pais_Oficina oRq)
        {
            Response_Departamento_Por_Compania_Campania_Pais_Oficina oRp;

            oRp = MvcApplication._Deserialize<Response_Departamento_Por_Compania_Campania_Pais_Oficina>(
                    MvcApplication._Servicio_Campania.listarDepartamentos_Por_CodCliente_Por_CodCampania_Por_CodPais_CodOficina(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-06-01
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Departamento> ListaBackus(Request_GetDepartamento oRq)
        {
            Response_GetDepartamento oRp;

            oRp = MvcApplication._Deserialize<Response_GetDepartamento>(
                    MvcApplication._Servicio_Campania.Listar_Departamento_Por_CodCampania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-16
    /// </summary>
    public class Request_Departamento_Por_Compania_Campania_Pais_Oficina
    {
        [JsonProperty("a")]
        public string compania { get; set; }

        [JsonProperty("b")]
        public string campania { get; set; }

        [JsonProperty("c")]
        public string pais { get; set; }

        [JsonProperty("d")]
        public string oficina { get; set; }
    }
    public class Response_Departamento_Por_Compania_Campania_Pais_Oficina
    {
        [JsonProperty("a")]
        public List<Departamento> Lista { get; set; }
    }


    public class Request_GetDepartamento
    {
        [JsonProperty("a")]
        public string Cod_Compania { get; set; }
    }

    public class Response_GetDepartamento
    {
        [JsonProperty("a")]
        public List<Departamento> Lista { get; set; }
    }

    #endregion
}