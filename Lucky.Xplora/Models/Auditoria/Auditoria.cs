﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Auditoria
{
    public class Auditoria
    {
        public bool Audit_Descarga(RequestParametrosAuditoriaUpDownLoad oRq)
        {
            ResponseInsertaAuditoriaUpDownLoad oRp;

            oRp = MvcApplication._Deserialize<ResponseInsertaAuditoriaUpDownLoad>(
                MvcApplication._Servicio_Operativa.AuditoriaUpDownLoad(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.respuesta;
        }


    }

    #region Request-Response-

    public class RequestParametrosAuditoriaUpDownLoad
    {
        [JsonProperty("_a")]
        public string aud_ruta { get; set; }

        [JsonProperty("_b")]
        public int aud_tipo_carga { get; set; }

        [JsonProperty("_c")]
        public string aud_usuario { get; set; }

        [JsonProperty("_d")]
        public string aud_comentario { get; set; }
    }
    public class ResponseInsertaAuditoriaUpDownLoad
    {
        [JsonProperty("_a")]
        public bool respuesta { get; set; }
    }
    
    #endregion

}