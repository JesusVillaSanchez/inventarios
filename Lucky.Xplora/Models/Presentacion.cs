﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Presentacion : APresentacion
    {
        [JsonProperty("_a")]
        public int pre_id { get; set; }

        [JsonProperty("_b")]
        public string pre_descripcion { get; set; }
    }

    public abstract class APresentacion {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-24
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Presentacion> Lista(Request_GetPresentacion_Opcion_Parametro oRq)
        {
            Response_GetPresentacion_Opcion_Parametro oRp;

            oRp = MvcApplication._Deserialize<Response_GetPresentacion_Opcion_Parametro>(
                MvcApplication._Servicio_Campania.GetPresentacion_Opcion_Parametro(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-24
    /// </summary>
    public class Request_GetPresentacion_Opcion_Parametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }
    public class Response_GetPresentacion_Opcion_Parametro
    {
        [JsonProperty("_a")]
        public List<Presentacion> Lista { get; set; }
    }

    #endregion
}