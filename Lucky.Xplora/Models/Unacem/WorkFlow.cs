﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Models.Unacem
{
    public class WorkFlow
    {
        public List<E_Fachada_Unacem> Consulta_Solicitud(Request_WF_General oRq)
        {
            Response_FW_Consulta_solicitud oRp = MvcApplication._Deserialize<Response_FW_Consulta_solicitud>
                (MvcApplication._Servicio_Operativa.FW_Una_consulta_solicitud(MvcApplication._Serialize(oRq)));

            return oRp.Obj;
        }
        public string Agregar_solicitud(Request_FW_add_solicitud oRq)
        {
            Response_FW_add_solicitud oRp;

            oRp = MvcApplication._Deserialize<Response_FW_add_solicitud>(MvcApplication._Servicio_Operativa.FW_Una_add_solicitud(MvcApplication._Serialize(oRq)));

            return oRp.obj;
        }
        public E_WF_Grupo_Per Consultar_Grupo_Persona(Request_WF_Grupo_Per oRq)
        {
            Response_E_WF_Grupo_Per oRp;

            oRp = MvcApplication._Deserialize<Response_E_WF_Grupo_Per>(MvcApplication._Servicio_Operativa.WF_Una_Grupo_Per(MvcApplication._Serialize(oRq)));

            return oRp.Obj;
        }
        public E_WF_Apro_Solicitud_p2 P2_Aprobacion_Solicitud_Trade(Request_WF_General oRq)
        {
            Response_E_WF_Aprobacion_Solicitud_p2 oRp;

            oRp = MvcApplication._Deserialize<Response_E_WF_Aprobacion_Solicitud_p2>(MvcApplication._Servicio_Operativa.WF_Una_Apro_Solicitud_Trade_p2(MvcApplication._Serialize(oRq)));

            return oRp.obj;
        }

        public List<E_Nw_DetailGanttAssingLlenado> LlenarGantt(Request_WF_General oRq)
        {
            Response_E_WF_Llenado_Gantt oRp;

            oRp = MvcApplication._Deserialize<Response_E_WF_Llenado_Gantt>(
                    MvcApplication._Servicio_Operativa.WF_Una_Llenado_Gantt(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
        public E_Nw_DetailGanttRegistro Registrar_DetailGantt(Request_WF_DetailGantt oRq)
        {
            Response_E_WF_DetailGantt oRp;

            oRp = MvcApplication._Deserialize<Response_E_WF_DetailGantt>(
                    MvcApplication._Servicio_Operativa.WF_Una_Insert_DetailGantt(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        public E_WF_Fotomontaje AddUpt_Fotomontaje(Request_WF_General oRq)
        {
            Response_E_WF_AddUpt_Fotomontaje oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_AddUpt_Fotomontaje>(MvcApplication._Servicio_Operativa.WF_Una_Add_Upt_Fotomontaje(MvcApplication._Serialize(oRq)));
            return oRp.Obj;
        }
        public string AddFotomont_PrePresupuesto(Request_WF_General oRq)
        {
            Response_E_WF_Res_Fotomon_Prepresu oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Res_Fotomon_Prepresu>(MvcApplication._Servicio_Operativa.WF_Una_Add_Fotomon_Prepresu(MvcApplication._Serialize(oRq)));
            return oRp.Mensaje;
        }
        public string AsociacionPdv_PrePresupuesto(Request_WF_General oRq)
        {
            Response_E_WF_Res_Fotomon_Prepresu oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Res_Fotomon_Prepresu>(MvcApplication._Servicio_Operativa.WF_Una_Asociacion_Prepresupuesto(MvcApplication._Serialize(oRq)));
            return oRp.Mensaje;
        }
        public string Remove_SolicitudVentas(Request_WF_General oRq)
        {
            Response_E_WF_Res_Fotomon_Prepresu oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Res_Fotomon_Prepresu>(MvcApplication._Servicio_Operativa.WF_Unacem_Remove_Solicitud_Ventas(MvcApplication._Serialize(oRq)));
            return oRp.Mensaje;
        }
        public List<E_WF_Fotomontaje> Consul_FotoMontaje(Request_WF_General oRq)
        {
            Response_E_WF_Consul_Fotomontaje oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Consul_Fotomontaje>(MvcApplication._Servicio_Operativa.WF_Una_Consul_Fotomontaje(MvcApplication._Serialize(oRq)));
            return oRp.Obj;
        }
        public E_WF_Fotomontaje Ope_Paso4_Ventas(Request_WF_General oRq)
        {
            Response_E_WF_Paso4_Ventas oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso4_Ventas>(MvcApplication._Servicio_Operativa.WF_Una_Paso4_Ventas(MvcApplication._Serialize(oRq)));
            return oRp.Mensaje;
        }
        public string Ope_Paso5_Trade(Request_WF_General oRq)
        {
            Response_E_WF_Paso5_Trade oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso5_Trade>(MvcApplication._Servicio_Operativa.WF_Una_Paso5_Trade(MvcApplication._Serialize(oRq)));
            return oRp.Mensaje;
        }
        public List<E_WF_Fotomontaje> Ope_Paso6_LuckyAnalista(Request_WF_General oRq)
        {
            Response_E_WF_Paso6_LuckyAnalista oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso6_LuckyAnalista>(MvcApplication._Servicio_Operativa.WF_Una_Paso6_LuckyAnalista(MvcApplication._Serialize(oRq)));
            return oRp.obj;
        }
        public E_WF_Tramite Ope_Paso7_LuckyTramite(Request_WF_General oRq)
        {
            Response_E_WF_Paso7_LuckyTramite oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso7_LuckyTramite>(MvcApplication._Servicio_Operativa.WF_Una_Paso7_LuckyTramite(MvcApplication._Serialize(oRq)));
            return oRp.obj;
        }
        public string Ope_Paso7_LuckyAnalista(Request_WF_General oRq)
        {
            Response_E_WF_Paso7_LuckyAnalista oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso7_LuckyAnalista>(MvcApplication._Servicio_Operativa.WF_Una_Paso7_LuckyAnalista(MvcApplication._Serialize(oRq)));
            return oRp.dato;
        }
        public string Ope_Paso8_Trade(Request_WF_General oRq)
        {
            Response_E_WF_Paso8_Trade oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso8_Trade>(MvcApplication._Servicio_Operativa.WF_Una_Paso8_Trade(MvcApplication._Serialize(oRq)));
            return oRp.dato;
        }
        public List<E_WF_Visita> Ope_Visita(Request_WF_General oRq)
        {
            Response_E_WF_Visita oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Visita>(MvcApplication._Servicio_Operativa.WF_Una_Visita(MvcApplication._Serialize(oRq)));
            return oRp.Obj;
        }

        public List<E_Fachada_Unacem> Consulta_Mantenimiento(Request_WF_General oRq)
        {
            Response_E_WF_Consul_Matenimiento oRp = MvcApplication._Deserialize<Response_E_WF_Consul_Matenimiento>(MvcApplication._Servicio_Operativa.WF_Una_Consul_Mantenimiento(MvcApplication._Serialize(oRq)));

            return oRp.Obj;
        }
        public string Ope_Mantenimiento_Nuevo(Request_FW_add_Mantenimiento oRq)
        {
            Response_E_WF_Mant_new oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Mant_new>(MvcApplication._Servicio_Operativa.WF_Una_New_Mantenimiento(MvcApplication._Serialize(oRq)));
            return oRp.dato;
        }
        public string Ope_Mantenimiento_Pasos(Request_WF_General oRq)
        {
            Response_E_WF_Matenimiento_Pasos oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Matenimiento_Pasos>(MvcApplication._Servicio_Operativa.WF_Una_Mantenimiento_Pasos(MvcApplication._Serialize(oRq)));
            return oRp.dato;
        }

        public List<M_Combo> Filtro(Resquest_New_XPL_Una_WF oRq)
        {
            Response_New_XPL_SG_Una_WF oRp;

            oRp = MvcApplication._Deserialize<Response_New_XPL_SG_Una_WF>(
                    MvcApplication._Servicio_Operativa.Get_New_XPL_Unacem_WF_Filtros(
                        MvcApplication._Serialize(oRq)
                    )
                );
            if (oRp.Lista.Count == 0) { oRp.Lista.Add(new M_Combo() { Value = "0", Text = ".::Todos::." }); }

            return oRp.Lista;
        }
        public E_Nw_DetailGanttRegistro Actualizar_FechaEntrega_Foto(Request_WF_DetailGantt oRq)
        {
            Response_E_WF_DetailGantt oRp;

            oRp = MvcApplication._Deserialize<Response_E_WF_DetailGantt>(
                    MvcApplication._Servicio_Operativa.WF_Una_Update_Fotomontaje(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        //public E_Nw_DetailGanttRegistro Actualizar_FechaImplementacion(Request_WF_DetailGantt oRq)
        //{
        //    Response_E_WF_DetailGantt oRp;

        //    oRp = MvcApplication._Deserialize<Response_E_WF_DetailGantt>(
        //            MvcApplication._Servicio_Operativa.WF_Una_Update_Implementacion(
        //                MvcApplication._Serialize(oRq)
        //            )
        //        );

        //    return oRp.StatusRegistro;
        //}
        public string Actualizar_FechaImplementacion(Request_WF_General oRq)
        {
            Response_E_WF_Unacem oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Unacem>(
                MvcApplication._Servicio_Operativa.WF_Una_Update_Implementacion(
                MvcApplication._Serialize(oRq)
                )
              );
            return oRp.Mensaje;
        }



        public string Ope_Paso6_lucky(Request_WF_General oRq)
        {
            Response_E_WF_Paso6_Lucky oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso6_Lucky>(
                MvcApplication._Servicio_Operativa.WF_Una_Paso6_Lucky(
                MvcApplication._Serialize(oRq)
                )
              );
            return oRp.Mensaje;
        }
        public string Ope_Paso3_lucky(Request_WF_General oRq)
        {
            Response_E_WF_Paso3_Lucky oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso3_Lucky>(
                MvcApplication._Servicio_Operativa.WF_Una_Paso3_Lucky(
                MvcApplication._Serialize(oRq)
                )
              );
            return oRp.Mensaje;
        }

        public string Ope_Unacem(Request_WF_General oRq)
        {
            Response_E_WF_Unacem oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Unacem>(
                MvcApplication._Servicio_Operativa.WF_Una_Unacem(
                MvcApplication._Serialize(oRq)
                )
              );
            return oRp.Mensaje;
        }
        
        //Autor:wlopez
        //Fecha:23/11/2016
        public string AddPresupuesto_Final(Request_WF_General oRq)
        {
            Response_E_WF_Res_Presupuesto_Final oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Res_Presupuesto_Final>(MvcApplication._Servicio_Operativa.WF_Una_Add_PresupuestoFinal(MvcApplication._Serialize(oRq)));
            return oRp.Mensaje;
        }
        /// <summary>
        /// Autor:wlopez
        /// Fecha:09/12/2016
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public string DeleteArchive(Request_WF_General oRq)
        {
            Response_E_WF_DeleteArchive oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_DeleteArchive>(MvcApplication._Servicio_Operativa.WF_Una_DeleteArchive(MvcApplication._Serialize(oRq)));
            return oRp.Mensaje;
        }

        public List<E_Resumen_Presupuesto> Consulta_Resumen_Presupuesto(Request_WF_General oRq)
        {
            Response_FW_Resumen_Presupuesto oRp = MvcApplication._Deserialize<Response_FW_Resumen_Presupuesto>
                (MvcApplication._Servicio_Operativa.FW_Una_consulta_Resumen_Presupuesto(MvcApplication._Serialize(oRq)));

            return oRp.Obj;
        }

        public string Ope_Paso3_lucky_Tipo_Tramite(Request_WF_General oRq)
        {
            Response_E_WF_Paso3_Lucky_TipoTramite oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso3_Lucky_TipoTramite>(
                MvcApplication._Servicio_Operativa.WF_Una_Paso3_Lucky_TipoTramite(
                MvcApplication._Serialize(oRq)
                )
              );
            return oRp.Mensaje;
        }

        public string Ope_Paso3_lucky_Tiempo_Permiso(Request_WF_General oRq)
        {
            Response_E_WF_Paso3_Lucky_TiempoPermiso oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_Paso3_Lucky_TiempoPermiso>(
                MvcApplication._Servicio_Operativa.WF_Una_Paso3_Lucky_TiempoPermiso(
                MvcApplication._Serialize(oRq)
                )
              );
            return oRp.Mensaje;
        }

        /// <summary>
        /// Autor:wlopez
        /// Fecha:19/12/2016
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public E_WF_Fotomontaje AddUpt_FotoSolicitud(Request_WF_General oRq)
        {
            Response_E_WF_AddUpt_Fotomontaje oRp;
            oRp = MvcApplication._Deserialize<Response_E_WF_AddUpt_Fotomontaje>(MvcApplication._Servicio_Operativa.WF_Una_Add_Upt_Fotomontaje(MvcApplication._Serialize(oRq)));
            return oRp.Obj;
        }

        public List<E_WF_Tramite> Consulta_Grilla_Tramite(Request_WF_General oRq)
        {
            Response_FW_Consulta_Grilla_Tramite oRp = MvcApplication._Deserialize<Response_FW_Consulta_Grilla_Tramite>
                (MvcApplication._Servicio_Operativa.WF_Una_Paso7_LuckyTramite_Grilla_Historico(MvcApplication._Serialize(oRq)));

            return oRp.Obj;
        }
        

    }

    #region Entidades

    public class E_Fachada_Unacem
    {
        [JsonProperty("_a")]
        public string Num_Solicitud { get; set; }
        [JsonProperty("_b")]
        public string Fec_Solicitud { get; set; }
        [JsonProperty("_c")]
        public string Nom_PDV { get; set; }
        [JsonProperty("_d")]
        public string Num_RUC { get; set; }
        [JsonProperty("_e")]
        public string Codigo { get; set; }
        [JsonProperty("_f")]
        public string Direccion { get; set; }
        [JsonProperty("_g")]
        public string Distrito { get; set; }
        [JsonProperty("_h")]
        public string Provincia { get; set; }
        [JsonProperty("_i")]
        public string Departamento { get; set; }
        [JsonProperty("_j")]
        public string Contacto { get; set; }
        [JsonProperty("_k")]
        public string Num_Telef { get; set; }
        [JsonProperty("_l")]
        public string Num_Cel { get; set; }
        [JsonProperty("_m")]
        public string Distribuidora { get; set; }
        [JsonProperty("_n")]
        public string Foto1 { get; set; }
        [JsonProperty("_o")]
        public string Ven_Prom_Soles { get; set; }
        [JsonProperty("_p")]
        public string Ven_Prom_Ton { get; set; }
        [JsonProperty("_q")]
        public string Linea_Credito { get; set; }
        [JsonProperty("_r")]
        public string Deu_Actual { get; set; }
        [JsonProperty("_s")]
        public string Est_Solicitud { get; set; }
        [JsonProperty("_t")]
        public string Est_Proc_Solicitud { get; set; }
        [JsonProperty("_u")]
        public string Paso_1 { get; set; }
        [JsonProperty("_v")]
        public string Apro_Solicitud { get; set; }
        [JsonProperty("_w")]
        public string Fec_Apro_Solicitud { get; set; }
        [JsonProperty("_x")]
        public string Comen_Apro_Solicitud { get; set; }
        [JsonProperty("_y")]
        public int Paso_2 { get; set; }
        [JsonProperty("_z")]
        public string Url_Gantt_Foto { get; set; }
        [JsonProperty("_aa")]
        public string Url_Fotomontaje { get; set; }
        [JsonProperty("_ab")]
        public string Url_Presupuesto { get; set; }
        [JsonProperty("_ac")]
        public int Paso_3 { get; set; }

        [JsonProperty("_ad")]
        public string Apro_Fotomontaje { get; set; }
        [JsonProperty("_ae")]
        public string Fec_Apro_Fotomonataje { get; set; }
        [JsonProperty("_af")]
        public string Comen_Apro_Fotomontaje { get; set; }
        [JsonProperty("_ag")]
        public string Url_Carta_Fotomontaje { get; set; }
        [JsonProperty("_ah")]
        public int Paso_4 { get; set; }

        [JsonProperty("_ai")]
        public string Apro_pre_Presu { get; set; }
        [JsonProperty("_aj")]
        public string Fec_Apro_pre_Presu { get; set; }
        [JsonProperty("_ak")]
        public string Orden_Compra { get; set; }
        [JsonProperty("_al")]
        public string HES { get; set; }
        [JsonProperty("_am")]
        public string Comen_Apro_Pre_Presu { get; set; }
        [JsonProperty("_an")]
        public int Paso_5 { get; set; }

        [JsonProperty("_ao")]
        public string Url_Gantt_Imple { get; set; }
        [JsonProperty("_ap")]
        public int Est_Doc_Requisito { get; set; }
        [JsonProperty("_aq")]
        public int Cantidad { get; set; }
        [JsonProperty("_ar")]
        public int Paso_6 { get; set; }

        [JsonProperty("_as")]
        public string Fec_ingre_expe { get; set; }
        [JsonProperty("_at")]
        public string Url_Rec_Tramite_Muni { get; set; }
        [JsonProperty("_au")]
        public string Obs_Tramite { get; set; }
        [JsonProperty("_av")]
        public string Fec_Entre_Licencia { get; set; }
        [JsonProperty("_aw")]
        public string Fec_Cadu_Licencia { get; set; }
        [JsonProperty("_ax")]
        public string Url_Licencia { get; set; }
        [JsonProperty("_ay")]
        public string Url_Report_Fotografico { get; set; }
        [JsonProperty("_az")]
        public string Url_Presu_Final { get; set; }
        [JsonProperty("_ba")]
        public int Paso_7 { get; set; }

        [JsonProperty("_bb")]
        public string Vali_Fechada_Imple { get; set; }
        [JsonProperty("_bc")]
        public string Fec_Vali_Fachada_Imple { get; set; }
        [JsonProperty("_bd")]
        public int Paso_8 { get; set; }

        [JsonProperty("_be")]
        public string Finalizado { get; set; }
        [JsonProperty("_bf")]
        public string Pendiente { get; set; }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2016-06-09
        /// </summary>
        [JsonProperty("_bj")]
        public string P1_Usuario_Solicita { get; set; }
        [JsonProperty("_bk")]
        public int P1_idZona { get; set; }
        [JsonProperty("_bl")]
        public string P1_Comentarios { get; set; }
        [JsonProperty("_bm")]
        public string P2_Fecha_Entrega_Foto { get; set; }
        [JsonProperty("_bn")]
        public string P1_Tipo_Solicitud { get; set; }

        [JsonProperty("_bo")]
        public string p3_monto_presupuesto { get; set; }
        [JsonProperty("_bp")]
        public string p3_codigo_presupuesto { get; set; }
        [JsonProperty("_bq")]
        public string wf_monto_total { get; set; }
        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2016-09-08
        /// </summary>
        [JsonProperty("_br")]
        public string chk_fila { get; set; }
        [JsonProperty("_bs")]
        public string comen_expediente { get; set; }
        [JsonProperty("_bt")]
        public string fecha_implementacion { get; set; }
        [JsonProperty("_bu")]
        public int Provider_code { get; set; }
        [JsonProperty("_bv")]
        public int marca { get; set; }
        [JsonProperty("_bw")]
        public string p3_monto_presupuesto_final { get; set; }
        [JsonProperty("_bx")]
        public string p3_codigo_presupuesto_final { get; set; }
        [JsonProperty("_by")]
        public string Url_Carta_Presentacion { get; set; }
        [JsonProperty("_bz")]
        public string diferencia_montos { get; set; }
        [JsonProperty("_ca")]
        public string Foto2 { get; set; }
        [JsonProperty("_cb")]
        public string Foto3 { get; set; }
        [JsonProperty("_cc")]
        public string Foto4 { get; set; }
        [JsonProperty("_cd")]
        public string Foto5 { get; set; }
        [JsonProperty("_ce")]
        public int Tipo_Tramite { get; set; }
        [JsonProperty("_cf")]
        public int Tiempo_permiso { get; set; }
        [JsonProperty("_cg")]
        public int Tipo_GralSolicitud { get; set; }
        [JsonProperty("_ch")]
        public int Estado { get; set; }
        [JsonProperty("_ci")]
        public string Url_Fotomontaje_Orden { get; set; }
        [JsonProperty("_cj")]
        public string PrimerNombreUsuario { get; set; }
        [JsonProperty("_ck")]
        public string SegundoNombreUsuario { get; set; }
        [JsonProperty("_cl")]
        public string PrimerApellidoUsuario { get; set; }
        [JsonProperty("_cm")]
        public string SegundoApellidoUsuario { get; set; }
        [JsonProperty("_cn")]
        public string DescripcionZona { get; set; }
        [JsonProperty("_co")]
        public string DescripcionMarca { get; set; }
        [JsonProperty("_cp")]
        public string DescripcionTipoSolicitud { get; set; }
        [JsonProperty("_cq")]
        public string DescripcionTipoTramite { get; set; }
        [JsonProperty("_cr")]
        public string DescripcionTiempoPermiso { get; set; }
        [JsonProperty("_cs")]
        public string DescripcionProveedor { get; set; }
        [JsonProperty("_ct")]
        public string DescripcionTipoTramite2 { get; set; }
        [JsonProperty("_cu")]
        public string DescripcionEstadoTramite { get; set; }
        [JsonProperty("_cv")]
        public string RutaFotoSolicitud { get; set; }
        [JsonProperty("_cw")]
        public string RutaCompletaFotoSolicitud { get; set; }
        [JsonProperty("_cx")]
        public string FechaFotoSolicitud { get; set; }
        [JsonProperty("_cy")]
        public string RutaFotoFotomontaje { get; set; }
        [JsonProperty("_cz")]
        public string RutaCompletaFotoFotomontaje { get; set; }
        [JsonProperty("_da")]
        public string FechaFotoFotomontaje { get; set; }
        [JsonProperty("_db")]
        public string RutaFotoFotografico { get; set; }
        [JsonProperty("_dc")]
        public string RutaCompletaFotoFotografico { get; set; }
        [JsonProperty("_dd")]
        public string FechaFotoFotografico { get; set; }
        [JsonProperty("_de")]
        public string PrimerNombrePrimerApellidoUsuario { get; set; }
        [JsonProperty("_df")]
        public string fecha_p5_trade { get; set; }
        [JsonProperty("_dg")]
        public string Apro_Solicitud_FV { get; set; }
        [JsonProperty("_dh")]
        public string Fec_Apro_Solicitud_FV { get; set; }
        [JsonProperty("_di")]
        public string Id_Registro { get; set; }
        [JsonProperty("_dj")]
        public string Referencia { get; set; }
        [JsonProperty("_dk")]
        public string Comen_Apro_Solicitud_fv { get; set; }
        
    }
    public class E_WF_Grupo_Per
    {
        [JsonProperty("_a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("_b")]
        public string Grupo { get; set; }
    }
    public class E_WF_Apro_Solicitud_p2
    {
        [JsonProperty("_a")]
        public string Fecha { get; set; }
        [JsonProperty("_b")]
        public string Comentario { get; set; }
    }
    public class E_Nw_DetailGanttLlenado
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("code")]
        public string code { get; set; }
        [JsonProperty("level")]
        public int level { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
        [JsonProperty("canWrite")]
        public bool canWrite { get; set; }
        [JsonProperty("start")]
        public string start { get; set; }
        [JsonProperty("duration")]
        public int duration { get; set; }
        [JsonProperty("end")]
        public string gend { get; set; }
        [JsonProperty("startIsMilestone")]
        public bool startIsMilestone { get; set; }
        [JsonProperty("endIsMilestone")]
        public bool endIsMilestone { get; set; }
        [JsonProperty("collapsed")]
        public bool collapsed { get; set; }
        //[JsonProperty("assigs")]
        //public List<E_Detail_assigs> assigs { get; set; }
        [JsonProperty("assigs")]
        public string assigs { get; set; }
        [JsonProperty("hasChild")]
        public bool hasChild { get; set; }
        [JsonProperty("progress")]
        public int progress { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
        [JsonProperty("depends")]
        public string depends { get; set; }
    }
    public class E_Nw_DetailGanttAssingLlenado
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("code")]
        public string code { get; set; }
        [JsonProperty("level")]
        public int level { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
        [JsonProperty("canWrite")]
        public bool canWrite { get; set; }
        [JsonProperty("start")]
        public float start { get; set; }
        [JsonProperty("duration")]
        public int duration { get; set; }
        [JsonProperty("end")]
        public float gend { get; set; }
        [JsonProperty("startIsMilestone")]
        public bool startIsMilestone { get; set; }
        [JsonProperty("endIsMilestone")]
        public bool endIsMilestone { get; set; }
        [JsonProperty("collapsed")]
        public bool collapsed { get; set; }
        [JsonProperty("assigs")]
        public List<E_Detail_assigs> assigs { get; set; }
        [JsonProperty("hasChild")]
        public bool hasChild { get; set; }
        [JsonProperty("progress")]
        public int progress { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
        [JsonProperty("depends")]
        public string depends { get; set; }
    }
    public class E_Detail_assigs
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("resourceId")]
        public string resourceId { get; set; }
        [JsonProperty("roleId")]
        public string roleId { get; set; }
        [JsonProperty("effot")]
        public string effot { get; set; }
    }
    public class E_Nw_DetailGanttRegistro
    {
        public const int REGISTRO_OK = 1;
        public const int GENERAL_ERROR = 2;

        public int CodStatus { get; set; }
        public string Mensaje { get; set; }
    }
    public class E_Nw_Detail_Gantt
    {
        [JsonProperty("_a")]
        public string id { get; set; }
        [JsonProperty("_b")]
        public string name { get; set; }
        [JsonProperty("_c")]
        public string code { get; set; }
        [JsonProperty("_d")]
        public int level { get; set; }
        [JsonProperty("_e")]
        public string status { get; set; }
        [JsonProperty("_f")]
        public bool canWrite { get; set; }
        [JsonProperty("_g")]
        public string start { get; set; }
        [JsonProperty("_h")]
        public int duration { get; set; }
        [JsonProperty("_i")]
        public string gend { get; set; }
        [JsonProperty("_j")]
        public bool startIsMilestone { get; set; }
        [JsonProperty("_k")]
        public bool endIsMilestone { get; set; }
        [JsonProperty("_l")]
        public bool collapsed { get; set; }
        [JsonProperty("_m")]
        public string assigs { get; set; }
        [JsonProperty("_n")]
        public bool hasChild { get; set; }
        [JsonProperty("_o")]
        public int progress { get; set; }
        [JsonProperty("_p")]
        public string description { get; set; }
        [JsonProperty("_q")]
        public string depends { get; set; }
        [JsonProperty("_r")]
        public string Promotion_CreateBy { get; set; }
    }
    public class E_WF_Fotomontaje
    {
        [JsonProperty("_a")]
        public int Cod_Reg_Table { get; set; }
        [JsonProperty("_b")]
        public int Orden { get; set; }
        [JsonProperty("_c")]
        public string Url { get; set; }
        [JsonProperty("_d")]
        public string Url_Fotomontaje { get; set; }
        [JsonProperty("_e")]
        public string Fecha { get; set; }
        [JsonProperty("_f")]
        public string Comment { get; set; }
        [JsonProperty("_g")]
        public string Estado { get; set; }
        [JsonProperty("_h")]
        public string Nombre_Archivo { get; set; }
    }
    public class E_WF_Tramite
    {
        [JsonProperty("_a")]
        public string Fec_Ingreso_expe { get; set; }
        [JsonProperty("_b")]
        public string Url_rec_tramite { get; set; }
        [JsonProperty("_c")]
        public string Obs_Tramite { get; set; }
        [JsonProperty("_d")]
        public string Fec_Entre_Licencia { get; set; }
        [JsonProperty("_e")]
        public string fec_cadu_licencia { get; set; }
        [JsonProperty("_f")]
        public string Url_Licencia { get; set; }
        [JsonProperty("_g")]
        public string Num_solicitud { get; set; }
        [JsonProperty("_h")]
        public string Monto_Presupuesto_Final { get; set; }
        [JsonProperty("_i")]
        public string Codigo_Presupuesto_Final { get; set; }
        [JsonProperty("_j")]
        public string Url_Presupuesto_Final { get; set; }
    }
    public class E_WF_Visita
    {
        [JsonProperty("_a")]
        public int Num_Solicitud { get; set; }
        [JsonProperty("_b")]
        public int Orden { get; set; }
        [JsonProperty("_c")]
        public string Fecha { get; set; }
        [JsonProperty("_d")]
        public string Url_Img { get; set; }
        [JsonProperty("_e")]
        public string Est_Alerta { get; set; }
    }
    public class Request_FW_add_Mantenimiento
    {
        [JsonProperty("_a")]
        public int Id_Reg { get; set; }
        [JsonProperty("_b")]
        public string foto { get; set; }
        [JsonProperty("_c")]
        public string ven_prom_soles { get; set; }
        [JsonProperty("_d")]
        public string ven_prom_ton { get; set; }
        [JsonProperty("_e")]
        public string linea_credito { get; set; }
        [JsonProperty("_f")]
        public string deu_actual { get; set; }
        [JsonProperty("_g")]
        public int Cod_Op { get; set; }
        [JsonProperty("_h")]
        public int Estado { get; set; }
        [JsonProperty("_i")]
        public int Cod_Detalle { get; set; }
    }

    public class E_Resumen_Presupuesto
    {
        [JsonProperty("_a")]
        public string Tipo_Solicitud { get; set; }
        [JsonProperty("_b")]
        public string Monto_Soles { get; set; }
        [JsonProperty("_c")]
        public string Porc_Soles { get; set; }
        [JsonProperty("_d")]
        public string Cantidad_Solicitud { get; set; }
        [JsonProperty("_e")]
        public string Porc_Cantidad_Solicitud { get; set; }
    }

    public class Rep_Una_Fachada
    {
        [JsonProperty("_a")]
        public string Zona { get; set; }
        [JsonProperty("_b")]
        public int CantZona { get; set; }
        [JsonProperty("_c")]
        public string colorZona { get; set; }
        [JsonProperty("_d")]
        public string EreDescripcion { get; set; }
    }
    public class Rep_Una_Permiso
    {
        [JsonProperty("_a")]
        public string Zona { get; set; }
        [JsonProperty("_b")]
        public int CantZona { get; set; }
        [JsonProperty("_c")]
        public string colorZona { get; set; }
        [JsonProperty("_d")]
        public string EreDescripcion { get; set; }
    }
    public class Rep_Una_Presupuestal
    {
        [JsonProperty("_a")]
        public string DescripcionMes { get; set; }
        [JsonProperty("_b")]
        public string EreDescripcion { get; set; }
        [JsonProperty("_c")]
        public string Color_Descripcion{ get; set; }
        [JsonProperty("_c")]
        public string Cantidad_Descripcion { get; set; }
    }

    public class Autocomp_Nueva_Solicitud : AAutocomp_Solicitud
    {
        public string label { get; set; }
        public string nom_pdv { get; set; }
        public string num_RUC { get; set; }
        public string marca { get; set; }
        public string cod_pdv { get; set; }
        public string num_celular { get; set; }
        public string id_tiposolicitud { get; set; }
        public string departamento { get; set; }
        public string contacto { get; set; }
        public string provincia { get; set; }
        public string telefono { get; set; }
        public string distrito { get; set; }
        public string direccion { get; set; }
        public string distribuidora { get; set; }
        public string comentarios_solicitud { get; set; }
        public string id_zona { get; set; }
        public string num_solicitud { get; set; }
        public string fotos { get; set; }
        public string estado_solicitud { get; set; }

    }

    public abstract class AAutocomp_Solicitud
    {
        /// <summary>
        /// Autor: wlopez
        /// Fecha: 22/12/2016
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<Autocomp_Nueva_Solicitud> ListaData_Nueva_solicitud(Request_Autocomplete_Solicitud parametro)
        {
            return MvcApplication._Deserialize<ResponseAutocpmp_Nueva_Solicitud>(
                        MvcApplication._Servicio_Operativa.Autocomplete_NuevaSolicitud(
                            MvcApplication._Serialize(parametro))
                    ).objL;
        }
    }


    #endregion
    #region Request - Response
    public class Request_FW_add_solicitud
    {
        [JsonProperty("_a")]
        public string pdv { get; set; }
        [JsonProperty("_b")]
        public string ruc { get; set; }
        [JsonProperty("_c")]
        public string codigo { get; set; }
        [JsonProperty("_d")]
        public string direccion { get; set; }
        [JsonProperty("_e")]
        public string distrito { get; set; }
        [JsonProperty("_f")]
        public string provincia { get; set; }
        [JsonProperty("_g")]
        public string departamento { get; set; }
        [JsonProperty("_h")]
        public string contacto { get; set; }
        [JsonProperty("_i")]
        public string numtelef { get; set; }
        [JsonProperty("_j")]
        public string numcel { get; set; }
        [JsonProperty("_k")]
        public string distribuidora { get; set; }
        [JsonProperty("_l")]
        public string foto { get; set; }
        [JsonProperty("_m")]
        public string ven_prom_soles { get; set; }
        [JsonProperty("_n")]
        public string ven_prom_ton { get; set; }
        [JsonProperty("_o")]
        public string linea_credito { get; set; }
        [JsonProperty("_p")]
        public string deu_actual { get; set; }
        [JsonProperty("_q")]
        public string op_grabado { get; set; }
        [JsonProperty("_r")]
        public int ids { get; set; }
        [JsonProperty("_s")]
        public int op_opera { get; set; }
        [JsonProperty("_t")]
        public int p1_codperson { get; set; }
        [JsonProperty("_u")]
        public string p1_codTipo { get; set; }
        [JsonProperty("_v")]
        public int p1_codZona { get; set; }
        [JsonProperty("_w")]
        public string p1_comentarios { get; set; }
        [JsonProperty("_y")]
        public int marca { get; set; }
        [JsonProperty("_x")]
        public string cod_equipo { get; set; }
        [JsonProperty("_z")]
        public int tipo_gralSolicitud { get; set; }
        [JsonProperty("_aa")]
        public string referencia { get; set; }

    }
    public class Request_WF_Grupo_Per
    {
        [JsonProperty("_a")]
        public int Codigo { get; set; }
    }
    public class Request_WF_General
    {
        [JsonProperty("_a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("_b")]
        public int Num_Solicitud { get; set; }
        [JsonProperty("_c")]
        public int Estado { get; set; }
        [JsonProperty("_d")]
        public string Comen_Apro { get; set; }
        [JsonProperty("_e")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("_f")]
        public int Id_Reg { get; set; }
        [JsonProperty("_g")]
        public int Cod_Op { get; set; }
        [JsonProperty("_h")]
        public int Cod_Area { get; set; }
        [JsonProperty("_i")]
        public string Fecha { get; set; }
        [JsonProperty("_j")]
        public int Cod_Persona { get; set; }
        [JsonProperty("_k")]
        public string img { get; set; }
        [JsonProperty("_l")]
        public int orden { get; set; }
        [JsonProperty("_m")]
        public string Ord_Pago { get; set; }
        [JsonProperty("_n")]
        public string Hes { get; set; }
        [JsonProperty("_o")]
        public string Ord_Comentario { get; set; }
        [JsonProperty("_p")]
        public string Documento { get; set; }

        [JsonProperty("_q")]
        public string fec_ing_expe { get; set; }
        [JsonProperty("_r")]
        public string ruta_recibo { get; set; }
        [JsonProperty("_s")]
        public string obs_recibo { get; set; }
        [JsonProperty("_t")]
        public string fec_licencia { get; set; }
        [JsonProperty("_u")]
        public string fec_vencimiento { get; set; }
        [JsonProperty("_v")]
        public string ruta_licencia { get; set; }

        [JsonProperty("_w")]
        public int cod_zona { get; set; }
        [JsonProperty("_x")]
        public string fecha_solicitud { get; set; }
        [JsonProperty("_y")]
        public string fecha_foto { get; set; }
        [JsonProperty("_z")]
        public string distribuidora { get; set; }

        [JsonProperty("_aa")]
        public string Coment_Fotomontaje { get; set; }

        [JsonProperty("_ab")]
        public string Monto_Presupuesto { get; set; }
        [JsonProperty("_ac")]
        public string Codigo_Presupuesto { get; set; }

        [JsonProperty("_ad")]
        public string str_codigo_Presupuesto { get; set; }
        [JsonProperty("_ae")]
        public string fecha_presupuesto { get; set; }
        [JsonProperty("_af")]
        public string comen_expediente { get; set; }
        [JsonProperty("_ag")]
        public string chk_fila { get; set; }
        [JsonProperty("_ah")]
        public string fecha_implementacion { get; set; }
        [JsonProperty("_aj")]
        public int Provider_code { get; set; }
        [JsonProperty("_ai")]
        public string cod_equipo { get; set; }
        [JsonProperty("_ak")]
        public string Monto_Presupuesto_Final { get; set; }
        [JsonProperty("_al")]
        public string Codigo_Presupuesto_Final { get; set; }
        [JsonProperty("_am")]
        public string Nombre_Registro { get; set; }
        [JsonProperty("_an")]
        public int Tipo_Tramite { get; set; }
        [JsonProperty("_ao")]
        public int Tiempo_Permiso { get; set; }
        [JsonProperty("_ap")]
        public int Cod_Marca { get; set; }
        [JsonProperty("_aq")]
        public string Url_Presu_Final { get; set; }
        [JsonProperty("_ar")]
        public int status { get; set; }
    
    }
    public class Request_WF_DetailGantt
    {
        [JsonProperty("_a")]
        public List<E_Nw_Detail_Gantt> oDetailGantt { get; set; }
        [JsonProperty("_b")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("_c")]
        public int Cod_Reg_Tabla { get; set; }
        [JsonProperty("_d")]
        public int Cod_Opcion { get; set; }
        [JsonProperty("_e")]
        public string strlist_promocion { get; set; }
        [JsonProperty("_f")]
        public string fecha_entrega { get; set; }
        [JsonProperty("_g")]
        public string fecha_implementacion { get; set; }
    }
    public class Resquest_New_XPL_Una_WF
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string parametros { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int opcion { get; set; }
    }

    public class Request_Autocomplete_Solicitud
    {
        [JsonProperty("_a")]
        public int Opcion { get; set; }
        [JsonProperty("_b")]
        public string Parametro { get; set; }
        [JsonProperty("_c")]
        public int tipo { get; set; }
    }

    public class Request_UnaReporting_Parametros
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }
        [JsonProperty("_b")]
        public string Parametro { get; set; }
    }
    
    public class Response_FW_Consulta_solicitud 
    {
        [JsonProperty("_a")]
        public List<E_Fachada_Unacem> Obj { get; set; }
    }
    public class Response_FW_Consulta_Grilla_Tramite 
    {
        [JsonProperty("_a")]
        public List<E_WF_Tramite> Obj { get; set; }
    }

    
    public class Response_FW_add_solicitud 
    {
        [JsonProperty("_a")]
        public string obj { get; set; }
    }
    public class Response_E_WF_Grupo_Per
    {
        [JsonProperty("_a")]
        public E_WF_Grupo_Per Obj { get; set; }
    }
    public class Response_E_WF_Aprobacion_Solicitud_p2
    {
        [JsonProperty("_a")]
        public E_WF_Apro_Solicitud_p2 obj { get; set; }
    }
    public class Response_E_WF_Llenado_Gantt
    {
        [JsonProperty("_a")]
        public List<E_Nw_DetailGanttAssingLlenado> Response { get; set; }
    }
    public class Response_New_XPL_SG_Una_WF
    {
        [JsonProperty("_a")]
        public List<M_Combo> Lista { get; set; }
    }

    public class Response_E_WF_DetailGantt 
    {
        [JsonProperty("_a")]
        public E_Nw_DetailGanttRegistro StatusRegistro { get; set; }
    }
    public class Response_E_WF_AddUpt_Fotomontaje
    {
        [JsonProperty("_a")]
        public E_WF_Fotomontaje Obj { get; set; }

    }
    public class Response_E_WF_Res_Fotomon_Prepresu
    {
        [JsonProperty("_a")]
        public string Mensaje { get; set; }
    }
    public class Response_E_WF_Consul_Fotomontaje
    {
        [JsonProperty("_a")]
        public List<E_WF_Fotomontaje> Obj { get; set; }
    }
    public class Response_E_WF_Paso4_Ventas
    {
        [JsonProperty("_a")]
        public E_WF_Fotomontaje Mensaje { get; set; }
    }
    public class Response_E_WF_Paso5_Trade
    {
        [JsonProperty("_a")]
        public string Mensaje { get; set; }
    }
    public class Response_E_WF_Paso6_LuckyAnalista
    {
        [JsonProperty("_a")]
        public List<E_WF_Fotomontaje> obj { get; set; }
    }
    public class Response_E_WF_Paso7_LuckyTramite
    {
        [JsonProperty("_a")]
        public E_WF_Tramite obj { get; set; }
    }
    public class Response_E_WF_Paso7_LuckyAnalista
    {
        [JsonProperty("_a")]
        public string dato { get; set; }
    }
    public class Response_E_WF_Paso8_Trade
    {
        [JsonProperty("_a")]
        public string dato { get; set; }
    }
    public class Response_E_WF_Visita
    {
        [JsonProperty("_a")]
        public List<E_WF_Visita> Obj { get; set; }
    }

    public class Response_E_WF_Consul_Matenimiento 
    {
        [JsonProperty("_a")]
        public List<E_Fachada_Unacem> Obj { get; set; }
    }
    public class Response_E_WF_Mant_new 
    {
        [JsonProperty("_a")]
        public string dato { get; set; }
    }

    public class Response_E_WF_Paso6_Lucky
    {
        [JsonProperty("_a")]
        public string Mensaje { get; set; }
    }

    public class Response_E_WF_Paso3_Lucky
    {
        [JsonProperty("_a")]
        public string Mensaje { get; set; }
    }

    public class Response_E_WF_Matenimiento_Pasos 
    {
        [JsonProperty("_a")]
        public string dato { get; set; }
    }

    public class Response_E_WF_Unacem
    {
        [JsonProperty("_a")]
        public string Mensaje { get; set; }
    }

    public class Response_E_WF_Res_Presupuesto_Final
    {
        [JsonProperty("_a")]
        public string Mensaje { get; set; }
    }
    /// <summary>
    /// Autor:wlopez
    /// Fecha:09/12/2016
    /// </summary>
    public class Response_E_WF_DeleteArchive
    {
        [JsonProperty("_a")]
        public string Mensaje { get; set; }
    }

    public class Response_FW_Resumen_Presupuesto
    {
        [JsonProperty("_a")]
        public List<E_Resumen_Presupuesto> Obj { get; set; }
    }

    public class Response_E_WF_Paso3_Lucky_TipoTramite
    {
        [JsonProperty("_a")]
        public string Mensaje { get; set; }
    }

    public class Response_E_WF_Paso3_Lucky_TiempoPermiso
    {
        [JsonProperty("_a")]
        public string Mensaje { get; set; }
    }

    public class ResponseAutocpmp_Nueva_Solicitud
    {

        [JsonProperty("_a")]
        public List<Autocomp_Nueva_Solicitud> objL { get; set; }

    }
    #endregion
}