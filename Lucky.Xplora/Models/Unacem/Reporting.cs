﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Unacem
{
    #region Reporting
    /// <summary>
    /// Autor: wlopez
    /// Fecha: 2017-01-14
    /// </summary>
    public class UnacemReporting : AUnacemReporting
    {
        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2017-01-14
        /// </summary>
        /// <param name="Parametro"></param>
        /// <returns></returns>
        public override List<UnacemReportingStatusFachada> ListaStatusFachada(UnacemReportingParametro Parametro)
        {
            return MvcApplication._Deserialize<ResponseUnacemReportingStatusFachada>(
                    MvcApplication._Servicio_Operativa.UnacemReporting(MvcApplication._Serialize(Parametro))
                ).Lista;
        }
        public override List<UnacemReportingStatusFachadaDescarga> ListaStatusFachadaDescarga (UnacemReportingParametro Parametro)
        {
            return MvcApplication._Deserialize<ResponseUnacemReportingStatusFachadaDescarga>(
                    MvcApplication._Servicio_Operativa.UnacemReporting(MvcApplication._Serialize(Parametro))
                ).Lista;
        }
        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2017-01-14
        /// </summary>
        /// <param name="Parametro"></param>
        /// <returns></returns>
        public override List<UnacemReportingStatusPresupuestal> ListaStatusPresupuestal(UnacemReportingParametro Parametro)
        {
            return MvcApplication._Deserialize<ResponseUnacemReportingStatusPresupuestal>(
                    MvcApplication._Servicio_Operativa.UnacemReporting(MvcApplication._Serialize(Parametro))
                ).Lista;
        }
    }

    /// <summary>
    /// Autor: wlopez
    /// Fecha: 2017-01-14
    /// </summary>
    public class UnacemReportingParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }

    /// <summary>
    /// Autor: wlopez
    /// Fecha: 2017-01-14
    /// </summary>
    public class UnacemReportingStatusFachada
    {
        [JsonProperty("_a")]
        public int EreId { get; set; }

        [JsonProperty("_b")]
        public string EreDescripcion { get; set; }

        [JsonProperty("_c")]
        public string EreColor { get; set; }

        [JsonProperty("_d")]
        public List<UnacemReportingStatusFachadaZona> ListaZona { get; set; }
    }

    public class UnacemReportingStatusFachadaDescarga
    {
        
        [JsonProperty("_a")]
        public string EreDescripcion { get; set; }

        [JsonProperty("_b")]
        public string ZonDescripcion { get; set; }

        [JsonProperty("_c")]
        public int Cantidad { get; set; }

        [JsonProperty("_d")]
        public string Porcentaje { get; set; }
    }


    /// <summary>
    /// Autor: wlopez
    /// Fecha: 2017-01-14
    /// </summary>
    public class UnacemReportingStatusFachadaZona
    {
        [JsonProperty("_a")]
        public int ZonId { get; set; }

        [JsonProperty("_b")]
        public string ZonDescripcion { get; set; }

        [JsonProperty("_c")]
        public int Cantidad { get; set; }

        [JsonProperty("_d")]
        public string Porcentaje { get; set; }
    }

    /// <summary>
    /// Autor: wlopez
    /// Fecha: 2017-01-14
    /// </summary>
    public class UnacemReportingStatusPresupuestal
    {
        [JsonProperty("_a")]
        public int Anio { get; set; }

        [JsonProperty("_b")]
        public int MesId { get; set; }

        [JsonProperty("_c")]
        public string MesDescripcion { get; set; }

        [JsonProperty("_d")]
        public int Solicitada { get; set; }

        [JsonProperty("_e")]
        public int Aprobada { get; set; }

        [JsonProperty("_f")]
        public string Inversion { get; set; }

        [JsonProperty("_g")]
        public int Implementada { get; set; }

        [JsonProperty("_h")]
        public string InversionFachada { get; set; }

        [JsonProperty("_i")]
        public string InversionAcumulada { get; set; }
    }

    /// <summary>
    /// Autor: wlopez
    /// Fecha: 2017-01-14
    /// </summary>
    public abstract class AUnacemReporting
    {
        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2017-01-14
        /// </summary>
        /// <param name="Parametro"></param>
        /// <returns></returns>
        public abstract List<UnacemReportingStatusFachada> ListaStatusFachada(UnacemReportingParametro Parametro);

        public abstract List<UnacemReportingStatusFachadaDescarga> ListaStatusFachadaDescarga(UnacemReportingParametro Parametro);

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2017-01-14
        /// </summary>
        /// <param name="Parametro"></param>
        /// <returns></returns>
        public abstract List<UnacemReportingStatusPresupuestal> ListaStatusPresupuestal(UnacemReportingParametro Parametro);
    }

    #region Response
    /// <summary>
    /// Autor: wlopez
    /// Fecha: 2017-01-14
    /// </summary>
    public class ResponseUnacemReportingStatusFachada
    {
        [JsonProperty("_a")]
        public List<UnacemReportingStatusFachada> Lista { get; set; }
    }
    public class ResponseUnacemReportingStatusFachadaDescarga
    {
        [JsonProperty("_a")]
        public List<UnacemReportingStatusFachadaDescarga> Lista { get; set; }
    }

    /// <summary>
    /// Autor: wlopez
    /// Fecha: 2017-01-14
    /// </summary>
    public class ResponseUnacemReportingStatusPresupuestal
    {
        [JsonProperty("_a")]
        public List<UnacemReportingStatusPresupuestal> Lista { get; set; }
    }
    #endregion
    #endregion
}