﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Producto : AProducto
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string pro_sku { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string pro_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-14
    /// </summary>
    public class E_Producto : AE_Producto
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string Cod_Producto { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string Nombre_Producto { get; set; }
    }

    public abstract class AProducto {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-25
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Producto> Lista(Request_GetProducto_Campania_Compania_Categoria_ProductoFamilia_Marca_Presentacion_Envase oRq)
        {
            Response_GetProducto_Campania_Compania_Categoria_ProductoFamilia_Marca_Presentacion_Envase oRp;

            oRp = MvcApplication._Deserialize<Response_GetProducto_Campania_Compania_Categoria_ProductoFamilia_Marca_Presentacion_Envase>(
                    MvcApplication._Servicio_Campania.GetProducto_Campania_Compania_Categoria_ProductoFamilia_Marca_Presentacion_Envase(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class AE_Producto {
        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<E_Producto> Lista(Listar_Producto_Por_CodCampania_CodCategoria_CodSubCategoria_CodMarca_Request oRq)
        {
            Listar_Producto_Por_CodCampania_CodCategoria_CodSubCategoria_CodMarca_Response oRp;

            oRp = MvcApplication._Deserialize<Listar_Producto_Por_CodCampania_CodCategoria_CodSubCategoria_CodMarca_Response>(
                    MvcApplication._Servicio_Campania.GetProducto_Por_CodCampania_CodCategoria_CodSubCategoria_CodMarca(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oListaProducto;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-24
    /// </summary>
    public class Request_GetProducto_Campania_Compania_Categoria_ProductoFamilia_Marca_Presentacion_Envase
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int compania { get; set; }

        [JsonProperty("_c")]
        public int categoria { get; set; }

        [JsonProperty("_d")]
        public string producto_familia { get; set; }

        [JsonProperty("_e")]
        public int marca { get; set; }

        [JsonProperty("_f")]
        public int presentacion { get; set; }

        [JsonProperty("_g")]
        public string envase { get; set; }
    }
    public class Response_GetProducto_Campania_Compania_Categoria_ProductoFamilia_Marca_Presentacion_Envase
    {
        [JsonProperty("_a")]
        public List<Producto> Lista { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-14
    /// </summary>
    public class Listar_Producto_Por_CodCampania_CodCategoria_CodSubCategoria_CodMarca_Request
    {
        [JsonProperty("a")]
        public string CodCampania { get; set; }

        [JsonProperty("b")]
        public string CodCategoria { get; set; }

        [JsonProperty("c")]
        public string CodSubCategoria { get; set; }

        [JsonProperty("d")]
        public string CodMarca { get; set; }
    }
    public class Listar_Producto_Por_CodCampania_CodCategoria_CodSubCategoria_CodMarca_Response    {
        [JsonProperty("a")]
        public List<E_Producto> oListaProducto { get; set; }
    }

    #endregion
}