﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models.Map;

namespace Lucky.Xplora.Models
{
    public class AlicorpAASSLavanderia : A_AASSLavanderia
    {

    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-04
    /// </summary>
    public class AlicorpAASSLavanderiaParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-04
    /// </summary>
    public class AlicorpAASSLavanderiaCadena
    {
        [JsonProperty("_a")]
        public int CadId { get; set; }

        [JsonProperty("_b")]
        public string CadDescripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpAASSLavanderiaCadenaPdv> lPdv { get; set; }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-04
    /// </summary>
    public class AlicorpAASSLavanderiaCadenaPdv
    {
        [JsonProperty("_a")]
        public string PdvCodigo { get; set; }

        [JsonProperty("_b")]
        public string PdvDescripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpAASSLavanderiaCadenaPdvTipoExhibicion> lTipoExhibicion { get; set; }

        [JsonProperty("_d")]
        public int Puntaje { get; set; }

        [JsonProperty("_e")]
        public string Imagen { get; set; }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-04
    /// </summary>
    public class AlicorpAASSLavanderiaCadenaPdvTipoExhibicion
    {
        [JsonProperty("_a")]
        public string TipoExhibicion { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpAASSLavanderiaCadenaPdvTipoExhibicionExhibidor> lExhibidor { get; set; }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-04
    /// </summary>
    public class AlicorpAASSLavanderiaCadenaPdvTipoExhibicionExhibidor
    {
        [JsonProperty("_a")]
        public string Exhibidor { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpAASSLavanderiaCadenaPdvTipoExhibicionExhibidorAnio> lAnio { get; set; }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-04
    /// </summary>
    public class AlicorpAASSLavanderiaCadenaPdvTipoExhibicionExhibidorAnio
    {
        [JsonProperty("_a")]
        public int Anio { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpAASSLavanderiaCadenaPdvTipoExhibicionExhibidorAnioMes> lMes { get; set; }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-04
    /// </summary>
    public class AlicorpAASSLavanderiaCadenaPdvTipoExhibicionExhibidorAnioMes
    {
        [JsonProperty("_a")]
        public int MesId { get; set; }

        [JsonProperty("_b")]
        public string MesDescripcion { get; set; }

        [JsonProperty("_c")]
        public int Puntaje { get; set; }

        [JsonProperty("_d")]
        public List<AlicorpAASSLavanderiaCadenaPdvTipoExhibicionExhibidorAnioMesSemana> lSemana { get; set; }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-04
    /// </summary>
    public class AlicorpAASSLavanderiaCadenaPdvTipoExhibicionExhibidorAnioMesSemana
    {
        [JsonProperty("_a")]
        public int Semana { get; set; }

        [JsonProperty("_b")]
        public int Puntaje { get; set; }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-07
    /// </summary>
    public class AlicorpAASSLavanderiaSelecciona
    {
        [JsonProperty("_a")]
        public string SelCodigo { get; set; }

        [JsonProperty("_b")]
        public string SelDescripcion { get; set; }

        [JsonProperty("_c")]
        public int SelActivo { get; set; }
    }

    public abstract class A_AASSLavanderia
    {
        /// <summary>
        /// Autor: dpastor
        /// Fecha: 2017-03-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpAASSLavanderiaCadena> Lavanderia(AlicorpAASSLavanderiaParametro oRq)
        {
            return MvcApplication._Deserialize<ResponseCadenaAASSLavanderia>(
                        MvcApplication._Servicio_Operativa.AlicorpAASSLavanderia(MvcApplication._Serialize(oRq))
                    ).lista;
        }

        /// <summary>
        /// Autor: dpastor
        /// Fecha: 2017-03-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<AlicorpAASSLavanderiaSelecciona> Selecciona(AlicorpAASSLavanderiaParametro oRq)
        {
            return MvcApplication._Deserialize<ResponseCadenaAASSLavanderiaSelecciona>(
                        MvcApplication._Servicio_Operativa.AlicorpAASSLavanderia(MvcApplication._Serialize(oRq))
                    ).lista;
        }
    }

    /// <summary>
    /// Autor: dpastor
    /// Fecha: 2017-03-07
    /// </summary>
    public class ResponseCadenaAASSLavanderia
    {
        [JsonProperty("_a")]
        public List<AlicorpAASSLavanderiaCadena> lista { get; set; }
    }
    public class ResponseCadenaAASSLavanderiaSelecciona
    {
        [JsonProperty("_a")]
        public List<AlicorpAASSLavanderiaSelecciona> lista { get; set; }
    }
}