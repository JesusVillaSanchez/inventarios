﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-12-03
    /// </summary>
    public class PointOfSalePlanningOperating : APointOfSalePlanningOperating
    {
        [JsonProperty("_a")]
        public string CamCodigo { get; set; }

        [JsonProperty("_b")]
        public int PerId { get; set; }

        [JsonProperty("_c")]
        public int IdMposPlanning { get; set; }

        [JsonProperty("_d")]
        public string PdvCodigo { get; set; }

        [JsonProperty("_e")]
        public DateTime FechaDesde { get; set; }

        [JsonProperty("_f")]
        public DateTime FechaHasta { get; set; }

        [JsonProperty("_g")]
        public int UsuId { get; set; }

        [JsonProperty("_h")]
        public string NombreArchivo { get; set; }

        [JsonProperty("_i")]
        public string PdvDescripcion { get; set; }

        [JsonProperty("_j")]
        public string PerUsuario { get; set; }

        [JsonProperty("_k")]
        public string UsuUsuario { get; set; }

        [JsonProperty("_l")]
        public int IdPosPlanningOperating { get; set; }

        [JsonProperty("_m")]
        public int PosPlanningOperatingStatus { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-12-05
    /// </summary>
    public abstract class APointOfSalePlanningOperating {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-12-05
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public int Bulk(List<PointOfSalePlanningOperating> parametro) {
            return MvcApplication._Deserialize<ResponsePointOfSalePlanningOperatingBulk>(
                        MvcApplication._Servicio_Operativa.PointOfSalePlanningOperatingBulk(MvcApplication._Serialize(parametro))
                    ).Cantidad;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-12-05
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<PointOfSalePlanningOperating> Lista(PointOfSalePlanningOperatingParametro parametro)
        {
            return MvcApplication._Deserialize<ResponsePointOfSalePlanningOperating>(
                    MvcApplication._Servicio_Operativa.PointOfSalePlanningOperating(MvcApplication._Serialize(parametro))
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-12-05
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public int Elimina(PointOfSalePlanningOperatingParametro parametro)
        {
            return MvcApplication._Deserialize<ResponsePointOfSalePlanningOperatingBulk>(
                    MvcApplication._Servicio_Operativa.PointOfSalePlanningOperating(MvcApplication._Serialize(parametro))
                ).Cantidad;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-12-05
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public int Graba(PointOfSalePlanningOperatingParametro parametro)
        {
            return MvcApplication._Deserialize<ResponsePointOfSalePlanningOperatingBulk>(
                    MvcApplication._Servicio_Operativa.PointOfSalePlanningOperating(MvcApplication._Serialize(parametro))
                ).Cantidad;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-12-21
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public string Modifica(PointOfSalePlanningOperatingParametro parametro)
        {
            return MvcApplication._Deserialize<ResponsePointOfSalePlanningOperatingResultado>(
                    MvcApplication._Servicio_Operativa.PointOfSalePlanningOperating(MvcApplication._Serialize(parametro))
                ).Resultado;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-12-03
    /// </summary>
    public class PointOfSalePlanningOperatingParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-12-05
    /// </summary>
    public class ResponsePointOfSalePlanningOperatingBulk
    {
        [JsonProperty("_a")]
        public int Cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-12-05
    /// </summary>
    public class ResponsePointOfSalePlanningOperating
    {
        [JsonProperty("_a")]
        public List<PointOfSalePlanningOperating> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-12-21
    /// </summary>
    public class ResponsePointOfSalePlanningOperatingResultado
    {
        [JsonProperty("_a")]
        public string Resultado { get; set; }
    }
    #endregion
}