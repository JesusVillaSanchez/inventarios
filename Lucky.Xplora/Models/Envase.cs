﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Envase : AEnvase
    {
        [JsonProperty("_a")]
        public string ppr_id { get; set; }

        [JsonProperty("_b")]
        public string ppr_descripcion { get; set; }
    }

    public abstract class AEnvase {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-24
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Envase> Lista(Request_GetEnvase_Opcion_Parametro oRq)
        {
            Response_GetEnvase_Opcion_Parametro oRp;

            oRp = MvcApplication._Deserialize<Response_GetEnvase_Opcion_Parametro>(
                    MvcApplication._Servicio_Campania.GetEnvase_Opcion_Parametro(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Gestion de Envase

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-24
    /// </summary>
    public class Request_GetEnvase_Opcion_Parametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }
    public class Response_GetEnvase_Opcion_Parametro
    {
        [JsonProperty("_a")]
        public List<Envase> Lista { get; set; }
    }

    #endregion
}