﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.NewReportingHenkel;

namespace Lucky.Xplora.Models.NewReportingHenkel
{
    public class Oos_Henkel : A_Oos_Henkel
    {
        [JsonProperty("_a")]
        public List<Oos_Henkel_Data> OosDataIni { get; set; }
        [JsonProperty("_b")]
        public List<Oos_Henkel_Percentage_Brand> OosxMarca { get; set; }
        [JsonProperty("_c")]
        public List<E_Oos_Ranking_PDV_Henkel> OosxPDVRanking { get; set; }
        [JsonProperty("_d")]
        public List<E_Oos_Evolutivo_Head_Henkel> OosEvolutivoHead { get; set; }
        [JsonProperty("_e")]
        public List<E_Oos_Evolutivo_Body_Henkel> OosEvolutivoBody { get; set; }
        [JsonProperty("_f")]
        public List<E_Oos_Evolutivo_BodyC_Henkel> OosEvoCadenaBody { get; set; }
        [JsonProperty("_g")]
        public List<E_Oos_Ranking_SKU_Henkel> OosxSKURanking { get; set; }
    }
    public class Oos_Henkel_Data
    {
        [JsonProperty("_a")]
        public string oos_perdida { get; set; }
        [JsonProperty("_b")]
        public string oos_cantidad { get; set; }
    }
    public class Oos_Henkel_Percentage_Brand
    {
        [JsonProperty("_a")]
        public string brand { get; set; }
        [JsonProperty("_b")]
        public string porcbrand { get; set; }
        [JsonProperty("_c")]
        public string cantidadpdv { get; set; }
        [JsonProperty("_d")]
        public string cantidadruta { get; set; }
    }
    public class E_Oos_Ranking_PDV_Henkel
    {
        [JsonProperty("_a")]
        public string pdvName { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }
    public class E_Oos_Ranking_SKU_Henkel
    {
        [JsonProperty("_a")]
        public string skuName { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }
    public class E_Oos_Evolutivo_Head_Henkel
    {
        [JsonProperty("_a")]
        public string id_head { get; set; }
        [JsonProperty("_b")]
        public string rpl_id_head { get; set; }
        [JsonProperty("_c")]
        public string descripcion_head { get; set; }
    }
    public class E_Oos_Evolutivo_Body_Henkel
    {
        [JsonProperty("_a")]
        public string row { get; set; }
        [JsonProperty("_b")]
        public string producto { get; set; }
        [JsonProperty("_c")]
        public string rpl_1 { get; set; }
        [JsonProperty("_d")]
        public string rpl_2 { get; set; }
        [JsonProperty("_e")]
        public string rpl_3 { get; set; }
        [JsonProperty("_f")]
        public string rpl_4 { get; set; }
        [JsonProperty("_g")]
        public string rpl_5 { get; set; }
        [JsonProperty("_h")]
        public string rpl_6 { get; set; }
        [JsonProperty("_i")]
        public string rpl_7 { get; set; }
        [JsonProperty("_j")]
        public string rpl_8 { get; set; }
        [JsonProperty("_k")]
        public string rpl_9 { get; set; }
        [JsonProperty("_l")]
        public string rpl_10 { get; set; }
        [JsonProperty("_m")]
        public string rpl_11 { get; set; }
        [JsonProperty("_n")]
        public string rpl_12 { get; set; }
    }
    public class E_Oos_Evolutivo_BodyC_Henkel
    {
        [JsonProperty("_a")]
        public string cadena { get; set; }
        [JsonProperty("_b")]
        public string producto { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }
    }

    public class Oos_Henkel_Det_Quiebre
    {
        [JsonProperty("_a")]
        public string cadena { get; set; }
        [JsonProperty("_b")]
        public string pdv { get; set; }
        [JsonProperty("_c")]
        public string categoria { get; set; }
        [JsonProperty("_d")]
        public string presentacion { get; set; }
        [JsonProperty("_e")]
        public string sku { get; set; }
        [JsonProperty("_f")]
        public string producto { get; set; }
        [JsonProperty("_g")]
        public string fecha { get; set; }
    }
    public class Oos_Henkel_Det_RankingPdv
    {
        [JsonProperty("_a")]
        public string ciudad { get; set; }
        [JsonProperty("_b")]
        public string pdv { get; set; }
        [JsonProperty("_c")]
        public string mes_1 { get; set; }
        [JsonProperty("_d")]
        public string mes_2 { get; set; }
        [JsonProperty("_e")]
        public string mes_3 { get; set; }
        [JsonProperty("_f")]
        public string mes_4 { get; set; }
        [JsonProperty("_g")]
        public string mes_5 { get; set; }
        [JsonProperty("_h")]
        public string mes_6 { get; set; }
        [JsonProperty("_i")]
        public string mes_7 { get; set; }
        [JsonProperty("_j")]
        public string mes_8 { get; set; }
        [JsonProperty("_k")]
        public string mes_9 { get; set; }
        [JsonProperty("_l")]
        public string mes_10 { get; set; }
        [JsonProperty("_m")]
        public string mes_11 { get; set; }
        [JsonProperty("_n")]
        public string mes_12 { get; set; }
        [JsonProperty("_ñ")]
        public string mes_13 { get; set; }

    }
    public class Oos_Henkel_Det_RankingPdvmonto
    {
        [JsonProperty("_a")]
        public string ciudad { get; set; }
        [JsonProperty("_b")]
        public string pdv { get; set; }
        [JsonProperty("_c")]
        public string mes_1 { get; set; }
        [JsonProperty("_d")]
        public string mes_2 { get; set; }
        [JsonProperty("_e")]
        public string mes_3 { get; set; }
        [JsonProperty("_f")]
        public string mes_4 { get; set; }
        [JsonProperty("_g")]
        public string mes_5 { get; set; }
        [JsonProperty("_h")]
        public string mes_6 { get; set; }
        [JsonProperty("_i")]
        public string mes_7 { get; set; }
        [JsonProperty("_j")]
        public string mes_8 { get; set; }
        [JsonProperty("_k")]
        public string mes_9 { get; set; }
        [JsonProperty("_l")]
        public string mes_10 { get; set; }
        [JsonProperty("_m")]
        public string mes_11 { get; set; }
        [JsonProperty("_n")]
        public string mes_12 { get; set; }
        [JsonProperty("_ñ")]
        public string mes_13 { get; set; }

    }
    public class Oos_Henkel_Det_RankingSku
    {
        [JsonProperty("_a")]
        public string sku { get; set; }
        [JsonProperty("_b")]
        public string mes_1 { get; set; }
        [JsonProperty("_c")]
        public string mes_2 { get; set; }
        [JsonProperty("_d")]
        public string mes_3 { get; set; }
        [JsonProperty("_e")]
        public string mes_4 { get; set; }
        [JsonProperty("_f")]
        public string mes_5 { get; set; }
        [JsonProperty("_g")]
        public string mes_6 { get; set; }
        [JsonProperty("_h")]
        public string mes_7 { get; set; }
        [JsonProperty("_i")]
        public string mes_8 { get; set; }
        [JsonProperty("_j")]
        public string mes_9 { get; set; }
        [JsonProperty("_k")]
        public string mes_10 { get; set; }
        [JsonProperty("_l")]
        public string mes_11 { get; set; }
        [JsonProperty("_m")]
        public string mes_12 { get; set; }
        [JsonProperty("_n")]
        public string mes_13 { get; set; }
    }
    public class Oos_Henkel_Det_RankingSkumonto
    {
        [JsonProperty("_a")]
        public string sku { get; set; }
        [JsonProperty("_b")]
        public string mes_1 { get; set; }
        [JsonProperty("_c")]
        public string mes_2 { get; set; }
        [JsonProperty("_d")]
        public string mes_3 { get; set; }
        [JsonProperty("_e")]
        public string mes_4 { get; set; }
        [JsonProperty("_f")]
        public string mes_5 { get; set; }
        [JsonProperty("_g")]
        public string mes_6 { get; set; }
        [JsonProperty("_h")]
        public string mes_7 { get; set; }
        [JsonProperty("_i")]
        public string mes_8 { get; set; }
        [JsonProperty("_j")]
        public string mes_9 { get; set; }
        [JsonProperty("_k")]
        public string mes_10 { get; set; }
        [JsonProperty("_l")]
        public string mes_11 { get; set; }
        [JsonProperty("_m")]
        public string mes_12 { get; set; }
        [JsonProperty("_n")]
        public string mes_13 { get; set; }
    }

    public class E_Oos_Data_Export_Henkel : A_Oos_Data_Export_Henkel
    {
        [JsonProperty("a")]
        public string id_periodo { get; set; }
        [JsonProperty("b")]
        public string ciudad { get; set; }
        [JsonProperty("c")]
        public string cadena { get; set; }
        [JsonProperty("d")]
        public string cod_pdv { get; set; }
        [JsonProperty("e")]
        public string pdv { get; set; }
        [JsonProperty("f")]
        public string categoria { get; set; }
        [JsonProperty("g")]
        public string marca { get; set; }
        [JsonProperty("h")]
        public string presentacion { get; set; }
        [JsonProperty("i")]
        public string sku { get; set; }
        [JsonProperty("j")]
        public string producto { get; set; }
        [JsonProperty("k")]
        public string quiebres { get; set; }
        [JsonProperty("l")]
        public string fechacel { get; set; }
    }

    public abstract class A_Oos_Henkel
    {
        public Oos_Henkel Objeto_Oos(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_Henkel_Reporting_Oos oRp = MvcApplication._Deserialize<Response_Henkel_Reporting_Oos>(
                    MvcApplication._Servicio_Operativa.Henkel_Oos(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Henkel_Det_Quiebre> Objeto_OOSDetalle(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_Henkel_Oos_DetQuiebre oRp = MvcApplication._Deserialize<Response_Henkel_Oos_DetQuiebre>(
                    MvcApplication._Servicio_Operativa.Henkel_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Henkel_Det_RankingPdv> Objeto_OOSDetRankingPdv(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_Henkel_Oos_DetRankingPdv oRp = MvcApplication._Deserialize<Response_Henkel_Oos_DetRankingPdv>(
                    MvcApplication._Servicio_Operativa.Henkel_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Henkel_Det_RankingPdvmonto> Objeto_OOSDetRankingPdvmonto(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_Henkel_Oos_DetRankingPdvmonto oRp = MvcApplication._Deserialize<Response_Henkel_Oos_DetRankingPdvmonto>(
                    MvcApplication._Servicio_Operativa.Henkel_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Henkel_Det_RankingSku> Objeto_OOSDetRankingSku(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_Henkel_Oos_DetRankingSku oRp = MvcApplication._Deserialize<Response_Henkel_Oos_DetRankingSku>(
                    MvcApplication._Servicio_Operativa.Henkel_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Henkel_Det_RankingSkumonto> Objeto_OOSDetRankingSkumonto(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_Henkel_Oos_DetRankingSkumonto oRp = MvcApplication._Deserialize<Response_Henkel_Oos_DetRankingSkumonto>(
                        MvcApplication._Servicio_Operativa.Henkel_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

    }

    public abstract class A_Oos_Data_Export_Henkel
    {
        public List<E_Oos_Data_Export_Henkel> Lista_OosDataExport(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_Henkel_Oos_DataExport oRp = MvcApplication._Deserialize<Response_Henkel_Oos_DataExport>(
                    MvcApplication._Servicio_Operativa.Henkel_OosDetalle_Export(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    //public class Request_Henkel_Reporting_Parametros
    //{
    //    [JsonProperty("_a")]
    //    public int opcion { get; set; }
    //    [JsonProperty("_b")]
    //    public int subcanal { get; set; }
    //    [JsonProperty("_c")]
    //    public String categoria { get; set; }
    //    [JsonProperty("_d")]
    //    public String marca { get; set; }
    //    [JsonProperty("_e")]
    //    public String periodo { get; set; }
    //    [JsonProperty("_f")]
    //    public String zona { get; set; }
    //    [JsonProperty("_g")]
    //    public String distrito { get; set; }

    //    [JsonProperty("_h")]
    //    public String segmento { get; set; }
    //    [JsonProperty("_i")]
    //    public String cadena { get; set; }
    //    [JsonProperty("_j")]
    //    public String empresa { get; set; }
    //    [JsonProperty("_k")]
    //    public String tienda { get; set; }
    //    [JsonProperty("_l")]
    //    public String tipoactividad { get; set; }

    //    [JsonProperty("_m")]
    //    public String fecha_incidencia { get; set; }
    //    [JsonProperty("_n")]
    //    public String ciudad { get; set; }
    //    [JsonProperty("_o")]
    //    public String perfil { get; set; }
    //    [JsonProperty("_p")]
    //    public String semana { get; set; }

    //    [JsonProperty("_q")]
    //    public String producto { get; set; }

    //    [JsonProperty("_r")]
    //    public String cod_equipo { get; set; }
    //    [JsonProperty("_s")]
    //    public String cod_material { get; set; }
    //    [JsonProperty("_t")]
    //    public String cod_pdv { get; set; }
    //    [JsonProperty("_u")]
    //    public string subcanal2 { get; set; }
    //    [JsonProperty("_v")]
    //    public int periodo2 { get; set; }

    //    [JsonProperty("_w")]
    //    public int anio { get; set; }
    //    [JsonProperty("_x")]
    //    public int mes { get; set; }
    //    [JsonProperty("_y")]
    //    public int elemento { get; set; }
    //    [JsonProperty("_z")]
    //    public string dias { get; set; }
    //}

    #region Request & Response

    public class Response_Henkel_Reporting_Oos
    {
        [JsonProperty("_a")]
        public Oos_Henkel Objeto { get; set; }
    }

    public class Response_Henkel_Oos_DetQuiebre
    {
        [JsonProperty("_a")]
        public List<Oos_Henkel_Det_Quiebre> Objeto { get; set; }
    }
    public class Response_Henkel_Oos_DetRankingPdv
    {
        [JsonProperty("_a")]
        public List<Oos_Henkel_Det_RankingPdv> Objeto { get; set; }
    }
    public class Response_Henkel_Oos_DetRankingPdvmonto
    {
        [JsonProperty("_a")]
        public List<Oos_Henkel_Det_RankingPdvmonto> Objeto { get; set; }
    }
    public class Response_Henkel_Oos_DetRankingSku
    {
        [JsonProperty("_a")]
        public List<Oos_Henkel_Det_RankingSku> Objeto { get; set; }
    }
    public class Response_Henkel_Oos_DetRankingSkumonto
    {
        [JsonProperty("_a")]
        public List<Oos_Henkel_Det_RankingSkumonto> Objeto { get; set; }
    }

    public class Response_Henkel_Oos_DataExport
    {
        [JsonProperty("_a")]
        public List<E_Oos_Data_Export_Henkel> Objeto { get; set; }
    }

    #endregion

    #region <<< REGION GENERAL >>>
    public class Request_NWRepStd_General
    {
        [JsonProperty("_a")]
        public String Parametros { get; set; }
        [JsonProperty("_b")]
        public int op { get; set; }
        [JsonProperty("_c")]
        public String subcanal { get; set; }
        [JsonProperty("_d")]
        public String cadena { get; set; }
        [JsonProperty("_e")]
        public String categoria { get; set; }
        [JsonProperty("_f")]
        public String empresa { get; set; }
        [JsonProperty("_g")]
        public String marca { get; set; }
        [JsonProperty("_h")]
        public String SKU { get; set; }
        [JsonProperty("_i")]
        public int periodo { get; set; }
        [JsonProperty("_j")]
        public String zona { get; set; }
        [JsonProperty("_k")]
        public String distrito { get; set; }
        [JsonProperty("_l")]
        public String tienda { get; set; }
        [JsonProperty("_m")]
        public String segmento { get; set; }
        [JsonProperty("_n")]
        public int Persona { get; set; }
        [JsonProperty("_o")]
        public int mes { get; set; }
        [JsonProperty("_p")]
        public int anio { get; set; }
        [JsonProperty("_q")]
        public int idtipo_material { get; set; }
        [JsonProperty("_r")]
        public string min { get; set; }
        [JsonProperty("_s")]
        public string max { get; set; }
    }
    public class NwRepPrecioAASS
    {
        public E_Precio_AASS Consulta_Reporte(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Precio oRp = MvcApplication._Deserialize<Response_NWRepStd_Precio>(MvcApplication._Servicio_Operativa.NwRepStd_Consultar_PreciosAASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        //hacer excel edwin 2
        public E_Index_Precio_AASS Consulta_IndexPrecio(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Precio_Index oRp = MvcApplication._Deserialize<Response_NWRepStd_Precio_Index>(MvcApplication._Servicio_Operativa.NwRepStd_Consultar_IndexPreciosAASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<M_Combo> Filtro(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Filtro oRp = MvcApplication._Deserialize<Response_NWRepStd_Filtro>
                (MvcApplication._Servicio_Operativa.NwRepStd_Filtros_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        //guia excel edwin 1
        public List<E_Excel_Precio_AASS> Excel_Precio(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Excel_Precio oRp = MvcApplication._Deserialize
           <Response_NWRepStd_Excel_Precio>(MvcApplication._Servicio_Operativa.NwRepStd_Excel_PreciosAASS
                (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        //haciendo
        public List<E_excel_index_price_AA_SS_edw> Excel_index_Precio_edw(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Excel_index_Precio_edw oRp = MvcApplication._Deserialize
           <Response_NWRepStd_Excel_index_Precio_edw>(MvcApplication._Servicio_Operativa.Nw_excel_Index_Precio_AASS_edw
           (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        //duplicado Nw_Exportar_Index_Precio_2
        public List<E_excel_index_price_AA_SS_edw> Excel_index_Precio_2(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Excel_index_Precio_edw oRp = MvcApplication._Deserialize
           <Response_NWRepStd_Excel_index_Precio_edw>(MvcApplication._Servicio_Operativa.Nw_Exportar_Index_Precio_2
           (MvcApplication._Serialize(request)));
            return oRp.Objeto;
        }

    }

    public class Response_NWRepStd_Precio
    {
        [JsonProperty("_a")]
        public E_Precio_AASS Objeto { get; set; }
    }

    #region <<Entidad>>
    public class E_Precio_AASS
    {
        [JsonProperty("_a")]
        public List<String> ListPeriodos { get; set; }
        [JsonProperty("_b")]
        public List<String> ListCadena { get; set; }
        [JsonProperty("_c")]
        public List<E_Precio_AASS_Fila> Filas { get; set; }
    }
    public class E_Precio_AASS_Fila
    {
        [JsonProperty("_a")]
        public String Empresa { get; set; }
        [JsonProperty("_b")]
        public String Presentacion { get; set; }
        [JsonProperty("_c")]
        public String Descripcion { get; set; }
        [JsonProperty("_d")]
        public List<E_Precio_AASS_Periodo> Periodos { get; set; }
        [JsonProperty("_e")]
        public List<E_Precio_AASS_Cadena> Cadenas { get; set; }
    }
    public class E_Precio_AASS_Periodo
    {
        [JsonProperty("_a")]
        public String Periodo { get; set; }
        [JsonProperty("_b")]
        public String P_Regular { get; set; }
        [JsonProperty("_c")]
        public String P_Oferta { set; get; }
        [JsonProperty("_d")]
        public String Margen { get; set; }
        [JsonProperty("_e")]
        public String Cod_TOferta { get; set; }
        [JsonProperty("_f")]
        public String Des_TOferta { get; set; }
        [JsonProperty("_g")]
        public String P_Peso { get; set; }
    }
    public class E_Precio_AASS_Cadena
    {
        [JsonProperty("_a")]
        public String P_Moda { get; set; }
        [JsonProperty("_b")]
        public String Cod_TOferta { get; set; }
        [JsonProperty("_c")]
        public String Des_TOferta { get; set; }
    }

    //

    public class E_Index_Precio_AASS
    {
        [JsonProperty("_a")]
        public List<String> Cadenas { get; set; }
        [JsonProperty("_b")]
        public List<E_Index_Precio_grup_AASS> Grupos { get; set; }
    }
    public class E_Index_Precio_grup_AASS
    {
        [JsonProperty("_a")]
        public List<E_Index_Precio_Row_AASS> Filas { get; set; }

    }

    public class Response_NWRepStd_Precio_Index
    {
        [JsonProperty("_a")]
        public E_Index_Precio_AASS Objeto { get; set; }
    }
    public class Response_NWRepStd_Excel_Precio
    {
        [JsonProperty("_a")]
        public List<E_Excel_Precio_AASS> Objeto { get; set; }
    }

    public class Response_NWRepStd_Excel_index_Precio_edw
    {
        [JsonProperty("_a")]
        public List<E_excel_index_price_AA_SS_edw> Objeto { get; set; }
    }
    public class E_Index_Precio_Row_AASS
    {
        [JsonProperty("_a")]
        public String Presentacion { get; set; }
        [JsonProperty("_b")]
        public String Producto { get; set; }
        [JsonProperty("_d")]
        public string Propio { get; set; }
        [JsonProperty("_c")]
        public List<E_Index_Precio_cadena_AASS> Cadenas { get; set; }
    }
    public class E_Index_Precio_cadena_AASS
    {
        [JsonProperty("_a")]
        public String P_Regular { get; set; }
        [JsonProperty("_b")]
        public String Index { get; set; }
        [JsonProperty("_c")]
        public String P_Peso { get; set; }
        [JsonProperty("_d")]
        public String N_Index { get; set; }
        [JsonProperty("_e")]
        public String T_precio { get; set; }
    }
    public class E_Excel_Precio_AASS
    {

        [JsonProperty("_a")]
        public String Cadena { get; set; }
        [JsonProperty("_b")]
        public String Cod_PDV { get; set; }
        [JsonProperty("_c")]
        public String PDV { get; set; }
        [JsonProperty("_d")]
        public String Empresa { get; set; }
        [JsonProperty("_e")]
        public String Presentacion { get; set; }
        [JsonProperty("_f")]
        public String Categoria { get; set; }
        [JsonProperty("_g")]
        public String Marca { get; set; }
        [JsonProperty("_h")]
        public String Producto { get; set; }
        [JsonProperty("_i")]
        public String P_Regular { get; set; }
        [JsonProperty("_j")]
        public String P_Oferta { get; set; }
        [JsonProperty("_k")]
        public String Tipoprecio { get; set; }
        [JsonProperty("_l")]
        public String Periodo { get; set; }
    }

    public class E_excel_index_price_AA_SS_edw
    {

        [JsonProperty("_a")]
        public String codigo { get; set; }
        [JsonProperty("_b")]
        public String orden { get; set; }
        [JsonProperty("_c")]
        public String nombre { get; set; }

        [JsonProperty("_d")]
        public String Cod_periodo { get; set; }
        [JsonProperty("_e")]
        public String Cod_cadena { get; set; }
        [JsonProperty("_f")]
        public String Id_product { get; set; }
        [JsonProperty("_g")]
        public String Propio { get; set; }
        [JsonProperty("_h")]
        public String Cod_padre { get; set; }

        [JsonProperty("_i")]
        public String moda_propio { get; set; }
        [JsonProperty("_j")]
        public String indice_propio { get; set; }
        [JsonProperty("_k")]
        public String p_peso { get; set; }
        [JsonProperty("_l")]
        public String new_index { get; set; }
        [JsonProperty("_m")]
        public String nom_producto { get; set; }
        [JsonProperty("_o")]
        public String nom_presentacion { get; set; }

    }

    #endregion

    public class Response_NWRepStd_Filtro
    {
        [JsonProperty("_a")]
        public List<M_Combo> Objeto { get; set; }
    }

    #endregion

}