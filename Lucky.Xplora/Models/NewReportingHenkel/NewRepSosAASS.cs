﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

using Lucky.Xplora.Models;

namespace Lucky.Xplora.Models.NewReportingHenkel
{

    public class Sos_Henkel : A_Sos_Henkel
    {
        [JsonProperty("_a")]
        public List<E_Sos_Participation_Brand_Henkel> SosxMarca { get; set; }
        [JsonProperty("_b")]
        public List<E_Sos_Participation_Company_Henkel> SosxEmpresa { get; set; }
        [JsonProperty("_c")]
        public List<E_Sos_Ranking_PDV_Henkel> SosxPDVRanking { get; set; }
        [JsonProperty("_d")]
        public List<E_Sos_Participation_CompanyxBrand_Henkel> SosxCompanyxBrand { get; set; }
    }
    public class E_Sos_Participation_Brand_Henkel
    {
        [JsonProperty("_a")]
        public string company { get; set; }
        [JsonProperty("_b")]
        public string brand { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }
    public class E_Sos_Participation_Company_Henkel
    {
        [JsonProperty("_a")]
        public string rpl_description { get; set; }
        [JsonProperty("_b")]
        public string company { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }
    public class E_Sos_Ranking_PDV_Henkel
    {
        [JsonProperty("_a")]
        public string pdv_name { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }
    public class E_Sos_Participation_CompanyxBrand_Henkel
    {
        [JsonProperty("_a")]
        public string ncm_name { get; set; }
        [JsonProperty("_b")]
        public string company { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }

    public class Sos_Henkel_EvoBrand
    {
        [JsonProperty("_a")]
        public List<E_Sos_Evolutivo_Brand_Henkel> SosEvobrand { get; set; }
    }
    public class E_Sos_Evolutivo_Brand_Henkel
    {
        [JsonProperty("_a")]
        public string rpl_description { get; set; }
        [JsonProperty("_b")]
        public string brand { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }

    public abstract class A_Sos_Henkel
    {
        public Sos_Henkel Objeto_Sos(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_HenkelReporting_Sos oRp = MvcApplication._Deserialize<Response_HenkelReporting_Sos>(
                    MvcApplication._Servicio_Operativa.Aje_Sos(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public E_Sos_Consulta_Brand_Henkel Consultar_Marca(Request_Sos_Henkel_Brand oRq)
        {
            Response_Sos_Henkel_Brand oRp;

            oRp = MvcApplication._Deserialize<Response_Sos_Henkel_Brand>(MvcApplication._Servicio_Operativa.Henkel_Sos_Consulta_Marca(MvcApplication._Serialize(oRq)));

            return oRp.Obj;
        }

        public Sos_Henkel_EvoBrand Objeto_Sos_EvoBrand(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_HenkelReporting_Sos_EvoBrand oRp = MvcApplication._Deserialize<Response_HenkelReporting_Sos_EvoBrand>(
                    MvcApplication._Servicio_Operativa.Henkel_Sos(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    /*Descarga*/
    public class E_Sos_Detalle_Henkel : H_Sos_Detalle
    {
        [JsonProperty("a")]
        public string gie { get; set; }
        [JsonProperty("b")]
        public string cadenas { get; set; }
        [JsonProperty("c")]
        public string pdv { get; set; }
        [JsonProperty("d")]
        public string categoria { get; set; }
        [JsonProperty("e")]
        public string fecha_celular { get; set; }
        [JsonProperty("f")]
        public string tipomaterial { get; set; }
        [JsonProperty("g")]
        public string marca { get; set; }
        [JsonProperty("h")]
        public string cantidad { get; set; }
        [JsonProperty("i")]
        public string total { get; set; }
        [JsonProperty("j")]
        public string foto { get; set; }
        [JsonProperty("k")]
        public string tipoobservacion { get; set; }
        [JsonProperty("l")]
        public string medida { get; set; }
        [JsonProperty("m")]
        public string observacion { get; set; }
    }

    public abstract class H_Sos_Detalle
    {
        public List<E_Sos_Detalle_Henkel> Lista_SosDetalle(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_HenkelReporting_Sos_Detalle oRp = MvcApplication._Deserialize<Response_HenkelReporting_Sos_Detalle>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Sos_Detalle_AASS_henkel(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    public class E_Sos_Detalle_Cumplimiento_Export_Henkel : A_Sos_Detalle_Cumplimiento_Export_Henkel
    {
        [JsonProperty("a")]
        public string cadena { get; set; }
        [JsonProperty("b")]
        public string pdv { get; set; }
        [JsonProperty("c")]
        public string tipomaterial { get; set; }
        [JsonProperty("d")]
        public string empresa { get; set; }
        [JsonProperty("e")]
        public string ciudad { get; set; }
        [JsonProperty("f")]
        public string marca { get; set; }
        [JsonProperty("g")]
        public string cantidad { get; set; }

        [JsonProperty("h")]
        public string cordinado { get; set; }
        [JsonProperty("i")]
        public string porcentaje { get; set; }
    }

    public abstract class A_Sos_Detalle_Cumplimiento_Export_Henkel
    {
        public List<E_Sos_Detalle_Cumplimiento_Export_Henkel> Lista_SosCumplimiento(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_HenkelReporting_Sos_Cumplimiento oRp = MvcApplication._Deserialize<Response_HenkelReporting_Sos_Cumplimiento>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Sos_Detalle_Cumpplimiento(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
        public List<E_Sos_Detalle_Cumplimiento_Export_Henkel> Lista_SosCumplimiento_Exprotar_Detalle_Henkel(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_HenkelReporting_Sos_Cumplimiento_Detalle oRp = MvcApplication._Deserialize<Response_HenkelReporting_Sos_Cumplimiento_Detalle>(
                    MvcApplication._Servicio_Operativa.NW_Aje_Cumplimiento_Detalle_Exportar_2(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }


    }
    //public List<E_Sos_Detalle_Cumplimiento_Export_Henkel> Lista_SosCumplimiento_Exprotar_Detalle_Henkel(Request_Henkel_Reporting_Parametros oRq)
    //{
    //    Response_HenkelReporting_Sos_Cumplimiento_Detalle oRp = MvcApplication._Deserialize<Response_HenkelReporting_Sos_Cumplimiento_Detalle>(
    //            MvcApplication._Servicio_Operativa.NW_Henkel_Cumplimiento_Detalle_Exportar_2(
    //                MvcApplication._Serialize(oRq)
    //            )
    //        );

    //    return oRp.Objeto;
    //}

    //public abstract class A_Sos_Detalle_Cumplimiento_Export
    //{

    //}

    #region Request & Response

    public class Response_HenkelReporting_Sos
    {
        [JsonProperty("_a")]
        public Sos_Henkel Objeto { get; set; }
    }

    public class Response_HenkelReporting_Sos_EvoBrand
    {
        [JsonProperty("_a")]
        public Sos_Henkel_EvoBrand Objeto { get; set; }
    }

    public class Response_HenkelReporting_Sos_Detalle
    {
        [JsonProperty("_a")]
        public List<E_Sos_Detalle_Henkel> Objeto { get; set; }
    }

    public class Response_HenkelReporting_Sos_Cumplimiento
    {
        [JsonProperty("_a")]
        public List<E_Sos_Detalle_Cumplimiento_Export_Henkel> Objeto { get; set; }
    }

    public class Response_HenkelReporting_Sos_Cumplimiento_Detalle
    {
        [JsonProperty("_a")]
        public List<E_Sos_Detalle_Cumplimiento_Export_Henkel> Objeto { get; set; }
    }

    #endregion




    #region Brand

    public class E_Sos_Consulta_Brand_Henkel
    {
        [JsonProperty("_a")]
        public string Cod_Marca { get; set; }
        [JsonProperty("_b")]
        public string Marca { get; set; }
    }

    #region Request & Response

    public class Request_Sos_Henkel_Brand
    {
        [JsonProperty("_a")]
        public string Text { get; set; }
    }
    public class Response_Sos_Henkel_Brand
    {
        [JsonProperty("_a")]
        public E_Sos_Consulta_Brand_Henkel Obj { get; set; }
    }

    #endregion


    #endregion

    public class Sos_RpAASS_Cumplimiento_detalle_V2_Henkel
    {
        public E_Cumplimiento_Detalle_AASS Lista_SosCumplimiento_Detalle_v2_Henkel(Request_Henkel_Reporting_Parametros oRq)
        {
            Response_HenkelRp_Sos_Cumplimiento_Detalle oRp = MvcApplication._Deserialize<Response_HenkelRp_Sos_Cumplimiento_Detalle>(
                    MvcApplication._Servicio_Operativa.Henkel_RpSosDetalle_v2(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }



    #region Entity
    public class E_Cumplimiento_Detalle_AASS
    {

        [JsonProperty("_a")]
        public List<E_Cumplimiento_Detalle_AASS_rows> Filas { get; set; }
        [JsonProperty("_b")]
        public List<E_Cumplimiento_Detalle_AASS_Cab> Cabeceras { get; set; }
    }

    // crear clase 
    public class E_Cumplimiento_Detalle_AASS_rows
    {
        [JsonProperty("_a")]
        public string Cadena { get; set; }

        [JsonProperty("_b")]
        public string Pdv { get; set; }

        [JsonProperty("_c")]
        public string Ciudad { get; set; }

        [JsonProperty("_d")]
        public List<E_Cumplimiento_Detalle_AASS_celda> Celdas { get; set; }
    }
    public class E_Cumplimiento_Detalle_AASS_Cab
    {
        [JsonProperty("_a")]
        public string Empresa { get; set; }

        [JsonProperty("_b")]
        public List<String> Marcas { get; set; }
    }
    public class E_Cumplimiento_Detalle_AASS_celda
    {
        [JsonProperty("_a")]
        public String Valor { get; set; }
        [JsonProperty("_b")]
        public String Color { get; set; }
    }
    #endregion
    #region Response
    public class Response_HenkelRp_Sos_Cumplimiento_Detalle
    {
        [JsonProperty("_a")]
        public E_Cumplimiento_Detalle_AASS Objeto { get; set; }
    }
    #endregion

    public class Request_Henkel_Reporting_Parametros
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }
        [JsonProperty("_b")]
        public int subcanal { get; set; }
        [JsonProperty("_c")]
        public String categoria { get; set; }
        [JsonProperty("_d")]
        public String marca { get; set; }
        [JsonProperty("_e")]
        public String periodo { get; set; }
        [JsonProperty("_f")]
        public String zona { get; set; }
        [JsonProperty("_g")]
        public String distrito { get; set; }

        [JsonProperty("_h")]
        public String segmento { get; set; }
        [JsonProperty("_i")]
        public String cadena { get; set; }
        [JsonProperty("_j")]
        public String empresa { get; set; }
        [JsonProperty("_k")]
        public String tienda { get; set; }
        [JsonProperty("_l")]
        public String tipoactividad { get; set; }

        [JsonProperty("_m")]
        public String fecha_incidencia { get; set; }
        [JsonProperty("_n")]
        public String ciudad { get; set; }
        [JsonProperty("_o")]
        public String perfil { get; set; }
        [JsonProperty("_p")]
        public String semana { get; set; }

        [JsonProperty("_q")]
        public String producto { get; set; }

        [JsonProperty("_r")]
        public String cod_equipo { get; set; }
        [JsonProperty("_s")]
        public String cod_material { get; set; }
        [JsonProperty("_t")]
        public String cod_pdv { get; set; }
        [JsonProperty("_u")]
        public string subcanal2 { get; set; }
        [JsonProperty("_v")]
        public int periodo2 { get; set; }

        [JsonProperty("_w")]
        public int anio { get; set; }
        [JsonProperty("_x")]
        public int mes { get; set; }
        [JsonProperty("_y")]
        public int elemento { get; set; }
        [JsonProperty("_z")]
        public string dias { get; set; }
    }
}