﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucky.Xplora.Models;
using Newtonsoft.Json;
using Lucky.Xplora.Models.NwRepStdAASS;

namespace Lucky.Xplora.Models.NewReportingHenkel
{
    public class Nw_Rep_HenkelAASS
    {
        public List<M_Combo> Filtro(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Filtro oRp = MvcApplication._Deserialize<Response_NWRepStd_Filtro>
                (MvcApplication._Servicio_Operativa.NwRepHenkel_Filtros_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        #region Visibilidad
        public E_Visibilidad_Henkel_AASS Consulta_Visibilidad(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visibilidad_Hk_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visibilidad_Hk_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visibilidad_henkel_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public E_Visibilidad_Henkel_AASS_v3 Consulta_Visibilidadv3(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visibilidad_v3_Hk_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visibilidad_v3_Hk_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visibilidad_v3_henkel_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }  //aqui llega el total
        public E_Visibilidad_Henkel_AASS_Efectividad Consulta_Efectividad(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visib_Efectividad_Hk_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visib_Efectividad_Hk_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visib_Efectividad_henkel_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<M_Combo> Filtros(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visib_Filtro_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visib_Filtro_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visib_Filtro_Henkel_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<E_Visibilidad_Henkel_AASS_Excel> Descarga(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visib_Excel_Hk_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visib_Excel_Hk_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visib_Excel_Henkel_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }

        #endregion
        #region Competencia
        public Competencia_Aje Objeto_Competencia(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Competencia oRp = MvcApplication._Deserialize<Response_AjeReporting_Competencia>(
                    MvcApplication._Servicio_Operativa.Henkel_Competencia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
        public List<E_Reporting_Competencia_Detalle> Lista_CompetenciaDetalle(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Competencia_Detalle oRp = MvcApplication._Deserialize<Response_AjeReporting_Competencia_Detalle>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Competencia_Detalle_Henkel(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
        #endregion
    }


    #region Visibilidad

    #region Entidades
    public class E_Visibilidad_Henkel_AASS
    {
        [JsonProperty("_a")]
        public E_Visibilidad_Henkel_AASS_v1 vista1 { get; set; }
        [JsonProperty("_b")]
        public E_Visibilidad_Henkel_AASS_v2 vista2 { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v1
    {
        [JsonProperty("_a")]
        public List<String> ListPeriodo { get; set; }
        [JsonProperty("_b")]
        public List<String> ListEmpresa { get; set; }
        [JsonProperty("_c")]
        public List<E_Visibilidad_Henkel_AASS_v1_plantilla> Grafico1 { get; set; }
        [JsonProperty("_d")]
        public List<E_Visibilidad_Henkel_AASS_v1_plantilla2> Grafico2 { get; set; }
        [JsonProperty("_e")]
        public List<String> Coberturados { get; set; }
        [JsonProperty("_f")]
        public List<E_Visibilidad_Henkel_AASS_v1_plantilla3> Grafico3 { get; set; }
        [JsonProperty("_g")]
        public List<String> ListMarca { get; set; }
        [JsonProperty("_h")]
        public List<String> lPdv_Visitados { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v1_plantilla
    {
        [JsonProperty("_a")]
        public int Cod_Elemento { get; set; }
        [JsonProperty("_b")]
        public String Nom_Elemento { get; set; }
        [JsonProperty("_c")]
        public int Cantidad { get; set; }
        [JsonProperty("_d")]
        public String Porcentaje { get; set; }
        [JsonProperty("_e")]
        public String Color { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v1_plantilla2
    {
        [JsonProperty("_a")]
        public String Nombre { get; set; }
        [JsonProperty("_b")]
        public List<String> Valores { get; set; }
        [JsonProperty("_c")]
        public String Color { get; set; }
        [JsonProperty("_d")]
        public List<E_Visibilidad_Henkel_AASS_v1_val> valores2 { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v1_plantilla3
    {
        [JsonProperty("_a")]
        public String Nombre { get; set; }
        [JsonProperty("_b")]
        public List<String> Valores { get; set; }
        [JsonProperty("_c")]
        public String Color { get; set; }
        [JsonProperty("_d")]
        public List<E_Visibilidad_Henkel_AASS_v1_val2> valores2 { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v1_val
    {
        [JsonProperty("_a")]
        public String valor { get; set; }
        [JsonProperty("_b")]
        public String Porcentaje { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v1_val2
    {
        [JsonProperty("_a")]
        public String valor { get; set; }
        [JsonProperty("_b")]
        public String Porcentaje { get; set; }
        [JsonProperty("_c")]
        public String idempresa { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v2
    {
        [JsonProperty("_a")]
        public List<String> ListPeriodo { get; set; }
        [JsonProperty("_b")]
        public List<E_Visibilidad_Henkel_AASS_v2_plantilla> Grafico1 { get; set; }
        [JsonProperty("_c")]
        public List<E_Visibilidad_Henkel_AASS_v2_plantilla> Grafico2 { get; set; }
        [JsonProperty("_d")]
        public List<E_Visibilidad_Henkel_AASS_v2_item> Materiales { get; set; }
        [JsonProperty("_e")]
        public List<E_Visibilidad_Henkel_AASS_v2_plantilla> Grafico3 { get; set; }

    }
    public class E_Visibilidad_Henkel_AASS_v2_item
    {
        [JsonProperty("_a")]
        public int Codigo { get; set; }
        [JsonProperty("_b")]
        public String Valor { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v2_plantilla
    {
        [JsonProperty("_a")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("_b")]
        public List<E_Visibilidad_Henkel_AASS_v2_pla_1> Grafico1 { get; set; }
        [JsonProperty("_c")]
        public List<E_Visibilidad_Henkel_AASS_v2_pla_2> Grafico2 { get; set; }

    }
    public class E_Visibilidad_Henkel_AASS_v2_pla_1
    {
        [JsonProperty("_a")]
        public int Cod_Elemento { get; set; }
        [JsonProperty("_b")]
        public String Nom_Elemento { get; set; }
        [JsonProperty("_c")]
        public int Cantidad { get; set; }
        [JsonProperty("_d")]
        public String Porcentaje { get; set; }
        [JsonProperty("_e")]
        public String Color { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v2_pla_2
    {
        [JsonProperty("_a")]
        public int Cod_Elemento { get; set; }
        [JsonProperty("_b")]
        public String Nom_Elemento { get; set; }
        [JsonProperty("_c")]
        public List<String> Valor { get; set; }
    }

    public class E_Visibilidad_Henkel_AASS_v3
    {
        [JsonProperty("_a")]
        public List<String> Periodos { get; set; }
        [JsonProperty("_b")]
        public List<E_Visibilidad_Henkel_AASS_v1_plantilla> Pie { get; set; }
        [JsonProperty("_c")]
        public List<E_Visibilidad_Henkel_AASS_v1_plantilla2> Evolutivo { get; set; }
        [JsonProperty("_d")]
        public List<E_Visibilidad_Henkel_AASS_v3_item> Cab_Grilla { get; set; }
        [JsonProperty("_e")]
        public List<E_Visibilidad_Henkel_AASS_v3_Listitem> Content_Grilla { get; set; }

    }
    public class E_Visibilidad_Henkel_AASS_v3_item
    {
        [JsonProperty("_a")]
        public int Cod_Elemento { get; set; }
        [JsonProperty("_b")]
        public String Nom_Elmento { get; set; }
        [JsonProperty("_c")]
        public String Valor1 { get; set; }
        [JsonProperty("_d")]
        public String Valor2 { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_v3_Listitem
    {
        [JsonProperty("_a")]
        public String Cod_Elemento { get; set; }
        [JsonProperty("_b")]
        public String Nom_Elemento { get; set; }
        [JsonProperty("_c")]
        public String Total { get; set; }
        [JsonProperty("_d")]
        public List<String> Valores { get; set; }
    }

    public class E_Visibilidad_Henkel_AASS_Efectividad
    {
        [JsonProperty("_a")]
        public List<String> Periodos { get; set; }
        [JsonProperty("_b")]
        public List<E_Visibilidad_Henkel_AASS_Efect_Item> Grilla1 { get; set; }
        [JsonProperty("_c")]
        public List<E_Visibilidad_Henkel_AASS_Efect_Item> Evolutivo { get; set; }
        [JsonProperty("_d")]
        public List<E_Visibilidad_Henkel_AASS_Efect_Item> Grilla2 { get; set; }

    }
    public class E_Visibilidad_Henkel_AASS_Efect_Item
    {
        [JsonProperty("_a")]
        public String Elemento { get; set; }
        [JsonProperty("_b")]
        public String PDV { get; set; }
        [JsonProperty("_c")]
        public String Objetivo { get; set; }
        [JsonProperty("_d")]
        public String Real { get; set; }
        [JsonProperty("_e")]
        public String Efectividad { get; set; }
        [JsonProperty("_f")]
        public String Comentario { get; set; }
        [JsonProperty("_g")]
        public List<String> Valores { get; set; }
    }
    public class E_Visibilidad_Henkel_AASS_Excel
    {
        [JsonProperty("_a")]
        public String Cedis { get; set; }
        [JsonProperty("_b")]
        public String Cod_PDV { get; set; }
        [JsonProperty("_c")]
        public String Nom_PDV { get; set; }
        [JsonProperty("_d")]
        public String Empresa { get; set; }
        [JsonProperty("_e")]
        public String Categoria { get; set; }
        [JsonProperty("_f")]
        public String Marca { get; set; }
        [JsonProperty("_g")]
        public String Material { get; set; }
        [JsonProperty("_h")]
        public String Cantidad { get; set; }
        [JsonProperty("_i")]
        public String Fecha { get; set; }
    }

    public class E_DASH_Visibilidad_Henkel_AASS
    {
        [JsonProperty("_a")]
        public int Tipo { get; set; }
        [JsonProperty("_b")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("_c")]
        public string Nom_Empresa { get; set; }
        [JsonProperty("_d")]
        public int Cantidad { get; set; }
        [JsonProperty("_e")]
        public int Total { get; set; }
        [JsonProperty("_f")]
        public string Porcentaje { get; set; }
        [JsonProperty("_g")]
        public string Color { get; set; }
        [JsonProperty("_h")]
        public int Cod_Filtro { get; set; }
    }


    #endregion
    #region Response

    public class Response_NWRepStd_Visibilidad_Hk_AASS
    {
        [JsonProperty("_a")]
        public E_Visibilidad_Henkel_AASS Objeto { get; set; }
    }
    public class Response_NWRepStd_Visibilidad_v3_Hk_AASS
    {
        [JsonProperty("_a")]
        public E_Visibilidad_Henkel_AASS_v3 Objeto { get; set; }
    }
    public class Response_NWRepStd_Visib_Efectividad_Hk_AASS
    {
        [JsonProperty("_a")]
        public E_Visibilidad_Henkel_AASS_Efectividad Objeto { get; set; }
    }
    public class Response_NWRepStd_Visib_Excel_Hk_AASS
    {
        [JsonProperty("_a")]
        public List<E_Visibilidad_Henkel_AASS_Excel> Objeto { get; set; }
    }

    #endregion
    #endregion

    #region EE AA

    public class HenkelExhibicionAdicional : AHenkelExhibicionAdicional
    {
        [JsonProperty("_a")]
        public string Valorizado { get; set; }

        [JsonProperty("_b")]
        public string Cantidad { get; set; }

        [JsonProperty("_c")]
        public string TotalPdv { get; set; }

        [JsonProperty("_d")]
        public string PdvElemento { get; set; }

        [JsonProperty("_e")]
        public List<HenkelExhibicionAdicionalParticipacionMaterial> ParticipacionMaterial { get; set; }

        [JsonProperty("_f")]
        public List<HenkelExhibicionAdicionalParticipacionEvolutivoMes> EvolutivoMes { get; set; }

        [JsonProperty("_g")]
        public List<HenkelExhibicionAdicionalTarifario> TarifarioPeriodo { get; set; }

        [JsonProperty("_h")]
        public List<HenkelExhibicionAdicionalDetalle> Detalle { get; set; }
    }

    public class HenkelExhibicionAdicionalParticipacionMaterial
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string mat_cantidad { get; set; }

        [JsonProperty("_d")]
        public string mat_cantidad_porcentaje { get; set; }

        [JsonProperty("_e")]
        public string mat_valorizado { get; set; }

        [JsonProperty("_f")]
        public string mat_valorizado_porcentaje { get; set; }

        [JsonProperty("_g")]
        public List<HenkelExhibicionAdicionalParticipacionMaterialMarca> marca { get; set; }

        [JsonProperty("_h")]
        public List<HenkelExhibicionAdicionalParticipacionEvolutivoMes> evolutivo { get; set; }
    }

    public class HenkelExhibicionAdicionalParticipacionMaterialMarca
    {
        [JsonProperty("_a")]
        public int mar_id { get; set; }

        [JsonProperty("_b")]
        public string mar_descripcion { get; set; }

        [JsonProperty("_c")]
        public string mar_cantidad { get; set; }

        [JsonProperty("_d")]
        public string mar_cantidad_porcentaje { get; set; }

        [JsonProperty("_e")]
        public string mar_valorizado { get; set; }

        [JsonProperty("_f")]
        public string mar_valorizado_porcentaje { get; set; }

        [JsonProperty("_g")]
        public string color { get; set; }
    }

    public class HenkelExhibicionAdicionalParticipacionEvolutivoMes
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string mes { get; set; }

        [JsonProperty("_c")]
        public string valorizado { get; set; }

        [JsonProperty("_d")]
        public string cantidad { get; set; }

        [JsonProperty("_e")]
        public string pdv_total { get; set; }

        [JsonProperty("_f")]
        public string pdv_elemento { get; set; }

        [JsonProperty("_g")]
        public string cant_marca { get; set; }
    }

    public class HenkelExhibicionAdicionalTarifario
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string plazavea_valor { get; set; }

        [JsonProperty("_d")]
        public string tottus_valor { get; set; }

        [JsonProperty("_e")]
        public string metro_valor { get; set; }

        [JsonProperty("_f")]
        public string wong_valor { get; set; }

        [JsonProperty("_g")]
        public string vivanda_valor { get; set; }
    }

    public class HenkelExhibicionAdicionalDetalle
    {
        [JsonProperty("_a")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_b")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<HenkelExhibicionAdicionalDetalleMaterial> DetalleMaterial { get; set; }
    }

    public class HenkelExhibicionAdicionalDetalleMaterial : AHenkelExhibicionAdicional
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string foto { get; set; }

        [JsonProperty("_d")]
        public string existe { get; set; }

        [JsonProperty("_e")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_f")]
        public string pdv_descripcion { get; set; }
    }

    public abstract class AHenkelExhibicionAdicional
    {
        #region EE AA
        public HenkelExhibicionAdicional ExhibicionAdicional(E_Parametros_Henkel_AASS oRq)
        {
            return MvcApplication._Deserialize<ResponseHenkelExhibicionAdicional>(
                    MvcApplication._Servicio_Operativa.NWHenkelExhibicionAdicional(
                        MvcApplication._Serialize(new RequestHenkelExhibicionAdicional() { oParametros = oRq })
                    )
                ).Objeto;
        }
        public List<HenkelExhibicionAdicionalParticipacionMaterial> Excel_Exportar_exhibiciones_2(E_Parametros_Henkel_AASS request)
        {
            Reponse_Nw_excel_Exportar_exhibiciones oRp = MvcApplication._Deserialize<Reponse_Nw_excel_Exportar_exhibiciones>(
                MvcApplication._Servicio_Operativa.Nw_excel_Exportar_exhibiciones_2(
                MvcApplication._Serialize(request)
                )
               );
            return oRp.Objeto;
        }
        public List<E_Reporting_Excibicion_AASS> Exportar_Reporting_Exhibicion_AASS(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Exhibicion_AASS oRp = MvcApplication._Deserialize<Response_AjeReporting_Exhibicion_AASS>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Exhibicion_Henkel_AASS(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
        }
        public List<HenkelExhibicionAdicionalDetalleMaterialReporte> Exportar_Detalle_Reporting_Exhibicion_AASS(Request_AjeReporting_Parametros oRq)
        {
            Response_Henkel_Det_Reporting_Detalle_Exhibicion_AASS oRp = MvcApplication._Deserialize<Response_Henkel_Det_Reporting_Detalle_Exhibicion_AASS>(
                    MvcApplication._Servicio_Operativa.Exportar_Excel_Detalle_Henkel_Excibicion(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
            //return null;
        }
        #endregion

    }

    #region Request & Response

    public class Response_HenkelReporting_Detalle_Exhibicion_AASS
    {
        [JsonProperty("_a")]
        public List<HenkelExhibicionAdicionalDetalleMaterial> Objeto { get; set; }
    }
    #endregion
    #region Request & Response

    public class RequestHenkelExhibicionAdicional
    {
        [JsonProperty("a")]
        public E_Parametros_Henkel_AASS oParametros { get; set; }
    }

    public class ResponseHenkelExhibicionAdicional
    {
        [JsonProperty("_a")]
        public HenkelExhibicionAdicional Objeto { get; set; }
    }
    public class Response_AjeReporting_Exhibicion_Henkel_AASS
    {
        [JsonProperty("_a")]
        public List<E_Reporting_Excibicion_AASS> Objeto { get; set; }
    }
    public class Response_Henkel_Det_Reporting_Detalle_Exhibicion_AASS
    {
        [JsonProperty("_a")]
        public List<HenkelExhibicionAdicionalDetalleMaterialReporte> Objeto { get; set; }
    }
    #endregion

    #region EEAA
    public class E_ExhAdicional_Hijo
    {
        [JsonProperty("a")]
        public string Id { get; set; }
        [JsonProperty("b")]
        public string Valor { get; set; }
        [JsonProperty("c")]
        public string Foto { get; set; }
    }
    public class E_Parametros_Henkel_AASS
    {
        [JsonProperty("a")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("b")]
        public string Cod_SubCanal { get; set; }
        [JsonProperty("c")]
        public string Cod_Cadena { get; set; }
        [JsonProperty("d")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("e")]
        public string Cod_Empresa { get; set; }
        [JsonProperty("f")]
        public string Cod_Marca { get; set; }
        [JsonProperty("g")]
        public int Periodo { get; set; }
        [JsonProperty("h")]
        public string Cod_Zona { get; set; }
        [JsonProperty("i")]
        public string Cod_Distrito { get; set; }
        [JsonProperty("j")]
        public string Cod_PDV { get; set; }
        [JsonProperty("k")]
        public string segmento { get; set; }
        [JsonProperty("l")]
        public string cod_sku { get; set; }
        [JsonProperty("m")]
        public string Anio { get; set; }
        [JsonProperty("n")]
        public string Mes { get; set; }
        [JsonProperty("o")]
        public int cod_reporte { get; set; }
        [JsonProperty("p")]
        public string Cod_Ubieo { get; set; }
        [JsonProperty("q")]
        public string Cod_Cluster { get; set; }
        [JsonProperty("r")]
        public string Cod_Elemento { get; set; }
        [JsonProperty("s")]
        public string Parametros { get; set; }
        [JsonProperty("t")]
        public int opcion { get; set; }
    }
    public class E_ExhAdicional_AASS
    {
        [JsonProperty("_a")]
        public String ExhValorizado { get; set; }
        [JsonProperty("_b")]
        public String CantElement { get; set; }
        [JsonProperty("_c")]
        public String AlcancePDV { get; set; }
        [JsonProperty("_d")]
        public String pdvElement { get; set; }
        [JsonProperty("_e")]
        public List<E_ExhAdicional_Material_AASS> ListTarifarioMaterial { get; set; }
        [JsonProperty("_f")]
        public List<E_Participacion_ExhAdicional_AASS> ParticipacionExhAd { get; set; }
        [JsonProperty("_g")]
        public String Cod_Material { get; set; }
        [JsonProperty("_h")]
        public String Desc_Material { get; set; }
        [JsonProperty("_i")]
        public List<E_ExhAdicional_Detalle_AASS> ListExhAdDetalle { get; set; }
    }
    public class E_ExhAdicional_Detalle_AASS
    {
        [JsonProperty("_a")]
        public String Cod_PDV { get; set; }
        [JsonProperty("_b")]
        public String PDV_Name { get; set; }
        [JsonProperty("_c")]
        public String Cod_Cadena { get; set; }
        [JsonProperty("_d")]
        public String Desc_Material { get; set; }
        [JsonProperty("_e")]
        public String valor { get; set; }
        [JsonProperty("_f")]
        public int presencia { get; set; }
        [JsonProperty("_g")]
        public String Cod_Material { get; set; }
        [JsonProperty("_h")]
        public List<E_ExhAdicional_Hijo> Hijo { get; set; }
        [JsonProperty("_i")]
        public String Foto { get; set; }
    }
    public class E_Participacion_ExhAdicional_AASS
    {
        [JsonProperty("_a")]
        public String CodMaterial { get; set; }
        [JsonProperty("_b")]
        public String DescMaterial { get; set; }
        [JsonProperty("_c")]
        public String cant_element { get; set; }
        [JsonProperty("_d")]
        public String cant_porc { get; set; }
        [JsonProperty("_e")]
        public String cant_total { get; set; }
        [JsonProperty("_f")]
        public String valor { get; set; }
        [JsonProperty("_g")]
        public String valor_porc { get; set; }
        [JsonProperty("_h")]
        public String valor_total { get; set; }
    }
    public class E_ExhAdicional_Material_AASS
    {
        [JsonProperty("_a")]
        public String CodMaterial { get; set; }
        [JsonProperty("_b")]
        public String DescMaterial { get; set; }
        [JsonProperty("_c")]
        public String TipoMaterial { get; set; }
        [JsonProperty("_d")]
        public String valor { get; set; }
        [JsonProperty("_e")]
        public String cod_cadena { get; set; }
        [JsonProperty("_f")]
        public String cadena { get; set; }
        [JsonProperty("_h")]
        public List<E_ExhAdicional_Hijo> Hijo { get; set; }
    }
    public class Reponse_Nw_excel_Exportar_exhibiciones
    {
        [JsonProperty("_a")]
        public List<HenkelExhibicionAdicionalParticipacionMaterial> Objeto { get; set; }
    }
    public class HenkelExhibicionAdicionalDetalleMaterialReporte
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string foto { get; set; }

        [JsonProperty("_d")]
        public string existe { get; set; }

        [JsonProperty("_e")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_f")]
        public string pdv_descripcion { get; set; }
    }
    #endregion

    #endregion

    #region Competencia

    #endregion

}