﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Lucky.Entity.Common.Servicio.NewReportingHenkel;
using Newtonsoft.Json;
using Lucky.Xplora.Models.HenkelReporting;

namespace Lucky.Xplora.Models.NewReportingHenkel
{
    public class M_Dashboard_Henkel
    {

        //    public List<E_Precio_AASS_DASH> IndexPrice(Request_NWRepStd_General request)
        //    {
        //        Response_NWRepStd_Precio_Dash oRp = MvcApplication._Deserialize<Response_NWRepStd_Precio_Dash>(MvcApplication._Servicio_Operativa.NwRepStd_Precio_Dash_AASS(MvcApplication._Serialize(request)));

        //        return oRp.Objeto;
        //    }
        //    public List<E_DASH_Visibilidad_AJE_AASS> Visibilidad(Request_NWRepStd_General request)
        //    {
        //        Response_NWRepStd_Visibilidad_Dash oRp = MvcApplication._Deserialize<Response_NWRepStd_Visibilidad_Dash>(MvcApplication._Servicio_Operativa.NwRepStd_Visibilidad_Dash_AASS(MvcApplication._Serialize(request)));

        //        return oRp.Objeto;
        //    }
        //    public List<M_Combo> Filtros(Request_NWRepStd_General request)
        //    {
        //        Response_NWRepStd_Filtro oRp = MvcApplication._Deserialize<Response_NWRepStd_Filtro>(MvcApplication._Servicio_Operativa.NwRepStd_Filtro_Dash_AASS(MvcApplication._Serialize(request)));

        //        return oRp.Objeto;
        //    }
        //    public E_Ventas_AASS_DASH Ventas_x_Categoria(Request_NWRepStd_General request)
        //    {
        //        Response_NWRepStd_Ventas_catg_Dash oRp = MvcApplication._Deserialize<Response_NWRepStd_Ventas_catg_Dash>(MvcApplication._Servicio_Operativa.NwRepStd_VentasCatg_Dash_AASS(MvcApplication._Serialize(request)));

        //        return oRp.Objeto;
        //    }
        //    public List<E_SOP_AASS_DASH> SOP(Request_NWRepStd_General request)
        //    {
        //        Response_NWRepStd_SOP_Dash oRp = MvcApplication._Deserialize<Response_NWRepStd_SOP_Dash>(MvcApplication._Servicio_Operativa.NwRepStd_SOP_Dash_AASS(MvcApplication._Serialize(request)));

        //        return oRp.Objeto;
        //    }
        //}

        //public class M_Dashboard_Aje_Service
        //{
        //    public List<E_Participacion_Dash> Participacion(E_Parametros_Aje_AASS oParametros)
        //    {
        //        Response_Paticipacion_Dash oRp;
        //        Request_ParametrosGral_AJE oRequest = new Request_ParametrosGral_AJE();
        //        oRequest.parametros = oParametros;
        //        try
        //        {
        //            oRp = MvcApplication._Deserialize<Response_Paticipacion_Dash>(
        //                    MvcApplication._Servicio_Operativa.NwRepStd_Consultar_Participacion_Dash(
        //                        MvcApplication._Serialize(oRequest)
        //                    )
        //                );
        //            return oRp.Response;
        //        }
        //        catch (Exception ex)
        //        {
        //            return null;
        //        }

        //    }

        //    public List<E_IndexPrice_Dash> IndexPrice(E_Parametros_Aje_AASS oParametros)
        //    {
        //        Response_IndexPrice_Dash oRp;
        //        Request_ParametrosGral_AJE oRequest = new Request_ParametrosGral_AJE();
        //        oRequest.parametros = oParametros;
        //        try
        //        {
        //            oRp = MvcApplication._Deserialize<Response_IndexPrice_Dash>(
        //                    MvcApplication._Servicio_Operativa.NwRepStd_Consultar_IndexPrice_Dash(
        //                        MvcApplication._Serialize(oRequest)
        //                    )
        //                );
        //            return oRp.Response;
        //        }
        //        catch (Exception ex)
        //        {
        //            return null;
        //        }

        //    }

        //    public List<E_OOS_AASS_DASH> IndexStockOut(Request_AjeDasgAASS_Parametros oRq)
        //    {
        //        Response_AjeOos_DashAASS oRp = MvcApplication._Deserialize<Response_AjeOos_DashAASS>(
        //                MvcApplication._Servicio_Operativa.Aje_DashAASS_OOS(
        //                    MvcApplication._Serialize(oRq)
        //                )
        //            );

        //        return oRp.Objeto;
        //    }

        //    public List<E_EXHADICIONAL_AASS_DASH> IndexExhAdicional(Request_AjeDasgAASS_Parametros oRq)
        //    {
        //        Response_AjeExhAdicional_DashAASS oRp = MvcApplication._Deserialize<Response_AjeExhAdicional_DashAASS>(
        //                MvcApplication._Servicio_Operativa.Aje_DashAASS_ExhAdicional(
        //                    MvcApplication._Serialize(oRq)
        //                )
        //            );

        //        return oRp.Objeto;
        //    }

        //    public E_INCIDENCIAS_AASS_DASH IndexIncidencias(Request_AjeDasgAASS_Parametros oRq)
        //    {
        //        Response_AjeIncidencia_DashAASS oRp = MvcApplication._Deserialize<Response_AjeIncidencia_DashAASS>(
        //                MvcApplication._Servicio_Operativa.Aje_DashAASS_Incidencia(
        //                    MvcApplication._Serialize(oRq)
        //                )
        //            );

        //        return oRp.Objeto;
        //    }

        //    public List<E_VENTAS_AASS_DASH> IndexAlcanceVenta(Request_AjeDasgAASS_Parametros oRq)
        //    {
        //        Response_AjeVentas_DashAASS oRp = MvcApplication._Deserialize<Response_AjeVentas_DashAASS>(
        //                MvcApplication._Servicio_Operativa.Aje_DashAASS_Ventas(
        //                    MvcApplication._Serialize(oRq)
        //                )
        //            );

        //        return oRp.Objeto;
        //    }

        //}

        //public class E_Participacion_Dash
        //{
        //    [JsonProperty("_a")]
        //    public String id { get; set; }
        //    [JsonProperty("_b")]
        //    public String descripcion { get; set; }
        //    [JsonProperty("_c")]
        //    public String porcentaje { get; set; }
        //    [JsonProperty("_d")]
        //    public String valor { get; set; }
        //    [JsonProperty("_e")]
        //    public String color { get; set; }
        //    [JsonProperty("_f")]
        //    public String company { get; set; }
        //}
        //public class E_IndexPrice_Dash
        //{
        //    [JsonProperty("_a")]
        //    public String id { get; set; }
        //    [JsonProperty("_b")]
        //    public String descripcion { get; set; }
        //    [JsonProperty("_c")]
        //    public String precio { get; set; }
        //    [JsonProperty("_d")]
        //    public String indice_porc { get; set; }
        //    [JsonProperty("_e")]
        //    public String cantidad { get; set; }
        //    [JsonProperty("_f")]
        //    public String color { get; set; }
        //    [JsonProperty("_g")]
        //    public int propio { get; set; }
        //}

        //public class Request_ParametrosGral_AJE
        //{
        //    [JsonProperty("a")]
        //    public E_Parametros_Aje_AASS parametros { get; set; }
        //}

        //#region << Response >>
        //public class Response_Paticipacion_Dash
        //{
        //    [JsonProperty("_a")]
        //    public List<E_Participacion_Dash> Response { get; set; }
        //}
        //public class Response_IndexPrice_Dash
        //{
        //    [JsonProperty("_a")]
        //    public List<E_IndexPrice_Dash> Response { get; set; }
        //}
        //#endregion

        //#region << Entidad Dashboard AASS >>
        //public class E_Precio_AASS_DASH
        //{
        //    [JsonProperty("_a")]
        //    public int Tipo { get; set; }
        //    [JsonProperty("_b")]
        //    public int Cod_Empresa { get; set; }
        //    [JsonProperty("_c")]
        //    public string Empresa { get; set; }
        //    [JsonProperty("_d")]
        //    public int Cod_Marca { get; set; }
        //    [JsonProperty("_e")]
        //    public string Marca { get; set; }
        //    [JsonProperty("_f")]
        //    public string Precio { get; set; }
        //    [JsonProperty("_g")]
        //    public string Indice { get; set; }
        //    [JsonProperty("_h")]
        //    public int Cantidad { get; set; }
        //    [JsonProperty("_i")]
        //    public int Cod_Tipo { get; set; }
        //    [JsonProperty("_j")]
        //    public string Color { get; set; }
        //    [JsonProperty("_k")]
        //    public string Cod_Categoria { get; set; }
        //}
        //public class E_DASH_Visibilidad_AJE_AASS
        //{
        //    [JsonProperty("_a")]
        //    public int Tipo { get; set; }
        //    [JsonProperty("_b")]
        //    public int Cod_Empresa { get; set; }
        //    [JsonProperty("_c")]
        //    public string Nom_Empresa { get; set; }
        //    [JsonProperty("_d")]
        //    public int Cantidad { get; set; }
        //    [JsonProperty("_e")]
        //    public int Total { get; set; }
        //    [JsonProperty("_f")]
        //    public string Porcentaje { get; set; }
        //    [JsonProperty("_g")]
        //    public string Color { get; set; }
        //    [JsonProperty("_h")]
        //    public int Cod_Filtro { get; set; }
        //}
        //public class E_Ventas_AASS_DASH
        //{
        //    [JsonProperty("_a")]
        //    public string Volumen { get; set; }
        //    [JsonProperty("_b")]
        //    public string Litros { get; set; }
        //    [JsonProperty("_c")]
        //    public string Soles { get; set; }
        //    [JsonProperty("_d")]
        //    public string Unidades { get; set; }
        //    [JsonProperty("_e")]
        //    public string Presentacion { get; set; }
        //}
        //public class E_SOP_AASS_DASH
        //{
        //    [JsonProperty("_a")]
        //    public int Cod_Empresa { get; set; }
        //    [JsonProperty("_b")]
        //    public string Empresa { get; set; }
        //    [JsonProperty("_c")]
        //    public int Tipo { get; set; }
        //    [JsonProperty("_d")]
        //    public string Servicio { get; set; }
        //    [JsonProperty("_e")]
        //    public int Cantidad { get; set; }
        //}


        //#endregion

        //#region << Response Dashboard AASS >>
        //public class Response_NWRepStd_Precio_Dash
        //{
        //    [JsonProperty("_a")]
        //    public List<E_Precio_AASS_DASH> Objeto { get; set; }
        //}
        //public class Response_NWRepStd_Visibilidad_Dash
        //{
        //    [JsonProperty("_a")]
        //    public List<E_DASH_Visibilidad_AJE_AASS> Objeto { get; set; }
        //}
        //public class Response_NWRepStd_Filtro
        //{
        //    [JsonProperty("_a")]
        //    public List<M_Combo> Objeto { get; set; }
        //}
        //public class Response_NWRepStd_Ventas_catg_Dash
        //{
        //    [JsonProperty("_a")]
        //    public E_Ventas_AASS_DASH Objeto { get; set; }
        //}
        //public class Response_NWRepStd_SOP_Dash
        //{
        //    [JsonProperty("_a")]
        //    public List<E_SOP_AASS_DASH> Objeto { get; set; }
        //}

        //#endregion

        //#region << yrodriguezs >>

        //#region Entity

        //public class E_OOS_AASS_DASH
        //{
        //    [JsonProperty("_a")]
        //    public String oos_cadena { get; set; }
        //    [JsonProperty("_b")]
        //    public String oos_porcentaje { get; set; }
        //    [JsonProperty("_c")]
        //    public String oos_perdida { get; set; }
        //    [JsonProperty("_d")]
        //    public String color { get; set; }
        //}

        //public class E_EXHADICIONAL_AASS_DASH
        //{
        //    [JsonProperty("_a")]
        //    public String exhadi_cadena { get; set; }
        //    [JsonProperty("_b")]
        //    public String exhadi_soles { get; set; }
        //    [JsonProperty("_c")]
        //    public String exhadi_valorizado { get; set; }
        //    [JsonProperty("_d")]
        //    public String exhadi_retribucion { get; set; }
        //}

        //public class E_INCIDENCIAS_AASS_DASH
        //{
        //    [JsonProperty("_a")]
        //    public List<E_INCIDENCIAS_AASS_DASH_HEAD> IncidenciaDashHead { get; set; }
        //    [JsonProperty("_b")]
        //    public List<E_INCIDENCIAS_AASS_DASH_BODY> IncidenciaDashBody { get; set; }
        //}
        //public class E_INCIDENCIAS_AASS_DASH_HEAD
        //{
        //    [JsonProperty("_a")]
        //    public String head_id { get; set; }
        //    [JsonProperty("_b")]
        //    public String head_rpl_id { get; set; }
        //    [JsonProperty("_c")]
        //    public String head_descripcion { get; set; }
        //    [JsonProperty("_d")]
        //    public String head_llave { get; set; }
        //}
        //public class E_INCIDENCIAS_AASS_DASH_BODY
        //{
        //    [JsonProperty("_a")]
        //    public String body_descripcion { get; set; }
        //    [JsonProperty("_b")]
        //    public String body_rpl_id7 { get; set; }
        //    [JsonProperty("_c")]
        //    public String body_rpl_id6 { get; set; }
        //    [JsonProperty("_d")]
        //    public String body_rpl_id5 { get; set; }
        //    [JsonProperty("_e")]
        //    public String body_rpl_id4 { get; set; }
        //    [JsonProperty("_f")]
        //    public String body_rpl_id3 { get; set; }
        //    [JsonProperty("_g")]
        //    public String body_rpl_id2 { get; set; }
        //    [JsonProperty("_h")]
        //    public String body_rpl_id1 { get; set; }
        //}

        //public class E_VENTAS_AASS_DASH
        //{
        //    [JsonProperty("_a")]
        //    public String venta_cadena { get; set; }
        //    [JsonProperty("_b")]
        //    public String venta_objetivo { get; set; }
        //    [JsonProperty("_c")]
        //    public String venta_alcance { get; set; }
        //}

        //#endregion

        //#region Request
        //public class Request_AjeDasgAASS_Parametros
        //{
        //    [JsonProperty("_a")]
        //    public int subcanal { get; set; }
        //    [JsonProperty("_b")]
        //    public String cadena { get; set; }
        //    [JsonProperty("_c")]
        //    public String categoria { get; set; }
        //    [JsonProperty("_d")]
        //    public String empresa { get; set; }
        //    [JsonProperty("_e")]
        //    public String anio { get; set; }
        //    [JsonProperty("_f")]
        //    public String mes { get; set; }
        //    [JsonProperty("_g")]
        //    public String id_zona { get; set; }
        //    [JsonProperty("_h")]
        //    public String segmento { get; set; }
        //}

        //#endregion

        //#region Response
        //public class Response_AjeOos_DashAASS
        //{
        //    [JsonProperty("_a")]
        //    public List<E_OOS_AASS_DASH> Objeto { get; set; }
        //}
        //public class Response_AjeExhAdicional_DashAASS
        //{
        //    [JsonProperty("_a")]
        //    public List<E_EXHADICIONAL_AASS_DASH> Objeto { get; set; }
        //}

        //public class Response_AjeIncidencia_DashAASS
        //{
        //    [JsonProperty("_a")]
        //    public E_INCIDENCIAS_AASS_DASH Objeto { get; set; }
        //}

        //public class Response_AjeVentas_DashAASS
        //{
        //    [JsonProperty("_a")]
        //    public List<E_VENTAS_AASS_DASH> Objeto { get; set; }
        //}

        //#endregion

        //#endregion

    }
}