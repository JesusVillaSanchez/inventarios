﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Canal : ACanal
    {
        [JsonProperty("a")]
        public int can_id { get; set; }

        [JsonProperty("c")]
        public string can_descripcion { get; set; }
    }

    public abstract class ACanal {
        /// <summary>
        /// Autor: Curiarte
        /// Fecha: 2015-08-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Canal> Lista(Request_GetCanal_Compania_Campania oRq)
        {
            Response_GetCanal_Compania_Campania oRp = MvcApplication._Deserialize<Response_GetCanal_Compania_Campania>(
                    MvcApplication._Servicio_Campania.GetCanal_Compania_Campania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: Curiarte
    /// Fecha: 27/07/2015
    /// </summary>
    public class Request_GetCanal_Compania_Campania
    {
        [JsonProperty("_a")]
        public int compania { get; set; }

        [JsonProperty("_b")]
        public string campania { get; set; }
    }
    public class Response_GetCanal_Compania_Campania
    {
        [JsonProperty("_a")]
        public List<Canal> Lista { get; set; }
    }

    #endregion
}
