﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.NwRepStdTRAD
{
    public class NwRepIPPFTrad
    {
        public E_IPPF_TRAD Consulta_G1(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_IPPF_G1 oRp = MvcApplication._Deserialize<Reponse_NWRepStd_IPPF_G1>(MvcApplication._Servicio_Operativa.NwRepStd_TRD_IPPF_G1(MvcApplication._Serialize(request)));

            return oRp.objeto;
        }
        public E_IPPF_TRAD_GRAD_2 Consulta_G2(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_IPPF_G2 oRp = MvcApplication._Deserialize<Reponse_NWRepStd_IPPF_G2>(MvcApplication._Servicio_Operativa.NwRepStd_TRD_IPPF_G2(MvcApplication._Serialize(request)));

            return oRp.objeto;
        }
        public List<M_Combo> Filtros(Request_NWRepStd_Tradicional request)
        {
            Response_NWRepStd_Filtro oRp = MvcApplication._Deserialize<Response_NWRepStd_Filtro>(MvcApplication._Servicio_Operativa.NwRepStd_TRD_IPPF_FILTRO(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public E_IPPF_TRAD_GRAD_3 Consulta_G3(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_IPPF_G3 oRp = MvcApplication._Deserialize<Reponse_NWRepStd_IPPF_G3>(MvcApplication._Servicio_Operativa.NwRepStd_TRD_IPPF_G3(MvcApplication._Serialize(request)));

            return oRp.objeto;
        }
        public E_IPPF_TRAD_GRAD_4_elemento Consulta_G4(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_IPPF_G4 oRp = MvcApplication._Deserialize<Reponse_NWRepStd_IPPF_G4>(MvcApplication._Servicio_Operativa.NwRepStd_TRD_IPPF_G4(MvcApplication._Serialize(request)));

            return oRp.objeto;
        }
        public List<E_IPPF_TRAD_GRAD_5_elemento> Consulta_G5(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_IPPF_G5 oRp = MvcApplication._Deserialize<Reponse_NWRepStd_IPPF_G5>(MvcApplication._Servicio_Operativa.NwRepStd_TRD_IPPF_G5(MvcApplication._Serialize(request)));

            return oRp.objeto;
        }
        public E_Dash_IPPF_SOP_GRAF Dash_SOP(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_DASH_IPPF_SOP oRp = MvcApplication._Deserialize<Reponse_NWRepStd_DASH_IPPF_SOP>(MvcApplication._Servicio_Operativa.NwRepStd_DASH_TRD_IPPF_SOP(MvcApplication._Serialize(request)));

            return oRp.objeto;
        }
        public E_Dash_IPPF_SOP_GRAF Dash_INCIDENCIA(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_DASH_IPPF_INC oRp = MvcApplication._Deserialize<Reponse_NWRepStd_DASH_IPPF_INC>(MvcApplication._Servicio_Operativa.NwRepStd_DASH_TRD_IPPF_INCI(MvcApplication._Serialize(request)));
            return oRp.objeto;
        }
        public E_Dash_IPPF_STOCKOUT Dash_STOCKOUT(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_DASH_IPPF_STOCKOUT oRp = MvcApplication._Deserialize<Reponse_NWRepStd_DASH_IPPF_STOCKOUT>(MvcApplication._Servicio_Operativa.NwRepStd_DASH_TRD_IPPF_STOCKOUT(MvcApplication._Serialize(request)));
            return oRp.objeto;
        }
        public List<E_Dash_IPPF_COBERTURA_GRAF> Dash_COBERTURA(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_DASH_IPPF_COBERTURA oRp = MvcApplication._Deserialize<Reponse_NWRepStd_DASH_IPPF_COBERTURA>(MvcApplication._Servicio_Operativa.NwRepStd_DASH_TRD_IPPF_COBERTURA(MvcApplication._Serialize(request)));
            return oRp.objeto;
        }
        public List<E_Dash_IPPF_EXHI_VALORIZADO> Dash_EXHIBIVAL(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_DASH_IPPF_EXHIBIVAL oRp = MvcApplication._Deserialize<Reponse_NWRepStd_DASH_IPPF_EXHIBIVAL>(MvcApplication._Servicio_Operativa.NwRepStd_DASH_TRD_IPPF_EXHIBIVAL(MvcApplication._Serialize(request)));
            return oRp.objeto;
        }
        public List<E_Dash_IPPF_Cumplimiento> Dash_CUMPLIMIENTO(Request_NWRepStd_Tradicional request)
        {
            Reponse_NWRepStd_DASH_IPPF_CUMPLI oRp = MvcApplication._Deserialize<Reponse_NWRepStd_DASH_IPPF_CUMPLI>(MvcApplication._Servicio_Operativa.NwRepStd_DASH_TRD_IPPF_CUMPLI(MvcApplication._Serialize(request)));
            return oRp.objeto;
        }

    }


    #region << Request - Response >>
    public class Request_NWRepStd_Tradicional
    {
        [JsonProperty("_a")]
        public String Equipo { get; set; }
        [JsonProperty("_b")]
        public int Anio { get; set; }
        [JsonProperty("_c")]
        public int Mes { get; set; }
        [JsonProperty("_d")]
        public int Region { get; set; }
        [JsonProperty("_e")]
        public int Persona { get; set; }
        [JsonProperty("_f")]
        public int Distribuidora { get; set; }
        [JsonProperty("_g")]
        public int Opcion { get; set; }
        [JsonProperty("_h")]
        public String Parametros { get; set; }
        [JsonProperty("_i")]
        public int Elemento { get; set; }
        [JsonProperty("_j")]
        public int Marca { get; set; }
        [JsonProperty("_k")]
        public String Categoria { get; set; }
        [JsonProperty("_l")]
        public String Material { get; set; }
    }
    public class Reponse_NWRepStd_IPPF_G1
    {
        [JsonProperty("_a")]
        public E_IPPF_TRAD objeto { get; set; }
    }
    public class Reponse_NWRepStd_IPPF_G2
    {
        [JsonProperty("_a")]
        public E_IPPF_TRAD_GRAD_2 objeto { get; set; }
    }
    public class Response_NWRepStd_Filtro
    {
        [JsonProperty("_a")]
        public List<M_Combo> Objeto { get; set; }
    }
    public class Reponse_NWRepStd_IPPF_G3
    {
        [JsonProperty("_a")]
        public E_IPPF_TRAD_GRAD_3 objeto { get; set; }
    }
    public class Reponse_NWRepStd_IPPF_G4
    {
        [JsonProperty("_a")]
        public E_IPPF_TRAD_GRAD_4_elemento objeto { get; set; }
    }
    public class Reponse_NWRepStd_IPPF_G5
    {
        [JsonProperty("_a")]
        public List<E_IPPF_TRAD_GRAD_5_elemento> objeto { get; set; }
    }
    public class Reponse_NWRepStd_DASH_IPPF_SOP
    {
        [JsonProperty("_a")]
        public E_Dash_IPPF_SOP_GRAF objeto { get; set; }
    }
    public class Reponse_NWRepStd_DASH_IPPF_INC
    {
        [JsonProperty("_a")]
        public E_Dash_IPPF_SOP_GRAF objeto { get; set; }
    }
    public class Reponse_NWRepStd_DASH_IPPF_STOCKOUT
    {
        [JsonProperty("_a")]
        public E_Dash_IPPF_STOCKOUT objeto { get; set; }
    }
    public class Reponse_NWRepStd_DASH_IPPF_COBERTURA
    {
        [JsonProperty("_a")]
        public List<E_Dash_IPPF_COBERTURA_GRAF> objeto { get; set; }
    }
    public class Reponse_NWRepStd_DASH_IPPF_EXHIBIVAL
    {
        [JsonProperty("_a")]
        public List<E_Dash_IPPF_EXHI_VALORIZADO> objeto { get; set; }
    }
    public class Reponse_NWRepStd_DASH_IPPF_CUMPLI 
    {
        [JsonProperty("_a")]
        public List<E_Dash_IPPF_Cumplimiento> objeto { get; set; }
    }

    #endregion
    #region << Entidades >>
    public class E_IPPF_TRAD
    {
        [JsonProperty("_a")]
        public List<E_IPPF_TRAD_ZONA> Repzona { get; set; }
        [JsonProperty("_f")]
        public List<E_IPPF_TRAD_GRAD_1> Hijos { get; set; }
    }
    public class E_IPPF_TRAD_ZONA
    {
        [JsonProperty("_a")]
        public int anio { get; set; }
        [JsonProperty("_b")]
        public String Cod_Sector { get; set; }
        [JsonProperty("_c")]
        public String Sector { get; set; }
        [JsonProperty("_d")]
        public String Suma { get; set; }
        [JsonProperty("_e")]
        public String Total { get; set; }
        [JsonProperty("_f")]
        public String Porcentaje { get; set; }
    }
    public class E_IPPF_TRAD_GRAD_1
    {
        [JsonProperty("_a")]
        public String Cod_Distrib { get; set; }
        [JsonProperty("_b")]
        public String Distrib { get; set; }
        [JsonProperty("_c")]
        public String Suma { get; set; }
        [JsonProperty("_d")]
        public String Cant_Total { get; set; }
        [JsonProperty("_e")]
        public String Porcentaje { get; set; }
        [JsonProperty("_f")]
        public List<String> Mes { get; set; }
        [JsonProperty("_g")]
        public List<String> Valores { get; set; }
        [JsonProperty("_h")]
        public String Color { get; set; }
        [JsonProperty("_i")]
        public String Cod_Sector { get; set; }
    }
    public class E_IPPF_TRAD_GRAD_2
    {
        [JsonProperty("_a")]
        public E_IPPF_TRAD_GRAD_2_elemento Inventario { get; set; }
        [JsonProperty("_b")]
        public E_IPPF_TRAD_GRAD_2_elemento Posicion { get; set; }
        [JsonProperty("_c")]
        public E_IPPF_TRAD_GRAD_2_elemento Presentacion { get; set; }
        [JsonProperty("_d")]
        public E_IPPF_TRAD_GRAD_2_elemento Frio { get; set; }
        [JsonProperty("_e")]
        public E_IPPF_TRAD_GRAD_2_elemento Total { get; set; }
    }
    public class E_IPPF_TRAD_GRAD_2_elemento
    {
        [JsonProperty("_a")]
        public String Nombre { get; set; }
        [JsonProperty("_b")]
        public String Cantidad { get; set; }
        [JsonProperty("_c")]
        public String Porcentaje { get; set; }
        [JsonProperty("_d")]
        public List<String> Meses { get; set; }
        [JsonProperty("_e")]
        public List<String> valores { get; set; }
        [JsonProperty("_f")]
        public List<String> Criterio1 { get; set; }
        [JsonProperty("_g")]
        public List<String> Criterio2 { get; set; }
        [JsonProperty("_h")]
        public List<String> Criterio3 { get; set; }
    }
    public class E_IPPF_TRAD_GRAD_3
    {
        [JsonProperty("_a")]
        public List<E_IPPF_TRAD_GRAD_3_elemento> Sector { get; set; }
        [JsonProperty("_c")]
        public List<E_IPPF_TRAD_GRAD_3_elemento> Distribuidora { get; set; }
    }
    public class E_IPPF_TRAD_GRAD_3_elemento
    {
        [JsonProperty("_a")]
        public int Cod_Distribuidora { get; set; }
        [JsonProperty("_b")]
        public String Distribuidora { get; set; }
        [JsonProperty("_c")]
        public int Inicio { get; set; }
        [JsonProperty("_d")]
        public int Medio { get; set; }
        [JsonProperty("_e")]
        public int Todo { get; set; }
        [JsonProperty("_f")]
        public int Cod_Sector { get; set; }
        [JsonProperty("_g")]
        public List<String> Meses { get; set; }
        [JsonProperty("_h")]
        public List<E_IPPF_TRAD_GRAD_3_valores> valores { get; set; }
        [JsonProperty("_i")]
        public String Nom_Sector { get; set; }
    }
    public class E_IPPF_TRAD_GRAD_3_valores
    {
        [JsonProperty("_a")]
        public int Inicio { get; set; }
        [JsonProperty("_b")]
        public int Medio { get; set; }
        [JsonProperty("_c")]
        public int Todo { get; set; }
    }
    public class E_IPPF_TRAD_GRAD_4_elemento
    {
        [JsonProperty("_a")]
        public String Cod_Distribuidora { get; set; }
        [JsonProperty("_b")]
        public String Distribuidora { get; set; }
        [JsonProperty("_c")]
        public String Inicio { get; set; }
        [JsonProperty("_d")]
        public String Medio { get; set; }
        [JsonProperty("_e")]
        public String Todo { get; set; }
        [JsonProperty("_f")]
        public String Vencidos { get; set; }
        [JsonProperty("_g")]
        public String Total_PDV { get; set; }
        [JsonProperty("_h")]
        public String Por_Inicio { get; set; }
        [JsonProperty("_i")]
        public String Por_Medio { get; set; }
        [JsonProperty("_j")]
        public String Por_Todo { get; set; }
        [JsonProperty("_k")]
        public String Por_Vencido { get; set; }
        [JsonProperty("_l")]
        public String Pie_Inicio { get; set; }
        [JsonProperty("_m")]
        public String Pie_Medio { get; set; }
        [JsonProperty("_n")]
        public String Pie_Todo { get; set; }
        [JsonProperty("_o")]
        public List<E_IPPF_TRAD_GRAD_4_elemento_marca> Marca_Inventario { get; set; }
    }
    public class E_IPPF_TRAD_GRAD_4_elemento_marca
    {
        [JsonProperty("_a")]
        public int Cod_Marca { get; set; }
        [JsonProperty("_b")]
        public String Marca { get; set; }
        [JsonProperty("_c")]
        public int Cant_PDV { get; set; }
        [JsonProperty("_d")]
        public String Porcentaje { get; set; }
    }
    public class E_IPPF_TRAD_GRAD_5_elemento
    {
        [JsonProperty("_a")]
        public String Cod_pdv { get; set; }
        [JsonProperty("_b")]
        public String Nom_pdv { get; set; }
        [JsonProperty("_c")]
        public String Direccion { get; set; }
        [JsonProperty("_d")]
        public String Fec_reg_cel { get; set; }
        [JsonProperty("_e")]
        public String Detalle { get; set; }
        [JsonProperty("_f")]
        public String Punto { get; set; }
        [JsonProperty("_g")]
        public String Foto { get; set; }
        [JsonProperty("_h")]
        public String Penalizado { get; set; }
        [JsonProperty("_i")]
        public String Cant_Marca { get; set; }
        [JsonProperty("_j")]
        public String Marcas { get; set; }
        [JsonProperty("_k")]
        public String Criterio { get; set; }
    }
    public class E_Dash_IPPF_SOP
    {
        [JsonProperty("_a")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("_b")]
        public String Empresa { get; set; }
        [JsonProperty("_c")]
        public int Tipo_Servicio { get; set; }
        [JsonProperty("_d")]
        public String Servicio { get; set; }
        [JsonProperty("_e")]
        public int Cantidad { get; set; }
    }
    public class E_Dash_IPPF_SOP_GRAF
    {
        [JsonProperty("_a")]
        public List<String> Categorais { get; set; }
        [JsonProperty("_b")]
        public List<E_Dash_IPPF_SOP_GRAF_SERIE> Serires { get; set; }
    }
    public class E_Dash_IPPF_SOP_GRAF_SERIE
    {
        [JsonProperty("_a")]
        public String Nombre { get; set; }
        [JsonProperty("_b")]
        public List<String> Data { get; set; }
    }
    public class E_Dash_IPPF_INCIDENCIA
    {
        [JsonProperty("_a")]
        public int Num_Mes { get; set; }
        [JsonProperty("_b")]
        public String Mes { get; set; }
        [JsonProperty("_c")]
        public int Cant_Inc { get; set; }
        [JsonProperty("_d")]
        public int Cant_Result { get; set; }
        [JsonProperty("_e")]
        public String Porcentaje { get; set; }
        [JsonProperty("_f")]
        public int Tipo_Servicio { get; set; }
        [JsonProperty("_g")]
        public String Servicio { get; set; }
    }
    public class E_Dash_IPPF_STOCKOUT
    {
        [JsonProperty("_a")]
        public int Cantidad { get; set; }
        [JsonProperty("_b")]
        public int Total { get; set; }
        [JsonProperty("_c")]
        public String Porcentaje { get; set; }
    }
    public class E_Dash_IPPF_COBERTURA_GRAF
    {
        [JsonProperty("_a")]
        public int Tipo { get; set; }
        [JsonProperty("_b")]
        public List<String> Categorias { get; set; }
        [JsonProperty("_c")]
        public List<E_Dash_IPPF_COBERTURA_item> Objetivo { get; set; }
        [JsonProperty("_d")]
        public List<E_Dash_IPPF_COBERTURA_item> Resultado { get; set; }
        [JsonProperty("_e")]
        public String Total { get; set; }
    }
    public class E_Dash_IPPF_COBERTURA_item
    {
        [JsonProperty("_a")]
        public String Valor { get; set; }
        [JsonProperty("_b")]
        public String Porcentaje { get; set; }
    }
    public class E_Dash_IPPF_EXHI_VALORIZADO
    {
        [JsonProperty("_a")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("_b")]
        public String Empresa { get; set; }
        [JsonProperty("_c")]
        public String Cant_Soles { get; set; }
        [JsonProperty("_d")]
        public String Porcentaje { get; set; }
        [JsonProperty("_e")]
        public String Color { get; set; }
    }
    public class E_Dash_IPPF_Cumplimiento
    {
        [JsonProperty("_a")]
        public int Tipo { get; set; }
        [JsonProperty("_b")]
        public int Cod_Marca { get; set; }
        [JsonProperty("_c")]
        public string Cod_Producto { get; set; }
        [JsonProperty("_d")]
        public string Nombre { get; set; }
        [JsonProperty("_e")]
        public string Cant_Total { get; set; }
        [JsonProperty("_f")]
        public string Cantidad { get; set; }
        [JsonProperty("_g")]
        public string Porcentaje { get; set; }

    }
    #endregion
}