﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-16
    /// </summary>
    public class Oficina : AOficina
    {
        [JsonProperty("a")]
        public int ofi_id { get; set; }

        [JsonProperty("c")]
        public string ofi_descripcion { get; set; }
    }

    public abstract class AOficina {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-16
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Oficina> Lista(Request_Oficinas_Por_Campania oRq) {
            Response_Oficinas_Por_Campania oRp;

            oRp = MvcApplication._Deserialize<Response_Oficinas_Por_Campania>(
                    MvcApplication._Servicio_Campania.Listar_Oficinas_Por_CodCampania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-11
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Oficina> Lista(Request_Listar_Oficinas_Por_CodCompania_Reports oRq)
        {
            Response_Listar_Oficinas_Por_CodCompania_Reports oRp;

            oRp = MvcApplication._Deserialize<Response_Listar_Oficinas_Por_CodCompania_Reports>(
                MvcApplication._Servicio_Campania.Listar_Oficinas_Por_CodCompania(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Oficina> Lista(Request_GetOficina_Opcion oRq)
        {
            Response_GetOficina_Opcion oRp;

            oRp = MvcApplication._Deserialize<Response_GetOficina_Opcion>(
                MvcApplication._Servicio_Campania.GetOficina_Opcion(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-16
    /// </summary>
    public class Request_Oficinas_Por_Campania
    {
        [JsonProperty("a")]
        public string campania { get; set; }
    }
    public class Response_Oficinas_Por_Campania
    {
        [JsonProperty("a")]
        public List<Oficina> Lista { get; set; }
    }

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 2015-02-11
    /// </summary>
    public class Request_Listar_Oficinas_Por_CodCompania_Reports
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string compania { get; set; }
    }
    public class Response_Listar_Oficinas_Por_CodCompania_Reports
    {
        [JsonProperty("a")]
        public List<Oficina> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-14
    /// </summary>
    public class Request_GetOficina_Opcion
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }
    }
    public class Response_GetOficina_Opcion
    {
        [JsonProperty("_a")]
        public List<Oficina> Lista { get; set; }
    }

    #endregion
}