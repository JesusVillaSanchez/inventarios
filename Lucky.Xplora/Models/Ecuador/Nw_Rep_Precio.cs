﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Ecuador
{
    public class Nw_Rep_Precio : AE_Nw_Rep_Precio
    {
        [JsonProperty("_a")]
        public nw_graf_precio_cadena grafico_cadena { get; set; }
        [JsonProperty("_b")]
        public nw_graf_precio_evolutivo grafico_evolutivo { get; set; }
        [JsonProperty("_c")]
        public nw_table_precio_evolutivo tabla_evolutivo { get; set; }
    }
    #region Grafico 1
    public class nw_graf_precio_cadena
    {
        [JsonProperty("_a")]
        public List<string> Categoria { get; set; }
        [JsonProperty("_b")]
        public List<nw_graf_precio_cadena_valor> valor { get; set; }
    }
    public class nw_graf_precio_cadena_valor
    {
        [JsonProperty("_a")]
        public string valor { get; set; }
        [JsonProperty("_b")]
        public string color { get; set; }
    }
    #endregion

    #region Grafico 2
    public class nw_graf_precio_evolutivo
    {
        [JsonProperty("_a")]
        public List<string> Categoria { get; set; }
        [JsonProperty("_b")]
        public List<nw_graf_precio_evolutivo_valor> valores { get; set; }
    }
    public class nw_graf_precio_evolutivo_valor
    {
        [JsonProperty("_a")]
        public List<string> valor { get; set; }
        [JsonProperty("_b")]
        public string nom_serie { get; set; }
        [JsonProperty("_c")]
        public string color_serie { get; set; }
    }
    #endregion

    #region tabla
    public class nw_table_precio_evolutivo
    {
        [JsonProperty("_a")]
        public List<string> Periodos { get; set; }
        [JsonProperty("_b")]
        public List<nw_table_precio_evolutivo_fila> fila { get; set; }
    }
    public class nw_table_precio_evolutivo_fila
    {
        [JsonProperty("_a")]
        public string item { get; set; }
        [JsonProperty("_b")]
        public List<string> valores { get; set; }
    }
    #endregion

    public abstract class AE_Nw_Rep_Precio
    {
        public Nw_Rep_Precio Consulta_reporte_precio(Request_Nw_Reporting_Ecu_Precio request)
        {
            Reponse_Nw_Reporting_Ecu_Precio oRp;
            oRp = MvcApplication._Deserialize<Reponse_Nw_Reporting_Ecu_Precio>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Reporting_Ecu_Precio(
                        MvcApplication._Serialize(request)
                    )
                );
            return oRp.obj;
        }
    }
    public class Request_Nw_Reporting_Ecu_Precio
    {
        [JsonProperty("_a")]
        public string cod_equipo { get; set; }
        [JsonProperty("_b")]
        public string cod_categoria { get; set; }
        [JsonProperty("_c")]
        public string cod_marca { get; set; }
        [JsonProperty("_d")]
        public string cod_producto { get; set; }
        [JsonProperty("_e")]
        public string cod_periodo { get; set; }
        [JsonProperty("_f")]
        public string cod_perfil { get; set; }
        [JsonProperty("_g")]
        public string cod_provincia { get; set; }
        [JsonProperty("_h")]
        public string cod_ciudad { get; set; }

    }
    public class Reponse_Nw_Reporting_Ecu_Precio 
    {
        [JsonProperty("_a")]
        public Nw_Rep_Precio obj { get; set; }
    }
}