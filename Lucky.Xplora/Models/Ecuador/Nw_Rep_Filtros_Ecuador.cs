﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Ecuador
{
    public class Nw_Rep_Filtros_Ecuador : ANw_Rep_Filtros_Ecuador
    {
        [JsonProperty("a")]
        public string Value { get; set; }
        [JsonProperty("b")]
        public string Text { get; set; }
    }
    public class Response_Nw_Reporting_Ecu_Filtros
    {
        [JsonProperty("a")]
        public List<Nw_Rep_Filtros_Ecuador> Response { get; set; }
    }
    public class Request_Nw_Reporting_Ecu_Filtros
    {
        [JsonProperty("_a")]
        public string parametros { get; set; }
        [JsonProperty("_b")]
        public int opcion { get; set; }

    }
    public abstract class ANw_Rep_Filtros_Ecuador
    {
        public List<Nw_Rep_Filtros_Ecuador> Consul_Filtros(Request_Nw_Reporting_Ecu_Filtros oRq)
        {
            Response_Nw_Reporting_Ecu_Filtros oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Ecu_Filtros>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Rep_Ecu_Filtro(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }
}