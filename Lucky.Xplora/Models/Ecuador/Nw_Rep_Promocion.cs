﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Ecuador
{
    public class Nw_Rep_Promocion : AE_Nw_Rep_Promocion
    {
        [JsonProperty("_a")]
        public List<string> Marcas { get; set; }
        [JsonProperty("_b")]
        public List<nw_rep_promocion_tabla> tabla { get; set; }
    }
    public class nw_rep_promocion_tabla
    {
        [JsonProperty("_a")]
        public string cod_marca { get; set; }
        [JsonProperty("_b")]
        public string nom_marca { get; set; }
        [JsonProperty("_c")]
        public string valor { get; set; }
        [JsonProperty("_d")]
        public string porcentaje { get; set; }
        [JsonProperty("_e")]
        public string color { get; set; }
    }
    public abstract class AE_Nw_Rep_Promocion
    {
        public Nw_Rep_Promocion Consulta_reporte_promocion(Request_Nw_Reporting_Ecu_Promocion request)
        {
            Reponse_Nw_Reporting_Ecu_Promocion oRp;
            oRp = MvcApplication._Deserialize<Reponse_Nw_Reporting_Ecu_Promocion>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Reporting_Ecu_Promocion(
                        MvcApplication._Serialize(request)
                    )
                );
            return oRp.obj;
        }
    }

    public class Request_Nw_Reporting_Ecu_Promocion
    {
        [JsonProperty("_a")]
        public string cod_equipo { get; set; }
        [JsonProperty("_b")]
        public string cod_categoria { get; set; }
        [JsonProperty("_c")]
        public string cod_provincia { get; set; }
        [JsonProperty("_d")]
        public string cod_ciudad { get; set; }
        [JsonProperty("_e")]
        public string cod_cadena { get; set; }
        [JsonProperty("_f")]
        public string cod_periodo { get; set; }
        [JsonProperty("_g")]
        public string cod_perfil { get; set; }
    }
    public class Reponse_Nw_Reporting_Ecu_Promocion
    {
        [JsonProperty("_a")]
        public Nw_Rep_Promocion obj { get; set; }
    } 

}