﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Ecuador
{
    public class Nw_Rep_Parametros_Ini : ANw_Rep_Parametros_Ini
    {
        [JsonProperty("_a")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("_b")]
        public string Cod_Marca { get; set; }
        [JsonProperty("_c")]
        public string Cod_Producto { get; set; }
    }
    public abstract class ANw_Rep_Parametros_Ini
    {
        public List<Nw_Rep_Parametros_Ini> Consulta_Param_Ini(Request_Nw_Reporting_Ecu_Param_Ini request)
        {
            Reponse_Nw_Reporting_Ecu_Param_Ini oRp;
            oRp = MvcApplication._Deserialize<Reponse_Nw_Reporting_Ecu_Param_Ini>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Reporting_Ecu_Param_Ini(
                        MvcApplication._Serialize(request)
                    )
                );
            return oRp.obj;
        }
    }
    public class Request_Nw_Reporting_Ecu_Param_Ini
    {
        [JsonProperty("_a")]
        public string cod_equipo { get; set; }
        [JsonProperty("_b")]
        public string cod_reporte { get; set; }
    }
    public class Reponse_Nw_Reporting_Ecu_Param_Ini
    {
        [JsonProperty("_a")]
        public List<Nw_Rep_Parametros_Ini> obj { get; set; }
    }
}