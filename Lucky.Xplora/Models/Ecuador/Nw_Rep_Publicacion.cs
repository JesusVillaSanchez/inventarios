﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Ecuador
{
    public class Nw_Rep_Publicacion : AE_Nw_Rep_Publicacion
    { 
        [JsonProperty("_a")]
        public nw_graf_publicacion_evolutivo grafico_evolutivo { get; set; }
        [JsonProperty("_b")]
        public List<nw_table_publicacion> tabla { get; set; }
        [JsonProperty("_c")]
        public nw_graf_publicacion_evolutivo grafico_evolutivo_Emp { get; set; }
        [JsonProperty("_d")]
        public List<nw_table_publicacion> tabla_Emp { get; set; }
    }
    #region Grafico

    public class nw_graf_publicacion_evolutivo
    {
        [JsonProperty("_a")]
        public List<string> Categoria { get; set; }
        [JsonProperty("_b")]
        public List<nw_graf_publicacion_evolutivo_valor> valores { get; set; }
    }
    public class nw_graf_publicacion_evolutivo_valor
    {
        [JsonProperty("_a")]
        public List<string> valor { get; set; }
        [JsonProperty("_b")]
        public string nom_serie { get; set; }
        [JsonProperty("_c")]
        public string color_serie { get; set; }
    }

    #endregion
    #region Tabla
    public class nw_table_publicacion
    {
        [JsonProperty("_a")]
        public string Cadena { get; set; }
        [JsonProperty("_b")]
        public string Marca { get; set; }
        [JsonProperty("_c")]
        public string Cantidad { get; set; }
        [JsonProperty("_d")]
        public string Color { get; set; }
    }
    #endregion

    public abstract class AE_Nw_Rep_Publicacion
    {
        public Nw_Rep_Publicacion Consulta_reporte_publicacion(Request_Nw_Reporting_Ecu_Publicacion request)
        {
            Reponse_Nw_Reporting_Ecu_Publicacion oRp;
            oRp = MvcApplication._Deserialize<Reponse_Nw_Reporting_Ecu_Publicacion>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Reporting_Ecu_Publicacion(
                        MvcApplication._Serialize(request)
                    )
                );
            return oRp.obj;
        }
    }

    public class Request_Nw_Reporting_Ecu_Publicacion
    {
        [JsonProperty("_a")]
        public string cod_equipo { get; set; }
        [JsonProperty("_b")]
        public string cod_categoria { get; set; }
        [JsonProperty("_c")]
        public string cod_provincia { get; set; }
        [JsonProperty("_d")]
        public string cod_ciudad { get; set; }
        [JsonProperty("_e")]
        public string cod_cadena { get; set; }
        [JsonProperty("_f")]
        public string cod_periodo { get; set; }
        [JsonProperty("_g")]
        public string cod_perfil { get; set; }
    }
    public class Reponse_Nw_Reporting_Ecu_Publicacion
    {
        [JsonProperty("_a")]
        public Nw_Rep_Publicacion obj { get; set; }
    }


    #region Fotografico

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-02
    /// </summary>
    public class Nw_Rep_Fotografico : ANw_Rep_Fotografico
    {
        [JsonProperty("_a")]
        public string cad_descripcion { get; set; }

        [JsonProperty("_b")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_c")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_d")]
        public string com_descripcion { get; set; }

        [JsonProperty("_e")]
        public string mar_descripcion { get; set; }

        [JsonProperty("_f")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_g")]
        public string pbl_descripcion { get; set; }

        [JsonProperty("_h")]
        public string gru_descripcion { get; set; }

        [JsonProperty("_i")]
        public string foto { get; set; }
    }

    public abstract class ANw_Rep_Fotografico {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-07-02
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Nw_Rep_Fotografico> Lista(Request_Nw_Reporting_Ecu_Publicacion oRq)
        {
            Reponse_Nw_Rep_Fotografico oRp = MvcApplication._Deserialize<Reponse_Nw_Rep_Fotografico>(
                    MvcApplication._Servicio_Operativa.Nw_Rep_Fotografico(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-07-02
    /// </summary>
    public class Reponse_Nw_Rep_Fotografico
    {
        [JsonProperty("_a")]
        public List<Nw_Rep_Fotografico> Lista { get; set; }
    }

    #endregion

    #endregion
}