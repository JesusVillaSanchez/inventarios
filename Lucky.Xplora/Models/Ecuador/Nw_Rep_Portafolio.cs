﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Ecuador
{
    public class Nw_Rep_Portafolio : AE_Nw_Rep_Portafolio
    {
        [JsonProperty("_a")]
        public nw_graf_assorment_evolutivo grafico_evolutivo { get; set; }
    }
    #region Grafico

    public class nw_graf_assorment_evolutivo
    {
        [JsonProperty("_a")]
        public List<string> Categoria { get; set; }
        [JsonProperty("_b")]
        public List<nw_graf_assorment_evolutivo_valor> valores { get; set; }
        [JsonProperty("_c")]
        public string Cantidad { get; set; }
    }
    public class nw_graf_assorment_evolutivo_valor
    {
        [JsonProperty("_a")]
        public List<string> valor { get; set; }
        [JsonProperty("_b")]
        public string nom_serie { get; set; }
        [JsonProperty("_c")]
        public string color_serie { get; set; }
    }

    #endregion
    public abstract class AE_Nw_Rep_Portafolio
    {
        public Nw_Rep_Portafolio Consulta_reporte_portafolio(Request_Nw_Reporting_Ecu_Assorment request)
        {
            Reponse_Nw_Reporting_Ecu_Assorment oRp;
            oRp = MvcApplication._Deserialize<Reponse_Nw_Reporting_Ecu_Assorment>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Reporting_Ecu_Assorment(
                        MvcApplication._Serialize(request)
                    )
                );
            return oRp.obj;
        }
    }
    public class Request_Nw_Reporting_Ecu_Assorment
    {
        [JsonProperty("_a")]
        public string cod_equipo { get; set; }
        [JsonProperty("_b")]
        public string cod_categoria { get; set; }
        [JsonProperty("_c")]
        public string cod_provincia { get; set; }
        [JsonProperty("_d")]
        public string cod_ciudad { get; set; }
        [JsonProperty("_e")]
        public string cod_cadena { get; set; }
        [JsonProperty("_f")]
        public string cod_periodo { get; set; }
        [JsonProperty("_g")]
        public string cod_perfil { get; set; }
    }
    public class Reponse_Nw_Reporting_Ecu_Assorment
    {
        [JsonProperty("_a")]
        public Nw_Rep_Portafolio obj { get; set; }
    }
}