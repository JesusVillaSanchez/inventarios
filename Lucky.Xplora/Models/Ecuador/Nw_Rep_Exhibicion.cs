﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Ecuador
{
    public class Nw_Rep_Exhibicion : AE_Nw_Rep_Exhibicion
    {
        [JsonProperty("_a")]
        public nw_graf_exhibicion_evolutivo grafico_evolutivo { get; set; }
        [JsonProperty("_b")]
        public List<nw_table_exhibicion> tabla { get; set; }
    }
    public abstract class AE_Nw_Rep_Exhibicion
    {
        public Nw_Rep_Exhibicion Consulta_reporte_exhibicion(Request_Nw_Reporting_Ecu_Exhibicion request)
        {
            Reponse_Nw_Reporting_Ecu_Exhibicion oRp;
            oRp = MvcApplication._Deserialize<Reponse_Nw_Reporting_Ecu_Exhibicion>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Reporting_Ecu_Exhibicion(
                        MvcApplication._Serialize(request)
                    )
                );
            return oRp.obj;
        }
    }
    public class nw_graf_exhibicion_evolutivo
    {
        [JsonProperty("_a")]
        public List<string> Categoria { get; set; }
        [JsonProperty("_b")]
        public List<nw_graf_exhibicion_evolutivo_valor> valores { get; set; }
    }
    public class nw_graf_exhibicion_evolutivo_valor
    {
        [JsonProperty("_a")]
        public List<string> valor { get; set; }
        [JsonProperty("_b")]
        public string nom_serie { get; set; }
        [JsonProperty("_c")]
        public string color_serie { get; set; }
    }
    public class nw_table_exhibicion
    {
        [JsonProperty("_a")]
        public string Cadena { get; set; }
        [JsonProperty("_b")]
        public string Marca { get; set; }
        [JsonProperty("_c")]
        public string Cantidad { get; set; }
        [JsonProperty("_d")]
        public string Color { get; set; }
    }
    public class Request_Nw_Reporting_Ecu_Exhibicion
    {
        [JsonProperty("_a")]
        public string cod_equipo { get; set; }
        [JsonProperty("_b")]
        public string cod_categoria { get; set; }
        [JsonProperty("_c")]
        public string cod_provincia { get; set; }
        [JsonProperty("_d")]
        public string cod_ciudad { get; set; }
        [JsonProperty("_e")]
        public string cod_cadena { get; set; }
        [JsonProperty("_f")]
        public string cod_periodo { get; set; }
        [JsonProperty("_g")]
        public string cod_perfil { get; set; }
    }
    public class Reponse_Nw_Reporting_Ecu_Exhibicion 
    {
        [JsonProperty("_a")]
        public Nw_Rep_Exhibicion obj { get; set; }
    }
}