﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Kimberly.Reporting
{

    public class E_Precio_Kc
    {
        public E_Precio_AASS_Kimberly Get_KcPrecio(Request_KimberlyReporting_Precio request)
        {
            Response_KimberlyReporting_Precio oRp = MvcApplication._Deserialize<Response_KimberlyReporting_Precio>(MvcApplication._Servicio_Operativa.Nw_Consultar_Precio_AASS_Kimberly(MvcApplication._Serialize(request)));
            return oRp.Objeto;
        }

        public E_Precio_AASS_Kimberly Get_KcPrecio_det(Request_KimberlyReporting_Precio request)
        {
            Response_KimberlyReporting_Precio oRp = MvcApplication._Deserialize<Response_KimberlyReporting_Precio>(MvcApplication._Servicio_Operativa.Nw_Consultar_Precio_AASS_Kimberly(MvcApplication._Serialize(request)));
            return oRp.Objeto;
        }
    }
    #region Request - Response
    public class Request_KimberlyReporting_Precio
    {
        [JsonProperty("_a")]
        public String Parametros { get; set; }
        [JsonProperty("_b")]
        public int op { get; set; }
        [JsonProperty("_c")]
        public int subcanal { get; set; }
        [JsonProperty("_d")]
        public String cadena { get; set; }
        [JsonProperty("_e")]
        public String categoria { get; set; }
        [JsonProperty("_f")]
        public String empresa { get; set; }
        [JsonProperty("_g")]
        public String marca { get; set; }
        [JsonProperty("_h")]
        public String SKU { get; set; }
        [JsonProperty("_i")]
        public int periodo { get; set; }
        [JsonProperty("_j")]
        public String zona { get; set; }
        [JsonProperty("_k")]
        public String distrito { get; set; }
        [JsonProperty("_l")]
        public String tienda { get; set; }
        [JsonProperty("_m")]
        public String segmento { get; set; }
        [JsonProperty("_n")]
        public int Persona { get; set; }
        [JsonProperty("_o")]
        public int mes { get; set; }
        [JsonProperty("_p")]
        public int anio { get; set; }
        [JsonProperty("_q")]
        public int idtipo_material { get; set; }
        [JsonProperty("_r")]
        public string min { get; set; }
        [JsonProperty("_s")]
        public string max { get; set; }
        [JsonProperty("_x")]
        public int opcion { get; set; }
    }
    public class Response_KimberlyReporting_Precio
    {
        [JsonProperty("_a")]
        public E_Precio_AASS_Kimberly Objeto { get; set; }
    }
    #endregion
    #region Entidad
    public class E_Precio_AASS_Kimberly
    {
        [JsonProperty("_a")]
        public List<ListPeriodos> ListPeriodos { get; set; }
        [JsonProperty("_b")]
        public List<E_Precio_AASS_Fila_Kimberly> Filas { get; set; }
        [JsonProperty("_c")]
        public List<E_lista_cadena> cadenas { get; set; }
        [JsonProperty("_d")]
        public List<E_lista_Producto> productos { get; set; }

    }
    public class E_Precio_AASS_Fila_Kimberly
    {
        [JsonProperty("_a")]
        public String cadena { get; set; }
        [JsonProperty("_b")]
        public int cod_cadena { get; set; }
        [JsonProperty("_c")]
        public String categoria { get; set; }
        [JsonProperty("_d")]
        public String cod_categoria { get; set; }
        [JsonProperty("_e")]
        public String tier { get; set; }
        [JsonProperty("_f")]
        public int id_marca { get; set; }
        [JsonProperty("_g")]
        public String marca { get; set; }
        [JsonProperty("_h")]
        public String nombre_producto { get; set; }
        [JsonProperty("_i")]
        public String id_producto { get; set; }
        [JsonProperty("_j")]
        public int conteo { get; set; }
        [JsonProperty("_k")]
        public List<String> Valores { get; set; }
        [JsonProperty("_l")]
        public List<String> Valor_producto { get; set; }
    }
    public class ListPeriodos
    {
        [JsonProperty("_a")]
        public int orden { get; set; }
        [JsonProperty("_b")]
        public String cod_periodo { get; set; }
        [JsonProperty("_c")]
        public String descripcion { get; set; }
    }
    public class E_lista_cadena
    {
        [JsonProperty("_a")]
        public String cod_cadena { get; set; }
        [JsonProperty("_b")]
        public String nombre_cadena { get; set; }
    }
    public class E_lista_Producto
    {
        [JsonProperty("_a")]
        public String cod_producto { get; set; }
        [JsonProperty("_b")]
        public String nombre_producto { get; set; }
    }
    #endregion
}