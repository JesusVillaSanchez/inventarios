﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace Lucky.Xplora.Models.Kimberly.Reporting
{
    public class Competencia_Kimberly : A_Competencia_Kimberly
    {
        [JsonProperty("_a")]
        public List<E_Competencia_ACadena_Kimberly> CompetenciaxCadena { get; set; }
        [JsonProperty("_b")]
        public List<E_Competencia_PEmpresa_Kimberly> CompetenciaxEmpresa { get; set; }
        [JsonProperty("_c")]
        public List<E_Competencia_TActividades_Kimberly> CompetenciaxActividades { get; set; }
        [JsonProperty("_d")]
        public List<E_Competencia_AEvolutivo_Kimberly> CompetenciaxEvolutivo { get; set; }
    }
    public class E_Competencia_ACadena_Kimberly
    {
        [JsonProperty("_a")]
        public int id_cadena { get; set; }
        [JsonProperty("_b")]
        public string cadena { get; set; }
        [JsonProperty("_c")]
        public string empresa { get; set; }
        [JsonProperty("_d")]
        public string cantidad { get; set; }
        [JsonProperty("_e")]
        public string total { get; set; }

        [JsonProperty("_f")]
        public string color { get; set; }
    }
    public class E_Competencia_PEmpresa_Kimberly
    {
        [JsonProperty("_a")]
        public string empresa { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }

        [JsonProperty("_c")]
        public string color { get; set; }
    }
    public class E_Competencia_TActividades_Kimberly
    {
        [JsonProperty("_a")]
        public string empresa { get; set; }
        [JsonProperty("_b")]
        public string actividad { get; set; }
        [JsonProperty("_c")]
        public string participacion { get; set; }
    }
    public class E_Competencia_AEvolutivo_Kimberly
    {
        [JsonProperty("_a")]
        public string periodo { get; set; }
        [JsonProperty("_b")]
        public string empresa { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }

    public abstract class A_Competencia_Kimberly
    {
        public Competencia_Kimberly Objeto_Competencia(Request_KimberlyReporting_Parametros oRq)
        {
            Response_KimberlyReporting_Competencia oRp = MvcApplication._Deserialize<Response_KimberlyReporting_Competencia>(
                    MvcApplication._Servicio_Operativa.Kimberly_Competencia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    public class E_Reporting_Competencia_Detalle_Kimberly : A_E_Reporting_Competencia_Detalle_Kimberly
    {
        [JsonProperty("a")]
        public string Id_Row { get; set; }
        [JsonProperty("b")]
        public string oficina { get; set; }
        [JsonProperty("c")]
        public string cadena { get; set; }
        [JsonProperty("d")]
        public string fecha { get; set; }
        [JsonProperty("e")]
        public string empresa { get; set; }
        [JsonProperty("f")]
        public string marca { get; set; }
        [JsonProperty("g")]
        public string tipo_actividad { get; set; }
        [JsonProperty("h")]
        public string descripcion_comercial { get; set; }
        [JsonProperty("i")]
        public string id_competencia { get; set; }
        [JsonProperty("j")]
        public string categoria { get; set; }
        [JsonProperty("k")]
        public string foto { get; set; }
        [JsonProperty("l")]
        public string alcance { get; set; }
        [JsonProperty("m")]
        public string vigencia { get; set; }
        [JsonProperty("n")]
        public string codigo { get; set; }
        [JsonProperty("o")]
        public string precio { get; set; }
        [JsonProperty("p")]
        public string gramaje { get; set; }
        [JsonProperty("q")]
        public string impulso { get; set; }
        [JsonProperty("r")]
        public string pop { get; set; }
        [JsonProperty("s")]
        public string comentarios_adicionales { get; set; }
    }

    public abstract class A_E_Reporting_Competencia_Detalle_Kimberly
    {
        public List<E_Reporting_Competencia_Detalle_Kimberly> Lista_CompetenciaDetalle(Request_KimberlyReporting_Parametros oRq)
        {
            Response_KimberlyReporting_Competencia_Detalle oRp = MvcApplication._Deserialize<Response_KimberlyReporting_Competencia_Detalle>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Competencia_Detalle_AASS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response


    public class Response_KimberlyReporting_Competencia
    {
        [JsonProperty("_a")]
        public Competencia_Kimberly Objeto { get; set; }
    }
    public class Response_KimberlyReporting_Competencia_Detalle
    {
        [JsonProperty("_a")]
        public List<E_Reporting_Competencia_Detalle_Kimberly> Objeto { get; set; }
    }

    public class Request_KimberlyReporting_Parametros
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }
        [JsonProperty("_b")]
        public int subcanal { get; set; }
        [JsonProperty("_c")]
        public String categoria { get; set; }
        [JsonProperty("_d")]
        public String marca { get; set; }
        [JsonProperty("_e")]
        public String periodo { get; set; }
        [JsonProperty("_f")]
        public String zona { get; set; }
        [JsonProperty("_g")]
        public String distrito { get; set; }

        [JsonProperty("_h")]
        public String segmento { get; set; }
        [JsonProperty("_i")]
        public String cadena { get; set; }
        [JsonProperty("_j")]
        public String empresa { get; set; }
        [JsonProperty("_k")]
        public String tienda { get; set; }
        [JsonProperty("_l")]
        public String tipoactividad { get; set; }

        [JsonProperty("_m")]
        public String fecha_incidencia { get; set; }
        [JsonProperty("_n")]
        public String ciudad { get; set; }
        [JsonProperty("_o")]
        public String perfil { get; set; }
        [JsonProperty("_p")]
        public String semana { get; set; }

        [JsonProperty("_q")]
        public String producto { get; set; }

        [JsonProperty("_r")]
        public String cod_equipo { get; set; }
        [JsonProperty("_s")]
        public String cod_material { get; set; }
        [JsonProperty("_t")]
        public String cod_pdv { get; set; }
        [JsonProperty("_u")]
        public string subcanal2 { get; set; }
        [JsonProperty("_v")]
        public int periodo2 { get; set; }

        [JsonProperty("_w")]
        public int anio { get; set; }
        [JsonProperty("_x")]
        public int mes { get; set; }
        [JsonProperty("_y")]
        public int elemento { get; set; }
        [JsonProperty("_z")]
        public string dias { get; set; }
    }

    #endregion
}