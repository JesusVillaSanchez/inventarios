﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.AlicorpMulticategoriaCanje;

namespace Lucky.Xplora.Models.Kimberly.Reporting
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-26
    /// </summary>
    public class Trade_Marketing : ATrade_Marketing 
    {
        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public int mes_id { get; set; }

        [JsonProperty("_c")]
        public string mes_descripcion { get; set; }

        [JsonProperty("_d")]
        public int cat_id { get; set; }

        [JsonProperty("_e")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_f")]
        public string marca { get; set; }

        [JsonProperty("_g")]
        public int can_id { get; set; }

        [JsonProperty("_h")]
        public string can_descripcion { get; set; }

        [JsonProperty("_i")]
        public string alcance { get; set; }

        [JsonProperty("_j")]
        public string objetivo { get; set; }

        [JsonProperty("_k")]
        public string actividad { get; set; }

        [JsonProperty("_l")]
        public string mecanica { get; set; }

        [JsonProperty("_m")]
        public string incidencia { get; set; }

        [JsonProperty("_n")]
        public string restriccion { get; set; }

        [JsonProperty("_o")]
        public string caja { get; set; }

        [JsonProperty("_p")]
        public string vigencia_inicio { get; set; }

        [JsonProperty("_q")]
        public string vigencia_fin { get; set; }

        [JsonProperty("_r")]
        public string inversion { get; set; }

        [JsonProperty("_s")]
        public string comentario { get; set; }

        [JsonProperty("_t")]
        public string asignacion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-30
    /// </summary>
    public class Trade_Marketing_Galeria
    {
        [JsonProperty("_a")]
        public int tmi_id { get; set; }

        [JsonProperty("_b")]
        public int tmi_anio { get; set; }

        [JsonProperty("_c")]
        public int tmi_mes { get; set; }

        [JsonProperty("_d")]
        public int cat_id { get; set; }

        [JsonProperty("_e")]
        public string tmi_titulo { get; set; }

        [JsonProperty("_f")]
        public string tmi_url { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-03-03
    /// </summary>
    public class Precio_Condicion
    {
        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public int mes_id { get; set; }

        [JsonProperty("_c")]
        public int can_id { get; set; }

        [JsonProperty("_d")]
        public int cat_id { get; set; }

        [JsonProperty("_e")]
        public string ktf_descripcion { get; set; }

        [JsonProperty("_f")]
        public string pro_sku { get; set; }

        [JsonProperty("_g")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_h")]
        public string precio_lista { get; set; }

        [JsonProperty("_i")]
        public string precio_lista_igv { get; set; }

        [JsonProperty("_j")]
        public string descuento_innovacion { get; set; }

        [JsonProperty("_k")]
        public string precio_descuento_innovacion { get; set; }

        [JsonProperty("_l")]
        public string precio_full { get; set; }

        [JsonProperty("_m")]
        public string reventa { get; set; }

        [JsonProperty("_n")]
        public string reventa_mayor { get; set; }

        [JsonProperty("_o")]
        public string reventa_menor { get; set; }

        [JsonProperty("_p")]
        public string mg_lista_up { get; set; }

        [JsonProperty("_q")]
        public string mg_full_up { get; set; }

        [JsonProperty("_r")]
        public string mg_lista_down { get; set; }

        [JsonProperty("_s")]
        public string mg_full_down { get; set; }

        [JsonProperty("_t")]
        public string mg_lista_down_mayor { get; set; }

        [JsonProperty("_u")]
        public string mg_full_down_mayor { get; set; }

        [JsonProperty("_v")]
        public string mg_lista_down_menor { get; set; }

        [JsonProperty("_w")]
        public string mg_full_down_menor { get; set; }

        [JsonProperty("_x")]
        public string pvp_sugerido { get; set; }

        [JsonProperty("_y")]
        public string descuento_minicadena { get; set; }

        [JsonProperty("_z")]
        public string descuento_innovacion_minicadena { get; set; }

        [JsonProperty("_aa")]
        public string precio_lista_paquete_con_igv { get; set; }

        [JsonProperty("_ab")]
        public string descuento_activacion { get; set; }

        [JsonProperty("_ac")]
        public string rec_activacion { get; set; }

        [JsonProperty("_ad")]
        public string precio_oferta { get; set; }

        [JsonProperty("_ae")]
        public string mg_full { get; set; }

        [JsonProperty("_af")]
        public string mg_oferta { get; set; }

        [JsonProperty("_ag")]
        public string conteo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-30
    /// </summary>
    public class Marketing
    {
        [JsonProperty("_a")]
        public int mkt_id { get; set; }

        [JsonProperty("_b")]
        public int ktf_id { get; set; }

        [JsonProperty("_c")]
        public int cat_id { get; set; }

        [JsonProperty("_d")]
        public string mkt_nombre { get; set; }

        [JsonProperty("_e")]
        public string mkt_fecha { get; set; }

        [JsonProperty("_f")]
        public string mkt_url { get; set; }

        [JsonProperty("_g")]
        public int med_id { get; set; }

        [JsonProperty("_h")]
        public string med_descripcion { get; set; }

        [JsonProperty("_i")]
        public int mkt_width { get; set; }

        [JsonProperty("_j")]
        public int mkt_height { get; set; }

        [JsonProperty("_k")]
        public string mkt_galeria { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-27
    /// </summary>
    public abstract class ATrade_Marketing {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-01-27
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Listado> Filtro(Parametro oRq)
        {
            Response_AlicorpMulticategoriaCanjeListado oRp = MvcApplication._Deserialize<Response_AlicorpMulticategoriaCanjeListado>(
                    MvcApplication._Servicio_Operativa.KimberlyTradeMarketing(
                        MvcApplication._Serialize(new Request_AlicorpMulticategoriaCanjeParametro() { Objeto = oRq })
                    )
                );

            return oRp.Lista;
        } 

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-01-27
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Trade_Marketing_Galeria> Lista(Parametro oRq)
        {
            return MvcApplication._Deserialize<Response_KimberlyTradeMarketingGaleria>(
                    MvcApplication._Servicio_Operativa.KimberlyTradeMarketing(
                        MvcApplication._Serialize(new Request_AlicorpMulticategoriaCanjeParametro() { Objeto = oRq })
                    )
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-03-03
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Precio_Condicion> PrecioCondicion(Parametro oRq)
        {
            Response_KimberlyPrecioCondicion oRp = MvcApplication._Deserialize<Response_KimberlyPrecioCondicion>(
                    MvcApplication._Servicio_Operativa.KimberlyTradeMarketing(
                        MvcApplication._Serialize(new Request_AlicorpMulticategoriaCanjeParametro() { Objeto = oRq })
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-30
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Marketing> Marketing(Parametro oRq)
        {
            return MvcApplication._Deserialize<Response_KimberlyMarketing>(
                    MvcApplication._Servicio_Operativa.KimberlyTradeMarketing(
                        MvcApplication._Serialize(new Request_AlicorpMulticategoriaCanjeParametro() { Objeto = oRq })
                    )
                ).Lista;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-27
    /// </summary>
    public class Response_KimberlyTradeMarketingGaleria
    {
        [JsonProperty("_a")]
        public List<Trade_Marketing_Galeria> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-03-03
    /// </summary>
    public class Response_KimberlyPrecioCondicion
    {
        [JsonProperty("_a")]
        public List<Precio_Condicion> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-05-30
    /// </summary>
    public class Response_KimberlyMarketing
    {
        [JsonProperty("_a")]
        public List<Marketing> Lista { get; set; }
    }
    #endregion

    #region Objetivo del mes (venta)
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-08-17
    /// </summary>
    public class ObjetivoMes
    {
        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public int mes { get; set; }

        [JsonProperty("_c")]
        public string canal { get; set; }

        [JsonProperty("_d")]
        public string sales_gba_ship_to { get; set; }

        [JsonProperty("_e")]
        public string desc_gba { get; set; }

        [JsonProperty("_f")]
        public string desc_gba_ic { get; set; }

        [JsonProperty("_g")]
        public string desc_gba2_ic { get; set; }

        [JsonProperty("_h")]
        public string sales_rep_ship_to { get; set; }

        [JsonProperty("_i")]
        public string desc_rep_ship_to { get; set; }

        [JsonProperty("_j")]
        public string cust_ship_to { get; set; }

        [JsonProperty("_k")]
        public string desc_ship_to { get; set; }

        [JsonProperty("_l")]
        public string cust_sold_to { get; set; }

        [JsonProperty("_m")]
        public string desc_sold_to { get; set; }

        [JsonProperty("_n")]
        public string business_sector { get; set; }

        [JsonProperty("_o")]
        public string desc_business_sector { get; set; }

        [JsonProperty("_p")]
        public string business_category { get; set; }

        [JsonProperty("_q")]
        public string desc_business_category { get; set; }

        [JsonProperty("_r")]
        public string projected_net_sales_cs { get; set; }

        [JsonProperty("_s")]
        public string projected_net_sales_gsu { get; set; }

        [JsonProperty("_t")]
        public string projected_net_sales_usd { get; set; }

        [JsonProperty("_u")]
        public string forecast_cs { get; set; }

        [JsonProperty("_v")]
        public string forecast_gsu { get; set; }

        [JsonProperty("_w")]
        public string forecast_usd { get; set; }

        [JsonProperty("_x")]
        public int dia { get; set; }

        [JsonProperty("_ba")]
        public string ns_cs { get; set; }

        [JsonProperty("_bb")]
        public string o_cs { get; set; }

        [JsonProperty("_bc")]
        public string niv_usd { get; set; }

        [JsonProperty("_bd")]
        public string niv_lc { get; set; }

        [JsonProperty("_be")]
        public string forecast_lc { get; set; }

        [JsonProperty("_bf")]
        public string tipo_cliente { get; set; }

        [JsonProperty("_bg")]
        public string cliente_pareto { get; set; }

        [JsonProperty("_bh")]
        public DateTime fec_hora { get; set; }

        [JsonProperty("_br")]
        public string forecast_niv { get; set; }

    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-08-31
    /// </summary>
    public class ObjetivoPrincipal
    {
        [JsonProperty("_a")]
        public List<ObjetivoAgrupa3> agrupa3 { get; set; }

        [JsonProperty("_b")]
        public string fecha { get; set; }
    }


    /// <summary>
    /// Autor: adarrigo
    /// Fecha: 2016-11-02
    /// </summary>
    public class ObjetivoAgrupa3
    {
        [JsonProperty("_a")]
        public string agrupa3 { get; set; }

        [JsonProperty("_b")]
        public List<ObjetivoAgrupacion> agrupacion { get; set; }

    }


    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-10-06
    /// </summary>
    public class ObjetivoAgrupacion
    {
        [JsonProperty("_a")]
        public string agrupacion { get; set; }

        [JsonProperty("_b")]
        public List<ObjetivoVenta> grupo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-08-17
    /// </summary>
    public class ObjetivoVenta : AObjetivoVenta 
    {
        [JsonProperty("_a")]
        public string grupo { get; set; }

        [JsonProperty("_b")]
        public List<ObjetivoVentaDetalle> detalle { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-08-17
    /// </summary>
    public class ObjetivoVentaDetalle
    {
        [JsonProperty("_a")]
        public string grupo_detalle { get; set; }

        [JsonProperty("_b")]
        public List<ObjetivoVentaDetalleCategoria> categoria { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-08-17
    /// </summary>
    public class ObjetivoVentaDetalleCategoria
    {
        [JsonProperty("_a")]
        public string categoria { get; set; }

        [JsonProperty("_b")]
        public string projected { get; set; }

        [JsonProperty("_c")]
        public string forecast { get; set; }

        [JsonProperty("_d")]
        public string alcance { get; set; }

        [JsonProperty("_e")]
        public string alcance_imagen { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-08-17
    /// </summary>
    public abstract class AObjetivoVenta {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-08-17
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public ObjetivoPrincipal Lista(Parametro oRq)
        {
            return MvcApplication._Deserialize<ResponseKimberlyObjetivoVenta>(
                    MvcApplication._Servicio_Operativa.KimberlyTradeMarketing(
                        MvcApplication._Serialize(new Request_AlicorpMulticategoriaCanjeParametro() { Objeto = oRq })
                    )
                ).Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-08-29
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public int Volcado(Parametro oRq)
        {
            return MvcApplication._Deserialize<ResponseKimberlyObjetivoVentaBulk>(
                    MvcApplication._Servicio_Operativa.KimberlyTradeMarketing(
                        MvcApplication._Serialize(new Request_AlicorpMulticategoriaCanjeParametro() { Objeto = oRq })
                    )
                ).Resultado;
        }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-08-17
    /// </summary>
    public class ResponseKimberlyObjetivoVenta
    {
        [JsonProperty("_a")]
        public ObjetivoPrincipal Objeto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-08-29
    /// </summary>
    public class ResponseKimberlyObjetivoVentaBulk
    {
        [JsonProperty("_a")]
        public Int32 Resultado { get; set; }
    }
    #endregion
}