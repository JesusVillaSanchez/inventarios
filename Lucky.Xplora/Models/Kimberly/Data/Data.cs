﻿using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.AlicorpMulticategoriaCanje;

using Newtonsoft;
using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.Models.Kimberly.Data
{
    #region Quiebres
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-08
    /// </summary>
    public class KimberlyQuiebre : AKimberlyQuiebre 
    {
        [JsonProperty("_a")]
        public List<KimberlyQuiebreInformacion> quiebre { get; set; }

        [JsonProperty("_b")]
        public List<KimberlyQuiebreMotivo> motivo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-08
    /// </summary>
    public class KimberlyQuiebreMotivo
    {
        [JsonProperty("_a")]
        public string mot_codigo { get; set; }

        [JsonProperty("_b")]
        public string mot_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-08
    /// </summary>
    public class KimberlyQuiebreInformacion
    {
        [JsonProperty("_a")]
        public int sup_id { get; set; }

        [JsonProperty("_b")]
        public string sup_nombre { get; set; }

        [JsonProperty("_c")]
        public int gie_id { get; set; }

        [JsonProperty("_d")]
        public string gie_nombre { get; set; }

        [JsonProperty("_e")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_f")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_g")]
        public string fecha { get; set; }

        [JsonProperty("_h")]
        public List<KimberlyQuiebreProducto> producto { get; set; }

        [JsonProperty("_i")]
        public int cad_id { get; set; }

        [JsonProperty("_j")]
        public string cad_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-08
    /// </summary>
    public class KimberlyQuiebreProducto
    {
        [JsonProperty("_a")]
        public string pro_sku { get; set; }

        [JsonProperty("_b")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_c")]
        public string cab_id { get; set; }

        [JsonProperty("_d")]
        public string det_id { get; set; }

        [JsonProperty("_e")]
        public string validado { get; set; }

        [JsonProperty("_f")]
        public string motivo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-08
    /// </summary>
    public class KimberlyQuiebreTemporal
    {
        [JsonProperty("_a")]
        public string cab_id { get; set; }

        [JsonProperty("_b")]
        public string gie_id { get; set; }

        [JsonProperty("_c")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_d")]
        public string fecha { get; set; }

        [JsonProperty("_e")]
        public string det_id { get; set; }

        [JsonProperty("_f")]
        public string pro_sku { get; set; }

        [JsonProperty("_g")]
        public string validado { get; set; }

        [JsonProperty("_h")]
        public string motivo { get; set; }

        [JsonProperty("_i")]
        public string crud { get; set; }

        [JsonProperty("_j")]
        public string archivo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-08
    /// </summary>
    public abstract class AKimberlyQuiebre
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-08
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<Listado> Lista(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyListado>(
                    MvcApplication._Servicio_Operativa.KimberlyQuiebre(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-08
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public KimberlyQuiebre Quiebre(Parametro parametro) {
            return MvcApplication._Deserialize<ResponseKimberlyQuiebre>(
                    MvcApplication._Servicio_Operativa.KimberlyQuiebre(
                        MvcApplication._Serialize(parametro)
                    )
                ).Quiebre;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-09
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public Int32 Carga(Parametro parametro) {
            return MvcApplication._Deserialize<ResponseKimberlyResultado>(
                        MvcApplication._Servicio_Operativa.KimberlyQuiebre(
                            MvcApplication._Serialize(parametro)
                        )
                    ).Resultado;
        }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-08
    /// </summary>
    public class ResponseKimberlyQuiebre
    {
        [JsonProperty("_a")]
        public KimberlyQuiebre Quiebre { get; set; }
    }
    public class ResponseKimberlyListado
    {
        [JsonProperty("_a")]
        public List<Listado> Lista { get; set; }
    }
    public class ResponseKimberlyResultado
    {
        [JsonProperty("_a")]
        public Int32 Resultado { get; set; }
    }
    #endregion

    #region Evaluacion
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-16
    /// </summary>
    public class KimberlyEvaluacion : AKimberlyEvaluacion 
    {
        [JsonProperty("_a")]
        public int rpl_id { get; set; }

        [JsonProperty("_b")]
        public int per_id { get; set; }

        [JsonProperty("_c")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_d")]
        public int cat_id { get; set; }

        [JsonProperty("_e")]
        public List<KimberlyEvaluacionGrupo> grupo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-24
    /// </summary>
    public class KimberlyEvaluacionGrupo
    {
        [JsonProperty("_a")]
        public string grp_descripcion { get; set; }

        [JsonProperty("_b")]
        public List<KimberlyEvaluacionGrupoPregunta> pregunta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-24
    /// </summary>
    public class KimberlyEvaluacionGrupoPregunta
    {
        [JsonProperty("_a")]
        public int pre_id { get; set; }

        [JsonProperty("_b")]
        public string pre_descripcion { get; set; }

        [JsonProperty("_c")]
        public string si_no { get; set; }

        [JsonProperty("_d")]
        public int motivo { get; set; }

        [JsonProperty("_e")]
        public List<Listado> lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-16
    /// </summary>
    public abstract class AKimberlyEvaluacion
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<Listado> Lista(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyListado>(
                    MvcApplication._Servicio_Operativa.KimberlyEvaluacion(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public KimberlyEvaluacion Encuesta(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyEvaluacion>(
                    MvcApplication._Servicio_Operativa.KimberlyEvaluacion(
                        MvcApplication._Serialize(parametro)
                    )
                ).Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-19
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public Int32 InsertaActualiza(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyResultado>(
                        MvcApplication._Servicio_Operativa.KimberlyEvaluacion(
                            MvcApplication._Serialize(parametro)
                        )
                    ).Resultado;
        }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-16
    /// </summary>
    public class ResponseKimberlyEvaluacion
    {
        [JsonProperty("_a")]
        public KimberlyEvaluacion Objeto { get; set; }
    }
    #endregion

    #region OSA
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-19
    /// </summary>
    public class KimberlyOsa : AKimberlyOsa 
    {
        [JsonProperty("_a")]
        public int rpl_id { get; set; }

        [JsonProperty("_b")]
        public int per_id { get; set; }

        [JsonProperty("_c")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_d")]
        public int cat_id { get; set; }

        [JsonProperty("_e")]
        public string pro_sku { get; set; }

        [JsonProperty("_f")]
        public string presencia_objetivo { get; set; }

        [JsonProperty("_g")]
        public string si_no { get; set; }

        [JsonProperty("_h")]
        public int motivo { get; set; }

        [JsonProperty("_i")]
        public string pro_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-19
    /// </summary>
    public class AKimberlyOsa
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-19
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public Int32 InsertaActualiza(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyResultado>(
                        MvcApplication._Servicio_Operativa.KimberlyEvaluacion(
                            MvcApplication._Serialize(parametro)
                        )
                    ).Resultado;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<KimberlyOsa> Lista(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyOsa>(
                    MvcApplication._Servicio_Operativa.KimberlyEvaluacion(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-16
    /// </summary>
    public class ResponseKimberlyOsa
    {
        [JsonProperty("_a")]
        public List<KimberlyOsa> Lista { get; set; }
    }
    #endregion

    #region Arriendo
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-19
    /// </summary>
    public class KimberlyArriendo : AKimberlyArriendo 
    {
        [JsonProperty("_a")]
        public int rpl_id { get; set; }

        [JsonProperty("_b")]
        public int per_id { get; set; }

        [JsonProperty("_c")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_d")]
        public int cat_id { get; set; }

        [JsonProperty("_e")]
        public string pop_codigo { get; set; }

        [JsonProperty("_f")]
        public string fecha { get; set; }

        [JsonProperty("_g")]
        public string si_no { get; set; }

        [JsonProperty("_h")]
        public int motivo { get; set; }

        [JsonProperty("_i")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_j")]
        public string pop_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-19
    /// </summary>
    public class AKimberlyArriendo
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-19
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public Int32 InsertaActualiza(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyResultado>(
                        MvcApplication._Servicio_Operativa.KimberlyEvaluacion(
                            MvcApplication._Serialize(parametro)
                        )
                    ).Resultado;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<KimberlyArriendo> Lista(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyArriendo>(
                    MvcApplication._Servicio_Operativa.KimberlyEvaluacion(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-09-16
    /// </summary>
    public class ResponseKimberlyArriendo
    {
        [JsonProperty("_a")]
        public List<KimberlyArriendo> Lista { get; set; }
    }
    #endregion

    #region Cuestionario
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-16
    /// </summary>
    public class KimberlyCuestionario : AKimberlyCuestionario 
    {
        [JsonProperty("_a")]
        public List<KimberlyCuestionarioRespuesta> Respuesta { get; set; }

        [JsonProperty("_b")]
        public List<KimberlyCuestionarioCategoria> Pregunta { get; set; }

        [JsonProperty("_c")]
        public List<KimberlyCuestionarioPoseidonElemento> lElemento { get; set; }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public override KimberlyCuestionario Cuestionario(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyCuestionario>(
                    MvcApplication._Servicio_Operativa.KimberlyEvaluacion(MvcApplication._Serialize(parametro))
                ).Cuestionario;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public override List<Listado> Filtro(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyListado>(
                    MvcApplication._Servicio_Operativa.KimberlyEvaluacion(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public override Int32 InsertaActualiza(Parametro parametro)
        {
            return MvcApplication._Deserialize<ResponseKimberlyResultado>(
                        MvcApplication._Servicio_Operativa.KimberlyEvaluacion(
                            MvcApplication._Serialize(parametro)
                        )
                    ).Resultado;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public override List<KimberlySeleccion> PoseidonSelecciona(Parametro p)
        {
            return MvcApplication._Deserialize<ResponseKimberlySeleccion>(
                    MvcApplication._Servicio_Operativa.KimberlyEvaluacionPoseidon(MvcApplication._Serialize(p))
                ).Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public override Int32 PoseidonInsertaActualiza(Parametro p)
        {
            return MvcApplication._Deserialize<ResponseKimberlyResultado>(
                        MvcApplication._Servicio_Operativa.KimberlyEvaluacionPoseidon(MvcApplication._Serialize(p))
                    ).Resultado;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public override KimberlyCuestionario PoseidonCuestionario(Parametro p)
        {
            return MvcApplication._Deserialize<ResponseKimberlyCuestionario>(
                    MvcApplication._Servicio_Operativa.KimberlyEvaluacionPoseidon(MvcApplication._Serialize(p))
                ).Cuestionario;
        }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-16
    /// </summary>
    public class KimberlyCuestionarioRespuesta
    {
        [JsonProperty("_a")]
        public int CprId { get; set; }

        [JsonProperty("_b")]
        public string CprRespuesta { get; set; }

        [JsonProperty("_c")]
        public string CprAlias { get; set; }

        [JsonProperty("_d")]
        public string CprGrupo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-16
    /// </summary>
    public class KimberlyCuestionarioCategoria
    {
        [JsonProperty("_a")]
        public int CatId { get; set; }

        [JsonProperty("_b")]
        public string CatDescripcion { get; set; }

        [JsonProperty("_c")]
        public List<KimberlyCuestionarioCategoriaGrupo> ListaGrupo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-16
    /// </summary>
    public class KimberlyCuestionarioCategoriaGrupo
    {
        [JsonProperty("_a")]
        public string CueGrupo { get; set; }

        [JsonProperty("_b")]
        public List<KimberlyCuestionarioCategoriaGrupoPregunta> ListaPregunta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-16
    /// </summary>
    public class KimberlyCuestionarioCategoriaGrupoPregunta
    {
        [JsonProperty("_a")]
        public int CueId { get; set; }

        [JsonProperty("_b")]
        public string CuePregunta { get; set; }

        [JsonProperty("_c")]
        public string CueRespuesta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-03-10
    /// </summary>
    public class KimberlyCuestionarioPoseidonElemento
    {
        [JsonProperty("_a")]
        public string PlaCodigo { get; set; }

        [JsonProperty("_b")]
        public int RepId { get; set; }

        [JsonProperty("_c")]
        public int TclId { get; set; }

        [JsonProperty("_d")]
        public int CueId { get; set; }

        [JsonProperty("_e")]
        public string EleCodigo { get; set; }

        [JsonProperty("_f")]
        public string EleDescripcion { get; set; }

        [JsonProperty("_g")]
        public string CueRespuesta { get; set; }

        [JsonProperty("_h")]
        public int OfiId { get; set; }

        [JsonProperty("_i")]
        public string Foto { get; set; }

        [JsonProperty("_j")]
        public string Texto { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-03-10
    /// </summary>
    public class KimberlySeleccion
    {
        [JsonProperty("_a")]
        public string Codigo { get; set; }

        [JsonProperty("_b")]
        public string Texto { get; set; }

        [JsonProperty("_c")]
        public int Seleccionado { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-16
    /// </summary>
    public abstract class AKimberlyCuestionario
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public abstract KimberlyCuestionario Cuestionario(Parametro parametro);

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public abstract List<Listado> Filtro(Parametro parametro);

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-16
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public abstract Int32 InsertaActualiza(Parametro parametro);

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public abstract List<KimberlySeleccion> PoseidonSelecciona(Parametro p);

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public abstract Int32 PoseidonInsertaActualiza(Parametro p);

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public abstract KimberlyCuestionario PoseidonCuestionario(Parametro p);
    }

    #region Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-01-16
    /// </summary>
    public class ResponseKimberlyCuestionario
    {
        [JsonProperty("_a")]
        public KimberlyCuestionario Cuestionario { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-03-10
    /// </summary>
    public class ResponseKimberlySeleccion
    {
        [JsonProperty("_a")]
        public List<KimberlySeleccion> Lista { get; set; }
    }
    #endregion
    #endregion
}