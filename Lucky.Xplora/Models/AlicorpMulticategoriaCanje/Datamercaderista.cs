﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.AlicorpMulticategoriaCanje
{
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2015-12-23
    /// </summary>
    public class Datamercaderista : ADatamercaderista
    {
        [JsonProperty("_a")]
        public string codrepcanjedet { get; set; }
        [JsonProperty("_b")]
        public string fechahorainicio { get; set; }
        [JsonProperty("_c")]
        public string fechahorafin { get; set; }
        [JsonProperty("_d")]
        public string codgie { get; set; }
        [JsonProperty("_e")]
        public string nomgie { get; set; }
        [JsonProperty("_f")]
        public string oficina { get; set; }
        [JsonProperty("_g")]
        public string mercado { get; set; }
        [JsonProperty("_h")]
        public string tienda { get; set; }
        [JsonProperty("_i")]
        public string otros { get; set; }
        [JsonProperty("_j")]
        public string codpdv { get; set; }
        [JsonProperty("_k")]
        public string dni { get; set; }
        [JsonProperty("_l")]
        public string negocio { get; set; }
        [JsonProperty("_m")]
        public string locacion { get; set; }
        [JsonProperty("_n")]
        public string nroboletafact { get; set; }
        [JsonProperty("_o")]
        public string campaña { get; set; }
        [JsonProperty("_p")]
        public string marca { get; set; }
        [JsonProperty("_q")]
        public string categoria { get; set; }
        [JsonProperty("_r")]
        public string presentacion { get; set; }
        [JsonProperty("_s")]
        public string producto { get; set; }
        [JsonProperty("_t")]
        public string cantidad { get; set; }
        [JsonProperty("_u")]
        public string monto { get; set; }
        [JsonProperty("_v")]
        public string premio { get; set; }
        [JsonProperty("_x")]
        public string foto { get; set; }
        [JsonProperty("_y")]
        public string validado { get; set; }
        [JsonProperty("_z")]
        public int codrepcanje { get; set; }
        [JsonProperty("_aa")]
        public string fecha_relevo { get; set; }

        [JsonProperty("_ab")]
        public string gramaje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-12-23
    /// </summary>
    public class Parametro
    {
        public Parametro() { }

        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-12-23
    /// </summary>
    public class Listado
    {
        public Listado() { }

        [JsonProperty("_a")]
        public string valor { get; set; }

        [JsonProperty("_b")]
        public string texto { get; set; }

        [JsonProperty("_c")]
        public bool seleccion { get; set; }

        [JsonProperty("_d")]
        public string id_categoria { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-12-23
    /// </summary>
    public abstract class ADatamercaderista {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-12-23
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Listado> Lista(Request_AlicorpMulticategoriaCanjeParametro oRq)
        {
            Response_AlicorpMulticategoriaCanjeListado oRp = MvcApplication._Deserialize<Response_AlicorpMulticategoriaCanjeListado>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeFiltro(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: curiarte
        /// Fecha: 2015-12-30
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Datamercaderista> Lista(Request_AlicorpMulticategoriaCanjeData oRq)
        {
            Response_AlicorpMulticategoriaCanjeData_Listado oRp = MvcApplication._Deserialize<Response_AlicorpMulticategoriaCanjeData_Listado>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanje(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Obj;
        }

        /// <summary>
        /// Autor: curiarte
        /// Fecha: 2015-12-30
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public int Actualiza(Request_AlicorpMulticategoriaCanjeData oRq)
        {
            Response_AlicorpMulticategoriaCanjeData_Actualiza oRp = MvcApplication._Deserialize<Response_AlicorpMulticategoriaCanjeData_Actualiza>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanje(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Obj;
        }

    }

    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2016-01-14
    /// </summary>
    public class StockCanje_PDV : AStockCanje_PDV
    {
        [JsonProperty("_a")]
        public int per_id { get; set; }
        [JsonProperty("_b")]
        public string pdv_codigo { get; set; }
        [JsonProperty("_c")]
        public string pdv_descripcion { get; set; }
        [JsonProperty("_d")]
        public List<StockCanje_Campania> campanias { get; set; }
        [JsonProperty("_e")]
        public int rpl_id { get; set; }
    }

    public abstract class AStockCanje_PDV
    {
        public List<StockCanje_PDV> lista(Request_AlicorpStockCanje oRq)
        {
            Response_AlicorpStockCanje oRp = MvcApplication._Deserialize<Response_AlicorpStockCanje>(
            MvcApplication._Servicio_Operativa.AlicorpStockCanje(
            MvcApplication._Serialize(oRq)));

            return oRp.Obj;
        }

        public int Actualiza(Request_AlicorpStockCanje oRq)
        {
            Response_AlicorpStockCanje_Update oRp = MvcApplication._Deserialize<Response_AlicorpStockCanje_Update>(
                    MvcApplication._Servicio_Operativa.AlicorpStockCanje(
                        MvcApplication._Serialize(oRq)));

            return oRp.res;
        }
    }
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2016-01-14
    /// </summary>
    public class StockCanje_Campania
    {
        [JsonProperty("_a")]
        public int cam_id { get; set; }
        [JsonProperty("_b")]
        public string cam_descripcion { get; set; }
        [JsonProperty("_c")]
        public List<StockCanje_Escala> escalas { get; set; }
    }
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2016-01-14
    /// </summary>
    public class StockCanje_Escala
    {
        [JsonProperty("_a")]
        public int esc_id { get; set; }
        [JsonProperty("_b")]
        public string esc_descripcion { get; set; }
        [JsonProperty("_c")]
        public int esc_stock { get; set; }
        [JsonProperty("_d")]
        public List<StockCanje_Premio> premios { get; set; }
    }
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2016-01-14
    /// </summary>
    public class StockCanje_Premio
    {
        [JsonProperty("_a")]
        public int pre_id { get; set; }
        [JsonProperty("_b")]
        public string pre_descripcion { get; set; }
        [JsonProperty("_c")]
        public int pre_stock { get; set; }
        [JsonProperty("_d")]
        public int pre_stock_actual { get; set; }
    }


    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-12-23
    /// </summary>
    public class Request_AlicorpMulticategoriaCanjeParametro
    {
        [JsonProperty("_a")]
        public Parametro Objeto { get; set; }
    }
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2016-01-14
    /// </summary>
    public class Request_AlicorpStockCanje
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int opcion { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int escala { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string codpdv { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int codperson { get; set; }
        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int codperiodo { get; set; }
        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public int codpremio { get; set; }
        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public int stockini { get; set; }
        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public int campania { get; set; }
        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public int stockhist { get; set; }
        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public int codsuperv { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-12-23
    /// </summary>
    public class Response_AlicorpMulticategoriaCanjeListado
    {
        [JsonProperty("_a")]
        public List<Listado> Lista { get; set; }
    }
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2015-12-28
    /// </summary>
    public class Request_AlicorpMulticategoriaCanjeData
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string campania { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int anio { get; set; }

        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public int mes { get; set; }

        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int periodo { get; set; }

        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int oficina { get; set; }

        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public int tipoperf { get; set; }

        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public int opcion { get; set; }

        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public int coddetalle { get; set; }

        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public int validado { get; set; }

        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public int dia { get; set; }

        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public int codgie { get; set; }

        [JsonProperty("_l", NullValueHandling = NullValueHandling.Ignore)]
        public string pdv { get; set; }
    }
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2015-12-28
    /// </summary>
    public class Response_AlicorpMulticategoriaCanjeData_Listado
    {
        [JsonProperty("_a")]
        public List<Datamercaderista> Obj { get; set; }
    }
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2015-12-28
    /// </summary>
    public class Response_AlicorpMulticategoriaCanjeData_Actualiza
    {
        [JsonProperty("_a")]
        public int Obj { get; set; }
    }

    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2016-01-13
    /// </summary>
    public class Response_AlicorpStockCanje
    {
        [JsonProperty("_a")]
        public List<StockCanje_PDV> Obj { get; set; }
    }
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2016-01-13
    /// </summary>
    public class Response_AlicorpStockCanje_Update
    {
        [JsonProperty("_a")]
        public int res { get; set; }
    }
    #endregion
}