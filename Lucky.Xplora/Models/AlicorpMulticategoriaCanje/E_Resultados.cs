﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.AlicorpMulticategoriaCanje
{
    public class E_Resultados
    {
        public E_Resultao_Canje_tb_1 GetGrilla1(Request_AlicorpCanjeResultadosObj request)
        {
            Response_AlicorpCanjeResultadosObj oRp = MvcApplication._Deserialize<Response_AlicorpCanjeResultadosObj>(MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeResultadoObj(MvcApplication._Serialize(request)));

            return oRp.Obj;
        }
        public E_Resultao_Canje GetGrilla2(Request_AlicorpCanjeResultadosObj request)
        {
            Response_AlicorpCanjeResultadosObj_g2 oRp = MvcApplication._Deserialize<Response_AlicorpCanjeResultadosObj_g2>(MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeResultadoObj_g2(MvcApplication._Serialize(request)));
            return oRp.Obj;
        }
        public int Update_Grilla2(Request_AlicorpCanjeResultadosObj request)
        {
            Response_AlicorpCanjeResultadosObj_g2_UDP oRp = MvcApplication._Deserialize<Response_AlicorpCanjeResultadosObj_g2_UDP>(MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeResultadoObj_g2_UDP(MvcApplication._Serialize(request)));
            return oRp.Obj;
        }
    }
    #region  Request - Response
    public class Response_AlicorpCanjeResultadosObj
    {
        [JsonProperty("_a")]
        public E_Resultao_Canje_tb_1 Obj { get; set; }
    }
    public class Request_AlicorpCanjeResultadosObj
    {
        [JsonProperty("_a")]
        public int Anio { get; set; }
        [JsonProperty("_b")]
        public string Periodo { get; set; }
        [JsonProperty("_c")]
        public int Codigo { get; set; }
        [JsonProperty("_d")]
        public int Op { get; set; }
        [JsonProperty("_e")]
        public string valores { get; set; }
        [JsonProperty("_f")]
        public string Observacion { get; set; }
    }
    public class Response_AlicorpCanjeResultadosObj_g2
    {
        [JsonProperty("_a")]
        public E_Resultao_Canje Obj { get; set; }
    }
    public class Response_AlicorpCanjeResultadosObj_g2_UDP 
    {
        [JsonProperty("_a")]
        public int Obj { get; set; }
    }
    #endregion
    #region Entidad
    public class E_Resultao_Canje_tb_1
    {
        [JsonProperty("_a")]
        public List<String> Revisiones { get; set; }
        [JsonProperty("_b")]
        public List<String> Canales { get; set; }
        [JsonProperty("_c")]
        public List<String> Categorias { get; set; }
        [JsonProperty("_d")]
        public List<E_Resultao_Canje_tb_1_fila> Filas { get; set; }
    }
    public class E_Resultao_Canje_tb_1_fila
    {
        [JsonProperty("_a")]
        public int Estado_g { get; set; }
        [JsonProperty("_b")]
        public string Region { get; set; }
        [JsonProperty("_c")]
        public string Jerarquia { get; set; }
        [JsonProperty("_d")]
        public List<E_Resultao_Canje_tb_1_revision> revision { get; set; }

    }
    public class E_Resultao_Canje_tb_1_revision
    {
        [JsonProperty("_a")]
        public string Fecha { get; set; }
        [JsonProperty("_b")]
        public List<M_Combo> categorias { get; set; }
        [JsonProperty("_c")]
        public string Observacion { get; set; }
    }
    public class E_Resultao_Canje_tb_2
    {
        [JsonProperty("_a")]
        public int id { get; set; }
        [JsonProperty("_b")]
        public string cod_region { get; set; }
        [JsonProperty("_c")]
        public string nom_region { get; set; }
        [JsonProperty("_d")]
        public int cod_oficina { get; set; }
        [JsonProperty("_e")]
        public string nom_oficina { get; set; }
        [JsonProperty("_f")]
        public string segmento { get; set; }
        [JsonProperty("_g")]
        public string cod_cliente { get; set; }
        [JsonProperty("_h")]
        public string nom_cliente { get; set; }
        [JsonProperty("_i")]
        public string Rep { get; set; }
        [JsonProperty("_j")]
        public string plan { get; set; }
        [JsonProperty("_k")]
        public string Apoyo_actual { get; set; }
        [JsonProperty("_l")]
        public List<M_Combo> Escalas_actual { get; set; }
        [JsonProperty("_m")]
        public string Apoyo_modificado { get; set; }
        [JsonProperty("_n")]
        public List<M_Combo> Escalas_modificadas { get; set; }
        [JsonProperty("_o")]
        public int Tipo { get; set; }
    }
    public class E_Resultao_Canje
    {
        [JsonProperty("_a")]
        public List<string> Escalas { get; set; }
        [JsonProperty("_b")]
        public List<E_Resultao_Canje_tb_2> Tabla { get; set; }
        [JsonProperty("_c")]
        public string Monto_destinado { get; set; }
        [JsonProperty("_d")]
        public List<E_Resultao_Canje_tb_2_montos_escala> montos_escala { get; set; }
        [JsonProperty("_e")]
        public string Observacion { get; set; }
    }
    public class E_Resultao_Canje_tb_2_montos_escala
    {
        [JsonProperty("_a")]
        public string monto { get; set; }
        [JsonProperty("_b")]
        public string Tg { get; set; }
        [JsonProperty("_c")]
        public string Bonificacion { get; set; }
        [JsonProperty("_d")]
        public string Peso { get; set; }
        [JsonProperty("_e")]
        public int nroescala { get; set; }
    }

    #endregion
}