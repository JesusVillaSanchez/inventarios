﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.AlicorpMulticategoriaCanje
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-12
    /// </summary>
    public class Reporting
    {
        [JsonProperty("_a")]
        public List<Reporting_Campania> rotacion { get; set; }

        [JsonProperty("_b")]
        public List<Reporting_Campania_Oficina> desglose { get; set; }

        [JsonProperty("_c")]
        public List<Reporting_Campania_Resumen> resumen { get; set; }

        [JsonProperty("_d")]
        public string actualizado { get; set; }

        [JsonProperty("_e")]
        public ReportingCampaniaCanje canje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-08
    /// </summary>
    public class Reporting_Campania : AReporting_Campania 
    {
        public Reporting_Campania() { }

        [JsonProperty("_a")]
        public int cam_id { get; set; }

        [JsonProperty("_b")]
        public string cam_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<Reporting_Campania_Mes> meses { get; set; }

        [JsonProperty("_d")]
        public List<Reporting_Campania_Mes> comparativo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-08
    /// </summary>
    public class Reporting_Campania_Mes
    {
        public Reporting_Campania_Mes() { }

        [JsonProperty("_a")]
        public string mes { get; set; }

        [JsonProperty("_b")]
        public string mes_pla_monto { get; set; }

        [JsonProperty("_c")]
        public string mes_rotacion_peso { get; set; }

        [JsonProperty("_d")]
        public string mes_obj_monto { get; set; }

        [JsonProperty("_e")]
        public string mes_rotacion_plan { get; set; }

        [JsonProperty("_f")]
        public string mes_rotacion_moneda { get; set; }

        [JsonProperty("_g")]
        public string mes_ratio { get; set; }

        [JsonProperty("_h")]
        public List<Reporting_Campania_Mes_Periodo> periodo { get; set; }

        [JsonProperty("_i")]
        public int mes_dia { get; set; }

        [JsonProperty("_j")]
        public string mes_fecha { get; set; }

        [JsonProperty("_k")]
        public int mes_dias_transcurridos { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-08
    /// </summary>
    public class Reporting_Campania_Mes_Periodo
    {
        public Reporting_Campania_Mes_Periodo() { }

        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string orden { get; set; }

        [JsonProperty("_c")]
        public string periodo_rotacion_peso { get; set; }

        [JsonProperty("_d")]
        public string periodo_rotacion_plan { get; set; }

        [JsonProperty("_e")]
        public string periodo_rotacion_moneda { get; set; }

        [JsonProperty("_f")]
        public string periodo_ratio { get; set; }

        [JsonProperty("_g")]
        public string fecha { get; set; }

        [JsonProperty("_h")]
        public int dias_periodo { get; set; }

        [JsonProperty("_i")]
        public int dias_transcurridos { get; set; }
    }

    #region Desglose
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-12
    /// </summary>
    public class Reporting_Campania_Oficina
    {
        public Reporting_Campania_Oficina() { }

        [JsonProperty("_a")]
        public int ofi_id { get; set; }

        [JsonProperty("_b")]
        public string ofi_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<ReportingCampaniaOficinaPdv> pdv { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-12
    /// </summary>
    public class ReportingCampaniaOficinaPdv
    {
        public ReportingCampaniaOficinaPdv() { }

        [JsonProperty("_a")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_b")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<ReportingCampaniaOficinaPdvCampania> campania { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-12
    /// </summary>
    public class ReportingCampaniaOficinaPdvCampania
    {
        public ReportingCampaniaOficinaPdvCampania() { }

        [JsonProperty("_a")]
        public int cam_id { get; set; }

        [JsonProperty("_b")]
        public string cam_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<ReportingCampaniaOficinaPdvCampaniaCategoria> categoria { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-12
    /// </summary>
    public class ReportingCampaniaOficinaPdvCampaniaCategoria
    {
        public ReportingCampaniaOficinaPdvCampaniaCategoria() { }

        [JsonProperty("_a")]
        public int cat_id { get; set; }

        [JsonProperty("_b")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string mes_pla_monto { get; set; }

        [JsonProperty("_d")]
        public string mes_rotacion_peso { get; set; }

        [JsonProperty("_e")]
        public string mes_rotacion_plan { get; set; }
    }
    ///// <summary>
    ///// Autor: jlucero
    ///// Fecha: 2016-01-12
    ///// </summary>
    //public class Reporting_Campania_Oficina
    //{
    //    public Reporting_Campania_Oficina() { }

    //    [JsonProperty("_a")]
    //    public int cam_id { get; set; }

    //    [JsonProperty("_b")]
    //    public string cam_descripcion { get; set; }

    //    [JsonProperty("_c")]
    //    public List<Reporting_Campania_Mes_Oficina> oficina { get; set; }
    //}

    ///// <summary>
    ///// Autor: jlucero
    ///// Fecha: 2016-01-12
    ///// </summary>
    //public class Reporting_Campania_Mes_Oficina
    //{
    //    public Reporting_Campania_Mes_Oficina() { }

    //    [JsonProperty("_a")]
    //    public int ofi_id { get; set; }

    //    [JsonProperty("_b")]
    //    public string ofi_descripcion { get; set; }

    //    [JsonProperty("_c")]
    //    public string mes_pla_monto { get; set; }

    //    [JsonProperty("_d")]
    //    public string mes_rotacion_peso { get; set; }

    //    [JsonProperty("_e")]
    //    public string mes_rotacion_plan { get; set; }
    //}
    #endregion

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-13
    /// </summary>
    public class Reporting_Campania_Resumen
    {
        [JsonProperty("_a")]
        public int ofi_id { get; set; }

        [JsonProperty("_b")]
        public string ofi_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<Reporting_Campania_Resumen_Cadena> cadena { get; set; }

        [JsonProperty("_d")]
        public List<Reporting_Campania_Resumen_Campaña> campania { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-13
    /// </summary>
    public class Reporting_Campania_Resumen_Cadena
    {
        [JsonProperty("_a")]
        public int cad_id { get; set; }

        [JsonProperty("_b")]
        public string cad_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<Reporting_Campania_Resumen_Pdv> pdv { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-13
    /// </summary>
    public class Reporting_Campania_Resumen_Pdv
    {
        [JsonProperty("_a")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_b")]
        public int per_id { get; set; }

        [JsonProperty("_c")]
        public string per_nombre { get; set; }

        [JsonProperty("_d")]
        public int trabajo_hora { get; set; }

        [JsonProperty("_e")]
        public decimal trabajo_promedio { get; set; }

        [JsonProperty("_f")]
        public int canje { get; set; }

        [JsonProperty("_g")]
        public string tonelada { get; set; }

        [JsonProperty("_h")]
        public decimal promedio_canje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-19
    /// </summary>
    public class Reporting_Campania_Resumen_Campaña
    {
        public Reporting_Campania_Resumen_Campaña() { }

        [JsonProperty("_a")]
        public int cam_id { get; set; }

        [JsonProperty("_b")]
        public string cam_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<Reporting_Campania_Resumen_Escala> escala { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-19
    /// </summary>
    public class Reporting_Campania_Resumen_Escala
    {
        public Reporting_Campania_Resumen_Escala() { }

        [JsonProperty("_a")]
        public int esc_id { get; set; }

        [JsonProperty("_b")]
        public string esc_descripcion { get; set; }

        [JsonProperty("_c")]
        public string stock { get; set; }

        [JsonProperty("_d")]
        public string canje { get; set; }

        [JsonProperty("_e")]
        public string stock_final { get; set; }

        [JsonProperty("_f")]
        public string dia { get; set; }

        [JsonProperty("_g")]
        public string promedio_canje { get; set; }
    }

    #region Canje
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-04-26
    /// </summary>
    public class ReportingCampaniaCanje
    {
        [JsonProperty("_a")]
        public List<ReportingCampaniaCanjeCabecera> oficina { get; set; }

        [JsonProperty("_b")]
        public List<ReportingCampaniaCanjeCabecera> gestor { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-04-26
    /// </summary>
    public class ReportingCampaniaCanjeCabecera
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public List<ReportingCampaniaCanjeDetalle> detalle { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-04-26
    /// </summary>
    public class ReportingCampaniaCanjeDetalle
    {
        [JsonProperty("_a")]
        public string fecha { get; set; }

        [JsonProperty("_b")]
        public string gramaje { get; set; }

        [JsonProperty("_c")]
        public string canje { get; set; }
    }
    #endregion

    #region Canje workflow

    public class Reporte_Response_Alicorp_Canje
    {
        [JsonProperty("_a")]
        public string reporte { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public int cantidad { get; set; }
    }

    #region WorkFlowPeriodo
    /// <summary>
    /// Autor: amamani
    /// Fecha: 2017-01-17
    /// </summary>
    public class AlicorpCanjeParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }

    public class AlicorpCanjePeriodo : AAlicorpCanje
    {
        [JsonProperty("_a")]
        public string rpl_id { get; set; }
    }

    public class AlicorpCanjeListaPeriodo : AAlicorpCanje
{
    [JsonProperty("_a")]
    public int item { get; set; }

    [JsonProperty("_b")]
    public string rpl_id { get; set; }

    [JsonProperty("_c")]
    public string descripcion_periodo { get; set; }

    [JsonProperty("_d")]
    public int mes_inicial { get; set; }

    [JsonProperty("_e")]
    public int mes_cierre { get; set; }

    [JsonProperty("_f")]
    public decimal presupuesto { get; set; }

    [JsonProperty("_g")]
    public string fecha_inicial { get; set; }

    [JsonProperty("_h")]
    public string fecha_cierre { get; set; }

    [JsonProperty("_i")]
    public string observacion { get; set; }

    [JsonProperty("_j")]
    public List<AlicorpCanjeDetallePeriodo> lDetallePeriodo { set; get; }

    [JsonProperty("_k")]
    public List<AlicorpCanjeDetallePeriodoCategoria> lDetallePeriodoCategoria { set; get; }


}

    public class AlicorpCanjeDetallePeriodo
{
    [JsonProperty("_a")]
    public int id_canal { get; set; }

    [JsonProperty("_b")]
    public int variedad { get; set; }

    [JsonProperty("_c")]
    public int familia { get; set; }

    [JsonProperty("_d")]
    public int total_premio { get; set; }

    [JsonProperty("_e")]
    public int total_cliente { get; set; }
    
    [JsonProperty("_f")]
    public decimal plan_rpl_id { get; set; }

    [JsonProperty("_g")]
    public decimal monto_asignado { get; set; }

}

    public class AlicorpCanjeDetallePeriodoCategoria
{
    [JsonProperty("_a")]
    public string rpl_id { get; set; }

    [JsonProperty("_b")]
    public int id_categoria { get; set; }

    [JsonProperty("_c")]
    public int id_canal { get; set; }

    [JsonProperty("_d")]
    public string descripcion_canal { get; set; }

    [JsonProperty("_e")]
    public int variedad { get; set; }

    [JsonProperty("_f")]
    public int familia { get; set; }

    [JsonProperty("_g")]
    public int total_premio { get; set; }

    [JsonProperty("_h")]
    public int total_cliente { get; set; }

    [JsonProperty("_i")]
    public decimal plan_rpl_id { get; set; }

    [JsonProperty("_j")]
    public decimal monto_asignado { get; set; }

    [JsonProperty("_k")]
    public string porcentaje { get; set; }

}

    public abstract class AAlicorpCanje
    {
        /// <summary>
        /// Autor:amamani
        /// Fecha: 2017-01-17
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public string InsertPeriodo(AlicorpCanjeParametro parametro)
        {
            return MvcApplication._Deserialize<Response_AlicorpCanjePeriodo>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjePeriodoReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Respuesta;
        }

        public List<AlicorpCanjeListaPeriodo> ListaPeriodo(AlicorpCanjeParametro parametro)
        {
            return MvcApplication._Deserialize<Response_AlicorpCanjeListaPeriodo>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjePeriodoReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }
    }

    public class Response_AlicorpCanjePeriodo
    {
        [JsonProperty("_a")]
        public string Respuesta { get; set; }
    }

    public class Response_AlicorpCanjeListaPeriodo
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjeListaPeriodo> Lista { get; set; }
    }

    #endregion

    #region WorkFlowMecanica

    public class Reporte_Mecanica_Escala_Alicorp_Canje : AReporte_Mecanica_Escala_Alicorp_Canje
    {
        [JsonProperty("_a")]
        public string descripcion_canal { get; set; }

        [JsonProperty("_b")]
        public string descripcion_region { get; set; }

        [JsonProperty("_c")]
        public string descripcion_categoria { get; set; }

        [JsonProperty("_d")]
        public decimal nro_escala { get; set; }

        [JsonProperty("_e")]
        public decimal escala { get; set; }

        [JsonProperty("_f")]
        public string premio { get; set; }

        [JsonProperty("_g")]
        public int cantidad { get; set; }

        [JsonProperty("_h")]
        public string uni_med { get; set; }

        [JsonProperty("_i")]
        public decimal precio { get; set; }

        [JsonProperty("_j")]
        public decimal tg { get; set; }

        [JsonProperty("_k")]
        public int id_canal { get; set; }

        [JsonProperty("_l")]
        public int id_region { get; set; }

        [JsonProperty("_m")]
        public int id_categoria { get; set; }

        [JsonProperty("_n")]
        public string cod_premio { get; set; }

        [JsonProperty("_o")]
        public decimal bonificacion { get; set; }

        [JsonProperty("_p")]
        public decimal peso { get; set; }

        [JsonProperty("_q")]
        public decimal presupuesto_region { get; set; }

    }

    public class AlicorpCanjeMecanica : AAlicorpCanjePremio
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjeListaPremio> lPremio { set; get; }

        [JsonProperty("_b")]
        public List<AlicorpCanjePeriodoPremio> lPeriodoPremio { set; get; }
    }

    public class AlicorpCanjeListaPremio : AReporte_Premio_Alicorp_Canje
    {
        [JsonProperty("_a")]
        public string cod_premio { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public string tipo { get; set; }

        [JsonProperty("_d")]
        public string uni_med { get; set; }

        [JsonProperty("_e")]
        public int uni_com { get; set; }

        [JsonProperty("_f")]
        public decimal tonelada { get; set; }

        [JsonProperty("_g")]
        public decimal precio_costo { get; set; }

        [JsonProperty("_h")]
        public decimal precio_percibido { get; set; }

    }

    public class AlicorpCanjePeriodoPremio
    {
        [JsonProperty("_a")]
        public string rpl_id { get; set; }

        [JsonProperty("_b")]
        public string cod_premio { get; set; }

        [JsonProperty("_c")]
        public string descripcion { get; set; }

        [JsonProperty("_d")]
        public string tipo { get; set; }

        [JsonProperty("_e")]
        public string uni_med { get; set; }

        [JsonProperty("_f")]
        public int uni_com { get; set; }

        [JsonProperty("_g")]
        public decimal precio_costo { get; set; }

        [JsonProperty("_h")]
        public decimal precio_percibido { get; set; }

        [JsonProperty("_i")]
        public string observacion { get; set; }

        [JsonProperty("_j")]
        public int estado { get; set; }

    }

    public abstract class AAlicorpCanjePremio
        {
            /// <summary>
            /// Autor:amamani
            /// Fecha: 2017-01-17
            /// </summary>
            /// <param name="parametro"></param>
            /// <returns></returns>
            public List<AlicorpCanjeMecanica> ListaPremio(AlicorpCanjeParametro parametro)
            {
                return MvcApplication._Deserialize<Response_AlicorpCanjeMecanica>(
                        MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeMecanicaReporting(
                            MvcApplication._Serialize(parametro)
                        )
                    ).Lista;
            }
        }

    public class Response_AlicorpCanjeMecanica
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjeMecanica> Lista { get; set; }
    }

    /**********************************************/

    public class AlicorpCanjeMecanicaFiltro : AAlicorpCanjeMecanicaFiltro
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjeCanal> lCanal { set; get; }

        [JsonProperty("_b")]
        public List<AlicorpCanjeCategoria> lCategoria { set; get; }

        [JsonProperty("_c")]
        public List<AlicorpCanjeRegion> lRegion { set; get; }
    }

    public class AlicorpCanjeCanal
    {
        [JsonProperty("_a")]
        public int id_canal { get; set; }

        [JsonProperty("_b")]
        public string descripcion_canal { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpCanjeRegion> lRegion { get; set; }

        [JsonProperty("_d")]
        public List<AlicorpCanjeCategoria> lCategoria { get; set; }
    }

    public class AlicorpCanjeRegion
    {
        [JsonProperty("_a")]
        public int id_region { get; set; }

        [JsonProperty("_b")]
        public string descripcion_region { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpCanjeCategoria> lCategoria { get; set; }
    }

    public class AlicorpCanjeCategoria
    {
        [JsonProperty("_a")]
        public int id_categoria { get; set; }

        [JsonProperty("_b")]
        public string descripcion_categoria { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpCanjeMecanicaDetalleLista> lDetalleLista { get; set; }

        [JsonProperty("_d")]
        public List<AlicorpCanjePremio> lPremio { get; set; }

        [JsonProperty("_e")]
        public List<AlicorpCanjePlanDetalle> lDetallePlan { get; set; }

    }

    public class AlicorpCanjePremio
    {
        [JsonProperty("_a")]
        public string cod_premio { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpCanjeMecanicaEscala> lEscala { get; set; }
    }

    public class AlicorpCanjeMecanicaEscala
    {
        [JsonProperty("_a")]
        public int nro_escala { get; set; }

        [JsonProperty("_b")]
        public decimal monto_escala { get; set; }

        [JsonProperty("_c")]
        public int cant_product { get; set; }

        [JsonProperty("_d")]
        public decimal precio { get; set; }

        [JsonProperty("_e")]
        public decimal total_tg { get; set; }

        [JsonProperty("_f")]
        public decimal bonificacion { get; set; }

        [JsonProperty("_g")]
        public decimal peso { get; set; }

        [JsonProperty("_h")]
        public decimal presupuesto { get; set; }
    }

    public class AlicorpCanjeMecanicaDetalleLista
    {
        [JsonProperty("_a")]
        public int escala { get; set; }

        [JsonProperty("_b")]
        public int premio { get; set; }

        [JsonProperty("_c")]
        public decimal total_tg { get; set; }

        [JsonProperty("_d")]
        public decimal total_tg_canal_cat { get; set; }

        [JsonProperty("_e")]
        public int totales_escalas { get; set; }

        [JsonProperty("_f")]
        public int totales_premios { get; set; }

        [JsonProperty("_g")]
        public decimal totales_tgs { get; set; }
    }

    public class AlicorpCanjePlanDetalle
    {
        [JsonProperty("_a")]
        public int total_cliente { get; set; }

        [JsonProperty("_b")]
        public decimal plan_soles { get; set; }

        [JsonProperty("_c")]
        public decimal real_soles { get; set; }

        [JsonProperty("_d")]
        public decimal variedad_soles { get; set; }

        [JsonProperty("_e")]
        public decimal plan_tonelada { get; set; }

        [JsonProperty("_f")]
        public decimal real_tonelada { get; set; }

        [JsonProperty("_g")]
        public decimal variedad_tonelada { get; set; }

    }

    public class AlicorpCanjePlanFiltro
    {
        [JsonProperty("_a")]
        public string rpl_id { get; set; }

        [JsonProperty("_b")]
        public int mes { get; set; }

        [JsonProperty("_c")]
        public int anio { get; set; }

        [JsonProperty("_d")]
        public int posicion { get; set; }
    }

    public class AlicorpCanjeMecanicaDetalleEscala : AAlicorpCanjeMecanicaFiltro
    {
        [JsonProperty("_a")]
        public int id_canal { get; set; }

        [JsonProperty("_b")]
        public string descripcion_canal { get; set; }

        [JsonProperty("_c")]
        public int id_region { get; set; }

        [JsonProperty("_d")]
        public string descripcion_region { get; set; }

        [JsonProperty("_e")]
        public int id_categoria { get; set; }

        [JsonProperty("_f")]
        public string descripcion_categoria { get; set; }

        [JsonProperty("_g")]
        public int nro_escala { get; set; }

        [JsonProperty("_h")]
        public decimal escala { get; set; }

        [JsonProperty("_i")]
        public string tipo { get; set; }

        [JsonProperty("_j")]
        public string cod_premio { get; set; }

        [JsonProperty("_k")]
        public string descripcion_premio { get; set; }

        [JsonProperty("_l")]
        public string uni_med { get; set; }

        [JsonProperty("_m")]
        public int uni_com { get; set; }

        [JsonProperty("_n")]
        public decimal precio_costo { get; set; }

        [JsonProperty("_o")]
        public decimal precio_percibido { get; set; }

        [JsonProperty("_p")]
        public decimal peso { get; set; }

        [JsonProperty("_q")]
        public decimal total_tg { get; set; }

        [JsonProperty("_r")]
        public decimal bonificacion { get; set; }

    }

    public abstract class AAlicorpCanjeMecanicaFiltro
    {
        /// <summary>
        /// Autor:amamani
        /// Fecha: 2017-01-17
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<AlicorpCanjeMecanicaFiltro> ListaFiltro(AlicorpCanjeParametro parametro)
        {
            return MvcApplication._Deserialize<Response_AlicorpCanjeMecanicaFiltro>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeMecanicaReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }

        public List<AlicorpCanjeMecanicaFiltro> ListaDetallePlan(AlicorpCanjeParametro parametro)
        {
            return MvcApplication._Deserialize<Response_AlicorpCanjeMecanicaFiltro>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjePlanReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }

        public List<AlicorpCanjeMecanicaDetalleEscala> ListaEscalas(AlicorpCanjeParametro parametro)
        {
            return MvcApplication._Deserialize<Response_AlicorpCanjeMecanicaDetalleEscala>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeMecanicaReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }

        public List<AlicorpCanjePlanFiltro> ListaPeriodoFiltro(AlicorpCanjeParametro parametro)
        {
            return MvcApplication._Deserialize<Response_AlicorpCanjePlanFiltro>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjePlanReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }

    }

    public abstract class AReporte_Premio_Alicorp_Canje
    {
        /// <summary>
        /// Autor: amamai
        /// Fecha: 2017-03-07
        /// </summary>
        /// <param name="_oLs">Lista de registros</param>
        /// <returns></returns>
        /// 
        public List<Reporte_Response_Alicorp_Canje> BulkCopy_Premio_Alicorp_Canje(List<AlicorpCanjeListaPremio> _oLs, int __a, string __b)
        {
            var oRp = new Response_Reporte_Informe();

            oRp = MvcApplication._Deserialize<Response_Reporte_Informe>(
                MvcApplication._Servicio_Operativa.BulkCopy_Reporte_Premio_Alicorp_Canje(
                    MvcApplication._Serialize(
                        new Request_Reporte_Premio_Alicorp_Canje()
                        {
                            Lista = _oLs,
                            __a = __a,
                            __b = __b
                        }
                    )
                )
            );

            return oRp.Lista;
        }
    }

    public abstract class AReporte_Mecanica_Escala_Alicorp_Canje
    {
        /// <summary>
        /// Autor: amamai
        /// Fecha: 2017-03-07
        /// </summary>
        /// <param name="_oLs">Lista de registros</param>
        /// <returns></returns>
        /// 
        public List<Reporte_Response_Alicorp_Canje> BulkCopy_Mecanica_Escala_Alicorp_Canje(List<Reporte_Mecanica_Escala_Alicorp_Canje> _oLs, int __a, string __b)
        {
            var oRp = new Response_Reporte_Informe();

            oRp = MvcApplication._Deserialize<Response_Reporte_Informe>(
                MvcApplication._Servicio_Operativa.BulkCopy_Reporte_Mecanica_Escala_Alicorp_Canje(
                    MvcApplication._Serialize(
                        new Request_Reporte_Mecanica_Escala_Alicorp_Canje()
                        {
                            Lista = _oLs,
                            __a = __a,
                            __b = __b
                        }
                    )
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: amamani
    /// Fecha: 2017-03-07
    /// </summary>
    /// 
    public class Request_Reporte_Premio_Alicorp_Canje
    {
        [JsonProperty("a")]
        public List<AlicorpCanjeListaPremio> Lista { get; set; }

        [JsonProperty("b")]
        public int __a{ get; set; }

        [JsonProperty("c")]
        public string __b { get; set; }
    }

    /// <summary>
    /// Autor: amamani
    /// Fecha: 2017-03-08
    /// </summary>
    /// 
    public class Request_Reporte_Mecanica_Escala_Alicorp_Canje
    {
        [JsonProperty("a")]
        public List<Reporte_Mecanica_Escala_Alicorp_Canje> Lista { get; set; }

        [JsonProperty("b")]
        public int __a { get; set; }

        [JsonProperty("c")]
        public string __b { get; set; }
    }

    #endregion

    public class Response_AlicorpCanjeMecanicaFiltro
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjeMecanicaFiltro> Lista { get; set; }
    }

    public class Response_AlicorpCanjeMecanicaDetalleEscala
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjeMecanicaDetalleEscala> Lista { get; set; }
    }

    public class Response_AlicorpCanjePlanFiltro
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjePlanFiltro> Lista { get; set; }
    }

    

    #endregion

    #region WorkFlowPanel

    public class Reporte_Plan_Alicorp_Canje : AReporte_Plan_Alicorp_Canje
    {
        [JsonProperty("_a")]
        public int id_canal { get; set; }

        [JsonProperty("_b")]
        public int id_region { get; set; }

        [JsonProperty("_c")]
        public int id_oficina { get; set; }

        [JsonProperty("_d")]
        public string cod_cliente { get; set; }

        [JsonProperty("_e")]
        public int id_categoria { get; set; }

        [JsonProperty("_f")]
        public decimal plan_soles { get; set; }

        [JsonProperty("_g")]
        public decimal real_soles { get; set; }

        [JsonProperty("_h")]
        public decimal plan_tonelada { get; set; }

        [JsonProperty("_i")]
        public decimal real_tonelada { get; set; }

    }

    public class AlicorpCanjePanelCliente : AAlicorpCanjeCliente
    {
        [JsonProperty("_a")]
        public string cod_principal { get; set; }

        [JsonProperty("_b")]
        public string cod_cliente { get; set; }

        [JsonProperty("_c")]
        public string nombre { get; set; }

        [JsonProperty("_d")]
        public string segmentacion { get; set; }
    }


    public class AlicorpCanjePanelListaCliente : AAlicorpCanjeCliente
    {
        [JsonProperty("_a")]
        public string segmentacion { get; set; }

        [JsonProperty("_b")]
        public int id_region { get; set; }

        [JsonProperty("_c")]
        public string descripcion_region { get; set; }

        [JsonProperty("_d")]
        public int id_oficina { get; set; }

        [JsonProperty("_e")]
        public string descripcion_oficina { get; set; }

        [JsonProperty("_f")]
        public string cod_principal { get; set; }

        [JsonProperty("_g")]
        public string nombre { get; set; }

        [JsonProperty("_h")]
        public List<AlicorpCanjePanelDetalleCliente> lDetalleCliente { get; set; }

    }

    public class AlicorpCanjePanelDetalleCliente
    {
        [JsonProperty("_a")]
        public string rpl_id { get; set; }

        [JsonProperty("_b")]
        public int cantidad_region { get; set; }

        [JsonProperty("_c")]
        public int cantidad_oficina { get; set; }

        [JsonProperty("_d")]
        public int cantidad_cliente { get; set; }

        [JsonProperty("_e")]
        public decimal plan_soles { get; set; }

        [JsonProperty("_f")]
        public decimal real_soles { get; set; }

        [JsonProperty("_g")]
        public decimal plan_tonelada { get; set; }

        [JsonProperty("_h")]
        public decimal real_tonelada { get; set; }

        [JsonProperty("_i")]
        public int presencia { get; set; }

        [JsonProperty("_j")]
        public decimal plan_total { get; set; }

        [JsonProperty("_k")]
        public decimal real_total { get; set; }

    }

    public abstract class AAlicorpCanjeCliente
    {
        /// <summary>
        /// Autor:amamani
        /// Fecha: 2017-01-17
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        public List<AlicorpCanjePanelCliente> ListaCliente(AlicorpCanjeParametro parametro)
        {
            return MvcApplication._Deserialize<Response_AlicorpCanjePanelCliente>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjePanelReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }


        public List<AlicorpCanjePanelListaCliente> ListaPanelCliente(AlicorpCanjeParametro parametro)
        {
            return MvcApplication._Deserialize<Response_AlicorpCanjePanelListaCliente>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjePanelReporting(
                        MvcApplication._Serialize(parametro)
                    )
                ).Lista;
        }

    }

    public abstract class AReporte_Plan_Alicorp_Canje
    {
        /// <summary>
        /// Autor: amamai
        /// Fecha: 2017-03-07
        /// </summary>
        /// <param name="_oLs">Lista de registros</param>
        /// <returns></returns>
        /// 
        public List<Reporte_Response_Alicorp_Canje> BulkCopy_Plan_Alicorp_Canje(List<Reporte_Plan_Alicorp_Canje> _oLs, int __a, string __b)
        {
            var oRp = new Response_Reporte_Informe();

            oRp = MvcApplication._Deserialize<Response_Reporte_Informe>(
                MvcApplication._Servicio_Operativa.BulkCopy_Reporte_Plan_Alicorp_Canje(
                    MvcApplication._Serialize(
                        new Request_Reporte_Plan_Alicorp_Canje()
                        {
                            Lista = _oLs,
                            __a = __a,
                            __b = __b
                        }
                    )
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: amamani
    /// Fecha: 2017-06-23
    /// </summary>
    /// 
    public class Request_Reporte_Plan_Alicorp_Canje
    {
        [JsonProperty("a")]
        public List<Reporte_Plan_Alicorp_Canje> Lista { get; set; }

        [JsonProperty("b")]
        public int __a { get; set; }

        [JsonProperty("c")]
        public string __b { get; set; }
    }

    #endregion


    public class Response_AlicorpCanjePanelCliente
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjePanelCliente> Lista { get; set; }
    }

    public class Response_AlicorpCanjePanelListaCliente
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjePanelListaCliente> Lista { get; set; }
    }

    #endregion

    public class Response_Reporte_Informe
    {
        [JsonProperty("a")]
        public List<Reporte_Response_Alicorp_Canje> Lista { get; set; }
    }

    #region WorkFlowDistribucion
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-27
    /// </summary>
    public class AlicorpCanjeSelecciona
    {
        [JsonProperty("_a")]
        public string SelCodigo { get; set; }

        [JsonProperty("_b")]
        public string SelDescripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-24
    /// </summary>
    public class AlicorpCanjeDistribucion
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjeDistribucionEnvio> lEnvio { get; set; }

        [JsonProperty("_b")]
        public List<AlicorpCanjeDistribucionRegion> lRegion { get; set; }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-27
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<AlicorpCanjeSelecciona> Selecciona(AlicorpCanjeParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpMulticategoriaCanjeSelecciona>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeDistribucion(MvcApplication._Serialize(p))
                ).Selecciona;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-27
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public AlicorpCanjeDistribucion Distribucion(AlicorpCanjeParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpMulticategoriaCanjeDistribucion>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeDistribucion(MvcApplication._Serialize(p))
                ).Distribucion;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-01
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public int InsertaEnvio(AlicorpCanjeParametro p)
        {
            return MvcApplication._Deserialize<ResponseAlicorpMulticategoriaCanjeResultado>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeDistribucion(MvcApplication._Serialize(p))
                ).Resultado;
        }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-24
    /// </summary>
    public class AlicorpCanjeDistribucionEnvio
    {
        [JsonProperty("_a")]
        public string RplCodigo { get; set; }

        [JsonProperty("_b")]
        public int PenId { get; set; }

        [JsonProperty("_c")]
        public string PenFecha { get; set; }

        [JsonProperty("_d")]
        public string PenPorcentaje { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-24
    /// </summary>
    public class AlicorpCanjeDistribucionRegion
    {
        [JsonProperty("_a")]
        public string RplCodigo { get; set; }

        [JsonProperty("_b")]
        public int RegId { get; set; }

        [JsonProperty("_c")]
        public string RegDescripcion { get; set; }

        [JsonProperty("_d")]
        public List<AlicorpCanjeDistribucionRegionOficina> lOficina { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-24
    /// </summary>
    public class AlicorpCanjeDistribucionRegionOficina
    {
        [JsonProperty("_a")]
        public int OfiId { get; set; }

        [JsonProperty("_b")]
        public string OfiDescripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpCanjeDistribucionRegionOficinaPdv> lPdv { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-24
    /// </summary>
    public class AlicorpCanjeDistribucionRegionOficinaPdv
    {
        [JsonProperty("_a")]
        public string PdvCodigo { get; set; }

        [JsonProperty("_b")]
        public string PdvDescripcion { get; set; }

        [JsonProperty("_c")]
        public int ResId { get; set; }

        [JsonProperty("_d")]
        public string Envio1 { get; set; }

        [JsonProperty("_e")]
        public string Envio2 { get; set; }

        [JsonProperty("_f")]
        public List<AlicorpCanjeDistribucionRegionOficinaPdvCanal> lCanal { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-26
    /// </summary>
    public class AlicorpCanjeDistribucionRegionOficinaPdvCanal
    {
        [JsonProperty("_a")]
        public int CanId { get; set; }

        [JsonProperty("_b")]
        public string CanDescripcion { get; set; }

        [JsonProperty("_c")]
        public List<AlicorpCanjeDistribucionRegionOficinaPdvCanalPremio> lPremio { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-26
    /// </summary>
    public class AlicorpCanjeDistribucionRegionOficinaPdvCanalPremio
    {
        [JsonProperty("_a")]
        public string PreCodigo { get; set; }

        [JsonProperty("_b")]
        public string PreDescripcion { get; set; }

        [JsonProperty("_c")]
        public string Valor { get; set; }
    }

    #region Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2017-02-27
    /// </summary>
    public class ResponseAlicorpMulticategoriaCanjeDistribucion
    {
        [JsonProperty("_a")]
        public AlicorpCanjeDistribucion Distribucion { get; set; }
    }
    public class ResponseAlicorpMulticategoriaCanjeSelecciona
    {
        [JsonProperty("_a")]
        public List<AlicorpCanjeSelecciona> Selecciona { get; set; }
    }
    public class ResponseAlicorpMulticategoriaCanjeResultado
    {
        [JsonProperty("_a")]
        public int Resultado { get; set; }
    }
    #endregion
    #endregion
    #endregion

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-01-11
    /// </summary>
    public abstract class AReporting_Campania
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-01-11
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Reporting Objeto(Request_AlicorpMulticategoriaCanjeParametro oRq)
        {
            Response_AlicorpMulticategoriaCanjeReporting oRp = MvcApplication._Deserialize<Response_AlicorpMulticategoriaCanjeReporting>(
                    MvcApplication._Servicio_Operativa.AlicorpMulticategoriaCanjeReporting(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-12-23
    /// </summary>
    public class Response_AlicorpMulticategoriaCanjeReporting
    {
        [JsonProperty("_a")]
        public Reporting Objeto { get; set; }
    }
    #endregion
}