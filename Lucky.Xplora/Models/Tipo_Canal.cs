﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Tipo_Canal : ATipo_Canal
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int tca_id { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string tca_descripcion { get; set; }

        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string cargo_director { get; set; }

        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public string cargo_gerente { get; set; }
    }

    public abstract class ATipo_Canal
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-06
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Tipo_Canal> Lista(Request_GetTipoCanal_Campania oRq)
        {
            Response_GetTipoCanal_Campania oRp;

            oRp = MvcApplication._Deserialize<Response_GetTipoCanal_Campania>(
                    MvcApplication._Servicio_Campania.GetTipoCanal_Campania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        public List<Tipo_Canal> ListaTipoCanal(Request_GetTipoCanalBackusPlanning_Campania_Anio_Mes oRq)
        {
            Response_GetTipoCanalBackusPlanning_Campania_Anio_Mes oRp;

            oRp = MvcApplication._Deserialize<Response_GetTipoCanalBackusPlanning_Campania_Anio_Mes>(
                    MvcApplication._Servicio_Campania.GetTipoCanalBackusPlanning_Campania_Anio_Mes(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.oLista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-03-06
    /// </summary>
    public class Request_GetTipoCanal_Campania
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

    }
    public class Response_GetTipoCanal_Campania
    {
        [JsonProperty("a")]
        public List<Tipo_Canal> Lista { get; set; }
    }

    public class Request_GetTipoCanalBackusPlanning_Campania_Anio_Mes
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int anio { get; set; }

        [JsonProperty("_c")]
        public int mes { get; set; }

    }
    public class Response_GetTipoCanalBackusPlanning_Campania_Anio_Mes
    {
        [JsonProperty("_a")]
        public List<Tipo_Canal> oLista { get; set; }
    }

    #endregion
}