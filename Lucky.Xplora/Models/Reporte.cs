﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;


namespace Lucky.Xplora.Models
{
    public class Reporte : AReporte
    {
        [JsonProperty("a")]
        public int rep_id { get; set; }

        [JsonProperty("b")]
        public string rep_descripcion { get; set; }

    }

    public abstract class AReporte
    {
        /// <summary>
        /// Autor: curiarte
        /// Fecha: 2015-07-10
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Reporte> Lista(Request_GetReporte_Campania oRq)
        {
            Response_GetReporte_Campania orp = MvcApplication._Deserialize<Response_GetReporte_Campania>(
                        MvcApplication._Servicio_Campania.GetReporte_Campania(
                            MvcApplication._Serialize(oRq)
                        )
                    );

            return orp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: curiarte
    /// Fecha: 10-07-2015
    /// </summary>
    public class Request_GetReporte_Campania
    {
        [JsonProperty("_a")]
        public string campania { get; set; }
    }
    public class Response_GetReporte_Campania
    {
        [JsonProperty("_a")]
        public List<Reporte> Lista { get; set; }
    }

    #endregion

}