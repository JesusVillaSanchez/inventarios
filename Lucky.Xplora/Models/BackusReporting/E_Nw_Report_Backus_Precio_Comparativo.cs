﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models.Reporting;

namespace Lucky.Xplora.Models.BackusReporting
{
    public class E_Nw_Report_Backus_Precio_Comparativo : AE_Nw_Report_Backus_Precio_Comparativo
    {
        [JsonProperty("a")]
        public int Cod_Grafico { get; set; }
        [JsonProperty("b")]
        public int Cod_Orden { get; set; }
        [JsonProperty("c")]
        public int Cod_Periodo { get; set; }
        [JsonProperty("d")]
        public string Nom_Leyenda { get; set; }
        [JsonProperty("e")]
        public string Nom_Serie { get; set; }
        [JsonProperty("f")]
        public string Data { get; set; }
    }


    public class Response_Nw_Reporting_Graf_Comp_Precio_Backus 
    {
        [JsonProperty("a")]
        public List<E_Nw_Report_Backus_Precio_Comparativo> Response { get; set; }
    }
    public abstract class AE_Nw_Report_Backus_Precio_Comparativo
    {
        public List<E_Nw_Report_Backus_Precio_Comparativo> Consul_Grafico(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Graf_Comp_Precio_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Graf_Comp_Precio_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Graf_Precio_Comparativo_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }
}