﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models.Reporting;

namespace Lucky.Xplora.Models.BackusReporting
{
    public class E_Nw_TB_Report_Backus_SOD_Detalle : AE_Nw_TB_Report_Backus_SOD_Detalle
    {
        [JsonProperty("a")]
        public List<E_Nw_TB_Rp_Backus_SOD_Cab_Detalle> Cab_Filas { get; set; }
        [JsonProperty("b")]
        public List<string> Cab_Columna { get; set; }
        [JsonProperty("c")]
        public List<E_Nw_TB_Rp_Backus_SOD_List_Det_Detalle> Detalle { get; set; }
        [JsonProperty("d")]
        public List<E_Nw_TB_Rp_Backus_SOD_List_Det_Detalle> Detalle2 { get; set; }
    }
    public class E_Nw_TB_Rp_Backus_SOD_Cab_Detalle
    {
        [JsonProperty("a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("b")]
        public List<string> nom_cab_fila { get; set; }
    }
    public class E_Nw_TB_Rp_Backus_SOD_List_Det_Detalle
    {
        [JsonProperty("a")]
        public int Cod_TipEmpresa { get; set; }
        [JsonProperty("b")]
        public List<E_Nw_TB_Rp_Backus_SOD_Det_Detalle> Fila { get; set; }
    }
    public class E_Nw_TB_Rp_Backus_SOD_Det_Detalle
    {
        [JsonProperty("a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("b")]
        public List<string> nom_celda { get; set; }
    }
    public class Response_Nw_Reporting_SOD_Detalle_Backus 
    {
        [JsonProperty("a")]
        public E_Nw_TB_Report_Backus_SOD_Detalle Response { get; set; }
    }

    public abstract class AE_Nw_TB_Report_Backus_SOD_Detalle
    {

        public E_Nw_TB_Report_Backus_SOD_Detalle Consul_SOD_Detalle(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_SOD_Detalle_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_SOD_Detalle_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_SOD_Detalle_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }
}