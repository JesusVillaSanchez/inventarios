﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusReporting
{
    #region Resumen

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-07
    /// </summary>
    public class Reporting_Visibilidad : AReporting_Visibilidad
    {
        [JsonProperty("_a")]
        public List<Reporting_Visibilidad_Cadena> cadena { get; set; }

        [JsonProperty("_b")]
        public List<Reporting_Visibilidad_Material> material { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-07
    /// </summary>
    public class Reporting_Visibilidad_Cadena
    {
        [JsonProperty("_a")]
        public int cad_orden { get; set; }

        [JsonProperty("_b")]
        public int cad_id { get; set; }

        [JsonProperty("_c")]
        public string cad_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-07
    /// </summary>
    public class Reporting_Visibilidad_Material
    {
        [JsonProperty("_a")]
        public string color { get; set; }

        [JsonProperty("_b")]
        public int emp_id { get; set; }

        [JsonProperty("_c")]
        public string emp_descripcion { get; set; }

        [JsonProperty("_d")]
        public int mar_id { get; set; }

        [JsonProperty("_e")]
        public string mar_descripcion { get; set; }

        [JsonProperty("_f")]
        public int mat_id { get; set; }

        [JsonProperty("_g")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_h")]
        public string mat_foto { get; set; }

        [JsonProperty("_i")]
        public List<Reporting_Visibilidad_Material_Cadena> cadena { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-07
    /// </summary>
    public class Reporting_Visibilidad_Material_Cadena
    {
        [JsonProperty("_a")]
        public int cad_orden { get; set; }

        [JsonProperty("_b")]
        public int cad_id { get; set; }

        [JsonProperty("_c")]
        public int mat_cantidad { get; set; }
    }

    public abstract class AReporting_Visibilidad {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Reporting_Visibilidad Objeto(Request_ReportingVisibilidad oRq)
        {
            Response_ReportingVisibilidad oRp;

            oRp = MvcApplication._Deserialize<Response_ReportingVisibilidad>(
                    MvcApplication._Servicio_Operativa.Reporting_Visibilidad(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-07
    /// </summary>
    public class Request_ReportingVisibilidad
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int periodo { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public int categoria { get; set; }

        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public int actividad { get; set; }

        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public int tipo_canal { get; set; }

        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int cadena { get; set; }
    }
    public class Response_ReportingVisibilidad
    {
        [JsonProperty("_a")]
        public Reporting_Visibilidad Objeto { get; set; }
    }

    #endregion

    #endregion

    #region Cobertura

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Reporting_Visibilidad_Cobertura : AReporting_Visibilidad_Cobertura
    {
        [JsonProperty("_a")]
        public List<Reporting_Visibilidad_Cobertura_Cadena> cadena { get; set; }

        [JsonProperty("_b")]
        public List<Reporting_Visibilidad_Cobertura_Material> material { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Reporting_Visibilidad_Cobertura_Cadena
    {
        [JsonProperty("_a")]
        public int cad_orden { get; set; }

        [JsonProperty("_b")]
        public int cad_id { get; set; }

        [JsonProperty("_c")]
        public string cad_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Reporting_Visibilidad_Cobertura_Material
    {
        [JsonProperty("_a")]
        public string color { get; set; }

        [JsonProperty("_b")]
        public int emp_id { get; set; }

        [JsonProperty("_c")]
        public string emp_descripcion { get; set; }

        [JsonProperty("_d")]
        public int mar_id { get; set; }

        [JsonProperty("_e")]
        public string mar_descripcion { get; set; }

        [JsonProperty("_f")]
        public int mat_id { get; set; }

        [JsonProperty("_g")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_h")]
        public string mat_foto { get; set; }

        [JsonProperty("_i")]
        public List<Reporting_Visibilidad_Cobertura_Material_Cadena> cadena { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Reporting_Visibilidad_Cobertura_Material_Cadena
    {
        [JsonProperty("_a")]
        public int cad_orden { get; set; }

        [JsonProperty("_b")]
        public int cad_id { get; set; }

        [JsonProperty("_c")]
        public string mat_cantidad { get; set; }
    }

    public abstract class AReporting_Visibilidad_Cobertura
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Reporting_Visibilidad_Cobertura Objeto(Request_ReportingVisibilidad oRq)
        {
            Response_ReportingVisibilidadCobertura oRp;

            oRp = MvcApplication._Deserialize<Response_ReportingVisibilidadCobertura>(
                    MvcApplication._Servicio_Operativa.Reporting_Visibilidad_Cobertura(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Response_ReportingVisibilidadCobertura
    {
        [JsonProperty("_a")]
        public Reporting_Visibilidad_Cobertura Objeto { get; set; }
    }

    #endregion

    #endregion

    #region Detalle

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Reporting_Visibilidad_Detalle : AReporting_Visibilidad_Detalle
    {
        [JsonProperty("_a")]
        public List<Reporting_Visibilidad_Detalle_Pdv> pdv { get; set; }

        [JsonProperty("_b")]
        public List<Reporting_Visibilidad_Detalle_Material> material { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Reporting_Visibilidad_Detalle_Pdv
    {
        [JsonProperty("_a")]
        public int pdv_orden { get; set; }

        [JsonProperty("_b")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_c")]
        public string pdv_descripcion { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Reporting_Visibilidad_Detalle_Material
    {
        [JsonProperty("_a")]
        public string color { get; set; }

        [JsonProperty("_b")]
        public int emp_id { get; set; }

        [JsonProperty("_c")]
        public string emp_descripcion { get; set; }

        [JsonProperty("_d")]
        public int mar_id { get; set; }

        [JsonProperty("_e")]
        public string mar_descripcion { get; set; }

        [JsonProperty("_f")]
        public int mat_id { get; set; }

        [JsonProperty("_g")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_h")]
        public string mat_foto { get; set; }

        [JsonProperty("_i")]
        public List<Reporting_Visibilidad_Detalle_Material_Pdv> pdv { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Reporting_Visibilidad_Detalle_Material_Pdv
    {
        [JsonProperty("_a")]
        public int pdv_orden { get; set; }

        [JsonProperty("_b")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_c")]
        public int mat_presencia { get; set; }
    }

    public abstract class AReporting_Visibilidad_Detalle {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Reporting_Visibilidad_Detalle Objeto(Request_ReportingVisibilidad oRq)
        {
            Response_ReportingVisibilidadDetalle oRp;

            oRp = MvcApplication._Deserialize<Response_ReportingVisibilidadDetalle>(
                    MvcApplication._Servicio_Operativa.Reporting_Visibilidad_Detalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-08
    /// </summary>
    public class Response_ReportingVisibilidadDetalle
    {
        [JsonProperty("_a")]
        public Reporting_Visibilidad_Detalle Objeto { get; set; }
    }

    #endregion

    #endregion
}