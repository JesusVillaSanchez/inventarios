﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models.Reporting;

namespace Lucky.Xplora.Models.BackusReporting
{
    public class E_Nw_Encarte_GeneralSummary_Backus : AE_Nw_Rp_Encarte_Backus
    {
        [JsonProperty("_a")]
        public string Parte { get; set; }
        [JsonProperty("_b")]
        public string Empresa { get; set; }
        [JsonProperty("_c")]
        public string Calculo { get; set; }
    }

    public class E_Nw_Encarte_GeneralSummary_xCadenaBackus : AE_Nw_Rp_Encarte_xCadena_Backus
    {
        [JsonProperty("_a")]
        public string Empresa { get; set; }
        [JsonProperty("_b")]
        public string Cadena { get; set; }
        [JsonProperty("_c")]
        public string Calculo { get; set; }
    }

    public class E_Nw_Participacion_Cadena : AE_Nw_Participacion_Cadena
    {
        [JsonProperty("_a")]
        public string cadena { get; set; }
        [JsonProperty("_b")]
        public List<E_Nw_Participacion_Cadena_Detalle> lista { get; set; }
    }
    public class E_Nw_Participacion_Cadena_Detalle
    {
        [JsonProperty("_b")]
        public string calculo { get; set; }
        [JsonProperty("_a")]
        public string empresa { get; set; }
    }

    public class E_Nw_Encarte_Evolutionary_Summary : AE_Nw_Rp_Evolutionary_Summary
    {
        [JsonProperty("_a")]
        public string Parte { get; set; }
        [JsonProperty("_b")]
        public string Id_Mes { get; set; }
        [JsonProperty("_c")]
        public string Mes { get; set; }
        [JsonProperty("_d")]
        public string Empresa { get; set; }
        [JsonProperty("_e")]
        public string Cant_Anuncios { get; set; }
    }

    public class E_Nw_EvolutionarySummary_NoticeType : AE_Nw_EvolutionarySummary_NoticeType
    {
        [JsonProperty("_a")]
        public string Parte { get; set; }
        [JsonProperty("_b")]
        public string Tipo_anuncio { get; set; }
        [JsonProperty("_c")]
        public string Porcentaje { get; set; }
    }

    public class E_Nw_Participacion_Marca : AE_Nw_Participacion_Marca
    {
        [JsonProperty("_a")]
        public string marca { get; set; }

        [JsonProperty("_b")]
        public List<E_Nw_Participacion_Marca_Detalle> lista { get; set; }
    }
    public class E_Nw_Participacion_Marca_Detalle
    {
        [JsonProperty("_a")]
        public string tipo_anuncio { get; set; }

        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }

    public class E_Nw_Graf_Participacion_Marca : AE_Nw_Graf_Participacion_Marca
    {
        [JsonProperty("_a")]
        public string Marca { get; set; }
        [JsonProperty("_b")]
        public string Tipo_anuncio { get; set; }
        [JsonProperty("_c")]
        public string Porcentaje { get; set; }
    }

    public class E_Nw_Graf_Productdetail : AE_Nw_Graf_Productdetail
    {
        [JsonProperty("_a")]
        public string Marca { get; set; }
        [JsonProperty("_b")]
        public string Producto { get; set; }
        [JsonProperty("_c")]
        public string Mecanica { get; set; }
        [JsonProperty("_d")]
        public string Tipo_Anuncio { get; set; }
        [JsonProperty("_e")]
        public string Cadena { get; set; }
        [JsonProperty("_f")]
        public string Fecha_Inicio { get; set; }
        [JsonProperty("_g")]
        public string Fecha_Termino { get; set; }
        [JsonProperty("_h")]
        public string Precio_Oferta { get; set; }
        [JsonProperty("_i")]
        public string Precio_Normal { get; set; }

    }

    public abstract class AE_Nw_Rp_Encarte_Backus
    {
        public List<E_Nw_Encarte_GeneralSummary_Backus> Consul_Encarte_ResGeneral_Anuncios(Request_Nw_Encarte_Parametros oRq)
        {
            Response_Nw_Encarte_Parametros oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Encarte_Parametros>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Encarte_ResGeneral_Backus_AASS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_Nw_Rp_Encarte_xCadena_Backus {
        public List<E_Nw_Encarte_GeneralSummary_xCadenaBackus> Consul_Encarte_ResGeneral_Anuncios_xCadena(Request_Nw_Encarte_Parametros oRq)
        {
            Response_Nw_Encarte_xCadena_Parametros oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Encarte_xCadena_Parametros>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Encarte_ResGeneral_xCadena_Backus_AASS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_Nw_Participacion_Cadena
    {
        public List<E_Nw_Participacion_Cadena> Consul_Nw_Participacion_Cadena(Request_Nw_Encarte_Parametros oRq)
        {
            Response_Nw_Participacion_Cadena oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Participacion_Cadena>(
                    MvcApplication._Servicio_Operativa.Consul_Nw_Participacion_Cadena(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_Nw_Rp_Evolutionary_Summary {
        public List<E_Nw_Encarte_Evolutionary_Summary> Consul_Nw_Evolutionary_Summary(Request_Nw_Encarte_Parametros oRq)
        {
            Response_Nw_EvolutionarySummary oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_EvolutionarySummary>(
                    MvcApplication._Servicio_Operativa.Consul_Nw_EvolutionarySummary(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }

        public List<E_Nw_Encarte_Evolutionary_Summary> Consul_Nw_Evolutionary_Summary_Chain(Request_Nw_Encarte_Parametros oRq)
        {
            Response_Nw_EvolutionarySummary oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_EvolutionarySummary>(
                    MvcApplication._Servicio_Operativa.Consul_Nw_EvolutionarySummary_Chain(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_Nw_EvolutionarySummary_NoticeType {
        public List<E_Nw_EvolutionarySummary_NoticeType> Consul_Nw_EvolutionarySummary_NoticeType(Request_Nw_Encarte_Parametros oRq)
        {
            Response_Nw_EvolutionarySummary_NoticeType oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_EvolutionarySummary_NoticeType>(
                    MvcApplication._Servicio_Operativa.Consul_Nw_EvolutionarySummary_NoticeType(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_Nw_Participacion_Marca
    {
        public List<E_Nw_Participacion_Marca> Consul_Nw_Participacion_Cadena(Request_Nw_Encarte_Parametros oRq)
        {
            Response_Nw_Participacion_xMarca oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Participacion_xMarca>(
                    MvcApplication._Servicio_Operativa.Consul_Nw_Participacion_Marca(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_Nw_Graf_Participacion_Marca {
        public List<E_Nw_Graf_Participacion_Marca> Consul_Nw_Bar_Participacion_Marca(Request_Nw_Encarte_Parametros oRq)
        {
            Response_Graf_Participacion_Marca oRp;

            oRp = MvcApplication._Deserialize<Response_Graf_Participacion_Marca>(
                    MvcApplication._Servicio_Operativa.Consul_Nw_Bar_Participacion_Marca(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_Nw_Graf_Productdetail {
        public List<E_Nw_Graf_Productdetail> Consul_Nw_DetallePrd(Request_Nw_Encarte_Parametros oRq)
        {
            Response_Graf_Productdetail oRp;

            oRp = MvcApplication._Deserialize<Response_Graf_Productdetail>(
                    MvcApplication._Servicio_Operativa.Consul_Nw_DetallePrd(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    #region << Parametros RQ Lucky 137 v1.2>>
    
    public class E_Nw_Encarte_Parametros
    {
        [JsonProperty("_a")]
        public string Anio { get; set; }
        [JsonProperty("_b")]
        public int Mes { get; set; }
        [JsonProperty("_c")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("_d")]
        public string Cod_Cadena { get; set; }
        [JsonProperty("_e")]
        public string Cod_Nivel { get; set; }
    }

    #endregion
    #region << Request - Response  RQ Lucky 137 v1.2>>
    
    public class Request_Nw_Encarte_Parametros
    {
        [JsonProperty("_a")]
        public E_Nw_Encarte_Parametros oParametros { get; set; }
    }
    public class Response_Nw_Encarte_Parametros
    {
        [JsonProperty("_a")]
        public List<E_Nw_Encarte_GeneralSummary_Backus> Response { get; set; }
    }
    public class Response_Nw_Encarte_xCadena_Parametros
    {
        [JsonProperty("_a")]
        public List<E_Nw_Encarte_GeneralSummary_xCadenaBackus> Response { get; set; }
    }
    public class Response_Nw_Participacion_Cadena
    {
        [JsonProperty("_a")]
        public List<E_Nw_Participacion_Cadena> Response { get; set; }
    }
    public class Response_Nw_EvolutionarySummary
    {
        [JsonProperty("_a")]
        public List<E_Nw_Encarte_Evolutionary_Summary> Response { get; set; }
    }
    public class Response_Nw_EvolutionarySummary_NoticeType
    {
        [JsonProperty("_a")]
        public List<E_Nw_EvolutionarySummary_NoticeType> Response { get; set; }
    }
    public class Response_Nw_Participacion_xMarca
    {
        [JsonProperty("_a")]
        public List<E_Nw_Participacion_Marca> Response { get; set; }
    }
    public class Response_Graf_Participacion_Marca {
        [JsonProperty("_a")]
        public List<E_Nw_Graf_Participacion_Marca> Response { get; set; }
    }
    public class Response_Graf_Productdetail
    {
        [JsonProperty("_a")]
        public List<E_Nw_Graf_Productdetail> Response { get; set; }
    }
    #endregion
}