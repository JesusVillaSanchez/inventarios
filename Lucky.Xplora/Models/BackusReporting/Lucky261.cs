﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusReporting
{
    #region RQ Lucky 261 v1.0

    #region Stockout

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-14
    /// </summary>
    public class StockOut : AStockOut
    {
        [JsonProperty("_a")]
        public Objetivo objetivo { get; set; }

        [JsonProperty("_b")]
        public List<Stock> stock { get; set; }

        [JsonProperty("_c")]
        public List<Stock_Evolutivo> evolutivo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-14
    /// </summary>
    public class Objetivo
    {
        [JsonProperty("_a")]
        public int obj_id { get; set; }

        [JsonProperty("_b")]
        public string obj_objetivo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-14
    /// </summary>
    public class Stock
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public string porcentaje { get; set; }

        [JsonProperty("_d")]
        public string fecha { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-14
    /// </summary>
    public class Stock_Evolutivo
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public List<Stock_Evolutivo_Meses> meses { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-14
    /// </summary>
    public class Stock_Evolutivo_Meses
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public string porcentaje { get; set; }
    }

    public abstract class AStockOut {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public StockOut Objeto(Request_StockOut oRq)
        {
            Response_StockOut oRp;

            oRp = MvcApplication._Deserialize<Response_StockOut>(
                    MvcApplication._Servicio_Operativa.StockOut(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-14
    /// </summary>
    public class Request_StockOut
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int anio { get; set; }

        [JsonProperty("_c")]
        public int mes { get; set; }

        [JsonProperty("_d")]
        public int periodo { get; set; }

        [JsonProperty("_e")]
        public int tipo_canal { get; set; }

        [JsonProperty("_f")]
        public int cadena { get; set; }

        [JsonProperty("_g")]
        public int categoria { get; set; }

        [JsonProperty("_h")]
        public string nivel { get; set; }

        [JsonProperty("_i")]
        public int marca { get; set; }
    }
    public class Response_StockOut
    {
        [JsonProperty("a")]
        public StockOut Objeto { get; set; }
    }

    #endregion

    #endregion

    #region Precios

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-22
    /// </summary>
    public class Precio : APrecio
    {
        [JsonProperty("_a")]
        public List<Precio_Meses> meses { get; set; }

        [JsonProperty("_b")]
        public List<Precio_Sku> precio_sku { get; set; }

        [JsonProperty("_c")]
        public List<Precio_Litro> precio_litro { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-22
    /// </summary>
    public class Precio_Meses
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public string desde { get; set; }

        [JsonProperty("_d")]
        public string hasta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-22
    /// </summary>
    public class Precio_Sku
    {
        [JsonProperty("_a")]
        public string pro_sku { get; set; }

        [JsonProperty("_b")]
        public string pro_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<Precio_Sku_Meses> periodo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-22
    /// </summary>
    public class Precio_Sku_Meses
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public string precio { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-22
    /// </summary>
    public class Precio_Litro
    {
        [JsonProperty("_a")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_b")]
        public string nivel { get; set; }

        [JsonProperty("_c")]
        public string compania { get; set; }

        [JsonProperty("_d")]
        public int cantidad { get; set; }

        [JsonProperty("_e")]
        public string c1 { get; set; }

        [JsonProperty("_f")]
        public string c2 { get; set; }

        [JsonProperty("_g")]
        public string c3 { get; set; }

        [JsonProperty("_h")]
        public string c4 { get; set; }

        [JsonProperty("_i")]
        public string c5 { get; set; }

        [JsonProperty("_j")]
        public string c6 { get; set; }

        [JsonProperty("_k")]
        public string c7 { get; set; }

        [JsonProperty("_l")]
        public string c8 { get; set; }

        [JsonProperty("_m")]
        public string c9 { get; set; }

        [JsonProperty("_n")]
        public string c10 { get; set; }

        [JsonProperty("_o")]
        public string c11 { get; set; }

        [JsonProperty("_p")]
        public string c12 { get; set; }

        [JsonProperty("_q")]
        public string cod_reg { get; set; }

        [JsonProperty("_r")]
        public string cod_padre { get; set; }
    }

    public abstract class APrecio {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-22
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Precio Objeto(Request_Precio oRq)
        {
            Response_Precio oRp;

            oRp = MvcApplication._Deserialize<Response_Precio>(
                    MvcApplication._Servicio_Operativa.Precio(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-04-22
    /// </summary>
    public class Request_Precio
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int anio { get; set; }

        [JsonProperty("_c")]
        public int mes { get; set; }

        [JsonProperty("_d")]
        public int periodo { get; set; }

        [JsonProperty("_e")]
        public int tipo_canal { get; set; }

        [JsonProperty("_f")]
        public int cadena { get; set; }

        [JsonProperty("_g")]
        public int categoria { get; set; }

        [JsonProperty("_h")]
        public string nivel { get; set; }

        [JsonProperty("_i")]
        public int marca { get; set; }

        [JsonProperty("_j")]
        public int presentacion { get; set; }

        [JsonProperty("_k")]
        public string envase { get; set; }

        [JsonProperty("_l")]
        public string propio { get; set; }

        [JsonProperty("_m")]
        public string competencia { get; set; }
    }
    public class Response_Precio
    {
        [JsonProperty("a")]
        public Precio Objeto { get; set; }
    }

    #endregion

    #endregion

    #region Participacion

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion : AParticipacion 
    {
        [JsonProperty("_a")]
        public List<Participacion_Meses> meses { get; set; }

        [JsonProperty("_b")]
        public List<Participacion_Objetivo> objetivo { get; set; }

        [JsonProperty("_c")]
        public List<Participacion_Material> material { get; set; }

        [JsonProperty("_d")]
        public List<Participacion_Pdv> pdv { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Meses
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string descripcion { get; set; }

        [JsonProperty("_c")]
        public string desde { get; set; }

        [JsonProperty("_d")]
        public string hasta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Objetivo
    {
        [JsonProperty("_a")]
        public int tma_id { get; set; }

        [JsonProperty("_b")]
        public string tma_descripcion { get; set; }

        [JsonProperty("_c")]
        public int objetivo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Material
    {
        [JsonProperty("_a")]
        public int tma_id { get; set; }

        [JsonProperty("_b")]
        public List<Participacion_Material_Meses> meses { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Material_Meses
    {
        [JsonProperty("_a")]
        public int his_id { get; set; }

        [JsonProperty("_b")]
        public string his_descripcion { get; set; }

        [JsonProperty("_c")]
        public int total { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Pdv
    {
        [JsonProperty("_a")]
        public string tma_descripcion { get; set; }

        [JsonProperty("_b")]
        public int pdv_total { get; set; }

        [JsonProperty("_c")]
        public int pdv_material { get; set; }
    }

    public abstract class AParticipacion {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-04
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Participacion Objeto(Request_Participacion oRq)
        {
            Response_Participacion oRp;

            oRp = MvcApplication._Deserialize<Response_Participacion>(
                    MvcApplication._Servicio_Operativa.Participacion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Request_Participacion
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int anio { get; set; }

        [JsonProperty("_c")]
        public int mes { get; set; }

        [JsonProperty("_d")]
        public int periodo { get; set; }

        [JsonProperty("_e")]
        public int tipo_canal { get; set; }

        [JsonProperty("_f")]
        public int cadena { get; set; }

        [JsonProperty("_g")]
        public int categoria { get; set; }

        [JsonProperty("_h")]
        public int cerveza { get; set; }
    }
    public class Response_Participacion
    {
        [JsonProperty("_a")]
        public Participacion Objeto { get; set; }
    }

    #endregion

    #endregion

    #region Participacion Evolutivo

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Evolutivo : AParticipacion_Evolutivo
    {
        [JsonProperty("_a")]
        public List<Participacion_Evolutivo_Anio> anio { get; set; }

        [JsonProperty("_b")]
        public Participacion_Evolutivo_Objetivo objetivo { get; set; }

        [JsonProperty("_c")]
        public List<Participacion_Evolutivo_Material> material { get; set; }

        [JsonProperty("_d")]
        public List<Participacion_Evolutivo_Pdv> pdv { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>m
    public class Participacion_Evolutivo_Anio
    {
        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public List<Participacion_Evolutivo_Meses> meses { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>m
    public class Participacion_Evolutivo_Meses
    {
        [JsonProperty("_a")]
        public int mes_id { get; set; }

        [JsonProperty("_b")]
        public string mes_descripcion { get; set; }

        [JsonProperty("_c")]
        public string desde { get; set; }

        [JsonProperty("_d")]
        public string hasta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Evolutivo_Objetivo
    {
        [JsonProperty("_a")]
        public int tma_id { get; set; }

        [JsonProperty("_b")]
        public string tma_descripcion { get; set; }

        [JsonProperty("_c")]
        public int objetivo { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Evolutivo_Material
    {
        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public List<Participacion_Evolutivo_Material_Meses> meses { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Evolutivo_Material_Meses
    {
        [JsonProperty("_a")]
        public int mes_id { get; set; }

        [JsonProperty("_b")]
        public string mes_descripcion { get; set; }

        [JsonProperty("_c")]
        public int total { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Evolutivo_Pdv
    {
        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public List<Participacion_Evolutivo_Pdv_Meses> meses { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Participacion_Evolutivo_Pdv_Meses
    {
        [JsonProperty("_a")]
        public int mes_id { get; set; }

        [JsonProperty("_b")]
        public string mes_descripcion { get; set; }

        [JsonProperty("_c")]
        public string informacion { get; set; }
    }

    public abstract class AParticipacion_Evolutivo {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-04
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Participacion_Evolutivo Objeto(Request_Participacion_Evolutivo oRq)
        {
            Response_Participacion_Evolutivo oRp;

            oRp = MvcApplication._Deserialize<Response_Participacion_Evolutivo>(
                    MvcApplication._Servicio_Operativa.Participacion_Evolutivo(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-04
    /// </summary>
    public class Request_Participacion_Evolutivo
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int anio { get; set; }

        [JsonProperty("_c")]
        public int mes { get; set; }

        [JsonProperty("_d")]
        public int periodo { get; set; }

        [JsonProperty("_e")]
        public int tipo_canal { get; set; }

        [JsonProperty("_f")]
        public int cadena { get; set; }

        [JsonProperty("_g")]
        public int categoria { get; set; }

        [JsonProperty("_h")]
        public string nivel { get; set; }

        [JsonProperty("_i")]
        public int marca { get; set; }

        [JsonProperty("_j")]
        public int tipo_material { get; set; }
    }
    public class Response_Participacion_Evolutivo
    {
        [JsonProperty("_a")]
        public Participacion_Evolutivo Objeto { get; set; }
    }

    #endregion

    #endregion

    #region Exhibicion

    public class Exhibicion_Adicional : AExhibicion_Adicional
    {
        [JsonProperty("_a")]
        public string Parte { get; set; }
        [JsonProperty("_b")]
        public string commercialNodeName { get; set; }
        [JsonProperty("_c")]
        public string POP_name { get; set; }
        [JsonProperty("_d")]
        public string cantidad { get; set; }
    }

    public abstract class AExhibicion_Adicional {
        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-05-7
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Exhibicion_Adicional> Lista(Request_Additional_Exhibition oRq)
        {
            Response_Additional_Exhibition oRp;

            oRp = MvcApplication._Deserialize<Response_Additional_Exhibition>(
                    MvcApplication._Servicio_Operativa.Additional_Exhibition(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    public class Request_Additional_Exhibition
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int anio { get; set; }

        [JsonProperty("_c")]
        public int mes { get; set; }

        [JsonProperty("_d")]
        public int periodo { get; set; }

        [JsonProperty("_e")]
        public int tipo_canal { get; set; }

        [JsonProperty("_f")]
        public int cadena { get; set; }

        [JsonProperty("_g")]
        public int categoria { get; set; }

        [JsonProperty("_h")]
        public string nivel { get; set; }

        [JsonProperty("_i")]
        public int cod_marca { get; set; }
    }
    public class Response_Additional_Exhibition
    {
        [JsonProperty("_a")]
        public List<Exhibicion_Adicional> Lista { get; set; }
    }

     #endregion

    #endregion

    #endregion
}