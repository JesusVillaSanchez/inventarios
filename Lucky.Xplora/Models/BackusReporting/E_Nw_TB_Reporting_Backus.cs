﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models.Reporting;

namespace Lucky.Xplora.Models.BackusReporting
{
    public class E_Nw_TB_Reporting_Backus : AE_Nw_TB_Reporting_Backus
    {
        [JsonProperty("a")]
        public List<E_Nw_TB_Reporting_Cab_Backus> Lis_Cab { get; set; }
        [JsonProperty("b")]
        public List<E_Nw_TB_Reporting_Det_Backus> List_Det { get; set; }
        [JsonProperty("c")]
        public List<E_Nw_TB_Reporting_Det_Backus> List_Tot { get; set; }
    }
    public class E_Nw_TB_Reporting_Cab_Backus
    {
        [JsonProperty("a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("b")]
        public List<E_Nw_TB_Reporting_ItemCab_Backus> List_Cab { get; set; }
    }
    public class E_Nw_TB_Reporting_ItemCab_Backus
    {
        [JsonProperty("a")]
        public string Nom_Cab { get; set; }
        [JsonProperty("b")]
        public string Item_Select { get; set; }
    }
    public class E_Nw_TB_Reporting_Det_Backus
    {
        public List<E_Nw_TB_Reporting_FilaDet_Backus> Fila { get; set; }
    }
    public class E_Nw_TB_Reporting_FilaDet_Backus
    {
        [JsonProperty("a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("b")]
        public List<string> Lista { get; set; }
    }
    public class Response_Nw_Reporting_Backus 
    {
        [JsonProperty("a")]
        public E_Nw_TB_Reporting_Backus Response { get; set; }
    }
    public abstract class AE_Nw_TB_Reporting_Backus
    {

        public E_Nw_TB_Reporting_Backus Consul_OutOfStock_Reporting_evolutivo(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Tb_OutOfStock_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
        public E_Nw_TB_Reporting_Backus Consul_OutOfStockxCadena_Reporting_evolutivo(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Tb_OutOfStockxCadena_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
        public E_Nw_TB_Reporting_Backus Consul_PresenciaProducto_Reporting_evolutivo(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Tb_PresenciaProd_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
        public E_Nw_TB_Reporting_Backus Consul_PresenciaxCadena_Reporting_evolutivo(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Tb_PresenciaProdxCadena_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
        public E_Nw_TB_Reporting_Backus Consul_PrecioResumen_Reporting(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Tb_Precio_Resumen_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
        public E_Nw_TB_Reporting_Backus Consul_PrecioComparativo_Reporting(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Tb_Precio_Comparativo_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }

        public E_Nw_TB_Reporting_Backus Consul_SOD_Resumen_Reporting(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_SOD_Resumen_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }
}