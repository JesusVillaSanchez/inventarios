﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models.Reporting;

namespace Lucky.Xplora.Models.BackusReporting
{
    public class E_Nw_Rp_Backus_Param_Iniciales : AE_Nw_Rp_Backus_Param_Iniciales
    {
        [JsonProperty("a")]
        public int Anio { get; set; }
        [JsonProperty("b")]
        public string Mes { get; set; }
        [JsonProperty("c")]
        public int Cod_Periodo { get; set; }
        [JsonProperty("d")]
        public int Cod_Cadena { get; set; }
        [JsonProperty("e")]
        public string Cod_Departamento { get; set; }
        [JsonProperty("f")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("g")]
        public string Cod_Nivel { get; set; }
        [JsonProperty("h")]
        public string Cod_Actividad { get; set; }
        [JsonProperty("i")]
        public int Cod_Marca { get; set; }
        [JsonProperty("j")]
        public string Cod_Elemento { get; set; }
    }
    public class Response_Nw_Reporting_Param_Iniciales_Backus
    {
        [JsonProperty("a")]
        public E_Nw_Rp_Backus_Param_Iniciales Response { get; set; }
    }
    public abstract class AE_Nw_Rp_Backus_Param_Iniciales
    {
        public E_Nw_Rp_Backus_Param_Iniciales Consul_Inicial(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Param_Iniciales_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Param_Iniciales_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Param_Iniciales_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

}