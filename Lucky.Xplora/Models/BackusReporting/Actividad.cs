﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusReporting
{
    public class Actividad
    {
        [JsonProperty("_a")]
        public int cod_tipo_empresa { get; set; }

        [JsonProperty("_b")]
        public string nom_tipo_empresa { get; set; }

        [JsonProperty("_c")]
        public string color_empresa { get; set; }

        [JsonProperty("_d")]
        public int cod_categoria { get; set; }

        [JsonProperty("_e")]
        public string categoria { get; set; }

        [JsonProperty("_f")]
        public int cod_marca { get; set; }

        [JsonProperty("_g")]
        public string marca { get; set; }

        [JsonProperty("_h")]
        public int anio { get; set; }

        [JsonProperty("_i")]
        public int cod_mes { get; set; }

        [JsonProperty("_j")]
        public string nom_mes { get; set; }

        [JsonProperty("_k")]
        public string actividad { get; set; }

        [JsonProperty("_l")]
        public string color_marca { get; set; }

        [JsonProperty("_m")]
        public string color_texto { get; set; }
    }


    public class ActividadFoto : AActividad
    {
        [JsonProperty("_aa")]
        public int cod_actividad { get; set; }

        [JsonProperty("_ab")]
        public int cod_tipo_empresa { get; set; }

        [JsonProperty("_ac")]
        public string desc_tipo_empresa { get; set; }

        [JsonProperty("_ad")]
        public string cod_categoria { get; set; }

        [JsonProperty("_ae")]
        public string desc_categoria { get; set; }

        [JsonProperty("_af")]
        public string cod_marca { get; set; }

        [JsonProperty("_ag")]
        public string desc_marca { get; set; }

        [JsonProperty("_ah")]
        public int anio { get; set; }

        [JsonProperty("_ai")]
        public int mes_inicio { get; set; }

        [JsonProperty("_aj")]
        public int mes_fin { get; set; }

        [JsonProperty("_ak")]
        public string desc_canal { get; set; }

        [JsonProperty("_al")]
        public string desc_cadena { get; set; }

        [JsonProperty("_am")]
        public string desc_actividad { get; set; }

        [JsonProperty("_an")]
        public string nombre_foto { get; set; }

    }


    public abstract class AActividad
    {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-07-20
        /// </summary>
        /// <returns></returns>
        public List<ActividadFoto> Lista(Request_Consulta_Actividad_Foto oRq)
        {
            Response_Consulta_Actividad_Foto oRp;

            oRp = MvcApplication._Deserialize<Response_Consulta_Actividad_Foto>(
                MvcApplication._Servicio_Operativa.Lista_Actividad_Foto(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.oLista;
        }
    }

    ///<summary>
    ///Autor: rcontreras
    ///Fecha: 06-05-2015
    ///Description: Actividad - Backus
    ///
    public class Request_Actividad
    {
        [JsonProperty("_a")]
        public int anio { get; set; }
    }
    public class Response_Actividad
    {
        [JsonProperty("_a")]
        public List<Actividad> Lista { get; set; }
    }

    public class Request_Consulta_Actividad_Foto
    {
        [JsonProperty("_a")]
        public int cod_actividad { get; set; }
    }

    public class Response_Consulta_Actividad_Foto
    {
        [JsonProperty("_a")]
        public List<ActividadFoto> oLista { get; set; }
    }


}