﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusReporting
{
    public class Fotografico : AFotografico
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_c")]
        public string pdv_razonsocial { get; set; }

        [JsonProperty("_d")]
        public string pdv_direccion { get; set; }

        [JsonProperty("_e")]
        public string dis_descripcion { get; set; }

        [JsonProperty("_f")]
        public string prv_descripcion { get; set; }

        [JsonProperty("_g")]
        public string fecha { get; set; }

        [JsonProperty("_h")]
        public string hora { get; set; }

        [JsonProperty("_i")]
        public string foto { get; set; }

        [JsonProperty("_j")]
        public string comentario { get; set; }

        [JsonProperty("_k")]
        public string archivo { get; set; }

        [JsonProperty("_l")]
        public int cat_id { get; set; }

        [JsonProperty("_m")]
        public string cat_descripcion { get; set; }

        [JsonProperty("_n")]
        public string TMPPdvCod { get; set; }

        [JsonProperty("_o")]
        public string TMPPdvDescripcion { get; set; }

        [JsonProperty("_p")]
        public string TMPCiudadDescripcion { get; set; }
    }

    public abstract class AFotografico
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-02
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Fotografico> Lista(Request_Fotografico oRq) {
            Response_Fotografico oRp;

            oRp = MvcApplication._Deserialize<Response_Fotografico>(
                    MvcApplication._Servicio_Operativa.Fotografico(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-03-02
    /// </summary>
    public class Request_Fotografico
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int tipocanal { get; set; }

        [JsonProperty("_c")]
        public int anio { get; set; }

        [JsonProperty("_d")]
        public int mes { get; set; }

        [JsonProperty("_e")]
        public int periodo { get; set; }

        [JsonProperty("_f")]
        public int nodocomercial { get; set; }

        [JsonProperty("_g")]
        public int categoria { get; set; }

        [JsonProperty("_h")]
        public string puntoventa { get; set; }

        [JsonProperty("_i")]
        public int dia { get; set; }

        [JsonProperty("_j")]
        public string ciudad { get; set; }

        [JsonProperty("_k")]
        public string departamento { get; set; }

        [JsonProperty("_l")]
        public string fechaInicio { get; set; }

        [JsonProperty("_m")]
        public string fechaFin { get; set; }

        [JsonProperty("_n")]
        public string dex { get; set; }

        [JsonProperty("_o")]
        public string supervisor { get; set; }

        [JsonProperty("_p")]
        public string gestor { get; set; }

        [JsonProperty("_q")]
        public string programa { get; set; }

    }
    public class Response_Fotografico
    {
        [JsonProperty("_a")]
        public List<Fotografico> Lista { get; set; }
    }

    #endregion
}