﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.ColgateReporting
{
    public class Presencia_Summary : APresencia_Summary
    {
        [JsonProperty("_a")]
        public List<Presencia_Coverage> coverage { get; set; }

        [JsonProperty("_b")]
        public List<Presencia_Product> product { get; set; }

        [JsonProperty("_c")]
        public List<Presencia_Rango> rango { get; set; }

        [JsonProperty("_d")]
        public List<Presencia_Head> head { get; set; }

        [JsonProperty("_e")]
        public List<Presencia_Categoria_Product_Mandatorio> mandatorio { get; set; }

        [JsonProperty("_f")]
        public List<Presencia_Categoria_Product_Agrupado> agrupado { get; set; }
    }

    public class Presencia_Head
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public int periodo { get; set; }

        [JsonProperty("_c")]
        public string fecha_inicio { get; set; }

        [JsonProperty("_d")]
        public string fecha_fin { get; set; }

        [JsonProperty("_e")]
        public string descripcion { get; set; }

        [JsonProperty("_f")]
        public int llave { get; set; }
    }

    public class Presencia_Coverage
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string resumen { get; set; }

        [JsonProperty("_c")]
        public string periodo1 { get; set; }

        [JsonProperty("_d")]
        public string periodo2 { get; set; }

        [JsonProperty("_e")]
        public string periodo3 { get; set; }

        [JsonProperty("_f")]
        public string periodo4 { get; set; }

        [JsonProperty("_g")]
        public string periodo5 { get; set; }

        [JsonProperty("_h")]
        public string periodo6 { get; set; }

        [JsonProperty("_i")]
        public string periodo7 { get; set; }

        [JsonProperty("_j")]
        public string periodo8 { get; set; }

        [JsonProperty("_k")]
        public string nacional { get; set; }

        [JsonProperty("_l")]
        public string lima { get; set; }

        [JsonProperty("_m")]
        public string oficina1 { get; set; }

        [JsonProperty("_n")]
        public string oficina2 { get; set; }
    }

    public class Presencia_Product
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string categoria { get; set; }

        [JsonProperty("_c")]
        public string producto { get; set; }

        [JsonProperty("_d")]
        public string periodo1 { get; set; }

        [JsonProperty("_e")]
        public string periodo2 { get; set; }

        [JsonProperty("_f")]
        public string periodo3 { get; set; }

        [JsonProperty("_g")]
        public string periodo4 { get; set; }

        [JsonProperty("_h")]
        public string periodo5 { get; set; }

        [JsonProperty("_i")]
        public string periodo6 { get; set; }

        [JsonProperty("_j")]
        public string periodo7 { get; set; }

        [JsonProperty("_k")]
        public string periodo8 { get; set; }

        [JsonProperty("_l")]
        public string nacional { get; set; }

        [JsonProperty("_m")]
        public string lima { get; set; }

        [JsonProperty("_n")]
        public string oficina1 { get; set; }

        [JsonProperty("_o")]
        public string oficina2 { get; set; }
    }

    public class Presencia_Rango
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string rango { get; set; }

        [JsonProperty("_c")]
        public string periodo1 { get; set; }

        [JsonProperty("_d")]
        public string periodo2 { get; set; }

        [JsonProperty("_e")]
        public string periodo3 { get; set; }

        [JsonProperty("_f")]
        public string periodo4 { get; set; }

        [JsonProperty("_g")]
        public string periodo5 { get; set; }

        [JsonProperty("_h")]
        public string periodo6 { get; set; }

        [JsonProperty("_i")]
        public string periodo7 { get; set; }

        [JsonProperty("_j")]
        public string periodo8 { get; set; }

        [JsonProperty("_k")]
        public string nacional { get; set; }

        [JsonProperty("_l")]
        public string lima { get; set; }

        [JsonProperty("_m")]
        public string oficina1 { get; set; }

        [JsonProperty("_n")]
        public string oficina2 { get; set; }
    }

    public class Presencia_Categoria_Product_Mandatorio
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string compania { get; set; }

        [JsonProperty("_c")]
        public string producto { get; set; }

        [JsonProperty("_d")]
        public string periodo1 { get; set; }

        [JsonProperty("_e")]
        public string periodo2 { get; set; }

        [JsonProperty("_f")]
        public string periodo3 { get; set; }

        [JsonProperty("_g")]
        public string periodo4 { get; set; }

        [JsonProperty("_h")]
        public string periodo5 { get; set; }

        [JsonProperty("_i")]
        public string periodo6 { get; set; }

        [JsonProperty("_j")]
        public string periodo7 { get; set; }

        [JsonProperty("_k")]
        public string periodo8 { get; set; }

        [JsonProperty("_l")]
        public string nacional { get; set; }

        [JsonProperty("_m")]
        public string lima { get; set; }

        [JsonProperty("_n")]
        public string oficina1 { get; set; }

        [JsonProperty("_o")]
        public string oficina2 { get; set; }
    }

    public class Presencia_Categoria_Product_Agrupado
    {
        [JsonProperty("_a")]
        public int id { get; set; }

        [JsonProperty("_b")]
        public string grupo { get; set; }

        [JsonProperty("_c")]
        public string compania { get; set; }

        [JsonProperty("_d")]
        public string producto { get; set; }

        [JsonProperty("_e")]
        public string periodo1 { get; set; }

        [JsonProperty("_f")]
        public string periodo2 { get; set; }

        [JsonProperty("_g")]
        public string periodo3 { get; set; }

        [JsonProperty("_h")]
        public string periodo4 { get; set; }

        [JsonProperty("_i")]
        public string periodo5 { get; set; }

        [JsonProperty("_j")]
        public string periodo6 { get; set; }

        [JsonProperty("_k")]
        public string periodo7 { get; set; }

        [JsonProperty("_l")]
        public string periodo8 { get; set; }

        [JsonProperty("_m")]
        public string nacional { get; set; }

        [JsonProperty("_n")]
        public string lima { get; set; }

        [JsonProperty("_o")]
        public string oficina1 { get; set; }

        [JsonProperty("_p")]
        public string oficina2 { get; set; }
    }

    public abstract class APresencia_Summary {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-19
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Presencia_Summary Summary(Request_Presencia_Summary oRq) {
            Response_Presencia_Summary oRp;

            oRp = MvcApplication._Deserialize<Response_Presencia_Summary>(
                    MvcApplication._Servicio_Operativa.Presencia_Summary(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Summary;
        }


        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-07-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Presencia_Summary SummaryObjetivo(Request_Presencia_Summary oRq) {
            Response_Presencia_SummaryObjetivo oRp;

            oRp = MvcApplication._Deserialize<Response_Presencia_SummaryObjetivo>(
                    MvcApplication._Servicio_Operativa.Presencia_Summary_Objetivo(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.SummaryObjetivo;
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-07-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Presencia_Summary SummaryProduct(Request_Presencia_Summary oRq)
        {
            Response_Presencia_SummaryProduct oRp;

            oRp = MvcApplication._Deserialize<Response_Presencia_SummaryProduct>(
                    MvcApplication._Servicio_Operativa.Presencia_Product(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.SummaryProduct;
        }


        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-07-08
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Presencia_Summary SummaryRango(Request_Presencia_Summary oRq)
        {
            Response_Presencia_SummaryRango oRp;

            oRp = MvcApplication._Deserialize<Response_Presencia_SummaryRango>(
                    MvcApplication._Servicio_Operativa.Presencia_Rango(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.SummaryRango;
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-07-09
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Presencia_Summary productByCategory(Request_Presencia_Summary oRq)
        {
            Response_Presencia_Category oRp;

            oRp = MvcApplication._Deserialize<Response_Presencia_Category>(
                    MvcApplication._Servicio_Operativa.Presencia_Categoria(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.productByCategory;
        }

    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-19
    /// </summary>
    public class Request_Presencia_Summary
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int categoria { get; set; }

        [JsonProperty("_c")]
        public int periodo { get; set; }

        [JsonProperty("_d")]
        public int oficina { get; set; }

        [JsonProperty("_e")]
        public int sector { get; set; }

        [JsonProperty("_f")]
        public int nodocomercial { get; set; }
    }
    public class Response_Presencia_Summary
    {
        [JsonProperty("_a")]
        public Presencia_Summary Summary { get; set; }
    }

    public class Response_Presencia_SummaryObjetivo
    {
        [JsonProperty("_a")]
        public Presencia_Summary SummaryObjetivo { get; set; }
    }

    public class Response_Presencia_SummaryProduct
    {
        [JsonProperty("_a")]
        public Presencia_Summary SummaryProduct { get; set; }
    }

    public class Response_Presencia_SummaryRango
    {
        [JsonProperty("_a")]
        public Presencia_Summary SummaryRango { get; set; }
    }

    public class Response_Presencia_Category
    {
        [JsonProperty("_a")]
        public Presencia_Summary productByCategory { get; set; }
    }

    #endregion
}