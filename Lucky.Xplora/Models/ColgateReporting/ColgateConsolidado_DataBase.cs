﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.ColgateReporting
{
    public class ColgateConsolidado_DataBase : AColgateConsolidado_DataBase
    {
        [JsonProperty("a")]
        public int tmp_id { get; set; }
        [JsonProperty("b")]
        public string tmp_agrupado { get; set; }
        [JsonProperty("c")]
        public string tmp_cabecera { get; set; }
        [JsonProperty("d")]
        public string tmp_descripcion { get; set; }
        [JsonProperty("e")]
        public string tmp_codigo { get; set; }
    }

    public abstract class AColgateConsolidado_DataBase
    {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-02-2015
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<ColgateConsolidado_DataBase> Lista(Request_ColgateConsolidado_DataBase_Reports oRq)
        {
            Response_ColgateConsolidado_DataBase_Reports oRp;

            oRp = MvcApplication._Deserialize<Response_ColgateConsolidado_DataBase_Reports>(
                MvcApplication._Servicio_Operativa.Consulta_DataBase_Colgate(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        public List<ColgateConsolidado_DataBase> Lista(Consulta_DataBase_Colgate_Maps_Request oRq)
        {
            Response_ColgateConsolidado_DataBase_Reports oRp;

            oRp = MvcApplication._Deserialize<Response_ColgateConsolidado_DataBase_Reports>(
                MvcApplication._Servicio_Operativa.Consulta_DataBase_Colgate_Maps(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 2015-02-13
    /// </summary>
    public class Request_ColgateConsolidado_DataBase_Reports
    {
        [JsonProperty("a")]
        public string planning { get; set; }

        [JsonProperty("b")]
        public string periodo { get; set; }

        [JsonProperty("c")]
        public string filtro { get; set; }
    }

    public class Consulta_DataBase_Colgate_Maps_Request
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string planning { get; set; }
        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public int periodo { get; set; }
        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string tipoperfil { get; set; }
        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string cadena { get; set; }
        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public string pdv { get; set; }
    }

    public class Response_ColgateConsolidado_DataBase_Reports
    {
        [JsonProperty("a")]
        public List<ColgateConsolidado_DataBase> Lista { get; set; }
    }

    #endregion


}