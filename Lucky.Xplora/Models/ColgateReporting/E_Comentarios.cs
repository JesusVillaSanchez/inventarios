﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.ColgateReporting
{
    public class E_Comentarios : AInsertComentarios
    {
        [JsonProperty("_a")]
        public int periodo { get; set; }

        [JsonProperty("_b")]
        public string planning { get; set; }

        [JsonProperty("_c")]
        public string comentario { get; set; }
    }

    public abstract class AInsertComentarios
    {
        public int insertComentarios(InsertComentarios_Request oRq)
        {
            InsertComentarios_Response oRp;

            oRp = MvcApplication._Deserialize<InsertComentarios_Response>(
                    MvcApplication._Servicio_Operativa.insertComments(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.iRespuesta;
        }

        public List<E_Comentarios> Lista(InsertComentarios_Request oRq)
        {
            Response_ListaCommentsColgateFusion oRp;

            oRp = MvcApplication._Deserialize<Response_ListaCommentsColgateFusion>(
                MvcApplication._Servicio_Operativa.ListaCommentsColgateFusion(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }


    }

    /// <summary>
    /// Fecha: 08-04-2015
    /// Creado: rcontreras
    /// </summary>
    /// 
    public class InsertComentarios_Request
    {
        [JsonProperty("_a")]
        public int periodo { get; set; }

        [JsonProperty("_b")]
        public string planning { get; set; }

        [JsonProperty("_c")]
        public string comentario { get; set; }
    }
    public class InsertComentarios_Response
    {
        [JsonProperty("a")]
        public int iRespuesta { get; set; }
    }

    public class Response_ListaCommentsColgateFusion
    {
        [JsonProperty("_a")]
        public List<E_Comentarios> Lista { get; set; }
    }

}