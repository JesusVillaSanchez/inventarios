﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.ColgateReporting
{
    public class E_Dsh_Colgate
    {
        public E_Dsh_Colgate_Presencia_G Presencia(Request_Dsh_Colg oRq)
        {
            Response_Dsh_Colg_Presencia oRp;

            oRp = MvcApplication._Deserialize<Response_Dsh_Colg_Presencia>(MvcApplication._Servicio_Operativa.Dsh_Colg_Presencia(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<M_Combo> Filtros(Request_Dsh_Colg oRq)
        {
            Response_Dsh_Colg_Filtros oRp;

            oRp = MvcApplication._Deserialize<Response_Dsh_Colg_Filtros>(MvcApplication._Servicio_Operativa.Dsh_Colg_Filtros(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<E_Dsh_Colg_ShareOfShelf> ShareOfShelf(Request_Dsh_Colg oRq)
        {
            Response_Dsh_Colg_ShareOfShelf oRp;

            oRp = MvcApplication._Deserialize<Response_Dsh_Colg_ShareOfShelf>(MvcApplication._Servicio_Operativa.Dsh_Colg_ShareOfShelf(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<E_Dsh_Colg_Coverage> Coverage(Request_Dsh_Colg oRq)
        {
            Response_Dsh_Colg_Coverage oRp;

            oRp = MvcApplication._Deserialize<Response_Dsh_Colg_Coverage>(MvcApplication._Servicio_Operativa.Dsh_Colg_Coverage(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<E_Dsh_Colg_Precio> Precio(Request_Dsh_Colg oRq)
        {
            Response_Dsh_Colg_Precio oRp;

            oRp = MvcApplication._Deserialize<Response_Dsh_Colg_Precio>(MvcApplication._Servicio_Operativa.Dsh_Colg_Precio(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<E_Dsh_Colg_Visibilidad> Visibilidad(Request_Dsh_Colg oRq)
        {
            Response_Dsh_Colg_Visibilidad oRp;

            oRp = MvcApplication._Deserialize<Response_Dsh_Colg_Visibilidad>(MvcApplication._Servicio_Operativa.Dsh_Colg_Visibilidad(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<E_Dsh_Colg_ShareOfExhibicion> ShareOfExhibicion(Request_Dsh_Colg oRq)
        {
            Response_Dsh_Colg_ShareOfExhibicion oRp;

            oRp = MvcApplication._Deserialize<Response_Dsh_Colg_ShareOfExhibicion>(MvcApplication._Servicio_Operativa.Dsh_Colg_ShareOfExhibicion(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<E_Dsh_Colg_ShareOfPromocion> ShareOfPromocion(Request_Dsh_Colg oRq)
        {
            Response_Dsh_Colg_ShareOfPromocion oRp;

            oRp = MvcApplication._Deserialize<Response_Dsh_Colg_ShareOfPromocion>(MvcApplication._Servicio_Operativa.Dsh_Colg_ShareOfPromocion(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
    }

    #region << Request - Response >>
    public class Request_Dsh_Colg
    {
        [JsonProperty("_a")]
        public int Cod_Empresa { get; set; }
        [JsonProperty("_b")]
        public int Cod_Anio { get; set; }
        [JsonProperty("_c")]
        public int Cod_Mes { get; set; }
        [JsonProperty("_d")]
        public int Cod_Categoria { get; set; }
        [JsonProperty("_e")]
        public int Cod_Opcion { get; set; }
        [JsonProperty("_f")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("_g")]
        public string Parametros { get; set; }
        [JsonProperty("_h")]
        public int Cod_Periodo { get; set; }
        [JsonProperty("_i")]
        public string Cod_Sku { get; set; }
        [JsonProperty("_j")]
        public string Cod_Elemento { get; set; }

    }
    public class Response_Dsh_Colg_Presencia
    {
        [JsonProperty("_a")]
        public E_Dsh_Colgate_Presencia_G Response { get; set; }
    }
    public class Response_Dsh_Colg_Filtros
    {
        [JsonProperty("_a")]
        public List<M_Combo> Response { get; set; }
    }
    public class Response_Dsh_Colg_ShareOfShelf
    {
        [JsonProperty("_a")]
        public List<E_Dsh_Colg_ShareOfShelf> Response { get; set; }
    }
    public class Response_Dsh_Colg_Coverage
    {
        [JsonProperty("_a")]
        public List<E_Dsh_Colg_Coverage> Response { get; set; }
    }
    public class Response_Dsh_Colg_Precio
    {
        [JsonProperty("_a")]
        public List<E_Dsh_Colg_Precio> Response { get; set; }
    }
    public class Response_Dsh_Colg_Visibilidad
    {
        [JsonProperty("_a")]
        public List<E_Dsh_Colg_Visibilidad> Response { get; set; }
    }
    public class Response_Dsh_Colg_ShareOfExhibicion
    {
        [JsonProperty("_a")]
        public List<E_Dsh_Colg_ShareOfExhibicion> Response { get; set; }
    }
    public class Response_Dsh_Colg_ShareOfPromocion
    {
        [JsonProperty("_a")]
        public List<E_Dsh_Colg_ShareOfPromocion> Response { get; set; }
    }

    #endregion
    #region << Entidad >>

    public class E_Dsh_Colgate_Presencia_G
    {
        [JsonProperty("_a")]
        public List<E_Dsh_Colg_Presencia> Presencia { get; set; }
        [JsonProperty("_b")]
        public E_Dsh_Colg_Presencia_evo Presencia_Evo { get; set; }
    }
    public class E_Dsh_Colg_Presencia
    {

        [JsonProperty("_a")]
        public string Nom_Canal { get; set; }
        [JsonProperty("_b")]
        public int Cant_Categoria { get; set; }
        [JsonProperty("_c")]
        public int Cant_Total { get; set; }
        [JsonProperty("_d")]
        public int Cant_Porcentaje { get; set; }
        [JsonProperty("_e")]
        public string Ruta_Img { get; set; }
        [JsonProperty("_f")]
        public string Color { get; set; }
    }
    public class E_Dsh_Colg_Presencia_evo
    {

        [JsonProperty("_a")]
        public List<string> Categorias { get; set; }
        [JsonProperty("_b")]
        public List<E_Dsh_Colg_Presencia_evo_item> hijo { get; set; }

    }
    public class E_Dsh_Colg_Presencia_evo_item
    {
        [JsonProperty("_a")]
        public string nom_serie { get; set; }
        [JsonProperty("_b")]
        public List<string> data { get; set; }
        [JsonProperty("_c")]
        public string Color { get; set; }
    }
    public class E_Dsh_Colg_ShareOfShelf
    {
        [JsonProperty("_a")]
        public string Cod_Canal { get; set; }
        [JsonProperty("_b")]
        public string Nom_Canal { get; set; }
        [JsonProperty("_c")]
        public int Cod_Categoria { get; set; }
        [JsonProperty("_d")]
        public string Nom_Categoria { get; set; }
        [JsonProperty("_e")]
        public int Valor_Colgate { get; set; }
        [JsonProperty("_f")]
        public int Valor_Total { get; set; }
        [JsonProperty("_g")]
        public int Porcentaje { get; set; }
    }
    public class E_Dsh_Colg_Coverage
    {
        [JsonProperty("_a")]
        public int Grupo { get; set; }
        [JsonProperty("_b")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("_c")]
        public string Porcentaje { get; set; }
        [JsonProperty("_d")]
        public string Nom_Canal { get; set; }
        [JsonProperty("_e")]
        public string Color { get; set; }
        [JsonProperty("_f")]
        public int Orden { get; set; }
    }
    public class E_Dsh_Colg_Precio
    {
        [JsonProperty("_a")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("_b")]
        public string Nom_Canal { get; set; }
        [JsonProperty("_c")]
        public int Codigo { get; set; }
        [JsonProperty("_d")]
        public string Nombre { get; set; }
        [JsonProperty("_e")]
        public string valor { get; set; }
        [JsonProperty("_f")]
        public int Orden { get; set; }
    }
    public class E_Dsh_Colg_Visibilidad
    {
        [JsonProperty("_a")]
        public int Cod_Mes { get; set; }
        [JsonProperty("_b")]
        public string Nom_Mes { get; set; }
        [JsonProperty("_c")]
        public string Cod_Visibilidad { get; set; }
        [JsonProperty("_d")]
        public string Nom_Visibilidad { get; set; }
        [JsonProperty("_e")]
        public string Valor { get; set; }

    }
    public class E_Dsh_Colg_ShareOfExhibicion
    {
        [JsonProperty("_a")]
        public int Cod_Marca { get; set; }
        [JsonProperty("_b")]
        public string Nom_Marca { get; set; }
        [JsonProperty("_c")]
        public string Porcentaje { get; set; }
        public string Color { get; set; }
    }
    public class E_Dsh_Colg_ShareOfPromocion
    {
        [JsonProperty("_a")]
        public int Cod_Categoria { get; set; }
        [JsonProperty("_b")]
        public string Nom_Categoria { get; set; }
        [JsonProperty("_c")]
        public int Cod_Marca { get; set; }
        [JsonProperty("_d")]
        public string Nom_Marca { get; set; }
        [JsonProperty("_e")]
        public string Porcentaje { get; set; }
        [JsonProperty("_f")]
        public String Color { get; set; }
    }

    #endregion


    #region << RQ Lucky Col-011 v 1.0 >>

    #region Coverage

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-01-24
    /// </summary>
    public class Dashboard_Coverage : A_Dashboard_Coverage
    {
        [JsonProperty("_a")]
        public List<Dash_All_Channel_Coverage> DshCoverageChannel { get; set; }
        [JsonProperty("_b")]
        public List<Dash_Historic_Channel_Coverage> DshCoverageHistoric { get; set; }
    }
    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-01-24
    /// </summary>
    public class Dash_All_Channel_Coverage
    {
        [JsonProperty("_a")]
        public string channel_descripcion { get; set; }
        [JsonProperty("_b")]
        public int valor_objetivo { get; set; }
        [JsonProperty("_c")]
        public int pdv_coberturado { get; set; }
        [JsonProperty("_d")]
        public string channel_cobertura { get; set; }
        [JsonProperty("_e")]
        public string general_cobertura { get; set; }
    }
    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-01-24
    /// </summary>
    public class Dash_Historic_Channel_Coverage
    {
        [JsonProperty("_a")]
        public int channel_id { get; set; }
        [JsonProperty("_b")]
        public int valor_cobertura { get; set; }
        [JsonProperty("_c")]
        public string rpl_descripcion { get; set; }
    }

    public abstract class A_Dashboard_Coverage {
        public Dashboard_Coverage Objeto_Coverage(Request_ColgateDashboard_Parametros oRq)
        {
            Response_ColgateDashboard_Coverage oRp = MvcApplication._Deserialize<Response_ColgateDashboard_Coverage>(
                    MvcApplication._Servicio_Operativa.Dsh_Idicadores_Contrato(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    public class Request_ColgateDashboard_Parametros
    {
        [JsonProperty("_a")]
        public int compania { get; set; }
        [JsonProperty("_b")]
        public int anio { get; set; }
        [JsonProperty("_c")]
        public int mes { get; set; }
        [JsonProperty("_d")]
        public string param1 { get; set; }
        [JsonProperty("_e")]
        public string param2 { get; set; }
        [JsonProperty("_f")]
        public int opcion { get; set; }
    }
    public class Response_ColgateDashboard_Coverage
    {
        [JsonProperty("_a")]
        public Dashboard_Coverage Objeto { get; set; }
    }

    #endregion

    #endregion

    #region Call Value

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-01-25
    /// </summary>
    public class Dashboard_CallValue : A_Dashboard_CallValue
    {
        [JsonProperty("_a")]
        public List<Dash_All_Channel_CallValue> DshCallValueChannel { get; set; }
        [JsonProperty("_b")]
        public List<Dash_Historic_Channel_CallValue> DshCallValueHistoric { get; set; }
    }
    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-01-25
    /// </summary>
    public class Dash_All_Channel_CallValue
    {
        [JsonProperty("_a")]
        public string channel_descripcion { get; set; }
        [JsonProperty("_b")]
        public int pdv_visitados { get; set; }
        [JsonProperty("_c")]
        public int pdv_atendidos { get; set; }
        [JsonProperty("_d")]
        public string channel_callvalue { get; set; }
        [JsonProperty("_e")]
        public string general_callvalue { get; set; }
    }
    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-01-25
    /// </summary>
    public class Dash_Historic_Channel_CallValue
    {
        [JsonProperty("_a")]
        public int channel_id { get; set; }
        [JsonProperty("_b")]
        public int valor_callvalue { get; set; }
        [JsonProperty("_c")]
        public string rpl_descripcion { get; set; }
    }
    public abstract class A_Dashboard_CallValue {
        public Dashboard_CallValue Objeto_CallValue(Request_ColgateDashboard_Parametros oRq)
        {
            Response_ColgateDashboard_CallValue oRp = MvcApplication._Deserialize<Response_ColgateDashboard_CallValue>(
                    MvcApplication._Servicio_Operativa.Dsh_Idicadores_Contrato(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    public class Response_ColgateDashboard_CallValue
    {
        [JsonProperty("_a")]
        public Dashboard_CallValue Objeto { get; set; }
    }

    #endregion


    #endregion

    #region Visibility

    public class Dashboard_visibility : A_Dashboard_visibility
    {
        [JsonProperty("_a")]
        public List<Dash_All_Channel_Visibility> DshVisibilityChannel { get; set; }
        [JsonProperty("_b")]
        public List<Dash_Historic_Channel_Visibility> DshVisibilityHistoric { get; set; }
    }
    public class Dash_All_Channel_Visibility
    {
        [JsonProperty("_a")]
        public int cod_channel { get; set; }
        [JsonProperty("_b")]
        public string descripcion_channel { get; set; }
        [JsonProperty("_c")]
        public string alias { get; set; }
        [JsonProperty("_d")]
        public int objetivo { get; set; }
        [JsonProperty("_e")]
        public int real { get; set; }
        [JsonProperty("_f")]
        public string covertura_general { get; set; }
    }
    public class Dash_Historic_Channel_Visibility
    {
        [JsonProperty("_a")]
        public int cod_channel { get; set; }
        [JsonProperty("_b")]
        public string cod_visibility { get; set; }
        [JsonProperty("_c")]
        public string descripcion_visibility { get; set; }
        [JsonProperty("_d")]
        public int objetivo_visibility { get; set; }
        [JsonProperty("_e")]
        public int real_visibility { get; set; }
    }
    public class A_Dashboard_visibility {
        public Dashboard_visibility Objeto_Visibility(Request_ColgateDashboard_Parametros oRq)
        {
            Response_ColgateDashboard_Visibility oRp = MvcApplication._Deserialize<Response_ColgateDashboard_Visibility>(
                    MvcApplication._Servicio_Operativa.Dsh_Idicadores_Contrato(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }
    public class Response_ColgateDashboard_Visibility
    {
        [JsonProperty("_a")]
        public Dashboard_visibility Objeto { get; set; }
    }

    #endregion

    #region Rotation

    public class Dashboard_Rotation : A_Dashboard_Rotation
    {
        [JsonProperty("_a")]
        public int tiposervicio_id { get; set; }
        [JsonProperty("_b")]
        public string tiposervicio_descripcion { get; set; }
        [JsonProperty("_c")]
        public string locacion { get; set; }
        [JsonProperty("_d")]
        public string valor_rotacion { get; set; }
        [JsonProperty("_e")]
        public string valor_rotaciongeneral { get; set; }
    }
    public abstract class A_Dashboard_Rotation {
        public List<Dashboard_Rotation> Objeto_Rotation(Request_ColgateDashboard_Parametros oRq)
        {
            Response_ColgateDashboard_Rotation oRp = MvcApplication._Deserialize<Response_ColgateDashboard_Rotation>(
                    MvcApplication._Servicio_Operativa.Dsh_Idicadores_Contrato(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.oLs;
        }
    }
    public class Response_ColgateDashboard_Rotation
    {
        [JsonProperty("_a")]
        public List<Dashboard_Rotation> oLs { get; set; }
    }

    #endregion

    #region Compliance

    public class Dashboard_compliancerepotrs : A_Dashboard_compliancerepotrs
    {
        [JsonProperty("_a")]
        public List<Dash_All_Channel_Cumplimiento> DshComplianceChannel { get; set; }
        [JsonProperty("_b")]
        public List<Dash_Detalle_Cumplimiento_Head> DshComplianceHead { get; set; }
        [JsonProperty("_c")]
        public List<Dash_Detalle_Cumplimiento_Body> DshComplianceBody { get; set; }
    }

    public class Dash_All_Channel_Cumplimiento
    {
        [JsonProperty("_a")]
        public int cod_channel { get; set; }
        [JsonProperty("_b")]
        public string valor_cumplimiento { get; set; }
    }

    public class Dash_Detalle_Cumplimiento_Head
    {
        [JsonProperty("_a")]
        public int rpl_identity { get; set; }
        [JsonProperty("_b")]
        public int rpl_id { get; set; }
        [JsonProperty("_c")]
        public string descripcion_rpl { get; set; }
        [JsonProperty("_d")]
        public int llave { get; set; }
    }
    public class Dash_Detalle_Cumplimiento_Body
    {
        [JsonProperty("_a")]
        public int cod_canal { get; set; }
        [JsonProperty("_b")]
        public string descripcion_informe { get; set; }
        [JsonProperty("_c")]
        public string responsable { get; set; }
        [JsonProperty("_d")]
        public string frecuencia { get; set; }
        [JsonProperty("_e")]
        public string dia_entrega { get; set; }
        [JsonProperty("_f")]
        public int medi1_per1 { get; set; }
        [JsonProperty("_g")]
        public int medi2_per1 { get; set; }
        [JsonProperty("_h")]
        public int medi3_per1 { get; set; }
        [JsonProperty("_i")]
        public int medi4_per1 { get; set; }
        [JsonProperty("_j")]
        public int medi1_per2 { get; set; }
        [JsonProperty("_k")]
        public int medi2_per2 { get; set; }
        [JsonProperty("_l")]
        public int medi3_per2 { get; set; }
        [JsonProperty("_m")]
        public int medi4_per2 { get; set; }
        [JsonProperty("_n")]
        public int medi1_per3 { get; set; }
        [JsonProperty("_o")]
        public int medi2_per3 { get; set; }
        [JsonProperty("_p")]
        public int medi3_per3 { get; set; }
        [JsonProperty("_q")]
        public int medi4_per3 { get; set; }
        [JsonProperty("_r")]
        public int medi1_per4 { get; set; }
        [JsonProperty("_s")]
        public int medi2_per4 { get; set; }
        [JsonProperty("_t")]
        public int medi3_per4 { get; set; }
        [JsonProperty("_u")]
        public int medi4_per4 { get; set; }
        [JsonProperty("_v")]
        public int medi1_per5 { get; set; }
        [JsonProperty("_w")]
        public int medi2_per5 { get; set; }
        [JsonProperty("_x")]
        public int medi3_per5 { get; set; }
        [JsonProperty("_y")]
        public int medi4_per5 { get; set; }
        [JsonProperty("_z")]
        public int medi1_per6 { get; set; }
        [JsonProperty("_aa")]
        public int medi2_per6 { get; set; }
        [JsonProperty("_ab")]
        public int medi3_per6 { get; set; }
        [JsonProperty("_ac")]
        public int medi4_per6 { get; set; }
        [JsonProperty("_ad")]
        public int medi1_per7 { get; set; }
        [JsonProperty("_ae")]
        public int medi2_per7 { get; set; }
        [JsonProperty("_af")]
        public int medi3_per7 { get; set; }
        [JsonProperty("_ag")]
        public int medi4_per7 { get; set; }
        [JsonProperty("_ah")]
        public int medi1_per8 { get; set; }
        [JsonProperty("_ai")]
        public int medi2_per8 { get; set; }
        [JsonProperty("_aj")]
        public int medi3_per8 { get; set; }
        [JsonProperty("_ak")]
        public int medi4_per8 { get; set; }
        [JsonProperty("_al")]
        public int medi1_per9 { get; set; }
        [JsonProperty("_am")]
        public int medi2_per9 { get; set; }
        [JsonProperty("_an")]
        public int medi3_per9 { get; set; }
        [JsonProperty("_ao")]
        public int medi4_per9 { get; set; }
        [JsonProperty("_ap")]
        public int medi1_per10 { get; set; }
        [JsonProperty("_aq")]
        public int medi2_per10 { get; set; }
        [JsonProperty("_ar")]
        public int medi3_per10 { get; set; }
        [JsonProperty("_as")]
        public int medi4_per10 { get; set; }
        [JsonProperty("_at")]
        public int medi1_per11 { get; set; }
        [JsonProperty("_au")]
        public int medi2_per11 { get; set; }
        [JsonProperty("_av")]
        public int medi3_per11 { get; set; }
        [JsonProperty("_aw")]
        public int medi4_per11 { get; set; }
        [JsonProperty("_ax")]
        public int medi1_per12 { get; set; }
        [JsonProperty("_ay")]
        public int medi2_per12 { get; set; }
        [JsonProperty("_az")]
        public int medi3_per12 { get; set; }
        [JsonProperty("_ba")]
        public int medi4_per12 { get; set; }

    }

    public abstract class A_Dashboard_compliancerepotrs {
        public Dashboard_compliancerepotrs Objeto_Compliance(Request_ColgateDashboard_Parametros oRq)
        {
            Response_ColgateDashboard_Compliance oRp = MvcApplication._Deserialize<Response_ColgateDashboard_Compliance>(
                    MvcApplication._Servicio_Operativa.Dsh_Idicadores_Contrato(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    public class Response_ColgateDashboard_Compliance
    {
        [JsonProperty("_a")]
        public Dashboard_compliancerepotrs Objeto { get; set; }
    }

    #endregion

    #endregion

}