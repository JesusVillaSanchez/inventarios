﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.ColgateReporting
{
    public class ReporteColgateFusion : AReporteColgateFusion
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string pdv { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string idPeriodo { get; set; }

        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public int cantidad { get; set; }

        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime fecha { get; set; }

        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public string grupo { get; set; }
    }

    public abstract class AReporteColgateFusion
    {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-13
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<ReporteColgateFusion> Lista(Request_Lista_Reporte_Colgate_Fusion_Reports oRq)
        {
            Response_Lista_Reporte_Colgate_Fusion_Reports oRp;

            oRp = MvcApplication._Deserialize<Response_Lista_Reporte_Colgate_Fusion_Reports>(
                MvcApplication._Servicio_Campania.Lista_Reporte_Colgate_Fusion(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 2015-02-13
    /// </summary>
    public class Request_Lista_Reporte_Colgate_Fusion_Reports
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string planning { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string canal { get; set; }

        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string cliente { get; set; }

        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string periodo { get; set; }

        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public int oficina { get; set; }

        [JsonProperty("f", NullValueHandling = NullValueHandling.Ignore)]
        public string sector { get; set; }

        [JsonProperty("g", NullValueHandling = NullValueHandling.Ignore)]
        public string mercado { get; set; }
    }
    public class Response_Lista_Reporte_Colgate_Fusion_Reports
    {
        [JsonProperty("a")]
        public List<ReporteColgateFusion> Lista { get; set; }
    }

    #endregion
}