﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.ColgateReporting
{

    #region << RQ Lucky Col-002 v1 0 >>

    #region << Presence >>

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-10-14
    /// </summary>
    public class Presence_Product : A_ReportingColgate_Presence
    {
        [JsonProperty("_a")]
        public List<Presencia_Comparative_Category> ComparativeCategory { get; set; }
        [JsonProperty("_b")]
        public List<Presencia_Evolutive_Head> EvolutiveHead { get; set; }
        [JsonProperty("_c")]
        public List<Presencia_Evolutive_Body> EvolutiveBody { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-10-14
    /// </summary>
    public class Presencia_Comparative_Category
    {
        [JsonProperty("_a")]
        public string oficina { get; set; }
        [JsonProperty("_b")]
        public string porcentaje { get; set; }
        [JsonProperty("_c")]
        public string pcat_cod { get; set; }
        [JsonProperty("_d")]
        public string pcat_descripcion { get; set; }
        [JsonProperty("_e")]
        public string color { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-10-14
    /// </summary>
    public class Presencia_Evolutive_Head
    {
        [JsonProperty("_a")]
        public int id_row { get; set; }
        [JsonProperty("_b")]
        public int llave { get; set; }
        [JsonProperty("_c")]
        public string cabecera { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fehca: 2015-10-14
    /// </summary>
    public class Presencia_Evolutive_Body
    {
        [JsonProperty("_a")]
        public int id_row { get; set; }
        [JsonProperty("_b")]
        public string sku_descripcion { get; set; }
        [JsonProperty("_c")]
        public string presencia_rpl8 { get; set; }
        [JsonProperty("_d")]
        public string presencia_rpl7 { get; set; }
        [JsonProperty("_e")]
        public string presencia_rpl6 { get; set; }
        [JsonProperty("_f")]
        public string presencia_rpl5 { get; set; }
        [JsonProperty("_g")]
        public string presencia_rpl4 { get; set; }
        [JsonProperty("_h")]
        public string presencia_rpl3 { get; set; }
        [JsonProperty("_i")]
        public string presencia_rpl2 { get; set; }
        [JsonProperty("_j")]
        public string presencia_rpl1 { get; set; }
        [JsonProperty("_k")]
        public string cadena_1 { get; set; }
        [JsonProperty("_l")]
        public string cadena_2 { get; set; }
        [JsonProperty("_m")]
        public string cadena_3 { get; set; }
        [JsonProperty("_n")]
        public string cadena_4 { get; set; }
        [JsonProperty("_o")]
        public string cadena_5 { get; set; }

    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-10-14
    /// </summary>
    public abstract class A_ReportingColgate_Presence 
    {
        public Presence_Product Objeto_Presence(Request_ColgateReporting_Parametros oRq)
        {
            Response_ColgateReporting_Presence oRp = MvcApplication._Deserialize<Response_ColgateReporting_Presence>(
                    MvcApplication._Servicio_Operativa.Colgate_Presence(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }


    #region Request & Response

    public class Request_ColgateReporting_Parametros
    {
        [JsonProperty("_a")]
        public string campania { get; set; }
        [JsonProperty("_b")]
        public int categoria { get; set; }
        [JsonProperty("_c")]
        public int periodo { get; set; }
        [JsonProperty("_d")]
        public int oficina { get; set; }
        [JsonProperty("_e")]
        public int bioficina { get; set; }
        [JsonProperty("_f")]
        public int cadena { get; set; }
        [JsonProperty("_g")]
        public int opcion { get; set; }
    }

    public class Response_ColgateReporting_Presence
    {
        [JsonProperty("_a")]
        public Presence_Product Objeto { get; set; }
    }

    #endregion

    #endregion

    #region << Placement >>

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-12-01
    /// </summary>
    public class Placement_SOS : A_Placement_SOS
    {
        [JsonProperty("_a")]
        public List<PlacementCadena_Propio_Vs_Competencia> ComparativeCPOthers { get; set; }
        [JsonProperty("_b")]
        public List<Placement_SOS_Company_Head> SosCompanyHead { get; set; }
        [JsonProperty("_c")]
        public List<Placement_SOS_Company_Body> SosCompanyBody { get; set; }
        [JsonProperty("_d")]
        public List<Placement_SOS_NodeCommercial_Pdv> SosNodeCommercialPdv { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-12-01
    /// </summary>
    public class PlacementCadena_Propio_Vs_Competencia
    {
        //[JsonProperty("_a")]
        //public string cadena_descripcion { get; set; }
        //[JsonProperty("_b")]
        //public string valor_cp { get; set; }
        //[JsonProperty("_c")]
        //public string valor_others { get; set; }

        [JsonProperty("_a")]
        public string company_descripcion { get; set; }
        [JsonProperty("_b")]
        public string rg_color { get; set; }
        [JsonProperty("_c")]
        public string cadena_descripcion { get; set; }
        [JsonProperty("_d")]
        public string valor_metros { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-12-01
    /// </summary>
    public class Placement_SOS_Company_Head
    {
        [JsonProperty("_a")]
        public int soshead_id { get; set; }
        [JsonProperty("_b")]
        public int periodo_id { get; set; }
        [JsonProperty("_c")]
        public string periodo_descripcion { get; set; }
        [JsonProperty("_d")]
        public int llave { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-12-01
    /// </summary>
    public class Placement_SOS_Company_Body
    {
        [JsonProperty("_a")]
        public int sosbody_id { get; set; }
        [JsonProperty("_b")]
        public string company_descripcion { get; set; }
        [JsonProperty("_c")]
        public string placement_rpl8 { get; set; }
        [JsonProperty("_d")]
        public string placement_rpl7 { get; set; }
        [JsonProperty("_e")]
        public string placement_rpl6 { get; set; }
        [JsonProperty("_f")]
        public string placement_rpl5 { get; set; }
        [JsonProperty("_g")]
        public string placement_rpl4 { get; set; }
        [JsonProperty("_h")]
        public string placement_rpl3 { get; set; }
        [JsonProperty("_i")]
        public string placement_rpl2 { get; set; }
        [JsonProperty("_j")]
        public string placement_rpl1 { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-02-26
    /// </summary>
    public class Placement_SOS_NodeCommercial_Pdv
    {
        [JsonProperty("_a")]
        public int cadena_id { get; set; }
        [JsonProperty("_b")]
        public string cadena_descripcion { get; set; }
        [JsonProperty("_c")]
        public int pdv_count { get; set; }
    }

    public abstract class A_Placement_SOS
    {
        public Placement_SOS Objeto_Placement(Request_ColgateReporting_Parametros oRq)
        {
            Response_ColgateReporting_Placement oRp = MvcApplication._Deserialize<Response_ColgateReporting_Placement>(
                    MvcApplication._Servicio_Operativa.Colgate_Presence(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.ObjetoPlacement;
        }
    }

    #region Request & Response

    public class Response_ColgateReporting_Placement
    {
        [JsonProperty("_a")]
        public Placement_SOS ObjetoPlacement { get; set; }
    }

    #endregion


    #endregion

    #region << Promotion >>

    /// <summary>
    /// Autor: yrodriguez
    /// Fecha: 2015-12-09
    /// </summary>
    public class Promotion_Impulso : A_Promotion_Impulso
    {
        [JsonProperty("_a")]
        public List<Promotion_ICompany_Head> ComparativePushGirl { get; set; }
        [JsonProperty("_b")]
        public List<Promotion_ICompany_Real> ImpulsoCompanyReal { get; set; }
        [JsonProperty("_c")]
        public List<Promotion_ICompany_Sob> ImpulsoCompanySOB { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-12-09
    /// </summary>
    public class Promotion_ICompany_Head
    {
        [JsonProperty("_a")]
        public int impulsohead_id { get; set; }
        [JsonProperty("_b")]
        //public int periodo_id { get; set; }
        public string periodo_id { get; set; }
        [JsonProperty("_c")]
        public string periodo_descripcion { get; set; }
        [JsonProperty("_d")]
        public int llave { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-12-09
    /// </summary>
    public class Promotion_ICompany_Real
    {
        [JsonProperty("_a")]
        public string color_real { get; set; }
        [JsonProperty("_b")]
        public string company_descripcion { get; set; }
        [JsonProperty("_c")]
        public string promotion_rpl8 { get; set; }
        [JsonProperty("_d")]
        public string promotion_rpl7 { get; set; }
        [JsonProperty("_e")]
        public string promotion_rpl6 { get; set; }
        [JsonProperty("_f")]
        public string promotion_rpl5 { get; set; }
        [JsonProperty("_g")]
        public string promotion_rpl4 { get; set; }
        [JsonProperty("_h")]
        public string promotion_rpl3 { get; set; }
        [JsonProperty("_i")]
        public string promotion_rpl2 { get; set; }
        [JsonProperty("_j")]
        public string promotion_rpl1 { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-12-09
    /// </summary>
    public class Promotion_ICompany_Sob
    {
        [JsonProperty("_a")]
        public string company_descripcion { get; set; }
        [JsonProperty("_b")]
        public string promotion_rpl8 { get; set; }
        [JsonProperty("_c")]
        public string promotion_rpl7 { get; set; }
        [JsonProperty("_d")]
        public string promotion_rpl6 { get; set; }
        [JsonProperty("_e")]
        public string promotion_rpl5 { get; set; }
        [JsonProperty("_f")]
        public string promotion_rpl4 { get; set; }
        [JsonProperty("_g")]
        public string promotion_rpl3 { get; set; }
        [JsonProperty("_h")]
        public string promotion_rpl2 { get; set; }
        [JsonProperty("_i")]
        public string promotion_rpl1 { get; set; }
    }

    public abstract class A_Promotion_Impulso {
        public Promotion_Impulso Objeto_Promotion(Request_ColgateReporting_Parametros oRq)
        {
            Response_ColgateReporting_Promotion oRp = MvcApplication._Deserialize<Response_ColgateReporting_Promotion>(
                    MvcApplication._Servicio_Operativa.Colgate_Presence(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.ObjetoPromotion;
        }
    }

    #region Request & Response

    public class Response_ColgateReporting_Promotion
    {
        [JsonProperty("_a")]
        public Promotion_Impulso ObjetoPromotion { get; set; }
    }

    #endregion

    #endregion

    #region Price

    /// <summary>
    /// Autor: yrodriguez
    /// Fecha: 2015-12-14
    /// </summary>
    public class Price_Precio_Grilla : A_Price_Precio_Grilla
    {
        [JsonProperty("_a")]
        public List<Price_Colgate_Head> PriceCabecera { get; set; }
        [JsonProperty("_b")]
        public List<Price_Colgate_Body> PriceCuerpo { get; set; }
    }

    public class Price_Precio_GrafEvo : A_Price_Precio_GrafEvo
    {
        [JsonProperty("_a")]
        public string rpl_descripcion { get; set; }
        [JsonProperty("_b")]
        public string sku_descripcion { get; set; }
        [JsonProperty("_c")]
        public string value_precio { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-12-14
    /// </summary>
    public class Price_Colgate_Head
    {
        [JsonProperty("_a")]
        public int periodo_id { get; set; }
        [JsonProperty("_b")]
        public string periodo_descripcion { get; set; }
        [JsonProperty("_c")]
        public int llave { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-12-14
    /// </summary>
    public class Price_Colgate_Body
    {
        [JsonProperty("_a")]
        public string company_descripcion { get; set; }
        [JsonProperty("_b")]
        public string product_cod { get; set; }
        [JsonProperty("_c")]
        public string product_descripcion { get; set; }
        [JsonProperty("_d")]
        public string tamanio { get; set; }
        [JsonProperty("_e")]
        public string price_pregular6 { get; set; }
        [JsonProperty("_f")]
        public string price_poferta6 { get; set; }
        [JsonProperty("_g")]
        public string price_pregular5 { get; set; }
        [JsonProperty("_h")]
        public string price_poferta5 { get; set; }
        [JsonProperty("_i")]
        public string price_pregular4 { get; set; }
        [JsonProperty("_j")]
        public string price_poferta4 { get; set; }
        [JsonProperty("_k")]
        public string price_pregular3 { get; set; }
        [JsonProperty("_l")]
        public string price_poferta3 { get; set; }
        [JsonProperty("_m")]
        public string price_pregular2 { get; set; }
        [JsonProperty("_n")]
        public string price_poferta2 { get; set; }
        [JsonProperty("_ñ")]
        public string price_pregular1 { get; set; }
        [JsonProperty("_o")]
        public string price_poferta1 { get; set; }
        [JsonProperty("_p")]
        public string price_cadena1 { get; set; }
        [JsonProperty("_q")]
        public string price_cadena2 { get; set; }
        [JsonProperty("_r")]
        public string price_cadena3 { get; set; }
        [JsonProperty("_s")]
        public string price_cadena4 { get; set; }
        [JsonProperty("_t")]
        public string family_descripcion { get; set; }

    }

    public abstract class A_Price_Precio_Grilla {
        public Price_Precio_Grilla Objeto_Price(Request_ColgateReporting_Parametros oRq)
        {
            Response_ColgateReporting_Price oRp = MvcApplication._Deserialize<Response_ColgateReporting_Price>(
                    MvcApplication._Servicio_Operativa.Colgate_Presence(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.ObjetoPrice;
        }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-01-11
    /// </summary>
    public abstract class A_Price_Precio_GrafEvo {
        public List<Price_Precio_GrafEvo> Objeto_EvolutivePrice(Request_ColgateEvolutivePrice oRq)
        {
            Response_ColgateEvolutivePrice oRp;
            oRp = MvcApplication._Deserialize<Response_ColgateEvolutivePrice>(
                    MvcApplication._Servicio_Operativa.BL_Colgate_PriceEvolutivo(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    public class Response_ColgateReporting_Price
    {
        [JsonProperty("_a")]
        public Price_Precio_Grilla ObjetoPrice { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-01-11
    /// </summary>
    public class Request_ColgateEvolutivePrice
    {
        [JsonProperty("_a")]
        public string campania { get; set; }
        [JsonProperty("_b")]
        public int categoria { get; set; }
        [JsonProperty("_c")]
        public int periodo { get; set; }
        [JsonProperty("_d")]
        public int oficina { get; set; }
        [JsonProperty("_e")]
        public int bioficina { get; set; }
        [JsonProperty("_f")]
        public int cadena { get; set; }
        [JsonProperty("_g")]
        public string sku { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-01-11
    /// </summary>
    public class Response_ColgateEvolutivePrice
    {
        [JsonProperty("a")]
        public List<Price_Precio_GrafEvo> Lista { get; set; }
    }

    #endregion


    #endregion

    #region Exportss

    #region Presence

    public class Presence_Company_Table : A_Presence_Company_Table
    {
        [JsonProperty("_a")]
        public string rpl_descripcion { get; set; }
        [JsonProperty("_b")]
        public string city { get; set; }
        [JsonProperty("_c")]
        public string nodecommercial { get; set; }
        [JsonProperty("_d")]
        public string pdv_Name { get; set; }
        [JsonProperty("_e")]
        public string subcategory { get; set; }
        [JsonProperty("_f")]
        public string company { get; set; }
        [JsonProperty("_g")]
        public string product { get; set; }
        [JsonProperty("_h")]
        public string description { get; set; }
    }

    public abstract class A_Presence_Company_Table
    {
        public List<Presence_Company_Table> Objeto_ExportPresence(Request_ColgateReporting_Parametros oRq)
        {
            Response_Export_Presence oRp = MvcApplication._Deserialize<Response_Export_Presence>(
                    MvcApplication._Servicio_Operativa.Colgate_Export(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    public class Response_Export_Presence
    {
        [JsonProperty("_a")]
        public List<Presence_Company_Table> Objeto { get; set; }
    }

    #endregion

    #endregion

    #region Placement

    public class Placement_SOS_Company_Table : A_Placement_Export
    {
        [JsonProperty("_a")]
        public string rpl_id { get; set; }
        [JsonProperty("_b")]
        public string ncm_descripcion { get; set; }
        [JsonProperty("_c")]
        public string depart_descripcion { get; set; }
        [JsonProperty("_d")]
        public string coty_descripcion { get; set; }
        [JsonProperty("_e")]
        public string pdv_name { get; set; }
        [JsonProperty("_f")]
        public string subcategoria { get; set; }
        [JsonProperty("_g")]
        public string categoria { get; set; }
        [JsonProperty("_h")]
        public string company_descripcion { get; set; }
        [JsonProperty("_i")]
        public string metros { get; set; }
    }

    public abstract class A_Placement_Export
    {
        public List<Placement_SOS_Company_Table> Objeto_ExportPlacement(Request_ColgateReporting_Parametros oRq)
        {
            Response_Export_Placement oRp = MvcApplication._Deserialize<Response_Export_Placement>(
                    MvcApplication._Servicio_Operativa.Colgate_Export(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    public class Response_Export_Placement
    {
        [JsonProperty("_a")]
        public List<Placement_SOS_Company_Table> Objeto { get; set; }
    }

    #endregion

    #endregion
    
    #region Promotion

    public class PushGirl_Company_Table : A_PushGirl_Company_Table
    {
        [JsonProperty("_a")]
        public string rpl_descripcion { get; set; }
        [JsonProperty("_b")]
        public string city { get; set; }
        [JsonProperty("_c")]
        public string nodecommercial { get; set; }
        [JsonProperty("_d")]
        public string pdv_name { get; set; }
        [JsonProperty("_e")]
        public string company { get; set; }
        [JsonProperty("_f")]
        public string category { get; set; }
        [JsonProperty("_g")]
        public string subcategory { get; set; }
        [JsonProperty("_h")]
        public string real { get; set; }
        [JsonProperty("_i")]
        public string sob { get; set; }
    }

    public abstract class A_PushGirl_Company_Table {
        public List<PushGirl_Company_Table> Objeto_ExportPromotion(Request_ColgateReporting_Parametros oRq)
        {
            Response_Export_Promotion oRp = MvcApplication._Deserialize<Response_Export_Promotion>(
                    MvcApplication._Servicio_Operativa.Colgate_Export(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    public class Response_Export_Promotion
    {
        [JsonProperty("_a")]
        public List<PushGirl_Company_Table> Objeto { get; set; }
    }

    #endregion

    #endregion

    #region POP

    public class POP_Company_Table : A_POP_Company_Table
    {
        [JsonProperty("_a")]
        public string rpl_descripcion { get; set; }
        [JsonProperty("_b")]
        public string department { get; set; }
        [JsonProperty("_c")]
        public string city { get; set; }
        [JsonProperty("_d")]
        public string nodecommercial { get; set; }
        [JsonProperty("_e")]
        public string pdv_Name { get; set; }
        [JsonProperty("_f")]
        public string category { get; set; }
        [JsonProperty("_g")]
        public string subcategory { get; set; }
        [JsonProperty("_h")]
        public string sob { get; set; }
        [JsonProperty("_i")]
        public string company { get; set; }
        [JsonProperty("_j")]
        public string typeelement { get; set; }
        [JsonProperty("_k")]
        public string element { get; set; }
        [JsonProperty("_l")]
        public string cantidad { get; set; }
    }

    public abstract class A_POP_Company_Table {
        public List<POP_Company_Table> Objeto_ExportPOP(Request_ColgateReporting_Parametros oRq)
        {
            Response_Export_POP oRp = MvcApplication._Deserialize<Response_Export_POP>(
                    MvcApplication._Servicio_Operativa.Colgate_Export(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    public class Response_Export_POP
    {
        [JsonProperty("_a")]
        public List<POP_Company_Table> Objeto { get; set; }
    }

    #endregion

    #endregion

    #region Price

    public class Price_Company_Table : A_Price_Company_Table
    {
        [JsonProperty("_a")]
        public string rpl_descripcion { get; set; }
        [JsonProperty("_b")]
        public string city { get; set; }
        [JsonProperty("_c")]
        public string nodecommercial { get; set; }
        [JsonProperty("_d")]
        public string pdv_name { get; set; }
        [JsonProperty("_e")]
        public string category { get; set; }
        [JsonProperty("_f")]
        public string subcategory { get; set; }
        [JsonProperty("_g")]
        public string company { get; set; }
        [JsonProperty("_h")]
        public string brand { get; set; }
        [JsonProperty("_i")]
        public string product_cod { get; set; }
        [JsonProperty("_j")]
        public string product { get; set; }
        [JsonProperty("_k")]
        public string precio_regular { get; set; }
        [JsonProperty("_l")]
        public string precio_oferta { get; set; }
        [JsonProperty("_m")]
        public string tipooferta_cod { get; set; }
        [JsonProperty("_n")]
        public string tipooferta { get; set; }
        [JsonProperty("_o")]
        public string fecha_inicio { get; set; }
        [JsonProperty("_p")]
        public string fecha_fin { get; set; }
        [JsonProperty("_q")]
        public string mecanica { get; set; }
        [JsonProperty("_r")]
        public string observacion { get; set; }
    }

    public abstract class A_Price_Company_Table {
        public List<Price_Company_Table> Objeto_ExportPrice(Request_ColgateReporting_Parametros oRq)
        {
            Response_Export_Price oRp = MvcApplication._Deserialize<Response_Export_Price>(
                    MvcApplication._Servicio_Operativa.Colgate_Export(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    public class Response_Export_Price
    {
        [JsonProperty("_a")]
        public List<Price_Company_Table> Objeto { get; set; }
    }

    #endregion

    #endregion

    #endregion

    #region POP

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-03-09
    /// </summary>
    public class POP_Element_GrafEvo : A_POP_Element_GrafEvo
    {
        [JsonProperty("_a")]
        public List<POP_Head_GrafEvo> POP_Element_Head_GrafEvo { get; set; }
        [JsonProperty("_b")]
        public List<POP_Body_GrafEvo> POP_Element_Body_GrafEvo { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-03-09
    /// </summary>
    public class POP_Head_GrafEvo
    {
        [JsonProperty("_a")]
        public int rpl_id { get; set; }
        [JsonProperty("_b")]
        public string rpl_descripcion { get; set; }
        [JsonProperty("_c")]
        public int llave { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-03-09
    /// </summary>
    public class POP_Body_GrafEvo
    {
        [JsonProperty("_a")]
        public int mpop_id { get; set; }
        [JsonProperty("_b")]
        public string mpop_name { get; set; }
        [JsonProperty("_c")]                //YR 20160310
        public string color { get; set; }   //YR 20160310
        [JsonProperty("_d")]
        public string company_name { get; set; }
        [JsonProperty("_e")]
        public string element_rpl8 { get; set; }
        [JsonProperty("_f")]
        public string element_rpl7 { get; set; }
        [JsonProperty("_g")]
        public string element_rpl6 { get; set; }
        [JsonProperty("_h")]
        public string element_rpl5 { get; set; }
        [JsonProperty("_i")]
        public string element_rpl4 { get; set; }
        [JsonProperty("_j")]
        public string element_rpl3 { get; set; }
        [JsonProperty("_k")]
        public string element_rpl2 { get; set; }
        [JsonProperty("_l")]
        public string element_rpl1 { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-03-09
    /// </summary>
    public abstract class A_POP_Element_GrafEvo {
        public POP_Element_GrafEvo Objeto_EvolutivePop(Request_ColgateEvolutivePOP oRq)
        {
            Response_ColgateEvolutivePOP oRp;
            oRp = MvcApplication._Deserialize<Response_ColgateEvolutivePOP>(
                    MvcApplication._Servicio_Operativa.BL_Colgate_PopEvolutivo(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-03-09
    /// </summary>
    public class Request_ColgateEvolutivePOP
    {
        [JsonProperty("_a")]
        public string campania { get; set; }
        [JsonProperty("_b")]
        public int categoria { get; set; }
        [JsonProperty("_c")]
        public int periodo { get; set; }
        [JsonProperty("_d")]
        public string elements { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-03-09
    /// </summary>
    public class Response_ColgateEvolutivePOP
    {
        [JsonProperty("a")]
        public POP_Element_GrafEvo Lista { get; set; }
    }


    #endregion

    #endregion

    #region << Pharmacies >>

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-03-21
    /// </summary>
    public class Price_Precio_Pharmacies_Grilla : A_Price_Precio_Pharmacies_Grilla
    {
        [JsonProperty("_a")]
        public List<Price_Pharmacies_Head> PriceCabecera { get; set; }
        [JsonProperty("_b")]
        public List<Price_Pharmacies_Body> PriceCuerpo { get; set; }
    }
    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-03-21
    /// </summary>
    public class Price_Pharmacies_Head
    {
        [JsonProperty("_a")]
        public int periodo_id { get; set; }
        [JsonProperty("_b")]
        public string periodo_descripcion { get; set; }
        [JsonProperty("_c")]
        public int llave { get; set; }
    }
    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2016-03-21
    /// </summary>
    public class Price_Pharmacies_Body
    {
        /*
        [JsonProperty("_a")]
        public string grupo { get; set; }
        [JsonProperty("_b")]
        public string brand { get; set; }
        [JsonProperty("_c")]
        public string product_cod { get; set; }
        [JsonProperty("_d")]
        public string product_descripcion { get; set; }
        [JsonProperty("_e")]
        public string suggested { get; set; }
        [JsonProperty("_f")]
        public string price_pregular8 { get; set; }
        [JsonProperty("_g")]
        public string price_pregular7 { get; set; }
        [JsonProperty("_h")]
        public string price_pregular6 { get; set; }
        [JsonProperty("_i")]
        public string price_pregular5 { get; set; }
        [JsonProperty("_j")]
        public string price_pregular4 { get; set; }
        [JsonProperty("_k")]
        public string price_pregular3 { get; set; }
        [JsonProperty("_l")]
        public string price_pregular2 { get; set; }
        [JsonProperty("_m")]
        public string price_pregular1 { get; set; }
        */
        [JsonProperty("_a")]
        public string price_group { get; set; }
        [JsonProperty("_b")]
        public string brand_description { get; set; }
        [JsonProperty("_c")]
        public string product_cod { get; set; }
        [JsonProperty("_d")]
        public string product_description { get; set; }
        [JsonProperty("_e")]
        public string price_suggested { get; set; }
        [JsonProperty("_f")]
        public string price_pregular6 { get; set; }
        [JsonProperty("_g")]
        public string price_poferta6 { get; set; }
        [JsonProperty("_h")]
        public string price_pregular5 { get; set; }
        [JsonProperty("_i")]
        public string price_poferta5 { get; set; }
        [JsonProperty("_j")]
        public string price_pregular4 { get; set; }
        [JsonProperty("_k")]
        public string price_poferta4 { get; set; }
        [JsonProperty("_l")]
        public string price_pregular3 { get; set; }
        [JsonProperty("_m")]
        public string price_poferta3 { get; set; }
        [JsonProperty("_n")]
        public string price_pregular2 { get; set; }
        [JsonProperty("_ñ")]
        public string price_poferta2 { get; set; }
        [JsonProperty("_o")]
        public string price_pregular1 { get; set; }
        [JsonProperty("_p")]
        public string price_poferta1 { get; set; }
        [JsonProperty("_q")]
        public string price_cadena1 { get; set; }
        [JsonProperty("_r")]
        public string price_cadena2 { get; set; }
        [JsonProperty("_s")]
        public string price_cadena3 { get; set; }
        [JsonProperty("_t")]
        public string price_cadena4 { get; set; }
        [JsonProperty("_u")]
        public string price_cadena5 { get; set; }
    }

    public abstract class A_Price_Precio_Pharmacies_Grilla
    {
        public Price_Precio_Pharmacies_Grilla Objeto_PharmaciesPrice(Request_ColgateReporting_Parametros oRq)
        {
            Response_ColgatePharmacies_Price oRp;
            oRp = MvcApplication._Deserialize<Response_ColgatePharmacies_Price>(
                    MvcApplication._Servicio_Operativa.Colgate_Presence(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
        }
    }

    #region Request & Response

    public class Response_ColgatePharmacies_Price
    {
        [JsonProperty("_a")]
        public Price_Precio_Pharmacies_Grilla Objeto { get; set; }
    }

    #endregion

    #endregion

    #endregion
    
}