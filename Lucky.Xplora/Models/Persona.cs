﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Lucky.Xplora.DataAccess;
using Lucky.Xplora.Models.GestionDocumentalWeb;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Persona : APersona
    {

        [JsonProperty("IdUsuario", NullValueHandling = NullValueHandling.Ignore)]
        public long Person_idUsuario2 { get; set; }

        [JsonProperty("IdUsuario", NullValueHandling = NullValueHandling.Ignore)]
        public int Person_idUsuario { get; set; }

        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public int Person_id { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string id_typeDocument { get; set; }

        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_nd { get; set; }

        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Firtsname { get; set; }

        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_LastName { get; set; }

        [JsonProperty("f", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Surname { get; set; }

        [JsonProperty("g", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_SeconName { get; set; }

        [JsonProperty("h", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Email { get; set; }

        [JsonProperty("i", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Phone { get; set; }

        [JsonProperty("j", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Addres { get; set; }

        [JsonProperty("k", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_Country { get; set; }

        [JsonProperty("l", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_dpto { get; set; }

        [JsonProperty("m", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_city { get; set; }

        [JsonProperty("n", NullValueHandling = NullValueHandling.Ignore)]
        [Required(ErrorMessage="Ingrese un usuario", AllowEmptyStrings=false)]
        public string name_user { get; set; }

        [JsonProperty("o", NullValueHandling = NullValueHandling.Ignore)]
        [Required(ErrorMessage = "Ingrese una contraseña", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        public string User_Password { get; set; }

        [JsonProperty("p", NullValueHandling = NullValueHandling.Ignore)]
        public int Perfil_id { get; set; }

        [JsonProperty("q", NullValueHandling = NullValueHandling.Ignore)]
        public string Modulo_id { get; set; }

        [JsonProperty("r", NullValueHandling = NullValueHandling.Ignore)]
        public string User_Recall { get; set; }

        [JsonProperty("s", NullValueHandling = NullValueHandling.Ignore)]
        public int Company_id { get; set; }

        [JsonProperty("t", NullValueHandling = NullValueHandling.Ignore)]
        public bool Person_Status { get; set; }

        [JsonProperty("u", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_CreateBy { get; set; }

        [JsonProperty("v", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Person_DateBy { get; set; }

        [JsonProperty("w", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_ModiBy { get; set; }

        [JsonProperty("x", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Person_DateModiBy { get; set; }

        [JsonProperty("y", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Person_ExpirationDate { get; set; }

        [JsonProperty("z", NullValueHandling = NullValueHandling.Ignore)]
        public bool Person_aceptoCLUF { get; set; }

        //-- estado de login
        [JsonProperty("aa", NullValueHandling = NullValueHandling.Ignore)]
        public bool existe { get; set; }

        [JsonProperty("ab", NullValueHandling = NullValueHandling.Ignore)]
        public string company_name { get; set; }
        //------------------

        [JsonProperty("ac", NullValueHandling = NullValueHandling.Ignore)]
        public string per_nombrecompleto { get; set; }

        [JsonProperty("ad")]
        public string Cod_Equipo { get; set; }

        [JsonProperty("ae")]
        public string Cod_Canal { get; set; }

        //-----------------------------------------
        // Tipo de perfil
        [JsonProperty("_af")]
        public int tpf_id { get; set; }

        [JsonProperty("_ag")]
        public string tpf_descripcion { get; set; }

        [JsonProperty("_ah")]
        public string user_lat { get; set; }
        [JsonProperty("_ai")]
        public string user_lon { get; set; }

        [JsonProperty("_aj")]
        public List<E_NW_Ctrl_Aceeso_Proyecto> Accesos { get; set; }

        [JsonProperty("_ak", NullValueHandling = NullValueHandling.Ignore)]
        [Required(ErrorMessage = "Ingrese nueva contraseña", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        public string User_Password_Renew { get; set; }

        [JsonProperty("_al")]
        public string prf_descripcion { get; set; }

        [JsonProperty("_am")]
        public string ofi_id { get; set; }
    }

    public abstract class APersona {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-04-10
        /// </summary>
        /// <param name="usuario">usuario</param>
        /// <param name="contrasenia">contraseña</param>
        /// <returns></returns>
        public Persona Login(Persona_Login_Request a)
        {
            var oRp = new Persona_Login_Response();

            a.key_encrypt = "YourUglyRandomKeyLike-lkj54923c478";

            oRp = MvcApplication._Deserialize<Persona_Login_Response>(
                MvcApplication._Servicio_Operativa.Person_Login(MvcApplication._Serialize(a))
            );

            return oRp.Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-17
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Persona> Supervisor(Request_GetSupervisor_Campania_Departamento_Provincia_Distrito oRq) {
            Response_GetSupervisor_Campania_Departamento_Provincia_Distrito oRp;

            oRp = MvcApplication._Deserialize<Response_GetSupervisor_Campania_Departamento_Provincia_Distrito>(
                    MvcApplication._Servicio_Campania.GetSupervisor_Campania_Departamento_Provincia_Distrito(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-17
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Persona> Generador(Request_GetGenerador_Campania_Oficina_Departamento_Supervisor oRq)
        {
            Response_GetGenerador_Campania_Oficina_Departamento_Supervisor oRp;

            oRp = MvcApplication._Deserialize<Response_GetGenerador_Campania_Oficina_Departamento_Supervisor>(
                    MvcApplication._Servicio_Campania.GetGenerador_Campania_Oficina_Departamento_Supervisor(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Persona> Lista(MapMultiGPS_Request oRq)
        {
            List<Persona> oLs = new List<Persona>();
            MapMultiGPS_Filtro_Response oRp;

            oRp = MvcApplication._Deserialize<MapMultiGPS_Filtro_Response>(
                    MvcApplication._Servicio_Maps.Consul_Filtro_MultiGPS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            /*obtiene resultado y lo llena en listado de campanias*/
            if (oRp.Lista != null && oRp.Lista.Count > 0)
            {
                foreach (M_Combo oBj in oRp.Lista)
                {
                    oLs.Add(new Persona()
                    {
                        Person_id = Convert.ToInt32(oBj.Value),
                        per_nombrecompleto = oBj.Text
                    });
                }
            }

            return oLs;
        }

        /// <summary>
        /// Autor: jsulla
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public Person_Bloqueo Login_Bloqueo(Persona_Login_Request a)
        {
            var oRp = new Response_Person_Bloqueo();

            oRp = MvcApplication._Deserialize<Response_Person_Bloqueo>(
                MvcApplication._Servicio_Operativa.NW_Person_Bloqueo(MvcApplication._Serialize(a))
            );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jvilla
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public Persona Login_new(Persona_Login_Request a)
        {
            var oRp = new Persona();
            var user = new PersonaUsuario();
            //UsuarioDA usuarioDA = new UsuarioDA();
            user = UsuarioDA.ObtenerPersona(a.name_user, a.user_password);
            if (user == null)
            {
                oRp.existe = false;
                return oRp;
            }
            
            oRp.existe = true;
            oRp.Person_Status = true;
            oRp.per_nombrecompleto = user.Nombre + " " + user.ApellidoPaterno + " " + user.ApellidoMaterno;
            oRp.Person_Surname = user.Nombre + " " + user.ApellidoPaterno + " " + user.ApellidoMaterno;
            oRp.name_user = a.name_user;
            oRp.Perfil_id = user.IdRol;
            oRp.prf_descripcion = user.Rol;
            oRp.Person_idUsuario2 = user.IdUsuario;
            return oRp;
        }

        /// <summary>
        /// Autor: jsulla
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public int Cambiar_contrasena(Persona_Cambiar_Contrasena_Request a)
        {
            var oRp = new Persona_Cambiar_Contrasena_Response();

            oRp = MvcApplication._Deserialize<Persona_Cambiar_Contrasena_Response>(
                MvcApplication._Servicio_Operativa.Person_Cambiar_Contrasena(MvcApplication._Serialize(a))
            );

            return oRp.resultado;
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 18-06-2015
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Persona> ListarSupervisores(Request_GetSupervisor_Campania oRq)
        {
            Response_GetSupervisor_Campania oRp;

            oRp = MvcApplication._Deserialize<Response_GetSupervisor_Campania>(
                    MvcApplication._Servicio_Campania.Listar_Supervisor_Por_CodCampania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public int EnvioContraseña(PersonaParametro oRq) {
            return MvcApplication._Deserialize<PersonaLoginEnviaContraseña>(
                    MvcApplication._Servicio_Operativa.PersonaLogin(
                        MvcApplication._Serialize(oRq)
                    )
                ).resultado;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Persona Persona(PersonaParametro oRq) {
            return MvcApplication._Deserialize<PersonaLogin>(
                    MvcApplication._Servicio_Operativa.PersonaLogin(
                        MvcApplication._Serialize(oRq)
                    )
                ).objeto;
        }
    }

    #region Request y Response

    #region¨Login - Persona, por name_user, user_password, key_encrypt

    /// <summary>
    /// Autor:  jlucero
    /// Fecha:  2014-04-10
    /// </summary>
    public class Persona_Login_Request
    {
        [JsonProperty("a")]
        public string name_user { get; set; }

        [JsonProperty("b")]
        public string user_password { get; set; }

        [JsonProperty("c")]
        public string key_encrypt { get; set; }


        [JsonProperty("d")]
        public string user_ip { get; set; }

        [JsonProperty("e")]
        public string user_maquina { get; set; }

        [JsonProperty("f")]
        public string user_lat { get; set; }

        [JsonProperty("g")]
        public string user_lon { get; set; }
    }

    public class Persona_Cambiar_Contrasena_Request
    {
        [JsonProperty("a")]
        public int id_user { get; set; }

        [JsonProperty("b")]
        public string user_password { get; set; }

        [JsonProperty("c")]
        public string key_encrypt { get; set; }
    }

    /// <summary>
    /// Autor:  jlucero
    /// Fecha:  2014-04-10
    /// </summary>
    public class Persona_Login_Response
    {
        [JsonProperty("a")]
        public Persona Objeto { get; set; }
    }

    public class Persona_Cambiar_Contrasena_Response
    {
        [JsonProperty("a")]
        public int resultado { get; set; }
    }

    #endregion

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-17
    /// </summary>
    public class Request_GetSupervisor_Campania_Departamento_Provincia_Distrito
    {
        [JsonProperty("a")]
        public string campania { get; set; }

        [JsonProperty("b")]
        public string departamento { get; set; }

        [JsonProperty("c")]
        public string provincia { get; set; }

        [JsonProperty("d")]
        public string distrito { get; set; }
    }
    public class Response_GetSupervisor_Campania_Departamento_Provincia_Distrito
    {
        [JsonProperty("a")]
        public List<Persona> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-17
    /// </summary>
    public class Request_GetGenerador_Campania_Oficina_Departamento_Supervisor
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int oficina { get; set; }

        [JsonProperty("_c")]
        public string departamento { get; set; }

        [JsonProperty("_d")]
        public int supervisor { get; set; }
    }
    public class Response_GetGenerador_Campania_Oficina_Departamento_Supervisor
    {
        [JsonProperty("_a")]
        public List<Persona> Lista { get; set; }
    }

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 18-06-2015
    /// </summary>
    public class Request_GetSupervisor_Campania
    {
        [JsonProperty("a")]
        public string campania { get; set; }
    }
    public class Response_GetSupervisor_Campania
    {
        [JsonProperty("a")]
        public List<Persona> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jsulla
    /// </summary>
    public class Person_Bloqueo
    {
        [JsonProperty("a")]
        public int Estado { get; set; }
        [JsonProperty("b")]
        public string Comentario { get; set; }
    }
    public class Response_Person_Bloqueo
    {
        [JsonProperty("_a")]
        public Person_Bloqueo Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-06-23
    /// </summary>
    public class PersonaParametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }
    public class PersonaLogin
    {
        [JsonProperty("_a")]
        public Persona objeto { get; set; }
    }
    public class PersonaLoginEnviaContraseña { 
        [JsonProperty("_a")]
        public int resultado { get; set; }
    }
    #endregion
}