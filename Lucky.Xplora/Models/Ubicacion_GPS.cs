﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Ubicacion_GPS : AUbicacion_GPS
    {
        [JsonProperty("a")]
        public int Item { get; set; }

        [JsonProperty("b")]
        public string Cod_PDV { get; set; }

        [JsonProperty("c")]
        public string Fec_Reg_Inicio { get; set; }

        [JsonProperty("d")]
        public string Latitud_Inicio { get; set; }

        [JsonProperty("e")]
        public string Longitud_Inicio { get; set; }

        [JsonProperty("f")]
        public string Estado_Inicio { get; set; }

        [JsonProperty("g")]
        public string Fec_Reg_Fin { get; set; }

        [JsonProperty("h")]
        public string Latitud_Fin { get; set; }

        [JsonProperty("i")]
        public string Longitud_Fin { get; set; }

        [JsonProperty("j")]
        public string Estado_Fin { get; set; }

        [JsonProperty("k")]
        public string Comentario { get; set; }

        [JsonProperty("l")]
        public string Hora { get; set; }

        [JsonProperty("m")]
        public string Nom_PDV { get; set; }

        [JsonProperty("n")]
        public string Hora_Fin { get; set; }

        [JsonProperty("o")]
        public string Min_Atencion { get; set; }

        [JsonProperty("p")]
        public string Cod_Tip_Estado { get; set; }

        [JsonProperty("q")]
        public string Nom_Tip_Estado { get; set; }

        [JsonProperty("r")]
        public string Img { get; set; }

        [JsonProperty("s")]
        public string Ruta_obj { get; set; }
    }

    public abstract class AUbicacion_GPS {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Ubicacion_GPS> Lista(MapMultiGPS_Request oRq)
        {
            MapMultiGPS_ReporteUbi_Response oRp;

            oRp = MvcApplication._Deserialize<MapMultiGPS_ReporteUbi_Response>(
                    MvcApplication._Servicio_Maps.Consul_ReportUbi_MultiGPS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-09
    /// </summary>
    public class MapMultiGPS_ReporteUbi_Response
    {
        [JsonProperty("a")]
        public List<Ubicacion_GPS> Lista { get; set; }
    }

    #endregion
}