﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Tipo_Material : ATipo_Material
    {
        [JsonProperty("a")]
        public string atm_id { get; set; }

        [JsonProperty("b")]
        public string atm_descripcion { get; set; }
    }

    public class Material : AMaterial
    {
        [JsonProperty("a")]
        public string pop_id { get; set; }

        [JsonProperty("b")]
        public string pop_descripcion { get; set; }
    }

    public abstract class ATipo_Material {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-05
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Tipo_Material> Lista(Request_GetTipoMaterial_Opcion_Parametro oRq)
        {
            Response_GetTipoMaterial_Opcion_Parametro oRp;

            oRp = MvcApplication._Deserialize<Response_GetTipoMaterial_Opcion_Parametro>(
                    MvcApplication._Servicio_Campania.GetTipoMaterial_Opcion_Parametro(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }


    public abstract class AMaterial {
        public List<Material> Lista(Request_GetTipoMaterial_Opcion_Parametro oRq)
        {
            Response_GetMaterial_Opcion_Parametro oRp;

            oRp = MvcApplication._Deserialize<Response_GetMaterial_Opcion_Parametro>(
                    MvcApplication._Servicio_Campania.GetMaterial_Opcion_Parametro(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-05-05
    /// </summary>
    public class Request_GetTipoMaterial_Opcion_Parametro
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }

        [JsonProperty("_b")]
        public string parametro { get; set; }
    }
    public class Response_GetTipoMaterial_Opcion_Parametro
    {
        [JsonProperty("_a")]
        public List<Tipo_Material> Lista { get; set; }
    }

    public class Response_GetMaterial_Opcion_Parametro
    {
        [JsonProperty("_a")]
        public List<Material> Lista { get; set; }
    }

    #endregion
}