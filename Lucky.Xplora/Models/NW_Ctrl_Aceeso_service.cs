﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class E_NW_Ruteo_Parametros
    {
        [JsonProperty("a")]
        public string Cod_Perfil { get; set; }
        [JsonProperty("b")]
        public string Cod_Persona { get; set; }
    }
    public class E_NW_Ctrl_Aceeso
    {
        [JsonProperty("a")]
        public int Cod_Proyecto { get; set; }
        [JsonProperty("b")]
        public string Nom_Proyecto { get; set; }
        [JsonProperty("c")]
        public string Des_Proyecto { get; set; }
        [JsonProperty("d")]
        public string Img_Proyecto { get; set; }
        [JsonProperty("e")]
        public string Url_Proyecto { get; set; }
        [JsonProperty("f")]
        public int Cod_Modulo { get; set; }
        [JsonProperty("g")]
        public string Nom_Modulo { get; set; }
        [JsonProperty("h")]
        public string Des_Modulo { get; set; }
        [JsonProperty("i")]
        public string Img_Modulo { get; set; }
        [JsonProperty("j")]
        public string Url_Modulo { get; set; }
        [JsonProperty("k")]
        public int Cod_Vista { get; set; }
        [JsonProperty("l")]
        public string Nom_Vista { get; set; }
        [JsonProperty("m")]
        public string Des_Vista { get; set; }
        [JsonProperty("n")]
        public string Img_Vista { get; set; }
        [JsonProperty("o")]
        public string Url_vista { get; set; }
        [JsonProperty("p")]
        public string Cod_Elemento { get; set; }
        [JsonProperty("q")]
        public string Nom_Opcion { get; set; }
        [JsonProperty("r")]
        public string Des_Opcion { get; set; }
        [JsonProperty("s")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("t")]
        public int Cod_orden { get; set; }
        [JsonProperty("_u")]
        public string ovi_clase { get; set; }
        [JsonProperty("_v")]
        public int ovi_inicio { get; set; }
    }
    public class E_NW_Ctrl_Aceeso_Proyecto : ANW_Ctrl_Aceeso_service
    {
        [JsonProperty("a")]
        public int Cod_Proyecto { get; set; }
        [JsonProperty("b")]
        public string Nom_Proyecto { get; set; }
        [JsonProperty("c")]
        public string Des_Proyecto { get; set; }
        [JsonProperty("d")]
        public string Img_Proyecto { get; set; }
        [JsonProperty("e")]
        public string Url_Proyecto { get; set; }
        [JsonProperty("f")]
        public List<E_NW_Ctrl_Aceeso_Modulo> Hijo { get; set; }
    }
    public class E_NW_Ctrl_Aceeso_Modulo
    {
        [JsonProperty("a")]
        public int Cod_Modulo { get; set; }
        [JsonProperty("b")]
        public string Nom_Modulo { get; set; }
        [JsonProperty("c")]
        public string Des_Modulo { get; set; }
        [JsonProperty("d")]
        public string Img_Modulo { get; set; }
        [JsonProperty("e")]
        public string Url_Modulo { get; set; }
        [JsonProperty("f")]
        public List<E_NW_Ctrl_Aceeso_Vista> Hijo { get; set; }
        [JsonProperty("g")]
        public string Cod_Equipo { get; set; }
    }
    public class E_NW_Ctrl_Aceeso_Vista
    {
        [JsonProperty("a")]
        public int Cod_Vista { get; set; }
        [JsonProperty("b")]
        public string Nom_Vista { get; set; }
        [JsonProperty("c")]
        public string Des_Vista { get; set; }
        [JsonProperty("d")]
        public string Img_Vista { get; set; }
        [JsonProperty("e")]
        public string Url_vista { get; set; }
        //[JsonProperty("f")]
        //public List<E_NW_Ctrl_Aceeso_Opcion> Hijo { get; set; }
        [JsonProperty("g")]
        public string ovi_clase { get; set; }
        [JsonProperty("h")]
        public int ovi_inicio { get; set; }
    }
    public class E_NW_Ctrl_Aceeso_Opcion
    {
        [JsonProperty("a")]
        public string Cod_Elemento { get; set; }
        [JsonProperty("b")]
        public string Nom_Opcion { get; set; }
        [JsonProperty("c")]
        public string Des_Opcion { get; set; }
    }

    //public class NW_Ctrl_Aceeso_service : ANW_Ctrl_Aceeso_service
    //{

    //}
    public abstract class ANW_Ctrl_Aceeso_service
    {
        public List<E_NW_Ctrl_Aceeso_Proyecto> Lista(NW_Ctrl_Aceeso_Request oRq)
        {
            NW_Ctrl_Aceeso_Response oRp;

            oRp = MvcApplication._Deserialize<NW_Ctrl_Aceeso_Response>(
                    MvcApplication._Servicio_Campania.NW_Ctrl_Aceeso_Listar(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }
    public class NW_Ctrl_Aceeso_Request
    {
        [JsonProperty("a")]
        public E_NW_Ruteo_Parametros oParametros { get; set; }
    }
    public class NW_Ctrl_Aceeso_Response 
    {
        [JsonProperty("a")]
        public List<E_NW_Ctrl_Aceeso_Proyecto> Response { get; set; }
    }
}