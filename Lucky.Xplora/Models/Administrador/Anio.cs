﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Administrador
{
    public class Anio : AAnio
    {
        [JsonProperty("a")]
        public int anio { get; set; }
    }
    public abstract class AAnio
    {
        public List<Anio> Lista()
        {
            Response_Anio oRp;

            oRp = MvcApplication._Deserialize<Response_Anio>(
                    MvcApplication._Servicio_Campania.Listar_Anios()
                );
            return oRp.Lista;
        }
    }
    public class Response_Anio
    {
        [JsonProperty("a")]
        public List<Anio> Lista { get; set; }
    }
}