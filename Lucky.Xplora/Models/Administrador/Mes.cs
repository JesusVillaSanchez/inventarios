﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Administrador
{
    public class Mes : AMes
    {
        [JsonProperty("a")]
        public int id_mes { get; set; }

        [JsonProperty("b")]
        public string desc_mes { get; set; }
    }

    public abstract class AMes
    {
        public List<Mes> Lista()
        {
            Response_Mes oRp;

            oRp = MvcApplication._Deserialize<Response_Mes>(
                    MvcApplication._Servicio_Campania.Listar_Meses_New_Xplora()
                );
            return oRp.Lista;
        }
    }

    public class Response_Mes
    {
        [JsonProperty("a")]
        public List<Mes> Lista { get; set; }
    }

}