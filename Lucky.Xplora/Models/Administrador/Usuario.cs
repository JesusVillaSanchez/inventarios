﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Administrador
{
    public class Usuario : AUsuario
    {
        [JsonProperty("a")]
        public int id_person { get; set; }

        [JsonProperty("b")]
        public string desc_person { get; set; }
    }

    public abstract class AUsuario
    {
        public List<Usuario> Lista(Request_usuario oRq)
        {
            Response_Usuario oRp;

            oRp = MvcApplication._Deserialize<Response_Usuario>(
                    MvcApplication._Servicio_Operativa.Listar_Usuarios_Control_Acceso(
                    MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }
    }

    public class Request_usuario
    {
        [JsonProperty("_a")]
        public int persona { get; set; }

        [JsonProperty("_b")]
        public int tipo_perfil { get; set; }
    }

    public class Response_Usuario
    {
        [JsonProperty("a")]
        public List<Usuario> Lista { get; set; }
    }

    #region << Control de  Acceso >>
    public class Insert_Visita_Persona_Response : AVisita_Persona_Modulos
    {
        [JsonProperty("a")]
        public int CantReg { get; set; }
    }

    public abstract class AVisita_Persona_Modulos {
        public int Inserta_Visita_Modulo(Insert_Visita_Persona_Request oRq)
        {
            Insert_Visita_Persona_Response oRp;

            oRp = MvcApplication._Deserialize<Insert_Visita_Persona_Response>(
                    MvcApplication._Servicio_Operativa.Insertar_Visita_Person(
                    MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.CantReg;
        }
    }

    #region << Request Response >>
    public class Insert_Visita_Persona_Request
    {
        [JsonProperty("a")]
        public int person_id { get; set; }
        [JsonProperty("b")]
        public string modulo_id { get; set; }
        [JsonProperty("c")]
        public string company_id { get; set; }
        [JsonProperty("d")]
        public string fecha_ingreso { get; set; }
        [JsonProperty("e")]
        public int visitas { get; set; }
        [JsonProperty("f")]
        public string createBy { get; set; }
    }
    #endregion
    #endregion

    #region Control de Acesso
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-06-21
    /// </summary>
    public class ControlAcceso : AControlAcceso 
    {
        [JsonProperty("_a")]
        public int per_id { get; set; }

        [JsonProperty("_b")]
        public string per_nombre { get; set; }

        [JsonProperty("_c")]
        public string per_usuario { get; set; }

        [JsonProperty("_d")]
        public int ingreso { get; set; }

        [JsonProperty("_e")]
        public List<ControlAccesoVista> vista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-06-21
    /// </summary>
    public class ControlAccesoVista
    {
        [JsonProperty("_a")]
        public int pry_id { get; set; }

        [JsonProperty("_b")]
        public string pry_descripcion { get; set; }

        [JsonProperty("_c")]
        public int mdl_id { get; set; }

        [JsonProperty("_d")]
        public string mdl_descripcion { get; set; }

        [JsonProperty("_e")]
        public int vis_id { get; set; }

        [JsonProperty("_f")]
        public string vis_descripcion { get; set; }

        [JsonProperty("_g")]
        public int visita { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-06-21
    /// </summary>
    public abstract class AControlAcceso
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-21
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<ControlAcceso> Lista(RequestControlAcceso oRq) {
            return MvcApplication._Deserialize<ResponseControlAcceso>(
                    MvcApplication._Servicio_Operativa.ControlAcceso(MvcApplication._Serialize(oRq))
                ).Lista;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-06-21
    /// </summary>
    public class RequestControlAcceso
    {
        [JsonProperty("_a")]
        public int anio { get; set; }

        [JsonProperty("_b")]
        public int mes { get; set; }

        [JsonProperty("_c")]
        public int usu { get; set; }

        [JsonProperty("_d")]
        public int per { get; set; }

        [JsonProperty("_e")]
        public int prf { get; set; }

        [JsonProperty("_f")]
        public string parametro { get; set; }
    }
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2016-06-21
    /// </summary>
    public class ResponseControlAcceso
    {
        [JsonProperty("_a")]
        public List<ControlAcceso> Lista { get; set; }
    }
    #endregion
    #endregion
}