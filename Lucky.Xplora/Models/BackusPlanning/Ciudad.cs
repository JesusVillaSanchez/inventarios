﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusPlanning
{
    public class Ciudad : ACiudad
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_ciu { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string des_ciu { get; set; }

        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_dep { get; set; }
    }

    public abstract class ACiudad
    {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-06-22
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Ciudad> ListaCiudades(Request_GetCiudad oRq)
        {
            Response_GetCiudad oRp;

            oRp = MvcApplication._Deserialize<Response_GetCiudad>(
                    MvcApplication._Servicio_Campania.Listar_Ciudad_Por_CodCampania_CodDepartamento(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: gruiz
        /// Fecha: 2016-02-19
        /// </summary>
        /// <returns></returns>
        public List<Ciudad> ListaCiudades()
        {
            Response_GetCiudad oRp;

            oRp = MvcApplication._Deserialize<Response_GetCiudad>(
                    MvcApplication._Servicio_Campania.Listar_Ciudad_Departamento()
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 15/09/2016
        /// Descripcion: ...
        /// </summary>
        /// <param name="cod_equipo"></param>
        /// <returns></returns>
        public List<E_Oficina> oLstOficina(Request_GetCiudad oR)
        {
            Response_GetFotoOficina oRp = MvcApplication._Deserialize<Response_GetFotoOficina>(MvcApplication._Servicio_Campania.Listar_Oficinas_Por_CodCampania(MvcApplication._Serialize(oR)));
            return oRp.Lista;
        }
    }

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 2015-06-22
    /// </summary>
    public class Request_GetCiudad
    {
        [JsonProperty("a")]
        public string Cod_Campania { get; set; }
        [JsonProperty("b")]
        public string Cod_Departamento { get; set; }
    }
 
    public class Response_GetCiudad
    {
        [JsonProperty("a")]
        public List<Ciudad> Lista { get; set; }
    }

    #region << Oficina >>

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 15/09/16
        /// Descripcion: ...
        /// </summary>
        public class E_Oficina
        {
            [JsonProperty("a")]
            public long Cod_Oficina { get; set; }
            [JsonIgnore()]
            public int Company_id { get; set; }
            [JsonProperty("c")]
            public string Name_Oficina { get; set; }
            [JsonIgnore()]
            public string Abreviatura { get; set; }
            [JsonIgnore()]
            public int Orden { get; set; }
            [JsonIgnore()]
            public int Id_malla { get; set; }//Add 27/04/2012 PSA. No existe en el Framework Entity.Common.Application
            [JsonIgnore()]
            public bool Oficina_Status { get; set; }
            [JsonIgnore()]
            public string Oficina_CreateBy { get; set; }
            [JsonIgnore()]
            public DateTime Oficina_DateBy { get; set; }
            [JsonIgnore()]
            public string Oficina_ModiBy { get; set; }
            [JsonIgnore()]
            public DateTime Oficina_DateModiBy { get; set; }
        }
        /// <summary>
        /// Autor: wlopez
        /// Fecha: 15/09/2016
        /// Descripcion: ...
        /// </summary>
        public class Response_GetFotoOficina
        {
            [JsonProperty("a")]
            public List<E_Oficina> Lista { get; set; }
        }

    #endregion


}