﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusPlanning
{
    public class TipoCanalPlanning : ATipoCanalPlanning
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int cod_canal { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string des_canal { get; set; }

        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string cargo_director { get; set; }

        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public string cargo_gerente { get; set; }
    }

    public abstract class ATipoCanalPlanning
    {
        public List<TipoCanalPlanning> ListaTipoCanal(Request_GetTipoCanalBackusPlanning_Campania_Anio_Mes oRq)
        {
            Response_GetTipoCanalBackusPlanning_Campania_Anio_Mes oRp;

            oRp = MvcApplication._Deserialize<Response_GetTipoCanalBackusPlanning_Campania_Anio_Mes>(
                    MvcApplication._Servicio_Campania.GetTipoCanalBackusPlanning_Campania_Anio_Mes(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.oLista;
        }
    }

    #region Request & Response
    public class Request_GetTipoCanalBackusPlanning_Campania_Anio_Mes
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int anio { get; set; }

        [JsonProperty("_c")]
        public int mes { get; set; }

    }

    public class Response_GetTipoCanalBackusPlanning_Campania_Anio_Mes
    {
        [JsonProperty("_a")]
        public List<TipoCanalPlanning> oLista { get; set; }
    }
    #endregion

}