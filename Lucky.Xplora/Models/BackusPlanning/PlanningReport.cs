﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusPlanning
{
    public class PlanningReport : APlanningReport
    {
        [JsonProperty("_a")]
        public int id_corre { get; set; }

        [JsonProperty("_b")]
        public string cod_clie_sap { get; set; }

        [JsonProperty("_c")]
        public string cod_pdv { get; set; }

        [JsonProperty("_d")]
        public string pdv_Name { get; set; }

        [JsonProperty("_e")]
        public string distrito { get; set; }

        [JsonProperty("_f")]
        public string direccion { get; set; }

        [JsonProperty("_g")]
        public string des_clasi_cadena { get; set; }

        [JsonProperty("_h")]
        public string des_clasi_backus { get; set; }

        [JsonProperty("_i")]
        public string des_mercaderista { get; set; }

        [JsonProperty("_j")]
        public string nro_mercaderista { get; set; }

        [JsonProperty("_k")]
        public string d_lun { get; set; }

        [JsonProperty("_l")]
        public string d_mar { get; set; }

        [JsonProperty("_m")]
        public string d_mie { get; set; }

        [JsonProperty("_n")]
        public string d_jue { get; set; }

        [JsonProperty("_o")]
        public string d_vie { get; set; }

        [JsonProperty("_p")]
        public string d_sab { get; set; }

        [JsonProperty("_q")]
        public string d_dom { get; set; }

        [JsonProperty("_r")]
        public string refrigerio { get; set; }

        [JsonProperty("_s")]
        public string descanso { get; set; }
    }

    public abstract class APlanningReport
    {
        public List<PlanningReport> ListaPlanning(Request_PlanningReport oRq)
        {
            Response_PlanningReport oRp;

            oRp = MvcApplication._Deserialize<Response_PlanningReport>(
                    MvcApplication._Servicio_Operativa.Reporte_Planning_Backus(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.oLista;
        }
    }

    #region Request & Response
    public class Request_PlanningReport
    {
        [JsonProperty("_a")]
        public int periodo { get; set; }

        [JsonProperty("_b")]
        public int canal { get; set; }

        [JsonProperty("_c")]
        public int cadena { get; set; }

        [JsonProperty("_d")]
        public string departamento { get; set; }

        [JsonProperty("_e")]
        public string ciudad { get; set; }

        [JsonProperty("_f")]
        public string distrito { get; set; }

        [JsonProperty("_g")]
        public int supervisor { get; set; }

        [JsonProperty("_h")]
        public int clasiBackus { get; set; }

    }

    public class Response_PlanningReport
    {
        [JsonProperty("_a")]
        public List<PlanningReport> oLista { get; set; }
    }
    #endregion
}