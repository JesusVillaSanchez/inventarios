﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusPlanning
{
    public class Clasificacion : AClasificacion
    {
        [JsonProperty("_a")]
        public int cod_clasi { get; set; }

        [JsonProperty("_b")]
        public string des_clasi { get; set; }
    }

    public abstract class AClasificacion
    {
        public List<Clasificacion> Lista()
        {
            Response_Clasificacion oRp;

            oRp = MvcApplication._Deserialize<Response_Clasificacion>(
                    MvcApplication._Servicio_Operativa.Listar_Clasificacion_Backus()
                );
            return oRp.ListaClasi;
        }
    }

    public class Response_Clasificacion
    {
        [JsonProperty("_a")]
        public List<Clasificacion> ListaClasi { get; set; }
    }
}