﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusPlanning
{
    public class Distrito : ADistrito
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_dist{ get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string des_dist { get; set; }
    }

    public abstract class ADistrito
    {

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-06-22
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Distrito> ListaDistritos(Request_GetDistrito oRq)
        {
            Response_GetDistrito oRp;

            oRp = MvcApplication._Deserialize<Response_GetDistrito>(
                    MvcApplication._Servicio_Campania.GetDistrito(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

    }

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 2015-06-22
    /// </summary>
    public class Request_GetDistrito
    {
        [JsonProperty("a")]
        public string pais { get; set; }

        [JsonProperty("b")]
        public string departamento { get; set; }

        [JsonProperty("c")]
        public string provincia { get; set; }
    }

    public class Response_GetDistrito
    {
        [JsonProperty("a")]
        public List<Distrito> Lista { get; set; }
    }
}