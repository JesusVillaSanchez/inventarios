﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Sector : ASector
    {
        [JsonProperty("a")]
        public string sec_id { get; set; }

        [JsonProperty("b")]
        public string sec_descripcion { get; set; }
    }

    public abstract class ASector {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-11
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Sector> Lista(Request_Listar_Sector_OficinaEQ_Reports oRq)
        {
            Response_Listar_Sector_OficinaEQ_Reports oRp;

            oRp = MvcApplication._Deserialize<Response_Listar_Sector_OficinaEQ_Reports>(
                MvcApplication._Servicio_Campania.Llenar_Sector_oficinaEQ(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 2015-02-11
    /// </summary>
    public class Request_Listar_Sector_OficinaEQ_Reports
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public int compania { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public int oficina { get; set; }
    }
    public class Response_Listar_Sector_OficinaEQ_Reports
    {
        [JsonProperty("a")]
        public List<Sector> Lista { get; set; }
    }

    #endregion
}