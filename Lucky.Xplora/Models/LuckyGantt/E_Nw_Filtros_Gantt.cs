﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using Lucky.Xplora.Models.Reporting;

namespace Lucky.Xplora.Models.LuckyGantt
{
    public class E_Nw_Filtros_Backus : AE_Nw_Filtros_Backus
    {
        [JsonProperty("a")]
        public string Value { get; set; }
        [JsonProperty("b")]
        public string Text { get; set; }
    }
    public class Response_Nw_Reporting_Filtros_Backus
    {
        [JsonProperty("a")]
        public List<E_Nw_Filtros_Backus> Response { get; set; }
    }

    public abstract class AE_Nw_Filtros_Backus
    {
        public List<E_Nw_Filtros_Backus> Consul_Filtros(Request_NW_Reporting oRq)
        {
            Response_Nw_Reporting_Filtros_Backus oRp;

            oRp = MvcApplication._Deserialize<Response_Nw_Reporting_Filtros_Backus>(
                    MvcApplication._Servicio_Operativa.Consul_NW_Tb_Filtros_Gantt(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    //public class E_NW_Direccion_Documento {
    //    [JsonProperty("a")]
    //    public int id_Document { get; set; }
    //    [JsonProperty("b")]
    //    public string DocumentPromotion_Url { get; set; }        
    //}
}