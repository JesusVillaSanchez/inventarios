﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.LuckyGantt
{
    public class E_Nw_Consolidado_PromotionalActivities : AE_PromotionalActivities
    {
        [JsonProperty("_a")]
        public string promocion { get; set; }
        [JsonProperty("_b")]
        public int id_Brand { get; set; }
        [JsonProperty("_c")]
        public string marca { get; set; }
        [JsonProperty("_d")]
        public int id_typePromotion { get; set; }
        [JsonProperty("_e")]
        public string tipo_promocion { get; set; }
        [JsonProperty("_f")]
        public int id_Applicant { get; set; }
        [JsonProperty("_g")]
        public string solicitante { get; set; }
        [JsonProperty("_h")]
        public string nrd { get; set; }
        [JsonProperty("_i")]
        public string id_cobertura { get; set; }
        [JsonProperty("_j")]
        public string cobertura { get; set; }
        [JsonProperty("_k")]
        public string Premios { get; set; }
        [JsonProperty("_l")]
        public string fechainicio { get; set; }
        [JsonProperty("_m")]
        public string fechatermino { get; set; }
        [JsonProperty("_n")]
        public string estado { get; set; }
        [JsonProperty("_o")]
        public string id_row { get; set; }
        [JsonProperty("_p")]
        public string id_promocion { get; set; }
        [JsonProperty("_q")]
        public string color { get; set; }
        [JsonProperty("_r")]
        public string rd { get; set; }
        [JsonProperty("_s")]
        public string brief { get; set; }

        [JsonProperty("_t")]
        public string id_Document { get; set; }
        [JsonProperty("_v")]
        public string Document_Description { get; set; }
        [JsonProperty("_w")]
        public string id_StateDocument { get; set; }
        [JsonProperty("_x")]
        public string StateDocument { get; set; }

        [JsonProperty("_y")]
        public string pendiente { get; set; }
        [JsonProperty("_z")]
        public string compania { get; set; }
    }

    public class E_Nw_DetailGanttRegistro : AE_Nw_DetailGanttRegistro
    {
        public const int REGISTRO_OK = 1;
        public const int GENERAL_ERROR = 2;

        public int CodStatus { get; set; }
        public string Mensaje { get; set; }
    }

    public class E_Nw_DetailGanttAssingLlenado : AE_Nw_DetailGanttLlenado
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("code")]
        public string code { get; set; }
        [JsonProperty("level")]
        public int level { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
        [JsonProperty("canWrite")]
        public bool canWrite { get; set; }
        [JsonProperty("start")]
        public float start { get; set; }
        [JsonProperty("duration")]
        public int duration { get; set; }
        [JsonProperty("end")]
        public float gend { get; set; }
        [JsonProperty("startIsMilestone")]
        public bool startIsMilestone { get; set; }
        [JsonProperty("endIsMilestone")]
        public bool endIsMilestone { get; set; }
        [JsonProperty("collapsed")]
        public bool collapsed { get; set; }
        [JsonProperty("assigs")]
        public List<E_Detail_assigs> assigs { get; set; }
        [JsonProperty("hasChild")]
        public bool hasChild { get; set; }
        [JsonProperty("progress")]
        public int progress { get; set; }
        [JsonProperty("description")]
        public string description { get; set; }
        [JsonProperty("depends")]
        public string depends { get; set; }
    }
    public class E_Detail_assigs
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("resourceId")]
        public string resourceId { get; set; }
        [JsonProperty("roleId")]
        public string roleId { get; set; }
        [JsonProperty("effot")]
        public string effot { get; set; }
    }

    public class E_NW_Edit_PromotionalActivities : AE_NW_Edit_PromotionalActivities
    {
        [JsonProperty("a")]
        public int id_promotion { get; set; }
        [JsonProperty("b")]
        public string name_promotion { get; set; }
        [JsonProperty("c")]
        public int id_brand { get; set; }
        [JsonProperty("d")]
        public int id_typepromotion { get; set; }
        [JsonProperty("e")]
        public int id_applicant { get; set; }
        [JsonProperty("f")]
        public string rd { get; set; }
        [JsonProperty("g")]
        public string cod_cobertura { get; set; }
        [JsonProperty("h")]
        public string premios { get; set; }
        [JsonProperty("i")]
        public int id_statepromotion { get; set; }
        [JsonProperty("j")]
        public string company_id { get; set; }
        [JsonProperty("k")]
        public List<E_Nw_DocumentPromotion> document { get; set; }
    }

    public class E_Nw_DocumentPromotion
    {
        [JsonProperty("a")]
        public int id_promotion { get; set; }
        [JsonProperty("b")]
        public int id_document { get; set; }
        [JsonProperty("c")]
        public string insumo { get; set; }
        [JsonProperty("d")]
        public string document_url { get; set; }
    }

    public class E_NW_Actualizados_PromotionalActivities : AE_NW_Actualizados_PromotionalActivities
    {
        [JsonProperty("a")]
        public int id_promotion { get; set; }
        [JsonProperty("b")]
        public string name_promotion { get; set; }
        [JsonProperty("c")]
        public List<E_NW_Actualizados_Detail_PromActi> Detail { get; set; }
    }
    public class E_NW_Actualizados_Detail_PromActi
    {
        [JsonProperty("a")]
        public int id_promotion { get; set; }
        [JsonProperty("b")]
        public string id { get; set; }
        [JsonProperty("c")]
        public string name { get; set; }
    }



    //Listado
    public abstract class AE_PromotionalActivities {

        public List<E_Nw_Consolidado_PromotionalActivities> Consul_ActividadesPromocionales(Request_ActividadesPromocionales oRq)
        {
            Response_ActividadesPromocionales oRp;

            oRp = MvcApplication._Deserialize<Response_ActividadesPromocionales>(
                    MvcApplication._Servicio_Operativa.Consul_PromotionalActivities(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }

        public List<E_Nw_Consolidado_PromotionalActivities> Consul_DocumentosPendientes(Request_ActividadesPromocionales oRq)
        {
            Response_ActividadesPromocionales oRp;

            oRp = MvcApplication._Deserialize<Response_ActividadesPromocionales>(
                    MvcApplication._Servicio_Operativa.Consul_EarringsDocuments_ActProm(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    //Registro
    public abstract class AE_Nw_DetailGanttRegistro {
        public E_Nw_DetailGanttRegistro Registrar_DetailGantt(Request_DetailGantt oRq)
        {
            Response_DetailGantt oRp;

            oRp = MvcApplication._Deserialize<Response_DetailGantt>(
                    MvcApplication._Servicio_Operativa.BL_NW_Insert_DetailGantt(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }

        public E_Nw_DetailGanttRegistro Delete_Promotion(Request_DeletePromotion oRq)
        {
            Response_DetailGantt oRp;

            oRp = MvcApplication._Deserialize<Response_DetailGantt>(
                    MvcApplication._Servicio_Operativa.BL_NW_Delete_Promotion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }

        public E_Nw_DetailGanttRegistro Edit_Promotion(Request_EditPromotion oRq)
        {
            Response_DetailGantt oRp;

            oRp = MvcApplication._Deserialize<Response_DetailGantt>(
                    MvcApplication._Servicio_Operativa.BL_NW_Edit_Promotion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }

        public E_Nw_DetailGanttRegistro Generar_Cod_Promotion() {
            Response_DetailGantt oRp;

            oRp = MvcApplication._Deserialize<Response_DetailGantt>(
                    MvcApplication._Servicio_Operativa.BL_NW_Cod_Promotion()
                );

            return oRp.StatusRegistro;
        }

    }

    //Listado Gantt
    public abstract class AE_Nw_DetailGanttLlenado {

        public List<E_Nw_DetailGanttAssingLlenado> LlenarGantt(Request_GanttActivProm oRq)
        {
            Response_AssigsGanttActivProm oRp;

            oRp = MvcApplication._Deserialize<Response_AssigsGanttActivProm>(
                    MvcApplication._Servicio_Operativa.Consul_AssigsGanttPromActiv(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_NW_Edit_PromotionalActivities {
        public List<E_NW_Edit_PromotionalActivities> EditPromotionalActivities(Request_ActividadesPromocionales oRq)
        {
            Response_Edit_ActividadesPromocionales oRp;

            oRp = MvcApplication._Deserialize<Response_Edit_ActividadesPromocionales>(
                    MvcApplication._Servicio_Operativa.BL_NW_Edit_PromotionalActivities(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_NW_Actualizados_PromotionalActivities {
        public List<E_NW_Actualizados_PromotionalActivities> ActualizadosPromotionalActivities(Request_ActualizadosActivProm oRq)
        {
            Response_ActualizadosActivProm oRp;

            oRp = MvcApplication._Deserialize<Response_ActualizadosActivProm>(
                    MvcApplication._Servicio_Operativa.BL_NW_Actualizados_PromAct(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    #region << Parametros RQ Lucky 266 v1 0 >>

    public class E_Nw_PromotionalActivities
    {
        [JsonProperty("_a")]
        public int id_Brand { get; set; }
        [JsonProperty("_b")]
        public int anio { get; set; }
        [JsonProperty("_c")]
        public int id_typeactivity { get; set; }
        [JsonProperty("_d")]
        public int id_state { get; set; }
        [JsonProperty("_e")]
        public int mes { get; set; }
        [JsonProperty("_f")]
        public int id_solicitante { get; set; }
        [JsonProperty("_g")]
        public int id_promotional { get; set; }
        [JsonProperty("_h")]
        public int company_id { get; set; }

    }
    public class E_Nw_Detail_Gantt
    {
        [JsonProperty("_a")]
        public string id { get; set; }
        [JsonProperty("_b")]
        public string name { get; set; }
        [JsonProperty("_c")]
        public string code { get; set; }
        [JsonProperty("_d")]
        public int level { get; set; }
        [JsonProperty("_e")]
        public string status { get; set; }
        [JsonProperty("_f")]
        public bool canWrite { get; set; }
        [JsonProperty("_g")]
        public string start { get; set; }
        [JsonProperty("_h")]
        public int duration { get; set; }
        [JsonProperty("_i")]
        public string gend { get; set; }
        [JsonProperty("_j")]
        public bool startIsMilestone { get; set; }
        [JsonProperty("_k")]
        public bool endIsMilestone { get; set; }
        [JsonProperty("_l")]
        public bool collapsed { get; set; }
        [JsonProperty("_m")]
        public string assigs { get; set; }
        [JsonProperty("_n")]
        public bool hasChild { get; set; }
        [JsonProperty("_o")]
        public int progress { get; set; }
        [JsonProperty("_p")]
        public string description { get; set; }
        [JsonProperty("_q")]
        public string depends { get; set; }
        [JsonProperty("_r")]
        public string Promotion_CreateBy { get; set; }
    }

    public class E_NW_Editar_PromotionalActivities
    {
        [JsonProperty("a")]
        public int id_promotion { get; set; }
        [JsonProperty("b")]
        public string name_promotion { get; set; }
        [JsonProperty("c")]
        public int id_brand { get; set; }
        [JsonProperty("d")]
        public int id_typepromotion { get; set; }
        [JsonProperty("e")]
        public int id_applicant { get; set; }
        [JsonProperty("f")]
        public string rd { get; set; }
        [JsonProperty("g")]
        public string cod_cobertura { get; set; }
        [JsonProperty("h")]
        public string premios { get; set; }
        [JsonProperty("i")]
        public string doc_apertura { get; set; }

        [JsonProperty("j")]
        public string da_insumo { get; set; }
        [JsonProperty("k")]
        public string da_url { get; set; }
        [JsonProperty("l")]
        public string documento_cierre { get; set; }
        [JsonProperty("m")]
        public string dc_insumo { get; set; }
        [JsonProperty("n")]
        public string dc_url { get; set; }
        [JsonProperty("o")]
        public string estado_promocion { get; set; }
        [JsonProperty("p")]
        public string pendiente_apertura { get; set; }
        [JsonProperty("q")]
        public string pendiente_cierre { get; set; }
        [JsonProperty("r")]
        public string user { get; set; }

    }

    #endregion

    #region << Request - Response  RQ Lucky 266 v1 0 >>

    public class Request_ActividadesPromocionales
    {
        [JsonProperty("_a")]
        public E_Nw_PromotionalActivities oParametros { get; set; }
    }
    public class Response_ActividadesPromocionales
    {
        [JsonProperty("_a")]
        public List<E_Nw_Consolidado_PromotionalActivities> Response { get; set; }
    }

    public class Request_DetailGantt
    {
        [JsonProperty("_a")]
        public List<E_Nw_Detail_Gantt> oDetailGantt { get; set; }
        [JsonProperty("_b")]
        public int cod_promocion { get; set; }
        [JsonProperty("_c")]
        public string strlist_promocion { get; set; }
    }
    public class Response_DetailGantt
    {
        [JsonProperty("_a")]
        public E_Nw_DetailGanttRegistro StatusRegistro { get; set; }
    }

    public class Request_GanttActivProm
    {
        [JsonProperty("_a")]
        public int cod_promocion { get; set; }
        [JsonProperty("_b")]
        public int cod_analyst { get; set; }
    }
    public class Response_AssigsGanttActivProm
    {
        [JsonProperty("_a")]
        public List<E_Nw_DetailGanttAssingLlenado> Response { get; set; }
    }

    public class Response_Edit_ActividadesPromocionales
    {
        [JsonProperty("_a")]
        public List<E_NW_Edit_PromotionalActivities> Response { get; set; }
    }

    public class Request_DeletePromotion
    {
        [JsonProperty("_a")]
        public int cod_promocion { get; set; }
        [JsonProperty("_b")]
        public string user_modiby { get; set; }
    }

    public class Request_EditPromotion
    {
        [JsonProperty("_a")]
        public E_NW_Editar_PromotionalActivities oParametros { get; set; }
    }

    public class Request_ActualizadosActivProm
    {
        [JsonProperty("_a")]
        public int cod_promocion { get; set; }
    }
    public class Response_ActualizadosActivProm
    {
        [JsonProperty("_a")]
        public List<E_NW_Actualizados_PromotionalActivities> Response { get; set; }
    }

    #endregion
}