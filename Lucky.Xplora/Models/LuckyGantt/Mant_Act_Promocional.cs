﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.LuckyGantt
{
    public class E_NW_Brand : AE_NW_Brand
    {
        [JsonProperty("a")]
        public int Id { get; set; }
        [JsonProperty("b")]
        public int Id_Brand { get; set; }
        [JsonProperty("c")]
        public int Company_id { get; set; }
        [JsonProperty("d")]
        public string Company_Name { get; set; }
        [JsonProperty("e")]
        public string Name_Brand { get; set; }
        [JsonProperty("f")]
        public string Brand_Status { get; set; }
    }

    public class E_NW_TypePromotion : AE_NW_TypePromotion
    {
        [JsonProperty("a")]
        public int Id { get; set; }
        [JsonProperty("b")]
        public int Company_id { get; set; }
        [JsonProperty("c")]
        public string Company_Name { get; set; }
        [JsonProperty("d")]
        public string Name_TypePromotion { get; set; }
        [JsonProperty("e")]
        public int TypePromotion_Status { get; set; }
        [JsonProperty("f")]
        public int id_TypePromotion { get; set; }
    }

    public class E_NW_Area : AE_NW_Area
    {
        [JsonProperty("a")]
        public int Id { get; set; }
        [JsonProperty("b")]
        public int id_Department { get; set; }
        [JsonProperty("c")]
        public int Company_id { get; set; }
        [JsonProperty("d")]
        public string Company_Name { get; set; }
        [JsonProperty("e")]
        public string Department_Name { get; set; }
        [JsonProperty("f")]
        public string Department_Status { get; set; }
    }

    public class E_NW_Document : AE_NW_Document
    {
        [JsonProperty("a")]
        public int Id { get; set; }
        [JsonProperty("b")]
        public int id_Document { get; set; }
        [JsonProperty("c")]
        public int Company_id { get; set; }
        [JsonProperty("d")]
        public string Company_Name { get; set; }
        [JsonProperty("e")]
        public string Id_StateDocument { get; set; }
        [JsonProperty("f")]
        public string State_Description { get; set; }
        [JsonProperty("g")]
        public string Description { get; set; }
        [JsonProperty("h")]
        public string Document_Status { get; set; }
    }

    public class E_Nw_DetailGanttMantenimiento : AE_DetailMantenimiento
    {
        public const int REGISTRO_OK = 1;
        public const int GENERAL_ERROR = 2;

        public int CodStatus { get; set; }
        public string Mensaje { get; set; }
    }

    public abstract class AE_NW_Brand{

        public List<E_NW_Brand> Listar_Marca()
        {
            Response_ListadoMarca oRp;

            oRp = MvcApplication._Deserialize<Response_ListadoMarca>(
                    MvcApplication._Servicio_Operativa.BL_NW_Lista_Marca()
                );

            return oRp.Response;
        }

        public List<E_NW_Brand> Consulta_Marca(Request_InsertBrand oRq)
        {
            Response_ListadoMarca oRp;

            oRp = MvcApplication._Deserialize<Response_ListadoMarca>(
                    MvcApplication._Servicio_Operativa.BL_NW_Consulta_Marca(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }

    }

    public abstract class AE_NW_TypePromotion {
        public List<E_NW_TypePromotion> Listar_TipoPromocion()
        {
            Response_ListadoTipoPromocion oRp;

            oRp = MvcApplication._Deserialize<Response_ListadoTipoPromocion>(
                    MvcApplication._Servicio_Operativa.BL_NW_Lista_TipoPromocion()
                );

            return oRp.Response;
        }
        public List<E_NW_TypePromotion> Consulta_TipoPromocion(Request_InsertTipoPromocion oRq)
        {
            Response_ListadoTipoPromocion oRp;

            oRp = MvcApplication._Deserialize<Response_ListadoTipoPromocion>(
                    MvcApplication._Servicio_Operativa.BL_NW_Consulta_TipoPromocion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_NW_Area {
        public List<E_NW_Area> Listar_Solicitante()
        {
            Response_ListadoArea oRp;

            oRp = MvcApplication._Deserialize<Response_ListadoArea>(
                    MvcApplication._Servicio_Operativa.BL_NW_Lista_Area()
                );

            return oRp.Response;
        }
        public List<E_NW_Area> Consulta_Solicitante(Request_InsertArea oRq)
        {
            Response_ListadoArea oRp;

            oRp = MvcApplication._Deserialize<Response_ListadoArea>(
                    MvcApplication._Servicio_Operativa.BL_NW_Consulta_Area(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_NW_Document {
        public List<E_NW_Document> Listar_Document()
        {
            Response_ListadoDocument oRp;

            oRp = MvcApplication._Deserialize<Response_ListadoDocument>(
                    MvcApplication._Servicio_Operativa.BL_NW_Lista_Document()
                );

            return oRp.Response;
        }
        public List<E_NW_Document> Consulta_Document(Request_InsertDocument oRq)
        {
            Response_ListadoDocument oRp;

            oRp = MvcApplication._Deserialize<Response_ListadoDocument>(
                    MvcApplication._Servicio_Operativa.BL_NW_Consulta_Document(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    public abstract class AE_DetailMantenimiento {
        public E_Nw_DetailGanttMantenimiento Insert_Marca(Request_InsertBrand oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Insert_Brand(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        public E_Nw_DetailGanttMantenimiento Update_Marca(Request_InsertBrand oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Update_Brand(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        public E_Nw_DetailGanttMantenimiento Delete_Marca(Request_InsertBrand oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Delete_Brand(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }

        public E_Nw_DetailGanttMantenimiento Insert_TipoPromocion(Request_InsertTipoPromocion oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Insert_TipoPromocion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        public E_Nw_DetailGanttMantenimiento Update_TipoPromocion(Request_InsertTipoPromocion oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Update_TipoPromocion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        public E_Nw_DetailGanttMantenimiento Delete_TipoPromocion(Request_InsertTipoPromocion oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Delete_TipoPromocion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }

        public E_Nw_DetailGanttMantenimiento Insert_Solicitante(Request_InsertArea oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Insert_Area(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        public E_Nw_DetailGanttMantenimiento Update_Solicitante(Request_InsertArea oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Update_Area(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        public E_Nw_DetailGanttMantenimiento Delete_Solicitante(Request_InsertArea oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Delete_Area(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }

        public E_Nw_DetailGanttMantenimiento Insert_Document(Request_InsertDocument oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Insert_Document(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        public E_Nw_DetailGanttMantenimiento Update_Document(Request_InsertDocument oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Update_Document(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }
        public E_Nw_DetailGanttMantenimiento Delete_Document(Request_InsertDocument oRq)
        {
            Response_DetailMantenimiento oRp;

            oRp = MvcApplication._Deserialize<Response_DetailMantenimiento>(
                    MvcApplication._Servicio_Operativa.BL_NW_Delete_Document(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.StatusRegistro;
        }

    }

    #region << Parametros >>

    public class E_NW_Request_Brand
    {
        [JsonProperty("a")]
        public int Company_id { get; set; }
        [JsonProperty("b")]
        public string Brand_Name { get; set; }
        [JsonProperty("c")]
        public string user { get; set; }

        [JsonProperty("d")]
        public int id_Brand { get; set; }
        [JsonProperty("e")]
        public string opcion { get; set; }

    }

    public class E_NW_Request_TypePromotion
    {
        [JsonProperty("a")]
        public int Company_id { get; set; }
        [JsonProperty("b")]
        public string Name_typepromotion { get; set; }
        [JsonProperty("c")]
        public string user { get; set; }

        [JsonProperty("d")]
        public int Id_typePromotion { get; set; }
    }

    public class E_NW_Request_Area
    {
        [JsonProperty("a")]
        public int Company_id { get; set; }
        [JsonProperty("d")]
        public int Id_Area { get; set; }
        [JsonProperty("b")]
        public string Name_Area { get; set; }
        [JsonProperty("c")]
        public string user { get; set; }
    }

    public class E_NW_Request_Document
    {
        [JsonProperty("c")]
        public int Company_id { get; set; }
        [JsonProperty("e")]
        public int Id_Document { get; set; }
        [JsonProperty("a")]
        public int id_StateDocument { get; set; }
        [JsonProperty("b")]
        public string Name_Document { get; set; }
        [JsonProperty("d")]
        public string user { get; set; }
    }

    #endregion

    #region Request - Response

    public class Response_ListadoMarca
    {
        [JsonProperty("_a")]
        public List<E_NW_Brand> Response { get; set; }
    }
    public class Request_InsertBrand
    {
        [JsonProperty("_a")]
        public E_NW_Request_Brand oParametros { get; set; }
    }
    public class Response_DetailMantenimiento
    {
        [JsonProperty("_a")]
        public E_Nw_DetailGanttMantenimiento StatusRegistro { get; set; }
    }


    public class Response_ListadoTipoPromocion
    {
        [JsonProperty("_a")]
        public List<E_NW_TypePromotion> Response { get; set; }
    }
    public class Request_InsertTipoPromocion
    {
        [JsonProperty("_a")]
        public E_NW_Request_TypePromotion oParametros { get; set; }
    }

    public class Response_ListadoArea
    {
        [JsonProperty("_a")]
        public List<E_NW_Area> Response { get; set; }
    }
    public class Request_InsertArea
    {
        [JsonProperty("_a")]
        public E_NW_Request_Area oParametros { get; set; }
    }

    public class Response_ListadoDocument
    {
        [JsonProperty("_a")]
        public List<E_NW_Document> Response { get; set; }
    }
    public class Request_InsertDocument
    {
        [JsonProperty("_a")]
        public E_NW_Request_Document oParametros { get; set; }
    }


    #endregion

}