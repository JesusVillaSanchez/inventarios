﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.BackusRepositorio
{
    //public class Tipo : ATipo
    //{
    //    [JsonProperty("_a")]
    //    public int cod_tipo { get; set; }

    //    [JsonProperty("_b")]
    //    public string des_tipo { get; set; }
    //}

    public class E_Informesv2 : AE_Informesv2
    {
        [JsonProperty("a")]
        public string id { get; set; }
        [JsonProperty("b")]
        public string nom_reporte { get; set; }
        [JsonProperty("c")]
        public string ruta_reporte { get; set; }
        [JsonProperty("d")]
        public string anio { get; set; }
        [JsonProperty("e")]
        public string info_mesinforme { get; set; }
        [JsonProperty("f")]
        public string extension { get; set; }
    }

    //public abstract class ATipo
    //{
    //    public List<Tipo> Lista()
    //    {
    //        Response_Listar_Reporte_Por_CodCampania oRp;

    //        oRp = MvcApplication._Deserialize<Response_Listar_Reporte_Por_CodCampania>(
    //                MvcApplication._Servicio_Operativa.Listar_Tipo_Backus()
    //            );
    //        return oRp.ListaTipo;
    //    }
    //}

    public abstract class AE_Informesv2
    {
        public List<E_Informesv2> Lista_Informe_NW(NW_ListarInformesCM_Request oRq)
        {
            NW_ListarInformesCM_Response oRp;

            oRp = MvcApplication._Deserialize<NW_ListarInformesCM_Response>(
                MvcApplication._Servicio_Operativa.BL_ListarInformesCM(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.oListaInformes;
        }
    }

    //public class Response_Tipo
    //{
    //    [JsonProperty("_a")]
    //    public List<Tipo> ListaTipo { get; set; }
    //}

    //#region Request & Response
    //public class Request_Listar_Reporte_Por_CodCampania_Modulo
    //{
    //    [JsonProperty("_a")]
    //    public String CodCampania { get; set; }
    //}

    //public class Response_Listar_Reporte_Por_CodCampania_Modulo
    //{
    //    [JsonProperty("_a")]
    //    public List<Tipo> ListaTipo { get; set; }
    //}



    //#endregion





    public class NW_ListarInformesCM_Request
    {
        [JsonProperty("a")]
        public int Company_id { get; set; }
        [JsonProperty("b")]
        public int anio { get; set; }
        [JsonProperty("c")]
        public int mes { get; set; }
        [JsonProperty("d")]
        public int cod_subcanal { get; set; }
        [JsonProperty("e")]
        public int cod_tipoagrupamiento { get; set; }
    }
    public class NW_ListarInformesCM_Response
    {
        [JsonProperty("a")]
        public List<E_Informesv2> oListaInformes { get; set; }
    }

}