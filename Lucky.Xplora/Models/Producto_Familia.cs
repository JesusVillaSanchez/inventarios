﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Producto_Familia : AProducto_Familia
    {
        [JsonProperty("_a")]
        public string pfa_id { get; set; }

        [JsonProperty("_b")]
        public string pfa_descripcion { get; set; }
    }

    public abstract class AProducto_Familia {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-23
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Producto_Familia> Lista(Request_GetProductoFamilia_Campania_Reporte_Categoria oRq)
        {
            Response_GetProductoFamilia_Campania_Reporte_Categoria oRp;

            oRp = MvcApplication._Deserialize<Response_GetProductoFamilia_Campania_Reporte_Categoria>(
                    MvcApplication._Servicio_Campania.GetProductoFamilia_Campania_Reporte_Categoria(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-03-23
    /// </summary>
    public class Request_GetProductoFamilia_Campania_Reporte_Categoria
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int reporte { get; set; }

        [JsonProperty("_c")]
        public int categoria { get; set; }
    }
    public class Response_GetProductoFamilia_Campania_Reporte_Categoria
    {
        [JsonProperty("_a")]
        public List<Producto_Familia> Lista { get; set; }
    }

    #endregion
}