﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Empresa : AEmpresa
    {
        [JsonProperty("_a")]
        public int cod_empresa { get; set; }
        [JsonProperty("_b")]
        public string des_empresa { get; set; }
    }

    public abstract class AEmpresa
    {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 02-06-2015
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>    
        public List<Empresa> Lista(Request_GetEmpresa oRq)
        {

            Response_GetEmpresa oRp;

            oRp = MvcApplication._Deserialize<Response_GetEmpresa>(
                MvcApplication._Servicio_Campania.GetEmpresa(
                        MvcApplication._Serialize(oRq)
                    )
            );
            return oRp.Lista;
        }
    }


    #region Rwquest & Response
    ///<sumary>
    ///Autor: rcontreras
    ///Fecha: 02-06-2015
    ///</sumary>

    public class Request_GetEmpresa
    {
        [JsonProperty("_a")]
        public int company { get; set; }
    }

    public class Response_GetEmpresa
    {
        [JsonProperty("_a")]
        public List<Empresa> Lista { get; set; }
    }


    #endregion
}