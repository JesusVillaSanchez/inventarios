﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Module
{
    #region << Gestion Categoria >>

    public class E_DatosGrafico_Categoria : M_DatosGrafico_Categoria
    {
        [JsonProperty("_a")]
        public string cod_catego { get; set; }
        [JsonProperty("_b")]
        public string Name_Catego { get; set; }
    }

    public abstract class M_DatosGrafico_Categoria
    {
        public List<E_DatosGrafico_Categoria> Lista(Request_DatosGrafico_Categoria oRq)
        {
            Response_DatosGrafico_Categoria oRp;

            oRp = MvcApplication._Deserialize<Response_DatosGrafico_Categoria>(
                MvcApplication._Servicio_Operativa.BL_Module_DatosGrafico_Filtro_Categoria(
                    MvcApplication._Serialize(oRq)
                )
            );
            return oRp.Lista;
        }
    }

    #region << Request, Response >>

    public class Request_DatosGrafico_Categoria
    {
        [JsonProperty("_a")]
        public int cod_company { get; set; }
        [JsonProperty("_b")]
        public string cod_channel { get; set; }
        [JsonProperty("_c")]
        public int cod_reporte { get; set; }
        [JsonProperty("_d")]
        public string cod_categoria { get; set; }
        [JsonProperty("_e")]
        public int path { get; set; }
    }
    public class Response_DatosGrafico_Categoria
    {
        [JsonProperty("_a")]
        public List<E_DatosGrafico_Categoria> Lista { get; set; }
    }

    #endregion

    #endregion
}