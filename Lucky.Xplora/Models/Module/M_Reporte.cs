﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Module
{
    public class M_Reporte : AReporte
    {
        [JsonProperty("a")]
        public int Report_Id { get; set; }
        [JsonProperty("b")]
        public string Report_NameReport { get; set; }
    }

    public abstract class AReporte
    {
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 20-05-2015
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<M_Reporte> Lista(Request_Listar_Reporte_Por_CodCampaniaAnioMes oRq)
        {
            Response_Listar_Reporte_Por_CodCampaniaAnioMes oRp;

            oRp = MvcApplication._Deserialize<Response_Listar_Reporte_Por_CodCampaniaAnioMes>(
                MvcApplication._Servicio_Campania.Listar_Reporte_Por_CodCampaniaAnioMes(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }

    #region Request & Response
        public class Request_Listar_Reporte_Por_CodCampaniaAnioMes
        {
            [JsonProperty("_a")]
            public string CodCampania { get; set; }
            [JsonProperty("_b")]
            public int anio { get; set; }
            [JsonProperty("_c")]
            public int mes { get; set; }
        }
        public class Response_Listar_Reporte_Por_CodCampaniaAnioMes
        {
            [JsonProperty("_a")]
            public List<M_Reporte> Lista { get; set; }
        }
    #endregion

}