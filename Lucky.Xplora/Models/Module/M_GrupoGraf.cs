﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Module
{
    public class M_GrupoGraf : M_Combo_Grupo_x_Grafico
    {
        [JsonProperty("_a")]
        public string id_grupo { get; set; }
    }

    public abstract class M_Combo_Grupo_x_Grafico {
        public List<M_GrupoGraf> Lista(Request_Graf_Sku_Agrup_x_per oRq)
        {
            Response_Graf_Sku_Agrup_x_per oRp;

            oRp = MvcApplication._Deserialize<Response_Graf_Sku_Agrup_x_per>(
                MvcApplication._Servicio_Operativa.BL_Module_Graf_Sku_Agrup_x_per(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }

    #region << Reuquest Response >>

    public class Request_Graf_Sku_Agrup_x_per
    {
        [JsonProperty("a")]
        public string id_planning { get; set; }
        [JsonProperty("b")]
        public int id_grilla { get; set; }
        [JsonProperty("c")]
        public int id_reportsplanning { get; set; }
        [JsonProperty("d")]
        public string id_ProductCategory { get; set; }
    }

    public class Response_Graf_Sku_Agrup_x_per
    {
        [JsonProperty("a")]
        public List<M_GrupoGraf> Lista { get; set; }
    }

    #endregion
}