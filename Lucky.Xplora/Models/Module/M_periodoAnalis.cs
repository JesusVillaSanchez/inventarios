﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Module
{
    public class M_Periodo : Module_Periodo
    {
        [JsonProperty("a")]
        public int id { get; set; }

        [JsonProperty("b")]
        public string descripcion { get; set; }

        [JsonProperty("z")]
        public int validacion { get; set; }
    }

    public abstract class Module_Periodo
    {
        public List<M_Periodo> Lista(Request_Periodo oRq)
        {
            Response_Periodo oRp;

            oRp = MvcApplication._Deserialize<Response_Periodo>(
                MvcApplication._Servicio_Campania.GetPeriodoI(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }
    }


    #region Request & Response

    public class Request_Periodo
    {
        [JsonProperty("a")]
        public string servicio { get; set; }

        [JsonProperty("b")]
        public string canal { get; set; }

        [JsonProperty("c")]
        public string compania { get; set; }

        [JsonProperty("d")]
        public string reporte { get; set; }

        [JsonProperty("e")]
        public string anio { get; set; }

        [JsonProperty("f")]
        public string mes { get; set; }
    }
    public class Response_Periodo
    {
        [JsonProperty("a")]
        public List<M_Periodo> Lista { get; set; }
    }

    #endregion
}