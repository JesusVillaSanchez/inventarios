﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Module
{
    #region Gestion de List Modulos

    public class Module_Response_PrecSugerido_Col : M_Module_Listado_PrecioSugerido
    {
        [JsonProperty("_a")]
        public string reporte { get; set; }
        [JsonProperty("_b")]
        public int insert { get; set; }
        [JsonProperty("_c")]
        public int update { get; set; }
    }

    public class Module_PrecSugerido_Colgate : M_Module_Listado_PrecioSugerido
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int id_SkuObject { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string id_Planning { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public int Company_id { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public string Company_Name { get; set; }
        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int id_ReportsPlanning { get; set; }
        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public string Periodo { get; set; }
        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public string id_ProductCategory { get; set; }
        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public string Product_Category { get; set; }
        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public int id_Brand { get; set; }
        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public string Name_Brand { get; set; }
        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_Product { get; set; }
        [JsonProperty("_l", NullValueHandling = NullValueHandling.Ignore)]
        public string Product_Name { get; set; }
        [JsonProperty("_m", NullValueHandling = NullValueHandling.Ignore)]
        public string Sku_Mandatario { get; set; }
        [JsonProperty("_n", NullValueHandling = NullValueHandling.Ignore)]
        public string P_Sugerido { get; set; }

        [JsonProperty("_ñ", NullValueHandling = NullValueHandling.Ignore)]
        public string createby { get; set; }
        [JsonProperty("_o", NullValueHandling = NullValueHandling.Ignore)]
        public string archivo { get; set; }
    }

    public class Module_PrecSugerido_Col_Descarga : M_Module_Descarga_PrecioSugerido
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int company_id { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string Company_Name { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public string id_ProductCategory { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public string Product_Category { get; set; }
        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public int id_Brand { get; set; }
        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public string Name_Brand { get; set; }
        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public int id_Product { get; set; }
        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_Product { get; set; }
        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public string Product_Name { get; set; }
        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public string precio_sugerido { get; set; }
        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public string Sku_Mandatorio { get; set; }
    }

    public class Codigo_Module_Range_Sku_Mandatory : M_Module_Codigo_Genera_Rango
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string codigo_rango { get; set; }
    }

    public class Response_Insert_Module_RangoSku_Madatorio : M_Module_Rango_Sku_Mandatorios
    {
        [JsonProperty("a")]
        public int CantRegInsertados { get; set; }
    }

    public class Response_Update_Module_SkuGraficos : M_Update_Module_SkuGraficos
    {
        [JsonProperty("a")]
        public int CantRegActualizados { get; set; }
    }

    public class Response_Update_Module_Graficos_Group_Sku : M_Update_Module_Group_Sku_Graficos
    {
        [JsonProperty("a")]
        public int CantRegActualizados { get; set; }
    }

    public class M_ValAnalista : M_Module_ValAnalista
    {
        [JsonProperty("_a")]
        public int oBjCantidadUpd { get; set; }
    }

    /// <summary>
    /// Autor:wlopez   
    /// Fecha:28/09/2016
    /// </summary>
    public class M_ValFotografico : M_Module_ValFotografico
    {
        [JsonProperty("_a")]
        public int oBjCantidadUpd { get; set; }
    }

    public class Module_ValidacionAnalista_Periodo : M_Module_ValAnalista_Periodos
    {
        [JsonProperty("_a")]
        public string id_Year { get; set; }
        [JsonProperty("_b")]
        public string id_Month { get; set; }
        [JsonProperty("_c")]
        public string Planning_Name { get; set; }
        [JsonProperty("_d")]
        public string Periodo { get; set; }
        [JsonProperty("_e")]
        public string Validacion { get; set; }
    }
    /// <summary>
    /// Autor:wlopez
    /// Fecha: 27/09/2016
    /// </summary>
    public class Module_ValidacionAnalista_Periodofoto : M_Module_ValAnalista_Periodosfoto
    {
        [JsonProperty("_a")]
        public string id_Year { get; set; }
        [JsonProperty("_b")]
        public string id_Month { get; set; }
        [JsonProperty("_c")]
        public string Planning_Name { get; set; }
        [JsonProperty("_d")]
        public string Periodo { get; set; }
        [JsonProperty("_e")]
        public string Validacion { get; set; }
        [JsonProperty("_f")]
        public string CantidadFoto { get; set; }
    }

    public class Respone_New_Group_Sku_Graf : M_Respone_New_Group_Sku_Graf
    {
        [JsonProperty("a")]
        public int id_grupo_new { get; set; }
    }

    public class E_Rango_Mandatorio : M_Consulta_Rango_Mandatorio {
        [JsonProperty("a")]
        public string id_reportsPlanning { get; set; }
        [JsonProperty("b")]
        public string cod_range { get; set; }
        [JsonProperty("c")]
        public string Mandatorio_RangeDesde { get; set; }
        [JsonProperty("d")]
        public string Mandatorio_RangeHasta { get; set; }
    }


    public abstract class M_Module_Listado_PrecioSugerido
    {
        public List<Module_PrecSugerido_Colgate> Lista(Request_Precio_Sugerido oRq)
        {
            Response_Precio_Sugerido oRp;

            oRp = MvcApplication._Deserialize<Response_Precio_Sugerido>(
                    MvcApplication._Servicio_Operativa.Module_PrecSugerido_Colgate(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        public List<Module_Response_PrecSugerido_Col> BulkCopyPrecSugCol(Request_BulkCopy_Module_PrecSug_Col oRq)
        {
            Response_BulkCopy_Module_PrecSug_Col oRp;

            oRp = MvcApplication._Deserialize<Response_BulkCopy_Module_PrecSug_Col>(
                    MvcApplication._Servicio_Operativa.BulkCopy_Module_Precio_Colgate(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantidad;
        }
    }

    public abstract class M_Module_Descarga_PrecioSugerido {

        public List<Module_PrecSugerido_Col_Descarga> Lista(Request_Precio_Sugerido_Descarga oRq)
        {
            Response_Precio_Sugerido_Descarga oRp;

            oRp = MvcApplication._Deserialize<Response_Precio_Sugerido_Descarga>(
                    MvcApplication._Servicio_Operativa.Module_PrecSugerido_Col_Descarga(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class M_Module_Codigo_Genera_Rango {
        public List<Codigo_Module_Range_Sku_Mandatory> Lista(Request_Codigo_Rango_Sku oRq)
        {
            Response_Codigo_Rango_Sku oRp;

            oRp = MvcApplication._Deserialize<Response_Codigo_Rango_Sku>(
                    MvcApplication._Servicio_Operativa.BL_Generar_Codigo_Modulo_Rangos(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class M_Module_Rango_Sku_Mandatorios {

        public int Lista(Request_Insert_Module_RangoSku_Madatorio oRq)
        {
            Response_Insert_Module_RangoSku_Madatorio oRp;

            oRp = MvcApplication._Deserialize<Response_Insert_Module_RangoSku_Madatorio>(
                    MvcApplication._Servicio_Operativa.BL_Insert_Module_RangoSku_Madatorio(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.CantRegInsertados;
        }


    }

    public abstract class M_Update_Module_SkuGraficos {
        public int Update(Request_Update_Module_SkuGraficos oRq)
        {
            Response_Update_Module_SkuGraficos oRp;

            oRp = MvcApplication._Deserialize<Response_Update_Module_SkuGraficos>(
                    MvcApplication._Servicio_Operativa.BL_Update_GraficosDatos_Sku_Seleccionados(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.CantRegActualizados;
        }

        public int UpdateOrderSku(Request_Update_Module_SkuGraficos oRq)
        {
            Response_Update_Module_SkuGraficos oRp;

            oRp = MvcApplication._Deserialize<Response_Update_Module_SkuGraficos>(
                    MvcApplication._Servicio_Operativa.BL_Update_M_DatosGraficos_Sku_Order_By(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.CantRegActualizados;
        }
    }

    public abstract class M_Update_Module_Group_Sku_Graficos
    {
        public int Update(Request_Update_Grafico_Sku oRq)
        {
            Response_Update_Grafico_Sku oRp;

            oRp = MvcApplication._Deserialize<Response_Update_Grafico_Sku>(
                    MvcApplication._Servicio_Operativa.Update_Module_Graficos_Sku(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.CantRegActualizados;
        }
    }

    public abstract class M_Module_ValAnalista
    {
        public int UpdateValAnalista(Request_ValidacionAnalista oRq)
        {
            M_ValAnalista oRp;

            oRp = MvcApplication._Deserialize<M_ValAnalista>(
                    MvcApplication._Servicio_Operativa.ValidacionAnalista(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oBjCantidadUpd;
        }
    }

    /// <summary>
    /// Autor:wlopez
    /// Fecha:28/09/2016
    /// </summary>
    public abstract class M_Module_ValFotografico
    {
        public int UpdateValFotografico(Request_ValidacionFotografico oRq)
        {
            M_ValFotografico oRp;

            oRp = MvcApplication._Deserialize<M_ValFotografico>(
                    MvcApplication._Servicio_Operativa.ValidacionFotografico(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.oBjCantidadUpd;
        }
    }

    public abstract class M_Module_ValAnalista_Periodos
    {
        public List<Module_ValidacionAnalista_Periodo> Lista(Request_ValidacionAnalista_Perido oRq)
        {
            Response_ValidacionAnalista_Perido oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionAnalista_Perido>(
                    MvcApplication._Servicio_Operativa.BL_Module_ValidacionAnalista_Perido(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }
    /// <summary>
    /// Autor:wlopez
    /// Fecha:27/09/2016
    /// </summary>
    public abstract class M_Module_ValAnalista_Periodosfoto
    {
        public List<Module_ValidacionAnalista_Periodofoto> Lista(Request_ValidacionAnalista_Peridofoto oRq)
        {
            Response_ValidacionAnalista_Peridofoto oRp;

            oRp = MvcApplication._Deserialize<Response_ValidacionAnalista_Peridofoto>(
                    MvcApplication._Servicio_Operativa.BL_Module_ValidacionAnalista_Peridofoto(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class M_Respone_New_Group_Sku_Graf {
        public int Cod_Grupo(Request_New_Group_Sku_Graf oRq)
        {
            Respone_New_Group_Sku_Graf oRp;

            oRp = MvcApplication._Deserialize<Respone_New_Group_Sku_Graf>(
                    MvcApplication._Servicio_Operativa.BL_New_Group_Sku_Graf(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.id_grupo_new;
        }

        public int Inser_New_Grupo_Sku_Graf(Request_Insert_New_Group_Sku_Graf oRq)
        {
            Respone_New_Group_Sku_Graf oRp;

            oRp = MvcApplication._Deserialize<Respone_New_Group_Sku_Graf>(
                    MvcApplication._Servicio_Operativa.Bl_Insert_New_Group_Sku_Graf(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.id_grupo_new;
        }
    }

    public abstract class M_Consulta_Rango_Mandatorio {
        public List<E_Rango_Mandatorio> Lista(Request_Rango_Mandatorio oRq)
        {
            Response_Rango_Mandatorio oRp;

            oRp = MvcApplication._Deserialize<Response_Rango_Mandatorio>(
                    MvcApplication._Servicio_Operativa.Bl_Consulta_Module_Rangos_Mandatorios(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region << Rquest Response >>

    public class Request_Precio_Sugerido
    {
        [JsonProperty("_a")]
        public int periodo { get; set; }
    }
    public class Response_Precio_Sugerido
    {
        [JsonProperty("_a")]
        public List<Module_PrecSugerido_Colgate> Lista { get; set; }
    }

    public class Request_Precio_Sugerido_Descarga
    {
        [JsonProperty("_a")]
        public string id_planning { get; set; }
        [JsonProperty("_b")]
        public int report_id { get; set; }
        [JsonProperty("_c")]
        public int id_reportsplanning { get; set; }
    }
    public class Response_Precio_Sugerido_Descarga
    {
        [JsonProperty("_a")]
        public List<Module_PrecSugerido_Col_Descarga> Lista { get; set; }
    }

    public class Request_BulkCopy_Module_PrecSug_Col
    {
        [JsonProperty("_a")]
        public List<Module_PrecSugerido_Colgate> Lista { get; set; }
    }
    public class Response_BulkCopy_Module_PrecSug_Col
    {
        [JsonProperty("_a")]
        public List<Module_Response_PrecSugerido_Col> cantidad { get; set; }
    }

    public class Request_Codigo_Rango_Sku
    {
        [JsonProperty("_a")]
        public int numrango { get; set; }
    }
    public class Response_Codigo_Rango_Sku
    {
        [JsonProperty("_a")]
        public List<Codigo_Module_Range_Sku_Mandatory> Lista { get; set; }
    }

    public class Request_Insert_Module_RangoSku_Madatorio
    {
        [JsonProperty("a")]
        public int id_reportsPlanning { get; set; }
        [JsonProperty("b")]
        public string cod_range { get; set; }
        [JsonProperty("c")]
        public int Mandatorio_RangeDesde { get; set; }
        [JsonProperty("d")]
        public int Mandatorio_RangeHasta { get; set; }
    }
    public class Request_Update_Module_SkuGraficos
    {
        [JsonProperty("a")]
        public string cod_product { get; set; }
        [JsonProperty("b")]
        public int id_reportsplanning { get; set; }
        [JsonProperty("c")]
        public int report_id { get; set; }
        [JsonProperty("d")]
        public int company_id { get; set; }
        [JsonProperty("e")]
        public int id_grilla { get; set; }
        [JsonProperty("f")]
        public string id_productcategory { get; set; }
    }

    public class Request_ValidacionAnalista
    {
        [JsonProperty("_a")]
        public string periodoVal { get; set; }

        [JsonProperty("_b")]
        public string periodoInVal { get; set; }
    }

    /// <summary>
    /// Autor:wlopez
    /// Fecha:28/09/2016
    /// </summary>
    public class Request_ValidacionFotografico
    {
        [JsonProperty("_a")]
        public string periodoVal { get; set; }

        [JsonProperty("_b")]
        public string periodoInVal { get; set; }
    }

    public class Request_ValidacionAnalista_Perido
    {
        [JsonProperty("a")]
        public string id_planning { get; set; }
        [JsonProperty("b")]
        public string Report_Id { get; set; }
        [JsonProperty("c")]
        public int id_Year { get; set; }
        [JsonProperty("d")]
        public string id_Month { get; set; }
    }
    /// <summary>
    /// Autor:wlopez
    /// Fecha:27/09/2016
    /// </summary>
    public class Request_ValidacionAnalista_Peridofoto
    {
        [JsonProperty("a")]
        public string id_planning { get; set; }
        [JsonProperty("b")]
        public string Report_Id { get; set; }
        [JsonProperty("c")]
        public int id_Year { get; set; }
        [JsonProperty("d")]
        public string id_Month { get; set; }
    }
    public class Response_ValidacionAnalista_Peridofoto
    {
        [JsonProperty("a")]
        public List<Module_ValidacionAnalista_Periodofoto> Lista { get; set; }
    }

    public class Response_ValidacionAnalista_Perido
    {
        [JsonProperty("a")]
        public List<Module_ValidacionAnalista_Periodo> Lista { get; set; }
    }

    public class Request_Update_Grafico_Sku
    {
        [JsonProperty("a")]
        public string cod_product { get; set; }
        [JsonProperty("b")]
        public int id_reportsplanning { get; set; }
        [JsonProperty("c")]
        public int id_grupo { get; set; }
        [JsonProperty("d")]
        public int id_grilla { get; set; }
        [JsonProperty("e")]
        public string id_productcategory { get; set; }
    }
    public class Response_Update_Grafico_Sku
    {
        [JsonProperty("a")]
        public int CantRegActualizados { get; set; }
    }

    public class Request_New_Group_Sku_Graf
    {
        [JsonProperty("a")]
        public int id_reportsplanning { get; set; }
        [JsonProperty("b")]
        public int id_grilla { get; set; }
        [JsonProperty("c")]
        public string id_productcategory { get; set; }
    }

    public class Request_Insert_New_Group_Sku_Graf
    {
        [JsonProperty("a")]
        public string cod_product { get; set; }
        [JsonProperty("b")]
        public int id_reportsplanning { get; set; }
        [JsonProperty("c")]
        public int id_grilla { get; set; }
        [JsonProperty("d")]
        public int id_grupo { get; set; }
        [JsonProperty("e")]
        public string user { get; set; }
        [JsonProperty("f")]
        public string id_productCategory { get; set; }
    }

    public class Request_Rango_Mandatorio
    {
        [JsonProperty("a")]
        public int cod_periodo { get; set; }
    }
    public class Response_Rango_Mandatorio
    {
        public List<E_Rango_Mandatorio> Lista { get; set; }
    }

    #endregion



    #endregion
}

