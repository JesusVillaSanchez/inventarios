﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;
namespace Lucky.Xplora.Models.Module
{

    public class Module_Response_SKUMandatory : M_Module_Lista_SKUMandatory
    {
        [JsonProperty("_a")]
        public string comentario { get; set; }
        [JsonProperty("_b")]
        public int cantidad { get; set; }
        //[JsonProperty("_c")]
        //public int update { get; set; }
    }

    public class M_SKUMandatory : M_Module_Descarga_SKUMandatory
    {
        [JsonProperty("_e", NullValueHandling = NullValueHandling.Ignore)]
        public string periodo { get; set; }
        [JsonProperty("_j", NullValueHandling = NullValueHandling.Ignore)]
        public string product_Category { get; set; }
        [JsonProperty("_l", NullValueHandling = NullValueHandling.Ignore)]
        public string name_Brand { get; set; }

        [JsonProperty("_g", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_Product { get; set; }
        [JsonProperty("_h", NullValueHandling = NullValueHandling.Ignore)]
        public string product_Name { get; set; }

        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public int id_ReportsPlanning { get; set; }
        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string id_Planning { get; set; }
        [JsonProperty("_c", NullValueHandling = NullValueHandling.Ignore)]
        public int id_Year { get; set; }
        [JsonProperty("_d", NullValueHandling = NullValueHandling.Ignore)]
        public string id_Month { get; set; }

        [JsonProperty("_f", NullValueHandling = NullValueHandling.Ignore)]
        public int id_Product { get; set; }

        [JsonProperty("_i", NullValueHandling = NullValueHandling.Ignore)]
        public int id_ProductCategory { get; set; }

        [JsonProperty("_k", NullValueHandling = NullValueHandling.Ignore)]
        public string id_Brand { get; set; }

        [JsonProperty("_m", NullValueHandling = NullValueHandling.Ignore)]
        public int sku_Mandatario { get; set; }

        [JsonProperty("_ñ", NullValueHandling = NullValueHandling.Ignore)]
        public string createby { get; set; }
        [JsonProperty("_o", NullValueHandling = NullValueHandling.Ignore)]
        public string archivo { get; set; }
    }

    public abstract class M_Module_Lista_SKUMandatory
    {
        //PARA MANDATORIOS
        public List<Module_Response_SKUMandatory> BulkCopySKUsMandatory(Request_BulkCopy_Module_SKUMandatory oRq)
        {
            Response_BulkCopy_Module_SKUMandatory oRp;

            oRp = MvcApplication._Deserialize<Response_BulkCopy_Module_SKUMandatory>(
                    MvcApplication._Servicio_Operativa.BulkCopy_SKUsMandatorios(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.cantidad;
        }

    }
    public abstract class M_Module_Descarga_SKUMandatory
    {
        public List<M_SKUMandatory> Lista(Request_SKUMandatory_Descarga oRq)
        {
            Response_SKUMandatory_Descarga oRp;

            oRp = MvcApplication._Deserialize<Response_SKUMandatory_Descarga>(
                    MvcApplication._Servicio_Operativa.Module_SKUMandatory_Colgate(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Lista;
        }



    }




    #region << Rquest Response >>

        public class Request_SKUMandatory_Descarga
        {
            [JsonProperty("_a")]
            public int periodo { get; set; }
        }
        public class Response_SKUMandatory_Descarga
        {
            [JsonProperty("_a")]
        public List<M_SKUMandatory> Lista { get; set; }
        }

    #endregion


    public class Modulo_Carga_SkuMandatory
    {
        [JsonProperty("_a")]
        public int id_reportsplanning { get; set; }

        [JsonProperty("_b")]
        public int id_product { get; set; }

        [JsonProperty("_c")]
        public string cod_product { get; set; }

        [JsonProperty("_d")]
        public char estado { get; set; }

        [JsonProperty("_e")]
        public string createby { get; set; }

        [JsonProperty("_f")]
        public DateTime dateby { get; set; }

        [JsonProperty("_g")]
        public string modiby { get; set; }

        [JsonProperty("_h")]
        public DateTime datemodiby { get; set; }

        [JsonProperty("_i")]
        public string archivo { get; set; }
    }


    //public class Request_BulkCopy_Module_SKUsMandatory
    //{
    //    [JsonProperty("_a")]
    //    public List<M_SKUMandatory> Lista { get; set; }
    //}
    //public class Response_BulkCopy_Module_SKUsMandatory
    //{
    //    [JsonProperty("_a")]
    //    public List<M_SKUMandatory> cantidad { get; set; }
    //}


    public class Request_BulkCopy_Module_SKUMandatory
    {
        [JsonProperty("_b")]
        public int periodo { get; set; }

        [JsonProperty("_a")]
        public List<M_SKUMandatory> Lista { get; set; }
    }
    public class Response_BulkCopy_Module_SKUMandatory
    {
        [JsonProperty("_a")]
        public List<Module_Response_SKUMandatory> cantidad { get; set; }
    }

}