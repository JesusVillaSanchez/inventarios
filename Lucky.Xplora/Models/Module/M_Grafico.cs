﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Module
{
    public class E_Grafico : Module_Consulta_Grafico
    {
        [JsonProperty("a")]
        public string codigo { get; set; }

        [JsonProperty("b")]
        public string grafico { get; set; }
    }

    public abstract class Module_Consulta_Grafico {

        public List<E_Grafico> Lista(Consulta_Graficos_Request oRq)
        {
            Consulta_Graficos_Response oRp;

            oRp = MvcApplication._Deserialize<Consulta_Graficos_Response>(
                MvcApplication._Servicio_Campania.BL_Graficos_Por_Canal(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

    }


    #region Request & Response

    public class Consulta_Graficos_Request
    {
        [JsonProperty("a")]
        public int cod_canal { get; set; }
    }

    public class Consulta_Graficos_Response
    {
        [JsonProperty("a")]
        public List<E_Grafico> Lista { get; set; }
    }

    #endregion
}