﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Ciudad : ACiudad
    {
        [JsonProperty("_a", NullValueHandling = NullValueHandling.Ignore)]
        public string ciu_ubigeo { get; set; }

        [JsonProperty("_b", NullValueHandling = NullValueHandling.Ignore)]
        public string ciu_descripcion { get; set; }
    }

    public abstract class ACiudad {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-07
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Ciudad> Lista(MapMultiGPS_Request oRq)
        {
            List<Ciudad> oLs = new List<Ciudad>();
            MapMultiGPS_Filtro_Response oRp;

            oRp = MvcApplication._Deserialize<MapMultiGPS_Filtro_Response>(
                    MvcApplication._Servicio_Maps.Consul_Filtro_MultiGPS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            /*obtiene resultado y lo llena en listado de campanias*/
            if (oRp.Lista != null && oRp.Lista.Count > 0)
            {
                foreach (M_Combo oBj in oRp.Lista)
                {
                    oLs.Add(new Ciudad()
                    {
                        ciu_ubigeo = oBj.Value,
                        ciu_descripcion = oBj.Text
                    });
                }
            }

            return oLs;
        }
    }
}