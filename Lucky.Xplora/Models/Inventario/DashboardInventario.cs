﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.Models.Inventario
{
    public class DashboardInventario
    {
        public class LocalizadoData
        {
            public string Fecha { get; set; }
            public int? Salida { get; set; }
            public int? Stock { get; set; }
            public int? Promedio { get; set; }
        }

        public class BuscadoData
        {
            //public string Fecha { get; set; }
            //public int? UT { get; set; }
            //public int? UD { get; set; }
            //public int? PD { get; set; }

            public List<string> labels { get; set; }
            public List<BuscadoDataSet> datasets { get; set; }

        }

        public class BuscadoDataSet
        {
            public string label { get; set; }
            public double lineTension { get; set; }
            public string backgroundColor { get; set; }
            public string borderColor { get; set; }
            public int pointRadius { get; set; }
            public string pointBackgroundColor { get; set; }
            public string pointBorderColor { get; set; }
            public int pointHoverRadius { get; set; }
            public string pointHoverBackgroundColor { get; set; }
            public int pointHitRadius { get; set; }
            public int pointBorderWidth { get; set; }
            public List<int?> data { get; set; }
        }
    }
}