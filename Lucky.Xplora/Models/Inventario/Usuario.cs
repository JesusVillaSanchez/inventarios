﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lucky.Xplora.Models.Inventario
{
    public class UsuarioModel
    {
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
        public string Email { get; set; }
        public int IdRol { get; set; }
        public string Rol { get; set; }
        public bool Estado { get; set; }

    }

    public class RolModel {
        public int IdRol { get; set; }
        public string Rol { get; set; }
        public bool Estado { get; set; }
    }

    public class CategoriaModel {
        public int IdCategoria { get; set; }
        public string Categoria { get; set; }
        public bool Estado { get; set; }
    }

    public class ProductoModel {
        public int IdProducto { get; set; }
        public string Producto { get; set; }
        public string Descripcion { get; set; }
        public int IdCategoria { get; set; }
        public string Categoria { get; set; }
        public int Stock { get; set; }
        public float Precio { get; set; }
        public bool Estado { get; set; }
    }

    public class InventarioModel
    {
        public int IdInventario { get; set; }
        public int IdProducto { get; set; }
        public string Producto { get; set; }
        public int Ingreso { get; set; }
        public int Stock { get; set; }
        public int Salida { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public int Tipo { get; set; }
    }

    public class InventarioCategoriaModel
    {
        public int IdInventario { get; set; }
        public int IdProducto { get; set; }
        public string Producto { get; set; }
        public int IdCategoria { get; set; }
        public int Ingreso { get; set; }
        public int Stock { get; set; }
        public int Salida { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public int Tipo { get; set; }
    }

    public class ReporteModel
    {
        public int Item { get; set; }
        public DateTime? Fecha { get; set; }
        public string Descripcion { get; set; }
        public int Salida { get; set; }
        public int Stock { get; set; }
        public string Promedio { get; set; }
        
    }
}
