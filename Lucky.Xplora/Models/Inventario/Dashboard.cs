﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.Models.Inventario
{
    
        public class Indicador {
            public List<string> labels { get; set; }
            public List<DatoValue> datasets { get; set; }
        }
        public class DatoValue {
            public string label { get; set; } 
			public bool fill { get; set; }
            public int lineTension { get; set; }
            public string backgroundColor { get; set; }
            public int borderWidth { get; set; }
            public string borderColor { get; set; }
            public string borderCapStyle { get; set; }
            public string[] borderDash { get; set; } 
			public double borderDashOffset { get; set; }
            public string borderJoinStyle { get; set; }
            //pointStyle: 'cross',
            public int pointRadius { get; set; }
            public string pointBorderColor { get; set; }
            public string pointBackgroundColor { get; set; }
            public int pointBorderWidth { get; set; }
            public int pointHoverRadius { get; set; }
            public string pointHoverBackgroundColor { get; set; }
            public string pointHoverBorderColor { get; set; }
            public int pointHoverBorderWidth { get; set; }
            //pointRadius: 4,
            //pointHitRadius: 5,
            public List<int> data { get; set; }//: [20, 24, 32, 34, 38, 35, 37, 40, 53, 60, 62],
            public bool spanGaps { get; set; }
        }
    
}