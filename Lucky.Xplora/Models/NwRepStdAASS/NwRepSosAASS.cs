﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Models.NwRepStdAASS
{
    public class Sos_Aje : A_Sos_Aje
    {
        [JsonProperty("_a")]
        public List<E_Sos_Participation_Brand> SosxMarca { get; set; }
        [JsonProperty("_b")]
        public List<E_Sos_Participation_Company> SosxEmpresa { get; set; }
        [JsonProperty("_c")]
        public List<E_Sos_Ranking_PDV> SosxPDVRanking { get; set; }
        [JsonProperty("_d")]
        public List<E_Sos_Participation_CompanyxBrand> SosxCompanyxBrand { get; set; }
    }
    public class E_Sos_Participation_Brand
    {
        [JsonProperty("_a")]
        public string company { get; set; }
        [JsonProperty("_b")]
        public string brand { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }
    public class E_Sos_Participation_Company
    {
        [JsonProperty("_a")]
        public string rpl_description { get; set; }
        [JsonProperty("_b")]
        public string company { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }
    public class E_Sos_Ranking_PDV
    {
        [JsonProperty("_a")]
        public string pdv_name { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }
    public class E_Sos_Participation_CompanyxBrand
    {
        [JsonProperty("_a")]
        public string ncm_name { get; set; }
        [JsonProperty("_b")]
        public string company { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; } 
    }

    public class Sos_Aje_EvoBrand
    {
        [JsonProperty("_a")]
        public List<E_Sos_Evolutivo_Brand> SosEvobrand { get; set; }
    }
    public class E_Sos_Evolutivo_Brand
    {
        [JsonProperty("_a")]
        public string rpl_description { get; set; }
        [JsonProperty("_b")]
        public string brand { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }

    public abstract class A_Sos_Aje
    {
        public Sos_Aje Objeto_Sos(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Sos oRp = MvcApplication._Deserialize<Response_AjeReporting_Sos>(
                    MvcApplication._Servicio_Operativa.Aje_Sos(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public E_Sos_Consulta_Brand Consultar_Marca(Request_Sos_Aje_Brand oRq)
        {
            Response_Sos_Aje_Brand oRp;

            oRp = MvcApplication._Deserialize<Response_Sos_Aje_Brand>(MvcApplication._Servicio_Operativa.Aje_Sos_Consulta_Marca(MvcApplication._Serialize(oRq)));

            return oRp.Obj;
        }

        public Sos_Aje_EvoBrand Objeto_Sos_EvoBrand(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Sos_EvoBrand oRp = MvcApplication._Deserialize<Response_AjeReporting_Sos_EvoBrand>(
                    MvcApplication._Servicio_Operativa.Aje_Sos(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    /*Descarga*/
    public class E_Sos_Detalle : A_Sos_Detalle
    {
        [JsonProperty("a")]
        public string gie { get; set; }
        [JsonProperty("b")]
        public string cadenas { get; set; }
        [JsonProperty("c")]
        public string pdv { get; set; }
        [JsonProperty("d")]
        public string categoria { get; set; }
        [JsonProperty("e")]
        public string fecha_celular { get; set; }
        [JsonProperty("f")]
        public string tipomaterial { get; set; }
        [JsonProperty("g")]
        public string marca { get; set; }
        [JsonProperty("h")]
        public string cantidad { get; set; }
        [JsonProperty("i")]
        public string total { get; set; }
        [JsonProperty("j")]
        public string foto { get; set; }
        [JsonProperty("k")]
        public string tipoobservacion { get; set; }
        [JsonProperty("l")]
        public string medida { get; set; }
        [JsonProperty("m")]
        public string observacion { get; set; }
    }

    public abstract class A_Sos_Detalle
    {
        public List<E_Sos_Detalle> Lista_SosDetalle(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Sos_Detalle oRp = MvcApplication._Deserialize<Response_AjeReporting_Sos_Detalle>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Sos_Detalle_AASS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    public class E_Sos_Detalle_Cumplimiento_Export : A_Sos_Detalle_Cumplimiento_Export
    {
        [JsonProperty("a")]
        public string cadena { get; set; }
        [JsonProperty("b")]
        public string pdv { get; set; }
        [JsonProperty("c")]
        public string tipomaterial { get; set; }
        [JsonProperty("d")]
        public string empresa { get; set; }
        [JsonProperty("e")]
        public string ciudad { get; set; }
        [JsonProperty("f")]
        public string marca { get; set; }
        [JsonProperty("g")]
        public string cantidad { get; set; }

        [JsonProperty("h")]
        public string cordinado { get; set; }
        [JsonProperty("i")]
        public string porcentaje { get; set; }
    }

    public abstract class A_Sos_Detalle_Cumplimiento_Export
    {
        public List<E_Sos_Detalle_Cumplimiento_Export> Lista_SosCumplimiento(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Sos_Cumplimiento oRp = MvcApplication._Deserialize<Response_AjeReporting_Sos_Cumplimiento>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Sos_Detalle_Cumpplimiento(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<E_Sos_Detalle_Cumplimiento_Export> Lista_SosCumplimiento_Exprotar_Detalle(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Sos_Cumplimiento_Detalle oRp = MvcApplication._Deserialize<Response_AjeReporting_Sos_Cumplimiento_Detalle>(
                    MvcApplication._Servicio_Operativa.NW_Aje_Cumplimiento_Detalle_Exportar_2(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

       
    }
    //public abstract class A_Sos_Detalle_Cumplimiento_Export
    //{
        
    //}

    #region Request & Response

    public class Response_AjeReporting_Sos
    {
        [JsonProperty("_a")]
        public Sos_Aje Objeto { get; set; }
    }

    public class Response_AjeReporting_Sos_EvoBrand
    {
        [JsonProperty("_a")]
        public Sos_Aje_EvoBrand Objeto { get; set; }
    }

    public class Response_AjeReporting_Sos_Detalle
    {
        [JsonProperty("_a")]
        public List<E_Sos_Detalle> Objeto { get; set; }
    }

    public class Response_AjeReporting_Sos_Cumplimiento
    {
        [JsonProperty("_a")]
        public List<E_Sos_Detalle_Cumplimiento_Export> Objeto { get; set; }
    }

    public class Response_AjeReporting_Sos_Cumplimiento_Detalle
    {
        [JsonProperty("_a")]
        public List<E_Sos_Detalle_Cumplimiento_Export> Objeto { get; set; }
    }

    #endregion

    #region Brand

    public class E_Sos_Consulta_Brand
    {
        [JsonProperty("_a")]
        public string Cod_Marca { get; set; }
        [JsonProperty("_b")]
        public string Marca { get; set; }
    }

    #region Request & Response

    public class Request_Sos_Aje_Brand
    {
        [JsonProperty("_a")]
        public string Text { get; set; }
    }
    public class Response_Sos_Aje_Brand
    {
        [JsonProperty("_a")]
        public E_Sos_Consulta_Brand Obj { get; set; }
    }

    #endregion


    #endregion

    public class Sos_RpAASS_Cumplimiento_detalle_V2 {
        public E_Cumplimiento_Detalle_AASS Lista_SosCumplimiento_Detalle_v2(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeRp_Sos_Cumplimiento_Detalle oRp = MvcApplication._Deserialize<Response_AjeRp_Sos_Cumplimiento_Detalle>(
                    MvcApplication._Servicio_Operativa.Henkel_RpSosDetalle_v2(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;

            return null;
        }
    }

#region Entity
    public class E_Cumplimiento_Detalle_AASS
    {

        [JsonProperty("_a")]
        public List<E_Cumplimiento_Detalle_AASS_rows> Filas { get; set; }
        [JsonProperty("_b")]
        public List<E_Cumplimiento_Detalle_AASS_Cab> Cabeceras { get; set; }
    }

    // crear clase 
    public class E_Cumplimiento_Detalle_AASS_rows
    {
        [JsonProperty("_a")]
        public string Cadena { get; set; }

        [JsonProperty("_b")]
        public string Pdv { get; set; }

        [JsonProperty("_c")]
        public string Ciudad { get; set; }

        [JsonProperty("_d")]
        public List<E_Cumplimiento_Detalle_AASS_celda> Celdas { get; set; }
    }
    public class E_Cumplimiento_Detalle_AASS_Cab
    {
        [JsonProperty("_a")]
        public string Empresa { get; set; }

        [JsonProperty("_b")]
        public List<String> Marcas { get; set; }
    }
    public class E_Cumplimiento_Detalle_AASS_celda
    {
        [JsonProperty("_a")]
        public String Valor { get; set; }
        [JsonProperty("_b")]
        public String Color { get; set; }
    } 
#endregion
#region Response
    public class Response_AjeRp_Sos_Cumplimiento_Detalle
    {
        [JsonProperty("_a")]
        public E_Cumplimiento_Detalle_AASS Objeto { get; set; }
    }
#endregion
    
    }