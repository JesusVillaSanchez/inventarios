﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Models.NwRepStdAASS
{
    public class NwRepPrecioAASS
    {
        public E_Precio_AASS Consulta_Reporte(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Precio oRp = MvcApplication._Deserialize<Response_NWRepStd_Precio>(MvcApplication._Servicio_Operativa.NwRepStd_Consultar_PreciosAASS(MvcApplication._Serialize(request)));
                                                                                                                                 //.NwRepStd_Consultar_PreciosAASS
            //.NwRepStd_Consultar_QuiebreAASS
            return oRp.Objeto;
        }
        //hacer excel edwin 2
        public E_Index_Precio_AASS Consulta_IndexPrecio(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Precio_Index oRp = MvcApplication._Deserialize<Response_NWRepStd_Precio_Index>(MvcApplication._Servicio_Operativa.NwRepStd_Consultar_IndexPreciosAASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<M_Combo> Filtro(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Filtro oRp = MvcApplication._Deserialize<Response_NWRepStd_Filtro>
                (MvcApplication._Servicio_Operativa.NwRepStd_Filtros_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        //guia excel edwin 1
        public List<E_Excel_Precio_AASS> Excel_Precio(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Excel_Precio oRp = MvcApplication._Deserialize
           <Response_NWRepStd_Excel_Precio>(MvcApplication._Servicio_Operativa.NwRepStd_Excel_PreciosAASS
                (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        //haciendo
        public List<E_excel_index_price_AA_SS_edw> Excel_index_Precio_edw(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Excel_index_Precio_edw oRp = MvcApplication._Deserialize
           <Response_NWRepStd_Excel_index_Precio_edw>(MvcApplication._Servicio_Operativa.Nw_excel_Index_Precio_AASS_edw
           (MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        //duplicado Nw_Exportar_Index_Precio_2
        public List<E_excel_index_price_AA_SS_edw> Excel_index_Precio_2(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Excel_index_Precio_edw oRp = MvcApplication._Deserialize
           <Response_NWRepStd_Excel_index_Precio_edw>(MvcApplication._Servicio_Operativa.Nw_Exportar_Index_Precio_2
           (MvcApplication._Serialize(request)));
            return oRp.Objeto;
        }
        public E_Precio_AASS Consulta_ReporteAlicorpAASS(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Precio oRp = MvcApplication._Deserialize<Response_NWRepStd_Precio>(MvcApplication._Servicio_Operativa.NwRepStd_Consultar_QuiebreAASS(MvcApplication._Serialize(request)));
            //.NwRepStd_Consultar_PreciosAASS
            //.NwRepStd_Consultar_QuiebreAASS
            return oRp.Objeto;
        }
    }

    #region <<Entidad>>
    public class E_Precio_AASS
    {
        [JsonProperty("_a")]
        public List<String> ListPeriodos { get; set; }
        [JsonProperty("_b")]
        public List<String> ListCadena { get; set; }
        [JsonProperty("_c")]
        public List<E_Precio_AASS_Fila> Filas { get; set; }
        [JsonProperty("_d")]
        public List<E_Precio_AASS_Universo> Listacadena { get; set; }
    }
    public class E_Precio_AASS_Fila
    {
        [JsonProperty("_a")]
        public String Empresa { get; set; }
        [JsonProperty("_b")]
        public String Presentacion { get; set; }
        [JsonProperty("_c")]
        public String Descripcion { get; set; }
        [JsonProperty("_d")]
        public List<E_Precio_AASS_Periodo> Periodos { get; set; }
        [JsonProperty("_e")]
        public List<E_Precio_AASS_Cadena> Cadenas { get; set; }
        [JsonProperty("_f")]
        public String peso { get; set; }
        [JsonProperty("_g")]
        public String Categoria { get; set; }
        [JsonProperty("_h")]
        public String Marca { get; set; }
    }
    public class E_Precio_AASS_Universo
    {
        [JsonProperty("a")]
        public int Cadena { get; set; }
        [JsonProperty("b")]
        public int cantidad { get; set; }
    }
    public class E_Precio_AASS_Periodo
    {
        [JsonProperty("_a")]
        public String Periodo { get; set; }
        [JsonProperty("_b")]
        public String P_Regular { get; set; }
        [JsonProperty("_c")]
        public String P_Oferta { set; get; }
        [JsonProperty("_d")]
        public String Margen { get; set; }
        [JsonProperty("_e")]
        public String Cod_TOferta { get; set; }
        [JsonProperty("_f")]
        public String Des_TOferta { get; set; }
        [JsonProperty("_g")]
        public String P_Peso { get; set; }
    }
    public class E_Precio_AASS_Cadena
    {
        [JsonProperty("_a")]
        public String P_Moda { get; set; }
        [JsonProperty("_b")]
        public String Cod_TOferta { get; set; }
        [JsonProperty("_c")]
        public String Des_TOferta { get; set; }
    }

    //

    public class E_Index_Precio_AASS
    {
        [JsonProperty("_a")]
        public List<String> Cadenas { get; set; }
        [JsonProperty("_b")]
        public List<E_Index_Precio_grup_AASS> Grupos { get; set; }
    }
    public class E_Index_Precio_grup_AASS
    {
        [JsonProperty("_a")]
        public List<E_Index_Precio_Row_AASS> Filas { get; set; }
    }
    public class E_Index_Precio_Row_AASS
    {
        [JsonProperty("_a")]
        public String Presentacion { get; set; }
        [JsonProperty("_b")]
        public String Producto { get; set; }
        [JsonProperty("_d")]
        public string Propio { get; set; }
        [JsonProperty("_c")]
        public List<E_Index_Precio_cadena_AASS> Cadenas { get; set; }
    }
    public class E_Index_Precio_cadena_AASS
    {
        [JsonProperty("_a")]
        public String P_Regular { get; set; }
        [JsonProperty("_b")]
        public String Index { get; set; }
        [JsonProperty("_c")]
        public String P_Peso { get; set; }
        [JsonProperty("_d")]
        public String N_Index { get; set; }
        [JsonProperty("_e")]
        public String T_precio { get; set; }
    }
    public class E_Excel_Precio_AASS
    {

        [JsonProperty("_a")]
        public String Cadena { get; set; }
        [JsonProperty("_b")]
        public String Cod_PDV { get; set; }
        [JsonProperty("_c")]
        public String PDV { get; set; }
        [JsonProperty("_d")]
        public String Empresa { get; set; }
        [JsonProperty("_e")]
        public String Presentacion { get; set; }
        [JsonProperty("_f")]
        public String Categoria { get; set; }
        [JsonProperty("_g")]
        public String Marca { get; set; }
        [JsonProperty("_h")]
        public String Producto { get; set; }
        [JsonProperty("_i")]
        public String P_Regular { get; set; }
        [JsonProperty("_j")]
        public String P_Oferta { get; set; }
        [JsonProperty("_k")]
        public String Tipoprecio { get; set; }
        [JsonProperty("_l")]
        public String Periodo { get; set; }
        [JsonProperty("_m")]
        public String Tienda { get; set; }
        [JsonProperty("_n")]
        public String Cantidad { get; set; }
        [JsonProperty("_ñ")]
        public String Mes { get; set; }
        [JsonProperty("_o")]
        public String Anio { get; set; }
        [JsonProperty("_p")]
        public String nombre { get; set; }


    }

    public class E_excel_index_price_AA_SS_edw
    {

        [JsonProperty("_a")]
        public String codigo { get; set; }
        [JsonProperty("_b")]
        public String orden { get; set; }
        [JsonProperty("_c")]
        public String nombre { get; set; }

        [JsonProperty("_d")]
        public String Cod_periodo { get; set; }
        [JsonProperty("_e")]
        public String Cod_cadena { get; set; }
        [JsonProperty("_f")]
        public String Id_product { get; set; }
        [JsonProperty("_g")]
        public String Propio { get; set; }
        [JsonProperty("_h")]
        public String Cod_padre { get; set; }

        [JsonProperty("_i")]
        public String moda_propio { get; set; }
        [JsonProperty("_j")]
        public String indice_propio { get; set; }
        [JsonProperty("_k")]
        public String p_peso { get; set; }
        [JsonProperty("_l")]
        public String new_index { get; set; }
        [JsonProperty("_m")]
        public String nom_producto { get; set; }
        [JsonProperty("_o")]
        public String nom_presentacion { get; set; }

    }

    #endregion
    #region <<Request Response>>
    public class Request_NWRepStd_General 
    {
        [JsonProperty("_a")]
        public String Parametros { get; set; }
        [JsonProperty("_b")]
        public int op { get; set; }
        [JsonProperty("_c")]
        public String subcanal { get; set; }
        [JsonProperty("_d")]
        public String cadena { get; set; }
        [JsonProperty("_e")]
        public String categoria { get; set; }
        [JsonProperty("_f")]
        public String empresa { get; set; }
        [JsonProperty("_g")]
        public String marca { get; set; }
        [JsonProperty("_h")]
        public String SKU { get; set; }
        [JsonProperty("_i")]
        public int periodo { get; set; }
        [JsonProperty("_j")]
        public String zona { get; set; }
        [JsonProperty("_k")]
        public String distrito { get; set; }
        [JsonProperty("_l")]
        public String tienda { get; set; }
        [JsonProperty("_m")]
        public String segmento { get; set; }
        [JsonProperty("_n")]
        public int Persona { get; set; }
        [JsonProperty("_o")]
        public int mes { get; set; }
        [JsonProperty("_p")]
        public int anio { get; set; }
        [JsonProperty("_q")]
        public int idtipo_material { get; set; }
        [JsonProperty("_r")]
        public string min { get; set; }
        [JsonProperty("_s")]
        public string max { get; set; }
    }
    public class Response_NWRepStd_Filtro 
    {
        [JsonProperty("_a")]
        public List<M_Combo> Objeto { get; set; }
    }

    public class Response_NWRepStd_Precio
    {
        [JsonProperty("_a")]
        public E_Precio_AASS Objeto { get; set; }
    }
    public class Response_NWRepStd_Precio_Index
    {
        [JsonProperty("_a")]
        public E_Index_Precio_AASS Objeto { get; set; }
    }
    public class Response_NWRepStd_Excel_Precio
    {
        [JsonProperty("_a")]
        public List<E_Excel_Precio_AASS> Objeto { get; set; }
    }

    public class Response_NWRepStd_Excel_index_Precio_edw
    {
        [JsonProperty("_a")]
        public List<E_excel_index_price_AA_SS_edw> Objeto { get; set; }
    }

    #endregion
}