﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Models.NwRepStdAASS
{
    public class Incidencia_Aje : A_Incidencia_Aje
    {
        [JsonProperty("_a")]
        public List<E_Incidencia_Tipo> IncidenciaxGroup { get; set; }
        [JsonProperty("_b")]
        public List<E_Incidencia_EvoMensual> IncidenciaEvoxMes { get; set; }
        [JsonProperty("_c")]
        public List<E_Incidencia_PeriodoHead> IncidenciaEvoxMesHead { get; set; }
        [JsonProperty("_d")]
        public List<E_Incidencia_PeriodoBody> IncidenciaEvoxMesBody { get; set; }
        [JsonProperty("_e")]
        public List<E_Incidencia_RankingPersonal> IncidenciaRankingPersonal { get; set; }

        [JsonProperty("_f")]
        public List<E_Incidencia_EvoSemanal> IncidenciaEvoxSemana { get; set; }
        [JsonProperty("_g")]
        public List<E_Incidencia_SemanaloHead> IncidenciaEvoxSemanaHead { get; set; }
        [JsonProperty("_h")]
        public List<E_Incidencia_SemanalBody> IncidenciaEvoxSemanaBody { get; set; }
        [JsonProperty("_i")]
        public E_Incidencia_RPAJE_AASS IncidenciaEvoxDiarioMes { get; set; }
    }
    public class E_Incidencia_Tipo
    {
        [JsonProperty("_a")]
        public string tipoincidencia { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }
    public class E_Incidencia_EvoMensual
    {
        [JsonProperty("_a")]
        public int rpl_id { get; set; }
        [JsonProperty("_b")]
        public string rpl_descripcion { get; set; }
        [JsonProperty("_c")]
        public string solucionado_id { get; set; }
        [JsonProperty("_d")]
        public string solucionado { get; set; }
        [JsonProperty("_e")]
        public string cantidad { get; set; }
    }
    public class E_Incidencia_PeriodoHead
    {
        [JsonProperty("_a")]
        public int id_Row { get; set; }
        [JsonProperty("_b")]
        public string rpl_id { get; set; }
        [JsonProperty("_c")]
        public string rpl_descripcion { get; set; }
        [JsonProperty("_d")]
        public string llave { get; set; }
    }
    public class E_Incidencia_PeriodoBody
    {
        [JsonProperty("_a")]
        public string solucionado_Body { get; set; }
        [JsonProperty("_b")]
        public string rpl_id12 { get; set; }
        [JsonProperty("_c")]
        public string rpl_id11 { get; set; }
        [JsonProperty("_d")]
        public string rpl_id10 { get; set; }
        [JsonProperty("_e")]
        public string rpl_id9 { get; set; }
        [JsonProperty("_f")]
        public string rpl_id8 { get; set; }
        [JsonProperty("_g")]
        public string rpl_id7 { get; set; }
        [JsonProperty("_h")]
        public string rpl_id6 { get; set; }
        [JsonProperty("_i")]
        public string rpl_id5 { get; set; }
        [JsonProperty("_j")]
        public string rpl_id4 { get; set; }
        [JsonProperty("_k")]
        public string rpl_id3 { get; set; }
        [JsonProperty("_l")]
        public string rpl_id2 { get; set; }
        [JsonProperty("_m")]
        public string rpl_id1 { get; set; }
    }
    public class E_Incidencia_RankingPersonal
    {
        [JsonProperty("_a")]
        public string name { get; set; }
        [JsonProperty("_b")]
        public string perfil { get; set; }
        [JsonProperty("_c")]
        public string cadena { get; set; }
        [JsonProperty("_d")]
        public string cantidad { get; set; }
    }

    public class E_Incidencia_EvoSemanal
    {
        [JsonProperty("_a")]
        public int rpl_id { get; set; }
        [JsonProperty("_b")]
        public string rpl_descripcion { get; set; }
        [JsonProperty("_c")]
        public string solucionado_id { get; set; }
        [JsonProperty("_d")]
        public string solucionado { get; set; }
        [JsonProperty("_e")]
        public string cantidad { get; set; }
    }
    public class E_Incidencia_SemanaloHead
    {
        [JsonProperty("_a")]
        public string fecha { get; set; }
        [JsonProperty("_b")]
        public string descripcion { get; set; }
    }
    public class E_Incidencia_SemanalBody
    {
        [JsonProperty("_a")]
        public string solucionado_Body { get; set; }
        [JsonProperty("_b")]
        public string rpl_id7 { get; set; }
        [JsonProperty("_c")]
        public string rpl_id6 { get; set; }
        [JsonProperty("_d")]
        public string rpl_id5 { get; set; }
        [JsonProperty("_e")]
        public string rpl_id4 { get; set; }
        [JsonProperty("_f")]
        public string rpl_id3 { get; set; }
        [JsonProperty("_g")]
        public string rpl_id2 { get; set; }
        [JsonProperty("_h")]
        public string rpl_id1 { get; set; }
    }

    public class E_Incidencia_RPAJE_AASS
    {
        [JsonProperty("_a")]
        public List<string> listFechas { get; set; }
        [JsonProperty("_b")]
        public List<string> listLineal { get; set; }
        [JsonProperty("_c")]
        public List<string> listIncidencias { get; set; }
        [JsonProperty("_d")]
        public List<string> listIncResueltas { get; set; }
        [JsonProperty("_e")]
        public List<string> listPorcentajeInc { get; set; }
        [JsonProperty("_f")]
        public List<string> listIncNoResueltas { get; set; }
    }

    public abstract class A_Incidencia_Aje
    {
        public Incidencia_Aje Objeto_Incidencia(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Incidencia oRp = MvcApplication._Deserialize<Response_AjeReporting_Incidencia>(
                    MvcApplication._Servicio_Operativa.Aje_Incidencia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    public class E_Reporting_Incidencia_Detalle : A_Reporting_Incidencia_Detalle
    {
        [JsonProperty("a")]
        public string fecha { get; set; }
        [JsonProperty("b")]
        public string oficina { get; set; }
        [JsonProperty("c")]
        public string cadena { get; set; }
        [JsonProperty("d")]
        public string pdv_name { get; set; }
        [JsonProperty("e")]
        public string actividad { get; set; }
        [JsonProperty("f")]
        public string tipoincidencia { get; set; }
        [JsonProperty("g")]
        public string solucionado { get; set; }
        [JsonProperty("h")]
        public string supervisor { get; set; }
        [JsonProperty("i")]
        public string gie { get; set; }
        [JsonProperty("j")]
        public string perfil { get; set; }
        [JsonProperty("k")]
        public string cantidad { get; set; }
        [JsonProperty("l")]
        public string detalle { get; set; }
        [JsonProperty("m")]
        public string acciones { get; set; }
    }

    public abstract class A_Reporting_Incidencia_Detalle{
        public List<E_Reporting_Incidencia_Detalle> Lista_IncidenciaDetalle(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Incidencia_Detalle oRp = MvcApplication._Deserialize<Response_AjeReporting_Incidencia_Detalle>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Incidencia_Detalle_AASS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

     #region Request & Response

    public class Response_AjeReporting_Incidencia
    {
        [JsonProperty("_a")]
        public Incidencia_Aje Objeto { get; set; }
    }
    public class Response_AjeReporting_Incidencia_Detalle
    {
        [JsonProperty("_a")]
        public List<E_Reporting_Incidencia_Detalle> Objeto { get; set; }
    }
     #endregion

}