﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models.NwRepStdAASS;

namespace Lucky.Xplora.Models.NwRepStdAASS
{
    public class NwRepVisibilidadAASS
    {
        public E_Visibilidad_AJE_AASS Consulta_Visibilidad(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visibilidad_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visibilidad_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visibilidad_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public E_Visibilidad_AJE_AASS_v3 Consulta_Visibilidadv3(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visibilidad_v3_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visibilidad_v3_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visibilidad_v3_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }  //aqui llega el total
        public E_Visibilidad_AJE_AASS_Efectividad Consulta_Efectividad(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visib_Efectividad_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visib_Efectividad_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visib_Efectividad_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<M_Combo> Filtros(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visib_Filtro_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visib_Filtro_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visib_Filtro_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }
        public List<E_Visibilidad_AJE_AASS_Excel> Descarga(Request_NWRepStd_General request)
        {
            Response_NWRepStd_Visib_Excel_AASS oRp = MvcApplication._Deserialize<Response_NWRepStd_Visib_Excel_AASS>(MvcApplication._Servicio_Operativa.NwRepStd_Visib_Excel_AASS(MvcApplication._Serialize(request)));

            return oRp.Objeto;
        }


    }
    #region << Request - Response >>
        public class Response_NWRepStd_Visibilidad_AASS
        {
            [JsonProperty("_a")]
            public E_Visibilidad_AJE_AASS Objeto { get; set; }
        }
        public class Response_NWRepStd_Visibilidad_v3_AASS 
        {
            [JsonProperty("_a")]
            public E_Visibilidad_AJE_AASS_v3 Objeto { get; set; }
        }
        public class Response_NWRepStd_Visib_Efectividad_AASS
        {
            [JsonProperty("_a")]
            public E_Visibilidad_AJE_AASS_Efectividad Objeto { get; set; }
        }
        public class Response_NWRepStd_Visib_Filtro_AASS
        {
            [JsonProperty("_a")]
            public List<M_Combo> Objeto { get; set; }
        }
        public class Response_NWRepStd_Visib_Excel_AASS 
        {
            [JsonProperty("_a")]
            public List<E_Visibilidad_AJE_AASS_Excel> Objeto { get; set; }
        }
    #endregion
    #region << Entidades >>
        public class E_Visibilidad_AJE_AASS
        {
            [JsonProperty("_a")]
            public E_Visibilidad_AJE_AASS_v1 vista1 { get; set; }
            [JsonProperty("_b")]
            public E_Visibilidad_AJE_AASS_v2 vista2 { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v1
        {
            [JsonProperty("_a")]
            public List<String> ListPeriodo { get; set; }
            [JsonProperty("_b")]
            public List<String> ListEmpresa { get; set; }
            [JsonProperty("_c")]
            public List<E_Visibilidad_AJE_AASS_v1_plantilla> Grafico1 { get; set; }
            [JsonProperty("_d")]
            public List<E_Visibilidad_AJE_AASS_v1_plantilla2> Grafico2 { get; set; }
            [JsonProperty("_e")]
            public List<String> Coberturados { get; set; }
            [JsonProperty("_f")]
            public List<E_Visibilidad_AJE_AASS_v1_plantilla3> Grafico3 { get; set; }
            [JsonProperty("_g")]
            public List<String> ListMarca { get; set; }
            [JsonProperty("_h")]
            public List<String> lPdv_Visitados { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v1_plantilla
        {
            [JsonProperty("_a")]
            public int Cod_Elemento { get; set; }
            [JsonProperty("_b")]
            public String Nom_Elemento { get; set; }
            [JsonProperty("_c")]
            public int Cantidad { get; set; }
            [JsonProperty("_d")]
            public String Porcentaje { get; set; }
            [JsonProperty("_e")]
            public String Color { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v1_plantilla2
        {
            [JsonProperty("_a")]
            public String Nombre { get; set; }
            [JsonProperty("_b")]
            public List<String> Valores { get; set; }
            [JsonProperty("_c")]
            public String Color { get; set; }
            [JsonProperty("_d")]
            public List<E_Visibilidad_AJE_AASS_v1_val> valores2 { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v1_plantilla3
        {
            [JsonProperty("_a")]
            public String Nombre { get; set; }
            [JsonProperty("_b")]
            public List<String> Valores { get; set; }
            [JsonProperty("_c")]
            public String Color { get; set; }
            [JsonProperty("_d")]
            public List<E_Visibilidad_AJE_AASS_v1_val2> valores2 { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v1_val
        {
            [JsonProperty("_a")]
            public String valor { get; set; }
            [JsonProperty("_b")]
            public String Porcentaje { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v1_val2
        {
            [JsonProperty("_a")]
            public String valor { get; set; }
            [JsonProperty("_b")]
            public String Porcentaje { get; set; }
            [JsonProperty("_c")]
            public String idempresa { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v2
        {
            [JsonProperty("_a")]
            public List<String> ListPeriodo { get; set; }
            [JsonProperty("_b")]
            public List<E_Visibilidad_AJE_AASS_v2_plantilla> Grafico1 { get; set; }
            [JsonProperty("_c")]
            public List<E_Visibilidad_AJE_AASS_v2_plantilla> Grafico2 { get; set; }
            [JsonProperty("_d")]
            public List<E_Visibilidad_AJE_AASS_v2_item> Materiales { get; set; }
            [JsonProperty("_e")]
            public List<E_Visibilidad_AJE_AASS_v2_plantilla> Grafico3 { get; set; }

        }
        public class E_Visibilidad_AJE_AASS_v2_item
        {
            [JsonProperty("_a")]
            public int Codigo { get; set; }
            [JsonProperty("_b")]
            public String Valor { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v2_plantilla
        {
            [JsonProperty("_a")]
            public int Cod_Empresa { get; set; }
            [JsonProperty("_b")]
            public List<E_Visibilidad_AJE_AASS_v2_pla_1> Grafico1 { get; set; }
            [JsonProperty("_c")]
            public List<E_Visibilidad_AJE_AASS_v2_pla_2> Grafico2 { get; set; }

        }
        public class E_Visibilidad_AJE_AASS_v2_pla_1
        {
            [JsonProperty("_a")]
            public int Cod_Elemento { get; set; }
            [JsonProperty("_b")]
            public String Nom_Elemento { get; set; }
            [JsonProperty("_c")]
            public int Cantidad { get; set; }
            [JsonProperty("_d")]
            public String Porcentaje { get; set; }
            [JsonProperty("_e")]
            public String Color { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v2_pla_2
        {
            [JsonProperty("_a")]
            public int Cod_Elemento { get; set; }
            [JsonProperty("_b")]
            public String Nom_Elemento { get; set; }
            [JsonProperty("_c")]
            public List<String> Valor { get; set; }
        }

        public class E_Visibilidad_AJE_AASS_v3
        {
            [JsonProperty("_a")]
            public List<String> Periodos { get; set; }
            [JsonProperty("_b")]
            public List<E_Visibilidad_AJE_AASS_v1_plantilla> Pie { get; set; }
            [JsonProperty("_c")]
            public List<E_Visibilidad_AJE_AASS_v1_plantilla2> Evolutivo { get; set; }
            [JsonProperty("_d")]
            public List<E_Visibilidad_AJE_AASS_v3_item> Cab_Grilla { get; set; }
            [JsonProperty("_e")]
            public List<E_Visibilidad_AJE_AASS_v3_Listitem> Content_Grilla { get; set; }

        }
        public class E_Visibilidad_AJE_AASS_v3_item
        {
            [JsonProperty("_a")]
            public int Cod_Elemento { get; set; }
            [JsonProperty("_b")]
            public String Nom_Elmento { get; set; }
            [JsonProperty("_c")]
            public String Valor1 { get; set; }
            [JsonProperty("_d")]
            public String Valor2 { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_v3_Listitem
        {
            [JsonProperty("_a")]
            public String Cod_Elemento { get; set; }
            [JsonProperty("_b")]
            public String Nom_Elemento { get; set; }
            [JsonProperty("_c")]
            public String Total { get; set; }
            [JsonProperty("_d")]
            public List<String> Valores { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_Efectividad
        {
            [JsonProperty("_a")]
            public List<String> Periodos { get; set; }
            [JsonProperty("_b")]
            public List<E_Visibilidad_AJE_AASS_Efect_Item> Grilla1 { get; set; }
            [JsonProperty("_c")]
            public List<E_Visibilidad_AJE_AASS_Efect_Item> Evolutivo { get; set; }
            [JsonProperty("_d")]
            public List<E_Visibilidad_AJE_AASS_Efect_Item> Grilla2 { get; set; }

        }
        public class E_Visibilidad_AJE_AASS_Efect_Item
        {
            [JsonProperty("_a")]
            public String Elemento { get; set; }
            [JsonProperty("_b")]
            public String PDV { get; set; }
            [JsonProperty("_c")]
            public String Objetivo { get; set; }
            [JsonProperty("_d")]
            public String Real { get; set; }
            [JsonProperty("_e")]
            public String Efectividad { get; set; }
            [JsonProperty("_f")]
            public String Comentario { get; set; }
            [JsonProperty("_g")]
            public List<String> Valores { get; set; }
        }
        public class E_Visibilidad_AJE_AASS_Excel
        {
            [JsonProperty("_a")]
            public String Cedis { get; set; }
            [JsonProperty("_b")]
            public String Cod_PDV { get; set; }
            [JsonProperty("_c")]
            public String Nom_PDV { get; set; }
            [JsonProperty("_d")]
            public String Empresa { get; set; }
            [JsonProperty("_e")]
            public String Categoria { get; set; }
            [JsonProperty("_f")]
            public String Marca { get; set; }
            [JsonProperty("_g")]
            public String Material { get; set; }
            [JsonProperty("_h")]
            public String Cantidad { get; set; }
            [JsonProperty("_i")]
            public String Fecha { get; set; }
        }
    
    #endregion

}