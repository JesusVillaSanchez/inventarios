﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Models.NwRepStdAASS
{
    public class Oos_Aje : A_Oos_Aje
    {
        [JsonProperty("_a")]
        public List<Oos_Aje_Data> OosDataIni { get; set; }
        [JsonProperty("_b")]
        public List<Oos_Aje_Percentage_Brand> OosxMarca { get; set; }
        [JsonProperty("_c")]
        public List<E_Oos_Ranking_PDV> OosxPDVRanking { get; set; }
        [JsonProperty("_d")]
        public List<E_Oos_Evolutivo_Head> OosEvolutivoHead { get; set; }
        [JsonProperty("_e")]
        public List<E_Oos_Evolutivo_Body> OosEvolutivoBody { get; set; }
        [JsonProperty("_f")]
        public List<E_Oos_Evolutivo_BodyC> OosEvoCadenaBody { get; set; }
        [JsonProperty("_g")]
        public List<E_Oos_Ranking_SKU> OosxSKURanking { get; set; }
    }
    public class Oos_Aje_Data
    {
        [JsonProperty("_a")]
        public string oos_perdida { get; set; }
        [JsonProperty("_b")]
        public string oos_cantidad { get; set; }
    }
    public class Oos_Aje_Percentage_Brand
    {
        [JsonProperty("_a")]
        public string brand { get; set; }
        [JsonProperty("_b")]
        public string porcbrand { get; set; }
        [JsonProperty("_c")]
        public string cantidadpdv { get; set; }
        [JsonProperty("_d")]
        public string cantidadruta { get; set; }
    }
    public class E_Oos_Ranking_PDV
    {
        [JsonProperty("_a")]
        public string pdvName { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }
    public class E_Oos_Ranking_SKU
    {
        [JsonProperty("_a")]
        public string skuName { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }
    public class E_Oos_Evolutivo_Head
    {
        [JsonProperty("_a")]
        public string id_head { get; set; }
        [JsonProperty("_b")]
        public string rpl_id_head { get; set; }
        [JsonProperty("_c")]
        public string descripcion_head { get; set; }
    }
    public class E_Oos_Evolutivo_Body
    {
        [JsonProperty("_a")]
        public string row { get; set; }
        [JsonProperty("_b")]
        public string producto { get; set; }
        [JsonProperty("_c")]
        public string rpl_1 { get; set; }
        [JsonProperty("_d")]
        public string rpl_2 { get; set; }
        [JsonProperty("_e")]
        public string rpl_3 { get; set; }
        [JsonProperty("_f")]
        public string rpl_4 { get; set; }
        [JsonProperty("_g")]
        public string rpl_5 { get; set; }
        [JsonProperty("_h")]
        public string rpl_6 { get; set; }
        [JsonProperty("_i")]
        public string rpl_7 { get; set; }
        [JsonProperty("_j")]
        public string rpl_8 { get; set; }
        [JsonProperty("_k")]
        public string rpl_9 { get; set; }
        [JsonProperty("_l")]
        public string rpl_10 { get; set; }
        [JsonProperty("_m")]
        public string rpl_11 { get; set; }
        [JsonProperty("_n")]
        public string rpl_12 { get; set; }
    }
    public class E_Oos_Evolutivo_BodyC
    {
        [JsonProperty("_a")]
        public string cadena { get; set; }
        [JsonProperty("_b")]
        public string producto { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }
    }

    public class Oos_Aje_Det_Quiebre
    {
        [JsonProperty("_a")]
        public string cadena { get; set; }
        [JsonProperty("_b")]
        public string pdv { get; set; }
        [JsonProperty("_c")]
        public string categoria { get; set; }
        [JsonProperty("_d")]
        public string presentacion { get; set; }
        [JsonProperty("_e")]
        public string sku { get; set; }
        [JsonProperty("_f")]
        public string producto { get; set; }
        [JsonProperty("_g")]
        public string fecha { get; set; }
    }
    public class Oos_Aje_Det_RankingPdv
    {
        [JsonProperty("_a")]
        public string ciudad { get; set; }
        [JsonProperty("_b")]
        public string pdv { get; set; }
        [JsonProperty("_c")]
        public string mes_1 { get; set; }
        [JsonProperty("_d")]
        public string mes_2 { get; set; }
        [JsonProperty("_e")]
        public string mes_3 { get; set; }
        [JsonProperty("_f")]
        public string mes_4 { get; set; }
        [JsonProperty("_g")]
        public string mes_5 { get; set; }
        [JsonProperty("_h")]
        public string mes_6 { get; set; }
        [JsonProperty("_i")]
        public string mes_7 { get; set; }
        [JsonProperty("_j")]
        public string mes_8 { get; set; }
        [JsonProperty("_k")]
        public string mes_9 { get; set; }
        [JsonProperty("_l")]
        public string mes_10 { get; set; }
        [JsonProperty("_m")]
        public string mes_11 { get; set; }
        [JsonProperty("_n")]
        public string mes_12 { get; set; }
        [JsonProperty("_ñ")]
        public string mes_13 { get; set; }

    }
    public class Oos_Aje_Det_RankingPdvmonto
    {
        [JsonProperty("_a")]
        public string ciudad { get; set; }
        [JsonProperty("_b")]
        public string pdv { get; set; }
        [JsonProperty("_c")]
        public string mes_1 { get; set; }
        [JsonProperty("_d")]
        public string mes_2 { get; set; }
        [JsonProperty("_e")]
        public string mes_3 { get; set; }
        [JsonProperty("_f")]
        public string mes_4 { get; set; }
        [JsonProperty("_g")]
        public string mes_5 { get; set; }
        [JsonProperty("_h")]
        public string mes_6 { get; set; }
        [JsonProperty("_i")]
        public string mes_7 { get; set; }
        [JsonProperty("_j")]
        public string mes_8 { get; set; }
        [JsonProperty("_k")]
        public string mes_9 { get; set; }
        [JsonProperty("_l")]
        public string mes_10 { get; set; }
        [JsonProperty("_m")]
        public string mes_11 { get; set; }
        [JsonProperty("_n")]
        public string mes_12 { get; set; }
        [JsonProperty("_ñ")]
        public string mes_13 { get; set; }

    }
    public class Oos_Aje_Det_RankingSku
    {
        [JsonProperty("_a")]
        public string sku { get; set; }
        [JsonProperty("_b")]
        public string mes_1 { get; set; }
        [JsonProperty("_c")]
        public string mes_2 { get; set; }
        [JsonProperty("_d")]
        public string mes_3 { get; set; }
        [JsonProperty("_e")]
        public string mes_4 { get; set; }
        [JsonProperty("_f")]
        public string mes_5 { get; set; }
        [JsonProperty("_g")]
        public string mes_6 { get; set; }
        [JsonProperty("_h")]
        public string mes_7 { get; set; }
        [JsonProperty("_i")]
        public string mes_8 { get; set; }
        [JsonProperty("_j")]
        public string mes_9 { get; set; }
        [JsonProperty("_k")]
        public string mes_10 { get; set; }
        [JsonProperty("_l")]
        public string mes_11 { get; set; }
        [JsonProperty("_m")]
        public string mes_12 { get; set; }
        [JsonProperty("_n")]
        public string mes_13 { get; set; }
    }
    public class Oos_Aje_Det_RankingSkumonto
    {
        [JsonProperty("_a")]
        public string sku { get; set; }
        [JsonProperty("_b")]
        public string mes_1 { get; set; }
        [JsonProperty("_c")]
        public string mes_2 { get; set; }
        [JsonProperty("_d")]
        public string mes_3 { get; set; }
        [JsonProperty("_e")]
        public string mes_4 { get; set; }
        [JsonProperty("_f")]
        public string mes_5 { get; set; }
        [JsonProperty("_g")]
        public string mes_6 { get; set; }
        [JsonProperty("_h")]
        public string mes_7 { get; set; }
        [JsonProperty("_i")]
        public string mes_8 { get; set; }
        [JsonProperty("_j")]
        public string mes_9 { get; set; }
        [JsonProperty("_k")]
        public string mes_10 { get; set; }
        [JsonProperty("_l")]
        public string mes_11 { get; set; }
        [JsonProperty("_m")]
        public string mes_12 { get; set; }
        [JsonProperty("_n")]
        public string mes_13 { get; set; }
    }

    public class E_Oos_Data_Export : A_Oos_Data_Export
    {
        [JsonProperty("a")]
        public string id_periodo { get; set; }
        [JsonProperty("b")]
        public string ciudad { get; set; }
        [JsonProperty("c")]
        public string cadena { get; set; }
        [JsonProperty("d")]
        public string cod_pdv { get; set; }
        [JsonProperty("e")]
        public string pdv { get; set; }
        [JsonProperty("f")]
        public string categoria { get; set; }
        [JsonProperty("g")]
        public string marca { get; set; }
        [JsonProperty("h")]
        public string presentacion { get; set; }
        [JsonProperty("i")]
        public string sku { get; set; }
        [JsonProperty("j")]
        public string producto { get; set; }
        [JsonProperty("k")]
        public string quiebres { get; set; }
        [JsonProperty("l")]
        public string fechacel { get; set; }
    }

    public abstract class A_Oos_Aje
    {
        public Oos_Aje Objeto_Oos(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Oos oRp = MvcApplication._Deserialize<Response_AjeReporting_Oos>(
                    MvcApplication._Servicio_Operativa.Aje_Oos(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Aje_Det_Quiebre> Objeto_OOSDetalle(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeOos_DetQuiebre oRp = MvcApplication._Deserialize<Response_AjeOos_DetQuiebre>(
                    MvcApplication._Servicio_Operativa.Aje_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Aje_Det_RankingPdv> Objeto_OOSDetRankingPdv(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeOos_DetRankingPdv oRp = MvcApplication._Deserialize<Response_AjeOos_DetRankingPdv>(
                    MvcApplication._Servicio_Operativa.Aje_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Aje_Det_RankingPdvmonto> Objeto_OOSDetRankingPdvmonto(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeOos_DetRankingPdvmonto oRp = MvcApplication._Deserialize<Response_AjeOos_DetRankingPdvmonto>(
                    MvcApplication._Servicio_Operativa.Aje_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Aje_Det_RankingSku> Objeto_OOSDetRankingSku(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeOos_DetRankingSku oRp = MvcApplication._Deserialize<Response_AjeOos_DetRankingSku>(
                    MvcApplication._Servicio_Operativa.Aje_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

        public List<Oos_Aje_Det_RankingSkumonto> Objeto_OOSDetRankingSkumonto(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeOos_DetRankingSkumonto oRp = MvcApplication._Deserialize<Response_AjeOos_DetRankingSkumonto>(
                        MvcApplication._Servicio_Operativa.Aje_OosDetalle(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

    }

    public abstract class A_Oos_Data_Export
    {
        public List<E_Oos_Data_Export> Lista_OosDataExport(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeOos_DataExport oRp = MvcApplication._Deserialize<Response_AjeOos_DataExport>(
                    MvcApplication._Servicio_Operativa.Aje_OosDetalle_Export(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

    public class Response_AjeReporting_Oos
    {
        [JsonProperty("_a")]
        public Oos_Aje Objeto { get; set; }
    }

    public class Response_AjeOos_DetQuiebre
    {
        [JsonProperty("_a")]
        public List<Oos_Aje_Det_Quiebre> Objeto { get; set; }
    }
    public class Response_AjeOos_DetRankingPdv
    {
        [JsonProperty("_a")]
        public List<Oos_Aje_Det_RankingPdv> Objeto { get; set; }
    }
    public class Response_AjeOos_DetRankingPdvmonto
    {
        [JsonProperty("_a")]
        public List<Oos_Aje_Det_RankingPdvmonto> Objeto { get; set; }
    }
    public class Response_AjeOos_DetRankingSku
    {
        [JsonProperty("_a")]
        public List<Oos_Aje_Det_RankingSku> Objeto { get; set; }
    }
    public class Response_AjeOos_DetRankingSkumonto
    {
        [JsonProperty("_a")]
        public List<Oos_Aje_Det_RankingSkumonto> Objeto { get; set; }
    }

    public class Response_AjeOos_DataExport
    {
        [JsonProperty("_a")]
        public List<E_Oos_Data_Export> Objeto { get; set; }
    }

    #endregion

}