﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models.Aje;

namespace Lucky.Xplora.Models.NwRepStdAASS
{
    public class NwRep_Kc_AASS
    {
        public List<M_Combo> Get_Filtro(Request_NWRepStd_General request)
        {
            ResponseLimberly_AASSFiltro oRp = MvcApplication._Deserialize
           <ResponseLimberly_AASSFiltro>(MvcApplication._Servicio_Operativa.KC_AASS_Filtros
                (MvcApplication._Serialize(request)));

            return oRp.objeto;
        }        
        public E_EEAA_Kimberly_AASS Get_EEAA(E_Parametros_Aje_AASS oRq)
        {
            return MvcApplication._Deserialize<ResponseLimberly_AASSReporting>(
                    MvcApplication._Servicio_Operativa.KC_AASS_EEAA_Consulta(
                        MvcApplication._Serialize(new RequestAjeExhibicionAdicional() { oParametros = oRq })
                    )
                ).objeto;
        }
        public List<E_Reporting_Excibicion_AASS> Get_BD_EEAA(Request_AjeReporting_Parametros oRq)
        {
            Response_Kimberly_AASS_Reporting_EEAA_BD oRp = MvcApplication._Deserialize<Response_Kimberly_AASS_Reporting_EEAA_BD>(
                    MvcApplication._Servicio_Operativa.KC_AASS_EEAA_BD_Excel(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
        }
        public List<KC_EEAA_DetalleMaterialReporte> Get_Excel_Det_EEAA(Request_AjeReporting_Parametros oRq)
        {
            Response_Kimberly_AASS_Rep_Detalle_EEAA oRp = MvcApplication._Deserialize<Response_Kimberly_AASS_Rep_Detalle_EEAA>(
                    MvcApplication._Servicio_Operativa.KC_AASS_EEAA_Excel_Detalle(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
            //return null;
        }
        public E_StockOut_Kimberly_AASS Get_StockOut(Request_AjeReporting_Parametros oRq)
        {
            Response_KC_Reporting_StockOut oRp = MvcApplication._Deserialize<Response_KC_Reporting_StockOut>(
                    MvcApplication._Servicio_Operativa.KC_AASS_StockOut(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
        public E_StockOut_Kc_Consulta_Brand Get_StockOut_Marca(Request_Sos_Aje_Brand oRq)
        {
            Response_Kimberly_StockOut_Brand oRp;

            oRp = MvcApplication._Deserialize<Response_Kimberly_StockOut_Brand>(MvcApplication._Servicio_Operativa.KC_AASS_StockOut_Marca(MvcApplication._Serialize(oRq)));

            return oRp.Obj;
        }
        //
        public List<E_StockOut_Kc_Det_StockOut> Get_StockOut_Detalle(Request_AjeReporting_Parametros oRq)
        {
            Response_Kimberly_StockOut_Detalle oRp;

            oRp = MvcApplication._Deserialize<Response_Kimberly_StockOut_Detalle>(MvcApplication._Servicio_Operativa.KC_AASS_StockOut_Detalle(MvcApplication._Serialize(oRq)));

            return oRp.Objeto;
        }
        public List<E_StockOut_Kc_Det_StockOut_RangkingPDV> Get_StockOut_Det_RangPDV(Request_AjeReporting_Parametros oRq)
        {
            Response_Kimberly_StockOut_DetRankingPdv oRp;

            oRp = MvcApplication._Deserialize<Response_Kimberly_StockOut_DetRankingPdv>(MvcApplication._Servicio_Operativa.KC_AASS_StockOut_Detalle(MvcApplication._Serialize(oRq)));

            return oRp.Objeto;
        }
        public List<E_StockOut_Kc_Det_StockOut_RangkingSKU> Get_StockOut_Det_RangSKU(Request_AjeReporting_Parametros oRq)
        {
            Response_Kimberly_StockOut_DetRankingSku oRp;

            oRp = MvcApplication._Deserialize<Response_Kimberly_StockOut_DetRankingSku>(MvcApplication._Servicio_Operativa.KC_AASS_StockOut_Detalle(MvcApplication._Serialize(oRq)));

            return oRp.Objeto;
        }
        public List<E_StockOut_Kc_Det_RangkingPDVMonto> Get_StockOut_Det_RangPdvMonto(Request_AjeReporting_Parametros oRq)
        {
            Response_Kimberly_StockOut_DetRankingPdvmonto oRp;

            oRp = MvcApplication._Deserialize<Response_Kimberly_StockOut_DetRankingPdvmonto>(MvcApplication._Servicio_Operativa.KC_AASS_StockOut_Detalle(MvcApplication._Serialize(oRq)));

            return oRp.Objeto;
        }
        public List<E_StockOut_Kc_Det_RangkingSKUMonto> Get_StockOut_Det_RangSKUMonto(Request_AjeReporting_Parametros oRq)
        {
            Response_Kimberly_StockOut_DetRankingSkumonto oRp;

            oRp = MvcApplication._Deserialize<Response_Kimberly_StockOut_DetRankingSkumonto>(MvcApplication._Servicio_Operativa.KC_AASS_StockOut_Detalle(MvcApplication._Serialize(oRq)));

            return oRp.Objeto;
        }
        public List<E_Oos_Data_Export> ExportatDataStockOutBd(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeOos_DataExport oRp = MvcApplication._Deserialize<Response_AjeOos_DataExport>(
                    MvcApplication._Servicio_Operativa.KC_StockOutBd_Export(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }

     
    }
    #region Response
    public class ResponseLimberly_AASSReporting
    {
        [JsonProperty("_a")]
        public E_EEAA_Kimberly_AASS objeto { get; set; }
    }
    public class ResponseLimberly_AASSFiltro
    {
        [JsonProperty("_a")]
        public List<M_Combo> objeto { get; set; }
    }
    public class Response_Kimberly_AASS_Reporting_EEAA_BD
    {
        [JsonProperty("_a")]
        public List<E_Reporting_Excibicion_AASS> Objeto { get; set; }
    }
    public class Response_Kimberly_AASS_Rep_Detalle_EEAA
    {
        [JsonProperty("_a")]
        public List<KC_EEAA_DetalleMaterialReporte> Objeto { get; set; }
    }

    #region -- Stock Out --
    public class Response_KC_Reporting_StockOut
    {
        [JsonProperty("_a")]
        public E_StockOut_Kimberly_AASS Objeto { get; set; }
    }
    public class Response_Kimberly_StockOut_RankingPDV
    {
        [JsonProperty("_a")]
        public SotkcOut_Kimberly_Rep_RankingPDV Objeto { get; set; }
    }
    public class Response_Kimberly_StockOut_RankingSKU
    {
        [JsonProperty("_a")]
        public StockOut_Kimberly_Rep_RankingSKU Objeto { get; set; }
    }
    public class Response_Kimberly_StockOut_Brand
    {
        [JsonProperty("_a")]
        public E_StockOut_Kc_Consulta_Brand Obj { get; set; }
    }
    //
    public class Response_Kimberly_StockOut_Detalle
    {
        [JsonProperty("_a")]
        public List<E_StockOut_Kc_Det_StockOut> Objeto { get; set; }
    }
    public class Response_Kimberly_StockOut_DetRankingPdv
    {
        [JsonProperty("_a")]
        public List<E_StockOut_Kc_Det_StockOut_RangkingPDV> Objeto { get; set; }
    }
    public class Response_Kimberly_StockOut_DetRankingSku
    {
        [JsonProperty("_a")]
        public List<E_StockOut_Kc_Det_StockOut_RangkingSKU> Objeto { get; set; }
    }
    public class Response_Kimberly_StockOut_DetRankingPdvmonto
    {
        [JsonProperty("_a")]
        public List<E_StockOut_Kc_Det_RangkingPDVMonto> Objeto { get; set; }
    }
    public class Response_Kimberly_StockOut_DetRankingSkumonto
    {
        [JsonProperty("_a")]
        public List<E_StockOut_Kc_Det_RangkingSKUMonto> Objeto { get; set; }
    }

    #endregion

    #endregion
    #region Entidad
    #region EEAA
    public class E_EEAA_Kimberly_AASS
    {
        [JsonProperty("_a")]
        public string Valorizado { get; set; }
        [JsonProperty("_b")]
        public string Cantidad { get; set; }
        [JsonProperty("_c")]
        public string TotalPdv { get; set; }
        [JsonProperty("_d")]
        public string PdvElemento { get; set; }
        [JsonProperty("_e")]
        public List<KC_EEAA_ParticipacionMaterial> ParticipacionMaterial { get; set; }
        [JsonProperty("_f")]
        public List<KC_EEAA_ParticipacionEvolutivoMes> EvolutivoMes { get; set; }
        [JsonProperty("_g")]
        public List<KC_EEAA_Tarifario> TarifarioPeriodo { get; set; }
        [JsonProperty("_h")]
        public List<KC_EEAA_Detalle> Detalle { get; set; }
    }
    public class KC_EEAA_ParticipacionMaterial
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }
        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }
        [JsonProperty("_c")]
        public string mat_cantidad { get; set; }
        [JsonProperty("_d")]
        public string mat_cantidad_porcentaje { get; set; }
        [JsonProperty("_e")]
        public string mat_valorizado { get; set; }
        [JsonProperty("_f")]
        public string mat_valorizado_porcentaje { get; set; }
        [JsonProperty("_g")]
        public List<KC_EEAA_ParticipacionMaterialMarca> marca { get; set; }
        [JsonProperty("_h")]
        public List<KC_EEAA_ParticipacionEvolutivoMes> evolutivo { get; set; }
    }
    public class KC_EEAA_ParticipacionMaterialMarca
    {
        [JsonProperty("_a")]
        public int mar_id { get; set; }
        [JsonProperty("_b")]
        public string mar_descripcion { get; set; }
        [JsonProperty("_c")]
        public string mar_cantidad { get; set; }
        [JsonProperty("_d")]
        public string mar_cantidad_porcentaje { get; set; }
        [JsonProperty("_e")]
        public string mar_valorizado { get; set; }
        [JsonProperty("_f")]
        public string mar_valorizado_porcentaje { get; set; }
        [JsonProperty("_g")]
        public string color { get; set; }
    }
    public class KC_EEAA_ParticipacionEvolutivoMes
    {
        [JsonProperty("_a")]
        public int id { get; set; }
        [JsonProperty("_b")]
        public string mes { get; set; }
        [JsonProperty("_c")]
        public string valorizado { get; set; }
        [JsonProperty("_d")]
        public string cantidad { get; set; }
        [JsonProperty("_e")]
        public string pdv_total { get; set; }
        [JsonProperty("_f")]
        public string pdv_elemento { get; set; }
        [JsonProperty("_g")]
        public string cant_marca { get; set; }
    }
    public class KC_EEAA_Tarifario
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }
        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }
        [JsonProperty("_c")]
        public string plazavea_valor { get; set; }
        [JsonProperty("_d")]
        public string tottus_valor { get; set; }
        [JsonProperty("_e")]
        public string metro_valor { get; set; }
        [JsonProperty("_f")]
        public string wong_valor { get; set; }
        [JsonProperty("_g")]
        public string vivanda_valor { get; set; }
    }
    public class KC_EEAA_Detalle
    {
        [JsonProperty("_a")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_b")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_c")]
        public List<KC_EEAA_DetalleMaterial> DetalleMaterial { get; set; }
    }
    public class KC_EEAA_DetalleMaterial
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string foto { get; set; }

        [JsonProperty("_d")]
        public string existe { get; set; }

    }
    public class KC_EEAA_DetalleMaterialReporte
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string foto { get; set; }

        [JsonProperty("_d")]
        public string existe { get; set; }

        [JsonProperty("_e")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_f")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_g")]
        public string anio { get; set; }

        [JsonProperty("_h")]
        public string mes { get; set; }

        [JsonProperty("_i")]
        public string cad_nom { get; set; }
    }
    #endregion
    #region StockOut
    public class E_StockOut_Kimberly_AASS
    {
        [JsonProperty("_a")]
        public List<StockOut_Kimberly_Data> OosDataIni { get; set; }
        [JsonProperty("_b")]
        public List<StockOut_Kimberly_Percentage_Brand> OosxMarca { get; set; }
        [JsonProperty("_c")]
        public List<StockOut_Kimberly_Ranking_PDV> OosxPDVRanking { get; set; }
        [JsonProperty("_d")]
        public List<StockOut_Kimberly_Evolutivo_Head> OosEvolutivoHead { get; set; }
        [JsonProperty("_e")]
        public List<StockOut_Kimberly_Evolutivo_Body> OosEvolutivoBody { get; set; }
        [JsonProperty("_f")]
        public List<StockOut_Kimberly_Evolutivo_BodyC> OosEvoCadenaBody { get; set; }
        [JsonProperty("_g")]
        public List<StockOut_Kimberly_Ranking_SKU> OosxSKURanking { get; set; }
    }
    public class StockOut_Kimberly_Data
    {
        [JsonProperty("_a")]
        public string oos_perdida { get; set; }
        [JsonProperty("_b")]
        public string oos_cantidad { get; set; }
    }
    public class StockOut_Kimberly_Percentage_Brand
    {
        [JsonProperty("_a")]
        public string brand { get; set; }
        [JsonProperty("_b")]
        public string porcbrand { get; set; }
        [JsonProperty("_c")]
        public string cantidadpdv { get; set; }
        [JsonProperty("_d")]
        public string cantidadruta { get; set; }
    }
    public class StockOut_Kimberly_Ranking_PDV
    {
        [JsonProperty("_a")]
        public string pdvName { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }
    public class StockOut_Kimberly_Evolutivo_Head
    {
        [JsonProperty("_a")]
        public string id_head { get; set; }
        [JsonProperty("_b")]
        public string rpl_id_head { get; set; }
        [JsonProperty("_c")]
        public string descripcion_head { get; set; }
    }
    public class StockOut_Kimberly_Evolutivo_Body
    {
        [JsonProperty("_a")]
        public string row { get; set; }
        [JsonProperty("_b")]
        public string producto { get; set; }
        [JsonProperty("_c")]
        public string rpl_1 { get; set; }
        [JsonProperty("_d")]
        public string rpl_2 { get; set; }
        [JsonProperty("_e")]
        public string rpl_3 { get; set; }
        [JsonProperty("_f")]
        public string rpl_4 { get; set; }
        [JsonProperty("_g")]
        public string rpl_5 { get; set; }
        [JsonProperty("_h")]
        public string rpl_6 { get; set; }
        [JsonProperty("_i")]
        public string rpl_7 { get; set; }
        [JsonProperty("_j")]
        public string rpl_8 { get; set; }
        [JsonProperty("_k")]
        public string rpl_9 { get; set; }
        [JsonProperty("_l")]
        public string rpl_10 { get; set; }
        [JsonProperty("_m")]
        public string rpl_11 { get; set; }
        [JsonProperty("_n")]
        public string rpl_12 { get; set; }
    }
    public class StockOut_Kimberly_Ranking_SKU
    {
        [JsonProperty("_a")]
        public string skuName { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
    }
    public class StockOut_Kimberly_Evolutivo_BodyC
    {
        [JsonProperty("_a")]
        public string cadena { get; set; }
        [JsonProperty("_b")]
        public string producto { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }
    }
    public class SotkcOut_Kimberly_Rep_RankingPDV
    {
        [JsonProperty("_a")]
        public List<StockOut_Kimberly_Ranking_PDV> OosxPDVRanking { get; set; }
    }
    public class StockOut_Kimberly_Rep_RankingSKU
    {
        [JsonProperty("_a")]
        public List<StockOut_Kimberly_Ranking_SKU> OosxSKURanking { get; set; }
    }
    public class E_StockOut_Kc_Consulta_Brand
    {
        [JsonProperty("_a")]
        public string Cod_Marca { get; set; }
        [JsonProperty("_b")]
        public string Marca { get; set; }
    }
    //
    public class E_StockOut_Kc_Det_StockOut 
    {
        [JsonProperty("_a")]
        public string cadena { get; set; }
        [JsonProperty("_b")]
        public string pdv { get; set; }
        [JsonProperty("_c")]
        public string categoria { get; set; }
        [JsonProperty("_d")]
        public string presentacion { get; set; }
        [JsonProperty("_e")]
        public string sku { get; set; }
        [JsonProperty("_f")]
        public string producto { get; set; }
        [JsonProperty("_g")]
        public string fecha { get; set; }
    }
    public class E_StockOut_Kc_Det_StockOut_RangkingPDV 
    {
        [JsonProperty("_a")]
        public string ciudad { get; set; }
        [JsonProperty("_b")]
        public string pdv { get; set; }
        [JsonProperty("_c")]
        public string mes_1 { get; set; }
        [JsonProperty("_d")]
        public string mes_2 { get; set; }
        [JsonProperty("_e")]
        public string mes_3 { get; set; }
        [JsonProperty("_f")]
        public string mes_4 { get; set; }
        [JsonProperty("_g")]
        public string mes_5 { get; set; }
        [JsonProperty("_h")]
        public string mes_6 { get; set; }
        [JsonProperty("_i")]
        public string mes_7 { get; set; }
        [JsonProperty("_j")]
        public string mes_8 { get; set; }
        [JsonProperty("_k")]
        public string mes_9 { get; set; }
        [JsonProperty("_l")]
        public string mes_10 { get; set; }
        [JsonProperty("_m")]
        public string mes_11 { get; set; }
        [JsonProperty("_n")]
        public string mes_12 { get; set; }
        [JsonProperty("_nn")]
        public string mes_13 { get; set; }

    }
    public class E_StockOut_Kc_Det_StockOut_RangkingSKU 
    {
        [JsonProperty("_a")]
        public string sku { get; set; }
        [JsonProperty("_b")]
        public string mes_1 { get; set; }
        [JsonProperty("_c")]
        public string mes_2 { get; set; }
        [JsonProperty("_d")]
        public string mes_3 { get; set; }
        [JsonProperty("_e")]
        public string mes_4 { get; set; }
        [JsonProperty("_f")]
        public string mes_5 { get; set; }
        [JsonProperty("_g")]
        public string mes_6 { get; set; }
        [JsonProperty("_h")]
        public string mes_7 { get; set; }
        [JsonProperty("_i")]
        public string mes_8 { get; set; }
        [JsonProperty("_j")]
        public string mes_9 { get; set; }
        [JsonProperty("_k")]
        public string mes_10 { get; set; }
        [JsonProperty("_l")]
        public string mes_11 { get; set; }
        [JsonProperty("_m")]
        public string mes_12 { get; set; }
        [JsonProperty("_n")]
        public string mes_13 { get; set; }
    }
    public class E_StockOut_Kc_Det_RangkingPDVMonto 
    {
        [JsonProperty("_a")]
        public string ciudad { get; set; }
        [JsonProperty("_b")]
        public string pdv { get; set; }
        [JsonProperty("_c")]
        public string mes_1 { get; set; }
        [JsonProperty("_d")]
        public string mes_2 { get; set; }
        [JsonProperty("_e")]
        public string mes_3 { get; set; }
        [JsonProperty("_f")]
        public string mes_4 { get; set; }
        [JsonProperty("_g")]
        public string mes_5 { get; set; }
        [JsonProperty("_h")]
        public string mes_6 { get; set; }
        [JsonProperty("_i")]
        public string mes_7 { get; set; }
        [JsonProperty("_j")]
        public string mes_8 { get; set; }
        [JsonProperty("_k")]
        public string mes_9 { get; set; }
        [JsonProperty("_l")]
        public string mes_10 { get; set; }
        [JsonProperty("_m")]
        public string mes_11 { get; set; }
        [JsonProperty("_n")]
        public string mes_12 { get; set; }
        [JsonProperty("_ñ")]
        public string mes_13 { get; set; }

    }
    public class E_StockOut_Kc_Det_RangkingSKUMonto
    {
        [JsonProperty("_a")]
        public string sku { get; set; }
        [JsonProperty("_b")]
        public string mes_1 { get; set; }
        [JsonProperty("_c")]
        public string mes_2 { get; set; }
        [JsonProperty("_d")]
        public string mes_3 { get; set; }
        [JsonProperty("_e")]
        public string mes_4 { get; set; }
        [JsonProperty("_f")]
        public string mes_5 { get; set; }
        [JsonProperty("_g")]
        public string mes_6 { get; set; }
        [JsonProperty("_h")]
        public string mes_7 { get; set; }
        [JsonProperty("_i")]
        public string mes_8 { get; set; }
        [JsonProperty("_j")]
        public string mes_9 { get; set; }
        [JsonProperty("_k")]
        public string mes_10 { get; set; }
        [JsonProperty("_l")]
        public string mes_11 { get; set; }
        [JsonProperty("_m")]
        public string mes_12 { get; set; }
        [JsonProperty("_n")]
        public string mes_13 { get; set; }
    }
    #endregion
    #endregion
}