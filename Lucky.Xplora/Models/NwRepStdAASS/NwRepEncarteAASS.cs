﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Models.NwRepStdAASS
{
    public class Encarte_Aje : A_Encarte_Aje
    {
        [JsonProperty("_a")]
        public List<E_Encarte_ACadena> EncartexCadena { get; set; }
        [JsonProperty("_b")]
        public List<E_Encarte_PEmpresa> EncartexEmpresa { get; set; }
        [JsonProperty("_c")]
        public List<E_Encarte_TActividades> EncartexActividades { get; set; }
        [JsonProperty("_d")]
        public List<E_Encarte_AEvolutivo> EncartexEvolutivo { get; set; }
    }
    public class E_Encarte_ACadena
    {
        [JsonProperty("_a")]
        public int id_cadena { get; set; }
        [JsonProperty("_b")]
        public string cadena { get; set; }
        [JsonProperty("_c")]
        public string empresa { get; set; }
        [JsonProperty("_d")]
        public string cantidad { get; set; }

        [JsonProperty("_e")]
        public string porcentaje { get; set; }

        [JsonProperty("_f")]
        public string color { get; set; }
    }
    public class E_Encarte_PEmpresa
    {
        [JsonProperty("_a")]
        public string empresa { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }
        [JsonProperty("_c")]
        public string color { get; set; }
    }
    public class E_Encarte_TActividades
    {
        [JsonProperty("_a")]
        public string empresa { get; set; }
        [JsonProperty("_b")]
        public string actividad { get; set; }
        [JsonProperty("_c")]
        public string participacion { get; set; }
        //[JsonProperty("_d")]
        //public string color { get; set; }
    }
    public class E_Encarte_AEvolutivo
    {
        [JsonProperty("_a")]
        public string periodo  { get; set; }
        [JsonProperty("_b")]
        public string empresa  { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }

    public abstract class A_Encarte_Aje
    {
        public Encarte_Aje Objeto_Encarte(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Encarte oRp = MvcApplication._Deserialize<Response_AjeReporting_Encarte>(
                    MvcApplication._Servicio_Operativa.Aje_Encarte(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
        }
    }

    public class E_Reporting_Encarte_Detalle : A_E_Reporting_Encarte_Detalle
    {
        [JsonProperty("a")]
        public string Id_Row { get; set; }
        [JsonProperty("b")]
        public string ciudad { get; set; }
        [JsonProperty("c")]
        public string distrito { get; set; }
        [JsonProperty("d")]
        public string cadena { get; set; }
        [JsonProperty("e")]
        public string categoria { get; set; }
        [JsonProperty("f")]
        public string fecha_inico { get; set; }
        [JsonProperty("g")]
        public string fecha_fin { get; set; }
        [JsonProperty("h")]
        public string marca { get; set; }
        [JsonProperty("i")]
        public string producto { get; set; }
        [JsonProperty("j")]
        public string mecanica { get; set; }
        [JsonProperty("k")]
        public string tipo_anuncio { get; set; }
        [JsonProperty("l")]
        public string oficina { get; set; }
        [JsonProperty("m")]
        public string empresa { get; set; }
        [JsonProperty("n")]
        public string precio_oferta { get; set; }
        [JsonProperty("o")]
        public string precio_normal { get; set; }
        [JsonProperty("p")]
        public string encarte_comentarios { get; set; }
    }
    public class E_Reporting_Excibicion_AASS : A_E_Reporting_Exhibicion
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }
        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }
        [JsonProperty("_c")]
        public string mat_cantidad { get; set; }
        [JsonProperty("_d")]
        public string mat_cantidad_porcentaje { get; set; }
        [JsonProperty("_e")]
        public string mat_valorizado { get; set; }
        [JsonProperty("_f")]
        public string mat_valorizado_porcentaje { get; set; }
        [JsonProperty("_g")]
        public string mar_id { get; set; }

        [JsonProperty("_h")]
        public string mar_descripcion { get; set; }
        [JsonProperty("_i")]
        public string mar_cantidad { get; set; }
        [JsonProperty("_j")]
        public string mar_cantidad_porcentaje { get; set; }
        [JsonProperty("_k")]
        public string mar_valorizado { get; set; }
        [JsonProperty("_l")]
        public string mar_valorizado_porcentaje { get; set; }

        [JsonProperty("_m")]
        public string commercialNodeName { get; set; }
        [JsonProperty("_n")]
        public string foto { get; set; }
        [JsonProperty("_o")]
        public string mes { get; set; }
        [JsonProperty("_p")]
        public string periodo { get; set; }
        [JsonProperty("_q")]
        public string nombre_pdv { get; set; }

        [JsonProperty("_r")]
        public string ReportsPlanning_Periodo { get; set; }
        [JsonProperty("_s")]
        public string id_Month { get; set; }
    }

    public class AjeExhibicionAdicionalDetalleMaterialReporte : A_E_Reporting_Detalle_Exhibicion
    {
        [JsonProperty("_a")]
        public string mat_codigo { get; set; }

        [JsonProperty("_b")]
        public string mat_descripcion { get; set; }

        [JsonProperty("_c")]
        public string foto { get; set; }

        [JsonProperty("_d")]
        public string existe { get; set; }

        [JsonProperty("_e")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_f")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_g")]
        public string anio { get; set; }

        [JsonProperty("_h")]
        public string mes { get; set; }

        [JsonProperty("_i")]
        public string cad_nom { get; set; }

        [JsonProperty("_j")]
        public string cantidad { get; set; }


    }


    public abstract class A_E_Reporting_Encarte_Detalle
    {
        //guia lunes 1
        public List<E_Reporting_Encarte_Detalle> Lista_EncarteDetalle(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Encarte_Detalle oRp = MvcApplication._Deserialize<Response_AjeReporting_Encarte_Detalle>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Encarte_Detalle_AASS(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
        }
    }
    public abstract class A_E_Reporting_Exhibicion
    {
        //haciendo lunes 1
        public List<E_Reporting_Excibicion_AASS> Exportar_Reporting_Exhibicion_AASS(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Exhibicion_AASS oRp = MvcApplication._Deserialize<Response_AjeReporting_Exhibicion_AASS>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Exhibicion_AASS(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
        }
    
        //haciendo lunes 1
        public List<E_Reporting_Excibicion_AASS> Exportar_Reporting_Exhibicion_AASS_Alicorp(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Exhibicion_AASS oRp = MvcApplication._Deserialize<Response_AjeReporting_Exhibicion_AASS>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Exhibicion_AASS_Alicorp(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
        }
    }
    public abstract class A_E_Reporting_Detalle_Exhibicion
    {
        //viernes 2
        public List<AjeExhibicionAdicionalDetalleMaterialReporte> Exportar_Detalle_Reporting_Exhibicion_AASS(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Detalle_Exhibicion_AASS oRp = MvcApplication._Deserialize<Response_AjeReporting_Detalle_Exhibicion_AASS>(
                    MvcApplication._Servicio_Operativa.Aje_Exportar_Excel_Detalle_Excibicion(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
            //return null;
        }

        public List<AjeExhibicionAdicionalDetalleMaterialReporte> Exportar_Detalle_Reporting_Exhibicion_AASS_alicorp(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Detalle_Exhibicion_AASS oRp = MvcApplication._Deserialize<Response_AjeReporting_Detalle_Exhibicion_AASS>(
                    MvcApplication._Servicio_Operativa.Alicorp_Exportar_Excel_Detalle_Excibicion(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Objeto;
            //return null;
        }
    }
    

    public class Request_AjeReporting_Parametros
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }
        [JsonProperty("_b")]
        public int subcanal { get; set; }
        [JsonProperty("_c")]
        public String categoria { get; set; }
        [JsonProperty("_d")]
        public String marca { get; set; }
        [JsonProperty("_e")]
        public String periodo { get; set; }
        [JsonProperty("_f")]
        public String zona { get; set; }
        [JsonProperty("_g")]
        public String distrito { get; set; }

        [JsonProperty("_h")]
        public String segmento { get; set; }
        [JsonProperty("_i")]
        public String cadena { get; set; }
        [JsonProperty("_j")]
        public String empresa { get; set; }
        [JsonProperty("_k")]
        public String tienda { get; set; }
        [JsonProperty("_l")]
        public String tipoactividad { get; set; }

        [JsonProperty("_m")]
        public String fecha_incidencia { get; set; }
        [JsonProperty("_n")]
        public String ciudad { get; set; }
        [JsonProperty("_o")]
        public String perfil { get; set; }
        [JsonProperty("_p")]
        public String semana { get; set; }

        [JsonProperty("_q")]
        public String producto { get; set; }

        [JsonProperty("_r")]
        public String cod_equipo { get; set; }
        [JsonProperty("_s")]
        public String cod_material { get; set; }
        [JsonProperty("_t")]
        public String cod_pdv { get; set; }
        [JsonProperty("_u")]
        public string subcanal2 { get; set; }
        [JsonProperty("_v")]
        public int periodo2 { get; set; }
        
        [JsonProperty("_w")]
        public int anio { get; set; }
        [JsonProperty("_x")]
        public int mes { get; set; }
        [JsonProperty("_y")]
        public int elemento { get; set; }
        [JsonProperty("_z")]
        public string dias { get; set; } 
    }


    #region Request & Response

    public class Response_AjeReporting_Encarte
    {
        [JsonProperty("_a")]
        public Encarte_Aje Objeto { get; set; }
    }
    public class Response_AjeReporting_Encarte_Detalle
    {
        [JsonProperty("_a")]
        public List<E_Reporting_Encarte_Detalle> Objeto { get; set; }
    }

    public class Response_AjeReporting_Exhibicion_AASS
    {
        [JsonProperty("_a")]
        public List<E_Reporting_Excibicion_AASS> Objeto { get; set; }
    }

    public class Response_AjeReporting_Detalle_Exhibicion_AASS
    {
        [JsonProperty("_a")]
        public List<AjeExhibicionAdicionalDetalleMaterialReporte> Objeto { get; set; }
    }
    #endregion

}