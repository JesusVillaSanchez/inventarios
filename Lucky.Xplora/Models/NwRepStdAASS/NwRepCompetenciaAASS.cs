﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Models.NwRepStdAASS
{
    #region << Competencia >>

    public class Competencia_Aje : A_Competencia_Aje
    {
        [JsonProperty("_a")]
        public List<E_Competencia_ACadena> CompetenciaxCadena { get; set; }
        [JsonProperty("_b")]
        public List<E_Competencia_PEmpresa> CompetenciaxEmpresa { get; set; }
        [JsonProperty("_c")]
        public List<E_Competencia_TActividades> CompetenciaxActividades { get; set; }
        [JsonProperty("_d")]
        public List<E_Competencia_AEvolutivo> CompetenciaxEvolutivo { get; set; }
    }
    public class E_Competencia_ACadena
    {
        [JsonProperty("_a")]
        public int id_cadena { get; set; }
        [JsonProperty("_b")]
        public string cadena { get; set; }
        [JsonProperty("_c")]
        public string empresa { get; set; }
        [JsonProperty("_d")]
        public string cantidad { get; set; }
        [JsonProperty("_e")]
        public string total { get; set; }

        [JsonProperty("_f")]
        public string color { get; set; }
    }
    public class E_Competencia_PEmpresa
    {
        [JsonProperty("_a")]
        public string empresa { get; set; }
        [JsonProperty("_b")]
        public string cantidad { get; set; }

        [JsonProperty("_c")]
        public string color { get; set; }
    }
    public class E_Competencia_TActividades
    {
        [JsonProperty("_a")]
        public string empresa { get; set; }
        [JsonProperty("_b")]
        public string actividad { get; set; }
        [JsonProperty("_c")]
        public string participacion { get; set; }
    }
    public class E_Competencia_AEvolutivo
    {
        [JsonProperty("_a")]
        public string periodo { get; set; }
        [JsonProperty("_b")]
        public string empresa { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }

        [JsonProperty("_d")]
        public string color { get; set; }
    }

    public abstract class A_Competencia_Aje
    {
        public Competencia_Aje Objeto_Competencia(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Competencia oRp = MvcApplication._Deserialize<Response_AjeReporting_Competencia>(
                    MvcApplication._Servicio_Operativa.Aje_Competencia(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    public class E_Reporting_Competencia_Detalle : A_E_Reporting_Competencia_Detalle
    {
        [JsonProperty("a")]
        public string Id_Row { get; set; }
        [JsonProperty("b")]
        public string oficina { get; set; }
        [JsonProperty("c")]
        public string cadena { get; set; }
        [JsonProperty("d")]
        public string fecha { get; set; }
        [JsonProperty("e")]
        public string empresa { get; set; }
        [JsonProperty("f")]
        public string marca { get; set; }
        [JsonProperty("g")]
        public string tipo_actividad { get; set; }
        [JsonProperty("h")]
        public string descripcion_comercial { get; set; }
        [JsonProperty("i")]
        public string id_competencia { get; set; }
        [JsonProperty("j")]
        public string categoria { get; set; }
        [JsonProperty("k")]
        public string foto { get; set; }
        [JsonProperty("l")]
        public string alcance { get; set; }
        [JsonProperty("m")]
        public string vigencia { get; set; }
        [JsonProperty("n")]
        public string codigo { get; set; }
        [JsonProperty("o")]
        public string precio { get; set; }
        [JsonProperty("p")]
        public string gramaje { get; set; }
        [JsonProperty("q")]
        public string impulso { get; set; }
        [JsonProperty("r")]
        public string pop { get; set; }
        [JsonProperty("s")]
        public string comentarios_adicionales { get; set; }
    }

    public abstract class A_E_Reporting_Competencia_Detalle
    {
        public List<E_Reporting_Competencia_Detalle> Lista_CompetenciaDetalle(Request_AjeReporting_Parametros oRq)
        {
            Response_AjeReporting_Competencia_Detalle oRp = MvcApplication._Deserialize<Response_AjeReporting_Competencia_Detalle>(
                    MvcApplication._Servicio_Operativa.Nw_Reporting_Competencia_Detalle_AASS(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Objeto;
        }
    }

    #region Request & Response

   
    public class Response_AjeReporting_Competencia
    {
        [JsonProperty("_a")]
        public Competencia_Aje Objeto { get; set; }
    }
    public class Response_AjeReporting_Competencia_Detalle
    {
        [JsonProperty("_a")]
        public List<E_Reporting_Competencia_Detalle> Objeto { get; set; }
    }

    #endregion

    #endregion
}