﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.Models.GestionDocumentalWeb
{
    public class PersonaUsuario
    {
        public Int64 IdUsuario { get;set;}

        public string Usuario { get; set; }

        public string Password { get; set; }

        public string Nombre { get; set; }

        public string ApellidoPaterno { get; set; }
        
        public string ApellidoMaterno { get; set; }

        public string Rol { get; set; }

        public int IdRol { get; set; }

        public bool estado { get; set; }

        public int? IdEstadoCivil { get; set; }
        public string EstadoCivil { get; set; }

        public string Dni { get; set; }

        public string Direccion { get; set; }

        public string Telefono { get; set; }

        public string TelefEmergencia { get; set; }

    }

    public class Rol {
        public int IdRol { get; set; }
        public string Descripciion { get; set; }
        public bool Estado { get; set; }
        public string Nombre { get; set; }
    }

    public class PrioridadProyecto {
        public int IdPrioridad { get; set; }
        public string Prioridad { get; set; }
    }

    public class EstadoCivilUsuario {
        public int IdEstadoCivil { get; set; }
        public string EstadoCivil { get; set; }
    }
}