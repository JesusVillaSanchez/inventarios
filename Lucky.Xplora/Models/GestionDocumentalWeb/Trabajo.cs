﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.Models.GestionDocumentalWeb
{
    public class Trabajo
    {
        public int IdTrabajo { get; set; }
        public string TrabajoNombre { get; set; }
        public string TrabajoDescripcion { get; set; }
        public Int64 IdUsuario { get; set; }
        public int IdArea { get; set; }
        public string Area { get; set; }
        public int IdTarea { get; set; }
        public string Tarea { get; set; }
        public Int64 IdEmpresa { get; set; }
        public string Empresa { get; set; }
        public int IdPrioridad { get; set; }
        public string Prioridad { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        //public int? CantidadDocumentos { get; set; }
    }

    public class ProyectoRequest {
        public int IdTrabajo { get; set; }
        public string TrabajoNombre { get; set; }
        public string TrabajoDescripcion { get; set; }
        public Int64 IdUsuario { get; set; }
        public int IdArea { get; set; }
        public int IdTarea { get; set; }
        public Int64 IdEmpresa { get; set; }
        public int IdPrioridad { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public List<DocumentosRequest> documentos{ get; set; }
}

    public class DocumentosRequest {
        public string nombreDocumento { get; set; }
        public string descripcionDocumento { get; set; }
        public string nombreArchivo { get; set; }
    }

    public class TrabajoResponseReporte1
    {
        public int? IdTrabajo { get; set; }
        public string TrabajoNombre { get; set; }
        public string TrabajoDescripcion { get; set; }
        public Int64? IdUsuario { get; set; }
        public int? IdArea { get; set; }
        public string Area { get; set; }
        public int? IdTarea { get; set; }
        public string Tarea { get; set; }
        public Int64? IdEmpresa { get; set; }
        public string Empresa { get; set; }
        public int IdPrioridad { get; set; }
        public string Prioridad { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public List<DocumentosResponse> Documentos { get; set; }
        public int? DL /* Cantidad total de Documentos con Archivos*/ { get; set; }
        public int? DB/* Cantidad total de documentos*/ { get; set; }
        public int? PL /* DL/DB*/ { get; set; }
    }

    public class TrabajoResponseReporte2
    {
        public int? IdTrabajo { get; set; }
        public string TrabajoNombre { get; set; }
        public string TrabajoDescripcion { get; set; }
        public Int64? IdUsuario { get; set; }
        public int? IdArea { get; set; }
        public string Area { get; set; }
        public int? IdTarea { get; set; }
        public string Tarea { get; set; }
        public Int64? IdEmpresa { get; set; }
        public string Empresa { get; set; }
        public int IdPrioridad { get; set; }
        public string Prioridad { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public List<DocumentosResponse> Documentos { get; set; }
        public int? UT { get; set; } /*Cantidad de documentos totales*/
        public int? UD { get; set; } /*Cantidad de documentos con descripcion*/
        public int? PD { get; set; } /*UD/UT*/
    }

    public class TrabajoResponse
    {
        public int? IdTrabajo { get; set; }
        public string TrabajoNombre { get; set; }
        public string TrabajoDescripcion { get; set; }
        public Int64? IdUsuario { get; set; }
        public int? IdArea { get; set; }
        public string Area { get; set; }
        public int? IdTarea { get; set; }
        public string Tarea { get; set; }
        public Int64? IdEmpresa { get; set; }
        public string Empresa { get; set; }
        public int IdPrioridad { get; set; }
        public string Prioridad { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public List<DocumentosResponse> Documentos { get; set; }
        public int? CantidadDocumentos { get; set; }
    }





    public class DocumentosResponse
    {
        public int? IdDocumento { get; set; }
        public int? IdTrabajo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Archivo { get; set; }
    }
}