﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.Models.GestionDocumentalWeb
{
    public class Empresa
    {
        public Int64 IdEmpresa { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Direccion { get; set; }

        public string Ciudad { get; set; }

        public string Telefono1 { get; set; }

        public string Telefono2 { get; set; }
     
    }
}