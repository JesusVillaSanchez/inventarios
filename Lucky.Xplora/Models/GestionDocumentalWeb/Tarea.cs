﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lucky.Xplora.Models.GestionDocumentalWeb
{
    public class Tarea
    {
        public int IdTarea { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }
    }
}