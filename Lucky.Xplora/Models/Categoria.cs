﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-17
    /// </summary>
    public class Categoria : ACategoria
    {
        [JsonProperty("a")]
        public int cat_id { get; set; }

        [JsonProperty("b")]
        public string cat_descripcion { get; set; }
    }

    public abstract class ACategoria {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-17
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Categoria> Lista(Request_GetCategoria_Opcion_Campania oRq)
        {
            Response_GetCategoria_Opcion_Campania oRp;

            oRp = MvcApplication._Deserialize<Response_GetCategoria_Opcion_Campania>(
                    MvcApplication._Servicio_Campania.GetCategoria_Opcion_Campania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Categoria> Lista(Request_GetCategoria_Opcion oRq)
        {
            Response_GetCategoria_Opcion oRp;

            oRp = MvcApplication._Deserialize<Response_GetCategoria_Opcion>(
                    MvcApplication._Servicio_Campania.GetCategoria_Opcion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-27
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Categoria> Lista(Listar_Categoria_Por_CodCampania_y_CodReporte_Request oRq)
        {
            Listar_Categoria_Por_CodCampania_y_CodReporte_Response oRp;

            oRp = MvcApplication._Deserialize<Listar_Categoria_Por_CodCampania_y_CodReporte_Response>(
                    MvcApplication._Servicio_Campania.Listar_Categoria_Por_CodCampania_y_CodReporte(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-03-16
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Categoria> Lista(Request_GetCategoria_Compania oRq)
        {
            Response_GetCategoria_Compania oRp;

            oRp = MvcApplication._Deserialize<Response_GetCategoria_Compania>(
                    MvcApplication._Servicio_Campania.GetCategoria_Compania(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-17
    /// </summary>
    public class Request_GetCategoria_Opcion_Campania
    {
        [JsonProperty("_a")]
        public string opcion { get; set; }

        [JsonProperty("_b")]
        public string campania { get; set; }
    }
    public class Response_GetCategoria_Opcion_Campania
    {
        [JsonProperty("_a")]
        public List<Categoria> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-14
    /// </summary>
    public class Request_GetCategoria_Opcion
    {
        [JsonProperty("_a")]
        public int opcion { get; set; }
    }
    public class Response_GetCategoria_Opcion
    {
        [JsonProperty("_a")]
        public List<Categoria> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-27
    /// </summary>
    public class Listar_Categoria_Por_CodCampania_y_CodReporte_Request
    {
        [JsonProperty("a")]
        public string campania { get; set; }

        [JsonProperty("b")]
        public int reporte { get; set; }
    }
    public class Listar_Categoria_Por_CodCampania_y_CodReporte_Response
    {
        [JsonProperty("a")]
        public List<Categoria> Lista { get; set; }
    }

    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 16-03-2015
    /// Descripcion: 
    /// </summary>
    /// <returns></returns>
    public class Request_GetCategoria_Compania
    {
        [JsonProperty("_a")]
        public int compania { get; set; }
    }
    public class Response_GetCategoria_Compania
    {
        [JsonProperty("_a")]
        public List<Categoria> Lista { get; set; }
    }

    #endregion
}