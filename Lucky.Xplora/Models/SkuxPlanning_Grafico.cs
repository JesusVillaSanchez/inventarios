﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class E_Sku_x_Planning_Modulo_Grafico_1
    {
        [JsonProperty("_a")]
        public string cod_Product { get; set; }
        [JsonProperty("_b")]
        public string Product_Name { get; set; }
        [JsonProperty("_c")]
        public string id_ProductCategory { get; set; }
    }
    #region Request & Response

    public class Request_SkuxPlanning_Grafico
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_channel { get; set;}

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public int cod_company { get; set; }

        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public int cod_reporte { get; set; }

        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public int servicio { get; set; }

        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_grafico { get; set; }

    }
    public class Response_SkuxPlanning_Grafico
    {
        [JsonProperty("a")]
        public List<E_Sku_x_Planning_Modulo_Grafico_1> Lista { get; set; }
    }

    #endregion
}