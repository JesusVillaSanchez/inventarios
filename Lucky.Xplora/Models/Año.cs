﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    public class Año : AAño
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public int año_id { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string año_descripcion { get; set; }
    }

    public abstract class AAño {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-11
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Año> Lista(Request_Anio_Por_Planning_Reports oRq) {
            Response_Anio_Por_Planning_Reports oRp;

            oRp = MvcApplication._Deserialize<Response_Anio_Por_Planning_Reports>(
                MvcApplication._Servicio_Campania.GetAnioI(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        public List<Año> ListaAnio(Request_Anio_reporte oRq)
        {
            Response_Anio_Reporte oRp;

            oRp = MvcApplication._Deserialize<Response_Anio_Reporte>(
                MvcApplication._Servicio_Campania.Anio_Por_Planning_Reports(
                    MvcApplication._Serialize(oRq))
                );
            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-11
    /// </summary>
    public class Request_Anio_Por_Planning_Reports
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string campania { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public int reporte { get; set; }
    }
    public class Response_Anio_Por_Planning_Reports
    {
        [JsonProperty("a")]
        public List<Año> Lista { get; set; }
    }

    public class Request_Anio_reporte
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string campania { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public int reporte { get; set; }
    }
    public class Response_Anio_Reporte
    {
        [JsonProperty("a")]
        public List<Año> Lista { get; set; }
    }


    #endregion
}