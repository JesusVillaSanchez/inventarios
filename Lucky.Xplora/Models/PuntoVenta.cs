﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-17
    /// </summary>
    public class PuntoVenta : APuntoVenta
    {
        [JsonProperty("a")]
        public string pdv_codigo { get; set; }

        [JsonProperty("b")]
        public string pdv_descripcion { get; set; }
    }

    public abstract class APuntoVenta {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-17
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<PuntoVenta> Lista(Request_PuntoVenta_Campania_Departamento_Provincia_NodoComercial oRq)
        {
            Response_PuntoVenta_Campania_Departamento_Provincia_NodoComercial oRp;

            oRp = MvcApplication._Deserialize<Response_PuntoVenta_Campania_Departamento_Provincia_NodoComercial>(
                    MvcApplication._Servicio_Campania.Listar_PuntoDeVenta_Por_CodCampania_CodDepartamento_CodProvincia_CodNodeCommercial(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-27
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<PuntoVenta> Lista(Listar_PuntoDeVenta_Por_CodCampania_CodNodeCommercial_Request oRq)
        {
            Listar_PuntoDeVenta_Por_CodCampania_CodNodeCommercial_Response oRp;

            oRp = MvcApplication._Deserialize<Listar_PuntoDeVenta_Por_CodCampania_CodNodeCommercial_Response>(
                    MvcApplication._Servicio_Campania.Listar_PuntoDeVenta_Por_CodCampania_CodNodeCommercial(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-06
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<PuntoVenta> Lista(Request_GetPuntoVenta_Campania_TipoCanal_NodoComercial oRq)
        {
            Response_GetPuntoVenta_Campania_TipoCanal_NodoComercial oRp;

            oRp = MvcApplication._Deserialize<Response_GetPuntoVenta_Campania_TipoCanal_NodoComercial>(
                    MvcApplication._Servicio_Campania.GetPuntoVenta_Campania_TipoCanal_NodoComercial(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2014-12-17
    /// </summary>
    public class Request_PuntoVenta_Campania_Departamento_Provincia_NodoComercial
    {
        [JsonProperty("a")]
        public string campania { get; set; }

        [JsonProperty("b")]
        public string departamento { get; set; }

        [JsonProperty("c")]
        public string provincia { get; set; }

        [JsonProperty("d")]
        public string nodocomercial { get; set; }
    }
    public class Response_PuntoVenta_Campania_Departamento_Provincia_NodoComercial
    {
        [JsonProperty("a")]
        public List<PuntoVenta> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-02-27
    /// </summary>
    public class Listar_PuntoDeVenta_Por_CodCampania_CodNodeCommercial_Request
    {
        [JsonProperty("a")]
        public string campania { get; set; }

        [JsonProperty("b")]
        public int nodocomercial { get; set; }
    }
    public class Listar_PuntoDeVenta_Por_CodCampania_CodNodeCommercial_Response
    {
        [JsonProperty("a")]
        public List<PuntoVenta> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-03-06
    /// </summary>
    public class Request_GetPuntoVenta_Campania_TipoCanal_NodoComercial
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int tipocanal { get; set; }

        [JsonProperty("_c")]
        public int nodocomercial { get; set; }

        [JsonProperty("_d")]
        public string codCiudad { get; set; }

        [JsonProperty("_e")]
        public string codProvincia { get; set; }
    }

    public class Response_GetPuntoVenta_Campania_TipoCanal_NodoComercial
    {
        [JsonProperty("a")]
        public List<PuntoVenta> Lista { get; set; }
    }

    #endregion

    #region Punto de venta map

    public class PuntoVentaMapa
    {
        [JsonProperty("a")]
        public string pdv_codigo { get; set; }

        [JsonProperty("b")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("c")]
        public string latitud { get; set; }

        [JsonProperty("d")]
        public string longitud { get; set; }

        [JsonProperty("e")]
        public string color { get; set; }

        [JsonProperty("f")]
        public string seg_descripcion { get; set; }
    }

    #endregion
}