﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Nw_Rp_AliMay_Filtro : AE_Nw_Rp_AliMay_Filtro
    {
        [JsonProperty("a")]
        public string Value { get; set; }
        [JsonProperty("b")]
        public string Text { get; set; }
    }
    public class Response_Nw_Rp_AliMay_Filtro
    {
        [JsonProperty("a")]
        public List<E_Nw_Rp_AliMay_Filtro> Response { get; set; }
    }
    public abstract class AE_Nw_Rp_AliMay_Filtro
    {
        public List<E_Nw_Rp_AliMay_Filtro> Consul_Filtro(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_Filtro oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_Filtro>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_Filtro(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
    }
}