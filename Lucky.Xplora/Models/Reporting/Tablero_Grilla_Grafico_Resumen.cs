﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Tablero_Minorista_Grilla_Grafico : AE_Tablero_Minorista_Grilla_Grafico
    {
        [JsonProperty("_a")]
        public List<E_Actividad_Promocional> oListaActividadPromocional { get; set; }
        [JsonProperty("_b")]
        public E_ShareOfPeopel oListaShareOfPeopel { get; set; }
        [JsonProperty("_c")]
        public List<E_Incentivos_Empresa> oListaIncentivosEmpresa { get; set; }
    }

    public class E_ShareOfPeopel
    {
        [JsonProperty("_a")]
        public List<E_Periodos_SOP> cabecera { get; set; }
        [JsonProperty("_b")]
        public List<E_Share_Of_People_Empresa> detalle { get; set; }
    }

    public class E_Actividad_Promocional
    {
        [JsonProperty("_a")]
        public string company_name { get; set; }
        [JsonProperty("_b")]
        public string Tipo_Actividad { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }
        [JsonProperty("_d")]
        public string porcentaje { get; set; }

    }

    public class E_Periodos_SOP
    {
        [JsonProperty("_a")]
        public string periodo_descripcion { get; set; }
    }

    public class E_Share_Of_People_Empresa
    {
        [JsonProperty("_a")]
        public string cod_oficina { get; set; }
        [JsonProperty("_b")]
        public string empresa { get; set; }
        [JsonProperty("_c")]
        public string periodo_5 { get; set; }
        [JsonProperty("_d")]
        public string periodo_4 { get; set; }
        [JsonProperty("_e")]
        public string periodo_3 { get; set; }
        [JsonProperty("_f")]
        public string periodo_2 { get; set; }
        [JsonProperty("_g")]
        public string periodo_1 { get; set; }
        [JsonProperty("_h")]
        public string porcentaje { get; set; }
    }

    public class E_Incentivos_Empresa
    {
        [JsonProperty("_a")]
        public string empresa { get; set; }
        [JsonProperty("_b")]
        public string TipoIncentivo { get; set; }
        [JsonProperty("_c")]
        public string Dirigido_desc { get; set; }
        [JsonProperty("_d")]
        public string Mes { get; set; }
        [JsonProperty("_e")]
        public string Duracion_dias { get; set; }
        [JsonProperty("_f")]
        public string porcentaje { get; set; }
        [JsonProperty("_g")]
        public string cod_mes { get; set; }
    }

    public abstract class AE_Tablero_Minorista_Grilla_Grafico {
        public E_Tablero_Minorista_Grilla_Grafico Tablero_Minorista_Grilla_Grafico(Request_Indicador_Presencia_Sku oRq)
        {
            Response_BI_Tablero_Grilla_Grafico_Resumen oRp;

            oRp = MvcApplication._Deserialize<Response_BI_Tablero_Grilla_Grafico_Resumen>(MvcApplication._Servicio_Operativa.BL_BI_Tablero_Grilla_Grafico_Resumen(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
    }

    public class Response_BI_Tablero_Grilla_Grafico_Resumen
    {
        [JsonProperty("a")]
        public E_Tablero_Minorista_Grilla_Grafico Response { get; set; }
    }

}