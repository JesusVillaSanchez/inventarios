﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class NW_Hig_Grafico : NW_Hig_Grafico_service
    {
        [JsonProperty("a")]
        public List<E_Hig_Grafico_Categoria> Categoria { get; set; }
        [JsonProperty("b")]
        public List<E_Hig_Grafico_Serie> Hijo { get; set; }
    }
    public class E_Hig_Grafico_Categoria
    {
        [JsonProperty("a")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("b")]
        public string Nom_Categoria { get; set; }
    }
    public class E_Hig_Grafico_Serie
    {
        [JsonProperty("a")]
        public string Cod_Serie { get; set; }
        [JsonProperty("b")]
        public string Nom_Serie { get; set; }
        [JsonProperty("c")]
        public List<E_Hig_Grafico_Serie_Data> Hijo { get; set; }
    }
    public class E_Hig_Grafico_Serie_Data
    {
        [JsonProperty("a")]
        public string Valor { get; set; }
    }

    public class E_Hig_Grafico_PRT
    {
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("data")]
        public List<string> data { get; set; }
    }

    public abstract class NW_Hig_Grafico_service
    {
        #region <<< Alicorp Minorista >>>
        #region <<< Reporte SOD >>>
            public NW_Hig_Grafico ALi_MN_Consul_SOD_x_OficinaTipoGiro(Request_NW_Reporting oRq)
            {
                Response_SOD_x_OficinaTipoGiro oRp;

                oRp = MvcApplication._Deserialize<Response_SOD_x_OficinaTipoGiro>(
                        MvcApplication._Servicio_Operativa.NW_RP_Consul_SOD_x_OficinaTipoGiro(
                            MvcApplication._Serialize(oRq)
                        )
                    );

                return oRp.Response;
            }
            public NW_Hig_Grafico ALi_MN_Consul_SOD_x_OficinaTipoGiro_Acumulado(Request_NW_Reporting oRq)
            {
                Response_SOD_x_OficinaTipoGiro oRp;

                oRp = MvcApplication._Deserialize<Response_SOD_x_OficinaTipoGiro>(
                        MvcApplication._Servicio_Operativa.NW_RP_Consul_SOD_x_OficinaTipoGiro_Acu(
                            MvcApplication._Serialize(oRq)
                        )
                    );

                return oRp.Response;
            }
            public NW_Hig_Grafico ALi_MN_Consul_SOD_x_Categoria_Marca(Request_NW_Reporting oRq)
            {
                Response_SOD_x_OficinaTipoGiro oRp;

                oRp = MvcApplication._Deserialize<Response_SOD_x_OficinaTipoGiro>(
                        MvcApplication._Servicio_Operativa.NW_RP_Consul_SOD_x_Categoria_Marca(
                            MvcApplication._Serialize(oRq)
                        )
                    );

                return oRp.Response;
            }
            public NW_Hig_Grafico ALi_MN_Consul_SOD_x_Categoria_Marca_Acumulado(Request_NW_Reporting oRq)
            {
                Response_SOD_x_OficinaTipoGiro oRp;

                oRp = MvcApplication._Deserialize<Response_SOD_x_OficinaTipoGiro>(
                        MvcApplication._Servicio_Operativa.NW_RP_Consul_SOD_x_Categoria_Marca_Acu(
                            MvcApplication._Serialize(oRq)
                        )
                    );

                return oRp.Response;
            }
            public E_Parametros_Iniciales ALi_MN_Consul_Parametros_Iniciales(Request_NW_Reporting oRq)
            {
                Response_SOD_ParametrosIni oRp;

                oRp = MvcApplication._Deserialize<Response_SOD_ParametrosIni>(
                        MvcApplication._Servicio_Operativa.NW_RP_Parametros_Ini(
                            MvcApplication._Serialize(oRq)
                        )
                    );

                return oRp.Response;
            }
        #endregion
        #endregion

    }

    #region Parametros
    public class E_Hig_Grafico_Parametros
    {
        [JsonProperty("a")]
        public int Cod_Periodo { get; set; }
        [JsonProperty("b")]
        public int Cod_Giro { get; set; }
        [JsonProperty("c")]
        public int Cod_Oficina { get; set; }
        [JsonProperty("d")]
        public int Anio { get; set; }
        [JsonProperty("e")]
        public int Cod_Trimestre { get; set; }
        [JsonProperty("f")]
        public string Cod_Equipo { get; set; }
        [JsonProperty("g")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("h")]
        public int Cod_Op { get; set; }
        [JsonProperty("i")]
        public string Parametros { get; set; }
        [JsonProperty("j")]
        public string Cod_Mes { get; set; }
        [JsonProperty("k")]
        public int Cod_Cobertura { get; set; }
        [JsonProperty("l")]
        public string Cod_Perfil { get; set; }
        [JsonProperty("m")]
        public int Cod_Zona { get; set; }
        [JsonProperty("n")]
        public int Cod_Mercado { get; set; }
        [JsonProperty("o")]
        public int Cod_Reporte { get; set; }
        [JsonProperty("p")]
        public string Cod_Nivel { get; set; }
        [JsonProperty("q")]
        public int Cod_Marca { get; set; }
        [JsonProperty("r")]
        public string Cod_SKU { get; set; }
        [JsonProperty("s")]
        public string Cod_canal { get; set; }

    }
        public class E_Parametros_Iniciales
        {
            [JsonProperty("a")]
            public int Anio { get; set; }
            [JsonProperty("b")]
            public string Cod_Mes { get; set; }
            [JsonProperty("c")]
            public int Cod_Cobertura { get; set; }
            [JsonProperty("d")]
            public int Cod_Giro { get; set; }
            [JsonProperty("e")]
            public int Cod_Periodo { get; set; }
            [JsonProperty("f")]
            public int Cod_Oficina1 { get; set; }
            [JsonProperty("g")]
            public int Cod_Oficina2 { get; set; }
            [JsonProperty("h")]
            public string Cod_Categoria { get; set; }
            [JsonProperty("i")]
            public int Cod_Trimestre { get; set; }
        }
    #endregion

    #region Request - Response
        public class Request_NW_Reporting
        {
            [JsonProperty("_a")]
            public E_Hig_Grafico_Parametros oParametros { get; set; }
        }
        public class Response_SOD_x_OficinaTipoGiro
        {
            [JsonProperty("a")]
            public NW_Hig_Grafico Response { get; set; }
        }
        public class Response_SOD_ParametrosIni 
        {
            [JsonProperty("a")]
            public E_Parametros_Iniciales Response { get; set; }
        }
    #endregion
}