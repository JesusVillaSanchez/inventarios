﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Nw_Rp_AliMay_ResumenNNCatg : AE_Nw_Rp_AliMay_ResumenNNCatg
    {
        [JsonProperty("a")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("b")]
        public string Categoria { get; set; }
        [JsonProperty("c")]
        public string Stock_Inicial_Ton { get; set; }
        [JsonProperty("d")]
        public string Ventas_Ton { get; set; }
        [JsonProperty("e")]
        public string DG { get; set; }
        [JsonProperty("f")]
        public string Fecha_Relevo { get; set; }
    }
    public class Response_Nw_Rp_AliMay_ResumenCatg
    {
        [JsonProperty("a")]
        public List<E_Nw_Rp_AliMay_ResumenNNCatg> Response { get; set; }
    }

    public abstract class AE_Nw_Rp_AliMay_ResumenNNCatg
    {
        public List<E_Nw_Rp_AliMay_ResumenNNCatg> Consul_ResumenNN_Catg(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_ResumenCatg oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_ResumenCatg>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_ResumenNNCatg(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
    }
}