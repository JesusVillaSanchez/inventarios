﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Nw_Rp_AliMay_DetalleDGPDV : AE_Nw_Rp_AliMay_DetalleDGPDV
    {
        [JsonProperty("a")]
        public List<string> Cabecera { get; set; }
        [JsonProperty("b")]
        public List<E_Nw_Rp_AliMay_DetalleDGPDV_Fila> Filas { get; set; }
    }
    public class E_Nw_Rp_AliMay_DetalleDGPDV_Fila
    {
        [JsonProperty("a")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("b")]
        public string Categoria { get; set; }
        [JsonProperty("c")]
        public string Cod_Oficina { get; set; }
        [JsonProperty("d")]
        public string Oficina { get; set; }
        [JsonProperty("e")]
        public string Cod_PDV { get; set; }
        [JsonProperty("f")]
        public string Nom_PDV { get; set; }
        [JsonProperty("g")]
        public List<string> Periodos { get; set; }
    }
    public class Response_Nw_Rp_AliMay_Detalle_DG_PDV
    {
        [JsonProperty("a")]
        public E_Nw_Rp_AliMay_DetalleDGPDV Response { get; set; }
    }
    public abstract class AE_Nw_Rp_AliMay_DetalleDGPDV
    {
        public E_Nw_Rp_AliMay_DetalleDGPDV Consul_Detalle_DG_PDV(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_Detalle_DG_PDV oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_Detalle_DG_PDV>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_Detalle_DG_PDV(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
    }
}