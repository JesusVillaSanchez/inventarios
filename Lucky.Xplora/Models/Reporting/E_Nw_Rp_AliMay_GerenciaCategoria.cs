﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Nw_Rp_AliMay_GerenciaCategoria : AE_Nw_Rp_AliMay_GerenciaCategoria
    {
        [JsonProperty("a")]
        public string Cod_Oficina { get; set; }
        [JsonProperty("b")]
        public string Nom_Oficina { get; set; }
        [JsonProperty("c")]
        public string Stock_Ini_Ton { get; set; }
        [JsonProperty("d")]
        public string Venta_Ton { get; set; }
        [JsonProperty("e")]
        public string Stock_Fin_Ton { get; set; }
        [JsonProperty("f")]
        public string Sell_In_Ton { get; set; }
        [JsonProperty("g")]
        public string Sell_Out_Ton { get; set; }
        [JsonProperty("h")]
        public string DG { get; set; }
        [JsonProperty("i")]
        public string DG_Indicador { get; set; }
    }
    public class Response_Nw_Rp_AliMay_GerenciaCategoria 
    {
        [JsonProperty("a")]
        public List<E_Nw_Rp_AliMay_GerenciaCategoria> Response { get; set; }
    }
    public abstract class AE_Nw_Rp_AliMay_GerenciaCategoria
    {
        public List<E_Nw_Rp_AliMay_GerenciaCategoria> Consul_Gerencia_Categoria(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_GerenciaCategoria oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_GerenciaCategoria>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_Gerencia_Categoria(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
        public List<E_Nw_Rp_AliMay_GerenciaCategoria> Consul_Gerencia_Marca(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_GerenciaCategoria oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_GerenciaCategoria>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_Gerencia_Marca(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
    }

}