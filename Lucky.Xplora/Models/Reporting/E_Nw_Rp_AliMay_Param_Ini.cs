﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Nw_Rp_AliMay_Param_Ini : AE_Nw_Rp_AliMay_Param_Ini
    {
        [JsonProperty("a")]
        public int Cod_Periodo { get; set; }
        [JsonProperty("c")]
        public int Cod_Oficina { get; set; }
        [JsonProperty("d")]
        public int Anio { get; set; }
        [JsonProperty("g")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("j")]
        public string Cod_Mes { get; set; }
        [JsonProperty("q")]
        public int Cod_Marca { get; set; }

    }
    public class Response_Nw_Rp_AliMay_Param_Ini
    {
        [JsonProperty("a")]
        public E_Nw_Rp_AliMay_Param_Ini Response { get; set; }
    }
    public abstract class AE_Nw_Rp_AliMay_Param_Ini
    {
        public E_Nw_Rp_AliMay_Param_Ini Consul_Param_Ini(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_Param_Ini oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_Param_Ini>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_Param_Ini(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
    }
}