﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Nw_Rp_AliMay_EvolutivoOfiCatg : AE_Nw_Rp_AliMay_EvolutivoOfiCatg
    {
        [JsonProperty("a")]
        public string Cod_Categoria { get; set; }
        [JsonProperty("b")]
        public string Nom_Categoria { get; set; }
        [JsonProperty("c")]
        public string Periodo { get; set; }
        [JsonProperty("d")]
        public string Inicio_Relevo { get; set; }
        [JsonProperty("e")]
        public string Fin_Relevo { get; set; }
        [JsonProperty("f")]
        public string Stock_Inicial_Ton { get; set; }
        [JsonProperty("g")]
        public string Ventas_Ton { get; set; }
        [JsonProperty("h")]
        public string Stock_Fin_Ton { get; set; }
        [JsonProperty("i")]
        public string Sell_In_Ton { get; set; }
        [JsonProperty("j")]
        public string Sell_In_Type { get; set; }
        [JsonProperty("k")]
        public string Sell_Out_Ton { get; set; }
        [JsonProperty("l")]
        public string Sell_Out_Type { get; set; }
        [JsonProperty("m")]
        public string DG { get; set; }
        [JsonProperty("n")]
        public string DG_Type { get; set; }
        [JsonProperty("o")]
        public string DG_Indicador { get; set; }
    }

    public class Response_Nw_Rp_AliMay_EvolivoOfi_Catg 
    {
        [JsonProperty("a")]
        public List<E_Nw_Rp_AliMay_EvolutivoOfiCatg> Response { get; set; }
    }
    public abstract class AE_Nw_Rp_AliMay_EvolutivoOfiCatg
    {
        public List<E_Nw_Rp_AliMay_EvolutivoOfiCatg> Consul_Evolutivo_Categoria(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_EvolivoOfi_Catg oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_EvolivoOfi_Catg>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_EvolutivoOfi_catg(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
        public List<E_Nw_Rp_AliMay_EvolutivoOfiCatg> Consul_Evolutivo_Marca(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_EvolivoOfi_Catg oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_EvolivoOfi_Catg>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_EvolutivoOfi_marca(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
    }
}