﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Grafico_Sellout : NW_Indicadores_Grafico_service
    {

        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string mercaderismo { get; set; }
        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string cluster { get; set; }
        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string plan_ton { get; set; }
        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string real_ton { get; set; }
        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public string porc_cumpli { get; set; }
    }

    public class E_Grafico_NSG : NW_Indicadores_Grafico_NSG_service
    {   
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string mercaderismo { get; set; }
        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string cluster { get; set; }
        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string objetivo { get; set; }
        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string resultado { get; set; }
        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public string diferencia { get; set; }
    }

    public class E_Grafico_SOD : NW_Indicadores_Grafico_SOD_service
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string mercaderismo { get; set; }
        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string cluster { get; set; }
        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string resultado { get; set; }
        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string objetivo { get; set; }
        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public string SOD_vs_Layout { get; set; }
    }

    public class E_Grafico_EEAA : NW_Indicadores_Grafico_EEAA_service
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public string mercaderismo { get; set; }
        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string cluster { get; set; }
        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string Objetivo { get; set; }
        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string Resultado { get; set; }
        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public string Diferencia { get; set; }
        [JsonProperty("f", NullValueHandling = NullValueHandling.Ignore)]
        public string Cumplimiento { get; set; }
    }

    public class E_NW_Table_tblStrategy : OE_NW_Table_tblStrategy
    {
        [JsonProperty("a")]
        public List<E_NW_Table_cab_tblStrategy> Lis_cab { get; set; }
        [JsonProperty("b")]
        public List<E_NW_Table_det_tblStrategy> Lis_det { get; set; }
    }

    #region Entidad

    public class E_NW_Table_cab_tblStrategy
    {
        [JsonProperty("a")]
        public string nom_cab { get; set; }
        [JsonProperty("b")]
        public int Can_cab { get; set; }
    }

    public class E_NW_Table_det_tblStrategy
    {
        [JsonProperty("a")]
        public string mercaderismo { get; set; }
        [JsonProperty("b")]
        public int cant_g1 { get; set; }
        [JsonProperty("c")]
        public string cluster { get; set; }
        [JsonProperty("d")]
        public int cant_g2 { get; set; }
        [JsonProperty("e")]
        public string indicador { get; set; }
        [JsonProperty("f")]
        public int cant_g3 { get; set; }
        [JsonProperty("g")]
        public string texto { get; set; }
        [JsonProperty("h")]
        public string val1 { get; set; }
        [JsonProperty("i")]
        public string val2 { get; set; }
        [JsonProperty("j")]
        public string val3 { get; set; }
        [JsonProperty("k")]
        public string val4 { get; set; }
        [JsonProperty("l")]
        public string val5 { get; set; }

        [JsonProperty("m")]
        public string val6 { get; set; }
        [JsonProperty("n")]
        public string val7 { get; set; }
        [JsonProperty("ñ")]
        public string val8 { get; set; }

        //[JsonProperty("a")]
        //public string nom_campo { get; set; }
        //[JsonProperty("b")]
        //public int cant_filas { get; set; }
        //[JsonProperty("c")]
        //public List<E_NW_Table_det_tblStrategy> Hijo { get; set; }
        //[JsonProperty("d")]
        //public List<E_NW_Table_filas_tblStrategy> detalle { get; set; }

    }
    public class E_NW_Table_filas_tblStrategy
    {
        [JsonProperty("a")]
        public string nom_indicador { get; set; }
        [JsonProperty("b")]
        public string icon_indicador1 { get; set; }
        [JsonProperty("c")]
        public string val_indicador1 { get; set; }
        [JsonProperty("d")]
        public string icon_indicador2 { get; set; }
        [JsonProperty("e")]
        public string val_indicador2 { get; set; }
        [JsonProperty("f")]
        public string icon_indicador3 { get; set; }
        [JsonProperty("g")]
        public string val_indicador3 { get; set; }
        [JsonProperty("h")]
        public string icon_indicador4 { get; set; }
        [JsonProperty("i")]
        public string val_indicador4 { get; set; }
        [JsonProperty("j")]
        public string icon_indicador5 { get; set; }
        [JsonProperty("k")]
        public string val_indicador5 { get; set; }

    }

    #endregion

    public abstract class NW_Indicadores_Grafico_service
    {
        public List<E_Grafico_Sellout> Lista(Request_Graf_SellOut oRq)
        {
            Response_Graf_SellOut oRp;

            oRp = MvcApplication._Deserialize<Response_Graf_SellOut>(
                    MvcApplication._Servicio_Operativa.Bl_Graf_SllOut(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class NW_Indicadores_Grafico_NSG_service
    {
        public List<E_Grafico_NSG> Lista(Request_Graf_SellOut oRq)
        {
            Response_Graf_NSG oRp;

            oRp = MvcApplication._Deserialize<Response_Graf_NSG>(
                    MvcApplication._Servicio_Operativa.Bl_Graf_NSG(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class NW_Indicadores_Grafico_SOD_service
    {
        public List<E_Grafico_SOD> Lista(Request_Graf_SellOut oRq)
        {
            Response_Graf_SOD oRp;

            oRp = MvcApplication._Deserialize<Response_Graf_SOD>(
                    MvcApplication._Servicio_Operativa.Bl_Graf_SOD(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class NW_Indicadores_Grafico_EEAA_service
    {
        public List<E_Grafico_EEAA> Lista(Request_Graf_SellOut oRq)
        {
            Response_Graf_EEAA oRp;

            oRp = MvcApplication._Deserialize<Response_Graf_EEAA>(
                    MvcApplication._Servicio_Operativa.Bl_Graf_EEAA(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    public abstract class OE_NW_Table_tblStrategy {
        public E_NW_Table_tblStrategy Consul_TB_Strategic_Board(Request_NW_Table_tblStrategy oRq)
        {
            Response_NW_Table_tblStrategy oRp;

            oRp = MvcApplication._Deserialize<Response_NW_Table_tblStrategy>(
                    MvcApplication._Servicio_Operativa.Consul_Grid_AliReporting_Strategic_Board(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }

    #region Request - Response

    public class Request_Graf_SellOut
    {
        [JsonProperty("a")]
        public int anio { get; set; }
        [JsonProperty("b")]
        public int mes { get; set; }
        [JsonProperty("c")]
        public int cod_oficina { get; set; }
        [JsonProperty("d")]
        public string cod_cadena { get; set; }
        [JsonProperty("e")]
        public string cod_categoria { get; set; }
        [JsonProperty("f")]
        public int cod_marca { get; set; }
    }
    public class Response_Graf_SellOut
    {
        [JsonProperty("a")]
        public List<E_Grafico_Sellout> Lista { get; set; }
    }

    public class Response_Graf_NSG
    {
        [JsonProperty("a")]
        public List<E_Grafico_NSG> Lista { get; set; }
    }
    public class Response_Graf_SOD
    {
        [JsonProperty("a")]
        public List<E_Grafico_SOD> Lista { get; set; }
    }
    public class Response_Graf_EEAA
    {
        [JsonProperty("a")]
        public List<E_Grafico_EEAA> Lista { get; set; }
    }

    public class E_Strategic_Board_Grafico_Parametros
    {
        [JsonProperty("a")]
        public int anio { get; set; }
        [JsonProperty("b")]
        public int mes { get; set; }
        [JsonProperty("c")]
        public int cod_oficina { get; set; }
        [JsonProperty("d")]
        public string cod_cadena { get; set; }
        [JsonProperty("e")]
        public string cod_categoria { get; set; }
        [JsonProperty("f")]
        public int cod_marca { get; set; }
    }

    public class Request_NW_Table_tblStrategy
    {
        [JsonProperty("a")]
        public E_Strategic_Board_Grafico_Parametros oParametros { get; set; }
    }
    public class Response_NW_Table_tblStrategy
    {
        [JsonProperty("a")]
        public E_NW_Table_tblStrategy Response { get; set; }
    }

    #endregion

}