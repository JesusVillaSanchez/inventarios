﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace Lucky.Xplora.Models.Reporting
{
    public class E_NW_RPG_TABLA : OE_NW_RPG_TABLA
    {
        [JsonProperty("a")]
        public List<E_NW_RPG_TABLA_CABECERA> Cab_Tabla { get; set; }
        [JsonProperty("b")]
        public List<E_NW_RPG_TABLA_DET> List_Filas { get; set; }
        [JsonProperty("c")]
        public List<E_NW_RPG_GRAF_TB_CATEGORIA> Gra_Categorias { get; set; }
    }


    #region <<< Entidad >>>
    public class E_NW_RPG_TABLA_CABECERA
    {
        [JsonProperty("a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("b")]
        public List<E_NW_RPG_TABLA_CABECERA_VAL> Val_Cab { get; set; }
    }
    public class E_NW_RPG_TABLA_CABECERA_VAL
    {
        [JsonProperty("a")]
        public int Cod_Interno { get; set; }
        [JsonProperty("b")]
        public int Cod_Cab { get; set; }
        [JsonProperty("c")]
        public string Nom_Cab { get; set; }
        [JsonProperty("d")]
        public int Item_select { get; set; }
        [JsonProperty("e")]
        public string Color { get; set; }
        [JsonProperty("f")]
        public string Mascara { get; set; }
        [JsonProperty("g")]
        public string Posicion { get; set; }
        [JsonProperty("h")]
        public string Posicioncab { get; set; }
    }
    public class E_NW_RPG_TABLA_DET
    {
        [JsonProperty("a")]
        public List<E_NW_RPG_TABLA_DET_ROW> Fila { get; set; }
    }
    public class E_NW_RPG_TABLA_DET_ROW
    {
        [JsonProperty("a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("b")]
        public List<E_NW_RPG_TABLA_DET_ITEM> Val_Items { get; set; }
    }
    public class E_NW_RPG_TABLA_DET_ITEM
    {
        [JsonProperty("a")]
        public string Val_Item { get; set; }
        [JsonProperty("b")]
        public string Val_Mascara { get; set; }
        [JsonProperty("c")]
        public string Val_Posicion { get; set; }
        [JsonProperty("d")]
        public string Item_Color { get; set; }
    }
    public class E_NW_RPG_GRAF_TB_CATEGORIA
    {
        [JsonProperty("a")]
        public string Nom_Titulo { get; set; }
        [JsonProperty("b")]
        public List<String> nom_categoria { get; set; }
        [JsonProperty("c")]
        public List<E_NW_RPG_GRAF_TB_CATEGORIA_SERIA> Serie { get; set; }

    }
    public class E_NW_RPG_GRAF_TB_CATEGORIA_SERIA
    {
        [JsonProperty("a")]
        public string Nom_serie { get; set; }
        [JsonProperty("b")]
        public List<String> List_Seria { get; set; }
    }
    public class E_NW_RPG_FUSION_P_INCIALES
    {
        [JsonProperty("a")]
        public int Anio { get; set; }
        [JsonProperty("b")]
        public int Cod_Periodo { get; set; }
        [JsonProperty("c")]
        public int Cod_Oficina { get; set; }
        [JsonProperty("d")]
        public int Cod_Zona { get; set; }
        [JsonProperty("e")]
        public int Cod_Mercado { get; set; }

    }

    //rcontreras
    //07-04-2015
    public class E_INDICADORES_TB : indi_tab_estra
    {
        [JsonProperty("_a")]
        public string sellout_actu { get; set; }
        [JsonProperty("_b")]
        public double sellout_ante { get; set; }
        [JsonProperty("_c")]
        public double nsg_actu { get; set; }
        [JsonProperty("_d")]
        public double nsg_ante { get; set; }
        [JsonProperty("_e")]
        public double sod_actu { get; set; }
        [JsonProperty("_f")]
        public double sod_ante { get; set; }
        [JsonProperty("_g")]
        public string eeaa_actu { get; set; }
        [JsonProperty("_h")]
        public double eeaa_ante { get; set; }
    }

    #endregion
    #region <<< Response >>>
    public class Response_E_NW_RPG_TABLA 
    {
        [JsonProperty("a")]
        public E_NW_RPG_TABLA Response { get; set; }
    }
    public class Response_FusionColgate_ParametrosIni 
    {
        [JsonProperty("a")]
        public E_NW_RPG_FUSION_P_INCIALES Response { get; set; }
    }
    #endregion

    public abstract class OE_NW_RPG_TABLA
    {
        public E_NW_RPG_TABLA Consul_TB_Precio_Sumary(Request_NW_Reporting oRq)
        {
            Response_E_NW_RPG_TABLA oRp;

            oRp = MvcApplication._Deserialize<Response_E_NW_RPG_TABLA>(
                    MvcApplication._Servicio_Operativa.Consul_ColgReporting_Precio_Summary(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
        public E_NW_RPG_TABLA Consul_TB_Precio_Categoria(Request_NW_Reporting oRq)
        {
            Response_E_NW_RPG_TABLA oRp;

            oRp = MvcApplication._Deserialize<Response_E_NW_RPG_TABLA>(
                    MvcApplication._Servicio_Operativa.Consul_ColgReporting_Precio_Categoria(
                        MvcApplication._Serialize(oRq)
                    )
                );    

            return oRp.Response;
        }
        public E_NW_RPG_TABLA Consul_TB_Precio_Compliance(Request_NW_Reporting oRq)
        {
            Response_E_NW_RPG_TABLA oRp;

            oRp = MvcApplication._Deserialize<Response_E_NW_RPG_TABLA>(
                    MvcApplication._Servicio_Operativa.Consul_ColgReporting_Precio_Ctg_Compliance(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
        public E_NW_RPG_FUSION_P_INCIALES Consul_PametrosIni(Request_NW_Reporting oRq)
        {
            Response_FusionColgate_ParametrosIni oRp;

            oRp = MvcApplication._Deserialize<Response_FusionColgate_ParametrosIni>(
                    MvcApplication._Servicio_Operativa.Consul_ParamIni_ColgFusion(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Response;
        }
    }
    //rcontreras
    //07-04-2015
    public abstract class indi_tab_estra {
        public List<E_INDICADORES_TB> Lista(Request_Indi_Tab_Estra oRq)
        {
            Response_Indi_Tab_Estra oRp;

            oRp = MvcApplication._Deserialize<Response_Indi_Tab_Estra>(
                    MvcApplication._Servicio_Operativa.IndicadorTableroEstrategico(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }


    /// <summary>
    /// Autor: rcontreras
    /// Fecha: 07-04-2015
    /// </summary>
    public class Request_Indi_Tab_Estra
    {
        [JsonProperty("_a")]
        public int anio { get; set; }
        [JsonProperty("_b")]
        public int mes { get; set; }
        [JsonProperty("_c")]
        public int oficina { get; set; }
        [JsonProperty("_d")]
        public string cadena { get; set; }
        [JsonProperty("_e")]
        public string categoria { get; set; }
        [JsonProperty("_f")]
        public int marca { get; set; }
    }
    public class Response_Indi_Tab_Estra
    {
        [JsonProperty("_a")]
        public List<E_INDICADORES_TB> Lista { get; set; }
    }
}