﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Nw_Rp_AliMay_EvoClixPeriodo : AE_Nw_Rp_AliMay_EvoClixPeriodo
    {
        [JsonProperty("a")]
        public int Orden { get; set; }
        [JsonProperty("b")]
        public string Nom_PDV { get; set; }
        [JsonProperty("c")]
        public string Nom_Marca { get; set; }
        [JsonProperty("d")]
        public string Stock_Ini_Ton { get; set; }
        [JsonProperty("e")]
        public string Ventas_Ton { get; set; }
        [JsonProperty("f")]
        public string Stock_Fin_Ton { get; set; }
        [JsonProperty("g")]
        public string Sell_In_Ton { get; set; }
        [JsonProperty("h")]
        public string Sell_Out_Ton { get; set; }
        [JsonProperty("i")]
        public string DG { get; set; }
    }
    public class Response_Nw_Rp_AliMay_EvoClixPeriodo
    {
        [JsonProperty("a")]
        public List<E_Nw_Rp_AliMay_EvoClixPeriodo> Response { get; set; }
    }
    public abstract class AE_Nw_Rp_AliMay_EvoClixPeriodo
    {
        public List<E_Nw_Rp_AliMay_EvoClixPeriodo> Consul_EvolutivoxPeriodo_Categoria(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_EvoClixPeriodo oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_EvoClixPeriodo>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_EvoClixPeriodoCatg(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
        public List<E_Nw_Rp_AliMay_EvoClixPeriodo> Consul_EvolutivoxPeriodo_Marca(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_EvoClixPeriodo oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_EvoClixPeriodo>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_EvoClixPeriodoMarca(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
    }

}