﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class Tablero_Grilla_Venta : ATablero_Grilla_Venta
    {
        [JsonProperty("_a")]
        public List<string> List_Meses { get; set; }
        [JsonProperty("_b")]
        public List<string> List_Agrupados { get; set; }
        [JsonProperty("_c")]
        public List<Tab_Content_Grilla_Venta> Grilla { get; set; }
    }
    public class Tab_Content_Grilla_Venta
    {
        [JsonProperty("_a")]
        public int Cod_Grupo { get; set; }
        [JsonProperty("_b")]
        public string Nom_Grupo { get; set; }
        [JsonProperty("_c")]
        public int Cod_SubGrupo { get; set; }
        [JsonProperty("_d")]
        public string Nom_SubGrupo { get; set; }
        [JsonProperty("_e")]
        public string Periodo1 { get; set; }
        [JsonProperty("_f")]
        public string Periodo2 { get; set; }
        [JsonProperty("_g")]
        public string Periodo3 { get; set; }
        [JsonProperty("_h")]
        public string Periodo4 { get; set; }
    }
    public class Tablero_Grilla_Venta_cantidad
    {
        [JsonProperty("_a")]
        public int Codigo { get; set; }
        [JsonProperty("_b")]
        public int Cod_Anio { get; set; }
        [JsonProperty("_c")]
        public int Cantidad { get; set; }
    }
    public abstract class ATablero_Grilla_Venta
    {
        public Tablero_Grilla_Venta Grilla_ReporteVenta(Request_BI_Tablero_Rep_Venta oRq)
        {
            Response_BI_Tablero_Rep_Venta oRp;

            oRp = MvcApplication._Deserialize<Response_BI_Tablero_Rep_Venta>(MvcApplication._Servicio_Operativa.BL_BI_Tablero_Rep_Venta(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
        public List<Tablero_Grilla_Venta_cantidad> Grilla_ReporteVenta_Cantidad(Request_BI_Tablero_Rep_Venta oRq)
        {
            Response_BI_Tablero_Rep_Venta_Cantidad oRp;

            oRp = MvcApplication._Deserialize<Response_BI_Tablero_Rep_Venta_Cantidad>(MvcApplication._Servicio_Operativa.BL_BI_Tablero_Rep_Venta_Cantidad(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
    }

    #region <<< Request - Response >>>

    public class Request_BI_Tablero_Rep_Venta
    {
        [JsonProperty("_a")]
        public string oEquipo { get; set; }
        [JsonProperty("_b")]
        public int oAnio { get; set; }
        [JsonProperty("_c")]
        public int oMes { get; set; }
        [JsonProperty("_d")]
        public int oPeriodo { get; set; }
        [JsonProperty("_e")]
        public int oCluster { get; set; }
        [JsonProperty("_f")]
        public int oTipoNodo { get; set; }
        [JsonProperty("_g")]
        public int oOficina { get; set; }
        [JsonProperty("_h")]
        public string oCategoria { get; set; }
        [JsonProperty("_i")]
        public int oOpcion { get; set; }
        [JsonProperty("_j")]
        public string oParametros { get; set; }
        [JsonProperty("_k")]
        public int oUnidad { get; set; }
        [JsonProperty("_l")]
        public int oEspejo { get; set; }
    }
    public class Response_BI_Tablero_Rep_Venta
    {
        [JsonProperty("a")]
        public Tablero_Grilla_Venta Response { get; set; }
    }
    public class Response_BI_Tablero_Rep_Venta_Cantidad
    {
        [JsonProperty("a")]
        public List<Tablero_Grilla_Venta_cantidad> Response { get; set; }
    }
    #endregion
}