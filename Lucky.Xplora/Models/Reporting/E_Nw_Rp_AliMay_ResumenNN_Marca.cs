﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{
    public class E_Nw_Rp_AliMay_ResumenNN_Marca : AE_Nw_Rp_AliMay_ResumenNN_Marca
    {
        [JsonProperty("a")]
        public string Cod_Marca { get; set; }
        [JsonProperty("b")]
        public string Marca { get; set; }
        [JsonProperty("c")]
        public string Stock_Inicial_Ton { get; set; }
        [JsonProperty("d")]
        public string Sell_In_Ton { get; set; }
        [JsonProperty("e")]
        public string Stock_Fin_Ton { get; set; }
        [JsonProperty("f")]
        public string Prom_Ventas_Ton { get; set; }
        [JsonProperty("g")]
        public string DG { get; set; }
        [JsonProperty("h")]
        public string DG_Indicador { get; set; }
    }
    public class Response_Nw_Rp_AliMay_ResumenMarca 
    {
        [JsonProperty("a")]
        public List<E_Nw_Rp_AliMay_ResumenNN_Marca> Response { get; set; }
    }

    public abstract class AE_Nw_Rp_AliMay_ResumenNN_Marca
    {
        public List<E_Nw_Rp_AliMay_ResumenNN_Marca> Consul_ResumenNN_Marca(Request_NW_Reporting oRq)
        {
            Response_Nw_Rp_AliMay_ResumenMarca oRp;
            oRp = MvcApplication._Deserialize<Response_Nw_Rp_AliMay_ResumenMarca>(
                    MvcApplication._Servicio_Operativa.Consul_NW_RP_AliMayDG_ResumenNNMarca(
                        MvcApplication._Serialize(oRq)
                    )
                );
            return oRp.Response;
        }
    }
}