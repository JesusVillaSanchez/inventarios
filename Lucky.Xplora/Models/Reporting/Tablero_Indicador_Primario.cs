﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Reporting
{

    #region Indicador Primario

    public class Indicador_Presencia_Sku : AIndicador_Presencia_Sku
    {
        [JsonProperty("_a")]
        public string sku { get; set; }
        [JsonProperty("_b")]
        public string producto { get; set; }
        [JsonProperty("_c")]
        public string cantidad { get; set; }
        [JsonProperty("_d")]
        public string porcentaje { get; set; }
    }

    public class Indicador_Primario : AIndicador_Primario
    {

        [JsonProperty("_a")]
        public string cantidad { get; set; }
        [JsonProperty("_b")]
        public string porcentaje { get; set; }

    }
    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2015-10-14
    /// </summary>
    public class Termometros : ATermometros
    {
        [JsonProperty("_a")]
        public string id { get; set; }
        [JsonProperty("_b")]
        public string descripcion { get; set; }
        [JsonProperty("_c")]
        public string porcentaje { get; set; }
    }

    public abstract class AIndicador_Presencia_Sku {
        public List<Indicador_Presencia_Sku> Presencia_Producto(Request_Indicador_Presencia_Sku oRq)
        {
            Response_Indicador_Presencia_Sku oRp;

            oRp = MvcApplication._Deserialize<Response_Indicador_Presencia_Sku>(MvcApplication._Servicio_Operativa.BL_BI_Tablero_Menor_Pres_Sku(MvcApplication._Serialize(oRq)));

            return oRp.Response;
        }
    }

    public abstract class AIndicador_Primario {
        public List<Indicador_Primario> Exhibicion_Ventana(Request_Indicador_Presencia_Sku oRq)
        {
            Response_Indicador oRp;
            oRp = MvcApplication._Deserialize<Response_Indicador>(MvcApplication._Servicio_Operativa.BL_BI_Tablero_Menor_Exhib_Ventana(MvcApplication._Serialize(oRq)));
            return oRp.Response;
        }

        public List<Indicador_Primario> Elemento_Visibilidad(Request_Indicador_Presencia_Sku oRq)
        {
            Response_Indicador oRp;
            oRp = MvcApplication._Deserialize<Response_Indicador>(MvcApplication._Servicio_Operativa.BL_BI_Tablero_Menor_Elem_Visibilidad(MvcApplication._Serialize(oRq)));
            return oRp.Response;
        }

    }

    public abstract class ATermometros
    {
        /// <summary>
        /// Autor: curiarte
        /// Fecha: 2015-10-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Termometros> TermometroSku(Request_GetTermometro_Sku oRq)
        {
            Response_GetTermometro_Sku oRp;
            oRp = MvcApplication._Deserialize<Response_GetTermometro_Sku>(MvcApplication._Servicio_Operativa.BL_BI_TRM_SKU(MvcApplication._Serialize(oRq)));
            return oRp.listasku;
        }
        public List<Termometros> TermometroElmvisb(Request_GetTermometro_Elmvisb oRq)
        {
            Response_GetTermometro_Elmvisb oRp;
            oRp = MvcApplication._Deserialize<Response_GetTermometro_Elmvisb>(MvcApplication._Servicio_Operativa.BL_BI_TRM_VISB(MvcApplication._Serialize(oRq)));
            return oRp.listadoelmvisb;
        }
        public List<Termometros> TermometroVentana(Request_GetTermometro_ventana oRq)
        {
            Response_GetTermometro_ventana oRp;
            oRp = MvcApplication._Deserialize<Response_GetTermometro_ventana>(MvcApplication._Servicio_Operativa.BL_BI_TRM_VENTANA(MvcApplication._Serialize(oRq)));
            return oRp.listadovtn;
        }

    }

    #endregion

    #region Indicador Secundario

    public class E_Tablero_Minorista_Indi_Secun
    {
        //[JsonProperty("_a")]
        //public List<E_Tablero_Menor_Precio> oListaPrecio { get; set; }
        //[JsonProperty("_b")]
        //public List<E_Tablero_Menor_SOV> oListaSOV { get; set; }
        //[JsonProperty("_c")]
        //public List<E_Tablero_Menor_SOVV> oListaSOVV { get; set; }

        [JsonProperty("_a")]
        public decimal oListaPrecio { get; set; }
        [JsonProperty("_b")]
        public decimal oListaSOV { get; set; }
        [JsonProperty("_c")]
        public decimal oListaSOVV { get; set; }
    }

    //public class E_Tablero_Menor_Precio
    //{
    //    [JsonProperty("_a")]
    //    public string cantidad { get; set; }
    //    [JsonProperty("_b")]
    //    public string porcentaje { get; set; }
    //}

    //public class E_Tablero_Menor_SOV
    //{
    //    [JsonProperty("_a")]
    //    public string alicorp { get; set; }
    //    [JsonProperty("_b")]
    //    public string Competencia { get; set; }
    //}

    //public class E_Tablero_Menor_SOVV
    //{
    //    [JsonProperty("_a")]
    //    public string sov { get; set; }
    //    [JsonProperty("_b")]
    //    public string Competencia { get; set; }
    //}

    //public abstract class AE_Tablero_Minorista_Indi_Secun {
    //    public E_Tablero_Minorista_Indi_Secun Indicador_Secundario(Request_Indicador_Presencia_Sku oRq)
    //    {
    //        Response_BI_Tablero_Indicador_Secundario oRp;
    //        oRp = MvcApplication._Deserialize<Response_BI_Tablero_Indicador_Secundario>(MvcApplication._Servicio_Operativa.BL_BI_Tablero_Idicador_Secundario(MvcApplication._Serialize(oRq)));
    //        return oRp.Response;
    //    }
    //}

    #endregion

    #region <<< Request - Response >>>

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-14
    /// </summary>
    public class E_Tablero_Indicadores_Request
    {
        [JsonProperty("_a")]
        public int cod_oficina { get; set; }
        [JsonProperty("_b")]
        public int cod_cluster { get; set; }
        [JsonProperty("_c")]
        public int cod_tipo_nodo { get; set; }
        [JsonProperty("_d")]
        public string cod_categoria { get; set; }
        [JsonProperty("_e")]
        public int cod_marca { get; set; }
        [JsonProperty("_f")]
        public int cod_tipo_canal { get; set; }
        [JsonProperty("_g")]
        public string cod_sku { get; set; }
        [JsonProperty("_h")]
        public int cod_mes { get; set; }
        [JsonProperty("_i")]
        public int cod_periodo { get; set; }
        [JsonProperty("_j")]
        public string cod_pop { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-14
    /// </summary>
    public class Request_Indicador_Presencia_Sku
    {
        [JsonProperty("_a")]
        public E_Tablero_Indicadores_Request oParametros { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-14
    /// </summary>
    public class Response_Indicador_Presencia_Sku
    {
        [JsonProperty("_a")]
        public List<Indicador_Presencia_Sku> Response { get; set; }
    }

    /// <summary>
    /// Autor: yrodriguezs
    /// Fecha: 2015-09-15
    /// </summary>
    public class Response_Indicador
    {
        [JsonProperty("_a")]
        public List<Indicador_Primario> Response { get; set; }
    }

    public class Response_BI_Tablero_Indicador_Secundario
    {
        [JsonProperty("a")]
        public E_Tablero_Minorista_Indi_Secun Response { get; set; }
    }

    /// <summary>
    /// Autor: curiarte
    /// Fecha: 2015-10-14
    /// </summary>
    public class Request_GetTermometro_Sku
    {
        [JsonProperty("_a")]
        public int cod_oficina { get; set; }
        [JsonProperty("_b")]
        public int cod_cluster { get; set; }
        [JsonProperty("_c")]
        public int cod_tipo_nodo { get; set; }
        [JsonProperty("_d")]
        public string cod_categoria { get; set; }
        [JsonProperty("_e")]
        public int cod_marca { get; set; }
        [JsonProperty("_f")]
        public int cod_tipo_canal { get; set; }
        [JsonProperty("_g")]
        public int cod_mes { get; set; }
        [JsonProperty("_h")]
        public int cod_periodo { get; set; }
    }
    public class Request_GetTermometro_Elmvisb
    {
        [JsonProperty("_a")]
        public int cod_oficina { get; set; }
        [JsonProperty("_b")]
        public int cod_cluster { get; set; }
        [JsonProperty("_c")]
        public int cod_tipo_nodo { get; set; }
        [JsonProperty("_d")]
        public string cod_categoria { get; set; }
        [JsonProperty("_e")]
        public int cod_tipo_canal { get; set; }
        [JsonProperty("_f")]
        public int cod_mes { get; set; }
        [JsonProperty("_g")]
        public int cod_periodo { get; set; }
    }
    public class Request_GetTermometro_ventana
    {
        [JsonProperty("_a")]
        public int cod_oficina { get; set; }
        [JsonProperty("_b")]
        public int cod_cluster { get; set; }
        [JsonProperty("_c")]
        public int cod_tipo_nodo { get; set; }
        [JsonProperty("_d")]
        public string cod_categoria { get; set; }
        [JsonProperty("_e")]
        public int cod_tipo_canal { get; set; }
        [JsonProperty("_f")]
        public int cod_mes { get; set; }
        [JsonProperty("_g")]
        public int cod_periodo { get; set; }
    }

    public class Response_GetTermometro_Sku
    {
        [JsonProperty("_a")]
        public List<Termometros> listasku { get; set; }
    }
    public class Response_GetTermometro_Elmvisb
    {
        [JsonProperty("_a")]
        public List<Termometros> listadoelmvisb { get; set; }
    }
    public class Response_GetTermometro_ventana
    {
        [JsonProperty("_a")]
        public List<Termometros> listadovtn { get; set; }
    }

    #endregion

}