﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.Xplora.Models.Encuesta
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>
    public class Encuesta : AEncuesta
    {
        [JsonProperty("_a")]
        public int enc_id { get; set; }

        [JsonProperty("_b")]
        public string enc_descripcion { get; set; }

        [JsonProperty("_c")]
        public DateTime enc_fecha_inicio { get; set; }

        [JsonProperty("_d")]
        public DateTime enc_fecha_fin { get; set; }

        [JsonProperty("_e")]
        public int epr_cantidad { get; set; }

        [JsonProperty("_f")]
        public int ere_cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>
    public class Supervisor_Pregunta
    {
        [JsonProperty("_a")]
        public Supervisor supervisor { get; set; }

        [JsonProperty("_b")]
        public List<Encuesta_Pregunta> pregunta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>
    public class Supervisor
    {
        [JsonProperty("_a")]
        public int per_id { get; set; }

        [JsonProperty("_b")]
        public string per_nombre { get; set; }

        [JsonProperty("_c")]
        public string per_email { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>
    public class Encuesta_Pregunta
    {
        [JsonProperty("_a")]
        public int epr_id { get; set; }

        [JsonProperty("_b")]
        public string epr_pregunta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>
    public class Encuesta_Respuesta
    {
        [JsonProperty("_a")]
        public int ere_de { get; set; }

        [JsonProperty("_b")]
        public int ere_para { get; set; }

        [JsonProperty("_c")]
        public int enc_id { get; set; }

        [JsonProperty("_d")]
        public int epr_id { get; set; }

        [JsonProperty("_e")]
        public int ere_valor { get; set; }

        [JsonProperty("_f")]
        public DateTime ere_fecha { get; set; }

        [JsonProperty("_g")]
        public int ere_estado { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>
    public class Encuesta_Puntaje
    {
        [JsonProperty("_a")]
        public decimal puntaje_final { get; set; }

        [JsonProperty("_b")]
        public List<Encuesta_Pregunta_Puntaje> pregunta { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>
    public class Encuesta_Pregunta_Puntaje
    {
        [JsonProperty("_a")]
        public int epr_id { get; set; }

        [JsonProperty("_b")]
        public string epr_pregunta { get; set; }

        [JsonProperty("_c")]
        public decimal ere_valor { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>
    public abstract class AEncuesta
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Encuesta> Encuesta(Request_Encuesta oRq)
        {
            Response_Encuesta oRp = MvcApplication._Deserialize<Response_Encuesta>(
                MvcApplication._Servicio_Operativa.Encuesta(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Lista;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Supervisor_Pregunta Supervisor_Pregunta(Request_Encuesta oRq)
        {
            Response_Supervisor_Pregunta oRp = MvcApplication._Deserialize<Response_Supervisor_Pregunta>(
                MvcApplication._Servicio_Operativa.Encuesta(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Objeto;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public int Graba(Request_Encuesta_Graba oRq)
        {
            Response_Encuesta_Graba oRp = MvcApplication._Deserialize<Response_Encuesta_Graba>(
                MvcApplication._Servicio_Operativa.Encuesta_Graba(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.cantidad;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-14
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public Encuesta_Puntaje Puntaje(Request_Encuesta oRq)
        {
            Response_Encuesta_Puntaje oRp = MvcApplication._Deserialize<Response_Encuesta_Puntaje>(
                MvcApplication._Servicio_Operativa.Encuesta(
                    MvcApplication._Serialize(oRq)
                )
            );

            return oRp.Objeto;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>k8
    public class Request_Encuesta
    {
        [JsonProperty("_a")]
        public int persona { get; set; }

        [JsonProperty("_b")]
        public int encuesta { get; set; }

        [JsonProperty("_c")]
        public int opcion { get; set; }
    }
    public class Request_Encuesta_Graba
    {
        [JsonProperty("_a")]
        public List<Encuesta_Respuesta> Lista { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-09-14
    /// </summary>
    public class Response_Encuesta
    {
        [JsonProperty("_a")]
        public List<Encuesta> Lista { get; set; }
    }
    public class Response_Supervisor_Pregunta
    {
        [JsonProperty("_a")]
        public Supervisor_Pregunta Objeto { get; set; }
    }
    public class Response_Encuesta_Graba
    {
        [JsonProperty("_a")]
        public int cantidad { get; set; }
    }
    public class Response_Encuesta_Puntaje
    {
        [JsonProperty("_a")]
        public Encuesta_Puntaje Objeto { get; set; }
    }
    #endregion
}
