﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Security
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        
        
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary {
                { "action", "Unauthorized" },
                { "controller", "Error" }
            });
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool estado = false;
            string Email = httpContext.User.Identity.Name;

            var rd = httpContext.Request.RequestContext.RouteData;
            string currentAction = rd.GetRequiredString("action");
            string currentController = rd.GetRequiredString("controller");

            if (httpContext.Session != null) {
                Persona objPer = ((Persona)httpContext.Session["Session_Login"]);

                foreach (var item in objPer.Accesos )
                {
                    if (currentController == item.Url_Proyecto)
                    {
                        foreach (var xitem in item.Hijo)
                        {
                            foreach (var xxitem in xitem.Hijo)
                            {
                                if (currentAction == xxitem.Url_vista)
                                {
                                    estado = true;
                                }
                            }
                        }
                    }
                }

                //if (estado == false)
                //{
                //    httpContext.Session.RemoveAll();
                //}
            }
           
            return estado;
        }
    }
}