﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using Lucky.Xplora.Models;

namespace Lucky.Xplora.Security
{
    public class CustomIdentity : IIdentity
    {
        public IIdentity Identity { get; set; }
        public Persona Persona { get; set; }

        public CustomIdentity(Persona persona)
        {
            Identity = new GenericIdentity(persona.Person_Email);
            Persona = persona;
        }

        public string AuthenticationType
        {
            get { return Identity.AuthenticationType; }
        }

        public bool IsAuthenticated
        {
            get { return Identity.IsAuthenticated; }
        }

        public string Name
        {
            get { return Identity.Name; }
        }
    }
}