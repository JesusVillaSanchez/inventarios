﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.ColgateReporting;
using Lucky.Xplora.Models.Administrador;
using Lucky.Xplora.Models.Module;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using Lucky.Xplora.Security;
//using Excel1 = Microsoft.Office.Interop.Excel;
using System.Configuration;


namespace Lucky.Xplora.Controllers
{
    public class ModulosReportingController : Controller
    {
        //
        // GET: /ModulosReporting/
        //private static Persona _Persona;

        [CustomAuthorize]
        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            if (Request["_a"] != null)
            {
                Session["Session_Campania"] = Request["_a"];
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();

            }
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        [HttpPost]
        public ActionResult _GetMMes(int anio)
        {
            return PartialView(
                "../BackusReporting/GetMes",
                new Lucky.Xplora.Models.Mes().Lista(
                    new M_Request_Mes_Por_Planning_Reports_Anio()
                    {
                        id_planning = Convert.ToString(Session["Session_Campania"]),
                        id_report = 58,
                        anio = anio
                    }
                ));
        }

        [HttpPost]
        public ActionResult _GetMPeriodo(int anio, int mes)
        {
            return PartialView(
                "../ColgateReporting/GetPeriodo",
                new Periodo().Lista(
                    new Periodo_Por_Planning_Reports_Anio_Mes_Request()
                    {
                        campania = Convert.ToString(Session["Session_Campania"]),
                        reporte = 58,
                        anio = anio,
                        mes = mes
                    }
                ));
        }

        [HttpPost]
        public ActionResult GetPrecioSugerido(int __a)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Module_PrecSugerido_Colgate().Lista(new Request_Precio_Sugerido()
                    {
                        periodo= __a
                    })
                ),
                ContentType = "application/json"
            };
        }

        [HttpPost]
        public JsonResult GetDescarga(string campania, int reporte, int periodo)
        {
            string _fileFormatServer = "";
            string _fileServer = "";
            string _filePath = "";
            string _password = "";
            int _fila = 0;

            string UbicacionXplora = Convert.ToString(ConfigurationManager.AppSettings["xplora"]);

            _password = "maldito_quien_te_dio_la_contraseña!!!";
            _fileServer = String.Format("sku_precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            //_fileFormatServer = System.IO.Path.Combine(Server.MapPath("/Format"), "Lucky-Modulo-Precio-SKU.xlsx");
            _fileFormatServer = System.IO.Path.Combine(UbicacionXplora + "/Format/", "Lucky-Modulo-Precio-SKU.xlsx");
            //_filePath = Path.Combine(Server.MapPath("/Temp"), _fileServer);
            _filePath = Path.Combine(UbicacionXplora + "/Temp/", _fileServer);
            
            /*copia formato y lo ubica en carpeta temporal*/
            System.IO.File.Copy(_fileFormatServer, _filePath, true);

            FileInfo _fileNew = new FileInfo(_filePath);
            //if (_fileNew.Exists)
            //{
            //    _fileNew.Delete();
            //    _fileNew = new FileInfo(_filePath);
            //}

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                foreach (Excel.ExcelWorksheet oWsPrecio in oEx.Workbook.Worksheets)
                {
                    if (oWsPrecio.Name == "SKUPrecio")
                    {
                        #region <<< Instancias >>>

                        #region << Reporte Precio >>

                        List<Module_PrecSugerido_Col_Descarga> oPrecio = new Module_PrecSugerido_Col_Descarga().Lista(
                            new Request_Precio_Sugerido_Descarga()
                            {
                                id_planning = Convert.ToString(Session["Session_Campania"]),
                                report_id = reporte,
                                id_reportsplanning = periodo
                            }
                        );

                        #endregion

                        #endregion

                        #region <<< Reporte Precios >>>
                        if ((oPrecio != null) || (oPrecio.Count > 0))
                        {
                            //Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Precio");

                            oWsPrecio.Cells[1, 1].Value = "Periodo";
                            oWsPrecio.Cells[1, 2].Value = "Cod_Empresa";
                            oWsPrecio.Column(2).Hidden = true;
                            oWsPrecio.Cells[1, 3].Value = "Empresa";
                            oWsPrecio.Cells[1, 4].Value = "Cod_Categoria";
                            oWsPrecio.Column(4).Hidden = true;
                            oWsPrecio.Cells[1, 5].Value = "Categoria";
                            oWsPrecio.Cells[1, 6].Value = "Cod_Marca";
                            oWsPrecio.Column(6).Hidden = true;
                            oWsPrecio.Cells[1, 7].Value = "Marca";
                            oWsPrecio.Cells[1, 8].Value = "Cod_Producto";
                            oWsPrecio.Column(8).Hidden = true;
                            oWsPrecio.Cells[1, 9].Value = "Cod_Sku";
                            oWsPrecio.Cells[1, 10].Value = "Nombre_Sku";
                            oWsPrecio.Cells[1, 11].Value = "Sku_Mandatorio";
                            oWsPrecio.Cells[1, 12].Value = "Precio Sugerido";
                            oWsPrecio.Cells[2, 13].Value = _fileServer;
                            oWsPrecio.Column(13).Hidden = true;

                            _fila = 2;
                            foreach (Module_PrecSugerido_Col_Descarga oBj in oPrecio)
                            {
                                oWsPrecio.Cells[_fila, 1].Value = periodo;
                                oWsPrecio.Cells[_fila, 1].Style.Locked = true;
                                oWsPrecio.Cells[_fila, 2].Value = oBj.company_id;
                                oWsPrecio.Cells[_fila, 2].Style.Locked = true;
                                oWsPrecio.Cells[_fila, 3].Value = oBj.Company_Name;
                                oWsPrecio.Cells[_fila, 3].Style.Locked = true;
                                oWsPrecio.Cells[_fila, 4].Value = oBj.id_ProductCategory;
                                oWsPrecio.Cells[_fila, 4].Style.Locked = true;
                                oWsPrecio.Cells[_fila, 5].Value = oBj.Product_Category;
                                oWsPrecio.Cells[_fila, 5].Style.Locked = true;
                                oWsPrecio.Cells[_fila, 6].Value = oBj.id_Brand;
                                oWsPrecio.Cells[_fila, 6].Style.Locked = true;
                                oWsPrecio.Cells[_fila, 7].Value = oBj.Name_Brand;
                                oWsPrecio.Cells[_fila, 7].Style.Locked = true;
                                oWsPrecio.Cells[_fila, 8].Value = oBj.id_Product;
                                oWsPrecio.Cells[_fila, 8].Style.Locked = true;
                                oWsPrecio.Cells[_fila, 9].Value = oBj.cod_Product;
                                oWsPrecio.Cells[_fila, 9].Style.Locked = true;
                                oWsPrecio.Cells[_fila, 10].Value = oBj.Product_Name;
                                oWsPrecio.Cells[_fila, 10].Style.Locked = true;

                                if (oBj.Sku_Mandatorio.Equals("True"))
                                {
                                    oWsPrecio.Cells[_fila, 11].Value = "1";
                                }
                                else if (oBj.Sku_Mandatorio.Equals("False"))
                                {
                                    oWsPrecio.Cells[_fila, 11].Value = "0";
                                }
                                else
                                {
                                    oWsPrecio.Cells[_fila, 11].Value = "0";
                                }
                                //if (oBj.Sku_Mandatorio.Equals("False"))
                                //{
                                //    oWsPrecio.Cells[_fila, 11].Value = "0";
                                //}

                                oWsPrecio.Cells[_fila, 12].Value = oBj.precio_sugerido;

                                oWsPrecio.Cells[_fila, 12].Style.Locked = false;
                                oWsPrecio.Cells[_fila, 13].Style.Locked = true;
                                _fila++;

                            }

                            //Formato Cabecera
                            oWsPrecio.SelectedRange[1, 1, 1, 12].AutoFilter = true;
                            oWsPrecio.Row(1).Height = 25;
                            oWsPrecio.Row(1).Style.Font.Bold = true;
                            oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            //Columnas auto ajustadas
                            oWsPrecio.Column(1).Width = 15;
                            oWsPrecio.Column(3).Width = 40;
                            //oWsPrecio.Column(3).AutoFit();
                            oWsPrecio.Column(5).Width = 40;
                            oWsPrecio.Column(7).Width = 30;
                            oWsPrecio.Column(9).Width = 20;
                            oWsPrecio.Column(10).Width = 15;
                            oWsPrecio.Column(10).Width = 50;
                            oWsPrecio.Column(11).Width = 15;
                            oWsPrecio.Column(12).Width = 20;

                            oWsPrecio.View.FreezePanes(2, 1);

                            oWsPrecio.Protection.AllowInsertRows = false;
                            oWsPrecio.Protection.AllowInsertColumns = false;
                            oWsPrecio.Protection.AllowDeleteColumns = false;
                            oWsPrecio.Protection.AllowPivotTables = false;
                            oWsPrecio.Protection.AllowSelectLockedCells = true;
                            oWsPrecio.Protection.AllowSelectUnlockedCells = true;
                            oWsPrecio.Protection.AllowAutoFilter = true;
                            oWsPrecio.Protection.AllowSort = false;
                            oWsPrecio.Protection.IsProtected = true;
                            oWsPrecio.Protection.SetPassword(_password);
                        }
                        #endregion
                    }
                }
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult GetCarga(HttpPostedFileBase archivo)
        {
            bool bBucle;
            int iFila;
            string sPath = "";
            string sNombreCliente = "";
            string sNombreServidor = "";
            string _fileFormatServer = "";

            if (archivo != null && archivo.ContentLength > 0)
            {
                sNombreCliente = Path.GetFileName(archivo.FileName);
                sNombreServidor = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _fileFormatServer = Path.Combine(Server.MapPath("/Format"), "Lucky-Modulo-Precio-SKU.xlsx");


                sPath = Path.Combine(Server.MapPath("/Temp"), sNombreServidor);

                /*copia formato y lo ubica en carpeta temporal*/
                System.IO.File.Copy(_fileFormatServer, sPath, true);

                archivo.SaveAs(sPath);

                FileInfo oFile = new FileInfo(sPath);

                List<Module_PrecSugerido_Colgate> oPrecio = new List<Module_PrecSugerido_Colgate>();

                List<Module_Response_PrecSugerido_Col> oListaResponse = new List<Module_Response_PrecSugerido_Col>();
                List<Module_Response_PrecSugerido_Col> oPrecSug = null;
                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(oFile))
                {
                    foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                    {
                        #region <<< Precio >>>
                        if (oWs.Name == "SKUPrecio")
                        {
                            iFila = 2;
                            bBucle = false;

                            while (bBucle == false)
                            {
                                if (oWs.Cells[iFila, 1].Text.Length == 0)
                                {
                                    bBucle = true;
                                }
                                else
                                {
                                    if (oWs.Cells[iFila, 12].Text.Length > 0)
                                    {
                                        Module_PrecSugerido_Colgate oBj = new Module_PrecSugerido_Colgate();
                                        oBj.id_Planning = Convert.ToString(Session["Session_Campania"]);
                                        oBj.Company_id = Convert.ToInt32(oWs.Cells[iFila, 2].Text);
                                        oBj.id_ReportsPlanning = Convert.ToInt32(oWs.Cells[iFila, 1].Text);
                                        oBj.cod_Product = Convert.ToString(oWs.Cells[iFila, 9].Text);
                                        oBj.Sku_Mandatario = Convert.ToString(oWs.Cells[iFila, 11].Text);

                                        //try
                                        //{
                                            oBj.P_Sugerido = Convert.ToString(oWs.Cells[iFila, 12].Text);
                                        //}
                                        //catch (Exception e)
                                        //{
                                        //    Console.WriteLine(e.Message);
                                        //    return RedirectToAction("GetCargaSKUsMandatorios", "ModulosReporting");
                                        //}

                                        oBj.createby = ((Persona)Session["Session_Login"]).name_user.ToString();
                                        oBj.archivo = sNombreServidor;
                                        oPrecio.Add(oBj);
                                    }
                                }

                                iFila++;
                            }

                            //oListaResponse.Add(new Module_PrecSugerido_Colgate().BulkCopyPrecSugCol(new Request_BulkCopy_Module_PrecSug_Col() { Lista = oPrecio }));

                            oPrecSug = new Module_Response_PrecSugerido_Col().BulkCopyPrecSugCol(
                                new Request_BulkCopy_Module_PrecSug_Col()
                                {
                                    Lista = oPrecio
                                }
                            );

                        }
                        #endregion
                    }
                }
            }
            else {
                return RedirectToAction("Index", "ModulosReporting");
            }
            //return Json(new { Archivo = oPrecSug });
            return RedirectToAction("Index", "ModulosReporting");

        }

        [HttpPost]
        public ActionResult GetRangosSkuMandatorios(int numrango)
        {
            List<Codigo_Module_Range_Sku_Mandatory> oRangos = new Codigo_Module_Range_Sku_Mandatory().Lista(
                    new Request_Codigo_Rango_Sku()
                    {
                        numrango = numrango
                    }
                );
            return Json(new { Archivo = oRangos });
        }


        [HttpPost]
        public ActionResult GetInsertRangosSkuMandatorios(int id_reportsPlanning, string cod_range, int Mandatorio_RangeDesde, int Mandatorio_RangeHasta)
        {
            int oRangos = new Response_Insert_Module_RangoSku_Madatorio().Lista(
                    new Request_Insert_Module_RangoSku_Madatorio()
                    {
                        id_reportsPlanning = id_reportsPlanning,
                        cod_range = cod_range,
                        Mandatorio_RangeDesde = Mandatorio_RangeDesde,
                        Mandatorio_RangeHasta = Mandatorio_RangeHasta
                    }
                );
            return Json(new { Archivo = oRangos });
        }

        [HttpPost]
        public ActionResult _GetMGrafico(int cod_canal)
        {
            return View(
                new E_Grafico().Lista(
                    new Consulta_Graficos_Request()
                    {
                        cod_canal = cod_canal
                    }
                )
            );
        }

        [HttpPost]
        public ActionResult GetListRangosMandatorios(int __a)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new E_Rango_Mandatorio().Lista(new Request_Rango_Mandatorio()
                    {
                        cod_periodo = __a
                    })
                ),
                ContentType = "application/json"
            };
        }


        [HttpPost]
        public ActionResult GetReportes(string campania, int anio, int mes)
        {
            return View(
                new M_Reporte().Lista(
                    new Request_Listar_Reporte_Por_CodCampaniaAnioMes()
                    {
                        CodCampania = campania,
                        anio = anio,
                        mes = mes
                    }
                )
            );
        }

        #region << Modulo 2 >>

        [HttpPost]
        public ActionResult GetSKUMandatory(int __a)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new M_SKUMandatory().Lista(new Request_SKUMandatory_Descarga()
                    {
                        periodo = __a
                    })
                ),
                ContentType = "application/json"
            };
        }

        [HttpPost]
        public JsonResult GetDescargaSKUMandatory(string campania, int reporte, int periodo)
        {
            string _fileServer = "";
            string _filePath = "";

            string _fileFormatServer = "";

            string _password = "";
            int _fila = 0;

            string UbicacionXplora = Convert.ToString(ConfigurationManager.AppSettings["xplora"]);

            _password = "maldito_quien_te_dio_la_contraseña!!!";
            _fileServer = String.Format("sku_mandatorio_"+"{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            //_fileFormatServer = System.IO.Path.Combine(Server.MapPath("/Format"), "Lucky-Modulo-SKU-Mandatorio.xlsx");
            _fileFormatServer = System.IO.Path.Combine(UbicacionXplora + "/Format/", "Lucky-Modulo-SKU-Mandatorio.xlsx");
            //_filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);
            _filePath = Path.Combine(UbicacionXplora + "/Temp/", _fileServer);
            /*copia formato y lo ubica en carpeta temporal*/
            System.IO.File.Copy(_fileFormatServer, _filePath, true);

            FileInfo _fileNew = new FileInfo(_filePath);
            //if (_fileNew.Exists)
            //{
            //    _fileNew.Delete();
            //    _fileNew = new FileInfo(_filePath);
            //}

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                foreach (Excel.ExcelWorksheet oWsSKUMandatory in oEx.Workbook.Worksheets)
                {
                    if (oWsSKUMandatory.Name == "SKUMandatory")
                    {
                        #region <<< Instancias >>>

                        #region << Reporte Precio >>

                        List<Module_PrecSugerido_Col_Descarga> oPrecio = new Module_PrecSugerido_Col_Descarga().Lista(
                            new Request_Precio_Sugerido_Descarga()
                            {
                                id_planning = Convert.ToString(Session["Session_Campania"]),
                                report_id = reporte,
                                id_reportsplanning = periodo

                            }
                        );

                        #endregion

                        #endregion

                        #region <<< Reporte Precios >>>
                        if ((oPrecio != null) || (oPrecio.Count > 0))
                        {
                            //Excel.ExcelWorksheet oWsSKUMandatory = oEx.Workbook.Worksheets.Add("SKUMandatory");

                            oWsSKUMandatory.Cells[1, 1].Value = "PERIODO";
                            oWsSKUMandatory.Cells[1, 2].Value = "COD_EMPRESA";
                            oWsSKUMandatory.Column(2).Hidden = true;
                            oWsSKUMandatory.Cells[1, 3].Value = "EMPRESA";
                            oWsSKUMandatory.Cells[1, 4].Value = "COD_CATEGORIA";
                            oWsSKUMandatory.Column(4).Hidden = true;
                            oWsSKUMandatory.Cells[1, 5].Value = "CATEGORIA";
                            oWsSKUMandatory.Cells[1, 6].Value = "COD_MARCA";
                            oWsSKUMandatory.Column(6).Hidden = true;
                            oWsSKUMandatory.Cells[1, 7].Value = "MARCA";
                            oWsSKUMandatory.Cells[1, 8].Value = "ID_SKU";
                            oWsSKUMandatory.Cells[1, 9].Value = "COD_SKU";
                            oWsSKUMandatory.Cells[1, 10].Value = "NOMBRE_SKU";
                            oWsSKUMandatory.Cells[1, 11].Value = "Sku_Mandatorio";
                            //oWsSKUMandatory.Cells[1, 12].Value = _fileServer;
                            //oWsSKUMandatory.Column(12).Hidden = false;

                            _fila = 2;
                            foreach (Module_PrecSugerido_Col_Descarga oBj in oPrecio)
                            {
                                oWsSKUMandatory.Cells[_fila, 1].Value = periodo;
                                oWsSKUMandatory.Cells[_fila, 1].Style.Locked = true;
                                oWsSKUMandatory.Cells[_fila, 2].Value = oBj.company_id;
                                oWsSKUMandatory.Cells[_fila, 2].Style.Locked = true;
                                oWsSKUMandatory.Cells[_fila, 3].Value = oBj.Company_Name;
                                oWsSKUMandatory.Cells[_fila, 3].Style.Locked = true;
                                oWsSKUMandatory.Cells[_fila, 4].Value = oBj.id_ProductCategory;
                                oWsSKUMandatory.Cells[_fila, 4].Style.Locked = true;
                                oWsSKUMandatory.Cells[_fila, 5].Value = oBj.Product_Category;
                                oWsSKUMandatory.Cells[_fila, 5].Style.Locked = true;
                                oWsSKUMandatory.Cells[_fila, 6].Value = oBj.id_Brand;
                                oWsSKUMandatory.Cells[_fila, 6].Style.Locked = true;
                                oWsSKUMandatory.Cells[_fila, 7].Value = oBj.Name_Brand;
                                oWsSKUMandatory.Cells[_fila, 7].Style.Locked = true;
                                oWsSKUMandatory.Cells[_fila, 8].Value = oBj.id_Product;
                                oWsSKUMandatory.Cells[_fila, 8].Style.Locked = true;
                                oWsSKUMandatory.Cells[_fila, 9].Value = oBj.cod_Product;
                                oWsSKUMandatory.Cells[_fila, 9].Style.Locked = true;
                                oWsSKUMandatory.Cells[_fila, 10].Value = oBj.Product_Name;
                                oWsSKUMandatory.Cells[_fila, 10].Style.Locked = true;
                                if (oBj.Sku_Mandatorio.Equals("True"))
                                {
                                    oWsSKUMandatory.Cells[_fila, 11].Value = "1";
                                }
                                if (oBj.Sku_Mandatorio.Equals("False"))
                                {
                                    oWsSKUMandatory.Cells[_fila, 11].Value = "0";
                                }
                                oWsSKUMandatory.Cells[_fila, 11].Style.Locked = false;

                                _fila++;

                            }

                            //Formato Cabecera
                            //Formato Cabecera
                            oWsSKUMandatory.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                            oWsSKUMandatory.Row(1).Height = 25;
                            oWsSKUMandatory.Row(1).Style.Font.Bold = true;
                            oWsSKUMandatory.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWsSKUMandatory.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            //Columnas auto ajustadas
                            oWsSKUMandatory.Column(1).Width = 10;
                            oWsSKUMandatory.Column(3).Width = 35;
                            //oWsPrecio.Column(3).AutoFit();
                            oWsSKUMandatory.Column(5).Width = 30;
                            oWsSKUMandatory.Column(7).Width = 20;
                            oWsSKUMandatory.Column(8).Width = 10;
                            oWsSKUMandatory.Column(9).Width = 20;
                            oWsSKUMandatory.Column(10).Width = 15;
                            oWsSKUMandatory.Column(10).Width = 40;
                            oWsSKUMandatory.Column(11).Width = 20;
                            oWsSKUMandatory.Column(12).Width = 20;
                            //oWsSKUMandatory.Column(11).Style.Numberformat.NumFmtID = 5;
                            //"Integer"


                            oWsSKUMandatory.View.FreezePanes(2, 1);

                            oWsSKUMandatory.Protection.AllowInsertRows = false;
                            oWsSKUMandatory.Protection.AllowInsertColumns = false;
                            oWsSKUMandatory.Protection.AllowDeleteColumns = false;
                            oWsSKUMandatory.Protection.AllowPivotTables = false;
                            oWsSKUMandatory.Protection.AllowSelectLockedCells = true;
                            oWsSKUMandatory.Protection.AllowSelectUnlockedCells = true;
                            oWsSKUMandatory.Protection.AllowAutoFilter = true;
                            oWsSKUMandatory.Protection.AllowSort = false;
                            oWsSKUMandatory.Protection.IsProtected = true;
                            oWsSKUMandatory.Protection.SetPassword(_password);
                        }
                        #endregion
                    }
                }
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpGet]
        public ActionResult GetCargaSKUsMandatorios(string __a, string __b)
        {
            ViewBag.Campania = __a;
            ViewBag.Periodo = __b;

            return View();
        }


        [HttpPost]
        public ActionResult GetCargaSKUsMandatorios(HttpPostedFileBase archivoSkuMandatorios)
        {
            bool bBucle;
            int iFila;
            string sPath = "";
            string sNombreCliente = "";
            string sNombreServidor = "";

            if (archivoSkuMandatorios != null && archivoSkuMandatorios.ContentLength > 0)
            {
                sNombreCliente = Path.GetFileName(archivoSkuMandatorios.FileName);
                sNombreServidor = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);

                sPath = Path.Combine(Server.MapPath("/Temp"), sNombreServidor);
                archivoSkuMandatorios.SaveAs(sPath);

                FileInfo oFile = new FileInfo(sPath);

                List<M_SKUMandatory> oSKU = new List<M_SKUMandatory>();

                List<Module_Response_SKUMandatory> oListaResponse = new List<Module_Response_SKUMandatory>();
                List<Module_Response_SKUMandatory> oMandatory = null;
                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(oFile))
                {
                    foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                    {
                        #region <<< SKUMandatory >>>
                        int periodoExcel = 0;
                        if (oWs.Name == "SKUMandatory")
                        {
                            iFila = 2;
                            bBucle = false;

                            while (bBucle == false)
                            {
                                if (oWs.Cells[iFila, 1].Text.Length == 0)
                                {
                                    bBucle = true;
                                }
                                else
                                {
                                    if (oWs.Cells[iFila, 11].Text.Length > 0)
                                    {
                                        M_SKUMandatory oBj = new M_SKUMandatory();
                                        //int v_mandatorio = 0;
                                        //v_mandatorio = Convert.ToInt32(oWs.Cells[iFila, 11].Text);
                                        //if (v_mandatorio == 1)
                                        //{
                                            periodoExcel = Convert.ToInt32(oWs.Cells[iFila, 1].Text);

                                            oBj.id_ReportsPlanning = Convert.ToInt32(oWs.Cells[iFila, 1].Text);
                                            oBj.id_Product = Convert.ToInt32(oWs.Cells[iFila, 8].Text);
                                            oBj.cod_Product = Convert.ToString(oWs.Cells[iFila, 9].Text);

                                            //try
                                            //{
                                                oBj.sku_Mandatario = Convert.ToInt32(oWs.Cells[iFila, 11].Text);
                                            //}
                                            //catch (Exception e)
                                            //{
                                            //    Console.WriteLine(e.Message);
                                            //    return RedirectToAction("Index", "ModulosReporting"); 
                                            //}


                                            oBj.createby = "dbo";
                                            oBj.archivo = sNombreServidor;
                                            oSKU.Add(oBj);
                                        //}
                                    }
                                }

                                iFila++;
                            }


                            oMandatory = new Module_Response_SKUMandatory().BulkCopySKUsMandatory(
                                new Request_BulkCopy_Module_SKUMandatory()
                                {
                                    Lista = oSKU,
                                    periodo = periodoExcel

                                }
                            );
                        }
                        #endregion
                    }
                }

                return RedirectToAction("Index", "ModulosReporting");
            }
            else
            {
                return RedirectToAction("Index", "ModulosReporting");
            }
        }


        #region ColgateModulos_Param
        public ActionResult ColgateModulos_Param()
        {
            return View();
        }
        #endregion

        #endregion

        #region << Modulo 4 >>

        [HttpPost]
        public ActionResult _GetPeriodoModule(string reporte, string anio, string mes, string namemes, string idcmb)
        {
            ViewBag.validmes = namemes;
            ViewBag.validcmb = idcmb;
            return View(
                new M_Periodo().Lista(
                    new Request_Periodo()
                    {
                        servicio = "254",
                        canal = Convert.ToString(Session["Session_Campania"]),
                        compania = Convert.ToString(((Models.Persona)Session["Session_Login"]).Company_id),
                        reporte = reporte,
                        anio = anio,
                        mes = mes
                    }
                )
            );
        }

        [HttpPost]
        public ActionResult _UpdateValAnalista(string periodoVal, string periodoInval)
        {
            int oValAnalis = new M_ValAnalista().UpdateValAnalista(
                    new Request_ValidacionAnalista()
                    {
                        periodoVal = periodoVal,
                        periodoInVal = periodoInval
                    }
            );
            return Json(new { Archivo = oValAnalis });
        }

        /// <summary>
        /// Autor:wlopez
        /// Fecha:28/09/2016
        /// </summary>
        /// <param name="periodoVal"></param>
        /// <param name="periodoInval"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _UpdateValFotografico(string periodoVal, string periodoInval)
        {
            int oValFoto = new M_ValFotografico().UpdateValFotografico(
                    new Request_ValidacionFotografico()
                    {
                        periodoVal = periodoVal,
                        periodoInVal = periodoInval
                    }
            );
            return Json(new { Archivo = oValFoto });
        }

        [HttpPost]
        public ActionResult _GetValidacionAnalistaPeriodos(string reporte, int id_Year, string id_Month)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Module_ValidacionAnalista_Periodo().Lista(new Request_ValidacionAnalista_Perido()
                    {
                        id_planning = Convert.ToString(Session["Session_Campania"]),
                        Report_Id = reporte,
                        id_Year = id_Year,
                        id_Month = id_Month
                    })
                ),
                ContentType = "application/json"
            };
        }
        /// <summary>
        /// Autor:wloepz
        /// Fecha: 27/09/2016
        /// </summary>
        /// <param name="reporte"></param>
        /// <param name="id_Year"></param>
        /// <param name="id_Month"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _GetValidacionAnalistaPeriodosfoto(string reporte, int id_Year, string id_Month)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Module_ValidacionAnalista_Periodofoto().Lista(new Request_ValidacionAnalista_Peridofoto()
                    {
                        id_planning = Convert.ToString(Session["Session_Campania"]),
                        Report_Id = reporte,
                        id_Year = id_Year,
                        id_Month = id_Month
                    })
                ),
                ContentType = "application/json"
            };
        }

        #endregion

        #region << Modulo 5 >>

        [HttpPost]
        public ActionResult _GetMCategoria(string cod_channel)
        {
            return View(
                new E_DatosGrafico_Categoria().Lista(
                    new Request_DatosGrafico_Categoria()
                    {
                        cod_company = 1561,
                        cod_channel = cod_channel,
                        cod_reporte = 58,
                        cod_categoria = "",
                        path = 2
                    }
                )
            );
        }

        public ActionResult _GetMSkuGraficos(int cod_perido,int id_grilla, string categoria)
        {
            ViewBag.Periodo = cod_perido;
            //ViewBag.Reporte = 19;
            ViewBag.Company = 1561;
            ViewBag.Grilla = id_grilla;
            ViewBag.categoria = categoria;
            return View();  
        }

        [HttpPost]
        public ActionResult Get_Update_Sku_GraficosDatos(string cod_product, int id_reportsplanning, int id_grilla, string id_productcategory)
        {
            int oUpdate = new Response_Update_Module_SkuGraficos().Update(
                    new Request_Update_Module_SkuGraficos()
                    {
                        cod_product = cod_product,
                        id_reportsplanning = id_reportsplanning,
                        //report_id = 19,
                        company_id = 1561,
                        id_grilla = id_grilla,
                        id_productcategory = id_productcategory
                    }
                );
            return Json(new { Archivo = oUpdate });
        }

        [HttpPost]
        public ActionResult Get_Update_Sku_Order_GraficosDatos(string cod_product, int id_reportsplanning, int id_grilla, string id_productcategory)
        {
            int oUpdate = new Response_Update_Module_SkuGraficos().UpdateOrderSku(
                    new Request_Update_Module_SkuGraficos()
                    {
                        cod_product = cod_product,
                        id_reportsplanning = id_reportsplanning,
                        //report_id = 19,
                        company_id = 1561,
                        id_grilla = id_grilla,
                        id_productcategory = id_productcategory
                    }
                );
            return Json(new { Archivo = oUpdate });
        }

        [HttpPost]
        public ActionResult _GetMGrupoGraf(int id_grilla, int id_reportsplanning, string id_ProductCategory)
        {
            return View(
                new M_GrupoGraf().Lista(
                    new Request_Graf_Sku_Agrup_x_per()
                    {
                        id_planning = Convert.ToString(Session["Session_Campania"]),
                        id_grilla = id_grilla,
                        id_reportsplanning = id_reportsplanning,
                        id_ProductCategory = id_ProductCategory
                    }
                )
            );
        }

        [HttpPost]
        public ActionResult _GetMGroupSkuGraf(int id_reportsplanning, int id_grilla, string id_ProductCategory, int id_grupo)
        {
            ViewBag.Periodo = id_reportsplanning;
            ViewBag.Grilla = id_grilla;
            ViewBag.Categoria = id_ProductCategory;
            ViewBag.Grupo = id_grupo;
            return View();
        }

        [HttpPost]
        public ActionResult Get_Update_Group_Sku_GraficosDatos(string cod_product, int id_reportsplanning, int id_grupo, int id_grilla, string id_productcategory)
        {
            int oUpdate = new Response_Update_Module_Graficos_Group_Sku().Update(
                    new Request_Update_Grafico_Sku()
                    {
                        cod_product = cod_product,
                        id_reportsplanning = id_reportsplanning,
                        id_grupo = id_grupo,
                        id_grilla = id_grilla,
                        id_productcategory = id_productcategory
                    }
                );
            return Json(new { Archivo = oUpdate });
        }

        [HttpPost]
        public ActionResult Get_New_Cod_Group_Sku(int id_reportsplanning, int id_grilla, string id_productcategory)
        {
            int oUpdate = new Respone_New_Group_Sku_Graf().Cod_Grupo(
                    new Request_New_Group_Sku_Graf()
                    {
                        id_reportsplanning = id_reportsplanning,
                        id_grilla = id_grilla,
                        id_productcategory = id_productcategory
                    }
                );
            return Json(oUpdate);
        }

        [HttpPost]
        public ActionResult Inser_New_Grupo_Sku_Graf(string cod_product, int id_reportsplanning, int id_grilla, int id_grupo, string id_productCategory)
        {
            int oInsert = new Respone_New_Group_Sku_Graf().Inser_New_Grupo_Sku_Graf(
                    new Request_Insert_New_Group_Sku_Graf()
                    {
                        cod_product = cod_product,
                        id_reportsplanning = id_reportsplanning,
                        id_grilla = id_grilla,
                        id_grupo = id_grupo,
                        id_productCategory = id_productCategory,
                        user = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                );
            return Json(new { oInsert });
        }

        [HttpPost]
        public ActionResult _GetOrderSku(int cod_perido, int id_grilla, string categoria)
        {
            ViewBag.Periodo = cod_perido;
            //ViewBag.Reporte = 19;
            ViewBag.Company = 1561;
            ViewBag.Grilla = id_grilla;
            ViewBag.categoria = categoria;
            return View();
        }

        #endregion

        #region Modulo Comentarios

        [HttpPost, ValidateInput(false)]
        public ActionResult ReportingInsertComentarios(int periodo, string planning, string comentario)
        {
            InsertComentarios_Request oRq = new InsertComentarios_Request();
            InsertComentarios_Response oRp;

            oRq.periodo = periodo;
            oRq.planning = planning;
            oRq.comentario = comentario;
            //oRq.user_modif = ((LuckyXplora.Persona)Session["Session_Login"]).name_user;

            oRp = MvcApplication._Deserialize<InsertComentarios_Response>(MvcApplication._Servicio_Operativa.insertComments(MvcApplication._Serialize(oRq)));
            return Json(oRp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult ListaCommentsColgateFusion(int periodo, string planning)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new E_Comentarios().Lista(new InsertComentarios_Request()
                    {
                        periodo = periodo,
                        planning = planning
                    })
                ),
                ContentType = "application/json"
            };

        }

        #endregion

    }
}