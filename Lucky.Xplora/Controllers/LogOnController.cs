﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using Lucky.Xplora.DataAccess;
using Lucky.Xplora.Models;
//using Lucky.Xplora.Models.Alicorp;
using Lucky.Xplora.Models.GestionDocumentalWeb;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Converters;

namespace Lucky.Xplora.Controllers
{
    public class LogOnController : Controller
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            if (Session["PersonalAutenticado"] != null)
            {
                return RedirectToAction("PanelAcceso", "LogOn");
            }

            string ip = Request.UserHostAddress;
            ViewBag.IP = ip;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Persona a)
        {
            Int32 iConteo = 0;
            string sVista = "";
            string sProyecto = "";
            string sCodVista = "";
            string sCampania = "";
            string ip = Request.UserHostAddress;
            string mq = Request.UserHostName;

            //if (ModelState.IsValid)
            if (ModelState.IsValidField("name_user") && ModelState.IsValidField("user_password"))
            {
                DateTime dFechaActual = DateTime.Now;
                Persona oBj = new Persona();
                Persona_Login_Request _request = new Persona_Login_Request();

                _request.name_user = a.name_user;
                _request.user_password = a.User_Password;
                _request.key_encrypt = "YourUglyRandomKeyLike-lkj54923c478";
                _request.user_ip = ip;
                _request.user_maquina = mq;
                _request.user_lat = a.user_lat;
                _request.user_lon = a.user_lon;

                Session.Clear();
                 oBj = new Persona().Login_new(_request);
                //x = oBj.Login_new(_request);

                 if (oBj == null)
                 {
                     return View();
                 }

                if(oBj.existe == true)
                {
                    if (oBj.Person_Status == false)
                    {
                        ViewBag.msj = "El usuario se encuentra bloqueado, comunicarse con el administrador...";
                    }
                    /*else if (oBj.Person_ExpirationDate <= dFechaActual) {
                        return RedirectToAction("ModificaContraseña", new { __caducado = 1 });
                    }*/
                    else
                    {
                        #region Asignacion de accesos
                        /*
                         List<E_NW_Ctrl_Aceeso_Proyecto> oBjAccesos = new E_NW_Ctrl_Aceeso_Proyecto().Lista(new NW_Ctrl_Aceeso_Request()
                         {
                             oParametros = new E_NW_Ruteo_Parametros()
                             {
                                 Cod_Perfil = oBj.Perfil_id,
                                 Cod_Persona = Convert.ToString(oBj.Person_id)
                             }
                         });

                         oBj.Accesos = oBjAccesos;
                          */
                        #endregion

                        #region Pagina por default
                        //List<E_NW_Ctrl_Aceeso_Proyecto> lAc = (List<E_NW_Ctrl_Aceeso_Proyecto>)((Persona)Session["Session_Login"]).Accesos;
                        //List<E_NW_Ctrl_Aceeso_Proyecto> lAc = oBjAccesos;

                        /*if (lAc != null)
                        {
                            foreach (E_NW_Ctrl_Aceeso_Proyecto oAc in lAc)
                            {
                                foreach (E_NW_Ctrl_Aceeso_Modulo oMo in oAc.Hijo)
                                {
                                    foreach (E_NW_Ctrl_Aceeso_Vista oVi in oMo.Hijo)
                                    {
                                        if (oVi.ovi_inicio == 1)
                                        {
                                            sCampania = oMo.Cod_Equipo;
                                            sCodVista = Convert.ToString(oVi.Cod_Vista);
                                            sVista = oVi.Url_vista;
                                            sProyecto = oAc.Url_Proyecto;
                                            iConteo += oVi.ovi_inicio;
                                        }
                                    }
                                }
                            }
                        }*/
                        #endregion

                        Session["Session_Login"] = oBj;

                        if (iConteo == 1)
                        {
                            return RedirectToAction(sVista, sProyecto, new { _a = sCampania, _b = sCodVista });
                            //Response.Redirect(Url.Action(sVista, sProyecto));
                        }

                        return RedirectToAction("PanelAcceso");
                    }
                }
                else {
                    ViewBag.msj = "Nombre de usuario o contraseña incorrecta...";

                    if (Session["Intentos"] == null)
                    {
                        Session["Intentos"] = 1;
                    }
                    else
                    {
                        Session["Intentos"] = Convert.ToInt32(Session["Intentos"].ToString()) + 1;
                    }
                    if (Session["Intentos"].ToString() == "3")
                    {

                        Persona_Login_Request _request2 = new Persona_Login_Request();
                        _request2.name_user = a.name_user;

                        Person_Bloqueo oBj2 = new Persona().Login_Bloqueo(_request2);
                        ViewBag.msj = oBj2.Comentario;
                        Session["Intentos"] = null;
                    }
                }
            }

            return View();
        }

        /// <summary>
        /// Fecha: 2017-04-04
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AccesoDirecto(int Id) {
            int iConteo = 0;
            string sVista = "";
            string sProyecto = "";
            string sCodVista = "";
            string sCampania = "";

            //AlicorpGestionPreciosPersonaDatos oAlicorpGestionPreciosPersonaDatos = new AlicorpGestionPrecios().PersonaDatos(new AlicorpGestionPreciosParametro() { opcion = 10, parametro = Convert.ToString(Id) + ",0,0" });
            //Persona oPersona = new Persona();
            //oPersona.name_user = oAlicorpGestionPreciosPersonaDatos.PerUsuario;
            //oPersona.User_Password = oAlicorpGestionPreciosPersonaDatos.PerContraseña;

            Persona_Login_Request _request = new Persona_Login_Request();
            //_request.name_user = oPersona.name_user;
            //_request.user_password = oPersona.User_Password;
            //_request.key_encrypt = "YourUglyRandomKeyLike-lkj54923c478";

            Session.Clear();
            Persona oBj = new Persona().Login_new(_request);

            #region Asignacion de accesos
            List<E_NW_Ctrl_Aceeso_Proyecto> oBjAccesos = new E_NW_Ctrl_Aceeso_Proyecto().Lista(new NW_Ctrl_Aceeso_Request()
            {
                oParametros = new E_NW_Ruteo_Parametros()
                {
                    Cod_Perfil = oBj.Perfil_id.ToString(),
                    Cod_Persona = Convert.ToString(oBj.Person_id)
                }
            });

            oBj.Accesos = oBjAccesos;
            #endregion

            #region Pagina por default
            //List<E_NW_Ctrl_Aceeso_Proyecto> lAc = (List<E_NW_Ctrl_Aceeso_Proyecto>)((Persona)Session["Session_Login"]).Accesos;
            List<E_NW_Ctrl_Aceeso_Proyecto> lAc = oBjAccesos;

            if (lAc != null)
            {
                foreach (E_NW_Ctrl_Aceeso_Proyecto oAc in lAc)
                {
                    foreach (E_NW_Ctrl_Aceeso_Modulo oMo in oAc.Hijo)
                    {
                        foreach (E_NW_Ctrl_Aceeso_Vista oVi in oMo.Hijo)
                        {
                            if (oVi.ovi_inicio == 1)
                            {
                                sCampania = oMo.Cod_Equipo;
                                sCodVista = Convert.ToString(oVi.Cod_Vista);
                                sVista = oVi.Url_vista;
                                sProyecto = oAc.Url_Proyecto;
                                iConteo += oVi.ovi_inicio;
                            }
                        }
                    }
                }
            }
            #endregion

            Session["Session_Login"] = oBj;
            return RedirectToAction(sVista, sProyecto, new { _a = sCampania, _b = sCodVista });

            //return RedirectToAction("LogIn", "LogOn", new { a = oPersona });

            //Login(oPersona);

            //Redirect();

            //return null;
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult OlvidoContraseña() {
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OlvidoContraseña(Persona __a)
        {
            Int32 iResultado = new Persona().EnvioContraseña(new PersonaParametro() { opcion = 0, parametro = __a.name_user });

            if (iResultado == 1)
            {
                ViewBag.msj = "<div class=\"alert alert-info\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;Se envió su contraseña a su correo, favor de verificar su bandeja.</div>";
            }
            else
            {
                ViewBag.msj = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;Usuario incorrecto o deshabilitado, ingrese un usuario valido.</div>";
            }

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ModificaContraseña()
        {
            if (Request["__caducado"] != null && Convert.ToInt32(Request["__caducado"]) == 1)
            {
                ViewBag.msj = "<div class=\"alert alert-info\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;La contraseña ha expirado, favor de \"Modificar contraseña\".</div>";
            }
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModificaContraseña(Persona __a)
        {
            Int32 iResultado = 0;
            Persona oPe = new Persona().Persona(new PersonaParametro() { opcion = 1, parametro = __a.name_user });

            if (oPe == null) {
                ViewBag.msj = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;Usuario no existe, ingrese un usuario valido.</div>";
                return View();
            }
            else if (__a.User_Password != oPe.User_Password)
            {
                ViewBag.msj = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;Contraseña anterior no es la correcta, ingrese credenciales validas.</div>";
                return View();
            }
            else if (__a.User_Password == __a.User_Password_Renew) {
                ViewBag.msj = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;Anterior y la nueva contraseña no pueden ser iguales.</div>";
                return View();
            }
            else if (__a.User_Password_Renew.Length < 6) {
                ViewBag.msj = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;Cantidad de caracteres deben ser mayor o igual a 6.</div>";
                return View();
            }
            else if (!IsValidPassword(__a.User_Password_Renew))
            {
                ViewBag.msj = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;Contraseña debe ser alfanumerico.</div>";
                return View();
            }
            else {
                iResultado = new Persona().Cambiar_contrasena(new Persona_Cambiar_Contrasena_Request()
                {
                    id_user = oPe.Person_id,
                    user_password = __a.User_Password_Renew,
                    key_encrypt = "YourUglyRandomKeyLike-lkj54923c478"
                });

                if (iResultado > 0)
                {
                    ViewBag.msj = "<div class=\"alert alert-info\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;Contraseña modificada, inicie sesion con la nueva contraseña.</div>";
                }
                else {
                    ViewBag.msj = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span>&nbsp;Problemas con las aplicacion, favor de comunicarse con el administrador.</div>";
                }

                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult ValidarContrasena(string __a)
        {
            Persona_Login_Request _request = new Persona_Login_Request();
            _request.name_user = ((Persona)Session["Session_Login"]).name_user;
            _request.user_password = __a;
            _request.key_encrypt = "YourUglyRandomKeyLike-lkj54923c478";
            Persona oBj = new Persona().Login_new(_request);
            if (oBj != null)
            {
                if (oBj.existe == true)
                {
                    return Json(0);
                }
                else
                {
                    return Json(1);
                }
            }
            return Json(2);
        }

        [HttpPost]
        public JsonResult GrabarNuevaContrasena(string __a, string __b, string __c)
        {
            Persona_Login_Request _request = new Persona_Login_Request();
            _request.name_user = ((Persona)Session["Session_Login"]).name_user;
            _request.user_password = __a;
            _request.key_encrypt = "YourUglyRandomKeyLike-lkj54923c478";
            Persona oBj = new Persona().Login_new(_request);

            int salio = 0;
            if (oBj.existe == true && (__b == __c))
            {
                salio = new Persona().Cambiar_contrasena(new Persona_Cambiar_Contrasena_Request()
                {
                    id_user = ((Persona)Session["Session_Login"]).Person_id,
                    user_password = __b,
                    key_encrypt = "YourUglyRandomKeyLike-lkj54923c478"
                });
            }
            if (salio > 0)
            {
                return Json(new { status = "SUCCESS", message = "El cambio de contraseña se realizó con éxito" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "ERROR", message = "No se cambió la contraseña" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            Session.RemoveAll();

            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult PanelAcceso()
        {
            if (Session["Session_Login"] == null) {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [HttpPost]
        public ActionResult EliminarProyecto(int idtrabajo)
        {
            UsuarioDA.EliminarProyecto(idtrabajo);
            return Json(new { status = "SUCCESS", message = "El cambio de contraseña se realizó con éxito" }, JsonRequestBehavior.AllowGet);

        }

        #region Opciones Master Producciones
        [AllowAnonymous]
        public ActionResult ListarDocumento()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

      

        [AllowAnonymous]
        public ActionResult ListarPrivilegios()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult RegistrarFlujodeTrabajo()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult RegistrarEmpresa()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult RegistrarReunion()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult MantenimientoPrivilegio()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult ListarUsuario()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult ListarEmpresa()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult CrearUsuarios()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            List<Rol> roles = UsuarioDA.ObtenerRoles();
            ViewBag.Roles = roles;
            return View();
        }

        [HttpPost]
        public JsonResult Grabarusuario(string nombre, string apePaterno, string apeMaterno,int idRol,
            string usuario,string password, string dni, int estadoCivil, string direccion, string telefono,
                    string telefonoEmergencia)
        {
            PersonaUsuario personaUsuario = new PersonaUsuario();
            personaUsuario.ApellidoMaterno = apeMaterno;
            personaUsuario.ApellidoPaterno = apePaterno;
            personaUsuario.IdRol = idRol;
            personaUsuario.Nombre = nombre;
            personaUsuario.Usuario = usuario;
            personaUsuario.Password = password;
            
            //UsuarioDA usuarioDA = new UsuarioDA();
            int i = UsuarioDA.CrearUsuario(personaUsuario,dni, estadoCivil, direccion, telefono, telefonoEmergencia);
            if (i == 0)
            {
                //Exito
                return Json(new { status = "SUCCESS", message = "Se creo el Usuario con éxito" }, JsonRequestBehavior.AllowGet);

            }
            else if (i == 1)
            {
                //Ya existe
                return Json(new { status = "EXISTE", message = "Este Usuario ya existe" }, JsonRequestBehavior.AllowGet);

            }
            else {
                //Error
                return Json(new { status = "ERROR", message = "Hubo un error" }, JsonRequestBehavior.AllowGet);
            }
        }


        [AllowAnonymous]
        public ActionResult CrearEmpresa()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            List<Rol> roles = UsuarioDA.ObtenerRoles();
            ViewBag.Roles = roles;
            return View();
        }

        [HttpPost]
        public JsonResult GrabarEmpresa(string nombre, string descripcion,string direccion,string ciudad,
            string telefono1,string telefono2)
        {
            
            //UsuarioDA usuarioDA = new UsuarioDA();
            int i = UsuarioDA.CrearEmpresa(nombre, descripcion, direccion, ciudad,telefono1,telefono2);
            if (i != 2)
            {
                //Exito
                return Json(new { status = "SUCCESS", message = "Se creo el Usuario con éxito" }, JsonRequestBehavior.AllowGet);

            }
            
            else
            {
                //Error
                return Json(new { status = "ERROR", message = "Hubo un error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GrabarProyecto(string nombre, string descripcion, int tarea, int area, int empresa, long usuario, int prioridad, string fechaInicio, string fechaFin)
        {
            Int64 idusuario = ((Persona)Session["Session_Login"]).Person_idUsuario2;
           
            //UsuarioDA usuarioDA = new UsuarioDA();
            int i = UsuarioDA.CrearProyecto(usuario, nombre, descripcion, tarea, area, empresa, prioridad, fechaInicio, fechaFin);
            if (i > 0)
            {
                //Exito
                return Json(new { status = "SUCCESS", message = "Se creo el Proyecto con éxito" }, JsonRequestBehavior.AllowGet);

            }
            else if (i == 1)
            {
                //Ya existe
                return Json(new { status = "EXISTE", message = "Este Proyecto ya existe" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                //Error
                return Json(new { status = "ERROR", message = "Hubo un error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public ActionResult ListarTrabajos()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult RegistrarTrabajo()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            List<Area> areas = UsuarioDA.ObtenerAreas();
            List<Tarea> tareas = UsuarioDA.ObtenerTareas();
            List<Models.GestionDocumentalWeb.Empresa> empresas = UsuarioDA.ObtenerEmpresas();
            ViewBag.Areas = areas;
            ViewBag.Tareas = tareas;
            ViewBag.Empresas = empresas;
            return View();
        }

        [HttpPost]
        public JsonResult EditarUsuario(long IdUsuario)
        {
            //Exito
            Session.Remove("ParameterUsuario");
            Session["ParameterUsuario"] = IdUsuario;
            return Json(new { status = "SUCCESS", message = "Se creo el Usuario con éxito" }, JsonRequestBehavior.AllowGet);

        }


        [AllowAnonymous]
        public ActionResult EditarUsuarios()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            long IdUsuario = Int64.Parse(Session["ParameterUsuario"].ToString());

            PersonaUsuario personaUsuario = null;
            List<PersonaUsuario> users = UsuarioDA.ObtenerUsuarios();
            foreach (var obj in users) {
                if (obj.IdUsuario == IdUsuario) {
                    personaUsuario = obj;
                    break;
                }
            }
            ViewBag.IdUsuario = personaUsuario.IdUsuario;
            ViewBag.Nombre = personaUsuario.Nombre;
            ViewBag.ApellidoPaterno = personaUsuario.ApellidoPaterno;
            ViewBag.ApellidoMaterno = personaUsuario.ApellidoMaterno;
            ViewBag.Usuario = personaUsuario.Usuario;
            ViewBag.Password = personaUsuario.Password;
            ViewBag.IdRol = personaUsuario.IdRol;
            ViewBag.EstadoCivil = personaUsuario.IdEstadoCivil;

            ViewBag.Dni = personaUsuario.Dni;
            ViewBag.Direccion = personaUsuario.Direccion;
            ViewBag.Telefono = personaUsuario.Telefono;
            ViewBag.TelefEmergencia = personaUsuario.TelefEmergencia;
            return View(personaUsuario);
        }

        [HttpPost]
        public ActionResult EliminarUsuario(int idusuario)
        {
            UsuarioDA.EliminarProyecto(idusuario);
            return Json(new { status = "SUCCESS", message = "El cambio de contraseña se realizó con éxito" }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult ActualizarUsuario(long idusuario, string nombre, string apePaterno, string apeMaterno, int idRol,
            string usuario, string password, string dni, int estadocivil, string direccion, string telefono, string telefemergencia)
        {
            PersonaUsuario personaUsuario = new PersonaUsuario();
            personaUsuario.ApellidoMaterno = apeMaterno;
            personaUsuario.ApellidoPaterno = apePaterno;
            personaUsuario.IdRol = idRol;
            personaUsuario.Nombre = nombre;
            personaUsuario.Usuario = usuario;
            personaUsuario.Password = password;
            personaUsuario.IdUsuario = idusuario;
            //UsuarioDA usuarioDA = new UsuarioDA();
            int i = UsuarioDA.ActualizarUsuario(personaUsuario, dni, estadocivil, direccion, telefono, telefemergencia);
            if (i == 0)
            {
                //Exito
                return Json(new { status = "SUCCESS", message = "Se creo el Usuario con éxito" }, JsonRequestBehavior.AllowGet);

            }
            else if (i == 1)
            {
                //Ya existe
                return Json(new { status = "EXISTE", message = "Este Atributo Usuario ya existe" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                //Error
                return Json(new { status = "ERROR", message = "Hubo un error" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult EditarProyecto(int IdTrabajo)
        {
            //Exito
            Session.Remove("Parameter");
            Session["Parameter"] = IdTrabajo;
            return Json(new { status = "SUCCESS", message = "Se creo el Usuario con éxito" }, JsonRequestBehavior.AllowGet);

        }

        [AllowAnonymous]
        public ActionResult EditarProyectos()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            int IdTrabajo = Int32.Parse(Session["Parameter"].ToString());
            Trabajo trabajo = UsuarioDA.ObtenerTrabajoId(IdTrabajo);
            ViewBag.IdTrabajo = trabajo.IdTrabajo;
            ViewBag.IdUsuario = trabajo.IdUsuario;
            ViewBag.IdArea = trabajo.IdArea;
            ViewBag.IdTarea = trabajo.IdTarea;
            ViewBag.IdEmpresa = trabajo.IdEmpresa;
            ViewBag.IdPrioridad = trabajo.IdPrioridad;
            ViewBag.FechaInicio = trabajo.FechaInicio;
            ViewBag.FechaFin = trabajo.FechaFin;
            return View(trabajo);
        }

        [AllowAnonymous]
        public ActionResult ReporteDocumento()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            
            return View();
        }

        [AllowAnonymous]
        public ActionResult ReporteDescripcionProyecto()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            return View();
        }

        [HttpPost]
        public JsonResult ReporteDocumentacionProyectoAction(long? IdTrabajo) {
            Session.Remove("IdTrabajoDescripcion");
            Session["IdTrabajoDescripcion"] = IdTrabajo;
            return Json(new { status = "SUCCESS", message = "Se actualizo el proyecto con éxito" }, JsonRequestBehavior.AllowGet);
        }


        [AllowAnonymous]
        public ActionResult ReporteDocumentacionProyecto()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
                
            }

            long IdTrabajo = Int32.Parse(Session["IdTrabajoDescripcion"].ToString());
            List<TrabajoResponse> list = UsuarioDA.ObtenerTrabajosDocumentos();
            foreach (var obj in list)
            {
                if (obj.IdTrabajo == IdTrabajo)
                {
                    ViewBag.Trabajo = obj;
                    break;
                }
            }

            return View();
        }



        [HttpPost]
        public JsonResult ActualizarProyecto( )
        {
            //int IdTrabajo, Int64 IdUsuario, int IdTarea, int IdArea, int IdEmpresa
            //Exito
            try
            {
                var json = Request.Form["Json"];
                if (Request.Files.AllKeys.Any())
                {
                    
                    var Temp = JsonConvert.DeserializeObject(json);
                    string ParsedJson = JsonConvert.SerializeObject(Temp);
                    var format = "dd/MM/yyyy hh:mm:ss"; // your datetime format
                    var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

                    ProyectoRequest proyecto = JsonConvert.DeserializeObject<ProyectoRequest>(ParsedJson,dateTimeConverter);
                    long time = DateTime.Now.Ticks;
                    var now = Convert.ToString(time);
                    List<DocumentosRequest> list = proyecto.documentos;
                    
                    for(int i = 0; i < list.Count;i++)
                    {
                        var httpPostedFile = Request.Files["Archivo"+i];
                        var fileName = Path.GetFileName(httpPostedFile.FileName);
                        string nombre = httpPostedFile.FileName;
                        var nameFile = now + fileName;
                        var filePath = Path.Combine(Server.MapPath("~/Content/GestionDocumental"), nameFile);
                        list.ElementAt(i).nombreArchivo = nameFile;
                        //var filePath = Path.Combine(Server.MapPath(System.Web.HttpContext.Current.Server.MapPath(Common.Constantes.P100AunaDocs.FileSavePath)), fecha + fileName);
                        httpPostedFile.SaveAs(filePath);
                    }
                    proyecto.documentos = list;

                    //Update Proyecto
                    UsuarioDA.EliminarDocumentoProyecto(proyecto.IdTrabajo);
                    UsuarioDA.ActualizarProyecto(proyecto);
                    UsuarioDA.DocumentoProyecto(proyecto);
                    //Remove, Insert Document

                    return Json(new { status = "SUCCESS", message = "Se actualizo el proyecto con éxito" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { status = "BADREQUEST", message = "Se actualizo el proyecto con éxito" }, JsonRequestBehavior.AllowGet);

                }

                
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                return Json(new { status = "BADREQUEST", message = "Se actualizo el proyecto con éxito" }, JsonRequestBehavior.AllowGet);

            }

        }
        #endregion


        #region Valida contraseña
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        private bool IsLetter(char c)
        {
            return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        private bool IsDigit(char c)
        {
            return c >= '0' && c <= '9';
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        private bool IsSymbol(char c)
        {
            return c > 32 && c < 127 && !IsDigit(c) && !IsLetter(c);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-23
        /// </summary>
        /// <param name="contraseña"></param>
        /// <returns></returns>
        private bool IsValidPassword(string contraseña)
        {
            return
               contraseña.Any(c => IsLetter(c)) &&
               contraseña.Any(c => IsDigit(c));
        }
        #endregion

        #region Page - One

        public ActionResult PageOne() {
            return View();
        }

        #endregion
    }
}