﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Map;

using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;

namespace Lucky.Xplora.Controllers
{
    public class LuckyMapsController : Controller
    {
        //
        // GET: /LuckyMaps/

        public ActionResult Index()
        {
            /*temporal, con datos principales*/
            //Session["Session_Login"] = new Persona() { 
            //    Company_id = 1562
            //};
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        [HttpPost]
        public ActionResult GetCampania() {
            return View(new Campania().Lista(new MapMultiGPS_Request() {
                parametros = new E_parametros_MultiGPS(){
                    tipo = 1,
                    Empresa = Convert.ToString(((Persona)Session["Session_Login"]).Company_id)
                }
            }));
        }

        [HttpPost]
        public ActionResult GetCiudad(string __a)
        {
            return View(new Ciudad().Lista(new MapMultiGPS_Request()
            {
                parametros = new E_parametros_MultiGPS()
                {
                    tipo = 2,
                    Cod_Equipo = __a
                }
            }));
        }

        [HttpPost]
        public ActionResult GetPerfil(string __a, string __b)
        {
            return View(new Perfil().Lista(new MapMultiGPS_Request()
            {
                parametros = new E_parametros_MultiGPS()
                {
                    tipo = 3,
                    Cod_Equipo = __a,
                    Cod_Ciudad = __b
                }
            }));
        }

        [HttpPost]
        public ActionResult GetPersona(string __a, string __b, string __c)
        {
            return View(new Persona().Lista(new MapMultiGPS_Request()
            {
                parametros = new E_parametros_MultiGPS()
                {
                    tipo = 4,
                    Cod_Equipo = __a,
                    Cod_Ciudad = __b,
                    Cod_Cargo = __c
                }
            }));
        }

        [HttpPost]
        public ActionResult GetUbicacionGPS(string __a, string __b, string __c, int __d, string __e, string __f)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Ubicacion_GPS().Lista(new MapMultiGPS_Request()
                    {
                        parametros = new E_parametros_MultiGPS()
                        {
                            Cod_Equipo = __a,
                            Cod_Ciudad = __b,
                            Cod_Cargo = __c,
                            Cod_Usuario = __d,
                            Fecha_Inicio = __e,
                            Fecha_Fin = __f
                        }
                    })
                ),
                ContentType = "application/json"
            };
        }

        [HttpPost]
        public JsonResult DownloadInfo(string __a, string __b, string __c, string __d)
        {
            int _fila = 2;
            string _fileServer = "";
            string _filePath = "";

            /*verifica que la cadena este vacia*/
            if (__a.Length == 0) { return null; }

            List<Ubicacion_GPS> oLs = MvcApplication._Deserialize<List<Ubicacion_GPS>>(
                    __a
                );

            _fileServer = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            if (_fileNew.Exists)
            {
                _fileNew.Delete();
                _fileNew = new FileInfo(_filePath);
            }

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                /*crea hoja de trabajo*/
                Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("GPS");

                /*registro de cabecera*/
                oWs.Cells[1, 1].Value = "CIUDAD";
                oWs.Cells[1, 2].Value = "CARGO";
                oWs.Cells[1, 3].Value = "NOMBRE";
                oWs.Cells[1, 4].Value = "PDV";
                oWs.Cells[1, 5].Value = "FECHA INICION";
                oWs.Cells[1, 6].Value = "HORA INICIO";
                oWs.Cells[1, 7].Value = "LATITUD";
                oWs.Cells[1, 8].Value = "LONGITUD";
                oWs.Cells[1, 9].Value = "TIEMPO GESTION";
                oWs.Cells[1, 10].Value = "FECHA FIN";
                oWs.Cells[1, 11].Value = "HORA FIN";

                /*agrega detalle*/
                foreach (Ubicacion_GPS oBj in oLs)
                {
                    oWs.Cells[_fila, 1].Value = __b;
                    oWs.Cells[_fila, 2].Value = __c;
                    oWs.Cells[_fila, 3].Value = __d;
                    oWs.Cells[_fila, 4].Value = oBj.Nom_PDV;
                    oWs.Cells[_fila, 5].Value = oBj.Fec_Reg_Inicio;
                    oWs.Cells[_fila, 6].Value = oBj.Hora;
                    oWs.Cells[_fila, 7].Value = oBj.Latitud_Inicio;
                    oWs.Cells[_fila, 8].Value = oBj.Longitud_Inicio;
                    oWs.Cells[_fila, 9].Value = oBj.Min_Atencion;
                    oWs.Cells[_fila, 10].Value = oBj.Fec_Reg_Fin;
                    oWs.Cells[_fila, 11].Value = oBj.Hora_Fin;

                    _fila++;
                }

                oWs.Row(1).Style.Font.Bold = true;
                oWs.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                oWs.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                oWs.Column(1).AutoFit();
                oWs.Column(2).AutoFit();
                oWs.Column(3).AutoFit();
                oWs.Column(4).AutoFit();
                oWs.Column(5).AutoFit();
                oWs.Column(6).AutoFit();
                oWs.Column(7).AutoFit();
                oWs.Column(8).AutoFit();
                oWs.Column(9).AutoFit();
                oWs.Column(10).AutoFit();
                oWs.Column(11).AutoFit();

                oEx.Save();
            }

            return Json(new { Archivo = _fileServer });
        }

        public ActionResult MapsCalor() {
            return View();
        }

        [HttpPost]
        public ActionResult GetMapasTemp(string __a, string __b, string __c, int __d)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new DemMapTemperatura().Mapas(new Request_Maps_Temperatura()
                    {
                        vperiodo = __a,
                        vproducto = __b,
                        vcategoria=__c,
                        vmarca=__d
                      
                    })
                ),
                ContentType = "application/json"
            };
        }

    }
}