﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucky.Xplora.Models.Reporting;
using Lucky.Xplora.Models;
using System.Web.Script.Serialization;

namespace Lucky.Xplora.Controllers
{
    public class ReportingController : Controller
    {
        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            E_Hig_Grafico_Parametros Obj_p = new E_Hig_Grafico_Parametros();
            Obj_p.Anio = 2015;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View(new NW_Hig_Grafico().ALi_MN_Consul_Parametros_Iniciales(new Request_NW_Reporting()
            {
                oParametros = Obj_p
            }));
        }
        [HttpPost]
        public ActionResult GetGrafico1(int _a, string _b, int _c, int _d, int _e)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Anio = _a;
            o_Parametros.Cod_Mes = _b;
            o_Parametros.Cod_Periodo = _c;
            o_Parametros.Cod_Giro = _d;
            o_Parametros.Cod_Oficina = _e;
            o_Parametros.Cod_Equipo = "002892382010";

            NW_Hig_Grafico Objgrafico = new NW_Hig_Grafico();
            Objgrafico = null;
            Objgrafico = new NW_Hig_Grafico().ALi_MN_Consul_SOD_x_OficinaTipoGiro(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            });

            return View(Objgrafico);
        }
        [HttpPost]
        public ActionResult GetGrafico2(int _a, string _b, int _c, int _d, int _e)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Anio = _a;
            o_Parametros.Cod_Mes = _b;
            o_Parametros.Cod_Periodo = _c;
            o_Parametros.Cod_Giro = _d;
            o_Parametros.Cod_Oficina = _e;
            o_Parametros.Cod_Equipo = "002892382010";
            return View(new NW_Hig_Grafico().ALi_MN_Consul_SOD_x_OficinaTipoGiro_Acumulado(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        [HttpPost]
        public ActionResult GetGrafico3(int _a, int _b, int _c, int _d, string _e)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Anio = _a;
            o_Parametros.Cod_Trimestre = _b;
            o_Parametros.Cod_Equipo = "002892382010";
            o_Parametros.Cod_Giro = _c;
            o_Parametros.Cod_Oficina = _d;
            o_Parametros.Cod_Categoria = _e;

            return View(new NW_Hig_Grafico().ALi_MN_Consul_SOD_x_Categoria_Marca(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        [HttpPost]
        public ActionResult GetGrafico4(int _a, int _b, int _c, int _d, string _e)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Anio = _a;
            o_Parametros.Cod_Trimestre = _b;
            o_Parametros.Cod_Equipo = "002892382010";
            o_Parametros.Cod_Giro = _c;
            o_Parametros.Cod_Oficina = _d;
            o_Parametros.Cod_Categoria = _e;

            return View(new NW_Hig_Grafico().ALi_MN_Consul_SOD_x_Categoria_Marca_Acumulado(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }

        /* Inicio filtros */
        public ActionResult GetAnio(string _a)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Op = 1;
            o_Parametros.Parametros = "";
            return View(new M_Combo().Listar_combos(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        public ActionResult GetMes(string _a)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Op = 2;
            o_Parametros.Parametros = _a;
            return View(new M_Combo().Listar_combos(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        public ActionResult GetCobertura(string _a)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Op = 3;
            o_Parametros.Parametros = "";
            return View(new M_Combo().Listar_combos(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        public ActionResult GetTipoGiro(string _a)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Op = 4;
            o_Parametros.Parametros = "";
            return View(new M_Combo().Listar_combos(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        public ActionResult GetOficina(string _a, string _b)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Op = 5;
            o_Parametros.Parametros = _a;
            ViewBag._Op_Ofi = _b;
            return View(new M_Combo().Listar_combos(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        public ActionResult GetCategoria(string _a)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Op = 6;
            o_Parametros.Parametros = "";
            return View(new M_Combo().Listar_combos(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        public ActionResult GetPeriodo(string _a, string _b)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Op = 7;
            o_Parametros.Parametros = _a + "," + _b;
            return View(new M_Combo().Listar_combos(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        public ActionResult GetTrimestre(string _a)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Op = 8;
            o_Parametros.Parametros = "";
            return View(new M_Combo().Listar_combos(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }

        #region << RQ Lucky 255 v1.0 >>

        public ActionResult TableroIndex()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            if (Request["_a"] != null)
            {
                Session["Session_Campania"] = Request["_a"];
                ViewBag.campania = Request["_a"];
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        [HttpPost]
        public ActionResult _Get_Graf_SellOut_Strategic(int anio, int mes, int cod_oficina, string cod_cadena, string cod_categoria, int cod_marca)
        {
            List<E_Grafico_Sellout> oLs = new E_Grafico_Sellout().Lista(
                    new Request_Graf_SellOut()
                    {
                        anio = anio,
                        mes = mes,
                        cod_oficina = cod_oficina,
                        cod_cadena = cod_cadena,
                        cod_categoria = cod_categoria,
                        cod_marca = cod_marca
                    });

            return Json(new { Archivo = oLs });
            //return new ContentResult
            //{
            //    Content = MvcApplication._Serialize(oLs),
            //    ContentType = "application/json"
            //};
        }

        public ActionResult _Get_Graf_NSG_Strategic(int anio, int mes, int cod_oficina, string cod_cadena, string cod_categoria, int cod_marca)
        {
            List<E_Grafico_NSG> oLGnsg = new E_Grafico_NSG().Lista(
                    new Request_Graf_SellOut()
                    {
                        anio = anio,
                        mes = mes,
                        cod_oficina = cod_oficina,
                        cod_cadena = cod_cadena,
                        cod_categoria = cod_categoria,
                        cod_marca = cod_marca
                    });

            return Json(new { Archivo = oLGnsg });
        }

        public ActionResult _Get_Graf_SOD_Strategic(int anio, int mes, int cod_oficina, string cod_cadena, string cod_categoria, int cod_marca)
        {
            List <E_Grafico_SOD> oLSOD = new E_Grafico_SOD().Lista(
                   new Request_Graf_SellOut()
                   {
                       anio = anio,
                       mes = mes,
                       cod_oficina = cod_oficina,
                       cod_cadena = cod_cadena,
                       cod_categoria = cod_categoria,
                       cod_marca = cod_marca
                   }
               );
            return Json(new { Archivo = oLSOD });
        }

        public ActionResult _Get_Graf_EEAA_Strategic(int anio, int mes, int cod_oficina, string cod_cadena, string cod_categoria, int cod_marca)
        {
            List<E_Grafico_EEAA> olEEAA = new E_Grafico_EEAA().Lista(
                    new Request_Graf_SellOut()
                    {
                        anio = anio,
                        mes = mes,
                        cod_oficina = cod_oficina,
                        cod_cadena = cod_cadena,
                        cod_categoria = cod_categoria,
                        cod_marca = cod_marca
                    });
            return Json(new { Archivo = olEEAA });
        }

        public ActionResult PRT_StrategicBoardAliAASS(int anio, int mes, int cod_oficina, string cod_cadena, string cod_categoria, int cod_marca)
        {
            //ViewBag.Anio = anio;
            //ViewBag.Mes = mes;
            //ViewBag.Oficina = cod_oficina;
            //ViewBag.Cadena = cod_cadena;
            //ViewBag.Categoria = cod_categoria;
            //ViewBag.Marca = cod_marca;
            //return View();

            E_Strategic_Board_Grafico_Parametros o_Parametros = new E_Strategic_Board_Grafico_Parametros();

            o_Parametros.anio = anio;
            o_Parametros.mes = mes;
            o_Parametros.cod_oficina = cod_oficina;
            o_Parametros.cod_cadena = cod_cadena;
            o_Parametros.cod_categoria = cod_categoria;//cod_categoria;
            o_Parametros.cod_marca = cod_marca;

            return View(new E_NW_Table_tblStrategy().Consul_TB_Strategic_Board(new Request_NW_Table_tblStrategy()
            {
                oParametros = o_Parametros
            }));
        }

        public ActionResult _Get_Graf_SellOut_Cluster(int anio, int mes, int cod_oficina, string cod_cadena, string cod_categoria, int cod_marca)
        {
            return View(new E_Grafico_Sellout().Lista(
                    new Request_Graf_SellOut()
                    {
                        anio = anio,
                        mes = mes,
                        cod_oficina = cod_oficina,
                        cod_cadena = cod_cadena,
                        cod_categoria = cod_categoria,
                        cod_marca = cod_marca
                    }
                ));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 07-04-2015
        /// </summary>
        public ActionResult Indicador_Tablero_Estrategico(int anio, int mes, string cod_oficina, string cod_cadena, string cod_categoria, string cod_marca)
        {
            List<E_INDICADORES_TB> oLs = new E_INDICADORES_TB().Lista(
            new Request_Indi_Tab_Estra()
            {
                anio = anio,
                mes = mes,
                oficina = Convert.ToInt32(cod_oficina),
                cadena = cod_cadena,
                categoria = cod_categoria,
                marca = Convert.ToInt32(cod_marca)
            });

            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        #endregion


        #region Fotografico

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-25
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Fotografico(string _a)
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return RedirectToAction("Fotografico", "BackusReporting", new { _a = _a });
        }

        #endregion

        #region <<< Reporting Ali MAY >>>

        public ActionResult ResumneMY() {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = 0;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View(new E_Nw_Rp_AliMay_Param_Ini().Consul_Param_Ini(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_ResumenNNCatg(int _vperiodo) {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;

            return View(new E_Nw_Rp_AliMay_ResumenNNCatg().Consul_ResumenNN_Catg(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }
        public ActionResult PRT_ResumenNNMarca(int _vperiodo,string _vcategoria)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Categoria = _vcategoria;

            return View(new E_Nw_Rp_AliMay_ResumenNN_Marca().Consul_ResumenNN_Marca(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }
        
        public ActionResult EvolutivoDGMY() {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = 0;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View(new E_Nw_Rp_AliMay_Param_Ini().Consul_Param_Ini(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_EvolutivoOfiCatg(int _vperiodo,string _vcategoria,int _voficina) {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Categoria = _vcategoria;
            oParametros.Cod_Oficina = _voficina;

            return View(new E_Nw_Rp_AliMay_EvolutivoOfiCatg().Consul_Evolutivo_Categoria(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_EvolutivoOfiMarca(int _vperiodo, string _vcategoria, int _voficina,int _vmarca)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Categoria = _vcategoria;
            oParametros.Cod_Oficina = _voficina;
            oParametros.Cod_Marca = _vmarca;

            return View(new E_Nw_Rp_AliMay_EvolutivoOfiCatg().Consul_Evolutivo_Marca(new Request_NW_Reporting() { oParametros = oParametros }));
        }

        public ActionResult EvolutivoPeriodoxCategoria(int _vperiodo,string _vcategoria,int _voficina) {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Categoria = _vcategoria;
            oParametros.Cod_Oficina = _voficina;

            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
            var result = new ContentResult
            {
                Content = serializer.Serialize(
                        new E_Nw_Rp_AliMay_EvoClixPeriodo().Consul_EvolutivoxPeriodo_Categoria(new Request_NW_Reporting() { oParametros = oParametros })
                    ),
                ContentType = "application/json"
            };
            return result;
        }
        public ActionResult EvolutivoPeriodoxMarca(int _vperiodo, string _vcategoria, int _voficina,int _vmarca)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Categoria = _vcategoria;
            oParametros.Cod_Oficina = _voficina;
            oParametros.Cod_Marca = _vmarca;

            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
            var result = new ContentResult
            {
                Content = serializer.Serialize(
                        new E_Nw_Rp_AliMay_EvoClixPeriodo().Consul_EvolutivoxPeriodo_Marca(new Request_NW_Reporting() { oParametros = oParametros })
                    ),
                ContentType = "application/json"
            };
            return result;
        }


        public ActionResult GerenciaRegionalVentaMY() {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = 0;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View(new E_Nw_Rp_AliMay_Param_Ini().Consul_Param_Ini(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_VentaRegionalCatg(int _vperiodo,int _voficina) {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Oficina = _voficina;

            return View(new E_Nw_Rp_AliMay_VentaRegional().Consul_VentaRegional_Categoria(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_VentaRegionalMarca(int _vperiodo, int _voficina,string _vcategoria)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Oficina = _voficina;
            oParametros.Cod_Categoria = _vcategoria;

            return View(new E_Nw_Rp_AliMay_VentaRegional().Consul_VentaRegional_Marca(new Request_NW_Reporting() { oParametros = oParametros }));
        }

        public ActionResult GerenciaCategoriaMY() {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = 0;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View(new E_Nw_Rp_AliMay_Param_Ini().Consul_Param_Ini(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_Gerencia_Categoria(int _vperiodo,string _vcategoria) {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Categoria = _vcategoria;

            return View(new E_Nw_Rp_AliMay_GerenciaCategoria().Consul_Gerencia_Categoria(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_Gerencia_Marca(int _vperiodo, string _vcategoria,int _vmarca)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Categoria = _vcategoria;
            oParametros.Cod_Marca = _vmarca;

            return View(new E_Nw_Rp_AliMay_GerenciaCategoria().Consul_Gerencia_Marca(new Request_NW_Reporting() { oParametros = oParametros }));
        }

        public ActionResult DetalleDGPdvMY() {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = 0;
            
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View(new E_Nw_Rp_AliMay_Param_Ini().Consul_Param_Ini(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_DetalleDGPdvMY(int _vperiodo)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_Periodo = _vperiodo;

            return View(new E_Nw_Rp_AliMay_DetalleDGPDV().Consul_Detalle_DG_PDV(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult ConsulFiltro(string _vparam, int _vop)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Parametros = _vparam;
            oParametros.Cod_Op = _vop;

            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
            var result = new ContentResult
            {
                Content = serializer.Serialize(
                        new E_Nw_Rp_AliMay_Filtro().Consul_Filtro(new Request_NW_Reporting() { oParametros = oParametros })
                    ),
                ContentType = "application/json"
            };
            return result;
        }

        public ActionResult PRT_Filtros_AliMay(int _vobj,int _vop,string _vparam,string _vselect) {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Parametros = _vparam;
            oParametros.Cod_Op = _vop;
            ViewBag.cod_obj = _vobj;
            ViewBag.cod_select = _vselect;
            return View(new E_Nw_Rp_AliMay_Filtro().Consul_Filtro(new Request_NW_Reporting() { oParametros = oParametros }));
        }

        #endregion
    }
}