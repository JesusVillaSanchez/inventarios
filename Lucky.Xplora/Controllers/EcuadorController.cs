﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using IO = System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Ecuador;

using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using Drawing = OfficeOpenXml.Drawing;

using System.IO.MemoryMappedFiles;
using System.IO;
using Lucky.Business.Common.Servicio;
using System.Data;

namespace Lucky.Xplora.Controllers
{
    public class EcuadorController : Controller
    {
        //
        // GET: /Ecuador/

        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            return View();
        }

        public ActionResult Precio(string _a)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            ViewBag.vCod_equipo = _a;
            ViewBag.vCod_pf = ((Persona)Session["Session_Login"]).Perfil_id;            

            return View(new Nw_Rep_Filtros_Ecuador().Consul_Filtros(new Request_Nw_Reporting_Ecu_Filtros() { parametros = _a + ",19", opcion = 8 }));
        }
        public ActionResult PrecioCategoria(string _veq,string _vid,string _vcategoria,string _vmarca,string _vproducto,string _vperiodo,string _vprovincia,string _vciudad)
        {
            ViewBag.cod_elemento = _vid;
            return View(
                 new Nw_Rep_Precio().Consulta_reporte_precio(
                        new Request_Nw_Reporting_Ecu_Precio()
                        {
                            cod_equipo = _veq,
                            cod_categoria = _vcategoria,//"10306",
                            cod_marca = _vmarca,//"2561",
                            cod_perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString(),
                            cod_producto = _vproducto,//"E_SKU944",
                            cod_periodo = _vperiodo,
                            cod_provincia = _vprovincia,
                            cod_ciudad = _vciudad
                        }
                    )
                );        
        }
        public ActionResult Filtros(string _vobj, string _vparam, int _vop, string _vselect)
        {
            ViewBag.cod_obj = _vobj;
            ViewBag.cod_select = _vselect;
            ViewBag.cod_vop = _vop;
            return View(new Nw_Rep_Filtros_Ecuador().Consul_Filtros(new Request_Nw_Reporting_Ecu_Filtros() { parametros = _vparam, opcion = _vop }));         
        }

        public ActionResult Portafolio(string _a)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            ViewBag.vCod_equipo = _a;
            ViewBag.vCod_pf = ((Persona)Session["Session_Login"]).Perfil_id;
            return View(new Nw_Rep_Filtros_Ecuador().Consul_Filtros(new Request_Nw_Reporting_Ecu_Filtros() { parametros = _a + ",19", opcion = 8 }));
        }
        public ActionResult PortafolioCategoria(string _veq, string _vid, string _vcategoria,string _vprovincia,string _vciudad, string _vcadena,string _vperiodo)
        {
            ViewBag.cod_elemento = _vid;
            return View(
                 new Nw_Rep_Portafolio().Consulta_reporte_portafolio(
                        new Request_Nw_Reporting_Ecu_Assorment()
                        {
                            cod_equipo = _veq
                            , cod_categoria =_vcategoria
                            , cod_provincia = _vprovincia
                            , cod_ciudad = _vciudad 
                            , cod_cadena = _vcadena
                            , cod_periodo = _vperiodo
                            , cod_perfil =((Persona)Session["Session_Login"]).Perfil_id.ToString()
                        }
                    )
                );  
        }

        public ActionResult Publicacion(string _a) {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            ViewBag.vCod_equipo = _a;
            ViewBag.vCod_pf = ((Persona)Session["Session_Login"]).Perfil_id;
            return View();
        }
        public ActionResult PublicacionGrafico(string _veq, string _vcategoria, string _vprovincia, string _vciudad, string _vcadena, string _vperiodo)
        {            
            return View(
                 new Nw_Rep_Publicacion().Consulta_reporte_publicacion(
                        new Request_Nw_Reporting_Ecu_Publicacion()
                        {
                            cod_equipo = _veq
                            ,cod_categoria = _vcategoria
                            ,cod_provincia = _vprovincia
                            ,cod_ciudad = _vciudad
                            ,cod_cadena = _vcadena
                            ,cod_periodo = _vperiodo
                            ,cod_perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString()
                        }
                    )                
                );
        }

        public ActionResult Promocion(string _a)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            ViewBag.vCod_equipo = _a;
            ViewBag.vCod_pf = ((Persona)Session["Session_Login"]).Perfil_id;
            return View(new Nw_Rep_Filtros_Ecuador().Consul_Filtros(new Request_Nw_Reporting_Ecu_Filtros() { parametros = _a + ",55", opcion = 8 }));
        }
        public ActionResult PromocionCategoria(string _veq, string _vid, string _vcategoria, string _vperiodo, string _vprovincia, string _vciudad,string _vcadena)
        {
            
            ViewBag.cod_equipo = _veq;
            ViewBag.cod_categoria = _vcategoria;
            ViewBag.cod_provincia = _vprovincia;
            ViewBag.cod_ciudad = _vciudad;
            ViewBag.cod_cadena = _vcadena;
            ViewBag.cod_periodo = _vperiodo;
            ViewBag.cod_perfil = ((Persona)Session["Session_Login"]).Perfil_id;

            ViewBag.cod_elemento = _vid;
            return View(
                 new Nw_Rep_Promocion().Consulta_reporte_promocion(
                        new Request_Nw_Reporting_Ecu_Promocion()
                        {
                            cod_equipo = _veq,
                            cod_categoria = _vcategoria,//"10306",
                            cod_perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString(),
                            cod_periodo = _vperiodo,
                            cod_provincia = _vprovincia,
                            cod_ciudad = _vciudad,
                            cod_cadena = _vcadena
                        }
                    )
                );
        }

        public ActionResult Exhibicion(string _a)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            ViewBag.vCod_equipo = _a;
            ViewBag.vCod_pf = ((Persona)Session["Session_Login"]).Perfil_id;
            return View(new Nw_Rep_Filtros_Ecuador().Consul_Filtros(new Request_Nw_Reporting_Ecu_Filtros() { parametros = _a + ",31", opcion = 8 }));
        }
        public ActionResult ExhibicionCategoria(string _veq, string _vid, string _vcategoria, string _vperiodo, string _vprovincia, string _vciudad, string _vcadena)
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            ViewBag.cod_elemento = _vid;
            return View(
                 new Nw_Rep_Exhibicion().Consulta_reporte_exhibicion(
                        new Request_Nw_Reporting_Ecu_Exhibicion()
                        {
                            cod_equipo = _veq
                            ,cod_categoria = _vcategoria
                            ,cod_provincia = _vprovincia
                            ,cod_ciudad = _vciudad
                            ,cod_cadena = _vcadena
                            ,cod_periodo = _vperiodo
                            ,cod_perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString()
                        }
                    )
                );
        }

        public ActionResult JsonFiltro(string _vparam, int _vop)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Nw_Rep_Filtros_Ecuador().Consul_Filtros(
                    new Request_Nw_Reporting_Ecu_Filtros()
                    {
                        parametros = _vparam,
                        opcion = _vop
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-07-02
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DescargaFotografico(string __a, string __b, string __c, string __d, string __e, string __f, string __g)
        {
            string _server = "";
            string _local = "";
            int _fila = 2;

            List<Nw_Rep_Fotografico> oLs = new Nw_Rep_Fotografico().Lista(
                    new Request_Nw_Reporting_Ecu_Publicacion()
                    {
                        cod_equipo = __a,
                        cod_categoria = __b,
                        cod_provincia = __c,
                        cod_ciudad = __d,
                        cod_cadena = __e,
                        cod_periodo = __f,
                        cod_perfil = __g
                    }
                );

            _server = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _local = IO.Path.Combine(Server.MapPath("/Temp"), _server);

            IO.FileInfo _archivo = new IO.FileInfo(_local);
            if (_archivo.Exists)
            {
                _archivo.Delete();
                _archivo = new IO.FileInfo(_local);
            }

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_archivo))
            {
                Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Fotografico");

                oWs.Cells[1, 1].Value = "Cadena";
                oWs.Cells[1, 2].Value = "Punto de venta";
                oWs.Cells[1, 3].Value = "Categoria";
                oWs.Cells[1, 4].Value = "Fabricante";
                oWs.Cells[1, 5].Value = "Marca";
                oWs.Cells[1, 6].Value = "Producto";
                oWs.Cells[1, 7].Value = "Publicacion";
                oWs.Cells[1, 8].Value = "Grupo objetivo";
                oWs.Cells[1, 9].Value = "Foto";

                foreach (Nw_Rep_Fotografico oBj in oLs)
                {
                    oWs.Cells[_fila, 1].Value = oBj.cad_descripcion;
                    oWs.Cells[_fila, 2].Value = oBj.pdv_descripcion;
                    oWs.Cells[_fila, 3].Value = oBj.cat_descripcion;
                    oWs.Cells[_fila, 4].Value = oBj.com_descripcion;
                    oWs.Cells[_fila, 5].Value = oBj.mar_descripcion;
                    oWs.Cells[_fila, 6].Value = oBj.pro_descripcion;
                    oWs.Cells[_fila, 7].Value = oBj.pbl_descripcion;
                    oWs.Cells[_fila, 8].Value = oBj.gru_descripcion;

                    using (Image oIm = Image.FromFile(ConfigurationManager.AppSettings["Rutafoto"] + oBj.foto))
                    {
                        if (oIm != null)
                        {
                            oWs.Row(_fila).Height = 100;
                            Drawing.ExcelPicture pic = oWs.Drawings.AddPicture("Lucky-" + _fila, oIm);
                            pic.From.Column = 8;
                            pic.From.Row = _fila - 1;
                            pic.SetSize(110, 110);
                        }
                    }

                    oWs.Row(_fila).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Top;

                    oWs.Column(8).Width = 15;

                    _fila++;
                }

                oWs.Row(1).Height = 25;
                oWs.Row(1).Style.Font.Bold = true;
                oWs.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                oWs.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                oWs.Column(1).AutoFit();
                oWs.Column(2).AutoFit();
                oWs.Column(3).AutoFit();
                oWs.Column(4).AutoFit();
                oWs.Column(5).AutoFit();
                oWs.Column(6).AutoFit();
                oWs.Column(7).AutoFit();
                oWs.Column(8).AutoFit();

                oEx.Save();
            }

            return new ContentResult
            {
                Content = "{ \"_a\": \"" + _server + "\" }",
                ContentType = "application/json"
            };
        }

        #region Descargar BD

        public JsonResult DescargarBD(string _veq, int _voprep, string _vnombre, string _vcategoria, string _vprovincia, string _vciudad, string _vcadena, string _vperiodo)
        {
            DirectoryInfo directory = new DirectoryInfo(Server.MapPath("/Temp").ToString());
            FileInfo[] files = directory.GetFiles("*.xlsx");
            DirectoryInfo[] directories = directory.GetDirectories();

            BL_GES_Operativa oOperativa = new BL_GES_Operativa();

            DataTable TB_Data = null;
            if (_voprep == 1)  // Reporte Publicacion
            {
                TB_Data = oOperativa.BL_NW_Reporting_Ecu_Publicacion_BD(_veq, _vcategoria, _vprovincia, _vciudad, _vcadena, _vperiodo, ((Persona)Session["Session_Login"]).Perfil_id.ToString());
            }
            if (_voprep == 2) // Reporte Promocion
            {
                TB_Data = oOperativa.BL_NW_Reporting_Ecu_Promocion_BD(_veq, _vcategoria, _vprovincia, _vciudad, _vcadena, _vperiodo, ((Persona)Session["Session_Login"]).Perfil_id.ToString());
            }
            if (_voprep == 3) // Reporte Exhibicion
            {
                TB_Data = oOperativa.BL_NW_Reporting_Ecu_Exhibicion_BD(_veq, _vprovincia, _vciudad, _vperiodo, ((Persona)Session["Session_Login"]).Perfil_id.ToString());
            }
            if (_voprep == 4) // Reporte Precio
            {
                TB_Data = oOperativa.BL_NW_Reporting_Ecu_Precio_BD(_veq, _vprovincia, _vciudad, _vperiodo, ((Persona)Session["Session_Login"]).Perfil_id.ToString());
            }
            if (_voprep == 5) // Reporte Portafolio
            {
                TB_Data = oOperativa.BL_NW_Reporting_Ecu_Assorment_BD(_veq, _vprovincia, _vciudad, _vperiodo, ((Persona)Session["Session_Login"]).Perfil_id.ToString());
            }
            if (_voprep == 6) // Reporte Promocion tabla
            {
                TB_Data = oOperativa.BL_NW_Reporting_Ecu_Promocion_BD(_veq, _vcategoria, _vprovincia, _vciudad, _vcadena, _vperiodo, ((Persona)Session["Session_Login"]).Perfil_id.ToString());
            }
            string _fileServer = "";
            string _filePath = "";
            //string _password = "";
            int _fila = 0;

            // _password = "1234";
            _fileServer = String.Format(_vnombre + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            if (_fileNew.Exists)
            {
                _fileNew.Delete();
                _fileNew = new FileInfo(_filePath);
            }

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {

                if (_voprep == 1) // Reporte Publicaciones
                {
                    #region <<< Reporte Publicaciones >>>
                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["nom_cadena"] ascending, _Fila["nom_pdv"] ascending
                                       , _Fila["nom_categoria"] ascending, _Fila["nom_fabricante"] ascending, _Fila["nom_marca"] ascending
                                       select new
                                       {
                                           _Cadena = _Fila["nom_cadena"]
                                           ,_Nom_Pdv = _Fila["nom_pdv"]
                                           ,_Nom_Categoria = _Fila["nom_categoria"]
                                           ,_Nom_Fabricante = _Fila["nom_fabricante"]
                                           ,_Nom_Marca = _Fila["nom_marca"]
                                           ,_Nom_Producto = _Fila["nom_producto"]
                                           ,_Nom_Publicacion = _Fila["nom_publicacion"]
                                           ,_Grupo_obj = _Fila["grupo_objetivo"]
                                           ,_Nom_Foto = _Fila["nombrefoto"]
                                       }
                                       ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte Publicacion");
                        oWsReporte.Cells[1, 1].Value = "Cadena";
                        oWsReporte.Cells[1, 2].Value = "PDV";
                        oWsReporte.Cells[1, 3].Value = "Categoria";
                        oWsReporte.Cells[1, 4].Value = "Fabricante";
                        oWsReporte.Cells[1, 5].Value = "Marca";
                        oWsReporte.Cells[1, 6].Value = "Producto";
                        oWsReporte.Cells[1, 7].Value = "Publicacion";
                        oWsReporte.Cells[1, 8].Value = "Grupo Objetivo";
                        oWsReporte.Cells[1, 9].Value = "Nombre Foto";

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Cadena;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._Nom_Pdv;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Nom_Categoria;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Nom_Fabricante;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Nom_Marca;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Nom_Producto;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Nom_Publicacion;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Grupo_obj;
                            oWsReporte.Cells[_fila, 9].Value = _Datos._Nom_Foto;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);
                        oWsReporte.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    }
                    #endregion

                }

                if (_voprep == 2) // Reporte Promociones
                {
                    #region <<< Reporte Promociones >>>
                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["nom_cadena"] ascending, _Fila["nom_pdv"] ascending
                                       , _Fila["nom_categoria"] ascending, _Fila["nom_fabricante"] ascending, _Fila["nom_marca"] ascending
                                       select new
                                       {
                                           _Anio = _Fila["Año"]
                                           ,_Mes = _Fila["Mes"]
                                           ,
                                           _Nom_Periodo = _Fila["nom_periodo"]
                                           ,
                                           _Nom_Provincia = _Fila["nom_provincia"]
                                           ,
                                           _Nom_Ciudad = _Fila["nom_ciudad"]
                                           ,
                                           _Fecha_Ini = _Fila["fecha_ini"]
                                           ,
                                           _Nom_cadena = _Fila["nom_cadena"]
                                           ,
                                           _Nom_Pdv = _Fila["nom_pdv"]
                                           ,
                                           _Nom_Categoria = _Fila["nom_categoria"]
                                           ,
                                           _Nom_Fabricante = _Fila["nom_fabricante"]
                                           ,
                                           _Nom_Marca = _Fila["nom_marca"]
                                           ,
                                           _Nom_Producto = _Fila["nom_producto"]
                                           ,
                                           _Actividad = _Fila["actividad"]
                                           ,
                                           _grupo_objetivo = _Fila["grupo_objetivo"]
                                           ,
                                           _Fin_Actividad = _Fila["fin_actividad"]
                                           ,
                                           _Vigente = _Fila["Vigente"]
                                           ,
                                           _Obsequio = _Fila["Obsequio"]
                                           ,
                                           _Mecanica = _Fila["mecanica"]
                                           ,_Material_Apoyo = _Fila["material_apoyo"]
                                           ,
                                           _Impacto_Ventas = _Fila["impactoventas"]
                                           ,
                                           _Nombre_Foto = _Fila["nombrefoto"]
                                       }
                                       ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte Promocion");
                        oWsReporte.Cells[1, 1].Value = "Año";
                        oWsReporte.Cells[1, 2].Value = "Mes";
                        oWsReporte.Cells[1, 3].Value = "Periodo";
                        oWsReporte.Cells[1, 4].Value = "Provincia";
                        oWsReporte.Cells[1, 5].Value = "Ciudad";
                        oWsReporte.Cells[1, 6].Value = "Inicio Actividad";
                        oWsReporte.Cells[1, 7].Value = "Cadena";
                        oWsReporte.Cells[1, 8].Value = "PDV";
                        oWsReporte.Cells[1, 9].Value = "Categoria";
                        oWsReporte.Cells[1, 10].Value = "Fabricante";
                        oWsReporte.Cells[1, 11].Value = "Marca";
                        oWsReporte.Cells[1, 12].Value = "Producto";
                        oWsReporte.Cells[1, 13].Value = "Actividad";
                        oWsReporte.Cells[1, 14].Value = "Grupo Objetivo";
                        oWsReporte.Cells[1, 15].Value = "Fin Actividad";
                        oWsReporte.Cells[1, 16].Value = "Vigente";
                        oWsReporte.Cells[1, 17].Value = "Obsequio";
                        oWsReporte.Cells[1, 18].Value = "Mecanica";
                        oWsReporte.Cells[1, 19].Value = "Material Apoyo";
                        oWsReporte.Cells[1, 20].Value = "Impacto Ventas";
                        oWsReporte.Cells[1, 21].Value = "Nombre Foto";
                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Anio;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._Mes;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Nom_Periodo;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Nom_Provincia;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Nom_Ciudad;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Fecha_Ini;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Nom_cadena;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Nom_Pdv;
                            oWsReporte.Cells[_fila, 9].Value = _Datos._Nom_Categoria;
                            oWsReporte.Cells[_fila, 10].Value = _Datos._Nom_Fabricante;
                            oWsReporte.Cells[_fila, 11].Value = _Datos._Nom_Marca;
                            oWsReporte.Cells[_fila, 12].Value = _Datos._Nom_Producto;
                            oWsReporte.Cells[_fila, 13].Value = _Datos._Actividad;
                            oWsReporte.Cells[_fila, 14].Value = _Datos._grupo_objetivo;
                            oWsReporte.Cells[_fila, 15].Value = _Datos._Fin_Actividad;
                            oWsReporte.Cells[_fila, 16].Value = _Datos._Vigente;
                            oWsReporte.Cells[_fila, 17].Value = _Datos._Obsequio;
                            oWsReporte.Cells[_fila, 18].Value = _Datos._Mecanica;
                            oWsReporte.Cells[_fila, 19].Value = _Datos._Material_Apoyo;
                            oWsReporte.Cells[_fila, 20].Value = _Datos._Impacto_Ventas;
                            oWsReporte.Cells[_fila, 21].Value = _Datos._Nombre_Foto;

                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();
                        oWsReporte.Column(10).AutoFit();
                        oWsReporte.Column(11).AutoFit();
                        oWsReporte.Column(12).AutoFit();
                        oWsReporte.Column(13).AutoFit();
                        oWsReporte.Column(14).AutoFit();
                        oWsReporte.Column(15).AutoFit();
                        oWsReporte.Column(16).AutoFit();
                        oWsReporte.Column(17).AutoFit();
                        oWsReporte.Column(18).AutoFit();
                        oWsReporte.Column(19).AutoFit();
                        oWsReporte.Column(20).AutoFit();
                        oWsReporte.Column(21).AutoFit();

                        

                        oWsReporte.View.FreezePanes(2, 1);

                        oWsReporte.SelectedRange[1, 1, 1, 21].AutoFilter = true;

                    }
                    #endregion
                }
                if (_voprep == 3) // Reporte Exhibiciones
                {
                    #region <<< Reporte Exhibicion >>>
                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["nom_cadena"] ascending, _Fila["nom_pdv"] ascending
                                       , _Fila["nom_categoria"] ascending, _Fila["nom_empresa"] ascending, _Fila["nom_marca"] ascending
                                       select new
                                       {
                                           _Anio = _Fila["Año"]
                                           ,
                                           _Mes = _Fila["Mes"]
                                           ,
                                           _Nom_Periodo = _Fila["nom_periodo"]
                                           ,
                                           _Nom_Ciudad = _Fila["nom_ciudad"]
                                           ,
                                           _Nom_Cadena = _Fila["nom_cadena"]
                                           ,
                                           _Nom_PDV= _Fila["nom_pdv"]
                                           ,
                                           _Nom_Categoria = _Fila["nom_categoria"]
                                           ,
                                           _Nom_Empresa = _Fila["nom_empresa"]
                                           ,
                                           _Nom_Marca = _Fila["nom_marca"]
                                           ,
                                           _Nom_Exhibicion = _Fila["nom_exhibicion"]
                                           ,
                                           _Exhibicion = _Fila["exhibicion"]
                                           ,
                                           _Nom_Zona = _Fila["Zona"]
                                           ,
                                           _Nom_Comentario = _Fila["comentario"]

                                       }
                                        ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte Exhibicion");
                        oWsReporte.Cells[1, 1].Value = "Año";
                        oWsReporte.Cells[1, 2].Value = "Mes";
                        oWsReporte.Cells[1, 3].Value = "Periodo";
                        oWsReporte.Cells[1, 4].Value = "Ciudad";
                        oWsReporte.Cells[1, 5].Value = "Cadena";
                        oWsReporte.Cells[1, 6].Value = "PDV";
                        oWsReporte.Cells[1, 7].Value = "Categoria";
                        oWsReporte.Cells[1, 8].Value = "Empresa";
                        oWsReporte.Cells[1, 9].Value = "Marca";
                        oWsReporte.Cells[1, 10].Value = "Exhibicion";
                        oWsReporte.Cells[1, 11].Value = "Valor";
                        oWsReporte.Cells[1, 12].Value = "Zona";
                        oWsReporte.Cells[1, 13].Value = "Comentario";

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Anio;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._Mes;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Nom_Periodo;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Nom_Ciudad;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Nom_Cadena;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Nom_PDV;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Nom_Categoria;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Nom_Empresa;
                            oWsReporte.Cells[_fila, 9].Value = _Datos._Nom_Marca;
                            oWsReporte.Cells[_fila, 10].Value = _Datos._Nom_Exhibicion;
                            oWsReporte.Cells[_fila, 11].Value = _Datos._Exhibicion;
                            oWsReporte.Cells[_fila, 12].Value = _Datos._Nom_Zona;
                            oWsReporte.Cells[_fila, 13].Value = _Datos._Nom_Comentario;
                           
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();
                        oWsReporte.Column(10).AutoFit();
                        oWsReporte.Column(11).AutoFit();
                        oWsReporte.Column(12).AutoFit();
                        oWsReporte.Column(13).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);
                        oWsReporte.SelectedRange[1, 1, 1, 13].AutoFilter = true;
                    }
                    #endregion
                }
                if (_voprep == 4) // Reporte Precio
                {
                    #region <<< Reporte Precio >>>
                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["nom_cadena"] ascending, _Fila["pdv"] ascending
                                       , _Fila["categoria"] ascending, _Fila["prop_compe"] ascending, _Fila["marca"] ascending
                                       select new
                                       {
                                           _Periodo = _Fila["periodo"]
                                           ,
                                           _Canal = _Fila["canal"]
                                           ,
                                           _Nom_Cadena = _Fila["nom_cadena"]
                                           ,
                                           _Nom_PDV = _Fila["PDV"]
                                           ,
                                           _Nom_Region = _Fila["region"]
                                           ,
                                           _Nom_Provincia = _Fila["provincia"]
                                           ,
                                           _Nom_Ciudad = _Fila["Ciudad"]
                                           ,
                                           _Nom_Empresa = _Fila["Fabricante"]
                                           ,
                                           _Prop_Compe = _Fila["prop_compe"]
                                           ,
                                           _Nom_Categoria = _Fila["categoria"]
                                           ,
                                           _Nom_Marca = _Fila["marca"]
                                           ,
                                           _Nom_Producto = _Fila["producto"]
                                           ,
                                           _Nom_Presencia = _Fila["presencia"]
                                           ,
                                           _PVC = _Fila["PVC"]

                                       }
                                        ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte Precio");
                        oWsReporte.Cells[1, 1].Value = "Periodo";
                        oWsReporte.Cells[1, 2].Value = "Canal";
                        oWsReporte.Cells[1, 3].Value = "Cadena";
                        oWsReporte.Cells[1, 4].Value = "PDV";
                        oWsReporte.Cells[1, 5].Value = "Region";
                        oWsReporte.Cells[1, 6].Value = "Provincia";
                        oWsReporte.Cells[1, 7].Value = "Ciudad";
                        oWsReporte.Cells[1, 8].Value = "Fabricante";
                        oWsReporte.Cells[1, 9].Value = "Prop-Compe";
                        oWsReporte.Cells[1, 10].Value = "Categoria";
                        oWsReporte.Cells[1, 11].Value = "Marca";
                        oWsReporte.Cells[1, 12].Value = "Producto";
                        oWsReporte.Cells[1, 13].Value = "Presencia";
                        oWsReporte.Cells[1, 14].Value = "PVC";

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Periodo;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._Canal;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Nom_Cadena;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Nom_PDV;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Nom_Region;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Nom_Provincia;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Nom_Ciudad;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Nom_Empresa;
                            oWsReporte.Cells[_fila, 9].Value = _Datos._Prop_Compe;
                            oWsReporte.Cells[_fila, 10].Value = _Datos._Nom_Categoria;
                            oWsReporte.Cells[_fila, 11].Value = _Datos._Nom_Marca;
                            oWsReporte.Cells[_fila, 12].Value = _Datos._Nom_Producto;
                            oWsReporte.Cells[_fila, 13].Value = _Datos._Nom_Presencia;
                            oWsReporte.Cells[_fila, 14].Value = _Datos._PVC;

                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();
                        oWsReporte.Column(10).AutoFit();
                        oWsReporte.Column(11).AutoFit();
                        oWsReporte.Column(12).AutoFit();
                        oWsReporte.Column(13).AutoFit();
                        oWsReporte.Column(14).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);
                        oWsReporte.SelectedRange[1, 1, 1, 14].AutoFilter = true;
                    }
                    #endregion
                }
                if (_voprep == 5) // Reporte Portafolio
                {
                    #region <<< Reporte Portafolio >>>
                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["nom_cadena"] ascending, _Fila["nom_ciudad"] ascending
                                       , _Fila["nom_pdv"] ascending, _Fila["nom_categoria"] ascending, _Fila["nom_marca"] ascending
                                       select new
                                       {
                                           _Anio = _Fila["anio"]
                                           ,
                                           _Mes = _Fila["mes"]
                                           ,
                                           _Nom_Cadena = _Fila["nom_cadena"]
                                           ,
                                           _Nom_ciudad = _Fila["nom_ciudad"]
                                           ,
                                           _Nom_PDV = _Fila["nom_pdv"]
                                           ,
                                           _Nom_Categoria = _Fila["nom_categoria"]
                                           ,
                                           _Nom_Marca = _Fila["nom_marca"]
                                           ,
                                           _Nom_Empresa = _Fila["fabricante"]
                                           ,
                                           _Nom_Producto = _Fila["nom_producto"]
                                           ,
                                           _Presencia = _Fila["Presencia"]                                          

                                       }
                                        ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte Portafolio");
                        oWsReporte.Cells[1, 1].Value = "Año";
                        oWsReporte.Cells[1, 2].Value = "Mes";
                        oWsReporte.Cells[1, 3].Value = "Cadena";
                        oWsReporte.Cells[1, 4].Value = "Ciudad";
                        oWsReporte.Cells[1, 5].Value = "PDV";
                        oWsReporte.Cells[1, 6].Value = "Categoria";
                        oWsReporte.Cells[1, 7].Value = "Marca";
                        oWsReporte.Cells[1, 8].Value = "Fabricante";
                        oWsReporte.Cells[1, 9].Value = "Producto";
                        oWsReporte.Cells[1, 10].Value = "Presencia";                        

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Anio;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._Mes;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Nom_Cadena;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Nom_ciudad;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Nom_PDV;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Nom_Categoria;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Nom_Marca;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Nom_Empresa;
                            oWsReporte.Cells[_fila, 9].Value = _Datos._Nom_Producto;
                            oWsReporte.Cells[_fila, 10].Value = _Datos._Presencia;                            

                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();
                        oWsReporte.Column(10).AutoFit();                        

                        oWsReporte.View.FreezePanes(2, 1);
                        oWsReporte.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    }
                    #endregion
                }

                if (_voprep == 6) // Reporte Promocion Tabla
                {
                    #region <<< Reporte Promociones >>>
                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["nom_cadena"] ascending, _Fila["nom_pdv"] ascending
                                       , _Fila["nom_categoria"] ascending, _Fila["nom_fabricante"] ascending, _Fila["nom_marca"] ascending
                                       select new
                                       {
           
                                           _Nom_cadena = _Fila["nom_cadena"]
                                           ,
                                           _Nom_Pdv = _Fila["nom_pdv"]
                                           ,
                                           _Nom_Marca = _Fila["nom_marca"]
                                           ,
                                           _Nom_Producto = _Fila["nom_producto"]                                         
                                       }
                                       ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte Promocion");
                        oWsReporte.Cells[1, 1].Value = "Cadena";
                        oWsReporte.Cells[1, 2].Value = "PDV";
                        oWsReporte.Cells[1, 3].Value = "Marca";
                        oWsReporte.Cells[1, 4].Value = "Producto";
                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Nom_cadena;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._Nom_Pdv;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Nom_Marca;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Nom_Producto;
                            
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                       
                        oWsReporte.View.FreezePanes(2, 1);

                        oWsReporte.SelectedRange[1, 1, 1, 4].AutoFilter = true;

                    }
                    #endregion
                }
                oEx.Save();
            }


            if (TB_Data.Rows.Count <= 0 || TB_Data == null)
            {
                _fileServer = "";
            }

            return Json(new { Archivo = _fileServer });//_fileServer });
        } 
        

        #endregion
    }
}
