﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.BackusReporting;
using Lucky.Xplora.Models.Reporting;
using Lucky.Xplora.Models.Administrador;
//using NetOffice;
//using PowerPoint = NetOffice.PowerPointApi;
//using NetOffice.PowerPointApi.Enums;
//using NetOffice.OfficeApi.Enums;
using Lucky.Business.Common.Servicio;
using System.Data;
using Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.IO.MemoryMappedFiles;

//using DocumentFormat.OpenXml;
//using DocumentFormat.OpenXml.Drawing;
//using DocumentFormat.OpenXml.Office2010.Drawing;
//using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Presentation;
//using A = DocumentFormat.OpenXml.Drawing;
//using P = DocumentFormat.OpenXml.Presentation;
using System.Web.Script.Serialization;

using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using Lucky.Xplora.Security;
using System.Web.Security;
//using Lucky.Xplora.Models.Map;

namespace Lucky.Xplora.Controllers
{
    public class BackusReportingController : Controller
    {
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        string LocalFormat = Convert.ToString(ConfigurationManager.AppSettings["LocalFormat"]);

        //
        // GET: /BackusFotografico/
        public BackusReportingController() { 
            
        }

        #region RQ Lucky 256 v1.0

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-26
        /// </summary>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult Fotografico(string _a)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Convert.ToInt32(ViewBag.compania) == 1637 && _a != "9020110152013" && _a != "9020116052016")
            {
                return RedirectToAction("Fotografico", "BackusReporting", new { _a = "9020110152013" });
            }

            if (_a != "")
            {
                ViewBag.campania = _a;
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = _a }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            //if (Request["_a"] != null)
            //{
            //    ViewBag.campania = Request["_a"];
            //    ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion;
            //}

            return View();
        }

        [HttpPost]
        public ActionResult GetTipoCanal(string __a)
        {
            return View(
                    new Tipo_Canal().Lista(
                        new Request_GetTipoCanal_Campania()
                        {
                            campania = __a
                        }
                    )
                );
        }
        
        [HttpPost]
        public ActionResult GetCiudades()
        {
            return Json(new Lucky.Xplora.Models.BackusPlanning.Ciudad().ListaCiudades());
        }


        [HttpPost]
        public ActionResult GetFotoOficina(string __a)
        {

            return Json(new Lucky.Xplora.Models.BackusPlanning.Ciudad().oLstOficina(new Lucky.Xplora.Models.BackusPlanning.Request_GetCiudad { Cod_Campania = __a }));
        }

        [HttpPost]
        public ActionResult GetMes(string __a, string __b)
        {
            return View(
                    new Lucky.Xplora.Models.Mes().Lista(
                        new Request_GetMes_Campania_Mes()
                        {
                            campania = __a,
                            anio = Convert.ToInt32(__b)
                        }
                    )
                );
        }

        [HttpPost]
        public ActionResult GetMesI(string __a, string __b, string __c, string __d, string __e)
        {
            return PartialView(
                "GetMes",
                    new Lucky.Xplora.Models.Mes().Lista(
                        new Request_GetMes_Campania_Reporte_Anio_Formato_TipoPerfil()
                        {
                            campania = __a,
                            reporte = Convert.ToInt32(__b),
                            anio = Convert.ToInt32(__c),
                            formato = Convert.ToInt32(__d),
                            tipo_perfil = Convert.ToInt32(__e),
                        }
                    )
                );
        }

        [HttpPost]
        public ActionResult GetPeriodo(string __a, string __b, string __c)
        {
            return PartialView(
                "../ColgateReporting/GetPeriodo",
                new Periodo().Lista(
                        new Periodo_Por_Planning_Reports_Anio_Mes_Request()
                        {
                            campania = __a,
                            reporte = 23,
                            anio = Convert.ToInt32(__b),
                            mes = Convert.ToInt32(__c)
                        }
                    ));
        }

        [HttpPost]
        public ActionResult GetPeriodoI(string __a, string __b, string __c, string __d)
        {
            return PartialView(
                "../ColgateReporting/GetPeriodo",
                new Periodo().Lista(
                        new Request_GetPeriodo_Campania_Reporte_Anio_Mes_Formato()
                        {
                            campania = __a,
                            reporte = 163,
                            anio = Convert.ToInt32(__b),
                            mes = Convert.ToInt32(__c),
                            formato = Convert.ToInt32(__d)
                        }
                    ));
        }

        [HttpPost]
        public ActionResult GetPeriodoII(string __a, string __b, string __c, string __d, string __e)
        {
            return PartialView(
                "../ColgateReporting/GetPeriodo",
                new Periodo().Lista(
                        new Request_GetPeriodo_Campania_Reporte_Anio_Mes_Formato()
                        {
                            campania = __a,
                            reporte = Convert.ToInt32(__b),
                            anio = Convert.ToInt32(__c),
                            mes = Convert.ToInt32(__d),
                            formato = Convert.ToInt32(__e)
                        }
                    ));
        }

        [HttpPost]
        public ActionResult GetPeriodoIII(string __a, string __b, string __c, string __d, string __e, string __f)
        {
            return PartialView(
                "../ColgateReporting/GetPeriodo",
                new Periodo().Lista(
                        new Request_GetPeriodo_Campania_Reporte_Anio_Mes_Formato_TipoPerfil()
                        {
                            campania = __a,
                            reporte = Convert.ToInt32(__b),
                            anio = Convert.ToInt32(__c),
                            mes = Convert.ToInt32(__d),
                            formato = Convert.ToInt32(__e),
                            tipo_perfil = Convert.ToInt32(__f)
                        }
                    ));
        }

        [HttpPost]
        public ActionResult GetFiltro(int __a, string __b)
        {
            return Json(new Lucky.Xplora.Models.Map.New_XplSgGps_Filtro_Service().Filtro(
                        new Lucky.Xplora.Models.Map.Resquest_New_XPL_SG_GPS()
                        {
                            opcion = __a,
                            parametros = __b
                        }
                    ), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCategoria(string __a)
        {
            return PartialView(
                "../Alicorp/GetCategoria",
                new Categoria().Lista(
                        new Listar_Categoria_Por_CodCampania_y_CodReporte_Request()
                        {
                            campania = __a,
                            reporte = 23
                        }
                    ));
        }

        [HttpPost]
        public ActionResult GetNodoComercial(string __a, string __b)
        {
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            return PartialView(
                "../Alicorp/GetNodoComercial",
                new NodoComercial().Lista(
                        new Request_GetCadena_Campania_TipoCanal()
                        {
                            campania = __a,
                            tipocanal = Convert.ToInt32(__b)
                        }
                    ));
        }

        [HttpPost]
        public ActionResult GetPuntoVenta(string __a, string __b, string __c, string __d, string __e)
        {
            if (((Persona)Session["Session_Login"]).Company_id == 1561 || ((Persona)Session["Session_Login"]).Company_id == 1646)
            {
                return Json(
                    new PuntoVenta().Lista(
                            new Request_GetPuntoVenta_Campania_TipoCanal_NodoComercial()
                            {
                                campania = __a,
                                tipocanal = Convert.ToInt32(__b),
                                nodocomercial = Convert.ToInt32(__c),
                                codCiudad = __d,
                                codProvincia = __e
                            }
                   ));
            }
            return PartialView(
                    "../Alicorp/GetPuntoVenta",
                    new PuntoVenta().Lista(
                            new Request_GetPuntoVenta_Campania_TipoCanal_NodoComercial()
                            {
                                campania = __a,
                                tipocanal = Convert.ToInt32(__b),
                                nodocomercial = Convert.ToInt32(__c),
                                codCiudad = __d,
                                codProvincia = __e
                            }
                        ));
        }

        [HttpPost]
        public ActionResult GetGaleriaFotos(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m, string __n, string __o, string __p, string __q)
        {
            return View(
                    new Fotografico().Lista(new Request_Fotografico()
                    {
                        campania = __a,
                        tipocanal = Convert.ToInt32(__b),
                        anio = Convert.ToInt32(__c),
                        mes = Convert.ToInt32(__d),
                        periodo = Convert.ToInt32(__e),
                        nodocomercial = Convert.ToInt32(__f),
                        categoria = Convert.ToInt32(__g),
                        puntoventa = __h,
                        dia=Convert.ToInt32(__i),
                        ciudad=__j,
                        departamento=__k,
                        fechaInicio = __l,
                        fechaFin = __m,
                        dex = __n,
                        supervisor = __o,
                        gestor = __p,
                        programa = __q
                    })
                );
        }

        [HttpPost]
        public JsonResult DescargaArchivo(string __a, string __b)
        {
            string error = "";
            string _filePath = "";
            string _fileServer = "";
            string _fileFormatServer = "";
            int iCompania = ((Persona)Session["Session_Login"]).Company_id;
            string extension = __b;
            string textoCampania = Convert.ToString(Request.QueryString["__campania"]);

            try
            {
                List<Fotografico> oLs = MvcApplication._Deserialize<List<Fotografico>>(__a);

                _fileServer = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);
                if (iCompania == 1646)
                {
                    _fileFormatServer = System.IO.Path.Combine(LocalFormat, "lucky-Formato-Fotografico-aje.pptx");
                }
                else if (iCompania == 1637)
                {
                    _fileFormatServer = System.IO.Path.Combine(LocalFormat, "lucky-Formato-Fotografico-Backus.pptx");
                }
                else
                {
                    _fileFormatServer = System.IO.Path.Combine(LocalFormat, "lucky-Formato-Fotografico.pptx");
                }
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                /*copia formato y lo ubica en carpeta temporal*/
                System.IO.File.Copy(_fileFormatServer, _filePath + ".pptx", true);

                //using (PresentationDocument Pre = PresentationDocument.Open(_filePath + ".pptx", true))
                //{
                //    foreach (Fotografico oBj in oLs)
                //    {
                //        // Get the presentation Part of the presentation document 
                //        PresentationPart presentationPart = Pre.PresentationPart;
                //        Slide slide = new InsertImage().InsertSlide(presentationPart, "Diseño personalizado");

                //        new InsertImage().InsertImageInLastSlide(slide, ConfigurationManager.AppSettings["Rutafoto"] + oBj.archivo, "image/jpg");
                //        slide.Save();
                //    }

                //    Pre.PresentationPart.Presentation.Save();
                //}

                PowerPoint.Application App = new PowerPoint.Application();
                PowerPoint.Presentation Pre = App.Presentations.Open(_filePath + ".pptx", MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);

                if (iCompania == 1646)
                {
                    PowerPoint.Slide slidePortada = Pre.Slides[1];
                    slidePortada.Shapes["CANAL_AJE"].TextFrame.TextRange.Text = textoCampania;
                }

                foreach (Fotografico oBj in oLs)
                {
                    PowerPoint.Slide Sli;

                    if (iCompania == 1646)
                    {
                        Sli = Pre.Slides.Add(Pre.Slides.Count + 1, PowerPoint.PpSlideLayout.ppLayoutPictureWithCaption);

                        PowerPoint.Shape Sha = Sli.Shapes[2];
                        Sli.Shapes.AddPicture(ConfigurationManager.AppSettings["Rutafoto_reporte"] + oBj.archivo, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                        Sli.Shapes[1].TextFrame.TextRange.Text = oBj.pdv_razonsocial;
                        Sli.Shapes[3].TextFrame.TextRange.Text = "Cod. PDV: " + oBj.pdv_codigo + (char)13 + @"Dirección: " + oBj.pdv_direccion + " - " + oBj.prv_descripcion + @" \ " + oBj.dis_descripcion + (char)13 + @"Fecha\Hora: " + oBj.fecha + " " + oBj.hora;
                        //PowerPoint.CustomLayout customLayout = Pre.SlideMaster.CustomLayouts["AJE"];
                        //Sli.Shapes["IMAGEN_AJE"].(ConfigurationManager.AppSettings["Rutafoto_reporte"] + oBj.archivo, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);

                        ////PowerPoint.CustomLayout customLayout = Pre.SlideMaster.CustomLayouts["AJE"];
                        ////Sli.Shapes["IMAGEN_AJE"].(ConfigurationManager.AppSettings["Rutafoto_reporte"] + oBj.archivo, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                        //Sli.Shapes["CATEGORIA_AJE"].TextFrame.TextRange.Text = oBj.cat_descripcion;
                        //Sli.Shapes["TITULO_AJE"].TextFrame.TextRange.Text = oBj.pdv_razonsocial;
                        //Sli.Shapes["DESCRIPCION_AJE"].TextFrame.TextRange.Text = "Cod. PDV: " + oBj.pdv_codigo + (char)13 + @"Dirección: " + oBj.pdv_direccion + " - " + oBj.prv_descripcion + @" \ " + oBj.dis_descripcion + (char)13 + @"Fecha\Hora: " + oBj.fecha + " " + oBj.hora;
                    }
                    else if (iCompania == 1637) 
                    {
                        Sli = Pre.Slides.Add(Pre.Slides.Count + 1, PowerPoint.PpSlideLayout.ppLayoutPictureWithCaption);
                        PowerPoint.Shape Sha = Sli.Shapes[2];
                        Sli.Shapes.AddPicture(ConfigurationManager.AppSettings["Rutafoto_reporte"] + oBj.archivo, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                        Sli.Shapes[1].TextFrame.TextRange.Text = oBj.pdv_razonsocial;
                        Sli.Shapes[3].TextFrame.TextRange.Text = "Cod. Cliente: " + oBj.TMPPdvCod + (char)13 + @"Nombre Cliente: " + oBj.TMPPdvDescripcion + (char)13 + @"Ciudad - Distrito: " + oBj.TMPCiudadDescripcion + (char)13 + @"Comentarios: " + oBj.comentario;
                    }
                    else
                    {
                        Sli = Pre.Slides.Add(Pre.Slides.Count + 1, PowerPoint.PpSlideLayout.ppLayoutPictureWithCaption);
                        PowerPoint.Shape Sha = Sli.Shapes[2];
                        Sli.Shapes.AddPicture(ConfigurationManager.AppSettings["Rutafoto_reporte"] + oBj.archivo, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                        //Sli.Shapes.AddPicture(ConfigurationManager.AppSettings["Rutafoto"] + "slide.jpg", MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                        Sli.Shapes[1].TextFrame.TextRange.Text = oBj.pdv_razonsocial;
                        Sli.Shapes[3].TextFrame.TextRange.Text = "Cod. PDV: " + oBj.pdv_codigo + (char)13 + @"Dirección: " + oBj.pdv_direccion + " - " + oBj.prv_descripcion + @" \ " + oBj.dis_descripcion + (char)13 + @"Fecha\Hora: " + oBj.fecha + " " + oBj.hora;
                    }

                    //Sli.Shapes[2].ScaleHeight(1, MsoTriState.msoTrue);
                    //Sli.Shapes[2].ScaleWidth(1, MsoTriState.msoTrue);
                }

                if (extension == ".pdf")
                {
                    Pre.ExportAsFixedFormat(_filePath + ".pdf", PowerPoint.PpFixedFormatType.ppFixedFormatTypePDF, PowerPoint.PpFixedFormatIntent.ppFixedFormatIntentScreen);
                }

                Pre.Save();
            }
            catch (Exception e)
            {
                error = e.Message;
            }


            return Json(new { Archivo = _fileServer, Error = error });
        }

        #endregion

        #region Publicacion(Encarte)

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-30
        /// </summary>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult Publicacion()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            if (Request["_a"] == null)
            {
                return RedirectToAction("Publicacion", "Publicacion");
            }
            else
            {
                return RedirectToAction("Publicacion", "Publicacion", new { _a = Request["_a"] });
            }
        }

        #endregion

        #region RQ Lucky 261 v1.0

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-14
        /// </summary>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult Reporting()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.Tipo_Perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != "")
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = ViewBag.campania }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-14
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonStockOut(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new StockOut().Objeto(
                    new Request_StockOut()
                    {
                        campania = __a,
                        anio = Convert.ToInt32(__b),
                        mes = Convert.ToInt32(__c),
                        periodo = Convert.ToInt32(__d),
                        tipo_canal = Convert.ToInt32(__e),
                        cadena = Convert.ToInt32(__f),
                        categoria = Convert.ToInt32(__g),
                        nivel = __h,
                        marca = Convert.ToInt32(__i)
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-22
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <param name="__m"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonPrecio(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Precio().Objeto(
                    new Request_Precio()
                    {
                        campania = __a,
                        anio = Convert.ToInt32(__b),
                        mes = Convert.ToInt32(__c),
                        periodo = Convert.ToInt32(__d),
                        tipo_canal = Convert.ToInt32(__e),
                        cadena = Convert.ToInt32(__f),
                        categoria = Convert.ToInt32(__g),
                        nivel = __h,
                        marca = Convert.ToInt32(__i),
                        presentacion = Convert.ToInt32(__j),
                        envase = __k,
                        propio = __l,
                        competencia = __m
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonParticipacion(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Participacion().Objeto(
                    new Request_Participacion()
                    {
                        campania = __a,
                        anio = Convert.ToInt32(__b),
                        mes = Convert.ToInt32(__c),
                        periodo = Convert.ToInt32(__d),
                        tipo_canal = Convert.ToInt32(__e),
                        cadena = Convert.ToInt32(__f),
                        categoria = Convert.ToInt32(__g),
                        cerveza = Convert.ToInt32(__h)
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonParticipacionEvolutivo(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Participacion_Evolutivo().Objeto(
                    new Request_Participacion_Evolutivo()
                    {
                        campania = __a,
                        anio = Convert.ToInt32(__b),
                        mes = Convert.ToInt32(__c),
                        periodo = Convert.ToInt32(__d),
                        tipo_canal = Convert.ToInt32(__e),
                        cadena = Convert.ToInt32(__f),
                        categoria = Convert.ToInt32(__g),
                        nivel = __h,
                        marca = Convert.ToInt32(__i),
                        tipo_material = Convert.ToInt32(__j)
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult ExhibicionAdicional(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, int __i)
        {
            return View(new Exhibicion_Adicional().Lista(
                    new Request_Additional_Exhibition()
                    {
                        campania = __a,
                        anio = Convert.ToInt32(__b),
                        mes = Convert.ToInt32(__c),
                        periodo = Convert.ToInt32(__d),
                        tipo_canal = Convert.ToInt32(__e),
                        cadena = Convert.ToInt32(__f),
                        categoria = Convert.ToInt32(__g),
                        nivel = __h,
                        cod_marca = __i
                    }));
        }

        public ActionResult ExhibicionAdicionalMiles(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, int __i)
        {
            return View(new Exhibicion_Adicional().Lista(
                    new Request_Additional_Exhibition()
                    {
                        campania = __a,
                        anio = Convert.ToInt32(__b),
                        mes = Convert.ToInt32(__c),
                        periodo = Convert.ToInt32(__d),
                        tipo_canal = Convert.ToInt32(__e),
                        cadena = Convert.ToInt32(__f),
                        categoria = Convert.ToInt32(__g),
                        nivel = __h,
                        cod_marca = __i
                    }));
        }

        public ActionResult JsonExhibicionAdicional(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, int __i)
        {
            List<Exhibicion_Adicional> oLs = new Exhibicion_Adicional().Lista(
                    new Request_Additional_Exhibition()
                    {
                        campania = __a,
                        anio = Convert.ToInt32(__b),
                        mes = Convert.ToInt32(__c),
                        periodo = Convert.ToInt32(__d),
                        tipo_canal = Convert.ToInt32(__e),
                        cadena = Convert.ToInt32(__f),
                        categoria = Convert.ToInt32(__g),
                        nivel = __h,
                        cod_marca = __i
                    });
            return Json(new { Archivo = oLs });
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-23
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetProductoFamilia(string __a, string __b)
        {
            return View(
                new Producto_Familia().Lista(new Request_GetProductoFamilia_Campania_Reporte_Categoria()
                {
                    campania = __a,
                    reporte = 163,
                    categoria = Convert.ToInt32(__b)
                }));
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-23
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetMarca(string __a, string __b, string __c)
        {
            return PartialView(
                "../Alicorp/GetMarca",
                    new Marca().Lista(new Request_GetMarca_Campania_Reporte_Categoria_ProductoFamilia() { campania = __a, reporte = 163, categoria = Convert.ToInt32(__b), producto_familia = __c })
                );
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-24
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetEnvase(string __a, string __b)
        {
            return View(
                    new Envase().Lista(new Request_GetEnvase_Opcion_Parametro() { opcion = Convert.ToInt32(__a), parametro = __b })
                );
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-27
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetMProducto(string __a, string __b, string __c, string __d, string __e, string __f, string __g)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Producto().Lista(new Request_GetProducto_Campania_Compania_Categoria_ProductoFamilia_Marca_Presentacion_Envase()
                {
                    campania = __a,
                    compania = Convert.ToInt32(__b),
                    categoria = Convert.ToInt32(__c),
                    producto_familia = __d,
                    marca = Convert.ToInt32(__e),
                    presentacion = Convert.ToInt32(__f),
                    envase = __g
                }
                )),
                ContentType = "application/json"
            }.Content);
        }

        #endregion

        #region <<< Xplora Reporting Backus >>>
        [CustomAuthorize]
        public ActionResult OutofStockProducto()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_canal = "25";
            oParametros.Cod_Reporte = 150;

            return View(new E_Nw_Rp_Backus_Param_Iniciales().Consul_Inicial(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        [CustomAuthorize]
        public ActionResult PresenciaProducto()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_canal = "25";
            oParametros.Cod_Reporte = 150;

            return View(new E_Nw_Rp_Backus_Param_Iniciales().Consul_Inicial(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_LineStockOutxCadena(string _vperfil, int _vperiodo, int _vcanal, int _vcadena, string _vnivel, int _vmarca, string _vcategoria, string _vproducto)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();

            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString(); //_vperfil;//"170935";
            oParametros.Cod_Equipo = "9020110152013";
            oParametros.Cod_Periodo = _vperiodo;//5785;
            oParametros.Cod_Oficina = _vcanal;//25;

            oParametros.Cod_Mercado = _vcadena;// 29;
            oParametros.Cod_Nivel = _vnivel;// "N00001";
            oParametros.Cod_Marca = _vmarca;// 1789;
            oParametros.Cod_Categoria = _vcategoria;// "10066";
            oParametros.Cod_SKU = _vproducto;// "BP0031,BP0097,BP0030,BP0098";                

            return View(new E_Nw_TB_Reporting_Backus().Consul_OutOfStockxCadena_Reporting_evolutivo(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }
        public ActionResult PRT_TBStockOut(string _vperfil, int _vperiodo, int _vcanal, int _vcadena, string _vnivel, int _vmarca, string _vcategoria, string _vproducto)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();

            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString();//_vperfil;// "170935";
            oParametros.Cod_Equipo = "9020110152013";
            oParametros.Cod_Periodo = _vperiodo;// 5785;
            oParametros.Cod_Oficina = _vcanal;// 25;
            oParametros.Cod_Mercado = _vcadena;// 29;
            oParametros.Cod_Nivel = _vnivel;// "N00001";
            oParametros.Cod_Marca = _vmarca;// 1789;
            oParametros.Cod_Categoria = _vcategoria;// "10066";
            oParametros.Cod_SKU = _vproducto;// "BP0031,BP0097,BP0030,BP0098";
            return View(new E_Nw_TB_Reporting_Backus().Consul_OutOfStock_Reporting_evolutivo(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }
        public ActionResult PRT_TBStockOutxCadena(string _vperfil, int _vperiodo, int _vcanal, int _vcadena, string _vnivel, int _vmarca, string _vcategoria, string _vproducto)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();

            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString();//_vperfil;// "170935";
            oParametros.Cod_Equipo = "9020110152013";
            oParametros.Cod_Periodo = _vperiodo;// 5785;
            oParametros.Cod_Oficina = _vcanal;// 25;

            oParametros.Cod_Mercado = _vcadena;// 29;
            oParametros.Cod_Nivel = _vnivel;// "N00001";
            oParametros.Cod_Marca = _vmarca;// 1789;
            oParametros.Cod_Categoria = _vcategoria;//"10066";
            oParametros.Cod_SKU = _vproducto;// "BP0031,BP0097,BP0030,BP0098";

            return View(new E_Nw_TB_Reporting_Backus().Consul_OutOfStockxCadena_Reporting_evolutivo(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }
        [CustomAuthorize]
        public ActionResult ResumenEjecutivo()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        public ActionResult PRT_Filtros(int _vobj, string _vparam, int _vop, string _vselect)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Parametros = _vparam;
            oParametros.Cod_Op = _vop;
            ViewBag.cod_obj = _vobj;
            ViewBag.cod_select = _vselect;
            return View(new E_Nw_Filtros_Backus().Consul_Filtros(new Request_NW_Reporting() { oParametros = oParametros }));
        }

        public ActionResult PRT_TBPresenciaProducto(string _vperfil, int _vperiodo, int _vcanal, int _vcadena, string _vnivel, int _vmarca, string _vcategoria, string _vproducto)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();

            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString();//_vperfil;
            oParametros.Cod_Equipo = "9020110152013";
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Oficina = _vcanal;// 25;
            oParametros.Cod_Mercado = _vcadena;// 29;
            oParametros.Cod_Nivel = _vnivel;// "N00001";
            oParametros.Cod_Marca = _vmarca;// 1789;
            oParametros.Cod_Categoria = _vcategoria;// "10066";
            oParametros.Cod_SKU = _vproducto;// "BP0030,BP0031,BP0032,BP0097,BP0098";
            return View(new E_Nw_TB_Reporting_Backus().Consul_PresenciaProducto_Reporting_evolutivo(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }
        public ActionResult PRT_TBPresenciaProductoxCadena(string _vperfil, int _vperiodo, int _vcanal, int _vcadena, string _vnivel, int _vmarca, string _vcategoria, string _vproducto)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();

            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString();//_vperfil;// "170935";
            oParametros.Cod_Equipo = "9020110152013";
            oParametros.Cod_Periodo = _vperiodo;// 5785;
            oParametros.Cod_Oficina = _vcanal;// 25;

            oParametros.Cod_Mercado = _vcadena;// 29;
            oParametros.Cod_Nivel = _vnivel;// "N00001";
            oParametros.Cod_Marca = _vmarca;// 1789;
            oParametros.Cod_Categoria = _vcategoria;//"10066";
            oParametros.Cod_SKU = _vproducto;// "BP0030,BP0031,BP0032,BP0097,BP0098";

            return View(new E_Nw_TB_Reporting_Backus().Consul_PresenciaxCadena_Reporting_evolutivo(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }
        public ActionResult PRT_LinePresenciaxCadena(string _vperfil, int _vperiodo, int _vcanal, int _vcadena, string _vnivel, int _vmarca, string _vcategoria, string _vproducto)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();

            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString();//_vperfil;// "170935";
            oParametros.Cod_Equipo = "9020110152013";
            oParametros.Cod_Periodo = _vperiodo;// 5785;
            oParametros.Cod_Oficina = _vcanal;// 25;

            oParametros.Cod_Mercado = _vcadena;// 29;
            oParametros.Cod_Nivel = _vnivel;// "N00001";
            oParametros.Cod_Marca = _vmarca;// 1789;
            oParametros.Cod_Categoria = _vcategoria;//"10066";
            oParametros.Cod_SKU = _vproducto;// "BP0030,BP0031,BP0032,BP0097,BP0098";

            return View(new E_Nw_TB_Reporting_Backus().Consul_PresenciaxCadena_Reporting_evolutivo(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }
        [CustomAuthorize]
        public ActionResult PrecioProducto()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_canal = "25";
            oParametros.Cod_Reporte = 19;

            return View(new E_Nw_Rp_Backus_Param_Iniciales().Consul_Inicial(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult PRT_TBPrecioResumen(int _vperiodo, int _vcanal, int _vcadena, string _vnivel, string _vcategoria)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Anio = 0;
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Oficina = _vcanal;
            oParametros.Cod_Mercado = _vcadena;
            oParametros.Cod_Categoria = _vcategoria;
            oParametros.Cod_Nivel = _vnivel;
            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString();//"170935";
            oParametros.Cod_Equipo = "9020110152013";

            return View(new E_Nw_TB_Reporting_Backus().Consul_PrecioResumen_Reporting(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));

        }
        public ActionResult PRT_TBPrecioComparativo(int _vperiodo, int _vcanal, int _vcadena, string _vnivel, string _vcategoria, string _venvase)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Anio = 0;
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Oficina = _vcanal;
            oParametros.Cod_Mercado = _vcadena;
            oParametros.Cod_Categoria = _vcategoria;
            oParametros.Cod_Nivel = _vnivel;
            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString();//"170935";
            oParametros.Cod_Equipo = "9020110152013";
            oParametros.Parametros = _venvase;

            ViewBag.vw_periodo = _vperiodo;
            ViewBag.vw_canal = _vcanal;
            ViewBag.vw_cadena = _vcadena;
            ViewBag.vw_nivel = _vnivel;
            ViewBag.vw_categoria = _vcategoria;
            ViewBag.vw_envase = _venvase;

            return View(new E_Nw_TB_Reporting_Backus().Consul_PrecioComparativo_Reporting(
                        new Request_NW_Reporting() { oParametros = oParametros }
            ));
        }
        public ActionResult PRT_GrafPrecioComparativo(int _vperiodo, int _vcanal, int _vcadena, string _vnivel, string _vcategoria, string _venvase)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Anio = 0;
            oParametros.Cod_Periodo = _vperiodo;
            oParametros.Cod_Oficina = _vcanal;
            oParametros.Cod_Mercado = _vcadena;
            oParametros.Cod_Categoria = _vcategoria;
            oParametros.Cod_Nivel = _vnivel;
            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString();//"170935";
            oParametros.Cod_Equipo = "9020110152013";
            oParametros.Parametros = _venvase;

            return View(new E_Nw_Report_Backus_Precio_Comparativo().Consul_Grafico(
                         new Request_NW_Reporting() { oParametros = oParametros }
             ));
        }

        [CustomAuthorize]
        public ActionResult ShareOfDisplay()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_canal = "25";
            oParametros.Cod_Reporte = 21;

            return View(new E_Nw_Rp_Backus_Param_Iniciales().Consul_Inicial(new Request_NW_Reporting() { oParametros = oParametros }));
        }
        public ActionResult ShareOfDisplayResumen(int _vcanal, int _vanio, string _vmes, int _vnivel, string _vcategoria)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();

            oParametros.Cod_Perfil = ((Persona)Session["Session_Login"]).Perfil_id.ToString();// "170935";
            oParametros.Cod_Equipo = "9020110152013";
            oParametros.Cod_Oficina = _vcanal;
            oParametros.Anio = _vanio;
            oParametros.Cod_Mes = _vmes;
            oParametros.Cod_Op = _vnivel;
            oParametros.Cod_Categoria = _vcategoria;

            return View(new E_Nw_TB_Reporting_Backus().Consul_SOD_Resumen_Reporting(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }

        public ActionResult ShareOfDisplayDetalle(string _vcanal, int _vanio, string _vmes, string _vcategoria, int _vcadena, string _velemento, int _vnivel)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Cod_canal = _vcanal;
            oParametros.Anio = _vanio;
            oParametros.Cod_Mes = _vmes;
            oParametros.Cod_Categoria = _vcategoria;
            oParametros.Cod_Mercado = _vcadena;
            oParametros.Parametros = _velemento;
            oParametros.Cod_Op = _vnivel;

            return View(new E_Nw_TB_Report_Backus_SOD_Detalle().Consul_SOD_Detalle(
                        new Request_NW_Reporting() { oParametros = oParametros }
                    ));
        }

        [CustomAuthorize]
        public ActionResult XploraMaps()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public JsonResult DescargarBD(string Cod_canal, int Cod_Reporte, int Cod_Periodo, int vAnio, string Cod_Mes, string _vnombre)
        {
            //DirectoryInfo directory = new DirectoryInfo(RutaXplora + "/Temp".ToString());
            //FileInfo[] files = directory.GetFiles("*.xlsx");
            //DirectoryInfo[] directories = directory.GetDirectories();
            //for (int i = 0; i < files.Length; i++)
            //{
            //    string vdato = directory.ToString() + "\\" + ((FileInfo)files[i]).Name.ToString();
            //    System.IO.File.Delete(vdato);
            //}


            BL_GES_Operativa oOperativa = new BL_GES_Operativa();
            DataTable TB_Data = oOperativa.BL_NW_Reporting_Descarga_BD_Backus(Cod_canal, Cod_Reporte, Cod_Periodo, vAnio, Cod_Mes);


            string _fileServer = "";
            string _filePath = "";
            //string _password = "";
            int _fila = 0;

            // _password = "1234";
            _fileServer = String.Format(_vnombre + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            if (_fileNew.Exists)
            {
                _fileNew.Delete();
                _fileNew = new FileInfo(_filePath);
            }

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {

                if (Cod_Reporte == 58)
                {
                    #region <<< Reporte Presnecia >>>
                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["cadena"] ascending, _Fila["PDV"] ascending
                                       , _Fila["Nivel"] ascending, _Fila["Categoria"] ascending, _Fila["Marca"] ascending
                                       select new
                                       {
                                           _Cadena = _Fila["Cadena"]
                                           ,
                                           _PDV = _Fila["PDV"]
                                           ,
                                           _Nombre_sku = _Fila["Nombre sku"]
                                           ,
                                           _Categoria = _Fila["Categoria"]
                                           ,
                                           _Nivel = _Fila["Nivel"]
                                           ,
                                           _Marca = _Fila["Marca"]
                                           ,
                                           _Presencia = _Fila["Presencia"]
                                           ,
                                           _Fecha = _Fila["Fecha Relevo"]
                                       }
                                       ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte Presencia");
                        oWsReporte.Cells[1, 1].Value = "Cadena";
                        oWsReporte.Cells[1, 2].Value = "PDV";
                        oWsReporte.Cells[1, 3].Value = "Nivel";
                        oWsReporte.Cells[1, 4].Value = "Categoria";
                        oWsReporte.Cells[1, 5].Value = "Marca";
                        oWsReporte.Cells[1, 6].Value = "Nombre SKU";
                        oWsReporte.Cells[1, 7].Value = "Presencia";
                        oWsReporte.Cells[1, 8].Value = "Fecha Relevo";

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Cadena;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._PDV;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Nivel;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Categoria;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Marca;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Nombre_sku;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Presencia;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Fecha;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);

                    }
                    #endregion

                }

                if (Cod_Reporte == 163)
                {
                    #region <<< Reporte Stock Out >>>
                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["cadena"] ascending, _Fila["PDV"] ascending
                                       , _Fila["Nivel"] ascending, _Fila["Categoria"] ascending, _Fila["Marca"] ascending
                                       select new
                                       {
                                           _Cadena = _Fila["Cadena"]
                                           ,
                                           _PDV = _Fila["PDV"]
                                           ,
                                           _Nombre_sku = _Fila["Nombre sku"]
                                           ,
                                           _Categoria = _Fila["Categoria"]
                                           ,
                                           _Nivel = _Fila["Nivel"]
                                           ,
                                           _Marca = _Fila["Marca"]
                                           ,
                                           _Quiebre = _Fila["Quiebre"]
                                           ,
                                           _Fecha = _Fila["Fecha Relevo"]
                                       }
                                       ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte Stock Out");
                        oWsReporte.Cells[1, 1].Value = "Cadena";
                        oWsReporte.Cells[1, 2].Value = "PDV";
                        oWsReporte.Cells[1, 3].Value = "Nivel";
                        oWsReporte.Cells[1, 4].Value = "Categoria";
                        oWsReporte.Cells[1, 5].Value = "Marca";
                        oWsReporte.Cells[1, 6].Value = "Nombre SKU";
                        oWsReporte.Cells[1, 7].Value = "Quiebre";
                        oWsReporte.Cells[1, 8].Value = "Fecha Relevo";

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Cadena;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._PDV;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Nivel;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Categoria;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Marca;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Nombre_sku;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Quiebre;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Fecha;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);

                    }
                    #endregion
                }
                if (Cod_Reporte == 19)
                {
                    #region <<< Reporte Precio >>>
                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["cadena"] ascending, _Fila["PDV"] ascending
                                       , _Fila["Nivel"] ascending, _Fila["Categoria"] ascending, _Fila["Marca"] ascending
                                       select new
                                       {
                                           _Cadena = _Fila["Cadena"]
                                           ,
                                           _PDV = _Fila["PDV"]
                                           ,
                                           _Nombre_sku = _Fila["Nombre sku"]
                                           ,
                                           _Categoria = _Fila["Categoria"]
                                           ,
                                           _Nivel = _Fila["Nivel"]
                                           ,
                                           _Marca = _Fila["Marca"]
                                           ,
                                           _Quiebre = _Fila["precio"]
                                           ,
                                           _Fecha = _Fila["Fecha Relevo"]
                                       }
                                        ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte Precio");
                        oWsReporte.Cells[1, 1].Value = "Cadena";
                        oWsReporte.Cells[1, 2].Value = "PDV";
                        oWsReporte.Cells[1, 3].Value = "Nivel";
                        oWsReporte.Cells[1, 4].Value = "Categoria";
                        oWsReporte.Cells[1, 5].Value = "Marca";
                        oWsReporte.Cells[1, 6].Value = "Nombre SKU";
                        oWsReporte.Cells[1, 7].Value = "Precio";
                        oWsReporte.Cells[1, 8].Value = "Fecha Relevo";

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Cadena;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._PDV;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Nivel;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Categoria;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Marca;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Nombre_sku;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Quiebre;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Fecha;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);

                    }
                    #endregion
                }
                if (Cod_Reporte == 21)
                {
                    #region <<< Reporte SOD  >>>

                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["cadena"] ascending, _Fila["PDV"] ascending
                                       , _Fila["Categoria"] ascending
                                       select new
                                       {
                                           _Cadena = _Fila["Cadena"]
                                           ,
                                           _PDV = _Fila["PDV"]
                                           ,
                                           _Categoria = _Fila["Categoria"]
                                           ,
                                           _Tipo = _Fila["Tipo"]
                                           ,
                                           _Elemento = _Fila["Elemento"]
                                           ,
                                           _Empresa = _Fila["Empresa"]
                                           ,
                                           _Marca = _Fila["Marca"]
                                           ,
                                           _ValCentimetro = _Fila["val_centimetros"]
                                           ,
                                           _ValCantidad = _Fila["val_cantidad"]
                                           ,
                                           _Fecha_relevo = _Fila["Fecha Relevo"]
                                       }
                    ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte SOD");
                        oWsReporte.Cells[1, 1].Value = "Cadena";
                        oWsReporte.Cells[1, 2].Value = "PDV";
                        oWsReporte.Cells[1, 3].Value = "Categoria";
                        oWsReporte.Cells[1, 4].Value = "Tipo";
                        oWsReporte.Cells[1, 5].Value = "Elemento";
                        oWsReporte.Cells[1, 6].Value = "Empresa";
                        oWsReporte.Cells[1, 7].Value = "Marca";
                        oWsReporte.Cells[1, 8].Value = "Val Centimetros";
                        oWsReporte.Cells[1, 9].Value = "Val Cantidad";
                        oWsReporte.Cells[1, 10].Value = "Fecha Relevo";

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Cadena;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._PDV;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Categoria;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Tipo;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Elemento;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Empresa;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Marca;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._ValCentimetro;
                            oWsReporte.Cells[_fila, 9].Value = _Datos._ValCantidad;
                            oWsReporte.Cells[_fila, 10].Value = _Datos._Fecha_relevo;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();
                        oWsReporte.Column(10).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);

                    }

                    #endregion
                }
                if (Cod_Reporte == 66)
                {
                    #region  <<< Reporte POP >>>

                    var _List_Datos = (from DataRow _Fila in TB_Data.Rows
                                       orderby _Fila["cadena"] ascending, _Fila["PDV"] ascending
                                       , _Fila["Categoria"] ascending, _Fila["Marca"] ascending
                                       select new
                                       {
                                           _Cadena = _Fila["Cadena"]
                                           ,
                                           _PDV = _Fila["PDV"]
                                           ,
                                           _Categoria = _Fila["Categoria"]
                                           ,
                                           _Tipo_Empresa = _Fila["Tipo_Empresa"]
                                           ,
                                           _Marca = _Fila["Marca"]
                                           ,
                                           _Material = _Fila["Material"]
                                           ,
                                           _Foto = _Fila["foto"]
                                           ,
                                           _Cantidad = _Fila["cantidad"]
                                           ,
                                           _Fecha_Relevo = _Fila["Fecha Relevo"]
                                       }
                    ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Reporte POP");
                        oWsReporte.Cells[1, 1].Value = "Cadena";
                        oWsReporte.Cells[1, 2].Value = "PDV";
                        oWsReporte.Cells[1, 3].Value = "Categoria";
                        oWsReporte.Cells[1, 4].Value = "Tipo Empresa";
                        oWsReporte.Cells[1, 5].Value = "Marca";
                        oWsReporte.Cells[1, 6].Value = "Material";
                        oWsReporte.Cells[1, 7].Value = "Foto";
                        oWsReporte.Cells[1, 8].Value = "Cantidad";
                        oWsReporte.Cells[1, 9].Value = "Fecha Relevo";

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Cadena;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._PDV;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Categoria;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Tipo_Empresa;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Marca;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Material;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Foto;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Cantidad;
                            oWsReporte.Cells[_fila, 9].Value = _Datos._Fecha_Relevo;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);

                    }

                    #endregion
                }

                oEx.Save();
            }


            if (TB_Data.Rows.Count < 0 || TB_Data == null)
            {
                _fileServer = "";
            }

            return Json(new { Archivo = _fileServer });//_fileServer });
        }

        public ActionResult VWDemo() {

            return View();
        }

        #endregion

        #region Actividad

        [CustomAuthorize]
        public ActionResult Actividad()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.Tipo_Perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
            //ViewBag.compania = "9020110152013";


            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        [HttpPost]
        public ActionResult Consulta_Actividad_Backus(int canal, int anio, int cadena)
        {
            ViewBag.canal = canal;
            ViewBag.anio = anio;
            ViewBag.cadena = cadena;
            return View(
                //    new Actividad().Lista(
                //    new Request_Actividad()
                //    {
                //        anio = anio
                //    }
                //)
            );

        }

        [HttpPost]
        public ActionResult GetGaleriaFotosBackus(int __a)
        {
            //return View(
            //        new ActividadFoto().Lista(new Request_Consulta_Actividad_Foto()
            //        {
            //            cod_actividad = __a,
            //        })
            //    );

            return Json(new ActividadFoto().Lista(
            new Request_Consulta_Actividad_Foto()
            {
                cod_actividad = __a
            }
            ));


        }

        #endregion

        #region << RQ Lucky 137 v1.2 >>

        [CustomAuthorize]
        public ActionResult EncarteIndex()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            if (Request["_a"] != null)
            {
                Session["Session_Campania"] = Request["_a"];
                ViewBag.campania = Request["_a"];
            }
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        public ActionResult Consulta_AEncarte_ResGen_Anuncios(string _vanio, int _vmes, string _vcodcategoria)
        {
            E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

            oParametros.Anio = _vanio;
            oParametros.Mes = _vmes;
            oParametros.Cod_Categoria = _vcodcategoria;

            List<E_Nw_Encarte_GeneralSummary_Backus> oLs = new E_Nw_Encarte_GeneralSummary_Backus().Consul_Encarte_ResGeneral_Anuncios(
                    new Request_Nw_Encarte_Parametros()
                    {
                        oParametros = oParametros
                    });

            return Json(new { Archivo = oLs });
        }
        public ActionResult Consulta_AEncarte_ResGen_Anuncios_xCadena(string _vanio, int _vmes, string _vcodcategoria)
        {
            E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

            oParametros.Anio = _vanio;
            oParametros.Mes = _vmes;
            oParametros.Cod_Categoria = _vcodcategoria;

            List<E_Nw_Encarte_GeneralSummary_xCadenaBackus> oLs = new E_Nw_Encarte_GeneralSummary_xCadenaBackus().Consul_Encarte_ResGeneral_Anuncios_xCadena(
                    new Request_Nw_Encarte_Parametros()
                    {
                        oParametros = oParametros
                    });

            return Json(new { Archivo = oLs });
        }
        public ActionResult Consulta_Nw_Participacion_Cadena(string _vanio, int _vmes, string _vcodcategoria)
        {
            E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

            oParametros.Anio = _vanio;
            oParametros.Mes = _vmes;
            oParametros.Cod_Categoria = _vcodcategoria;

            List<E_Nw_Participacion_Cadena> oLs = new E_Nw_Participacion_Cadena().Consul_Nw_Participacion_Cadena(
                    new Request_Nw_Encarte_Parametros()
                    {
                        oParametros = oParametros
                    });

            return Json(new { Archivo = oLs });
        }

        public ActionResult EvolutionarySummary()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        public ActionResult Consulta_Nw_EvolutionarySummary(string _vanio, int _vmes, string _vcodcategoria, string _vcodcadena)
        {
            E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

            oParametros.Anio = _vanio;
            oParametros.Mes = _vmes;
            oParametros.Cod_Categoria = _vcodcategoria;
            oParametros.Cod_Cadena = _vcodcadena;

            List<E_Nw_Encarte_Evolutionary_Summary> oLs = new E_Nw_Encarte_Evolutionary_Summary().Consul_Nw_Evolutionary_Summary(
                    new Request_Nw_Encarte_Parametros()
                    {
                        oParametros = oParametros
                    });

            return Json(new { Archivo = oLs });
        }
        public ActionResult Consulta_Nw_EvolutionarySummary_Chain(string _vanio, int _vmes, string _vcodcategoria, string _vcodcadena)
        {
            E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

            oParametros.Anio = _vanio;
            oParametros.Mes = _vmes;
            oParametros.Cod_Categoria = _vcodcategoria;
            oParametros.Cod_Cadena = _vcodcadena;

            List<E_Nw_Encarte_Evolutionary_Summary> oLs = new E_Nw_Encarte_Evolutionary_Summary().Consul_Nw_Evolutionary_Summary_Chain(
                    new Request_Nw_Encarte_Parametros()
                    {
                        oParametros = oParametros
                    });

            return Json(new { Archivo = oLs });
        }

        public ActionResult SummarybyTypeOfAd()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        public ActionResult PRT_EvoluSummary_NoticeType(string _vanio, int _vmes, string _vcodcategoria, string _vcodnivel)
        {
            E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

            oParametros.Anio = _vanio;
            oParametros.Mes = _vmes;
            oParametros.Cod_Categoria = _vcodcategoria;
            oParametros.Cod_Nivel = _vcodnivel;

            return View(new E_Nw_EvolutionarySummary_NoticeType().Consul_Nw_EvolutionarySummary_NoticeType(
                    new Request_Nw_Encarte_Parametros()
                    {
                        oParametros = oParametros
                    }));

        }
        public ActionResult TB_EvoluSummary_NoticeType(string _vanio, int _vmes, string _vcodcategoria, string _vcodnivel)
        {
            E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

            oParametros.Anio = _vanio;
            oParametros.Mes = _vmes;
            oParametros.Cod_Categoria = _vcodcategoria;
            oParametros.Cod_Nivel = _vcodnivel;

            List<E_Nw_EvolutionarySummary_NoticeType> oLs = new E_Nw_EvolutionarySummary_NoticeType().Consul_Nw_EvolutionarySummary_NoticeType(
                    new Request_Nw_Encarte_Parametros()
                    {
                        oParametros = oParametros
                    });
            return Json(new { Archivo = oLs });
        }
        /*Evaluar tab inicio*/
        [HttpPost]
        public ActionResult Consulta_Nw_Participacion_Marca(string _vanio, int _vmes, string _vcodcategoria, string _vcodnivel, string _vcodcadena)
        {
            ViewBag.gf_anio = _vanio;
            ViewBag.gf_mes = _vmes;
            ViewBag.gf_categoria = _vcodcategoria;
            ViewBag.gf_nivel = _vcodnivel;
            ViewBag.gf_cadena = _vcodcadena;
            return View();
        }
        /*Evaluar tab fin*/
        public ActionResult PRT_BarParticipacion_Marca(string _vanio, int _vmes, string _vcodcategoria, string _vcodnivel, int _vcodcadena)
        {
            E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

            oParametros.Anio = _vanio;
            oParametros.Mes = _vmes;
            oParametros.Cod_Categoria = _vcodcategoria;
            oParametros.Cod_Nivel = _vcodnivel;
            oParametros.Cod_Cadena = Convert.ToString(_vcodcadena);

            return View(new E_Nw_Graf_Participacion_Marca().Consul_Nw_Bar_Participacion_Marca(
                    new Request_Nw_Encarte_Parametros()
                    {
                        oParametros = oParametros
                    }));
        }

        public ActionResult Productdetail()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        public ActionResult Consulta_Nw_Productdetail(string _vanio, int _vmes, string _vcodcategoria, string _vcodnivel, int _vcodcadena)
        {
            E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

            oParametros.Anio = _vanio;
            oParametros.Mes = _vmes;
            oParametros.Cod_Categoria = _vcodcategoria;
            oParametros.Cod_Nivel = _vcodnivel;
            oParametros.Cod_Cadena = Convert.ToString(_vcodcadena);

            List<E_Nw_Graf_Productdetail> oLs = new E_Nw_Graf_Productdetail().Consul_Nw_DetallePrd(
                    new Request_Nw_Encarte_Parametros()
                    {
                        oParametros = oParametros
                    });

            return Json(new { Archivo = oLs });
        }

        [CustomAuthorize]
        public ActionResult ExecutiveSummary()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        #endregion

        #region Visibilidad

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-07
        /// </summary>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult Visibilidad()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != "")
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = ViewBag.campania }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-07
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonVisibilidadResumen(string __a, string __b, string __c, string __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Reporting_Visibilidad().Objeto(
                    new Request_ReportingVisibilidad()
                    {
                        periodo = Convert.ToInt32(__a),
                        categoria = Convert.ToInt32(__b),
                        actividad = Convert.ToInt32(__c),
                        tipo_canal = Convert.ToInt32(__d)
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-08
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonVisibilidadCobertura(string __a, string __b, string __c, string __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Reporting_Visibilidad_Cobertura().Objeto(
                    new Request_ReportingVisibilidad()
                    {
                        periodo = Convert.ToInt32(__a),
                        categoria = Convert.ToInt32(__b),
                        actividad = Convert.ToInt32(__c),
                        tipo_canal = Convert.ToInt32(__d)
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-08
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonVisibilidadDetalle(string __a, string __b, string __c, string __d, string __e)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Reporting_Visibilidad_Detalle().Objeto(
                    new Request_ReportingVisibilidad()
                    {
                        periodo = Convert.ToInt32(__a),
                        categoria = Convert.ToInt32(__b),
                        actividad = Convert.ToInt32(__c),
                        tipo_canal = Convert.ToInt32(__d),
                        cadena = Convert.ToInt32(__e)
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        #endregion

        public JsonResult DescargarProductDetail(string _vanio, int _vmes, string _vcodcategoria, string _vcodnivel, int _vcodcadena,string _vnombre)
        {
            //DirectoryInfo directory = new DirectoryInfo(RutaXplora + "/Temp".ToString());
            //FileInfo[] files = directory.GetFiles("*.xlsx");
            //DirectoryInfo[] directories = directory.GetDirectories();

            BL_GES_Operativa oOperativa = new BL_GES_Operativa();

            //DataTable TB_Data = null;
   
            ///
     
                E_Nw_Encarte_Parametros oParametros = new E_Nw_Encarte_Parametros();

                oParametros.Anio = _vanio;
                oParametros.Mes = _vmes;
                oParametros.Cod_Categoria = _vcodcategoria;
                oParametros.Cod_Nivel = _vcodnivel;
                oParametros.Cod_Cadena = Convert.ToString(_vcodcadena);

                List<E_Nw_Graf_Productdetail> oLs = new E_Nw_Graf_Productdetail().Consul_Nw_DetallePrd(
                        new Request_Nw_Encarte_Parametros()
                        {
                            oParametros = oParametros
                        });
            
            ///
 
            string _fileServer = "";
            string _filePath = "";
            //string _password = "";
            int _fila = 0;

            // _password = "1234";
            _fileServer = String.Format(_vnombre + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            if (_fileNew.Exists)
            {
                _fileNew.Delete();
                _fileNew = new FileInfo(_filePath);
            }

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                    #region <<< Reporte Producto detalle >>>
                    var _List_Datos = (from  _Fila in oLs                                       
                                       select new
                                       {
                                           _Marca = _Fila.Marca
                                           ,
                                           _Producto = _Fila.Producto
                                           ,
                                           _Mecanica = _Fila.Mecanica
                                           ,
                                           _Tipo_Anuncio = _Fila.Tipo_Anuncio
                                           ,
                                           _Cadena = _Fila.Cadena
                                           ,
                                           _Fecha_Inicio = _Fila.Fecha_Inicio
                                           ,
                                           _Fecha_Termino = _Fila.Fecha_Termino
                                           ,
                                           _Precio_Oferta = _Fila.Precio_Oferta
                                           ,
                                           _Precio_Normal = _Fila.Precio_Normal
                                       }
                                       ).Distinct().ToList();

                    if (_List_Datos != null || _List_Datos.Count() > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Detalle Producto");
                        oWsReporte.Cells[1, 1].Value = "MARCA";
                        oWsReporte.Cells[1, 2].Value = "PRODUCTO";
                        oWsReporte.Cells[1, 3].Value = "MECANICA";
                        oWsReporte.Cells[1, 4].Value = "TIPO ANUNCIO";
                        oWsReporte.Cells[1, 5].Value = "CADENA";
                        oWsReporte.Cells[1, 6].Value = "FECHA INICIO";
                        oWsReporte.Cells[1, 7].Value = "FECHA TERMINO";
                        oWsReporte.Cells[1, 8].Value = "PRECIO OFERTA";
                        oWsReporte.Cells[1, 9].Value = "PRECIO NORMAL";

                        _fila = 2;
                        foreach (var _Datos in _List_Datos)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos._Marca;
                            oWsReporte.Cells[_fila, 2].Value = _Datos._Producto;
                            oWsReporte.Cells[_fila, 3].Value = _Datos._Mecanica;
                            oWsReporte.Cells[_fila, 4].Value = _Datos._Tipo_Anuncio;
                            oWsReporte.Cells[_fila, 5].Value = _Datos._Cadena;
                            oWsReporte.Cells[_fila, 6].Value = _Datos._Fecha_Inicio;
                            oWsReporte.Cells[_fila, 7].Value = _Datos._Fecha_Termino;
                            oWsReporte.Cells[_fila, 8].Value = _Datos._Precio_Oferta;
                            oWsReporte.Cells[_fila, 9].Value = _Datos._Precio_Normal;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);
                        oWsReporte.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    }
                    #endregion
               
                oEx.Save();
            }


            if (oLs.Count <= 0 || oLs == null)
            {
                _fileServer = "";
            }

            return Json(new { Archivo = _fileServer });//_fileServer });
        }
    }

    //public class InsertImage
    //{
    //    /// <summary> 
    //    /// Insert a new Slide into PowerPoint 
    //    /// </summary> 
    //    /// <param name="presentationPart">Presentation Part</param> 
    //    /// <param name="layoutName">Layout of the new Slide</param> 
    //    /// <returns>Slide Instance</returns> 
    //    public Slide InsertSlide(PresentationPart presentationPart, string layoutName)
    //    {
    //        UInt32 slideId = 256U;

    //        // Get the Slide Id collection of the presentation document 
    //        var slideIdList = presentationPart.Presentation.SlideIdList;

    //        if (slideIdList == null)
    //        {
    //            throw new NullReferenceException("The number of slide is empty, please select a ppt with a slide at least again");
    //        }

    //        slideId += Convert.ToUInt32(slideIdList.Count());

    //        // Creates a Slide instance and adds its children. 
    //        Slide slide = new Slide(new CommonSlideData(new ShapeTree()));

    //        SlidePart slidePart = presentationPart.AddNewPart<SlidePart>();
    //        slide.Save(slidePart);

    //        // Get SlideMasterPart and SlideLayoutPart from the existing Presentation Part 
    //        SlideMasterPart slideMasterPart = presentationPart.SlideMasterParts.First();
    //        SlideLayoutPart slideLayoutPart = slideMasterPart.SlideLayoutParts.SingleOrDefault(sl => sl.SlideLayout.CommonSlideData.Name.Value.Equals(layoutName, StringComparison.OrdinalIgnoreCase));

    //        if (slideLayoutPart == null)
    //        {
    //            throw new Exception("The slide layout " + layoutName + " is not found");
    //        }

    //        slidePart.AddPart<SlideLayoutPart>(slideLayoutPart);

    //        slidePart.Slide.CommonSlideData = (CommonSlideData)slideMasterPart.SlideLayoutParts.SingleOrDefault(sl => sl.SlideLayout.CommonSlideData.Name.Value.Equals(layoutName)).SlideLayout.CommonSlideData.Clone();

    //        // Create SlideId instance and Set property 
    //        SlideId newSlideId = presentationPart.Presentation.SlideIdList.AppendChild<SlideId>(new SlideId());
    //        newSlideId.Id = slideId;
    //        newSlideId.RelationshipId = presentationPart.GetIdOfPart(slidePart);

    //        return GetSlideByRelationShipId(presentationPart, newSlideId.RelationshipId);
    //    }

    //    /// <summary> 
    //    /// Get Slide By RelationShip ID 
    //    /// </summary> 
    //    /// <param name="presentationPart">Presentation Part</param> 
    //    /// <param name="relationshipId">Relationship ID</param> 
    //    /// <returns>Slide Object</returns> 
    //    private static Slide GetSlideByRelationShipId(PresentationPart presentationPart, StringValue relationshipId)
    //    {
    //        // Get Slide object by Relationship ID 
    //        SlidePart slidePart = presentationPart.GetPartById(relationshipId) as SlidePart;
    //        if (slidePart != null)
    //        {
    //            return slidePart.Slide;
    //        }
    //        else
    //        {
    //            return null;
    //        }
    //    }

    //    /// <summary> 
    //    /// Insert Image into Slide 
    //    /// </summary> 
    //    /// <param name="filePath">PowerPoint Path</param> 
    //    /// <param name="imagePath">Image Path</param> 
    //    /// <param name="imageExt">Image Extension</param> 
    //    public void InsertImageInLastSlide(Slide slide, string imagePath, string imageExt)
    //    {
    //        // Creates a Picture instance and adds its children. 
    //        P.Picture picture = new P.Picture();
    //        string embedId = string.Empty;
    //        embedId = "rId" + (slide.Elements<P.Picture>().Count() + 915).ToString();
    //        P.NonVisualPictureProperties nonVisualPictureProperties = new P.NonVisualPictureProperties(new P.NonVisualDrawingProperties() { Id = (UInt32Value)4U, Name = "Picture 5" }, new P.NonVisualPictureDrawingProperties(new A.PictureLocks() { NoChangeAspect = true }), new ApplicationNonVisualDrawingProperties());

    //        P.BlipFill blipFill = new P.BlipFill();
    //        Blip blip = new Blip() { Embed = embedId };

    //        // Creates a BlipExtensionList instance and adds its children 
    //        BlipExtensionList blipExtensionList = new BlipExtensionList();
    //        BlipExtension blipExtension = new BlipExtension() { Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}" };

    //        //UseLocalDpi useLocalDpi = new UseLocalDpi() { Val = false };
    //        //useLocalDpi.AddNamespaceDeclaration("a14",
    //        //    "http://schemas.microsoft.com/office/drawing/2010/main");

    //        //blipExtension.Append(useLocalDpi);
    //        blipExtensionList.Append(blipExtension);
    //        blip.Append(blipExtensionList);

    //        Stretch stretch = new Stretch();
    //        FillRectangle fillRectangle = new FillRectangle();
    //        stretch.Append(fillRectangle);

    //        blipFill.Append(blip);
    //        blipFill.Append(stretch);

    //        // Creates a ShapeProperties instance and adds its children. 
    //        P.ShapeProperties shapeProperties = new P.ShapeProperties();

    //        A.Transform2D transform2D = new A.Transform2D();
    //        A.Offset offset = new A.Offset() { X = 457200L, Y = 1524000L };
    //        A.Extents extents = new A.Extents() { Cx = 8229600L, Cy = 5029200L };

    //        transform2D.Append(offset);
    //        transform2D.Append(extents);

    //        A.PresetGeometry presetGeometry = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Rectangle };
    //        A.AdjustValueList adjustValueList = new A.AdjustValueList();

    //        presetGeometry.Append(adjustValueList);

    //        shapeProperties.Append(transform2D);
    //        shapeProperties.Append(presetGeometry);

    //        picture.Append(nonVisualPictureProperties);
    //        picture.Append(blipFill);
    //        picture.Append(shapeProperties);

    //        slide.CommonSlideData.ShapeTree.AppendChild(picture);
    //        slide.Save();

    //        // Generates content of imagePart. 
    //        ImagePart imagePart = slide.SlidePart.AddNewPart<ImagePart>(imageExt, embedId);
    //        FileStream fileStream = new FileStream(imagePath, FileMode.Open);
    //        imagePart.FeedData(fileStream);
    //        fileStream.Close();
    //    }
    //} 
}