﻿using Lucky.Xplora.DataAccess;
using Lucky.Xplora.Models.Inventario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lucky.Xplora.Controllers
{
    public class InventarioController : Controller
    {
        //
        // GET: /Inventario/

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            if (Session["Session_Login"] != null)
            {
                return RedirectToAction("PanelAcceso", "Inventario");
            }

            string ip = Request.UserHostAddress;
            ViewBag.IP = ip;
            return View();
        }

        [HttpPost]
        public ActionResult Index(string email, string password, bool? recordar)
        {
            var objUsu = InventarioDA.ObtenerPersona(email, password);
            int id = 0;
            string strMensaje = "El usuario y/o contraseña son incorrectos.";
            recordar = recordar == null ? false : true;
            if (objUsu != null)
            {
                Session["Session_Login"] = objUsu;
                strMensaje = Url.Content("~/Controllers/Inventario/PanelAcceso");
                return Json(new Response { IsSuccess = true, Message = strMensaje, Id = id }, JsonRequestBehavior.AllowGet);

            }
            else {
                return Json(new Response { IsSuccess = false, Message = strMensaje, Id = id }, JsonRequestBehavior.AllowGet);

            }
        }

        [AllowAnonymous]
        public ActionResult PanelAcceso()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel) Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Indicador1 = Indicador1();
            ViewBag.Indicador2 = Indicador2();
            return View();
        }

        [AllowAnonymous]
        public ActionResult Usuarios()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Usuarios";
            return View();
        }

        [AllowAnonymous]
        public ActionResult CrearUsuario()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Usuarios";
            return View();
        }

        [HttpPost]
        public ActionResult CrearUsuario(string nombre, string apellido,
            string email, int idRol, string usuario, string password)
        {
            int result = InventarioDA.CrearUsuario(nombre, apellido, email, idRol, usuario, password);
            
            string strMensaje = "";
            if (result == 1)
            {
                strMensaje = " Usuario existe";
                return Json(new Response { IsSuccess = false, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }
            else if (result == 2)
            {
                strMensaje = " Error Interno";
                return Json(new Response { IsSuccess = false, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }
            else {
                return Json(new Response { IsSuccess = true, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public ActionResult ActualizarUsuario(int idUsuario,string nombre, string apellido,
            string email, int idRol, string usuario, string password, int estado)
        {
            int result = InventarioDA.ActualizarUsuario(idUsuario,
                nombre, apellido, email, idRol, usuario, password, estado);

            string strMensaje = "";
            if (result == 1)
            {
                strMensaje = " Usuario existe";
                return Json(new Response { IsSuccess = false, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }
            else if (result == 2)
            {
                strMensaje = " Error Interno";
                return Json(new Response { IsSuccess = false, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new Response { IsSuccess = true, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }

        }

        [AllowAnonymous]
        public ActionResult ActualizarUsuario()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Productos";
            UsuarioModel user = (UsuarioModel)Session["ParameterUsuario"];
            ViewBag.nombre = user.Nombre;
            ViewBag.apellido = user.Apellido;
            ViewBag.usuario = user.Usuario;
            ViewBag.password = user.Contrasena;
            ViewBag.email = user.Email;
            ViewBag.estado = user.Estado;
            return View();
        }

        [AllowAnonymous]
        public ActionResult Productos()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Productos";
            return View();
        }

        [AllowAnonymous]
        public ActionResult ProductosInventarios()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Productos para Inventario";
            return View();
        }

        [AllowAnonymous]
        public ActionResult InventarioGeneral()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Inventario General";
            return View();
        }

        [AllowAnonymous]
        public ActionResult InventarioRotacionArticulo()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Inventario: Rotación de articulos";

            //Rotación de Articulos
            List<ReporteModel> list = new List<ReporteModel>();
            List<InventarioCategoriaModel> productosSemi = InventarioDA.ObtenerInventariosCategoria();
            List<InventarioCategoriaModel> productos = new List<InventarioCategoriaModel>();
            foreach (var o in productosSemi) {
                if (o.Tipo == 1) {
                    productos.Add(o);
                }
            }


            for (int i = 0; i < productos.Count(); i++) {
                ReporteModel obj = new ReporteModel();
                obj.Item = 1;
                obj.Stock = productos.ElementAt(i).Stock;
                obj.Salida = productos.ElementAt(i).Salida;
                obj.Descripcion = productos.ElementAt(i).Descripcion;
                obj.Fecha = productos.ElementAt(i).Fecha;
                if (obj.Salida != 0 && obj.Stock != 0)
                {
                    double promedio = (obj.Salida / obj.Stock);
                    decimal result = (Decimal)obj.Salida / (Decimal)obj.Stock;
                    float f = (float)result;
                    float fc = (float)Math.Round(f * 100f) / 100f;
                    if (fc != 0)
                    {
                        obj.Promedio = fc.ToString();
                    }
                    else {
                        obj.Promedio = "0.00";
                    }
                    
                }
                else
                {
                    obj.Promedio = "0.00";
                }

                list.Add(obj);
            }
            ViewBag.List = list;
            //
            return View();
        }

        public static string DoFormat(double myNumber)
        {
            var s = string.Format("{0:0.00}", myNumber);

            if (s.EndsWith("00"))
            {
                return ((int)myNumber).ToString();
            }
            else
            {
                return s;
            }
        }

        [AllowAnonymous]
        public ActionResult InventarioExactitud()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Inventario: Exactitud de producto";

            //Rotación de Articulos
            List<ReporteModel> list = new List<ReporteModel>();
            List<InventarioCategoriaModel> productosSemi = InventarioDA.ObtenerInventariosCategoria();
            List<InventarioCategoriaModel> productos = new List<InventarioCategoriaModel>();
            foreach (var o in productosSemi)
            {
                if (o.Tipo == 2)
                {
                    productos.Add(o);
                }
            }


            for (int i = 0; i < productos.Count(); i++)
            {
                ReporteModel obj = new ReporteModel();
                obj.Item = 1;
                obj.Stock = productos.ElementAt(i).Stock;
                obj.Salida = productos.ElementAt(i).Salida;
                obj.Descripcion = productos.ElementAt(i).Descripcion;
                obj.Fecha = productos.ElementAt(i).Fecha;
                if (obj.Salida != 0 && obj.Stock != 0)
                {
                    decimal result = ((Decimal)obj.Salida / (Decimal)obj.Stock ) * 100;
                    float f = (float)result;
                    float fc = (float)Math.Round(f * 100f) / 100f;
                    var roundedE =  Math.Round(fc, 0, MidpointRounding.AwayFromZero);

                    if (roundedE != 0)
                    {
                        obj.Promedio = roundedE.ToString();
                    }
                    else
                    {
                        obj.Promedio = "0";
                    }
                }
                else {
                    obj.Promedio = "0";
                }

                list.Add(obj);
            }
            ViewBag.List = list;
            //
            return View();
        }

        public static List<ReporteModel> Indicador1() {
            List<ReporteModel> list = new List<ReporteModel>();
            try
            {
                List<InventarioCategoriaModel> productosSemi = InventarioDA.ObtenerInventariosCategoria();
                List<InventarioCategoriaModel> productos = new List<InventarioCategoriaModel>();
                foreach (var o in productosSemi)
                {
                    if (o.Tipo == 1)
                    {
                        productos.Add(o);
                    }
                }


                for (int i = 0; i < productos.Count(); i++)
                {
                    ReporteModel obj = new ReporteModel();
                    obj.Item = 1;
                    obj.Stock = productos.ElementAt(i).Stock;
                    obj.Salida = productos.ElementAt(i).Salida;
                    obj.Descripcion = productos.ElementAt(i).Descripcion;
                    obj.Fecha = productos.ElementAt(i).Fecha;
                    if (obj.Salida != 0 && obj.Stock != 0)
                    {
                        double promedio = (obj.Salida / obj.Stock);
                        decimal result = (Decimal)obj.Salida / (Decimal)obj.Stock;
                        float f = (float)result;
                        float fc = (float)Math.Round(f * 100f) / 100f;
                        if (fc != 0)
                        {
                            obj.Promedio = fc.ToString();
                        }
                        else
                        {
                            obj.Promedio = "0.00";
                        }

                    }
                    else
                    {
                        obj.Promedio = "0.00";
                    }

                    list.Add(obj);
                }
                return list;
            }
            catch (Exception e) {
                return list;
            }
        }

        public static List<ReporteModel> Indicador2()
        {
            List<ReporteModel> list = new List<ReporteModel>();
            try
            {
                List<InventarioCategoriaModel> productosSemi = InventarioDA.ObtenerInventariosCategoria();
                List<InventarioCategoriaModel> productos = new List<InventarioCategoriaModel>();
                foreach (var o in productosSemi)
                {
                    if (o.Tipo == 2)
                    {
                        productos.Add(o);
                    }
                }


                for (int i = 0; i < productos.Count(); i++)
                {
                    ReporteModel obj = new ReporteModel();
                    obj.Item = 1;
                    obj.Stock = productos.ElementAt(i).Stock;
                    obj.Salida = productos.ElementAt(i).Salida;
                    obj.Descripcion = productos.ElementAt(i).Descripcion;
                    obj.Fecha = productos.ElementAt(i).Fecha;
                    if (obj.Salida != 0 && obj.Stock != 0)
                    {
                        decimal result = ((Decimal)obj.Salida / (Decimal)obj.Stock) * 100;
                        float f = (float)result;
                        float fc = (float)Math.Round(f * 100f) / 100f;
                        var roundedE = Math.Round(fc, 0, MidpointRounding.AwayFromZero);

                        if (roundedE != 0)
                        {
                            obj.Promedio = roundedE.ToString();
                        }
                        else
                        {
                            obj.Promedio = "0";
                        }
                    }
                    else
                    {
                        obj.Promedio = "0";
                    }

                    list.Add(obj);
                }
                return list;
            }
            catch (Exception e)
            {
                return list;
            }
        }

        [AllowAnonymous]
        public ActionResult CrearProducto()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Productos";
            return View();
        }

        [HttpPost]
        public ActionResult CrearProducto(string nombre, string descripcion,
            int idCategoria, int stock, float precio)
        {
            int result = InventarioDA.CrearProducto(nombre, descripcion, idCategoria, stock, precio);

            string strMensaje = "";
           if (result == 2)
            {
                strMensaje = " Error Interno";
                return Json(new Response { IsSuccess = false, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new Response { IsSuccess = true, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }

        }


        [AllowAnonymous]
        public ActionResult LogOut()
        {
            Session["Session_Login"] = null;
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            return View();
        }


        [HttpPost]
        public ActionResult InventarioProducto(int idProducto)
        {
            Models.Inventario.ProductoModel result = InventarioDA.ObtenerProducto(idProducto);
            Session.Remove("ParameterInventario");
            Session["ParameterInventario"] = result;
            return Json(new Response { IsSuccess = true, Message = "Exito", Id = 1 }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult CrearInventario()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Inventario";

            ViewBag.Producto = (ProductoModel)Session["ParameterInventario"];
            return View();
        }

        [AllowAnonymous]
        public ActionResult CrearInventario2()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "inventario");
            }

            UsuarioModel usuario = (UsuarioModel)Session["Session_Login"];
            ViewBag.Usuario = usuario;
            ViewBag.Active = 1;
            ViewBag.SubTitle = "Inventario";

            ViewBag.Producto = (ProductoModel)Session["ParameterInventario"];
            return View();
        }

        [HttpPost]
        public ActionResult CrearInventario(int idProducto, int ingreso, int salida, int stock, string descripcion)
        {
            int result = InventarioDA.CrearInventario(idProducto, ingreso, salida, stock, descripcion, 1);

            string strMensaje = "";
            if (result == 2)
            {
                strMensaje = " Error Interno";
                return Json(new Response { IsSuccess = false, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new Response { IsSuccess = true, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult CrearInventario2(int idProducto, int ingreso, int stock, string descripcion)
        {
            int result = InventarioDA.CrearInventario(idProducto, 0, ingreso, stock, descripcion, 2);

            string strMensaje = "";
            if (result == 2)
            {
                strMensaje = " Error Interno";
                return Json(new Response { IsSuccess = false, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new Response { IsSuccess = true, Message = strMensaje, Id = 1 }, JsonRequestBehavior.AllowGet);

            }
        }
    }
}
