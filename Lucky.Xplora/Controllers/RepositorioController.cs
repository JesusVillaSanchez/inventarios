﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Diagnostics;

using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Administrador;
using Lucky.Xplora.Models.BackusRepositorio;
using Lucky.Xplora.Security;

namespace Lucky.Xplora.Controllers
{
    public class RepositorioController : Controller
    {
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        string LocalInforme = Convert.ToString(ConfigurationManager.AppSettings["LocalInforme"]);
        //string UbicacionXplora = Convert.ToString(ConfigurationManager.AppSettings["xplora"]);

        //
        // GET: /BackusRepositorio/
        [CustomAuthorize]
        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.Tipo_Perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        [HttpPost]
        public ActionResult Listado_Informe(int anio, int mes, int cod_subcanal, int cod_tipoagrupamiento)
        {
            return View(
                    new E_Informesv2().Lista_Informe_NW(
                        new NW_ListarInformesCM_Request()
                        {
                            Company_id = ((Persona)Session["Session_Login"]).Company_id,
                            anio = anio,
                            mes = mes,
                            cod_subcanal = cod_subcanal,
                            cod_tipoagrupamiento = cod_tipoagrupamiento
                        }
                    )
                );
        }

        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 2015-06-25
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Descarga_Informe(string __a)
        {
            string _origen = "";
            string _destino = "";
            string _respuesta = "";
            string _error = "";

            try
            {
                _origen = Path.Combine(LocalInforme,__a);
                _destino = Path.Combine(LocalTemp, __a);

                System.IO.File.Copy(_origen, _destino, true);

                _respuesta = __a;
                _error = "Ok";
            }
            catch (Exception e)
            {
                _respuesta = "";
                _error = e.Message;
            }

            return new ContentResult
            {
                Content = "{ \"_a\": \"" + _respuesta + "\", \"_b\": \"" + _error + "\" }",
                ContentType = "application/json"
            };
        }

        #region inicio
        [HttpPost]
        public JsonResult getAnio(string campania_)
        {
            return Json(new Año().Lista(new Request_Anio_Por_Planning_Reports() { campania = campania_ }));
        }

        [HttpPost]
        public JsonResult getMes(string campania_,int anio)
        {
            return Json(new Lucky.Xplora.Models.Mes().Lista(new M_Request_Mes_Por_Planning_Reports_Anio() { id_planning = campania_, id_report = 216, anio = anio }));
        }

        [HttpPost]
        public JsonResult getReporte(string campania_)
        {
            return Json(new Reporte().Lista(new Request_GetReporte_Campania() { campania = campania_ }));
        }

        [HttpPost]
        public JsonResult getCanal(string campania_, int compania_)
        {
            return Json(new Canal().Lista(new Request_GetCanal_Compania_Campania() { compania = compania_, campania = campania_ }));
        }

        [HttpPost]
        public JsonResult getTipoCanal(string campania_)
        {
            return Json(new Tipo_Canal().Lista(new Request_GetTipoCanal_Campania() { campania = campania_ }));
        }
        
        [HttpPost]
        public JsonResult getCampania(string campania_)
        {
            return Json(new Campania().Lista(new Request_GetCampania_Compania() { compania = ((Persona)Session["Session_Login"]).Company_id }));
        }
        #endregion

        #region New Repositorio

        public ActionResult Repositorio() {
            return View();
        }
        public ActionResult RpElemento() {
            return View();
        }
        public ActionResult RpHistorico() {
            return View();
        }
        #endregion

    }
}