﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora.Models;
using Lucky.Xplora.Models.HenkelReporting;
using Lucky.Xplora.Models.Administrador;

namespace Lucky.Xplora.Controllers
{
    public class HenkelReportingController : Controller
    {
        //
        // GET: /HenkelReporting/

        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult VisibilidadIndex()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();

        }

        public ActionResult EvolutionaryVisIndex()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        public ActionResult ComparativeSobIndex()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult EvolutiveEffectiveness()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion;yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult DetailEffectiveness()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion;
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult ExhibitionSummary()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult Split()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult EvolutiveAddElements()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion;yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult RateNodeCommercial()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult Database()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
           
            return View();
        }

        public ActionResult ExecutiveSummary()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        [HttpPost]
        public ActionResult ListadoActividad(string planning, int anio, int mes, int cadena,string nomcadena, string categoria)
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            ViewBag.cadena = nomcadena;

            return View(

                  new Actividades().Lista(new Request_GetActividades()
                {
                    id_planning = planning,
                    anio = anio,
                    mes = mes,
                    cadena = cadena,
                    categoria = categoria
                }));



        }

        [HttpPost]
        public ActionResult GetAgrupMat(string __a, int __b)
        {
            return View(
                    new E_AgrupMaterial().Lista(new Request_AgrupMat()
                    {
                        id_planning = __a,
                        agruptipomat = __b
                    })
                );
        }

        [HttpPost]
        public ActionResult GetPeriodo(string __a, int __b, int __c, int __d, int __e)
        {
            return View(
                     new Periodo().Lista(new Request_GetPeriodo_Campania_Reporte_Anio_Mes_Formato()
                     {
                         campania = __a ,
                         reporte = __b ,
                         anio = __c ,
                         mes = __d ,
                         formato = __e
                     })
                   );
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-18
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetMarca(string __a, string __b, string __c)
        {
            return PartialView(
                "../Alicorp/GetMarca",
                new Marca().Lista(new Request_GetMarca_Opcion_Campania_Categoria()
                {
                    opcion = Convert.ToInt32(__a),
                    campania = __b,
                    categoria = Convert.ToInt32(__c)
                })
            );
        }

        /// <summary>
        /// Autor: curiarte
        /// Fecha: 2015-09-22
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetComparativeSobPost(string __a, int __b, int __c ,int __d, int __e, int __f)
        {

            return View(
                    new BI_ElementosVisb().ListaCompa(
                            new Request_GetElementosVisbComparativo()
                            {
                                id_planning =  __a,
                                periodo = __b,
                                ciudad = __c,
                                categoria = __d,
                                agruptipomat = __e,
                                agrupmat = __f
                            }
                        )
                        );
 
        }

        public ActionResult GetArrendados(string __a, int __b, int __c, int __d, int __e, int __f)
        {
            return View(
                    new E_Arrendados().Lista(
                            new Request_ElemVisbArrendados()
                            {
                                id_planning = __a,
                                periodo = __b,
                                categoria = __c,
                                cadena = __d,
                                agruptipomat = __e,
                                agrupmat = __f
                            })
                       );
        }

        #region OOS
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-16
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OOSCategoria()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
                       
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-16
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OOSCategoria(string __a, string __b, string __c) {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Henkel_AASS_OOS().Lista(new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = 0,
                        marca = 0,
                        cadena=0,
                        opcion = 0
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-16
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OOSCadena()
        {

            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-16
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OOSCadena(string __a, string __b, string __c, string __d)
        {

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Henkel_AASS_OOS().Lista(new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = Convert.ToInt32(__d),
                        marca = 0,
                        cadena = 0,
                        opcion = 1
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-18
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OOSEvolutivoProducto()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-18
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OOSEvolutivoProducto(string __a, string __b, string __c, string __d, string __e)
        {
            return View(new Henkel_AASS_OOS().Objeto(
                    new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = Convert.ToInt32(__d),
                        marca = Convert.ToInt32(__e),
                        cadena = 0,
                        opcion = 2
                    }
                ));
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-20
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OOSEvolutivoCadena()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-20
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OOSEvolutivoCadena(string __a, string __b, string __c, string __d, string __e, string __f)
        {
            return View(new Henkel_AASS_OOS().Objeto(
                    new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = Convert.ToInt32(__d),
                        marca = Convert.ToInt32(__e),
                        cadena = Convert.ToInt32(__f),
                        opcion = 3
                    }
                ));
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-21
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OOSValorizadoGeneral()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-21
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OOSValorizadoGeneral(string __a, string __b, string __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Henkel_AASS_OOS().Lista(new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = 0,
                        marca = 0,
                        cadena = 0,
                        opcion = 4
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-22
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OOSValorizadoCadena()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-22
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OOSValorizadoCadena(string __a, string __b, string __c, string __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Henkel_AASS_OOS().Lista(new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = Convert.ToInt32(__d),
                        marca = 0,
                        cadena = 0,
                        opcion = 5
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-22
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OOSValorizadoProducto()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-22
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OOSValorizadoProducto(string __a, string __b, string __c, string __d, string __e, string __f)
        {
            return View(new Henkel_AASS_OOS().Objeto(
                    new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = Convert.ToInt32(__d),
                        marca = Convert.ToInt32(__e),
                        cadena = Convert.ToInt32(__f),
                        opcion = 6
                    }
                ));
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-23
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OOSBase()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion;
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-23
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OOSBaseDato(string __a, string __b, string __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Henkel_AASS_OOS().Base(new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = 0,
                        marca = 0,
                        cadena = 0,
                        opcion = 7
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-22
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        //public ActionResult Filtros(bool categoria, bool marca, bool cadena, string campania, int tipo_perfil){
        public ActionResult Filtros(Henkel_AASS_OOS_Filtro __a){
            //return View(new Henkel_AASS_OOS_Filtro() { 
            //        categoria = categoria, 
            //        marca = marca, 
            //        cadena = cadena ,
            //        campania = campania,
            //        tipo_perfil = tipo_perfil
            //    });
            return View(__a);
        }
        #endregion

        #region Exhibicion
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExhibicionResumen()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExhibicionResumen(string __a, string __b, string __c, string __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Henkel_AASS_Exhibicion_Grafico().Grafico(new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = 0,
                        cadena = 0,
                        oficina = Convert.ToInt32(__d),
                        opcion = 0
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExhibicionSplit()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExhibicionSplit(string __a, string __b, string __c, string __d, string __e)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Henkel_AASS_Exhibicion_Grafico().Grafico(new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = Convert.ToInt32(__d),
                        cadena = 0,
                        oficina = Convert.ToInt32(__e),
                        opcion = 1
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExhibicionEvolutivo()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExhibicionEvolutivo(string __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Henkel_AASS_Exhibicion_Grafico().Grafico(new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = Convert.ToInt32(__d),
                        cadena = Convert.ToInt32(__e),
                        oficina = Convert.ToInt32(__f),
                        opcion = 2
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExhibicionTarifario()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion;  yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-25
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExhibicionTarifario(string __a, string __b, string __c)
        {
            return View(new Henkel_AASS_Exhibicion_Grafico().Tarifario(
                new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = 0,
                        cadena = 0,
                        oficina = 0,
                        opcion = 3
                    }));
        }

        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 2015-09-26
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExhibicionBase()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.Tipo_Perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 2015-09-26
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExhibicionBaseDato(string __a, string __b, string __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Henkel_AASS_Exhibicion_Grafico().BD(new Request_HenkelAASS_Parametros()
                    {
                        anio = Convert.ToInt32(__a),
                        mes = Convert.ToInt32(__b),
                        periodo = Convert.ToInt32(__c),
                        categoria = 0,
                        marca = 0,
                        cadena = 0,
                        opcion = 4
                    })),
                ContentType = "application/json"
            }.Content);
        }
        #endregion

        #region Resumen General - Share of Exhibicion

        public ActionResult GetDetalleEfectividad(string _id_planning, int _periodo, int _categoria, int _agruptipomat)
        {
            return View(new E_DetalleEfectividad().DetailEffectiveness(
                          new Request_GetEfectividad()
                          {
                              id_planning = _id_planning,
                              periodo = _periodo,
                              categoria = _categoria,
                              agruptipomat = _agruptipomat

                          }));
        }

        [HttpPost]
        public ActionResult GetGraficoElemtResum(string planning, int periodo, int ciudad, int categoria, int agruptipoma, int lectura)
        {
            ViewBag.vglectura = lectura;
            return View(
                    new BI_ElementosVisb().Lista(
                            new Request_GetElementosVisbResum()
                            {
                                id_planning = planning ,
                                periodo = periodo,
                                ciudad = ciudad,
                                categoria = categoria ,
                                agruptipomat = agruptipoma,
                                lectura = lectura
                            }
                        )
                        );
        }

        public ActionResult GetGraficoElemtResumTotal(string planning, int anio, int mes, int ciudad, int categoria, int agruptipoma, int lectura)
        {
            ViewBag.vglectura = lectura;
            return View(new BI_ElementosVisb().ListaTotal(
                                new Request_GetElementosVisbResumTotal()
                                {
                                    id_planning = planning ,
                                    anio = anio ,
                                    mes = mes ,
                                    ciudad = ciudad,
                                    categoria = categoria ,
                                    agruptipomat = agruptipoma,
                                    lectura = lectura

                                })
                                );
        }

        [HttpPost]
        public ActionResult GetGraficoElemntEvolutivoCiuCat(string id_planning, int anio, int mes, int ciudad, int categoria, int cadena, int agruptipomat, int agrupmat, int lectura,int periodo)
        {
            ViewBag.vglectura = lectura;
            return View(new BI_ElementosVisb().ListaEvol(
                            new Request_GetElementosVisbEvolutivoCiuCat()
                            {
                                id_planning = id_planning,
                                anio = anio,
                                mes = mes ,
                                ciudad = ciudad ,
                                categoria = categoria ,
                                cadena = cadena,
                                agruptipomat = agruptipomat ,
                                agrupmat = agrupmat ,
                                lectura = lectura,
                                periodo=periodo

                            })
                            );
        }
        #endregion

        #region <<< SOS >>>

        public ActionResult SOS_Resumen(string _a, string _b) {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil =  ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
            ViewBag.campania = _a;
        
            return View();
        }
        public ActionResult PRT_SOS_Resumen(int _anio,int _mes,string _equipo,int _oficina,int _categoria,int _lectura){
            ViewBag.vglectura = _lectura;
            return View(new E_SOS().SOS_Resumen(
                    new Request_Henkel_SOS()
                    {
                        Cod_Anio = _anio,
                        Cod_Mes = _mes,
                        Cod_Perfil = ((Persona)Session["Session_Login"]).tpf_id,
                        Cod_Equipo = _equipo,
                        Cod_Oficina = _oficina,
                        Cod_Categoria = _categoria,
                        Lectura =_lectura
                    }
                ));
        }
        public ActionResult SOS_Evolutivo(string _a, string _b)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
            ViewBag.campania = _a;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        public ActionResult PRT_SOS_Evolutivo(int _anio,int _mes, string _equipo,int _oficina, int _cadena,int _categoria, int _lectura) {
            ViewBag.vglectura = _lectura;
            return View(new E_SOS().SOS_Evolutivo(
                    new Request_Henkel_SOS()
                    {
                        Cod_Anio = _anio,
                        Cod_Mes = _mes,
                        Cod_Perfil = ((Persona)Session["Session_Login"]).tpf_id,
                        Cod_Equipo = _equipo,
                        Cod_Oficina = _oficina,
                        Cod_Cadena = _cadena,
                        Cod_Categoria = _categoria,
                        Lectura = _lectura
                    }
                ));
        }
        public ActionResult SOS_Evolutivo_Marca(string _a, string _b)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
            ViewBag.campania = _a;
            
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        public ActionResult PRT_SOS_Evolutivo_Marca(int _anio, int _mes, string _equipo, int _oficina, int _cadena, int _categoria, int _lectura)
        {
            ViewBag.vglectura = _lectura;
            return View(new E_SOS().SOS_Evolutivo_Marca(
                    new Request_Henkel_SOS()
                    {
                        Cod_Anio = _anio,
                        Cod_Mes = _mes,
                        Cod_Perfil = ((Persona)Session["Session_Login"]).tpf_id,
                        Cod_Equipo = _equipo,
                        Cod_Oficina = _oficina,
                        Cod_Cadena = _cadena,
                        Cod_Categoria = _categoria,
                        Lectura = _lectura
                    }
                ));
        }
        public ActionResult SOS_BD(string _a, string _b)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
            ViewBag.campania = _a;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
             
            return View();
        }
        public ActionResult PRT_SOS_BD(int _anio, int _mes, string _equipo, int _cadena, int _categoria, int _lectura,string _nomcadena)
        {
            ViewBag.vglectura = _lectura;
            ViewBag.vnomcadena = _nomcadena;
            return View(new E_SOS().SOS_BD(
                    new Request_Henkel_SOS()
                    {
                        Cod_Anio = _anio,
                        Cod_Mes = _mes,
                        Cod_Perfil = ((Persona)Session["Session_Login"]).tpf_id,
                        Cod_Equipo = _equipo,
                        Cod_Cadena = _cadena,
                        Cod_Categoria = _categoria,
                        Lectura = _lectura
                    }
                ));
        }
        
        #endregion

        #region Fotografico
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-23
        /// </summary>
        /// <param name="_a"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Fotografico(string _a)
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return RedirectToAction("Fotografico", "BackusReporting", new { _a = _a });
        }
        #endregion

        #region << Informe Precios >>

        public ActionResult InformePrecios() {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-22
        /// </summary>
        /// <param name="_vciudad"></param>
        /// <param name="_vcadena"></param>
        /// <param name="_vperiodo"></param>
        /// <returns></returns>
        public ActionResult PrecioResumenColor(int _vciudad, int _vcadena, int _vperiodo)
        {
            Henkel_AASS_Precios_Request oParametros = new Henkel_AASS_Precios_Request();
            oParametros.cod_ciudad = _vciudad;
            oParametros.cod_cadena = _vcadena;
            oParametros.cod_periodo = _vperiodo;

            return View(new Henkel_AASS_Precios().Lista_HenkelAASS_Precio(
                        new Request_HenkelAASS_Precios() { oParametros = oParametros }
                    ));

        }

        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 2015-09-23
        /// </summary>
        /// <param name="_vciudad"></param>
        /// <param name="_vcadena"></param>
        /// <param name="_vperiodo"></param>
        /// <returns></returns>
        public ActionResult PrecioResumenHBO(int _vciudad, int _vcadena, int _vperiodo)
        {
            Henkel_AASS_Precios_Request oParametros = new Henkel_AASS_Precios_Request();
            oParametros.cod_ciudad = _vciudad;
            oParametros.cod_cadena = _vcadena;
            oParametros.cod_periodo = _vperiodo;

            return View(new Henkel_AASS_Precios_HBO().Lista_HenkelAASS_Precio_HBO(
                        new Request_HenkelAASS_Precios() { oParametros = oParametros }
                    ));

        }

        #endregion

        #region << Informe Incidencias >>

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-23
        /// </summary>
        /// <returns></returns>
        public ActionResult ResumenIncidencia() {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 2015-09-24
        /// </summary>
        /// <param name="_vperiodo"></param>
        /// <param name="_vactividad"></param>
        /// <returns></returns>
        public ActionResult ResumenIncidenciaGrafico(int _vperiodo, int _vactividad)
        {
            Henkel_AASS_Incidencia_Request oParametros = new Henkel_AASS_Incidencia_Request();
            oParametros.cod_periodo = _vperiodo;
            oParametros.cod_actividad = _vactividad;

            return View(new Henkel_AASS_Incidencia_Resumen().Lista_HenkelAASS_Incidencia(
                        new Request_HenkelAASS_Incidencia() { oParametros = oParametros }
                    ));

        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-24
        /// </summary>
        /// <returns></returns>
        public ActionResult DetalleIncidencia() {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-24
        /// </summary>
        /// <param name="_vperiodo"></param>
        /// <param name="_vactividad"></param>
        /// <param name="_vcadena"></param>
        /// <param name="_voficina"></param>
        /// <returns></returns>
        public ActionResult DetalleIncidenciaGrafico(int _vperiodo, int _vactividad, int _vcadena, int _voficina)
        {
            Henkel_AASS_Incidencia_Request oParametros = new Henkel_AASS_Incidencia_Request();
            oParametros.cod_periodo = _vperiodo;
            oParametros.cod_actividad = _vactividad;
            oParametros.cod_cadena = _vcadena;
            oParametros.cod_oficina = _voficina;

            List<Henkel_AASS_Incidencia_Detalle> oLs = new Henkel_AASS_Incidencia_Detalle().Lista_HenkelAASS_IncidenciaDetalle(
                        new Request_HenkelAASS_Incidencia() { oParametros = oParametros });
             

            return Json(new { DtIncidencia = oLs });

        }

        #endregion
    }
}