﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
//using Lucky.Data;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Reporting;
using Lucky.Xplora.Models.ColgateReporting;
using System.IO;
using Lucky.Business.Common.Servicio;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using Lucky.Xplora.Models.Administrador;

namespace Lucky.Xplora.Controllers
{
    public class ColgateReportingController : Controller
    {
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

        //
        // GET: /ColgateReporting/
        private static Persona _Persona;
        //private Conexion oCoon = new Conexion(1);
        public ActionResult Index()
        {
            //if (Session["Session_Login"] == null)
            //{
            //    return RedirectToAction("login", "LogOn");

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            //}
            return View();
        }

        #region <<< Reporte Precio Colgate Fusion >>>

        public ActionResult VWPrecio(string _a)
        {
            if (Session["Session_Login"] == null)
            { return RedirectToAction("login", "LogOn"); }
            else
            {
                _Persona = (Persona)Session["Session_Login"];
            }

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            else
            {
                ViewBag.campania = "";
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Perfil = _Persona.Perfil_id.ToString();   //"170935";
            o_Parametros.Cod_Equipo = _a;   //"0133725102010";
            o_Parametros.Cod_Reporte = 19;
            ViewBag.c_codequipo = _a;
            return View(
            new E_NW_RPG_TABLA().Consul_PametrosIni(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));

        }

        [HttpPost]
        public ActionResult vGrafLinePrecio(string _a, string _b, string _c, string _d, string _e)
        {
            ViewBag.gf_titulo = _a;
            ViewBag.gf_id = _b;
            ViewBag.gf_serie = _c;
            ViewBag.gf_categorias = _d;
            ViewBag.gf_maximo = _e;
            return View();
        }

        [HttpPost]
        public ActionResult PRT_vGrafColumnPrecio(string _a, string _b, string _c, string _d)
        {
            ViewBag.gf_titulo = _a;
            ViewBag.gf_id = _b;
            ViewBag.gf_serie = _c;
            ViewBag.gf_categorias = _d;
            return View();
        }

        [HttpPost]
        public ActionResult PRT_PrecioSumaryColgFusion(string _a, int _b, int _c, int _d, int _e)
        {
            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();

            o_Parametros.Cod_Perfil = _Persona.Perfil_id.ToString();   //"170935";
            o_Parametros.Cod_Equipo = _a;   //"0133725102010";
            o_Parametros.Cod_Periodo = _b;  //22314;
            o_Parametros.Cod_Oficina = _c;  //7;
            o_Parametros.Cod_Zona = _d;     //0;
            o_Parametros.Cod_Mercado = _e;  //0;

            return View(new E_NW_RPG_TABLA().Consul_TB_Precio_Sumary(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }

        [HttpPost]
        public ActionResult PRT_PrecioCategoriaColgFusion(string _a, int _b, int _c, int _d, int _e, string _f)
        {

            ViewBag.perfil = _Persona.Perfil_id;
            ViewBag.equipo = _a;
            ViewBag.periodo = _b;
            ViewBag.oficina = _c;
            ViewBag.zona = _d;
            ViewBag.mercado = _e;
            ViewBag.categoria = _f;


            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Perfil = _Persona.Perfil_id.ToString(); // "170935";
            o_Parametros.Cod_Equipo = _a;//"0133725102010";
            o_Parametros.Cod_Periodo = _b;//22314;
            o_Parametros.Cod_Oficina = _c;//7;
            o_Parametros.Cod_Zona = _d; //0 ;
            o_Parametros.Cod_Mercado = _e;//0;
            o_Parametros.Cod_Categoria = _f; //"9993";
            return View(new E_NW_RPG_TABLA().Consul_TB_Precio_Categoria(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }
        [HttpPost]
        public ActionResult PRT_PrecioCatgComplianceColgFusion(string _a, int _b, int _c, int _d, int _e, string _f, string _g)
        {

            ViewBag.perfil = _Persona.Perfil_id;
            ViewBag.equipo = _a;
            ViewBag.periodo = _b;
            ViewBag.oficina = _c;
            ViewBag.zona = _d;
            ViewBag.mercado = _e;
            ViewBag.categoria = _f;


            E_Hig_Grafico_Parametros o_Parametros = new E_Hig_Grafico_Parametros();
            o_Parametros.Cod_Perfil = _Persona.Perfil_id.ToString();//"170935";
            o_Parametros.Cod_Equipo = _a; //"0133725102010";
            o_Parametros.Cod_Periodo = _b; //22314;
            o_Parametros.Cod_Oficina = _c; //7;
            o_Parametros.Cod_Zona = _d; //0;
            o_Parametros.Cod_Mercado = _e; //0;
            o_Parametros.Cod_Categoria = _f;//"9993";
            ViewBag.v_grafNom_periodo = _g;
            return View(new E_NW_RPG_TABLA().Consul_TB_Precio_Compliance(new Request_NW_Reporting()
            {
                oParametros = o_Parametros
            }));
        }

        #endregion

        #region RQ Lucky 240 v1.3 Reporte de Visibilidad (POP)

        /* Perfiles:
         * - "001" - Administrador(Multicanal)
         * - "002" - Analista(Monocanal)
         */

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        //[HttpPost]
        public ActionResult ReportingVisibilidadPorPeriodo(string planning, string periodo, string oficina, string sector, string mercado)
        {
            ViewBag.Campania = planning;
            ViewBag.Periodo = periodo;
            ViewBag.Oficina = oficina;
            ViewBag.Sector = sector;
            ViewBag.Mercado = mercado;

            return View();
        }


        [HttpPost]
        public ActionResult vGrafLineVisibilidad(string _a, string _b, string _c, string _d)
        {
            //ViewBag.gf_titulo = "Sari Sari Display";
            //ViewBag.gf_id = "16";
            //ViewBag.gf_serie = "[{ name: 'COLGATE PALMOLIVE PERU SAC', data: [88,86,86,84,83,84,0,87] }]";
            //ViewBag.gf_categorias = "['06/09/2015','13/09/2015','20/09/2015','27/09/2015','04/10/2015','11/10/2015','18/10/2015','01/11/2015']";

            ViewBag.gf_titulo = _a;
            ViewBag.gf_id = _b;
            ViewBag.gf_serie = _c;
            ViewBag.gf_categorias = _d;
            return View();
        }


        public ActionResult ReportingVisibilidad(string planning, string periodo, string oficina, string sector, string mercado)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            else
            {
                _Persona = (Persona)Session["Session_Login"];
            }

            ViewBag.Campania = planning;
            ViewBag.Periodo = periodo;
            ViewBag.Oficina = oficina;
            ViewBag.Sector = sector;
            ViewBag.Mercado = mercado;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            else
            {
                ViewBag.campania = "";
            }
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }


        #endregion

        #region Presencia(Producto)

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-11
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportingPresencia()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            else
            {
                ViewBag.campania = "";
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-14
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCampania()
        {
            return PartialView(
                "../LuckyMaps/GetCampania",
                new Campania().Lista(new Request_GetCampania_Opcion()
                {
                    opcion = 74
                }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-11
        /// </summary>
        /// <param name="__a">campania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAnio(string __a)
        {
            return View(new Año().Lista(new Request_Anio_Por_Planning_Reports()
            {
                campania = __a
            }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-11
        /// </summary>
        /// <param name="__a">compania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetOficina(string __a)
        {
            return View(
                    new Oficina().Lista(new Request_Listar_Oficinas_Por_CodCompania_Reports()
                    {
                        compania = __a
                    })
                );
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-11
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetPeriodo(string __a, string __b)
        {
            return View(new Periodo().Lista(new Request_GetPeriodo_Anio_Campania_TipoPerfil()
            {
                anio = Convert.ToInt32(__a),
                campania = __b,
                tipoperfil = ((Persona)Session["Session_Login"]).tpf_id
            }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-11
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetSector(string __a)
        {
            return View(new Sector().Lista(new Request_Listar_Sector_OficinaEQ_Reports()
            {
                //compania = ((Persona)Session["Session_Login"]).Company_id,
                compania = 1561,
                oficina = Convert.ToInt32(__a),
            }));
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-02-12
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetNodoComercial(string __a, string __b, string __c)
        {
            return PartialView(
                "../Alicorp/GetNodoComercial",
                new NodoComercial().Lista(new Request_GetCadena_Campania_Compania_Oficina_Sector()
                {
                    campania = __a,
                    compania = ((Models.Persona)Session["Session_Login"]).Company_id,
                    oficina = Convert.ToInt32(__b),
                    sector = Convert.ToInt32(__c)
                }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-02-12
        /// Desc.: NodoComercial por Compania, Oficina y Sector
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        //public ActionResult GetPresencia_Summary(string __a, int __b, int __c, int __d, int __e, int __f)
        public ActionResult GetPresencia_Summary()
        {
            //if (__b == 1) {
            //    __b = 0;
            //}

            //ViewBag.categoria = __b;

            return View(
                    //new Presencia_Summary().Summary(
                    //    new Request_Presencia_Summary()
                    //    {
                    //        campania = __a,
                    //        categoria = __b,
                    //        periodo = __c,
                    //        oficina = __d,
                    //        sector = __e,
                    //        nodocomercial = __f
                    //    }
                    //)
                );
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-07-07
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonPresencia_Summary_Objetivo(string __a, int __b, int __c, int __d, int __e, int __f)
        {
            if (__b == 1)
            {
                __b = 0;
            }

            ViewBag.categoria = __b;

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Presencia_Summary().SummaryObjetivo(
                    new Request_Presencia_Summary()
                    {
                        campania = __a,
                        categoria = __b,
                        periodo = __c,
                        oficina = __d,
                        sector = __e,
                        nodocomercial = __f
                    }
                )),
                ContentType = "application/json"
            }.Content);

        }


        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-07-08
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonPresencia_Summary_Product(string __a, int __b, int __c, int __d, int __e, int __f)
        {
            if (__b == 1)
            {
                __b = 0;
            }

            ViewBag.categoria = __b;

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Presencia_Summary().SummaryProduct(
                    new Request_Presencia_Summary()
                    {
                        campania = __a,
                        categoria = __b,
                        periodo = __c,
                        oficina = __d,
                        sector = __e,
                        nodocomercial = __f
                    }
                )),
                ContentType = "application/json"
            }.Content);

        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-07-08
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonPresencia_Summary_Rango(string __a, int __b, int __c, int __d, int __e, int __f)
        {
            if (__b == 1)
            {
                __b = 0;
            }

            ViewBag.categoria = __b;

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Presencia_Summary().SummaryRango(
                    new Request_Presencia_Summary()
                    {
                        campania = __a,
                        categoria = __b,
                        periodo = __c,
                        oficina = __d,
                        sector = __e,
                        nodocomercial = __f
                    }
                )),
                ContentType = "application/json"
            }.Content);

        }


        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-07-09
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JsonPresencia_Category_Product(string __a, int __b, int __c, int __d, int __e, int __f)
        {
            if (__b == 1)
            {
                __b = 0;
            }

            ViewBag.categoria = __b;

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Presencia_Summary().productByCategory(
                    new Request_Presencia_Summary()
                    {
                        campania = __a,
                        categoria = __b,
                        periodo = __c,
                        oficina = __d,
                        sector = __e,
                        nodocomercial = __f
                    }
                )),
                ContentType = "application/json"
            }.Content);

        }

        #endregion

        #region RQ Lucky 240 v1.3 Comentarios

        public ActionResult ReportingComentarios()
        {
            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            else
            {
                ViewBag.campania = "";
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            
            return View();
        }

         [HttpPost, ValidateInput(false)]
         public ActionResult ListaCommentsColgateFusion(int periodo, string planning)
         {
             return new ContentResult
             {
                 Content = MvcApplication._Serialize(
                     new E_Comentarios().Lista(new InsertComentarios_Request()
                     {
                         periodo = periodo,
                         planning = planning
                     })
                 ),
                 ContentType = "application/json"
             };


         }


        #endregion

        #region RQ Lucky 240 v1.3 DataBase

        public ActionResult ReportingDataBase()
        {
            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            else
            {
                ViewBag.campania = "";
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        [HttpPost]
        public ActionResult ReportingDataBaseConsolidado(string planning, string periodo, string filtro)
        {
            ViewBag.Campania = planning;
            ViewBag.Periodo = periodo;
            ViewBag.Filtro = filtro;

            return View(
                new ColgateConsolidado_DataBase().Lista(
                new Request_ColgateConsolidado_DataBase_Reports()
                {
                    planning = planning,
                    periodo = periodo,
                    filtro = filtro
                }
            ));
        }

        [HttpPost]
        public ActionResult ReportingDataBaseConsolidadoPrecio(string planning, string periodo, string filtro)
        {
            ViewBag.Campania = planning;
            ViewBag.Periodo = periodo;
            ViewBag.Filtro = filtro;

            return View(
                new ColgateConsolidado_DataBase().Lista(
                new Request_ColgateConsolidado_DataBase_Reports()
                {
                    planning = planning,
                    periodo = periodo,
                    filtro = filtro
                }
            ));
        }

        [HttpPost]
        public JsonResult ExportaExcelConsolidado(string planning, string periodo, string filtro)
        {
            string _fileServer = "";
            string _filePath = "";
            //int _fila = 0;
            string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

            _fileServer = String.Format("reporteConsolidado_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Consolidado >>
                List<ColgateConsolidado_DataBase> oConsolidado = new ColgateConsolidado_DataBase().Lista(
                    new Request_ColgateConsolidado_DataBase_Reports()
                    {
                        planning = planning,
                        periodo = periodo,
                        filtro = filtro
                    }
                );
                #endregion
                #endregion

                #region <<< Reporte Consolidado >>>


                BL_GES_Operativa oOperativa = new BL_GES_Operativa();
                DataTable oDt = oOperativa.BL_Colgate_Fusion_Consolidado_Detalle(planning, Convert.ToInt32(periodo), filtro);

                string nom_hoja = "";
                if (filtro == "2")
                {
                    nom_hoja = "SUPERVISOR";
                }
                else
                {
                    nom_hoja = "GIE";
                }

                int cta_p_col = 0, cta_p_com = 0, cta_v_col = 0, cta_v_com = 0;
                //string v_p_col = "pres. colgate";
                //string v_p_com = "pres. competencia";
                //string v_v_col = "visibilidad colgate";
                //string v_v_com = "visibilidad competencia";
                int cnt = 0, cnt2 = 1, cnt3 = 11;//9;
                if ((oConsolidado != null) || (oConsolidado.Count > 0))
                {
                    Excel.ExcelWorksheet oWsConsolidado = oEx.Workbook.Worksheets.Add("ReporteConsolidado_" + nom_hoja);

                    foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                    {
                        // oWsConsolidado.Cells[_fila, 1].Value = oBj.tmp_cabecera;
                        if (oBj.tmp_codigo.ToString() == "04")
                        {
                            cta_p_col++;
                        }
                        if (oBj.tmp_codigo.ToString() == "05")
                        {
                            cta_p_com++;
                        }
                        if (oBj.tmp_codigo.ToString() == "03")
                        {
                            cta_v_col++;
                        }
                        if (oBj.tmp_codigo.ToString() == "06")
                        {
                            cta_v_com++;
                        }
                    }

                    foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                    {
                        cnt++;
                        if (oBj.tmp_codigo.ToString() == "03" ||
                            oBj.tmp_codigo.ToString() == "04" ||
                            oBj.tmp_codigo.ToString() == "05" ||
                            oBj.tmp_codigo.ToString() == "06")
                        {
                            string v_imprime = "";
                            v_imprime = oBj.tmp_cabecera.ToString();
                            if (v_imprime.Length > 4)
                            {
                                string v_imprime2 = oBj.tmp_descripcion.ToString().ToUpper();
                                oWsConsolidado.Cells[2, cnt].Value = v_imprime2;
                            }
                        }
                    }

                    using (ExcelRange rng = oWsConsolidado.Cells[1, 11, 1, cnt])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    using (ExcelRange rng = oWsConsolidado.Cells[2, 11, 2, cnt])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    oWsConsolidado.Cells[3, 10].Value = "CANTIDAD";
                    using (ExcelRange rng = oWsConsolidado.Cells[3, 10, 3, 10])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    int cnt_cant = 1;
                    foreach (DataColumn oCols2 in oDt.Columns)
                    {

                        string v_imprime = "";
                        v_imprime = oCols2.ColumnName.ToString();
                        if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                        {
                            int total = 0;
                            foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                            {
                                string v_compara = oBj.tmp_cabecera.ToString();
                                if (v_imprime.ToString() == v_compara)
                                {
                                    foreach (DataRow oFils in oDt.Rows)
                                    {
                                        var valor2 = oFils[oCols2].ToString();
                                        if (valor2 != "")
                                        {
                                            total++;
                                        }
                                    }
                                    break;
                                }
                            }
                            oWsConsolidado.Cells[3, cnt_cant].Value = total;
                        }
                        cnt_cant++;
                    }


                    oWsConsolidado.Cells[4, 10].Value = "MAXIMO";
                    using (ExcelRange rng = oWsConsolidado.Cells[4, 10, 4, 10])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    int cnt_max = 1;
                    foreach (DataColumn oCols2 in oDt.Columns)
                    {
                        string v_imprime = "";
                        v_imprime = oCols2.ColumnName.ToString();
                        if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                        {
                            Double max = 0.00;
                            foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                            {

                                string v_compara = oBj.tmp_cabecera.ToString();
                                if (v_imprime.ToString() == v_compara)
                                {


                                    foreach (DataRow oFils in oDt.Rows)
                                    {
                                        var valor2 = oFils[oCols2].ToString();
                                        if (valor2 != "")
                                        {
                                            if (Convert.ToDouble(valor2) > max)
                                            {
                                                max = Convert.ToDouble(valor2);
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                            max = Math.Round(max, 2);
                            oWsConsolidado.Cells[4, cnt_max].Value = max;
                        }
                        cnt_max++;
                    }

                    oWsConsolidado.Cells[5, 10].Value = "MINIMO";
                    using (ExcelRange rng = oWsConsolidado.Cells[5, 10, 5, 10])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    int cnt_min = 1;
                    foreach (DataColumn oCols2 in oDt.Columns)
                    {
                        string v_imprime = "";
                        v_imprime = oCols2.ColumnName.ToString();
                        if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                        {
                            Double min = 999.99;
                            foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                            {


                                string v_compara = oBj.tmp_cabecera.ToString();
                                if (v_imprime.ToString() == v_compara)
                                {
                                    foreach (DataRow oFils in oDt.Rows)
                                    {
                                        var valor2 = oFils[oCols2].ToString();
                                        Double val = 0.00;
                                        if (valor2 != "")
                                        {
                                            val = Convert.ToDouble(valor2);
                                            if (val < min)
                                            {
                                                min = val;

                                            }
                                        }

                                    }
                                    break;
                                }
                            }

                            if (min == 999.99)
                            {
                                min = 0;
                            }

                            min = Math.Round(min, 2);
                            oWsConsolidado.Cells[5, cnt_min].Value = min;
                        }
                        cnt_min++;
                    }


                    oWsConsolidado.Cells[6, 10].Value = "MODA";
                    using (ExcelRange rng = oWsConsolidado.Cells[6, 10, 6, 10])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    int cnt_mod = 1;
                    foreach (DataColumn oCols2 in oDt.Columns)
                    {
                        string v_imprime = "";
                        v_imprime = oCols2.ColumnName.ToString();
                        if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                        {
                            //Double valor = 0.00;
                            int contador = 0;
                            int contAnt = 0;
                            Double modaAnt = 0.00;
                            Double moda = 0.00;

                            foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                            {

                                string v_compara = oBj.tmp_cabecera.ToString();
                                if (v_imprime.ToString() == v_compara)
                                {

                                    foreach (DataRow oFils in oDt.Rows)
                                    {
                                        var valor2 = oFils[oCols2].ToString();
                                        Double val = 0.00;
                                        if (valor2 != "")
                                        {
                                            moda = Convert.ToDouble(valor2);
                                            contador = 1;

                                            foreach (DataRow oFils2 in oDt.Rows)
                                            {

                                                var valor3 = oFils2[oCols2].ToString();
                                                if (valor3 != "")
                                                {
                                                    val = Convert.ToDouble(valor3);
                                                    if (moda == val)
                                                    {
                                                        contador++;
                                                        if (contador > contAnt)
                                                        {
                                                            modaAnt = moda;
                                                            contAnt = contador;
                                                        }
                                                    }

                                                }
                                            }

                                        }

                                    }
                                    break;
                                }
                            }
                            modaAnt = Math.Round(modaAnt, 2);
                            oWsConsolidado.Cells[6, cnt_mod].Value = modaAnt;
                        }
                        cnt_mod++;
                    }

                    foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                    {

                        string v_imprime = "";
                        v_imprime = oBj.tmp_cabecera.ToString();
                        if (v_imprime == "Id" ||
                            v_imprime == "Fecha" ||
                            v_imprime == "Ciudad" ||
                            v_imprime == "Direccion" || //YR 23022016
                            v_imprime == "Distrito" ||  //YR 23022016
                            //v_imprime == "Supervisor" ||
                            v_imprime == "Mercaderista" ||
                            v_imprime == "Mercado" ||
                            v_imprime == "P.D.V." ||
                            v_imprime == "Cliente")
                        {
                            v_imprime = v_imprime.ToUpper();
                            oWsConsolidado.Cells[7, cnt2].Value = v_imprime;
                            cnt2++;
                        }
                    }

                    oWsConsolidado.Cells[7, 10].Value = "";

                    foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                    {

                        if (oBj.tmp_codigo.ToString() == "03" ||
                            oBj.tmp_codigo.ToString() == "04" ||
                            oBj.tmp_codigo.ToString() == "05" ||
                            oBj.tmp_codigo.ToString() == "06")
                        {
                            string v_imprime = "";
                            v_imprime = oBj.tmp_cabecera.ToString();
                            if (v_imprime.Length > 4)
                            {
                                string v_imprime2 = oBj.tmp_descripcion.ToString().ToUpper();
                                oWsConsolidado.Cells[7, cnt3].Value = v_imprime2;
                                cnt3++;
                            }
                        }
                    }
                    oWsConsolidado.SelectedRange[7, 1, 7, cnt3].AutoFilter = true;

                    using (ExcelRange rng = oWsConsolidado.Cells[7, 1, 7, cnt3])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    int filas = 8;
                    foreach (DataRow oFils in oDt.Rows)
                    {
                        int cnt4 = 1;
                        foreach (DataColumn oCols in oDt.Columns)
                        {
                            string v_imprime = "";
                            v_imprime = oCols.ColumnName.ToString();
                            if (v_imprime == "id" ||
                                v_imprime == "fecha" ||
                                v_imprime == "ciudad" ||
                                v_imprime == "direccion" || //YR 23022016
                                v_imprime == "distrito" ||  //YR 23022016
                                //v_imprime == "supervisor" ||
                                v_imprime == "gie" ||
                                v_imprime == "cadena" ||
                                v_imprime == "pdv")
                            {
                                string detalle = oFils[oCols].ToString().ToUpper();
                                oWsConsolidado.Cells[filas, cnt4].Value = detalle;
                                cnt4++;
                            }

                            if (v_imprime == "tienda")
                            {
                                string detalle2 = oFils[oCols].ToString().ToUpper();
                                oWsConsolidado.Cells[filas, cnt4].Value = detalle2;
                                cnt4++;
                            }
                        }

                        int col9 = 11;
                        foreach (DataColumn oCols2 in oDt.Columns)
                        {
                            string v_imprime = "";
                            v_imprime = oCols2.ColumnName.ToString();
                            if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                            {
                                foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                                {
                                    string v_compara = oBj.tmp_cabecera.ToString();
                                    if (v_imprime.ToString() == v_compara)
                                    {
                                        var detalle3 = oFils[oCols2].ToString();
                                        oWsConsolidado.Cells[filas, col9].Value = detalle3;
                                        col9++;
                                        //break;
                                    }
                                    //else
                                    //{
                                    //    var detalle3 = "";
                                    //    oWsConsolidado.Cells[filas, col9].Value = detalle3;
                                    //}
                                    //col9++;
                                }
                            }
                        }
                        filas++;
                    }

                    oWsConsolidado.Cells.AutoFitColumns();
                    oWsConsolidado.Row(1).Style.Font.Bold = true;
                    oWsConsolidado.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsConsolidado.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    oWsConsolidado.Row(2).Style.Font.Bold = true;
                    oWsConsolidado.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsConsolidado.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    oWsConsolidado.Row(7).Style.Font.Bold = true;
                    oWsConsolidado.Row(7).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsConsolidado.Row(7).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                }
                #endregion  
                oEx.Save();
            }

            return Json(new { Archivo = _fileServer });
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2016-02-25
        /// </summary>
        /// <param name="planning"></param>
        /// <param name="periodo"></param>
        /// <param name="cadena"></param>
        /// <param name="pdv"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ExportaExcelConsolidadoMaps(string planning, string periodo, string cadena, string pdv)
        {
            string _fileServer = "";
            string _filePath = "";
            //int _fila = 0;

            _fileServer = String.Format("reporteConsolidado_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Consolidado >>
                List<ColgateConsolidado_DataBase> oConsolidado = new ColgateConsolidado_DataBase().Lista(
                    new Consulta_DataBase_Colgate_Maps_Request()
                    {
                        planning = planning,
                        periodo = Convert.ToInt32(periodo),
                        tipoperfil = "1", //filtro
                        cadena = cadena,
                        pdv= pdv
                    }
                );
                #endregion
                #endregion

                #region <<< Reporte Consolidado >>>


                BL_GES_Operativa oOperativa = new BL_GES_Operativa();
                DataTable oDt = oOperativa.BL_Colgate_Fusion_Consolidado_Detalle_Maps(planning, Convert.ToInt32(periodo), "1", cadena, pdv);

                string nom_hoja = "GIE";
                //if ("1" == "2")
                //{
                //    nom_hoja = "SUPERVISOR";
                //}
                //else
                //{
                //    nom_hoja = "GIE";
                //}

                int cta_p_col = 0, cta_p_com = 0, cta_v_col = 0, cta_v_com = 0;
                //string v_p_col = "pres. colgate";
                //string v_p_com = "pres. competencia";
                //string v_v_col = "visibilidad colgate";
                //string v_v_com = "visibilidad competencia";
                int cnt = 0, cnt2 = 1, cnt3 = 11;//9;
                if ((oConsolidado != null) || (oConsolidado.Count > 0))
                {
                    Excel.ExcelWorksheet oWsConsolidado = oEx.Workbook.Worksheets.Add("ReporteConsolidado_" + nom_hoja);

                    foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                    {
                        // oWsConsolidado.Cells[_fila, 1].Value = oBj.tmp_cabecera;
                        if (oBj.tmp_codigo.ToString() == "04")
                        {
                            cta_p_col++;
                        }
                        if (oBj.tmp_codigo.ToString() == "05")
                        {
                            cta_p_com++;
                        }
                        if (oBj.tmp_codigo.ToString() == "03")
                        {
                            cta_v_col++;
                        }
                        if (oBj.tmp_codigo.ToString() == "06")
                        {
                            cta_v_com++;
                        }
                    }

                    foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                    {
                        cnt++;
                        if (oBj.tmp_codigo.ToString() == "03" ||
                            oBj.tmp_codigo.ToString() == "04" ||
                            oBj.tmp_codigo.ToString() == "05" ||
                            oBj.tmp_codigo.ToString() == "06")
                        {
                            string v_imprime = "";
                            v_imprime = oBj.tmp_cabecera.ToString();
                            if (v_imprime.Length > 4)
                            {
                                string v_imprime2 = oBj.tmp_descripcion.ToString().ToUpper();
                                oWsConsolidado.Cells[2, cnt].Value = v_imprime2;
                            }
                        }
                    }

                    using (ExcelRange rng = oWsConsolidado.Cells[1, 11, 1, cnt])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    using (ExcelRange rng = oWsConsolidado.Cells[2, 11, 2, cnt])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    oWsConsolidado.Cells[3, 10].Value = "CANTIDAD";
                    using (ExcelRange rng = oWsConsolidado.Cells[3, 10, 3, 10])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    int cnt_cant = 1;
                    foreach (DataColumn oCols2 in oDt.Columns)
                    {

                        string v_imprime = "";
                        v_imprime = oCols2.ColumnName.ToString();
                        if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                        {
                            int total = 0;
                            foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                            {
                                string v_compara = oBj.tmp_cabecera.ToString();
                                if (v_imprime.ToString() == v_compara)
                                {
                                    foreach (DataRow oFils in oDt.Rows)
                                    {
                                        var valor2 = oFils[oCols2].ToString();
                                        if (valor2 != "")
                                        {
                                            total++;
                                        }
                                    }
                                    break;
                                }
                            }
                            oWsConsolidado.Cells[3, cnt_cant].Value = total;
                        }
                        cnt_cant++;
                    }


                    oWsConsolidado.Cells[4, 10].Value = "MAXIMO";
                    using (ExcelRange rng = oWsConsolidado.Cells[4, 10, 4, 10])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    int cnt_max = 1;
                    foreach (DataColumn oCols2 in oDt.Columns)
                    {
                        string v_imprime = "";
                        v_imprime = oCols2.ColumnName.ToString();
                        if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                        {
                            Double max = 0.00;
                            foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                            {

                                string v_compara = oBj.tmp_cabecera.ToString();
                                if (v_imprime.ToString() == v_compara)
                                {


                                    foreach (DataRow oFils in oDt.Rows)
                                    {
                                        var valor2 = oFils[oCols2].ToString();
                                        if (valor2 != "")
                                        {
                                            if (Convert.ToDouble(valor2) > max)
                                            {
                                                max = Convert.ToDouble(valor2);
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                            max = Math.Round(max, 2);
                            oWsConsolidado.Cells[4, cnt_max].Value = max;
                        }
                        cnt_max++;
                    }

                    oWsConsolidado.Cells[5, 10].Value = "MINIMO";
                    using (ExcelRange rng = oWsConsolidado.Cells[5, 10, 5, 10])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    int cnt_min = 1;
                    foreach (DataColumn oCols2 in oDt.Columns)
                    {
                        string v_imprime = "";
                        v_imprime = oCols2.ColumnName.ToString();
                        if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                        {
                            Double min = 999.99;
                            foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                            {


                                string v_compara = oBj.tmp_cabecera.ToString();
                                if (v_imprime.ToString() == v_compara)
                                {
                                    foreach (DataRow oFils in oDt.Rows)
                                    {
                                        var valor2 = oFils[oCols2].ToString();
                                        Double val = 0.00;
                                        if (valor2 != "")
                                        {
                                            val = Convert.ToDouble(valor2);
                                            if (val < min)
                                            {
                                                min = val;

                                            }
                                        }

                                    }
                                    break;
                                }
                            }

                            if (min == 999.99)
                            {
                                min = 0;
                            }

                            min = Math.Round(min, 2);
                            oWsConsolidado.Cells[5, cnt_min].Value = min;
                        }
                        cnt_min++;
                    }


                    oWsConsolidado.Cells[6, 10].Value = "MODA";
                    using (ExcelRange rng = oWsConsolidado.Cells[6, 10, 6, 10])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                    int cnt_mod = 1;
                    foreach (DataColumn oCols2 in oDt.Columns)
                    {
                        string v_imprime = "";
                        v_imprime = oCols2.ColumnName.ToString();
                        if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                        {
                            //Double valor = 0.00;
                            int contador = 0;
                            int contAnt = 0;
                            Double modaAnt = 0.00;
                            Double moda = 0.00;

                            foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                            {

                                string v_compara = oBj.tmp_cabecera.ToString();
                                if (v_imprime.ToString() == v_compara)
                                {

                                    foreach (DataRow oFils in oDt.Rows)
                                    {
                                        var valor2 = oFils[oCols2].ToString();
                                        Double val = 0.00;
                                        if (valor2 != "")
                                        {
                                            moda = Convert.ToDouble(valor2);
                                            contador = 1;

                                            foreach (DataRow oFils2 in oDt.Rows)
                                            {

                                                var valor3 = oFils2[oCols2].ToString();
                                                if (valor3 != "")
                                                {
                                                    val = Convert.ToDouble(valor3);
                                                    if (moda == val)
                                                    {
                                                        contador++;
                                                        if (contador > contAnt)
                                                        {
                                                            modaAnt = moda;
                                                            contAnt = contador;
                                                        }
                                                    }

                                                }
                                            }

                                        }

                                    }
                                    break;
                                }
                            }
                            modaAnt = Math.Round(modaAnt, 2);
                            oWsConsolidado.Cells[6, cnt_mod].Value = modaAnt;
                        }
                        cnt_mod++;
                    }

                    foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                    {

                        string v_imprime = "";
                        v_imprime = oBj.tmp_cabecera.ToString();
                        if (v_imprime == "Id" ||
                            v_imprime == "Fecha" ||
                            v_imprime == "Ciudad" ||
                            v_imprime == "Direccion" || //YR 23022016
                            v_imprime == "Distrito" ||  //YR 23022016
                            //v_imprime == "Supervisor" ||
                            v_imprime == "Mercaderista" ||
                            v_imprime == "Mercado" ||
                            v_imprime == "P.D.V." ||
                            v_imprime == "Cliente")
                        {
                            v_imprime = v_imprime.ToUpper();
                            oWsConsolidado.Cells[7, cnt2].Value = v_imprime;
                            cnt2++;
                        }
                    }

                    oWsConsolidado.Cells[7, 10].Value = "";

                    foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                    {

                        if (oBj.tmp_codigo.ToString() == "03" ||
                            oBj.tmp_codigo.ToString() == "04" ||
                            oBj.tmp_codigo.ToString() == "05" ||
                            oBj.tmp_codigo.ToString() == "06")
                        {
                            string v_imprime = "";
                            v_imprime = oBj.tmp_cabecera.ToString();
                            if (v_imprime.Length > 4)
                            {
                                string v_imprime2 = oBj.tmp_descripcion.ToString().ToUpper();
                                oWsConsolidado.Cells[7, cnt3].Value = v_imprime2;
                                cnt3++;
                            }
                        }
                    }
                    oWsConsolidado.SelectedRange[7, 1, 7, cnt3].AutoFilter = true;

                    using (ExcelRange rng = oWsConsolidado.Cells[7, 1, 7, cnt3])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    int filas = 8;
                    foreach (DataRow oFils in oDt.Rows)
                    {
                        int cnt4 = 1;
                        foreach (DataColumn oCols in oDt.Columns)
                        {
                            string v_imprime = "";
                            v_imprime = oCols.ColumnName.ToString();
                            if (v_imprime == "id" ||
                                v_imprime == "fecha" ||
                                v_imprime == "ciudad" ||
                                v_imprime == "direccion" || //YR 23022016
                                v_imprime == "distrito" ||  //YR 23022016
                                //v_imprime == "supervisor" ||
                                v_imprime == "gie" ||
                                v_imprime == "cadena" ||
                                v_imprime == "pdv")
                            {
                                string detalle = oFils[oCols].ToString().ToUpper();
                                oWsConsolidado.Cells[filas, cnt4].Value = detalle;
                                cnt4++;
                            }

                            if (v_imprime == "tienda")
                            {
                                string detalle2 = oFils[oCols].ToString().ToUpper();
                                oWsConsolidado.Cells[filas, cnt4].Value = detalle2;
                                cnt4++;
                            }
                        }

                        int col9 = 11;
                        foreach (DataColumn oCols2 in oDt.Columns)
                        {
                            string v_imprime = "";
                            v_imprime = oCols2.ColumnName.ToString();
                            if (v_imprime.Length > 4 && v_imprime.Substring(0, 4) == "val_")
                            {
                                foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
                                {
                                    string v_compara = oBj.tmp_cabecera.ToString();
                                    if (v_imprime.ToString() == v_compara)
                                    {
                                        var detalle3 = oFils[oCols2].ToString();
                                        oWsConsolidado.Cells[filas, col9].Value = detalle3;
                                        col9++;
                                        //break;
                                    }
                                    //else
                                    //{
                                    //    var detalle3 = "";
                                    //    oWsConsolidado.Cells[filas, col9].Value = detalle3;
                                    //}
                                    //col9++;
                                }
                            }
                        }
                        filas++;
                    }

                    oWsConsolidado.Cells.AutoFitColumns();
                    oWsConsolidado.Row(1).Style.Font.Bold = true;
                    oWsConsolidado.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsConsolidado.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    oWsConsolidado.Row(2).Style.Font.Bold = true;
                    oWsConsolidado.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsConsolidado.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    oWsConsolidado.Row(7).Style.Font.Bold = true;
                    oWsConsolidado.Row(7).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsConsolidado.Row(7).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                }
                #endregion
                oEx.Save();
            }

            return Json(new { Archivo = _fileServer });
        }


        #endregion

        #region << Maps Colgate Fusion >>

        public ActionResult XploraMapsCol()
        {
            if (Request["_a"] != null)
            {
                ViewBag.campania = Convert.ToString(Request["_a"]);
            }
            return View();
        }

        #endregion

        #region  <<< Dashboard >>>

        public ActionResult Dashboard()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
         
            return View();
        }
        public ActionResult Dsh_Presencia(int _anio, int _mes, int _categoria, int _opcion,string _equipo)
        {
            ViewBag.vOpcion = _opcion;
            return View(new E_Dsh_Colgate().Presencia(
                    new Request_Dsh_Colg()
                    {
                        Cod_Empresa = 0
                        ,Cod_Anio = _anio
                        ,Cod_Mes = _mes
                        ,Cod_Categoria = _categoria
                        ,Cod_Opcion = _opcion
                        ,Cod_Equipo = _equipo
                    }
                ));
        }
        public ActionResult Dsh_Shareofshelf(int _anio,int _mes,int _periodo,int _opcion,int _categoria)
        {
            ViewBag.vg_opcion = _opcion;
            return View(new E_Dsh_Colgate().ShareOfShelf(
                    new Request_Dsh_Colg()
                    {
                        Cod_Anio = _anio,
                        Cod_Mes = _mes,
                        Cod_Periodo = _periodo,
                        Cod_Opcion = _opcion,
                        Cod_Categoria = _categoria
                    }
                ));
        }
        public ActionResult Dsh_Coverage(int _anio,int _mes)
        {
            return View(new E_Dsh_Colgate().Coverage(
                    new Request_Dsh_Colg()
                    {
                        Cod_Anio = _anio,
                        Cod_Mes = _mes
                    }
                ));
        }
        public ActionResult Dsh_SosSoe()
        {
            return View();
        }
        public ActionResult Dsh_Precio(int _anio,int _mes,int _categoria,string _sku)
        {
            return View(new E_Dsh_Colgate().Precio(
                    new Request_Dsh_Colg()
                    {
                        Cod_Anio = _anio,
                        Cod_Mes = _mes,
                        Cod_Categoria = _categoria,
                        Cod_Sku = _sku
                    }
                ));
        }
        public ActionResult Dsh_Visibilidad(string _equipo,int _anio,int _mes,string _elemento,int _opcion)
        {
            ViewBag.vgOpcion = _opcion;
            return View(new E_Dsh_Colgate().Visibilidad(
                    new Request_Dsh_Colg()
                    {
                        Cod_Equipo = _equipo,
                        Cod_Anio = _anio,
                        Cod_Mes = _mes,
                        Cod_Elemento = _elemento,
                        Cod_Opcion = _opcion
                    }
                ));
        }
        public ActionResult Dsh_ShareOfExhibicion(string _equipo,int _anio ,int _mes, int _categoria) {
            return View(new E_Dsh_Colgate().ShareOfExhibicion(
                    new Request_Dsh_Colg()
                    {
                        Cod_Equipo = _equipo,
                        Cod_Anio = _anio,
                        Cod_Mes = _mes,
                        Cod_Categoria = _categoria                      
                    }
                ));
        }
        public ActionResult Dsh_ShareOfPromocion(string _equipo, int _anio, int _mes)
        {
            return View(new E_Dsh_Colgate().ShareOfPromocion(
                    new Request_Dsh_Colg()
                    {
                        Cod_Equipo = _equipo,
                        Cod_Anio = _anio,
                        Cod_Mes = _mes
                    }
                ));
        }
        public ActionResult Dsh_Filtros(string _id, string _parametros, int _opcion)
        {
            ViewBag.vgId = _id;
            ViewBag.vgOpcion = _opcion;
            return View(new E_Dsh_Colgate().Filtros(new Request_Dsh_Colg() { Parametros = _parametros, Cod_Opcion = _opcion }));
        }

        #endregion

        #region << RQ Lucky Col-002 v1 0 >>

        #region << Presence >>

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-10-13
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportingConsolidadoPresencia()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
                        
            return View();                 
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-10-14
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReportingConsolidadoPresencia(int __a, int __b, int __c, int __d, int __e)
        {
            ViewBag.campania = Request["_a"];
            return View(new Presence_Product().Objeto_Presence(
                    new Request_ColgateReporting_Parametros()
                    {
                        campania = Request["_a"].ToString(),
                        categoria = Convert.ToInt32(__a),
                        periodo = Convert.ToInt32(__b),
                        oficina = Convert.ToInt32(__c),
                        bioficina = Convert.ToInt32(__d),
                        cadena = Convert.ToInt32(__e),
                        opcion = 0
                    }
                ));
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-10-13
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportingConsolidadoOOS()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-10-14
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReportingConsolidadoOOS(int __a, int __b, int __c, int __d, int __e)
        {

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            ViewBag.campania = Request["_a"];
            return View(new Presence_Product().Objeto_Presence(
                    new Request_ColgateReporting_Parametros()
                    {
                        campania = Request["_a"].ToString(),
                        categoria = Convert.ToInt32(__a),
                        periodo = Convert.ToInt32(__b),
                        oficina = Convert.ToInt32(__c),
                        bioficina = Convert.ToInt32(__d),
                        cadena = Convert.ToInt32(__e),
                        opcion = 0
                    }
                ));
        }

        #endregion

        #region << Placement >>

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-12-02
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportingConsolidadoPlacement()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
                        
            return View();
        }


        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-12-02
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReportingConsolidadoPlacement(int __a, int __b, int __c, int __d, int __e)
        {
            ViewBag.campania = Request["_a"].ToString();
            ViewBag.categoria = __a.ToString();
            ViewBag.periodo = __b.ToString();
            ViewBag.oficina = __c.ToString();
            ViewBag.bioficina = __d.ToString();
            ViewBag.cadena = __e.ToString();

            return View(new Placement_SOS().Objeto_Placement(
                    new Request_ColgateReporting_Parametros()
                    {
                        campania = Request["_a"].ToString(),
                        categoria = Convert.ToInt32(__a),
                        periodo = Convert.ToInt32(__b),
                        oficina = Convert.ToInt32(__c),
                        bioficina = Convert.ToInt32(__d),
                        cadena = Convert.ToInt32(__e),
                        opcion = 1
                    }
                ));
        }

        #endregion

        #region << Promotion >>

        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 2015-09-12
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportingConsolidadoPromotion()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
                        
            return View();
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-12
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReportingConsolidadoPromotion(int __a, int __b, int __c, int __d, int __e)
        {
            ViewBag.campania = Request["_a"].ToString();
            ViewBag.categoria = __a.ToString();
            ViewBag.periodo = __b.ToString();
            ViewBag.oficina = __c.ToString();
            ViewBag.bioficina = __d.ToString();
            ViewBag.cadena = __e.ToString();

            return View(new Promotion_Impulso().Objeto_Promotion(
                    new Request_ColgateReporting_Parametros()
                    {
                        campania = Request["_a"].ToString(),
                        categoria = Convert.ToInt32(__a),
                        periodo = Convert.ToInt32(__b),
                        oficina = Convert.ToInt32(__c),
                        bioficina = Convert.ToInt32(__d),
                        cadena = Convert.ToInt32(__e),
                        opcion = 2
                    }
                ));
        }

        #endregion

        #region << Price >>

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-12-14
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportingConsolidadoPrice()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }


            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-12-14
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReportingConsolidadoPrice(int __a, int __b, int __c, int __d, int __e)
        {
            ViewBag.campania = Request["_a"].ToString();
            ViewBag.categoria = __a.ToString();
            ViewBag.periodo = __b.ToString();
            ViewBag.oficina = __c.ToString();
            ViewBag.bioficina = __d.ToString();
            ViewBag.cadena = __e.ToString();

            return View(new Price_Precio_Grilla().Objeto_Price(
                    new Request_ColgateReporting_Parametros()
                    {
                        campania = Request["_a"].ToString(),
                        categoria = Convert.ToInt32(__a),
                        periodo = Convert.ToInt32(__b),
                        oficina = Convert.ToInt32(__c),
                        bioficina = Convert.ToInt32(__d),
                        cadena = Convert.ToInt32(__e),
                        opcion = 3
                    }
                ));
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2016-01-11
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReportingEvolutivePrice(string campania, int __a, int __b, int __c, int __d, int __e, string __f)
        {
            List<Price_Precio_GrafEvo> oLGEP = new Price_Precio_GrafEvo().Objeto_EvolutivePrice(
                    new Request_ColgateEvolutivePrice()
                    {
                        campania = campania, //"4111304112013", //Request["_a"].ToString(),
                        categoria = Convert.ToInt32(__a),
                        periodo = Convert.ToInt32(__b),
                        oficina = Convert.ToInt32(__c),
                        bioficina = Convert.ToInt32(__d),
                        cadena = Convert.ToInt32(__e),
                        sku = Convert.ToString(__f)
                    });

            return Json(new { Archivo = oLGEP });
        }

        [HttpPost]
        public ActionResult _Get_Graf_SellOut_Strategic(int anio, int mes, int cod_oficina, string cod_cadena, string cod_categoria, int cod_marca)
        {
            List<E_Grafico_Sellout> oLs = new E_Grafico_Sellout().Lista(
                    new Request_Graf_SellOut()
                    {
                        anio = anio,
                        mes = mes,
                        cod_oficina = cod_oficina,
                        cod_cadena = cod_cadena,
                        cod_categoria = cod_categoria,
                        cod_marca = cod_marca
                    });

            return Json(new { Archivo = oLs });
            //return new ContentResult
            //{
            //    Content = MvcApplication._Serialize(oLs),
            //    ContentType = "application/json"
            //};
        }

        #endregion

        #region << POP >>

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-12-16
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportingConsolidadoPop()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
                        
            return View();
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-12-16
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReportingConsolidadoPop(int __a, int __b, int __c, int __d, int __e)
        {
            ViewBag.campania = Request["_a"].ToString();
            ViewBag.categoria = __a.ToString();
            ViewBag.periodo = __b.ToString(); //32595;
            ViewBag.oficina = __c.ToString();
            ViewBag.bioficina = __d.ToString();
            ViewBag.cadena = __e.ToString(); //25;
            return View();
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2016-03-09
        /// </summary>
        /// <param name="campania"></param>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReportingEvolutivePop(string campania, int __a, int __b, string __c)
        {
            POP_Element_GrafEvo oLGEP = new POP_Element_GrafEvo().Objeto_EvolutivePop(
                    new Request_ColgateEvolutivePOP()
                    {
                        campania = campania, //"4111304112013", //Request["_a"].ToString(),
                        categoria = Convert.ToInt32(__a),
                        periodo = Convert.ToInt32(__b),
                        elements = __c.ToString()
                    });

            return Json(new { Archivo = oLGEP });
        }

        #endregion

        #region << Exports >>

        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 17/02/2016
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ExportaExcelColgate(int __a, int __b, int __c, int __d, int __e, int __f)
        {
            string _fileServer = "";
            string _filePath = "";
            int _fila = 0;

            _fileServer = String.Format("Reporte5psColgate_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                if (__f == 0)
                {
                    #region <<< Instancias >>>

                    List<Presence_Company_Table> oConsolidado = new Presence_Company_Table().Objeto_ExportPresence(
                        new Request_ColgateReporting_Parametros()
                        {
                            campania = "4111304112013", //Request["_a"].ToString(),
                            categoria = Convert.ToInt32(__a),
                            periodo = Convert.ToInt32(__b),
                            oficina = Convert.ToInt32(__c),
                            bioficina = Convert.ToInt32(__d),
                            cadena = Convert.ToInt32(__e),
                            opcion = 0
                        }
                    );

                    #endregion
                    #region <<< Reporte Presence >>>

                    if (oConsolidado != null || oConsolidado.Count > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Base Presence");
                        oWsReporte.Cells[1, 1].Value = "Fecha Relevo";
                        oWsReporte.Cells[1, 2].Value = "Ciudad";
                        oWsReporte.Cells[1, 3].Value = "Cadena";
                        oWsReporte.Cells[1, 4].Value = "Tienda";
                        oWsReporte.Cells[1, 5].Value = "SubCategoria";
                        oWsReporte.Cells[1, 6].Value = "Empresa";
                        oWsReporte.Cells[1, 7].Value = "Producto";
                        oWsReporte.Cells[1, 8].Value = "Quiebre";

                        _fila = 2;
                        foreach (var _Datos in oConsolidado)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos.rpl_descripcion;
                            oWsReporte.Cells[_fila, 2].Value = _Datos.city;
                            oWsReporte.Cells[_fila, 3].Value = _Datos.nodecommercial;
                            oWsReporte.Cells[_fila, 4].Value = _Datos.pdv_Name;
                            oWsReporte.Cells[_fila, 5].Value = _Datos.subcategory;
                            oWsReporte.Cells[_fila, 6].Value = _Datos.company;
                            oWsReporte.Cells[_fila, 7].Value = _Datos.product;
                            oWsReporte.Cells[_fila, 8].Value = _Datos.description;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.SelectedRange[1, 1, 1, 8].AutoFilter = true;

                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);

                    }
                    #endregion
                }
                if (__f == 1)
                {
                    #region <<< Instancias >>>

                    List<Placement_SOS_Company_Table> oConsolidado = new Placement_SOS_Company_Table().Objeto_ExportPlacement(
                        new Request_ColgateReporting_Parametros()
                        {
                            campania = "4111304112013", //Request["_a"].ToString(),
                            categoria = Convert.ToInt32(__a),
                            periodo = Convert.ToInt32(__b),
                            oficina = Convert.ToInt32(__c),
                            bioficina = Convert.ToInt32(__d),
                            cadena = Convert.ToInt32(__e),
                            opcion = 1
                        }
                    );

                    #endregion
                    #region <<< Reporte Placement >>>

                    if (oConsolidado != null || oConsolidado.Count > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Base SOS");
                        oWsReporte.Cells[1, 1].Value = "Periodo";
                        oWsReporte.Cells[1, 2].Value = "Cadena";
                        oWsReporte.Cells[1, 3].Value = "Lugar";
                        oWsReporte.Cells[1, 4].Value = "Ciudad";
                        oWsReporte.Cells[1, 5].Value = "Descripcion CP";
                        oWsReporte.Cells[1, 6].Value = "Sub Categoria";
                        oWsReporte.Cells[1, 7].Value = "Categoria";
                        oWsReporte.Cells[1, 8].Value = "Empresa";
                        oWsReporte.Cells[1, 9].Value = "Cantidad";

                        _fila = 2;
                        foreach (var _Datos in oConsolidado)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos.rpl_id;
                            oWsReporte.Cells[_fila, 1].Style.Locked = false;
                            oWsReporte.Cells[_fila, 2].Value = _Datos.ncm_descripcion;
                            oWsReporte.Cells[_fila, 2].Style.Locked = false;
                            oWsReporte.Cells[_fila, 3].Value = _Datos.depart_descripcion;
                            oWsReporte.Cells[_fila, 3].Style.Locked = false;
                            oWsReporte.Cells[_fila, 4].Value = _Datos.coty_descripcion;
                            oWsReporte.Cells[_fila, 4].Style.Locked = false;
                            oWsReporte.Cells[_fila, 5].Value = _Datos.pdv_name;
                            oWsReporte.Cells[_fila, 5].Style.Locked = false;
                            oWsReporte.Cells[_fila, 6].Value = _Datos.subcategoria;
                            oWsReporte.Cells[_fila, 6].Style.Locked = false;
                            oWsReporte.Cells[_fila, 7].Value = _Datos.categoria;
                            oWsReporte.Cells[_fila, 7].Style.Locked = false;
                            oWsReporte.Cells[_fila, 8].Value = _Datos.company_descripcion;
                            oWsReporte.Cells[_fila, 8].Style.Locked = false;
                            oWsReporte.Cells[_fila, 9].Value = _Datos.metros;
                            oWsReporte.Cells[_fila, 9].Style.Locked = false;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.SelectedRange[1, 1, 1, 9].AutoFilter = true;

                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);
                        /*//==//==//==//==//==//==//==//==//==//==//==//==//==//*/

                        //oWsReporte.Protection.AllowInsertRows = false;
                        //oWsReporte.Protection.AllowInsertColumns = false;
                        //oWsReporte.Protection.AllowDeleteColumns = false;
                        //oWsReporte.Protection.AllowPivotTables = false;
                        //oWsReporte.Protection.AllowSelectLockedCells = true;
                        //oWsReporte.Protection.AllowSelectUnlockedCells = true;
                        //oWsReporte.Protection.AllowAutoFilter = true;
                        //oWsReporte.Protection.AllowSort = false;
                        //oWsReporte.Protection.IsProtected = true;
                    }
                    #endregion
                }
                if (__f == 2)
                {
                    #region <<< Instancias >>>

                    List<PushGirl_Company_Table> oConsolidado = new PushGirl_Company_Table().Objeto_ExportPromotion(
                        new Request_ColgateReporting_Parametros()
                        {
                            campania = "4111304112013", //Request["_a"].ToString(),
                            categoria = Convert.ToInt32(__a),
                            periodo = Convert.ToInt32(__b),
                            oficina = Convert.ToInt32(__c),
                            bioficina = Convert.ToInt32(__d),
                            cadena = Convert.ToInt32(__e),
                            opcion = 2
                        }
                    );

                    #endregion
                    #region <<< Reporte Promotion >>>

                    if (oConsolidado != null || oConsolidado.Count > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Base PushGirl");
                        oWsReporte.Cells[1, 1].Value = "Periodo";
                        oWsReporte.Cells[1, 2].Value = "Ciudad";
                        oWsReporte.Cells[1, 3].Value = "Cadena";
                        oWsReporte.Cells[1, 4].Value = "Tienda";
                        oWsReporte.Cells[1, 5].Value = "Compañia";
                        oWsReporte.Cells[1, 6].Value = "Categoria";
                        oWsReporte.Cells[1, 7].Value = "Sub Categoria";
                        oWsReporte.Cells[1, 8].Value = "Real";
                        oWsReporte.Cells[1, 9].Value = "SOB";

                        _fila = 2;
                        foreach (var _Datos in oConsolidado)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos.rpl_descripcion;
                            oWsReporte.Cells[_fila, 1].Style.Locked = false;
                            oWsReporte.Cells[_fila, 2].Value = _Datos.city;
                            oWsReporte.Cells[_fila, 2].Style.Locked = false;
                            oWsReporte.Cells[_fila, 3].Value = _Datos.nodecommercial;
                            oWsReporte.Cells[_fila, 3].Style.Locked = false;
                            oWsReporte.Cells[_fila, 4].Value = _Datos.pdv_name;
                            oWsReporte.Cells[_fila, 4].Style.Locked = false;
                            oWsReporte.Cells[_fila, 5].Value = _Datos.company;
                            oWsReporte.Cells[_fila, 5].Style.Locked = false;
                            oWsReporte.Cells[_fila, 6].Value = _Datos.category;
                            oWsReporte.Cells[_fila, 6].Style.Locked = false;
                            oWsReporte.Cells[_fila, 7].Value = _Datos.subcategory;
                            oWsReporte.Cells[_fila, 7].Style.Locked = false;
                            oWsReporte.Cells[_fila, 8].Value = _Datos.real;
                            oWsReporte.Cells[_fila, 8].Style.Locked = false;
                            oWsReporte.Cells[_fila, 9].Value = _Datos.sob;
                            oWsReporte.Cells[_fila, 9].Style.Locked = false;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.SelectedRange[1, 1, 1, 9].AutoFilter = true;

                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);

                        /*//==//==//==//==//==//==//==//==//==//==//==//==//==//*/

                        //oWsReporte.Protection.AllowInsertRows = false;
                        //oWsReporte.Protection.AllowInsertColumns = false;
                        //oWsReporte.Protection.AllowDeleteColumns = false;
                        //oWsReporte.Protection.AllowPivotTables = false;
                        //oWsReporte.Protection.AllowSelectLockedCells = true;
                        //oWsReporte.Protection.AllowSelectUnlockedCells = true;
                        //oWsReporte.Protection.AllowAutoFilter = true;
                        //oWsReporte.Protection.AllowSort = false;
                        //oWsReporte.Protection.IsProtected = true;
                    }
                    #endregion
                }
                if (__f == 3)
                {
                    #region <<< Instancias >>>

                    List<POP_Company_Table> oConsolidado = new POP_Company_Table().Objeto_ExportPOP(
                        new Request_ColgateReporting_Parametros()
                        {
                            campania = "4111304112013", //Request["_a"].ToString(),
                            categoria = Convert.ToInt32(__a),
                            periodo = Convert.ToInt32(__b),
                            oficina = Convert.ToInt32(__c),
                            bioficina = Convert.ToInt32(__d),
                            cadena = Convert.ToInt32(__e),
                            opcion = 3
                        }
                    );

                    #endregion
                    #region <<< Reporte POP >>>

                    if (oConsolidado != null || oConsolidado.Count > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Base POP");
                        oWsReporte.Cells[1, 1].Value = "Periodo";
                        oWsReporte.Cells[1, 2].Value = "Lugar";
                        oWsReporte.Cells[1, 3].Value = "Ciudad";
                        oWsReporte.Cells[1, 4].Value = "Cadena";
                        oWsReporte.Cells[1, 5].Value = "Tienda";
                        oWsReporte.Cells[1, 6].Value = "Categoria";
                        oWsReporte.Cells[1, 7].Value = "Sub Categoria";
                        oWsReporte.Cells[1, 8].Value = "SOB";
                        oWsReporte.Cells[1, 9].Value = "Empresa";
                        oWsReporte.Cells[1, 10].Value = "Tipo Elemento";
                        oWsReporte.Cells[1, 11].Value = "Elemento";
                        oWsReporte.Cells[1, 12].Value = "Cantidad";

                        _fila = 2;
                        foreach (var _Datos in oConsolidado)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos.rpl_descripcion;
                            oWsReporte.Cells[_fila, 1].Style.Locked = false;
                            oWsReporte.Cells[_fila, 2].Value = _Datos.department;
                            oWsReporte.Cells[_fila, 2].Style.Locked = false;
                            oWsReporte.Cells[_fila, 3].Value = _Datos.city;
                            oWsReporte.Cells[_fila, 3].Style.Locked = false;
                            oWsReporte.Cells[_fila, 4].Value = _Datos.nodecommercial;
                            oWsReporte.Cells[_fila, 4].Style.Locked = false;
                            oWsReporte.Cells[_fila, 5].Value = _Datos.pdv_Name;
                            oWsReporte.Cells[_fila, 5].Style.Locked = false;
                            oWsReporte.Cells[_fila, 6].Value = _Datos.category;
                            oWsReporte.Cells[_fila, 6].Style.Locked = false;
                            oWsReporte.Cells[_fila, 7].Value = _Datos.subcategory;
                            oWsReporte.Cells[_fila, 7].Style.Locked = false;
                            oWsReporte.Cells[_fila, 8].Value = _Datos.sob;
                            oWsReporte.Cells[_fila, 8].Style.Locked = false;
                            oWsReporte.Cells[_fila, 9].Value = _Datos.company;
                            oWsReporte.Cells[_fila, 9].Style.Locked = false;
                            oWsReporte.Cells[_fila, 10].Value = _Datos.typeelement;
                            oWsReporte.Cells[_fila, 10].Style.Locked = false;
                            oWsReporte.Cells[_fila, 11].Value = _Datos.element;
                            oWsReporte.Cells[_fila, 11].Style.Locked = false;
                            oWsReporte.Cells[_fila, 12].Value = _Datos.cantidad;
                            oWsReporte.Cells[_fila, 12].Style.Locked = false;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.SelectedRange[1, 1, 1, 12].AutoFilter = true;

                        //Formato Cabecera
                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();
                        oWsReporte.Column(10).AutoFit();
                        oWsReporte.Column(11).AutoFit();
                        oWsReporte.Column(12).AutoFit();

                        oWsReporte.View.FreezePanes(2, 1);

                        /*//==//==//==//==//==//==//==//==//==//==//==//==//==//*/

                        //oWsReporte.Protection.AllowInsertRows = false;
                        //oWsReporte.Protection.AllowInsertColumns = false;
                        //oWsReporte.Protection.AllowDeleteColumns = false;
                        //oWsReporte.Protection.AllowPivotTables = false;
                        //oWsReporte.Protection.AllowSelectLockedCells = true;
                        //oWsReporte.Protection.AllowSelectUnlockedCells = true;
                        //oWsReporte.Protection.AllowAutoFilter = true;
                        //oWsReporte.Protection.AllowSort = false;
                        //oWsReporte.Protection.IsProtected = true;
                    }
                    #endregion
                }
                if (__f == 4)
                {
                    #region <<< Instancias >>>

                    List<Price_Company_Table> oConsolidado = new Price_Company_Table().Objeto_ExportPrice(
                        new Request_ColgateReporting_Parametros()
                        {
                            campania = "4111304112013", //Request["_a"].ToString(),
                            categoria = Convert.ToInt32(__a),
                            periodo = Convert.ToInt32(__b),
                            oficina = Convert.ToInt32(__c),
                            bioficina = Convert.ToInt32(__d),
                            cadena = Convert.ToInt32(__e),
                            opcion = 4
                        }
                    );

                    #endregion
                    #region <<< Reporte Price >>>

                    if (oConsolidado != null || oConsolidado.Count > 0)
                    {
                        Excel.ExcelWorksheet oWsReporte = oEx.Workbook.Worksheets.Add("Base Price");
                        oWsReporte.Cells[1, 1].Value = "Periodo";
                        oWsReporte.Cells[1, 2].Value = "Ciudad";
                        oWsReporte.Cells[1, 3].Value = "Cadena";
                        oWsReporte.Cells[1, 4].Value = "Tienda";
                        oWsReporte.Cells[1, 5].Value = "Categoria";
                        oWsReporte.Cells[1, 6].Value = "Sub Categoria";
                        oWsReporte.Cells[1, 7].Value = "Compañia";
                        oWsReporte.Cells[1, 8].Value = "Marca";
                        oWsReporte.Cells[1, 9].Value = "Producto : ID";
                        oWsReporte.Cells[1, 10].Value = "Producto";
                        oWsReporte.Cells[1, 11].Value = "Precio Regular";
                        oWsReporte.Cells[1, 12].Value = "Precio Oferta";
                        oWsReporte.Cells[1, 13].Value = "Tipo Oferta : ID";
                        oWsReporte.Cells[1, 14].Value = "Tipo Oferta";
                        oWsReporte.Cells[1, 15].Value = "Fecha Inicio";
                        oWsReporte.Cells[1, 16].Value = "Fecha Fin";
                        oWsReporte.Cells[1, 17].Value = "Mecanica";
                        oWsReporte.Cells[1, 18].Value = "Observacion";

                        _fila = 2;
                        foreach (var _Datos in oConsolidado)
                        {
                            oWsReporte.Cells[_fila, 1].Value = _Datos.rpl_descripcion;
                            oWsReporte.Cells[_fila, 1].Style.Locked = false;
                            oWsReporte.Cells[_fila, 2].Value = _Datos.city;
                            oWsReporte.Cells[_fila, 2].Style.Locked = false;
                            oWsReporte.Cells[_fila, 3].Value = _Datos.nodecommercial;
                            oWsReporte.Cells[_fila, 3].Style.Locked = false;
                            oWsReporte.Cells[_fila, 4].Value = _Datos.pdv_name;
                            oWsReporte.Cells[_fila, 4].Style.Locked = false;
                            oWsReporte.Cells[_fila, 5].Value = _Datos.category;
                            oWsReporte.Cells[_fila, 5].Style.Locked = false;
                            oWsReporte.Cells[_fila, 6].Value = _Datos.subcategory;
                            oWsReporte.Cells[_fila, 6].Style.Locked = false;
                            oWsReporte.Cells[_fila, 7].Value = _Datos.company;
                            oWsReporte.Cells[_fila, 7].Style.Locked = false;
                            oWsReporte.Cells[_fila, 8].Value = _Datos.brand;
                            oWsReporte.Cells[_fila, 8].Style.Locked = false;
                            oWsReporte.Cells[_fila, 9].Value = _Datos.product_cod;
                            oWsReporte.Cells[_fila, 9].Style.Locked = false;
                            oWsReporte.Cells[_fila, 10].Value = _Datos.product;
                            oWsReporte.Cells[_fila, 10].Style.Locked = false;
                            
                            oWsReporte.Cells[_fila, 11].Value = _Datos.precio_regular;
                            oWsReporte.Cells[_fila, 11].Style.Locked = false;
                            oWsReporte.Cells[_fila, 12].Value = _Datos.precio_oferta;
                            oWsReporte.Cells[_fila, 12].Style.Locked = false;

                            oWsReporte.Cells[_fila, 13].Value = _Datos.tipooferta_cod;
                            oWsReporte.Cells[_fila, 13].Style.Locked = false;

                            oWsReporte.Cells[_fila, 14].Value = _Datos.tipooferta;
                            oWsReporte.Cells[_fila, 14].Style.Locked = false;
                            oWsReporte.Cells[_fila, 15].Value = _Datos.fecha_inicio;
                            oWsReporte.Cells[_fila, 15].Style.Locked = false;
                            oWsReporte.Cells[_fila, 16].Value = _Datos.fecha_fin;
                            oWsReporte.Cells[_fila, 16].Style.Locked = false;
                            oWsReporte.Cells[_fila, 17].Value = _Datos.mecanica;
                            oWsReporte.Cells[_fila, 17].Style.Locked = false;
                            oWsReporte.Cells[_fila, 18].Value = _Datos.observacion;
                            oWsReporte.Cells[_fila, 18].Style.Locked = false;
                            _fila++;
                        }
                        //Formato Cabecera
                        oWsReporte.SelectedRange[1, 1, 1, 18].AutoFilter = true;

                        oWsReporte.Row(1).Height = 25;
                        oWsReporte.Row(1).Style.Font.Bold = true;
                        oWsReporte.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsReporte.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsReporte.Column(1).AutoFit();
                        oWsReporte.Column(2).AutoFit();
                        oWsReporte.Column(3).AutoFit();
                        oWsReporte.Column(4).AutoFit();
                        oWsReporte.Column(5).AutoFit();
                        oWsReporte.Column(6).AutoFit();
                        oWsReporte.Column(7).AutoFit();
                        oWsReporte.Column(8).AutoFit();
                        oWsReporte.Column(9).AutoFit();
                        oWsReporte.Column(10).AutoFit();
                        oWsReporte.Column(11).AutoFit();
                        oWsReporte.Column(12).AutoFit();
                        oWsReporte.Column(13).AutoFit();
                        oWsReporte.Column(14).AutoFit();
                        oWsReporte.Column(15).AutoFit();
                        oWsReporte.Column(16).AutoFit();
                        oWsReporte.Column(17).AutoFit();
                        oWsReporte.Column(18).AutoFit();


                        oWsReporte.View.FreezePanes(2, 1);
                        /*//==//==//==//==//==//==//==//==//==//==//==//==//==//*/

                        //oWsReporte.Protection.AllowInsertRows = false;
                        //oWsReporte.Protection.AllowInsertColumns = false;
                        //oWsReporte.Protection.AllowDeleteColumns = false;
                        //oWsReporte.Protection.AllowPivotTables = false;
                        //oWsReporte.Protection.AllowSelectLockedCells = true;
                        //oWsReporte.Protection.AllowSelectUnlockedCells = true;
                        //oWsReporte.Protection.AllowAutoFilter = true;
                        //oWsReporte.Protection.AllowSort = false;
                        //oWsReporte.Protection.IsProtected = true;
                        //oWsPrecio.Protection.SetPassword(_password);

                    }
                    #endregion
                }
                oEx.Save();
            }

            return Json(new { Archivo = _fileServer });
        }

        #endregion

        #region << Pharmacies >>

        public ActionResult ReportingConsPharmaciesPrice()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }


            if (Request.QueryString["_b"] != null)
            {
                new Insert_Visita_Persona_Response().Inserta_Visita_Modulo(
                    new Insert_Visita_Persona_Request()
                    {
                        person_id = ((Persona)Session["Session_Login"]).Person_id,
                        modulo_id = Request["_b"].ToString(),
                        company_id = ((Persona)Session["Session_Login"]).Company_id.ToString(),
                        fecha_ingreso = DateTime.Today.ToString("dd/MM/yyyy"),
                        visitas = 1,
                        createBy = ((Persona)Session["Session_Login"]).name_user
                    }
                );
            }
            

            return View();
        }

        [HttpPost]
        public ActionResult ReportingConsPharmaciesPrice(int __a, int __b, int __c, int __d, int __e)
        {
            ViewBag.campania = Request["_a"].ToString();
            ViewBag.categoria = __a.ToString();
            ViewBag.periodo = __b.ToString();
            ViewBag.oficina = __c.ToString();
            ViewBag.bioficina = __d.ToString();
            ViewBag.cadena = __e.ToString();

            return View(new Price_Precio_Pharmacies_Grilla().Objeto_PharmaciesPrice(
                    new Request_ColgateReporting_Parametros()
                    {
                        campania = Request["_a"].ToString(),
                        categoria = Convert.ToInt32(__a),
                        periodo = Convert.ToInt32(__b),
                        oficina = Convert.ToInt32(__c),
                        bioficina = Convert.ToInt32(__d),
                        cadena = Convert.ToInt32(__e),
                        opcion = 4
                    }
                ));
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2016-04-04
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportingConsoPopPharmacies()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            if (Request.QueryString["_b"] != null)
            {
                new Insert_Visita_Persona_Response().Inserta_Visita_Modulo(
                    new Insert_Visita_Persona_Request()
                    {
                        person_id = ((Persona)Session["Session_Login"]).Person_id,
                        modulo_id = Request["_b"].ToString(),
                        company_id = ((Persona)Session["Session_Login"]).Company_id.ToString(),
                        fecha_ingreso = DateTime.Today.ToString("dd/MM/yyyy"),
                        visitas = 1,
                        createBy = ((Persona)Session["Session_Login"]).name_user
                    }
                );
            }

            return View();
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2016-04-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReportingConsoPopPharmacies(int __a, int __b, int __c, int __d, int __e)
        {
            ViewBag.campania = Request["_a"].ToString();
            ViewBag.categoria = __a.ToString();
            ViewBag.periodo = __b.ToString(); //32595;
            ViewBag.oficina = __c.ToString();
            ViewBag.bioficina = __d.ToString();
            ViewBag.cadena = __e.ToString(); //25;
            return View();
        }


        #endregion

        #endregion

    }
}