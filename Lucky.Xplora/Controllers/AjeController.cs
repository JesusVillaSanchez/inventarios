﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.NwRepStdAASS;
using Lucky.Xplora.Models.NwRepStdTRAD;
using Lucky.Xplora.Security;
using Lucky.Xplora.Models.Aje;
using System.IO;

using System.Drawing;
using System.Drawing.Imaging;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using System.Net;
using System.Configuration; 

namespace Lucky.Xplora.Controllers
{
    public class AjeController : Controller
    {
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        string RutaXplora = Convert.ToString(ConfigurationManager.AppSettings["XploraTemp"]);

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
      
        #region <<< Reporte Precio >>>
        public ActionResult Precio()
        {
            return View();
        }
        [CustomAuthorize]
        public ActionResult Precio2()
        {
            //NwRepPrecioAASS objprecio = new NwRepPrecioAASS();
            return View();
        }
        public ActionResult GrillaPrecio(String __a, String __b, String __c, String __d, String __e, String __f, int __g, String __h, String __i, String __j, String __k, String __l, String __m)
        {
            return View(new NwRepPrecioAASS().Consulta_Reporte(new Request_NWRepStd_General
            {
                subcanal = __a,
                cadena = __b,
                categoria = __c,
                empresa = __d,
                marca = __e,
                SKU = __f,
                periodo = __g,
                zona = __h,
                distrito = "0",
                tienda = "0",
                segmento = "0",
                Persona = ((Persona)Session["Session_Login"]).Person_id,
                min = __l,
                max = __m
                // Persona = ((Persona)Session["Session_Login"]).Person_id, 
                //edwin 
            }));
        }

        public ActionResult IndexPrecio(String __a, String __b, String __c, int __d, String __e)
        {
            return View(new NwRepPrecioAASS().Consulta_IndexPrecio(new Request_NWRepStd_General
            {
                subcanal = __a,
                cadena = __b,
                categoria = __c,
                empresa = "0",
                marca = __e,
                SKU = "0",
                periodo = __d,
                zona = "",
                distrito = "",
                tienda = "",
                segmento = "",
                Persona = ((Persona)Session["Session_Login"]).Person_id
            }));
        }

        //guia
        public JsonResult PrecioExcel(String __a, String __b, String __c, String __d, String __e, String __f, int __g, String __h, String __i, String __j, String __k)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Precio >>

                List<E_Excel_Precio_AASS> oPrecio = new NwRepPrecioAASS().Excel_Precio(
                   new Request_NWRepStd_General()
                   {
                       subcanal = __a,
                       cadena = __b,
                       categoria = __c,
                       empresa = __d,
                       marca = __e,
                       SKU = __f,
                       periodo = __g,
                       zona = __h,
                       distrito = __i,
                       tienda = __j,
                       segmento = __k,
                       Persona = ((Persona)Session["Session_Login"]).Person_id
                   });

                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Precio");
                    oWsPrecio.Cells[1, 1].Value = "Cadena";
                    oWsPrecio.Cells[1, 2].Value = "Codigo PDV";
                    oWsPrecio.Cells[1, 3].Value = "Nombre PDV";
                    oWsPrecio.Cells[1, 4].Value = "Empresa";
                    oWsPrecio.Cells[1, 5].Value = "Presentacion";
                    oWsPrecio.Cells[1, 6].Value = "Categoria";
                    oWsPrecio.Cells[1, 7].Value = "Marca";
                    oWsPrecio.Cells[1, 8].Value = "Producto";
                    oWsPrecio.Cells[1, 9].Value = "Precio_Regular";
                    oWsPrecio.Cells[1, 10].Value = "Precio_Oferta";
                    oWsPrecio.Cells[1, 11].Value = "Tipo_Precio";
                    oWsPrecio.Cells[1, 12].Value = "Periodo";

                    _fila = 2;
                    foreach (E_Excel_Precio_AASS oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.Cadena;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.Cod_PDV;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.PDV;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.Empresa;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.Presentacion;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.Categoria;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.Marca;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.Producto;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.P_Regular;
                        oWsPrecio.Cells[_fila, 10].Value = oBj.P_Oferta;
                        oWsPrecio.Cells[_fila, 11].Value = oBj.Tipoprecio;
                        oWsPrecio.Cells[_fila, 12].Value = oBj.Periodo;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        //haciendo
        public JsonResult PrecioIndexExcel(String __a, String __b, String __c, int __d, String __e)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Index_Precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Precio >>

                List<E_excel_index_price_AA_SS_edw> oPrecio = new NwRepPrecioAASS().Excel_index_Precio_edw(
                   new Request_NWRepStd_General()
                   {
                       subcanal = __a,
                       cadena = __b,
                       categoria = __c,
                       empresa = "0",
                       marca = __e,
                       SKU = "0",
                       periodo = __d,
                       zona = "",
                       distrito = "",
                       tienda = "",
                       segmento = "",
                       Persona = ((Persona)Session["Session_Login"]).Person_id
                   });
                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Index Precio");
                    //oWsPrecio.Cells[1, 1].Value = "Cadena";
                    //oWsPrecio.Cells[1, 2].Value = "Codigo PDV";
                    //oWsPrecio.Cells[1, 3].Value = "Nombre PDV";
                    //oWsPrecio.Cells[1, 4].Value = "Empresa";
                    //oWsPrecio.Cells[1, 5].Value = "Presentacion";


                    //AB CDEF GHIJ KLMN OPQR STUV
                    //Combinacion de celdas

                    oWsPrecio.Cells["C1:F1"].Merge = true;
                    oWsPrecio.Cells["G1:J1"].Merge = true;
                    oWsPrecio.Cells["K1:N1"].Merge = true;
                    oWsPrecio.Cells["O1:R1"].Merge = true;
                    oWsPrecio.Cells["S1:V1"].Merge = true;
                    
                    //dando valor a celdas combinadas
                    
                    oWsPrecio.Cells["C1:F1"].Value = "METRO";
                    oWsPrecio.Cells["G1:J1"].Value = "VIVANDA";
                    oWsPrecio.Cells["K1:N1"].Value = "PLAZA VEA";
                    oWsPrecio.Cells["O1:R1"].Value = "WONG";
                    oWsPrecio.Cells["S1:V1"].Value = "TOTUS";

                    oWsPrecio.Cells["A2"].Value = "Presentacion";
                    oWsPrecio.Cells["B2"].Value = "Descripcion";

                    oWsPrecio.Cells["C2"].Value = "P. Reg.";
                    oWsPrecio.Cells["D2"].Value = "Index";
                    oWsPrecio.Cells["E2"].Value = "P. Peso";
                    oWsPrecio.Cells["F2"].Value = "N.Index.";

                    oWsPrecio.Cells["G2"].Value = "P. Reg.";
                    oWsPrecio.Cells["H2"].Value = "Index";
                    oWsPrecio.Cells["I2"].Value = "P. Peso";
                    oWsPrecio.Cells["J2"].Value = "N.Index.";

                    oWsPrecio.Cells["K2"].Value = "P. Reg.";
                    oWsPrecio.Cells["L2"].Value = "Index";
                    oWsPrecio.Cells["M2"].Value = "P. Peso";
                    oWsPrecio.Cells["N2"].Value = "N.Index.";

                    oWsPrecio.Cells["O2"].Value = "P. Reg.";
                    oWsPrecio.Cells["P2"].Value = "Index";
                    oWsPrecio.Cells["Q2"].Value = "P. Peso";
                    oWsPrecio.Cells["R2"].Value = "N.Index.";

                    oWsPrecio.Cells["S2"].Value = "P. Reg.";
                    oWsPrecio.Cells["T2"].Value = "Index";
                    oWsPrecio.Cells["U2"].Value = "P. Peso";
                    oWsPrecio.Cells["V2"].Value = "N.Index.";

                    _fila = 3;
                    foreach (E_excel_index_price_AA_SS_edw oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.codigo;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.orden;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.nombre;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.codigo;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.orden;

                        //oWsPrecio.Cells[_fila, 6].Value = oBj.Cod_periodo;
                        //oWsPrecio.Cells[_fila, 7].Value = oBj.Cod_cadena ;
                        //oWsPrecio.Cells[_fila, 8].Value = oBj.Id_product ;
                        //oWsPrecio.Cells[_fila, 9].Value = oBj.Propio ;
                        //oWsPrecio.Cells[_fila, 10].Value = oBj.Cod_padre;
                        
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 1].AutoFilter = true;
                    //oWsUna.SelectedRange["A2:M2"].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    oWsPrecio.Row(2).Height = 25;
                    oWsPrecio.Row(2).Style.Font.Bold = true;
                    oWsPrecio.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    //oWsPrecio.Column(6).AutoFit();
                    //oWsPrecio.Column(7).AutoFit();
                    //oWsPrecio.Column(8).AutoFit();
                    //oWsPrecio.Column(9).AutoFit();
                    //oWsPrecio.Column(10).AutoFit();
                    //oWsPrecio.Column(11).AutoFit();
                    //oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(4, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        //duplicado
        public JsonResult PrecioIndexExcel2(String __a, String __b, String __c, int __d, String __e)
        {
            string _fileServer = "";
            string _filePath = "";

            _fileServer = String.Format("Precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Precio >>

                E_Index_Precio_AASS oPrecio = new NwRepPrecioAASS().Consulta_IndexPrecio(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    empresa = "0",
                    marca = __e,
                    SKU = "0",
                    periodo = __d,
                    zona = "",
                    distrito = "",
                    tienda = "",
                    segmento = "",
                    Persona = ((Persona)Session["Session_Login"]).Person_id
                });

                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Precio");
                    oWsPrecio.Cells[1, 1, 1, 2].Merge = true;
                    oWsPrecio.Cells[1, 1, 1, 2].Value = "";
                    oWsPrecio.Cells[2, 1].Value = "Presentacion";
                    oWsPrecio.Cells[2, 2].Value = "Descripcion";

                    for (int k = 1; k <= 2; k++)
                    {
                        oWsPrecio.Cells[2, k].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, k].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, k].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, k].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;
                    }

                    int colcab = 3;
                    foreach (var item in oPrecio.Cadenas)
                    {

                        oWsPrecio.Cells[1, colcab].Value = "";
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Merge = true;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Value = item.ToString();

                        //dando borde
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;


                        Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#9BC2E6");
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex1);

                        oWsPrecio.Cells[2, colcab].Value = "";
                        oWsPrecio.Cells[2, (colcab + 1)].Value = "Precio";
                        oWsPrecio.Cells[2, (colcab + 2)].Value = "Index";
                        oWsPrecio.Cells[2, (colcab + 3)].Value = "P. Peso";
                        oWsPrecio.Cells[2, (colcab + 4)].Value = "N. Index";

                        //background color negro
                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#000000");
                        oWsPrecio.Cells[2, (colcab + 3), 2, (colcab + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsPrecio.Cells[2, (colcab + 3), 2, (colcab + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex2);
                        //pintando color a blanco
                        Color colFromHex3 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                        oWsPrecio.Cells[2, (colcab + 3), 2, (colcab + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsPrecio.Cells[2, (colcab + 3), 2, (colcab + 4)].Style.Font.Color.SetColor(colFromHex3);

                        //dando borde
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                        //alineando al centro
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        //alineado vertical al medio
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //oWsPrecio.SelectedRange[2, (sw + 1), 2, (sw + 4)].AutoFilter = true;

                        //dando ancho a columnas
                        oWsPrecio.Column(colcab).Width = 1;
                        oWsPrecio.Column(colcab + 1).Width = 8;
                        oWsPrecio.Column(colcab + 2).Width = 8;
                        oWsPrecio.Column(colcab + 3).Width = 8;
                        oWsPrecio.Column(colcab + 4).Width = 8;

                        colcab = colcab + 5;

                    }
                    int colcuerpo = 3;
                    int rowcuerpo = 3;
                    foreach (var item2 in oPrecio.Grupos)
                    {
                        foreach (var row in item2.Filas)
                        {
                            if (row.Propio=="1")
                            {
                                Color colFromHex4 = System.Drawing.ColorTranslator.FromHtml("#C6E0B4");
                                oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Fill.BackgroundColor.SetColor(colFromHex4);
                            }
                            else
                            {
                                Color colFromHex4 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                                oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Fill.BackgroundColor.SetColor(colFromHex4);
                            }
                            oWsPrecio.Cells[rowcuerpo, 1].Value = row.Presentacion;
                            oWsPrecio.Cells[rowcuerpo, 2].Value = row.Producto;

                            //dando borde
                            oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                            oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                            oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                            int conta = 0;
                            foreach (var vfg in row.Cadenas)
                            {
                                if (row.Propio == "1")
                                {
                                    Color colFromHex5 = System.Drawing.ColorTranslator.FromHtml("#C6E0B4");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex5);
                                }
                                else
                                {
                                    Color colFromHex5 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex5);
                                }

                                if (vfg.T_precio=="1")
                                {
                                    Color colFromHex6 = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Style.Font.Color.SetColor(colFromHex6);
                                }
                                else
                                {
                                    Color colFromHex6 = System.Drawing.ColorTranslator.FromHtml("#000000");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Style.Font.Color.SetColor(colFromHex6);
                                }
                                oWsPrecio.Cells[rowcuerpo, colcuerpo].Value = "";

                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Value = decimal.Parse(vfg.P_Regular);
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 2)].Value = decimal.Parse(vfg.Index);
                                if (vfg.P_Peso == "")
                                {
                                    vfg.P_Peso = "0";
                                }
                                else
                                {
                                    vfg.P_Peso = vfg.P_Peso;
                                }
                                if (vfg.N_Index == "")
                                {
                                    vfg.N_Index = "0";
                                }
                                else
                                {
                                    vfg.N_Index = vfg.N_Index;
                                }
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 3)].Value = decimal.Parse(vfg.P_Peso);
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 4)].Value = decimal.Parse(vfg.N_Index);

                                //dando borde
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;


                                //alineando al centro
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                                //alineado vertical al medio
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;


                                colcuerpo += 5;
                                conta += 1;
                            }
                            if (conta < Convert.ToInt32(oPrecio.Cadenas.Count()))
                            {
                                for (int i = 1; conta < Convert.ToInt32(oPrecio.Cadenas.Count()); i++)
                                {
                                    oWsPrecio.Cells[rowcuerpo, colcuerpo].Value = "";
                                    Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#C0C0C0");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex2);


                                    //dando borde
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Value = "";
                                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo + 2)].Value = "";
                                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo + 3)].Value = "";
                                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo + 4)].Value = "";

                                    colcuerpo += 5;
                                    conta += 1;
                                }
                            }
                            colcuerpo = 3;
                            rowcuerpo += 1;
                        }
                        oWsPrecio.Cells[rowcuerpo, 1].Value = "";
                        oWsPrecio.Cells[rowcuerpo, 2].Value = "";
                        for (int j = 1; j < Convert.ToInt32(oPrecio.Cadenas.Count()); j++)
                        {
                            oWsPrecio.Cells[rowcuerpo, colcuerpo].Value = "";
                            oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Value = "";
                            oWsPrecio.Cells[rowcuerpo, (colcuerpo + 2)].Value = "";
                            oWsPrecio.Cells[rowcuerpo, (colcuerpo + 3)].Value = "";
                            oWsPrecio.Cells[rowcuerpo, (colcuerpo + 4)].Value = "";

                            colcuerpo += 5;
                        }
                        colcuerpo = 3;
                        rowcuerpo += 1;
                    }

                    ////leyenda
                    //rowcuerpo += 1;
                    //int colcuerpo2 = 4;

                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, rowcuerpo, (colcuerpo2 + 1)].Merge = true;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2].Value = "letra roja";
                    

                    //oWsPrecio.Cells[(rowcuerpo + 1), colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 1)].Merge = true;
                    //oWsPrecio.Cells[(rowcuerpo + 1), colcuerpo2].Value = "letra roja";
                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo2 + 2), (rowcuerpo + 1), (colcuerpo2 + 3)].Merge = true;
                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo2 + 2), (rowcuerpo + 1), (colcuerpo2 + 3)].Value = "P. Oferta";

                    ////estilos de leyenda
                    ////dando borde
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;
                    ////alineando al centro
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                    ////alineado vertical al medio
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    ////pintando color a rojo
                    //Color colFromHex7 = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Font.Color.SetColor(colFromHex7);
                    ////pintando fondo a verde bajo
                    //Color colFromHex8 = System.Drawing.ColorTranslator.FromHtml("#C6E0B4");
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, rowcuerpo, (colcuerpo2 + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, rowcuerpo, (colcuerpo2 + 1)].Style.Fill.BackgroundColor.SetColor(colFromHex8);
                    ////pintando fondo a blanco
                    //Color colFromHex9 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                    //oWsPrecio.Cells[(rowcuerpo + 1), colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsPrecio.Cells[(rowcuerpo + 1), colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 1)].Style.Fill.BackgroundColor.SetColor(colFromHex9);
                    //Color colFromHex10 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo2 + 2), (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo2 + 2), (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Fill.BackgroundColor.SetColor(colFromHex10);


                    //////////////////////////////////////////////////////////////
                    ////dando alto
                    //oWsPrecio.Row(1).Height = 25;
                    //oWsPrecio.Row(2).Height = 20;

                    ////dando estilo negrita
                    //oWsPrecio.Row(1).Style.Font.Bold = true;
                    //oWsPrecio.Row(2).Style.Font.Bold = true;

                    ////alineacion al centro
                    //oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;


                    ////alineacion vertical al medio
                    //oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    ////Columnas auto ajustadas
                    //oWsPrecio.Column(1).AutoFit();
                    //oWsPrecio.Column(2).AutoFit();

                    //oWsPrecio.View.FreezePanes(3, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        #endregion
        
        public ActionResult Filtro(String parametro, int oop)
        {
            ViewBag.opcion = oop;

            return View(new NwRepPrecioAASS().Filtro(new Request_NWRepStd_General { Parametros = parametro, op = oop }));
        }
        public ActionResult StockOut()
        {
            return View();
        }

        #region <<< AJE TRADICIONAL >>>

        public ActionResult Ippf()
        {
            return View();
        }
        public ActionResult IppfPartial1(String _a, int _b, int _c, int _d)
        {
            return View(new NwRepIPPFTrad().Consulta_G1(new Request_NWRepStd_Tradicional { Equipo = _a, Anio = _b, Mes = _c, Region = _d, Persona = ((Persona)Session["Session_Login"]).Person_id }));
        }
        public ActionResult IppfPartial2(String _a, int _b, int _c, int _d, int _e)
        {
            return View(new NwRepIPPFTrad().Consulta_G2(new Request_NWRepStd_Tradicional { Equipo = _a, Anio = _b, Mes = _c, Region = _d, Persona = ((Persona)Session["Session_Login"]).Person_id, Distribuidora = _e }));
        }
        public ActionResult IppfPartial3(String _a, int _b, int _c, int _d, int _e)
        {
            return View(new NwRepIPPFTrad().Consulta_G3(new Request_NWRepStd_Tradicional { Equipo = _a, Anio = _b, Mes = _c, Region = _d, Persona = ((Persona)Session["Session_Login"]).Person_id, Distribuidora = _e }));
        }
        public ActionResult IppfPartial4(String _a, int _b, int _c, int _d, int _e, int _f)
        {
            ViewBag.cod_elemento = _f;
            return View(new NwRepIPPFTrad().Consulta_G4(new Request_NWRepStd_Tradicional { Equipo = _a, Anio = _b, Mes = _c, Region = _d, Persona = ((Persona)Session["Session_Login"]).Person_id, Distribuidora = _e, Elemento = _f }));
        }
        public ActionResult IppfFiltro(int _a, string _b, string _c)
        {
            ViewBag.cod_opcion = _a;
            ViewBag.id_elemento = _c;
            return View(new NwRepIPPFTrad().Filtros(new Request_NWRepStd_Tradicional { Opcion = _a, Parametros = _b }));
        }

        public ActionResult IppfPartial5(String _a, int _b, int _c, int _d, int _e, int _f)
        {

            return Json(new { Archivo = new NwRepIPPFTrad().Consulta_G5(new Request_NWRepStd_Tradicional { Equipo = _a, Anio = _b, Mes = _c, Region = _d, Persona = ((Persona)Session["Session_Login"]).Person_id, Distribuidora = _e, Elemento = _f }) });
        }

        public ActionResult IppfPartial5Grilla(String _a, int _b, int _c, int _d, int _e, int _f)
        {
            ViewBag.cod_tipo = _f;
            return View(new NwRepIPPFTrad().Consulta_G5(new Request_NWRepStd_Tradicional { Equipo = _a, Anio = _b, Mes = _c, Region = _d, Persona = ((Persona)Session["Session_Login"]).Person_id, Distribuidora = _e, Elemento = _f }));
        }

        public JsonResult IppfPartial5GrillaExcel(String _a, int _b, int _c, int _d, int _e, int _f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Detalle_Reporte" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Detalle >>

                List<E_IPPF_TRAD_GRAD_5_elemento> oPrecio = new NwRepIPPFTrad().Consulta_G5(new Request_NWRepStd_Tradicional { Equipo = _a, Anio = _b, Mes = _c, Region = _d, Persona = ((Persona)Session["Session_Login"]).Person_id, Distribuidora = _e, Elemento = _f });

                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Detalle IPP+F");
                    oWsPrecio.Cells[1, 1].Value = "Codigo PDV";
                    oWsPrecio.Cells[1, 2].Value = "Nombre PDV";
                    oWsPrecio.Cells[1, 3].Value = "Direccion";
                    oWsPrecio.Cells[1, 4].Value = "Fecha Visita";
                    oWsPrecio.Cells[1, 5].Value = "Detalle Item";
                    oWsPrecio.Cells[1, 6].Value = "Evaluacion Item";
                    oWsPrecio.Cells[1, 7].Value = "Foto";
                    oWsPrecio.Cells[1, 8].Value = "Penalizado";
                    oWsPrecio.Cells[1, 9].Value = "Criterio";

                    if (_f == 1) {
                        oWsPrecio.Cells[1, 10].Value = "Cant. Marca";
                        oWsPrecio.Cells[1, 11].Value = "Marcas";
                    }

                    _fila = 2;
                    foreach (E_IPPF_TRAD_GRAD_5_elemento oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.Cod_pdv;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.Nom_pdv;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.Direccion;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.Fec_reg_cel;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.Detalle;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.Punto;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.Foto;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.Penalizado;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.Criterio;
                        if (_f == 1)
                        {
                            oWsPrecio.Cells[_fila, 10].Value = oBj.Cant_Marca;
                            oWsPrecio.Cells[_fila, 11].Value = oBj.Marcas;
                        }
                        _fila++;
                    }

                    //Formato Cabecera
                    if (_f == 1)
                    {
                        oWsPrecio.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                    }
                    else {
                        oWsPrecio.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    }
                    
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();

                    if (_f == 1) {
                        oWsPrecio.Column(10).AutoFit();
                        oWsPrecio.Column(11).AutoFit();
                    }
                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public ActionResult DashboardTrad()
        {
            return View();
        }

        public ActionResult Dash_Sop(int _a, int _b, string _c)
        {
            return Json(new { Archivo = new NwRepIPPFTrad().Dash_SOP(new Request_NWRepStd_Tradicional { Anio = _a, Mes = _b, Equipo = _c, Persona = ((Persona)Session["Session_Login"]).Person_id }) });
        }
        public ActionResult Dash_Inci(int _a, int _b, string _c, int _d)
        {
            return Json(new { Archivo = new NwRepIPPFTrad().Dash_INCIDENCIA(new Request_NWRepStd_Tradicional { Anio = _a, Mes = _b, Equipo = _c, Opcion = _d, Persona = ((Persona)Session["Session_Login"]).Person_id }) });
        }
        public ActionResult Dash_StockOut(int _a, int _b, string _c, int _d, int _e)
        {
            return Json(new { Archivo = new NwRepIPPFTrad().Dash_STOCKOUT(new Request_NWRepStd_Tradicional { Anio = _a, Mes = _b, Equipo = _c, Distribuidora = _d, Marca = _e, Persona = ((Persona)Session["Session_Login"]).Person_id }) });
        }
        public ActionResult Dash_Cobertura(int _a, int _b, string _c)
        {
            return Json(new { Archivo = new NwRepIPPFTrad().Dash_COBERTURA(new Request_NWRepStd_Tradicional { Anio = _a, Mes = _b, Equipo = _c, Persona = ((Persona)Session["Session_Login"]).Person_id }) });
        }
        public ActionResult Dash_ExhibVal(int _a, int _b, string _c,string _d,string _e)
        {
            return Json(new { Archivo = new NwRepIPPFTrad().Dash_EXHIBIVAL(new Request_NWRepStd_Tradicional { Anio = _a, Mes = _b, Equipo = _c, Persona = ((Persona)Session["Session_Login"]).Person_id , Categoria = _d,Material=_e}) });
        } 
        #endregion

        #region << yrodriguez >>
        #region Competencia

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 07-2016
        /// Descripcion: --
        /// </summary>
        /// <returns></returns>
        public ActionResult Competencia()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            return View();
        }
        [HttpPost]
        public ActionResult JsonChainCompany(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Competencia_Aje().Objeto_Competencia(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 0,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        categoria = __c.ToString(),
                        empresa = __d.ToString(),
                        marca = __e.ToString(),
                        periodo = __f.ToString(),
                        zona = __g.ToString(),
                        distrito = __h.ToString(),
                        tienda = __i.ToString(),
                        segmento = __j.ToString(),
                        tipoactividad = __k.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult JsonDetalleCompetencia(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            List<E_Reporting_Competencia_Detalle> oLs = new E_Reporting_Competencia_Detalle().Lista_CompetenciaDetalle(
                    new Request_AjeReporting_Parametros()
                    {
                        //edwin
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        categoria = __c.ToString(),
                        empresa = __d.ToString(),
                        marca = __e.ToString(),
                        periodo = __f.ToString(),
                        zona = __g.ToString(),
                        distrito = __h.ToString(),
                        tienda = __i.ToString(),
                        segmento = __j.ToString(),
                        tipoactividad = __k.ToString()
                    });

            return Json(new { Archivo = oLs });
        }

        public JsonResult Exportar_Competencia_Detalle(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Competencia_Detalle_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Competencia >>

                List<E_Reporting_Competencia_Detalle> oCompetencia = new E_Reporting_Competencia_Detalle().Lista_CompetenciaDetalle(
                   new Request_AjeReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       categoria = __c.ToString(),
                       empresa = __d.ToString(),
                       marca = __e.ToString(),
                       periodo = __f.ToString(),
                       zona = __g.ToString(),
                       distrito = __h.ToString(),
                       tienda = __i.ToString(),
                       segmento = __j.ToString(),
                       tipoactividad = __k.ToString()
                   });

                #endregion

                #endregion

                #region <<< Reporte Competencia >>>
                if ((oCompetencia != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Competencia");
                    oWsPrecio.Cells[1, 1].Value = "Oficina";
                    oWsPrecio.Cells[1, 2].Value = "Cadena";
                    oWsPrecio.Cells[1, 3].Value = "Fecha";
                    oWsPrecio.Cells[1, 4].Value = "Empresa";
                    oWsPrecio.Cells[1, 5].Value = "Marca";
                    oWsPrecio.Cells[1, 6].Value = "Tipo de Actividad";
                    oWsPrecio.Cells[1, 7].Value = "Descripcion Comercial";
                    oWsPrecio.Cells[1, 8].Value = "Foto";
                   


                    _fila = 2;
                    foreach (E_Reporting_Competencia_Detalle oBj in oCompetencia)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.oficina;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.cadena;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.fecha;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.empresa;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.marca;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.tipo_actividad;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.descripcion_comercial;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.foto;
                       
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 8].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                   

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult CompetenciaExcel(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Competencia_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Competencia >>

                List<E_Reporting_Competencia_Detalle> oCompetencia = new E_Reporting_Competencia_Detalle().Lista_CompetenciaDetalle(
                   new Request_AjeReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       categoria = __c.ToString(),
                       empresa = __d.ToString(),
                       marca = __e.ToString(),
                       periodo = __f.ToString(),
                       zona = __g.ToString(),
                       distrito = __h.ToString(),
                       tienda = __i.ToString(),
                       segmento = __j.ToString(),
                       tipoactividad = __k.ToString()
                   });

                #endregion

                #endregion

                #region <<< Reporte Competencia >>>
                if ((oCompetencia != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Competencia");
                    oWsPrecio.Cells[1, 1].Value = "Oficina";
                    oWsPrecio.Cells[1, 2].Value = "Cadena";
                    oWsPrecio.Cells[1, 3].Value = "Fecha";
                    oWsPrecio.Cells[1, 4].Value = "Empresa";
                    oWsPrecio.Cells[1, 5].Value = "Marca";
                    oWsPrecio.Cells[1, 6].Value = "Tipo de Actividad";
                    oWsPrecio.Cells[1, 7].Value = "Descripcion Comercial";
                    oWsPrecio.Cells[1, 8].Value = "Categoria";
                    //oWsPrecio.Cells[1, 9].Value = "Foto";
                    oWsPrecio.Cells[1, 9].Value = "Alcance";
                    oWsPrecio.Cells[1, 10].Value = "Vigencia";
                    oWsPrecio.Cells[1, 11].Value = "Codigo";
                    oWsPrecio.Cells[1, 12].Value = "Precio";
                    oWsPrecio.Cells[1, 13].Value = "Gramaje";
                    oWsPrecio.Cells[1, 14].Value = "Impulso";
                    oWsPrecio.Cells[1, 15].Value = "POP";
                    oWsPrecio.Cells[1, 16].Value = "Comentarios Adicionales";


                    _fila = 2;
                    foreach (E_Reporting_Competencia_Detalle oBj in oCompetencia)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.oficina;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.cadena;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.fecha;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.empresa;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.marca;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.tipo_actividad;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.descripcion_comercial;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.categoria;
                        //oWsPrecio.Cells[_fila, 9].Value = oBj.foto;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.alcance;
                        oWsPrecio.Cells[_fila, 10].Value = oBj.vigencia;
                        oWsPrecio.Cells[_fila, 11].Value = oBj.codigo;
                        oWsPrecio.Cells[_fila, 12].Value = oBj.precio;
                        oWsPrecio.Cells[_fila, 13].Value = oBj.gramaje;
                        oWsPrecio.Cells[_fila, 14].Value = oBj.impulso;
                        oWsPrecio.Cells[_fila, 15].Value = oBj.pop;
                        oWsPrecio.Cells[_fila, 16].Value = oBj.comentarios_adicionales;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 16].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        #endregion

        #region Encarte
        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 07-2016
        /// Descripcion: --
        /// </summary>
        /// <returns></returns>
        public ActionResult Encarte()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            return View();
        }

        [HttpPost]
        //Carga el reporte de graficos
        public ActionResult JsonDataEncarte(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Encarte_Aje().Objeto_Encarte(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 0,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        categoria = __c.ToString(),
                        empresa = __d.ToString(),
                        marca = __e.ToString(),
                        periodo = __f.ToString(),
                        zona = __g.ToString(),
                        distrito = __h.ToString(),
                        tienda = __i.ToString(),
                        segmento = __j.ToString(),
                        tipoactividad = __k.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        //Carga la data de la ventana modal.
        public ActionResult JsonDetalleEncarte(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            List<E_Reporting_Encarte_Detalle> oLs = new E_Reporting_Encarte_Detalle().Lista_EncarteDetalle(
                    new Request_AjeReporting_Parametros()
                    {
                        subcanal = Convert.ToInt32(__a), 
                        cadena = __b.ToString(),
                        categoria = __c.ToString(),
                        empresa = __d.ToString(),
                        marca = __e.ToString(),
                        periodo = __f.ToString(),
                        zona = __g.ToString(),
                        distrito = __h.ToString(),
                        tienda = __i.ToString(),
                        segmento = __j.ToString(),
                        tipoactividad = __k.ToString()
                    });

            return Json(new { Archivo = oLs });
        }

        //guia lunes 1
        public JsonResult ExportarJsonDetalleEncarte(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("DetalleEncarte_s" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Encarte >>

                List<E_Reporting_Encarte_Detalle> oEncarte = new E_Reporting_Encarte_Detalle().Lista_EncarteDetalle(
                   new Request_AjeReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       categoria = __c.ToString(),
                       empresa = __d.ToString(),
                       marca = __e.ToString(),
                       periodo = __f.ToString(),
                       zona = __g.ToString(),
                       distrito = __h.ToString(),
                       tienda = __i.ToString(),
                       segmento = __j.ToString(),
                       tipoactividad = __k.ToString()
                   });

                #endregion

                #endregion

                #region <<< Reporte Encarte >>>
                if ((oEncarte != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Encarte");

                    oWsPrecio.Cells[1, 1].Value = "Cadena";
                    oWsPrecio.Cells[1, 2].Value = "Categoria";
                    oWsPrecio.Cells[1, 3].Value = "Fecha Inicio";
                    oWsPrecio.Cells[1, 4].Value = "Fecha Fin";
                    oWsPrecio.Cells[1, 5].Value = "Marca";
                    oWsPrecio.Cells[1, 6].Value = "Producto";
                    oWsPrecio.Cells[1, 7].Value = "Mecanica";
                    oWsPrecio.Cells[1, 8].Value = "Tipo de Anuncio";
                    

                    _fila = 2;
                    foreach (E_Reporting_Encarte_Detalle oBj in oEncarte)
                    {
                        
                        oWsPrecio.Cells[_fila, 1].Value = oBj.cadena;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.categoria;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.fecha_inico;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.fecha_fin;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.marca;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.producto;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.mecanica;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.tipo_anuncio;
                        
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 8].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
       
        //haciendo lunes 1
        public ActionResult ExportarJsonExhibicion(string __a, string __b, int __c, string __d, int __e)
        {
            #region
            string _fileServer = "";
            string _filePath = "";
            int _fila = 0;

            _fileServer = String.Format("Exhibicion_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            #endregion
            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Encarte >>
               
                List<E_Reporting_Excibicion_AASS> oEncarte = new E_Reporting_Excibicion_AASS().Exportar_Reporting_Exhibicion_AASS(
                 new Request_AjeReporting_Parametros()
                 {
                     cod_equipo = "55052016652016",
                     subcanal2 = "0",
                     cadena = "0",
                     categoria = Convert.ToString(__a),
                     empresa = "0",
                     marca = Convert.ToString(__b),
                     periodo2 = Convert.ToInt32(__c),
                     zona = Convert.ToString(__d),
                     distrito = "0",
                     cod_pdv = "0",
                     segmento = Convert.ToString(__e),
                     cod_material = "0",
                     opcion = 0
                 });

                #endregion

                #endregion

                #region <<< Reporte Encarte >>>
                if ((oEncarte != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Exhibicion");


                    oWsPrecio.Cells[1, 1].Value = "Mat descripcion";
                    oWsPrecio.Cells[1, 2].Value = "Mar descripcion";
                    oWsPrecio.Cells[1, 3].Value = "Mar cantidad";
                    oWsPrecio.Cells[1, 4].Value = "Mar valorizado";
                    oWsPrecio.Cells[1, 5].Value = "Cadena";
                    oWsPrecio.Cells[1, 6].Value = "Nombre Pdv";
                    oWsPrecio.Cells[1, 7].Value = "Mes";
                    oWsPrecio.Cells[1, 8].Value = "Periodo";

                    oWsPrecio.Cells[1, 9].Value = "foto";


                    _fila = 2;
                    foreach (E_Reporting_Excibicion_AASS oBj in oEncarte)
                    {

                        oWsPrecio.Cells[_fila, 1].Value = oBj.mat_descripcion   ;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.mar_descripcion   ;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.mar_cantidad      ;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.mar_valorizado    ;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.commercialNodeName;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.nombre_pdv;

                        #region
                        if (oBj.foto =="")
                        {
                            oWsPrecio.Cells[_fila, 9].Value = "SIN FOTO";
                        }
                       
                        if (oBj.id_Month =="01")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "ENERO";
                            oWsPrecio.Cells[_fila, 8].Value = "ENERO - " +  oBj.ReportsPlanning_Periodo;
                            
                        }
                        else if (oBj.id_Month == "02")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "FEBRERO";
                            oWsPrecio.Cells[_fila, 8].Value = "FEBRERO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "03")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "MARZO";
                            oWsPrecio.Cells[_fila, 8].Value = "MARZO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "04")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "ABRIL";
                            oWsPrecio.Cells[_fila, 8].Value = "ABRIL - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "05")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "MAYO";
                            oWsPrecio.Cells[_fila, 8].Value = "MAYO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "06")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "JUONI";
                            oWsPrecio.Cells[_fila, 8].Value = "JUNIO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "07")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "JULIO";
                            oWsPrecio.Cells[_fila, 8].Value = "JULIO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "08")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "AGOSTO";
                            oWsPrecio.Cells[_fila, 8].Value = "AGOSTO - " + oBj.ReportsPlanning_Periodo;

                        }
                        else if (oBj.id_Month == "09")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "SETIEMBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "SETIEMBRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "10")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "OCTUBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "OCTUBRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "11")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "NOVIEMBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "NOVIEMNRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "12")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "DICIEMBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "DICIEMBRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        _fila++;

                        #endregion

                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        //edwin
        public ActionResult     ExportarDuple(string __a, string __b, int __c, string __d, int __e)
        {
            #region
            string _fileServer = "";
            string _filePath = "";
            //int _fila = 0;

            _fileServer = String.Format("Exhibicion_Detalle_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            #endregion
            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Encarte >>

                List<AjeExhibicionAdicionalDetalleMaterialReporte> oExhibicionAdicional = new AjeExhibicionAdicionalDetalleMaterialReporte().Exportar_Detalle_Reporting_Exhibicion_AASS(
                 new Request_AjeReporting_Parametros()
                 {
                     cod_equipo = "55052016652016",
                     subcanal2 = "0",
                     cadena = "0",
                     categoria = Convert.ToString(__a),
                     empresa = "0",
                     marca = Convert.ToString(__b),
                     periodo2 = Convert.ToInt32(__c),
                     zona = Convert.ToString(__d),
                     distrito = "0",
                     cod_pdv = "0",
                     segmento = Convert.ToString(__e),
                     cod_material = "0",
                     opcion = 0
                 });

                #endregion

                #endregion

                #region <<< Reporte Encarte >>>

                if ((oExhibicionAdicional != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Exhibicion Detalle");

                    oWsPrecio.Cells[1, 1].Value = "";
                    oWsPrecio.Cells[1, 2].Value = "";

                    //Fila 2
                    oWsPrecio.Cells[2, 1].Value = "";
                    oWsPrecio.Cells[2, 2].Value = "";

                    int CuentaReg = 0; 

                    int sw = 3;
                    //int Fila = 3;
                    //int con = 0;

                    var list_PDV = (from v_campos in oExhibicionAdicional
                                     where v_campos.pdv_codigo != "0"
                                     select new { 
                                        cod_pdv = v_campos.pdv_codigo,
                                        pdv_nombre = v_campos.pdv_descripcion
                                     }).Distinct().ToList();

                    var list_material = (from v_campos in oExhibicionAdicional
                                             where v_campos.pdv_codigo =="0"
                                             select new {
                                                codigo = v_campos.mat_codigo,
                                                descrip = v_campos.mat_descripcion,
                                                existe = v_campos.existe
                                             }).Distinct().ToList();

                    foreach(var item in list_material){
                        oWsPrecio.Cells[1, sw].Value = item.descrip;
                        oWsPrecio.Cells[2, sw].Value = Convert.ToInt32(item.existe);
                        sw++;
                        CuentaReg++;
                    }
                    int swFila = 3;
                    int swFilanum = 1;
                    int swcolumn = 3;
                    foreach (var item in list_PDV)
                    {
                        oWsPrecio.Cells[swFila, 1].Value = swFilanum;
                        oWsPrecio.Cells[swFila, 1].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsPrecio.Cells[swFila, 2].Value = Convert.ToString(item.pdv_nombre);
                    
                        foreach (var vitem in list_material) {
                            String vexiste = "0";
                            
                            var list_data = (from v_campos in oExhibicionAdicional
                                             where v_campos.pdv_codigo == Convert.ToString(item.cod_pdv) && v_campos.mat_codigo == Convert.ToString(vitem.codigo)
                                             select new {
                                                valor = v_campos.existe
                                             }).Distinct().ToList();

                            foreach (var xitem in list_data)
                            {
                                vexiste = Convert.ToString(xitem.valor);
                            }

                            oWsPrecio.Cells[swFila, swcolumn].Value = Convert.ToInt32(vexiste);
                            oWsPrecio.Cells[swFila, swcolumn].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWsPrecio.Cells[swFila, swcolumn].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                            swcolumn++;
                        }
                        swcolumn = 3;
                        swFila++;
                        swFilanum++;
                    }

                    //Formato Cabecera
                    //oWsPrecio.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                    oWsPrecio.Row(2).Height = 25;
                    oWsPrecio.Row(2).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    ////Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    //oWsPrecio.Column(3).AutoFit();
                    //oWsPrecio.Column(4).AutoFit();
                    //oWsPrecio.Column(5).AutoFit();
                    //oWsPrecio.Column(6).AutoFit();
                    //oWsPrecio.Column(7).AutoFit();
                    //oWsPrecio.Column(8).AutoFit();
                    //oWsPrecio.Column(9).AutoFit();
                    //oWsPrecio.Column(10).AutoFit();
                    //oWsPrecio.Column(11).AutoFit();
                    //oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(3, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }


        public ActionResult ExportarJsonDetallExhibicion(string __a, string __b, int __c, string __d, int __e)
        {
            #region
            string _fileServer = "";
            string _filePath = "";
            //int _fila = 0;

            _fileServer = String.Format("Exhibicion_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            #endregion
            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Encarte >>
                //Exportar_Detalle_Reporting_Exhibicion_AASS
                List<AjeExhibicionAdicionalDetalleMaterialReporte> oDatelleExhibicion = new AjeExhibicionAdicionalDetalleMaterialReporte().Exportar_Detalle_Reporting_Exhibicion_AASS(
                 new Request_AjeReporting_Parametros()
                 {

                     cod_equipo = "55052016652016",
                     subcanal2 = "0",
                     cadena = "0",
                     categoria = Convert.ToString(__a),
                     empresa = "0",
                     marca = Convert.ToString(__b),
                     periodo2 = Convert.ToInt32(__c),
                     zona = Convert.ToString(__d),
                     distrito = "0",
                     cod_pdv = "0",
                     segmento = Convert.ToString(__e),
                     cod_material = "0",
                     opcion = 0
                 });

                #endregion

                #endregion

                #region <<< Reporte Encarte >>>
                if ((oDatelleExhibicion != null))
                {

                    #region
                    Excel.ExcelWorksheet oWsDetalleEx = oEx.Workbook.Worksheets.Add("Reporte Exhibicion");

                    //oWsDetalleEx.Cells[1, 1].Value = "Mat descripcion";
                    //oWsDetalleEx.Cells[1, 2].Value = "Mar descripcion";
                    //oWsDetalleEx.Cells[1, 3].Value = "Mar cantidad";
                    //oWsDetalleEx.Cells[1, 4].Value = "Mar valorizado";
                    //oWsDetalleEx.Cells[1, 5].Value = "Cadena";
                    //oWsDetalleEx.Cells[1, 6].Value = "Nombre Pdv";


                    //_fila = 2;
                    //foreach (AjeExhibicionAdicionalDetalleMaterialReporte oBj in oDatelleExhibicion)
                    //{

                    //    oWsDetalleEx.Cells[_fila, 1].Value = oBj.mat_descripcion;
                    //    oWsDetalleEx.Cells[_fila, 2].Value = oBj.pdv_codigo;
                    //    oWsDetalleEx.Cells[_fila, 3].Value = oBj.mat_codigo;
                    //    oWsDetalleEx.Cells[_fila, 4].Value = oBj.foto;
                    //    oWsDetalleEx.Cells[_fila, 5].Value = oBj.pdv_descripcion;
                    //    oWsDetalleEx.Cells[_fila, 6].Value = oBj.foto;
                    //}

                    ////Formato Cabecera
                    //oWsDetalleEx.SelectedRange[1, 1, 1, 6].AutoFilter = true;
                    //oWsDetalleEx.Row(1).Height = 25;
                    //oWsDetalleEx.Row(1).Style.Font.Bold = true;
                    //oWsDetalleEx.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    //oWsDetalleEx.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    ////Columnas auto ajustadas
                    //oWsDetalleEx.Column(1).AutoFit();
                    //oWsDetalleEx.Column(2).AutoFit();
                    //oWsDetalleEx.Column(3).AutoFit();
                    //oWsDetalleEx.Column(4).AutoFit();
                    //oWsDetalleEx.Column(5).AutoFit();
                    //oWsDetalleEx.Column(6).AutoFit();
                    //oWsDetalleEx.Column(7).AutoFit();
                    //oWsDetalleEx.Column(8).AutoFit();
                    //oWsDetalleEx.Column(9).AutoFit();
                    //oWsDetalleEx.Column(10).AutoFit();
                    //oWsDetalleEx.Column(11).AutoFit();
                    //oWsDetalleEx.Column(12).AutoFit();

                    //oWsDetalleEx.View.FreezePanes(2, 1);


                    //oEx.Save();
                     
                    #endregion

                    #region
                    //Excel.ExcelWorksheet oWsExhibicion = oEx.Workbook.Worksheets.Add("Reporte Exhibicion");

                    //Fila 1
                    oWsDetalleEx.Cells[1, 1].Value = "";
                    oWsDetalleEx.Cells[1, 2].Value = "";

                    //Fila 2
                    oWsDetalleEx.Cells[2, 1].Value = "";
                    oWsDetalleEx.Cells[2, 2].Value = "";

                    int sw = 3;
                    foreach (var item in oDatelleExhibicion)
                    {
                        if (item.pdv_codigo == "0")
                        {
                            oWsDetalleEx.Cells[1, sw].Value = item.mat_descripcion;
                            oWsDetalleEx.Cells[2, sw].Value = item.existe;
                            oWsDetalleEx.Cells[3, sw].Value = item.mat_descripcion;
                        }

                        sw++;
                    }

                    //Formato Cabecera
                    oWsDetalleEx.SelectedRange[1, 1, 1, 6].AutoFilter = true;
                    oWsDetalleEx.Row(1).Height = 25;
                    oWsDetalleEx.Row(1).Style.Font.Bold = true;
                    oWsDetalleEx.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsDetalleEx.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsDetalleEx.Column(1).AutoFit();
                    oWsDetalleEx.Column(2).AutoFit();
                    oWsDetalleEx.Column(3).AutoFit();
                    oWsDetalleEx.Column(4).AutoFit();
                    oWsDetalleEx.Column(5).AutoFit();
                    oWsDetalleEx.Column(6).AutoFit();
                    oWsDetalleEx.Column(7).AutoFit();
                    oWsDetalleEx.Column(8).AutoFit();
                    oWsDetalleEx.Column(9).AutoFit();
                    oWsDetalleEx.Column(10).AutoFit();
                    oWsDetalleEx.Column(11).AutoFit();
                    oWsDetalleEx.Column(12).AutoFit();
                    oWsDetalleEx.View.FreezePanes(2, 1);


                    oEx.Save();
               #endregion
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        //Exprota datos suponer del grafico
        public JsonResult EncarteExcel  (int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Encarte_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
           //  ExhibicionDescarga

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Encarte >>

                List<E_Reporting_Encarte_Detalle> oEncarte = new E_Reporting_Encarte_Detalle().Lista_EncarteDetalle(
                   new Request_AjeReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       categoria = __c.ToString(),
                       empresa = __d.ToString(),
                       marca = __e.ToString(),
                       periodo = __f.ToString(),
                       zona = __g.ToString(),
                       distrito = __h.ToString(),
                       tienda = __i.ToString(),
                       segmento = __j.ToString(),
                       tipoactividad = __k.ToString()
                   });

                #endregion

                #endregion

                #region <<< Reporte Encarte >>>
                if ((oEncarte != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Encarte");
                    //oWsPrecio.Cells[1, 1].Value = "Ciudad";
                    //oWsPrecio.Cells[1, 2].Value = "Distrito";
                    oWsPrecio.Cells[1, 1].Value = "Cadena";
                    oWsPrecio.Cells[1, 2].Value = "Categoria";
                    oWsPrecio.Cells[1, 3].Value = "Fecha Inicio";
                    oWsPrecio.Cells[1, 4].Value = "Fecha Fin";
                    oWsPrecio.Cells[1, 5].Value = "Marca";
                    oWsPrecio.Cells[1, 6].Value = "Producto";
                    oWsPrecio.Cells[1, 7].Value = "Mecanica";
                    oWsPrecio.Cells[1, 8].Value = "Tipo de Anuncio";
                    //oWsPrecio.Cells[1, 11].Value = "Oficina";
                    oWsPrecio.Cells[1, 9].Value = "Empresa";
                    oWsPrecio.Cells[1, 10].Value = "Precio Oferta";
                    oWsPrecio.Cells[1, 11].Value = "Precio Normal";
                    oWsPrecio.Cells[1, 12].Value = "Comentarios Adicionales";

                    _fila = 2;
                    foreach (E_Reporting_Encarte_Detalle oBj in oEncarte)
                    {
                        //oWsPrecio.Cells[_fila, 1].Value = oBj.ciudad;
                        //oWsPrecio.Cells[_fila, 2].Value = oBj.distrito;
                        oWsPrecio.Cells[_fila, 1].Value = oBj.cadena;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.categoria;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.fecha_inico;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.fecha_fin;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.marca;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.producto;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.mecanica;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.tipo_anuncio;
                        //oWsPrecio.Cells[_fila, 11].Value = oBj.oficina;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.empresa;
                        oWsPrecio.Cells[_fila, 10].Value = oBj.precio_oferta;
                        oWsPrecio.Cells[_fila, 11].Value = oBj.precio_normal;
                        oWsPrecio.Cells[_fila, 12].Value = oBj.encarte_comentarios;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
       
        
        #endregion

        #region Incidencias
        [HttpGet]
        public ActionResult Inicidencia()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Inicidencia(string a, string b) {
            if (Convert.ToInt32(a) == 0)
            {
                #region Descarga incidencia
                string server = "";
                string path = "";
                int fila = 0;

                List<E_Reporting_Incidencia_Detalle> lIncidencia = MvcApplication._Deserialize<List<E_Reporting_Incidencia_Detalle>>(b);

                if (lIncidencia.Count == 0) return View
                    ();

                server = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                //path = System.IO.Path.Combine(RutaXplora, server);
                path = System.IO.Path.Combine(LocalTemp, server);

                FileInfo archivo =  new FileInfo(path);

                /*_fileServer = String.Format("Incidencia_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);*/

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(archivo))
                {
                    Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Incidencia");

                    oWs.Cells[1, 1].Value = "Fecha";
                    oWs.Cells[1, 2].Value = "Ciudad";
                    oWs.Cells[1, 3].Value = "Cadena";
                    oWs.Cells[1, 4].Value = "Tienda";
                    oWs.Cells[1, 5].Value = "Actividad";
                    oWs.Cells[1, 6].Value = "Supervisor";
                    oWs.Cells[1, 7].Value = "Tipo de incidencia";
                    oWs.Cells[1, 8].Value = "¿Se soluciono?";

                    fila = 2;
                    foreach (E_Reporting_Incidencia_Detalle oBj in lIncidencia)
                    {
                        oWs.Cells[fila, 1].Value = oBj.fecha;
                        oWs.Cells[fila, 2].Value = oBj.oficina;
                        oWs.Cells[fila, 3].Value = oBj.cadena;
                        oWs.Cells[fila, 4].Value = oBj.pdv_name;
                        oWs.Cells[fila, 5].Value = oBj.actividad;
                        oWs.Cells[fila, 6].Value = oBj.supervisor;
                        oWs.Cells[fila, 7].Value = oBj.tipoincidencia;
                        oWs.Cells[fila, 8].Value = oBj.solucionado;

                        fila++;
                    }

                    oWs.Column(1).AutoFit();
                    oWs.Column(2).AutoFit();
                    oWs.Column(3).AutoFit();
                    oWs.Column(4).AutoFit();
                    oWs.Column(5).AutoFit();
                    oWs.Column(6).AutoFit();
                    oWs.Column(7).AutoFit();
                    oWs.Column(8).AutoFit();

                    oEx.Save();
                }

                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"" + server + "\" }",
                    ContentType = "application/json"
                }.Content);
                #endregion
            }

            return View();
        }

        [HttpPost]
        public ActionResult JsonDataIncidencia(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Incidencia_Aje().Objeto_Incidencia(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 0,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        periodo = __c.ToString(),
                        zona = __d.ToString(),
                        perfil = __e.ToString(),
                        semana = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult JsonDetalleIncidencia(int __a, string __b, string __c, string __d, string __e, string __f, string __g)
        {
            List<E_Reporting_Incidencia_Detalle> oLs = new E_Reporting_Incidencia_Detalle().Lista_IncidenciaDetalle(
                    new Request_AjeReporting_Parametros()
                    {
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        periodo = __c.ToString(),
                        zona = __d.ToString(),
                        fecha_incidencia = __e.ToString(),
                        tipoactividad = __f.ToString(),
                        ciudad = __g.ToString()
                    });

            //return Json(new { Archivo = oLs });
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public JsonResult IncidenciaExcel(int __a, string __b, string __c, string __d)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Incidencia_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Incidencia >>

                List<E_Reporting_Incidencia_Detalle> oIncidencia = new E_Reporting_Incidencia_Detalle().Lista_IncidenciaDetalle(
                   new Request_AjeReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       periodo = __c.ToString(),
                       zona = __d.ToString(),
                       fecha_incidencia = "0",
                       tipoactividad = "0",
                       ciudad = "0"
                   });

                #endregion

                #endregion

                #region <<< Reporte Incidencia >>>
                if ((oIncidencia != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Incidencia");
                    oWsPrecio.Cells[1, 1].Value = "Fecha";
                    oWsPrecio.Cells[1, 2].Value = "Oficina";
                    oWsPrecio.Cells[1, 3].Value = "Cadena";
                    oWsPrecio.Cells[1, 4].Value = "PDV";
                    oWsPrecio.Cells[1, 5].Value = "Actividad";
                    oWsPrecio.Cells[1, 6].Value = "Tipo Incidencia";
                    oWsPrecio.Cells[1, 7].Value = "Solucionado";
                    oWsPrecio.Cells[1, 8].Value = "Supervisor";
                    oWsPrecio.Cells[1, 9].Value = "Gie";
                    oWsPrecio.Cells[1, 10].Value = "Cantidad";
                    oWsPrecio.Cells[1, 11].Value = "Detalles";
                    oWsPrecio.Cells[1, 12].Value = "Acciones";

                    _fila = 2;
                    foreach (E_Reporting_Incidencia_Detalle oBj in oIncidencia)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.fecha;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.oficina;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.cadena;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.pdv_name;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.actividad;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.tipoincidencia;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.solucionado;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.supervisor;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.gie;
                        oWsPrecio.Cells[_fila, 10].Value = oBj.cantidad;
                        oWsPrecio.Cells[_fila, 11].Value = oBj.detalle;
                        oWsPrecio.Cells[_fila, 12].Value = oBj.acciones;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
        #endregion

        #region SOS

        public ActionResult Sos()
        {
            return View();
        }

        [HttpPost]
        public ActionResult JsonDataSos(int op, int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Sos_Aje().Objeto_Sos(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = op,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        categoria = __c.ToString(),
                        empresa = __d.ToString(),
                        marca = __e.ToString(),
                        periodo = __f.ToString(),
                        zona = __g.ToString(),
                        tienda = __h.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult JsonDataSosRanking(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h)
        {
            E_Sos_Consulta_Brand oResper = new E_Sos_Consulta_Brand();
            oResper = new Sos_Aje().Consultar_Marca(new Request_Sos_Aje_Brand { Text = __e });
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Sos_Aje().Objeto_Sos(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 0,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        categoria = __c.ToString(),
                        empresa = __d.ToString(),
                        marca = oResper.Cod_Marca.ToString(), //__e.ToString(),
                        periodo = __f.ToString(),
                        zona = __g.ToString(),
                        tienda = __h.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult JsonDataSosEvoBrand(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Sos_Aje().Objeto_Sos_EvoBrand(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 1,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        categoria = __c.ToString(),
                        empresa = __d.ToString(),
                        marca = __e.ToString(),
                        periodo = __f.ToString(),
                        zona = __g.ToString(),
                        tienda = __h.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public JsonResult SosExcel(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Sos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Sos >>

                List<E_Sos_Detalle> oSos = new E_Sos_Detalle().Lista_SosDetalle(
                   new Request_AjeReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       categoria = __c.ToString(),
                       empresa = __d.ToString(),
                       marca = __e.ToString(),
                       periodo = __f.ToString(),
                       zona = __g.ToString(),
                       tienda = __h.ToString()
                   });

                #endregion

                #endregion

                #region <<< Reporte Sos >>>
                if ((oSos != null))
                {
                    Excel.ExcelWorksheet oWsSos = oEx.Workbook.Worksheets.Add("Reporte Sos");
                    oWsSos.Cells[1, 1].Value = "Gie";
                    oWsSos.Cells[1, 2].Value = "Cadena";
                    oWsSos.Cells[1, 3].Value = "Pdv";
                    oWsSos.Cells[1, 4].Value = "Categoria";
                    oWsSos.Cells[1, 5].Value = "Fecha Celular";
                    oWsSos.Cells[1, 6].Value = "Tipo Material";
                    oWsSos.Cells[1, 7].Value = "Marca";
                    oWsSos.Cells[1, 8].Value = "Cantidad";
                    oWsSos.Cells[1, 9].Value = "Total";
                    oWsSos.Cells[1, 10].Value = "Foto";
                    oWsSos.Cells[1, 11].Value = "Tipo Observacion";
                    oWsSos.Cells[1, 12].Value = "Medida";
                    oWsSos.Cells[1, 13].Value = "Observaciones";

                    _fila = 2;
                    foreach (E_Sos_Detalle oBj in oSos)
                    {
                        oWsSos.Cells[_fila, 1].Value = oBj.gie;
                        oWsSos.Cells[_fila, 2].Value = oBj.cadenas;
                        oWsSos.Cells[_fila, 3].Value = oBj.pdv;
                        oWsSos.Cells[_fila, 4].Value = oBj.categoria;
                        oWsSos.Cells[_fila, 5].Value = oBj.fecha_celular;
                        oWsSos.Cells[_fila, 6].Value = oBj.tipomaterial;
                        oWsSos.Cells[_fila, 7].Value = oBj.marca;
                        oWsSos.Cells[_fila, 8].Value = oBj.cantidad;
                        oWsSos.Cells[_fila, 9].Value = oBj.total;
                        oWsSos.Cells[_fila, 10].Value = oBj.foto;
                        oWsSos.Cells[_fila, 11].Value = oBj.tipoobservacion;
                        oWsSos.Cells[_fila, 12].Value = oBj.medida;
                        oWsSos.Cells[_fila, 13].Value = oBj.observacion;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsSos.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    oWsSos.Row(1).Height = 25;
                    oWsSos.Row(1).Style.Font.Bold = true;
                    oWsSos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsSos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsSos.Column(1).AutoFit();
                    oWsSos.Column(2).AutoFit();
                    oWsSos.Column(3).AutoFit();
                    oWsSos.Column(4).AutoFit();
                    oWsSos.Column(5).AutoFit();
                    oWsSos.Column(6).AutoFit();
                    oWsSos.Column(7).AutoFit();
                    oWsSos.Column(8).AutoFit();
                    oWsSos.Column(9).AutoFit();
                    oWsSos.Column(10).AutoFit();
                    oWsSos.Column(11).AutoFit();
                    oWsSos.Column(12).AutoFit();

                    oWsSos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public ActionResult SosCumplimiento(int __a,  string __c, string __d, string __e, string __f)
        {
            ViewBag.vbid_subcanal = __a;
            ViewBag.vbid_categoria = "0";
            ViewBag.vbid_marca = __c;
            ViewBag.vbid_periodo = __d;
            ViewBag.vbid_zona = __e;
            ViewBag.vbnom_pdv = __f;
            return View();
        }


        public ActionResult SosCumplimientoDetalle_copy(int __a, string __c, string __d, string __e, string __f)
        {
            ViewBag.vbid_subcanal = __a;
            ViewBag.vbid_categoria = "0";
            ViewBag.vbid_marca = __c;
            ViewBag.vbid_periodo = __d;
            ViewBag.vbid_zona = __e;
            ViewBag.vbnom_pdv = __f;
            return View();
        }

        public ActionResult SosCumplimientoDetalle(string __a, string __b, int __c, int __d,int __e,string __f)
        {
            //ViewBag.vbequipo = "55052016652016";
            //ViewBag.vbanio = __a;
            //ViewBag.vbmes = __b;
            //ViewBag.vbelemento = __c;
            //ViewBag.vbcategoria = __d;
            return View(new Sos_RpAASS_Cumplimiento_detalle_V2().Lista_SosCumplimiento_Detalle_v2(new Request_AjeReporting_Parametros()
            {
               subcanal=Convert.ToInt32(__a),
                cod_equipo = __b,
                anio = __c,
                mes = __d,
                elemento = __e,
                categoria = __f
                
            }));
        }

        public ActionResult SosCumplimiento2(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            ViewBag.vbid_subcanal = __a;
            ViewBag.vbid_categoria = __b;
            ViewBag.vbid_marca = __c;
            ViewBag.vbid_periodo = __d;
            ViewBag.vbid_zona = __e;
            ViewBag.vbnom_pdv = __f;
            return View();
        }

        public JsonResult SosCumplimientoExcel(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("SosCumplimiento_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Sos >>

                List<E_Sos_Detalle_Cumplimiento_Export> oSos = new E_Sos_Detalle_Cumplimiento_Export().Lista_SosCumplimiento(
                   new Request_AjeReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       categoria = __b.ToString(),
                       marca = __c.ToString(),
                       periodo = __d.ToString(),
                       zona = __e.ToString(),
                       tienda = __f.ToString()
                   });

                #endregion

                #endregion

                #region <<< Reporte Sos >>>
                if ((oSos != null))
                {
                    Excel.ExcelWorksheet oWsSos = oEx.Workbook.Worksheets.Add("Reporte Cumplimiento Sos");
                    oWsSos.Cells[1, 1].Value = "Cadena";
                    oWsSos.Cells[1, 2].Value = "Nombre Tienda";
                    oWsSos.Cells[1, 3].Value = "Tipo Elemento";
                    oWsSos.Cells[1, 4].Value = "Empresa";
                    oWsSos.Cells[1, 5].Value = "Ciudad";
                    oWsSos.Cells[1, 6].Value = "Marca";
                    oWsSos.Cells[1, 7].Value = "Cantidad";

                    _fila = 2;
                    foreach (E_Sos_Detalle_Cumplimiento_Export oBj in oSos)
                    {
                        oWsSos.Cells[_fila, 1].Value = oBj.cadena;
                        oWsSos.Cells[_fila, 2].Value = oBj.pdv;
                        oWsSos.Cells[_fila, 3].Value = oBj.tipomaterial;
                        oWsSos.Cells[_fila, 4].Value = oBj.empresa;
                        oWsSos.Cells[_fila, 5].Value = oBj.ciudad;
                        oWsSos.Cells[_fila, 6].Value = oBj.marca;
                        oWsSos.Cells[_fila, 7].Value = oBj.cantidad;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsSos.SelectedRange[1, 1, 1, 7].AutoFilter = true;
                    oWsSos.Row(1).Height = 25;
                    oWsSos.Row(1).Style.Font.Bold = true;
                    oWsSos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsSos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsSos.Column(1).AutoFit();
                    oWsSos.Column(2).AutoFit();
                    oWsSos.Column(3).AutoFit();
                    oWsSos.Column(4).AutoFit();
                    oWsSos.Column(5).AutoFit();
                    oWsSos.Column(6).AutoFit();
                    oWsSos.Column(7).AutoFit();

                    oWsSos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        //sosdetalle
        
        public JsonResult SosCumplimientoDetalleExcel( string __a,int __b, int __c, int __d, string __e  )
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Sos_Cumplimiento_Detalle" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Sos >>

                List<E_Sos_Detalle_Cumplimiento_Export> oSos = new E_Sos_Detalle_Cumplimiento_Export().Lista_SosCumplimiento_Exprotar_Detalle(
                   new Request_AjeReporting_Parametros() 
                   {
                       subcanal = Convert.ToInt32(__a),
                       cod_equipo = "55052016652016",
                       anio = Convert.ToInt32(__b),
                       mes = Convert.ToInt32(__c),
                       elemento = Convert.ToInt32(__d),
                       categoria = __e.ToString(),
                      
                   });

                #endregion

                #endregion

                #region <<< Reporte Sos >>>
                if ((oSos != null))
                {
                    Excel.ExcelWorksheet oWsSos = oEx.Workbook.Worksheets.Add("Reporte Cumplimiento Detalle Sos");
                    oWsSos.Cells[1, 1].Value = "Cadena";
                    oWsSos.Cells[1, 2].Value = "Punto de Venta";
                    oWsSos.Cells[1, 3].Value = "Ciudad";
                    oWsSos.Cells[1, 4].Value = "empresa";

                    oWsSos.Cells[1, 5].Value = "marca";
                    oWsSos.Cells[1, 6].Value = "cantidad";
                    oWsSos.Cells[1, 7].Value = "cantidad";
                   

                     
                    

                    _fila = 2;
                    foreach (E_Sos_Detalle_Cumplimiento_Export oBj in oSos)
                    {
                        oWsSos.Cells[_fila, 1].Value = oBj.cadena;
                        oWsSos.Cells[_fila, 2].Value = oBj.pdv;
                        oWsSos.Cells[_fila, 3].Value = oBj.ciudad;
                        oWsSos.Cells[_fila, 4].Value = oBj.empresa;

                        oWsSos.Cells[_fila, 5].Value = oBj.marca;
                        oWsSos.Cells[_fila, 6].Value = Convert.ToInt32(oBj.cantidad);
                        oWsSos.Cells[_fila, 7].Value = Convert.ToString(oBj.porcentaje) +" %";
                        

                        oWsSos.Row(4).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                       
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsSos.SelectedRange[1, 1, 1, 7].AutoFilter = true;
                    oWsSos.Row(1).Height = 25;
                    oWsSos.Row(1).Style.Font.Bold = true;
                    oWsSos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsSos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;



                    //Columnas auto ajustadas
                    oWsSos.Column(1).AutoFit();
                    oWsSos.Column(2).AutoFit();
                    oWsSos.Column(3).AutoFit();
                    oWsSos.Column(4).AutoFit();
                    oWsSos.Column(5).AutoFit();
                    oWsSos.Column(6).AutoFit();
                    oWsSos.Column(7).AutoFit();

                    oWsSos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        #endregion

        #region OOS

        public ActionResult Oos()
        {
            return View();
        }

        [HttpPost]
        public ActionResult JsonDataOos(int op, int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h)
        {
            E_Sos_Consulta_Brand oResper = new E_Sos_Consulta_Brand();
            oResper = new Sos_Aje().Consultar_Marca(new Request_Sos_Aje_Brand { Text = __h });
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Oos_Aje().Objeto_Oos(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = op,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        producto = __c.ToString(),
                        periodo = __d.ToString(),
                        zona = __e.ToString(),
                        tienda = __f.ToString(),
                        segmento = __g.ToString(),
                        marca = (__h == "Total" ? "0" : oResper.Cod_Marca.ToString())
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult JsonDataOosBrand(int op, int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h)
        {
            E_Sos_Consulta_Brand oResper = new E_Sos_Consulta_Brand();
            oResper = new Sos_Aje().Consultar_Marca(new Request_Sos_Aje_Brand { Text = __h });
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Oos_Aje().Objeto_Oos(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = op,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        producto = __c.ToString(),
                        periodo = __d.ToString(),
                        zona = __e.ToString(),
                        tienda = __f.ToString(),
                        segmento = __g.ToString(),
                        marca = (__h == "Total" ? "0" : oResper.Cod_Marca.ToString())
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }


        [HttpPost]
        public ActionResult JsonDataOosQuiebre(int __a, string __b, string __c, string __d, string __e, string __f, string __g)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Oos_Aje().Objeto_OOSDetalle(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 0,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        dias = __g.ToString().Trim(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult JsonDataOosRankingPdv(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Oos_Aje().Objeto_OOSDetRankingPdv(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 1,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult JsonDataOosRankingPdvmonto(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Oos_Aje().Objeto_OOSDetRankingPdvmonto(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 3,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult JsonDataOosRankingSku(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Oos_Aje().Objeto_OOSDetRankingSku(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 2,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult JsonDataOosRankingSkumonto(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Oos_Aje().Objeto_OOSDetRankingSkumonto(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 4,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public JsonResult OosExcel(int __a, string __b, string __c, string __d, string __e, string __f, string __g)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Oos >>

                List<E_Oos_Data_Export> oOos = new E_Oos_Data_Export().Lista_OosDataExport(
                   new Request_AjeReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       producto = __c.ToString(),
                       periodo = __d.ToString(),
                       zona = __e.ToString(),
                       tienda = __f.ToString(),
                       segmento = __g.ToString(),
                   });

                #endregion

                #endregion

                #region <<< Reporte Oos >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Oos");
                    oWsOos.Cells[1, 1].Value = "Fecha Celular";
                    oWsOos.Cells[1, 2].Value = "Ciudad";
                    oWsOos.Cells[1, 3].Value = "Cadena";
                    oWsOos.Cells[1, 4].Value = "ClientPDV_Code";
                    oWsOos.Cells[1, 5].Value = "Tienda";
                    oWsOos.Cells[1, 6].Value = "Categoria";
                    oWsOos.Cells[1, 7].Value = "Marca";
                    oWsOos.Cells[1, 8].Value = "Presentación";
                    oWsOos.Cells[1, 9].Value = "Sku";
                    oWsOos.Cells[1, 10].Value = "Producto";
                    oWsOos.Cells[1, 11].Value = "Quiebres";

                    _fila = 2;
                    foreach (E_Oos_Data_Export oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.fechacel;
                        oWsOos.Cells[_fila, 2].Value = oBj.ciudad;
                        oWsOos.Cells[_fila, 3].Value = oBj.cadena;
                        oWsOos.Cells[_fila, 4].Value = oBj.cod_pdv;
                        oWsOos.Cells[_fila, 5].Value = oBj.pdv;
                        oWsOos.Cells[_fila, 6].Value = oBj.categoria;
                        oWsOos.Cells[_fila, 7].Value = oBj.marca;
                        oWsOos.Cells[_fila, 8].Value = oBj.presentacion;
                        oWsOos.Cells[_fila, 9].Value = oBj.sku;
                        oWsOos.Cells[_fila, 10].Value = oBj.producto;
                        oWsOos.Cells[_fila, 11].Value = oBj.quiebres;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult OosExcelQuiebre(int __a, string __b, string __c, string __d, string __e, string __f , string __g)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle Quiebre >>

                List<Oos_Aje_Det_Quiebre> oOos = new Oos_Aje().Objeto_OOSDetalle(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 0,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        dias=__g.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle Quiebre >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det Quiebre");
                    oWsOos.Cells[1, 1].Value = "Cadena";
                    oWsOos.Cells[1, 2].Value = "Fecha";
                    oWsOos.Cells[1, 3].Value = "PDV";
                    oWsOos.Cells[1, 4].Value = "Categoria";
                    oWsOos.Cells[1, 5].Value = "Presentación";
                    oWsOos.Cells[1, 6].Value = "Descripcion SKU";
                    //oWsOos.Cells[1, 7].Value = "SKU";
                    _fila = 2;
                    foreach (Oos_Aje_Det_Quiebre oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.cadena;
                        oWsOos.Cells[_fila, 2].Value = oBj.fecha;
                        oWsOos.Cells[_fila, 3].Value = oBj.pdv;
                        oWsOos.Cells[_fila, 4].Value = oBj.categoria;
                        oWsOos.Cells[_fila, 5].Value = oBj.presentacion;
                        oWsOos.Cells[_fila, 6].Value = oBj.producto;
                        //oWsOos.Cells[_fila, 7].Value = oBj.sku;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 6].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult OosExcelRankingPDV(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle RankingPDV >>

                List<Oos_Aje_Det_RankingPdv> oOos = new Oos_Aje().Objeto_OOSDetRankingPdv(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 1,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle RankingPDV >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det RankingPDV");
                    oWsOos.Cells[1, 1].Value = "Ciudad";
                    oWsOos.Cells[1, 2].Value = "PDV";
                    oWsOos.Cells[1, 3].Value = "Ene";
                    oWsOos.Cells[1, 4].Value = "Feb";
                    oWsOos.Cells[1, 5].Value = "Mar";
                    oWsOos.Cells[1, 6].Value = "Abr";
                    oWsOos.Cells[1, 7].Value = "May";
                    oWsOos.Cells[1, 8].Value = "Jun";
                    oWsOos.Cells[1, 9].Value = "Jul";
                    oWsOos.Cells[1, 10].Value = "Ago";
                    oWsOos.Cells[1, 11].Value = "Sep";
                    oWsOos.Cells[1, 12].Value = "Oct";
                    oWsOos.Cells[1, 13].Value = "Nov";
                    oWsOos.Cells[1, 14].Value = "Dic";
                    oWsOos.Cells[1, 15].Value = "Total";

                    _fila = 2;
                    foreach (Oos_Aje_Det_RankingPdv oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.ciudad;
                        oWsOos.Cells[_fila, 2].Value = oBj.pdv;
                        oWsOos.Cells[_fila, 3].Value = oBj.mes_1;
                        oWsOos.Cells[_fila, 4].Value = oBj.mes_2;
                        oWsOos.Cells[_fila, 5].Value = oBj.mes_3;
                        oWsOos.Cells[_fila, 6].Value = oBj.mes_4;
                        oWsOos.Cells[_fila, 7].Value = oBj.mes_5;
                        oWsOos.Cells[_fila, 8].Value = oBj.mes_6;
                        oWsOos.Cells[_fila, 9].Value = oBj.mes_7;
                        oWsOos.Cells[_fila, 10].Value = oBj.mes_8;
                        oWsOos.Cells[_fila, 11].Value = oBj.mes_9;
                        oWsOos.Cells[_fila, 12].Value = oBj.mes_10;
                        oWsOos.Cells[_fila, 13].Value = oBj.mes_11;
                        oWsOos.Cells[_fila, 14].Value = oBj.mes_12;
                        oWsOos.Cells[_fila, 15].Value = oBj.mes_13;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 15].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.Column(12).AutoFit();
                    oWsOos.Column(13).AutoFit();
                    oWsOos.Column(14).AutoFit();
                    oWsOos.Column(15).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult OosExcelRankingPDVmonto(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle RankingPDV Montos>>

                List<Oos_Aje_Det_RankingPdvmonto> oOos = new Oos_Aje().Objeto_OOSDetRankingPdvmonto(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 3,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle RankingPDV Montos >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det RankingPDV Montos");
                    oWsOos.Cells[1, 1].Value = "Ciudad";
                    oWsOos.Cells[1, 2].Value = "PDV";
                    oWsOos.Cells[1, 3].Value = "Ene";
                    oWsOos.Cells[1, 4].Value = "Feb";
                    oWsOos.Cells[1, 5].Value = "Mar";
                    oWsOos.Cells[1, 6].Value = "Abr";
                    oWsOos.Cells[1, 7].Value = "May";
                    oWsOos.Cells[1, 8].Value = "Jun";
                    oWsOos.Cells[1, 9].Value = "Jul";
                    oWsOos.Cells[1, 10].Value = "Ago";
                    oWsOos.Cells[1, 11].Value = "Sep";
                    oWsOos.Cells[1, 12].Value = "Oct";
                    oWsOos.Cells[1, 13].Value = "Nov";
                    oWsOos.Cells[1, 14].Value = "Dic";
                    oWsOos.Cells[1, 15].Value = "Total";

                    _fila = 2;
                    foreach (Oos_Aje_Det_RankingPdvmonto oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.ciudad;
                        oWsOos.Cells[_fila, 2].Value = oBj.pdv;
                        oWsOos.Cells[_fila, 3].Value = oBj.mes_1;
                        oWsOos.Cells[_fila, 4].Value = oBj.mes_2;
                        oWsOos.Cells[_fila, 5].Value = oBj.mes_3;
                        oWsOos.Cells[_fila, 6].Value = oBj.mes_4;
                        oWsOos.Cells[_fila, 7].Value = oBj.mes_5;
                        oWsOos.Cells[_fila, 8].Value = oBj.mes_6;
                        oWsOos.Cells[_fila, 9].Value = oBj.mes_7;
                        oWsOos.Cells[_fila, 10].Value = oBj.mes_8;
                        oWsOos.Cells[_fila, 11].Value = oBj.mes_9;
                        oWsOos.Cells[_fila, 12].Value = oBj.mes_10;
                        oWsOos.Cells[_fila, 13].Value = oBj.mes_11;
                        oWsOos.Cells[_fila, 14].Value = oBj.mes_12;
                        oWsOos.Cells[_fila, 15].Value = oBj.mes_13;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 15].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.Column(12).AutoFit();
                    oWsOos.Column(13).AutoFit();
                    oWsOos.Column(14).AutoFit();
                    oWsOos.Column(15).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }


        public JsonResult OosExcelRankingSKU(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle RankingSku >>

                List<Oos_Aje_Det_RankingSku> oOos = new Oos_Aje().Objeto_OOSDetRankingSku(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 2,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle RankingSku >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det RankingSku");
                    oWsOos.Cells[1, 1].Value = "Presentación";
                    oWsOos.Cells[1, 2].Value = "Ene";
                    oWsOos.Cells[1, 3].Value = "Feb";
                    oWsOos.Cells[1, 4].Value = "Mar";
                    oWsOos.Cells[1, 5].Value = "Abr";
                    oWsOos.Cells[1, 6].Value = "May";
                    oWsOos.Cells[1, 7].Value = "Jun";
                    oWsOos.Cells[1, 8].Value = "Jul";
                    oWsOos.Cells[1, 9].Value = "Ago";
                    oWsOos.Cells[1, 10].Value = "Sep";
                    oWsOos.Cells[1, 11].Value = "Oct";
                    oWsOos.Cells[1, 12].Value = "Nov";
                    oWsOos.Cells[1, 13].Value = "Dic";
                    oWsOos.Cells[1, 14].Value = "Total";

                    _fila = 2;
                    foreach (Oos_Aje_Det_RankingSku oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.sku;
                        oWsOos.Cells[_fila, 2].Value = oBj.mes_1;
                        oWsOos.Cells[_fila, 3].Value = oBj.mes_2;
                        oWsOos.Cells[_fila, 4].Value = oBj.mes_3;
                        oWsOos.Cells[_fila, 5].Value = oBj.mes_4;
                        oWsOos.Cells[_fila, 6].Value = oBj.mes_5;
                        oWsOos.Cells[_fila, 7].Value = oBj.mes_6;
                        oWsOos.Cells[_fila, 8].Value = oBj.mes_7;
                        oWsOos.Cells[_fila, 9].Value = oBj.mes_8;
                        oWsOos.Cells[_fila, 10].Value = oBj.mes_9;
                        oWsOos.Cells[_fila, 11].Value = oBj.mes_10;
                        oWsOos.Cells[_fila, 12].Value = oBj.mes_11;
                        oWsOos.Cells[_fila, 13].Value = oBj.mes_12;
                        oWsOos.Cells[_fila, 14].Value = oBj.mes_13;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 14].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.Column(12).AutoFit();
                    oWsOos.Column(13).AutoFit();
                    oWsOos.Column(14).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult OosExcelRankingSKUmonto(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle RankingSku Montos >>

                List<Oos_Aje_Det_RankingSkumonto> oOos = new Oos_Aje().Objeto_OOSDetRankingSkumonto(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 4,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle RankingSku Montos >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det RankingSku Montos");
                    oWsOos.Cells[1, 1].Value = "Presentación";
                    oWsOos.Cells[1, 2].Value = "Ene";
                    oWsOos.Cells[1, 3].Value = "Feb";
                    oWsOos.Cells[1, 4].Value = "Mar";
                    oWsOos.Cells[1, 5].Value = "Abr";
                    oWsOos.Cells[1, 6].Value = "May";
                    oWsOos.Cells[1, 7].Value = "Jun";
                    oWsOos.Cells[1, 8].Value = "Jul";
                    oWsOos.Cells[1, 9].Value = "Ago";
                    oWsOos.Cells[1, 10].Value = "Sep";
                    oWsOos.Cells[1, 11].Value = "Oct";
                    oWsOos.Cells[1, 12].Value = "Nov";
                    oWsOos.Cells[1, 13].Value = "Dic";
                    oWsOos.Cells[1, 14].Value = "Total";

                    _fila = 2;
                    foreach (Oos_Aje_Det_RankingSkumonto oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.sku;
                        oWsOos.Cells[_fila, 2].Value = oBj.mes_1;
                        oWsOos.Cells[_fila, 3].Value = oBj.mes_2;
                        oWsOos.Cells[_fila, 4].Value = oBj.mes_3;
                        oWsOos.Cells[_fila, 5].Value = oBj.mes_4;
                        oWsOos.Cells[_fila, 6].Value = oBj.mes_5;
                        oWsOos.Cells[_fila, 7].Value = oBj.mes_6;
                        oWsOos.Cells[_fila, 8].Value = oBj.mes_7;
                        oWsOos.Cells[_fila, 9].Value = oBj.mes_8;
                        oWsOos.Cells[_fila, 10].Value = oBj.mes_9;
                        oWsOos.Cells[_fila, 11].Value = oBj.mes_10;
                        oWsOos.Cells[_fila, 12].Value = oBj.mes_11;
                        oWsOos.Cells[_fila, 13].Value = oBj.mes_12;
                        oWsOos.Cells[_fila, 14].Value = oBj.mes_13;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 14].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.Column(12).AutoFit();
                    oWsOos.Column(13).AutoFit();
                    oWsOos.Column(14).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }


        #endregion
        #endregion

        #region Exhibixion Adicional
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-07-24
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExhibicionAdicional()
        {
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-07-24
        /// </summary>
        /// <returns></returns>
        /// 
        //viernes 1
        public JsonResult Exportar_exhibiciones_2()
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Exhibicion_Adicional_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Precio >>

                List<AjeExhibicionAdicionalParticipacionMaterial> oPrecio = new AjeExhibicionAdicional().Excel_Exportar_exhibiciones_2(
                   new E_Parametros_Aje_AASS()
                   {
                       Cod_Equipo = "55052016652016",
                       Cod_SubCanal = "0",
                       Cod_Cadena = "0",
                       Cod_Categoria = "0",
                       Cod_Empresa = "1646",
                       Cod_Marca = "",
                       Periodo = 36125,
                       Cod_Zona = "0",
                       Cod_Distrito = "0",
                       Cod_PDV = "0",
                       segmento = "0",
                       Cod_Elemento = "0",
                       opcion = 0
                     });
                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Exhibicion Adicional");
                    oWsPrecio.Cells[1, 1].Value = "mat_codigo";
                    oWsPrecio.Cells[1, 2].Value = "mat_descripcion";
                    oWsPrecio.Cells[1, 3].Value = "mat_cantidad";
                    oWsPrecio.Cells[1, 4].Value = "Empresa";
                    oWsPrecio.Cells[1, 5].Value = "Presentacion";
                    _fila = 2;
                    foreach (AjeExhibicionAdicionalParticipacionMaterial oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.mat_codigo;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.mat_descripcion;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.mat_cantidad;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.mat_codigo;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.mat_codigo;

                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 1].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;


                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    //oWsPrecio.Column(6).AutoFit();
                    //oWsPrecio.Column(7).AutoFit();
                    //oWsPrecio.Column(8).AutoFit();
                    //oWsPrecio.Column(9).AutoFit();
                    //oWsPrecio.Column(10).AutoFit();
                    //oWsPrecio.Column(11).AutoFit();
                    //oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }


        [HttpPost]
        public ActionResult ExhibicionAdicional(string __a, string __b)
        {
            string[] StringParametro = __b.Split(',');

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AjeExhibicionAdicional().ExhibicionAdicional(new E_Parametros_Aje_AASS()
                {
                    Cod_Equipo = "55052016652016",
                    Cod_SubCanal = "0",
                    Cod_Empresa = Convert.ToString(StringParametro[0]),
                    Cod_Categoria = Convert.ToString(StringParametro[1]),
                    Cod_Marca = Convert.ToString(StringParametro[2]),
                    Periodo = Convert.ToInt32(StringParametro[3]),
                    Cod_Zona = Convert.ToString(StringParametro[4]),
                    Cod_Distrito = Convert.ToString(StringParametro[5]),
                    Cod_PDV = Convert.ToString(StringParametro[6]),
                    segmento = Convert.ToString(StringParametro[7]),
                    Cod_Cadena = Convert.ToString(StringParametro[8]),
                    Cod_Elemento = "0",
                    opcion = 0
                    
                })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult ExportarDetalle(string __a, string __b)
        {
            string[] StringParametro = __b.Split(',');

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AjeExhibicionAdicional().ExhibicionAdicional(new E_Parametros_Aje_AASS()
                {
                    Cod_Equipo = "55052016652016",
                    Cod_SubCanal = "0",
                    Cod_Empresa = Convert.ToString(StringParametro[0]),
                    Cod_Categoria = Convert.ToString(StringParametro[1]),
                    Cod_Marca = Convert.ToString(StringParametro[2]),
                    Periodo = Convert.ToInt32(StringParametro[3]),
                    Cod_Zona = Convert.ToString(StringParametro[4]),
                    Cod_Distrito = Convert.ToString(StringParametro[5]),
                    Cod_PDV = Convert.ToString(StringParametro[6]),
                    segmento = Convert.ToString(StringParametro[7]),
                    Cod_Cadena = Convert.ToString(StringParametro[8]),
                    Cod_Elemento = "0",
                    opcion = 0
                })),
                ContentType = "application/json"
            }.Content);


        }

      

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-08-02
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        /// //edwin ayala
        [HttpPost]
        public ActionResult ExhibicionDescarga(string __a)
        {
            int fila = 2;
            int columna = 2;
            string ruta = "";
            string archivo = "";

            if (__a.Length == 0) return View();

            List<AjeExhibicionAdicionalDetalle> lDetalle = MvcApplication._Deserialize<List<AjeExhibicionAdicionalDetalle>>(__a);

            archivo = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            ruta = System.IO.Path.Combine(LocalTemp, archivo);

            FileInfo archivoNuevo = new FileInfo(ruta);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(archivoNuevo))
            {
                Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Exhibicion");

                foreach (AjeExhibicionAdicionalDetalle oBj in lDetalle)
                {
                    columna = 2;
                    if (lDetalle.First() != oBj)
                    {
                        oWs.Cells[fila, 1].Value = oBj.pdv_descripcion;
                    }

                    foreach (AjeExhibicionAdicionalDetalleMaterial oMa in oBj.DetalleMaterial)
                    {
                        if (lDetalle.First() == oBj)
                        {
                            oWs.Cells[1, columna].Value = oMa.mat_descripcion;
                            oWs.Cells[2, columna].Value = Convert.ToInt32(oMa.existe);

                            oWs.Column(columna).Width = 15;
                            oWs.Row(1).Style.WrapText = true;
                        }
                        else
                        {
                            oWs.Cells[fila, columna].Value = Convert.ToInt32(oMa.existe);
                            oWs.Cells[fila, columna].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWs.Cells[fila, columna].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                        }

                        columna++;
                    }

                    fila++;
                }

                oWs.Column(1).AutoFit();
                oWs.Column(1).Style.Font.Bold = true;

                oWs.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                oWs.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                oWs.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                oWs.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                oEx.Save();
            }

            return Content(new ContentResult
            {
                Content = "{ \"_a\":\"/Temp/" + archivo + "\" }",
                ContentType = "application/json"
            }.Content);
        }

        //no vale
        //public ActionResult ExbAdicional()
        //{
        //    return View();
        //}
        //[HttpPost]
        //public ActionResult getEXhAdicional_AASS(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 1;
        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
        //    return Content(new ContentResult
        //    {
        //        Content = MvcApplication._Serialize(oObj),
        //        ContentType = "application/json"
        //    }.Content);
        //}
        ////cod_equipo: _a,cod_subcanal: _b,canal: _c,cod_categoria: _d,cod_empresa: _e,cod_marca: _f,periodo: _g,zona: _h,cod_distrito: _i,cod_pdv: _j,segmento: _k,cod_material: _l
        //[HttpPost]
        //public ActionResult GetGraficoParticipacionEXhAdicional(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 1;
        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
        //    return View(oObj);
        //}
        //[HttpPost]
        //public ActionResult GetGraficoParticipacionMarcaEXhAdicional(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 4;
        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
        //    return View("~/Views/Aje/GetGraficoParticipacionEXhAdicional.cshtml", oObj);
        //}

        //[HttpPost]
        //public ActionResult GetEXhAdicional_Tarifario(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 2;
        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_Tarifario_AASS(parametros);
        //    try
        //    {
        //        #region
        //        List<E_ExhAdicional_Material_AASS> oList_E_tabla = new List<E_ExhAdicional_Material_AASS>();

        //        var v_cabeceras = (from v_cabecera in oObj.ListTarifarioMaterial
        //                           orderby v_cabecera.DescMaterial ascending
        //                           select new
        //                           {
        //                               v_cabecera.CodMaterial,
        //                               v_cabecera.DescMaterial,
        //                               v_cabecera.TipoMaterial
        //                           }).Distinct();
        //        foreach (var v_cab in v_cabeceras)
        //        {
        //            E_ExhAdicional_Material_AASS objMat = new E_ExhAdicional_Material_AASS();
        //            List<E_ExhAdicional_Hijo> oList_Hijo = new List<E_ExhAdicional_Hijo>();
        //            objMat.CodMaterial = v_cab.CodMaterial;
        //            objMat.DescMaterial = v_cab.DescMaterial;
        //            objMat.TipoMaterial = v_cab.TipoMaterial;

        //            //List<E_ExhAdicional_Material_AASS> oE_tabla = new List<E_ExhAdicional_Material_AASS>();

        //            var v_detalles = (from v_detalle in oObj.ListTarifarioMaterial
        //                              where v_detalle.CodMaterial == v_cab.CodMaterial
        //                              select new
        //                              {
        //                                  v_detalle.cod_cadena,
        //                                  v_detalle.valor
        //                              }).Distinct();

        //            foreach (var v_det2 in v_detalles)
        //            {
        //                E_ExhAdicional_Hijo oHijo = new E_ExhAdicional_Hijo();
        //                oHijo.Valor = v_det2.valor;
        //                oHijo.Id = v_det2.cod_cadena;

        //                oList_Hijo.Add(oHijo);
        //            }
        //            objMat.Hijo = oList_Hijo;
        //            //oList_E_tabla.Hijo = oList_Hijo;
        //            oList_E_tabla.Add(objMat);
        //        }
        //        #endregion
        //        oObj.ListTarifarioMaterial = oList_E_tabla;
        //    }
        //    catch
        //    {
        //        oObj = null;
        //    }
        //    return View(oObj);
        //}
        //[HttpPost]
        //public ActionResult GetEXhAdicional_Detalle(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 2;

        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_Detalle_AASS(parametros);
        //    try
        //    {
        //        #region

        //        int totalCab = oObj.ListTarifarioMaterial.Count();
        //        oObj.ListTarifarioMaterial = oObj.ListTarifarioMaterial.OrderBy(o => o.CodMaterial).ToList();

        //        List<E_ExhAdicional_Detalle_AASS> oList_E_tabla = new List<E_ExhAdicional_Detalle_AASS>();

        //        var v_cabeceras = (from v_cabecera in oObj.ListExhAdDetalle
        //                           orderby v_cabecera.PDV_Name ascending
        //                           select new
        //                           {
        //                               v_cabecera.Cod_PDV,
        //                               v_cabecera.PDV_Name
        //                           }).Distinct();

        //        foreach (var v_cab in v_cabeceras)
        //        {
        //            E_ExhAdicional_Detalle_AASS objMat = new E_ExhAdicional_Detalle_AASS();
        //            List<E_ExhAdicional_Hijo> oList_Hijo = new List<E_ExhAdicional_Hijo>();
        //            objMat.Cod_PDV = v_cab.Cod_PDV;
        //            objMat.PDV_Name = v_cab.PDV_Name;

        //            //List<E_ExhAdicional_Material_AASS> oE_tabla = new List<E_ExhAdicional_Material_AASS>();

        //            var v_detalles = (from v_detalle in oObj.ListExhAdDetalle
        //                              where v_detalle.Cod_PDV == v_cab.Cod_PDV
        //                              select new
        //                              {
        //                                  v_detalle.Cod_Material,
        //                                  v_detalle.Desc_Material,
        //                                  v_detalle.valor,
        //                                  v_detalle.Foto
        //                              }).Distinct();

        //            foreach (var v_det in v_detalles)
        //            {
        //                E_ExhAdicional_Hijo oHijo = new E_ExhAdicional_Hijo();
        //                oHijo.Id = v_det.Cod_Material;
        //                oHijo.Valor = v_det.valor;
        //                oHijo.Foto = v_det.Foto;

        //                oList_Hijo.Add(oHijo);
        //            }
        //            /////////////////////////////////////////////////////
        //            //Completar de materiales que no esten en la lista
        //            //int filaHijo = oList_Hijo.Count();
        //            int filas = totalCab - oList_Hijo.Count();//cantidad que falta
        //            if (filas < totalCab)
        //            {

        //                List<E_ExhAdicional_Material_AASS> listMaterial = new List<E_ExhAdicional_Material_AASS>();

        //                foreach (E_ExhAdicional_Material_AASS material in oObj.ListTarifarioMaterial)
        //                {
        //                    var result = (from t in oList_Hijo
        //                                  where t.Id == material.CodMaterial
        //                                  select t).Count();
        //                    if (result == 0)
        //                    {
        //                        E_ExhAdicional_Hijo oHijo = new E_ExhAdicional_Hijo();
        //                        oHijo.Id = material.CodMaterial;
        //                        oHijo.Valor = " - ";
        //                        oList_Hijo.Add(oHijo);
        //                    }
        //                }
        //                //for (int i = 0; i < filas; i++)
        //                //{
        //                //    E_ExhAdicional_Hijo oHijo = new E_ExhAdicional_Hijo();
        //                //    oHijo.Id = "0";
        //                //    oHijo.Valor = " - ";
        //                //    oList_Hijo.Add(oHijo);
        //                //}
        //            }
        //            /////////////////////////////////////////////////////
        //            List<E_ExhAdicional_Hijo> hijos = oList_Hijo.OrderBy(o => o.Id).ToList();//Ordenando
        //            objMat.Hijo = hijos;
        //            //objMat.Hijo = oList_Hijo;

        //            oList_E_tabla.Add(objMat);
        //        }
        //        #endregion
        //        oObj.ListExhAdDetalle = oList_E_tabla;
        //    }
        //    catch
        //    {
        //        oObj = null;
        //    }
        //    return View(oObj);
        //}
        //[HttpPost]
        //public ActionResult GetEXhAdicional_Evolutivo(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l, string _m)
        //{
        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 3;

        //    ViewBag.Exhibicion = _m;

        //    List<E_ExhAdicional_Evolutivo_AASS> oObj = mExhAd.ExhAdicional_Evolutivo_AASS(parametros);
        //    return View(oObj);
        //}
        //public JsonResult ExhibicionExcel(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{
        //    string _fileServer = "";
        //    string _filePath = "";

        //    int _fila = 0;

        //    _fileServer = String.Format("Exhibicion_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
        //    _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

        //    FileInfo _fileNew = new FileInfo(_filePath);

        //    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
        //    {
        //        #region <<< Instancias >>>
        //        #region << Reporte Exhibicion >>
        //        List<E_Excel_Exhibicion_AASS> oExhibicion = new M_ExhAdicional_Aje_AASS().Excel_Exhibicion(
        //           new E_Parametros_Aje_AASS()
        //           {
        //               Cod_Equipo = _a,
        //               Cod_SubCanal = _b,
        //               Cod_Cadena = _c,
        //               Cod_Categoria = _d,
        //               Cod_Empresa = _e,
        //               Cod_Marca = _f,
        //               Periodo = _g,
        //               Cod_Zona = _h,
        //               Cod_Distrito = _i,
        //               Cod_PDV = _j,
        //               segmento = _k,
        //               Cod_Elemento = _l,
        //               opcion = 0
        //           }); 

        //        #endregion

        //        #endregion

        //        #region <<< Reporte Exhibicion >>>
        //        if ((oExhibicion != null))
        //        {
        //            Excel.ExcelWorksheet oWsExhibicion = oEx.Workbook.Worksheets.Add("Reporte Exhibicion");
        //            oWsExhibicion.Cells[1, 1].Value = "Cadena";
        //            oWsExhibicion.Cells[1, 2].Value = "Codigo PDV";
        //            oWsExhibicion.Cells[1, 3].Value = "Nombre PDV";
        //            oWsExhibicion.Cells[1, 4].Value = "Empresa";
        //            oWsExhibicion.Cells[1, 5].Value = "Categoria";
        //            oWsExhibicion.Cells[1, 6].Value = "Marca";
        //            oWsExhibicion.Cells[1, 7].Value = "Material";
        //            oWsExhibicion.Cells[1, 8].Value = "Precio";
        //            oWsExhibicion.Cells[1, 9].Value = "Periodo";

        //            _fila = 2;
        //            foreach (E_Excel_Exhibicion_AASS oBj in oExhibicion)
        //            {
        //                oWsExhibicion.Cells[_fila, 1].Value = oBj.Cadena;
        //                oWsExhibicion.Cells[_fila, 2].Value = oBj.Cod_PDV;
        //                oWsExhibicion.Cells[_fila, 3].Value = oBj.PDV;
        //                oWsExhibicion.Cells[_fila, 4].Value = oBj.Empresa;
        //                oWsExhibicion.Cells[_fila, 5].Value = oBj.Categoria;
        //                oWsExhibicion.Cells[_fila, 6].Value = oBj.Marca;
        //                oWsExhibicion.Cells[_fila, 7].Value = oBj.Material;
        //                oWsExhibicion.Cells[_fila, 8].Value = oBj.P_Regular;
        //                oWsExhibicion.Cells[_fila, 9].Value = oBj.Periodo;
        //                _fila++;
        //            }

        //            //Formato Cabecera
        //            oWsExhibicion.SelectedRange[1, 1, 1, 9].AutoFilter = true;
        //            oWsExhibicion.Row(1).Height = 25;
        //            oWsExhibicion.Row(1).Style.Font.Bold = true;
        //            oWsExhibicion.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
        //            oWsExhibicion.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

        //            //Columnas auto ajustadas
        //            oWsExhibicion.Column(1).AutoFit();
        //            oWsExhibicion.Column(2).AutoFit();
        //            oWsExhibicion.Column(3).AutoFit();
        //            oWsExhibicion.Column(4).AutoFit();
        //            oWsExhibicion.Column(5).AutoFit();
        //            oWsExhibicion.Column(6).AutoFit();
        //            oWsExhibicion.Column(7).AutoFit();
        //            oWsExhibicion.Column(8).AutoFit();
        //            oWsExhibicion.Column(9).AutoFit();

        //            oWsExhibicion.View.FreezePanes(2, 1);


        //            oEx.Save();
        //        }
        //        else
        //        {
        //            _fileServer = "0";
        //        }
        //        #endregion
        //    }
        //    return Json(new { Archivo = _fileServer });
        //}
        #endregion

        #region << Dashboard >>
        [HttpPost]
        public ActionResult DashGrafEXhAdi_Valorizado(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        {

            //M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
            //E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
            //parametros.Cod_Equipo = _a;
            //parametros.Cod_SubCanal = _b;
            //parametros.Cod_Cadena = _c;
            //parametros.Cod_Categoria = _d;
            //parametros.Cod_Empresa = _e;
            //parametros.Cod_Marca = _f;
            //parametros.Periodo = _g;
            //parametros.Cod_Zona = _h;
            //parametros.Cod_Distrito = _i;
            //parametros.Cod_PDV = _j;
            //parametros.segmento = _k;
            //parametros.Cod_Elemento = _l;
            //parametros.opcion = 1;
            //E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
            E_ExhAdicional_AASS oObj = new E_ExhAdicional_AASS();
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_Valorizado.cshtml", oObj);
        }

        [HttpPost]
        public ActionResult DashGrafEXhAdi_Incidencias(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        {

            //M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
            //E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
            //parametros.Cod_Equipo = _a;
            //parametros.Cod_SubCanal = _b;
            //parametros.Cod_Cadena = _c;
            //parametros.Cod_Categoria = _d;
            //parametros.Cod_Empresa = _e;
            //parametros.Cod_Marca = _f;
            //parametros.Periodo = _g;
            //parametros.Cod_Zona = _h;
            //parametros.Cod_Distrito = _i;
            //parametros.Cod_PDV = _j;
            //parametros.segmento = _k;
            //parametros.Cod_Elemento = _l;
            //parametros.opcion = 1;
            //E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
            E_ExhAdicional_AASS oObj = new E_ExhAdicional_AASS();
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_Incidencias.cshtml", oObj);
        }

        [HttpPost]
        public ActionResult DashGrafEXhAdi_SOP(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        {

            //M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
            //E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
            //parametros.Cod_Equipo = _a;
            //parametros.Cod_SubCanal = _b;
            //parametros.Cod_Cadena = _c;
            //parametros.Cod_Categoria = _d;
            //parametros.Cod_Empresa = _e;
            //parametros.Cod_Marca = _f;
            //parametros.Periodo = _g;
            //parametros.Cod_Zona = _h;
            //parametros.Cod_Distrito = _i;
            //parametros.Cod_PDV = _j;
            //parametros.segmento = _k;
            //parametros.Cod_Elemento = _l;
            //parametros.opcion = 1;
            //E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
            E_ExhAdicional_AASS oObj = new E_ExhAdicional_AASS();
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_SOP.cshtml", oObj);
        }

        [HttpPost]
        public ActionResult DashGrafEXhAdi_AlcanceVtas(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        {

            //M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
            //E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
            //parametros.Cod_Equipo = _a;
            //parametros.Cod_SubCanal = _b;
            //parametros.Cod_Cadena = _c;
            //parametros.Cod_Categoria = _d;
            //parametros.Cod_Empresa = _e;
            //parametros.Cod_Marca = _f;
            //parametros.Periodo = _g;
            //parametros.Cod_Zona = _h;
            //parametros.Cod_Distrito = _i;
            //parametros.Cod_PDV = _j;
            //parametros.segmento = _k;
            //parametros.Cod_Elemento = _l;
            //parametros.opcion = 1;
            //E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
            E_ExhAdicional_AASS oObj = new E_ExhAdicional_AASS();
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_AlcanceVtas.cshtml", oObj);
        }

        [HttpPost]
        public ActionResult DashGrafEXhAdi_VtasCategoria(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        {
            //M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
            //E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
            //parametros.Cod_Equipo = _a;
            //parametros.Cod_SubCanal = _b;
            //parametros.Cod_Cadena = _c;
            //parametros.Cod_Categoria = _d;
            //parametros.Cod_Empresa = _e;
            //parametros.Cod_Marca = _f;
            //parametros.Periodo = _g;
            //parametros.Cod_Zona = _h;
            //parametros.Cod_Distrito = _i;
            //parametros.Cod_PDV = _j;
            //parametros.segmento = _k;
            //parametros.Cod_Elemento = _l;
            //parametros.opcion = 1;
            //E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
            E_ExhAdicional_AASS oObj = new E_ExhAdicional_AASS();
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_VtasCategoria.cshtml", oObj);
        }

        [HttpPost]
        public ActionResult DashGrafEXhAdi_IndexPrice(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        {
            //M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
            //E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
            //parametros.Cod_Equipo = _a;
            //parametros.Cod_SubCanal = _b;
            //parametros.Cod_Cadena = _c;
            //parametros.Cod_Categoria = _d;
            //parametros.Cod_Empresa = _e;
            //parametros.Cod_Marca = _f;
            //parametros.Periodo = _g;
            //parametros.Cod_Zona = _h;
            //parametros.Cod_Distrito = _i;
            //parametros.Cod_PDV = _j;
            //parametros.segmento = _k;
            //parametros.Cod_Elemento = _l;
            //parametros.opcion = 1;
            //E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
            E_ExhAdicional_AASS oObj = new E_ExhAdicional_AASS();
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_IndexPrice.cshtml", oObj);
        }

        [HttpPost]
        public ActionResult DashGrafEXhAdi_ElementVisibilidad(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        {
            //M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
            //E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
            //parametros.Cod_Equipo = _a;
            //parametros.Cod_SubCanal = _b;
            //parametros.Cod_Cadena = _c;
            //parametros.Cod_Categoria = _d;
            //parametros.Cod_Empresa = _e;
            //parametros.Cod_Marca = _f;
            //parametros.Periodo = _g;
            //parametros.Cod_Zona = _h;
            //parametros.Cod_Distrito = _i;
            //parametros.Cod_PDV = _j;
            //parametros.segmento = _k;
            //parametros.Cod_Elemento = _l;
            //parametros.opcion = 1;
            //E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
            E_ExhAdicional_AASS oObj = new E_ExhAdicional_AASS();
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_ElementVisibilidad.cshtml", oObj);
        }

        #endregion

        #region << Dashboard Tradicional >>
        [HttpPost]
        public ActionResult FiltroTrad(String parametro, int oop)
        {
            //return View(new NwRepPrecioAASS().Filtro(new Request_NWRepStd_General { Parametros = parametro, op = oop }));
            return Json(new { response = new NwRepPrecioAASS().Filtro(new Request_NWRepStd_General { Parametros = parametro, op = oop }) });
        }
        [HttpPost]
        public ActionResult DashGrafEXhAdi_ElementVisibilidad_Trad(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        {
            M_Dashboard_Aje_Service mDash = new M_Dashboard_Aje_Service();
            Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS parametros = new Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS();
            parametros.Cod_Equipo = _a;
            parametros.Cod_SubCanal = _b;
            parametros.Cod_Cadena = _c;
            parametros.Cod_Categoria = _d;
            parametros.Cod_Empresa = _e;
            parametros.Cod_Marca = _f;
            parametros.Periodo = _g;
            parametros.Cod_Zona = _h;
            parametros.Cod_Distrito = _i;
            parametros.Cod_PDV = _j;
            parametros.segmento = _k;
            parametros.Cod_Elemento = _l;
            parametros.opcion = 1;
            List<E_Participacion_Dash> oObj = mDash.Participacion(parametros);
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_ElementVisibilidad_Trad.cshtml", oObj);
        }
        //[HttpPost]
        //public ActionResult DashGrafEXhAdi_ElementVisibilidad_TradElement(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{
        //    M_Dashboard_Aje_Service mDash = new M_Dashboard_Aje_Service();
        //    Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS parametros = new Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 2;
        //    List<E_Participacion_Dash> oObj = mDash.Participacion(parametros);
        //    return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_ElementVisibilidad_TradElement.cshtml", oObj);
        //}
        [HttpPost]//Metodo que obtiene desde la vista los datos para el segundo grafico de Visibilidad(Treemap)
        public ActionResult DashGrafEXhAdi_ElementVisibilidad_TradElement(string _a)
        {
            List<E_Participacion_Dash> oObj = MvcApplication._Deserialize<List<E_Participacion_Dash>>(_a);
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_ElementVisibilidad_TradElement.cshtml", oObj);
        }
        [HttpPost]//Metodo que trae todas los elementos de visibilidad de todas las empresas y se guarda en json en la vista.
        public ActionResult DashGrafEXhAdi_ElementVisibilidad_TradElements(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        {
            M_Dashboard_Aje_Service mDash = new M_Dashboard_Aje_Service();
            Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS parametros = new Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS();
            parametros.Cod_Equipo = _a;
            parametros.Cod_SubCanal = _b;
            parametros.Cod_Cadena = _c;
            parametros.Cod_Categoria = _d;
            parametros.Cod_Empresa = _e;
            parametros.Cod_Marca = _f;
            parametros.Periodo = _g;
            parametros.Cod_Zona = _h;
            parametros.Cod_Distrito = _i;
            parametros.Cod_PDV = _j;
            parametros.segmento = _k;
            parametros.Cod_Elemento = _l;
            parametros.opcion = 2;
            List<E_Participacion_Dash> oObj = mDash.Participacion(parametros);
            //return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_ElementVisibilidad_TradElement.cshtml", oObj);
            return Json(oObj);
        }
        [HttpPost]
        public ActionResult DashGrafEXhAdi_IndexPrice_Trad(string _a, string _b, string _c, string _d,string _e,int _f)
        {
            M_Dashboard_Aje_Service mDash = new M_Dashboard_Aje_Service();
            Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS parametros = new Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS();
            parametros.Cod_Equipo = _a;
            parametros.Anio = _b;
            parametros.Mes = _c;
            parametros.Cod_Categoria = _d;
            parametros.Cod_Empresa = _e;
            parametros.opcion = _f;
            List<E_IndexPrice_Dash> oObj = mDash.IndexPrice(parametros);
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_IndexPrice_Trad.cshtml", oObj);
        }
        [HttpPost]
        public ActionResult DashGrafEXhAdi_IndexPriceMarca_Trad(string _a, string _b, string _c, string _d, string _e, int _f)
        {
            M_Dashboard_Aje_Service mDash = new M_Dashboard_Aje_Service();
            Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS parametros = new Lucky.Xplora.Models.Aje.E_Parametros_Aje_AASS();
            parametros.Cod_Equipo = _a;
            parametros.Anio = _b;
            parametros.Mes = _c;
            parametros.Cod_Categoria = _d;
            parametros.Cod_Empresa = _e;
            parametros.opcion = _f;
            List<E_IndexPrice_Dash> oObj = mDash.IndexPrice(parametros);
            return View("~/Views/Aje/Dashboard/DashGrafEXhAdi_IndexPriceMarca_Trad.cshtml", oObj);
        }

        public ActionResult DashGrafCumplimiento_Trad(string _a, int _b, int _c, string _d, int _e, int _f)
        {
            return View(new NwRepIPPFTrad().Dash_CUMPLIMIENTO(new Request_NWRepStd_Tradicional
            {
                Equipo = _a,
                Anio = _b,
                Mes = _c,
                Persona = ((Persona)Session["Session_Login"]).Person_id,
                Categoria = _d,
                Marca = _e,
                Region = _f
            }));
        }
        #endregion

        #region << AJE AASS >>

        public ActionResult Visibilidad() {
            ViewBag.persona = ((Persona)Session["Session_Login"]).Person_id;
            return View();
        }
        public ActionResult CnsVisibilidad(string _a,string _b,string _c,string _d,string _e,int _f,string _g) {
            return Json(new { response = new NwRepVisibilidadAASS().Consulta_Visibilidad(new Request_NWRepStd_General { 
                subcanal = _a,cadena=_b,categoria=_c,empresa=_d,marca=_e,periodo = _f ,zona= _g
            }) });
        }
        public ActionResult CnsVisibilidadv3(string _a, string _b, string _c, string _d, string _e, int _f, string _g,string _h)
        {
            return Json(new { response = new NwRepVisibilidadAASS().Consulta_Visibilidadv3(new Request_NWRepStd_General { 
            subcanal = _a,cadena=_b,categoria=_c,empresa=_d,marca=_e,periodo = _f,zona=_g,tienda=_h
            }) });
        }
        public ActionResult CnsEfectividad(string _a, string _b, string _c, string _d, string _e, int _f, string _g)
        {
            return Json(new { response = new NwRepVisibilidadAASS().Consulta_Efectividad(new Request_NWRepStd_General { 
                subcanal = _a,cadena=_b,categoria=_c,empresa=_d,marca=_e,periodo = _f ,zona=_g            
            }) });
        }
        public ActionResult CnsFiltro(string _a, int _b)
        {
            return Json(new { response = new NwRepVisibilidadAASS().Filtros(new Request_NWRepStd_General { Parametros = _a,op = _b }) });
        }
        public JsonResult VisibilidadExcel(string _a, string _b, string _c, string _d, string _e, int _f, string _g)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Visibilidad_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Visibi >>

                List<E_Visibilidad_AJE_AASS_Excel> oPrecio = new NwRepVisibilidadAASS().Descarga(
                   new Request_NWRepStd_General()
                   {
                       subcanal = _a,
                       cadena = _b,
                       categoria = _c,
                       empresa = _d,
                       marca = _e,
                       periodo = _f,
                       zona = _g
                   });

                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Visibilidad");
                    oWsPrecio.Cells[1, 1].Value = "Cedis";
                    oWsPrecio.Cells[1, 2].Value = "Cod_PDV";
                    oWsPrecio.Cells[1, 3].Value = "Nombre PDV";
                    oWsPrecio.Cells[1, 4].Value = "Empresa";
                    oWsPrecio.Cells[1, 5].Value = "Categoria";
                    oWsPrecio.Cells[1, 6].Value = "Marca";
                    oWsPrecio.Cells[1, 7].Value = "Material";
                    oWsPrecio.Cells[1, 8].Value = "Cantidad";
                    oWsPrecio.Cells[1, 9].Value = "Fecha";

                    _fila = 2;
                    foreach (E_Visibilidad_AJE_AASS_Excel oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.Cedis;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.Cod_PDV;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.Nom_PDV;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.Empresa;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.Categoria;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.Marca ;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.Material;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.Cantidad;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.Fecha;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public ActionResult DashboardAASS() {
            ViewBag.persona = ((Persona)Session["Session_Login"]).Person_id;
            return View();
        }

        [HttpPost]
        public ActionResult DashAASSOOS(int _a, string _b, string _c, string _d, string _e, string _f, string _g, string _h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new M_Dashboard_Aje_Service().IndexStockOut(
                    new Request_AjeDasgAASS_Parametros()
                    {
                        subcanal = _a,
                        cadena = _b,
                        categoria = _c,
                        empresa = _d,
                        anio = _e,
                        mes = _f,
                        id_zona = _g,
                        segmento = _h
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult DashAASSExhAdicional(int _a, string _b, string _c, string _d, string _e, string _f, string _g, string _h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new M_Dashboard_Aje_Service().IndexExhAdicional(
                    new Request_AjeDasgAASS_Parametros()
                    {
                        subcanal = _a,
                        cadena = _b,
                        categoria = _c,
                        empresa = _d,
                        anio = _e,
                        mes = _f,
                        id_zona = _g,
                        segmento = _h
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult DashAASSIncidencia(int _a, string _b, string _c, string _d, string _e, string _f, string _g, string _h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new M_Dashboard_Aje_Service().IndexIncidencias(
                    new Request_AjeDasgAASS_Parametros()
                    {
                        subcanal = _a,
                        cadena = _b,
                        categoria = _c,
                        empresa = _d,
                        anio = _e,
                        mes = _f,
                        id_zona = _g,
                        segmento = _h
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult DashAASSVentas(int _a, string _b, string _c, string _d, string _e, string _f, string _g, string _h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new M_Dashboard_Aje_Service().IndexAlcanceVenta(
                    new Request_AjeDasgAASS_Parametros()
                    {
                        subcanal = _a,
                        cadena = _b,
                        categoria = _c,
                        empresa = _d,
                        anio = _e,
                        mes = _f,
                        id_zona = _g,
                        segmento = _h
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult DashAASSPrice(string _a,string _b,string _c,string _d,int _e,int _f,string _g,string _h) {
            return Json(new
            {
                response = new M_Dashboard_Aje().IndexPrice(new Request_NWRepStd_General
                {
                    subcanal = _a
                    ,cadena= _b
                    ,categoria= _c
                    ,empresa = _d
                    ,anio = _e
                    ,mes= _f
                    ,zona = _g
                    ,segmento = _h
                })
            });
        }
        public ActionResult DashAASSVisibilidad(string _a, string _b, string _c, string _d, int _e, int _f, string _g, string _h, int _i)
        {
            return Json(new
            {
                response = new M_Dashboard_Aje().Visibilidad(new Request_NWRepStd_General
                {
                    subcanal = _a,
                    cadena = _b,
                    categoria = _c,
                    empresa = _d,
                    anio = _e,
                    mes = _f,
                    zona = _g,
                    segmento = _h,
                    idtipo_material = _i
                })
            });
        }
        public ActionResult DashFiltro(string _a,int _b) {
            return Json(new { response = new M_Dashboard_Aje().Filtros(new Request_NWRepStd_General { Parametros = _a, op = _b }) } );
        }
        public ActionResult DashVentasCatg(string _a, string _b, string _c, string _d, int _e, int _f, string _g, string _h)
        {
            return Json(new
            {
                response = new M_Dashboard_Aje().Ventas_x_Categoria(new Request_NWRepStd_General
                {
                    subcanal = _a,
                    cadena = _b,
                    categoria = _c,
                    empresa = _d,
                    anio = _e,
                    mes = _f,
                    zona = _g,
                    segmento = _h
                })
            });
        }
        public ActionResult DashSOP(string _a, string _b, string _c, string _d, int _e, int _f, string _g, string _h)
        {
            return Json(new
            {
                response = new M_Dashboard_Aje().SOP(new Request_NWRepStd_General
                {
                    subcanal = _a,
                    cadena = _b,
                    categoria = _c,
                    empresa = _d,
                    anio = _e,
                    mes = _f,
                    zona = _g,
                    segmento = _h
                })
            });
        }

        public JsonResult VisibilidadExcel2(String __a, String __b, String __c, String __d, String __e, int __f, String __g)
        {
            string _fileServer = "";
            string _filePath = "";

            _fileServer = String.Format("Visivilidad_Efec" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Visibilidad >>

                E_Visibilidad_AJE_AASS oVisibilidad = new NwRepVisibilidadAASS().Consulta_Visibilidad(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena=__b,
                    categoria=__c,
                    empresa=__d,
                    marca=__e,
                    periodo = __f ,
                    zona= __g,
                    Persona = ((Persona)Session["Session_Login"]).Person_id
                });

                #endregion

                #endregion

                #region <<< Reporte Visibilidad >>>
                if ((oVisibilidad != null))
                {
                    Excel.ExcelWorksheet oWsVisibilidad = oEx.Workbook.Worksheets.Add("Reporte Visibilidad");

                    oWsVisibilidad.Cells[1, 1].Value = "PDV con Elemento";

                    //dando borde
                    oWsVisibilidad.Cells[1, 1].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidad.Cells[1, 1].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidad.Cells[1, 1].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidad.Cells[1, 1].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                    //dando ancho a columnas
                    oWsVisibilidad.Column(1).Width = 22;

                    int colcab = 2;
                    foreach (var item in oVisibilidad.vista1.Coberturados)
                    {
                        oWsVisibilidad.Cells[1, colcab].Value = item.ToString();

                        //dando borde
                        oWsVisibilidad.Cells[1, colcab].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidad.Cells[1, colcab].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidad.Cells[1, colcab].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidad.Cells[1, colcab].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;


                        //alineando al centro
                        oWsVisibilidad.Cells[1, colcab].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        //alineado vertical al medio
                        oWsVisibilidad.Cells[1, colcab].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //oWsPrecio.SelectedRange[2, (sw + 1), 2, (sw + 4)].AutoFilter = true;

                        //dando ancho a columnas
                        oWsVisibilidad.Column(colcab).Width = 11;

                        colcab = colcab + 1;

                    }
                    int colcab2 = 2;
                    oWsVisibilidad.Cells[2, 1].Value = "Elemento";
                    //dando borde
                    oWsVisibilidad.Cells[2, 1].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidad.Cells[2, 1].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidad.Cells[2, 1].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidad.Cells[2, 1].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                    //background color negro
                    Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#366092");
                    oWsVisibilidad.Cells[2, 1].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    oWsVisibilidad.Cells[2, 1].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                    //pintando color a blanco
                    Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                    oWsVisibilidad.Cells[2, 1].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    oWsVisibilidad.Cells[2, 1].Style.Font.Color.SetColor(colFromHex2);
                    foreach (var item2 in oVisibilidad.vista1.ListMarca)
                    {
                        oWsVisibilidad.Cells[2, colcab2].Value = item2.ToString();

                        //background color negro
                        Color colFromHex3 = System.Drawing.ColorTranslator.FromHtml("#366092");
                        oWsVisibilidad.Cells[2, colcab2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsVisibilidad.Cells[2, colcab2].Style.Fill.BackgroundColor.SetColor(colFromHex3);
                        //pintando color a blanco
                        Color colFromHex4 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                        oWsVisibilidad.Cells[2, colcab2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsVisibilidad.Cells[2, colcab2].Style.Font.Color.SetColor(colFromHex4);

                        //dando borde
                        oWsVisibilidad.Cells[2, colcab2].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidad.Cells[2, colcab2].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidad.Cells[2, colcab2].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidad.Cells[2, colcab2].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                        //alineando al centro
                        oWsVisibilidad.Cells[2, colcab2].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        //alineado vertical al medio
                        oWsVisibilidad.Cells[2, colcab2].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        colcab2 += 1;
                    }

                    int rowcuerpo = 3;
                    int colcuerpo = 1;

                    foreach (var item3 in oVisibilidad.vista1.Grafico3)
                    {
                        oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Value = item3.Nombre;
                        oWsVisibilidad.Column(colcuerpo).AutoFit();
                        //dando borde
                        oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                        //alineando al centro
                        oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;

                        //alineado vertical al medio
                        oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        foreach (var row in item3.valores2)
                        {
                            colcuerpo += 1;
                            oWsVisibilidad.Column(colcuerpo).AutoFit();
                            oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Value = row.valor;

                            //if (int.Parse(row.idempresa) == 1646)
                            //{
                                if (decimal.Parse(row.Porcentaje) < 50)
                                {
                                    //background color rojo
                                    Color colFromHex5 = System.Drawing.ColorTranslator.FromHtml("#F5A9A9");
                                    oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Fill.BackgroundColor.SetColor(colFromHex5);
                                }
                                else if (decimal.Parse(row.Porcentaje) >= 50 && decimal.Parse(row.Porcentaje) <= 75)
                                {
                                    //background color amarillo
                                    Color colFromHex5 = System.Drawing.ColorTranslator.FromHtml("#F2F5A9");
                                    oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Fill.BackgroundColor.SetColor(colFromHex5);
                                }
                                else if (decimal.Parse(row.Porcentaje) > 75)
                                {
                                    //background color verde
                                    Color colFromHex5 = System.Drawing.ColorTranslator.FromHtml("#D0F5A9");
                                    oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Fill.BackgroundColor.SetColor(colFromHex5);
                                }
                            //}

                            //dando borde
                            oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                            oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                            oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                            //alineando al centro
                            oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                            //alineado vertical al medio
                            oWsVisibilidad.Cells[rowcuerpo, colcuerpo].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                            
                        }
                        colcuerpo = 1;
                        rowcuerpo += 1;
                       
                    }


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }


        public JsonResult VisibilidadExcel3(String __a, String __b, String __c, String __d, String __e, int __f, String __g, String __h)
        {
            string _fileServer = "";
            string _filePath = "";

            _fileServer = String.Format("Visivilidad_Presencia" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Visibilidad Presencia >>

                E_Visibilidad_AJE_AASS_v3 oVisibilidadPresencia= new NwRepVisibilidadAASS().Consulta_Visibilidadv3(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    empresa = __d,
                    marca = __e,
                    periodo = __f,
                    zona = __g,
                    tienda = __h,
                    Persona = ((Persona)Session["Session_Login"]).Person_id
                });

                #endregion

                #endregion

                #region <<< Reporte Visibilidad Presencia >>>
                if ((oVisibilidadPresencia != null))
                {
                    Excel.ExcelWorksheet oWsVisibilidadPresencia = oEx.Workbook.Worksheets.Add("Reporte Visibilidad Presencia");

                    oWsVisibilidadPresencia.Cells[1, 1, 1, 2].Merge = true;
                    oWsVisibilidadPresencia.Cells[1, 1, 1, 2].Value = "PDV con Elemento";

                    //alineado vertical al medio
                    oWsVisibilidadPresencia.Cells[1, 1, 1, 2].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;


                    //dando borde
                    oWsVisibilidadPresencia.Cells[1, 1, 1, 2].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadPresencia.Cells[1, 1, 1, 2].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadPresencia.Cells[1, 1, 1, 2].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadPresencia.Cells[1, 1, 1, 2].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                    //dando ancho a columnas
                    //oWsVisibilidadPresencia.Column(1).Width = 22;

                    int colcab = 3;
                    foreach (var item in oVisibilidadPresencia.Cab_Grilla)
                    {
                        oWsVisibilidadPresencia.Cells[1, colcab].Value = item.Valor1;

                        //dando borde
                        oWsVisibilidadPresencia.Cells[1, colcab].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[1, colcab].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[1, colcab].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[1, colcab].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;


                        //alineando al centro
                        oWsVisibilidadPresencia.Cells[1, colcab].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        //alineado vertical al medio
                        oWsVisibilidadPresencia.Cells[1, colcab].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //oWsPrecio.SelectedRange[2, (sw + 1), 2, (sw + 4)].AutoFilter = true;

                        //dando ancho a columnas
                        oWsVisibilidadPresencia.Column(colcab).Width = 11;

                        colcab = colcab + 1;

                    }
                    int colcab2 = 3;
                    oWsVisibilidadPresencia.Cells[2, 1, 2, 2].Merge = true;
                    oWsVisibilidadPresencia.Cells[2, 1, 2, 2].Value = "Cantidad de Elemento";

                    //alineado vertical al medio
                    oWsVisibilidadPresencia.Cells[2, 1, 2, 2].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //dando borde
                    oWsVisibilidadPresencia.Cells[2, 1, 2, 2].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadPresencia.Cells[2, 1, 2, 2].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadPresencia.Cells[2, 1, 2, 2].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadPresencia.Cells[2, 1, 2, 2].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                    //background color negro
                    //Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#366092");
                    //oWsVisibilidadPresencia.Cells[2, 1].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsVisibilidadPresencia.Cells[2, 1].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                    ////pintando color a blanco
                    //Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                    //oWsVisibilidadPresencia.Cells[2, 1].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsVisibilidadPresencia.Cells[2, 1].Style.Font.Color.SetColor(colFromHex2);
                    foreach (var item2 in oVisibilidadPresencia.Cab_Grilla)
                    {
                        oWsVisibilidadPresencia.Cells[2, colcab2].Value = item2.Valor2;

                        //background color negro
                        //Color colFromHex3 = System.Drawing.ColorTranslator.FromHtml("#366092");
                        //oWsVisibilidadPresencia.Cells[2, colcab2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        //oWsVisibilidadPresencia.Cells[2, colcab2].Style.Fill.BackgroundColor.SetColor(colFromHex3);
                        ////pintando color a blanco
                        //Color colFromHex4 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                        //oWsVisibilidadPresencia.Cells[2, colcab2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        //oWsVisibilidadPresencia.Cells[2, colcab2].Style.Font.Color.SetColor(colFromHex4);

                        //dando borde
                        oWsVisibilidadPresencia.Cells[2, colcab2].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[2, colcab2].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[2, colcab2].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[2, colcab2].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                        //alineando al centro
                        oWsVisibilidadPresencia.Cells[2, colcab2].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        //alineado vertical al medio
                        oWsVisibilidadPresencia.Cells[2, colcab2].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        colcab2 += 1;
                    }

                    int colcab3 = 3;
                    oWsVisibilidadPresencia.Cells[3, 1].Value = "Nombre Elemento";
                    oWsVisibilidadPresencia.Cells[3, 2].Value = "Total Elementos";

                    //background color azul
                    Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#366092");
                    oWsVisibilidadPresencia.Cells[3, 1, 3, 2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    oWsVisibilidadPresencia.Cells[3, 1, 3, 2].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                    //pintando color a blanco
                    Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                    oWsVisibilidadPresencia.Cells[3, 1, 3, 2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    oWsVisibilidadPresencia.Cells[3, 1, 3, 2].Style.Font.Color.SetColor(colFromHex2);
                    
                    //dando borde
                    oWsVisibilidadPresencia.Cells[3, 1, 3, 2].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadPresencia.Cells[3, 1, 3, 2].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadPresencia.Cells[3, 1, 3, 2].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadPresencia.Cells[3, 1, 3, 2].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                    //alineando al centro
                    oWsVisibilidadPresencia.Cells[3, 2].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                    //alineado vertical al medio
                    oWsVisibilidadPresencia.Cells[3, 1, 3, 2].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    foreach (var item3 in oVisibilidadPresencia.Cab_Grilla)
                    {
                        oWsVisibilidadPresencia.Cells[3, colcab3].Value = item3.Nom_Elmento;

                        //background color negro
                        Color colFromHex3 = System.Drawing.ColorTranslator.FromHtml("#366092");
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.Fill.BackgroundColor.SetColor(colFromHex3);
                        //pintando color a blanco
                        Color colFromHex4 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.Font.Color.SetColor(colFromHex4);

                        //dando borde
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                        //alineando al centro
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        //alineado vertical al medio
                        oWsVisibilidadPresencia.Cells[3, colcab3].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        colcab3 += 1;

                    }
                    int rowcuerpo = 4;
                    int colcuerpo = 3;

                    foreach (var item3 in oVisibilidadPresencia.Content_Grilla)
                    {
                        oWsVisibilidadPresencia.Cells[rowcuerpo, 1].Value = item3.Nom_Elemento;
                        oWsVisibilidadPresencia.Cells[rowcuerpo, 2].Value = item3.Total;
                        //dando borde
                        oWsVisibilidadPresencia.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadPresencia.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                        //alineando al centro
                        oWsVisibilidadPresencia.Cells[rowcuerpo, 2].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        //alineado vertical al medio
                        oWsVisibilidadPresencia.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        foreach (var row in item3.Valores)
                        {
                            if (int.Parse(row.ToString()) > 0)
                            {
                                oWsVisibilidadPresencia.Cells[rowcuerpo, colcuerpo].Value = "X";
                            }
                            else
                            {
                                oWsVisibilidadPresencia.Cells[rowcuerpo, colcuerpo].Value = "";
                            }


                            //dando borde
                            oWsVisibilidadPresencia.Cells[rowcuerpo, colcuerpo].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oWsVisibilidadPresencia.Cells[rowcuerpo, colcuerpo].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                            oWsVisibilidadPresencia.Cells[rowcuerpo, colcuerpo].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                            oWsVisibilidadPresencia.Cells[rowcuerpo, colcuerpo].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                            //alineando al centro
                            oWsVisibilidadPresencia.Cells[rowcuerpo, colcuerpo].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                            //alineado vertical al medio
                            oWsVisibilidadPresencia.Cells[rowcuerpo, colcuerpo].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            colcuerpo += 1;
                        }
                        colcuerpo = 3;
                        rowcuerpo += 1;

                    }

                    //dando ancho a columnas
                    oWsVisibilidadPresencia.Column(1).Width = 21;
                    oWsVisibilidadPresencia.Column(2).Width = 11;

                    //Ajustando texto
                    oWsVisibilidadPresencia.Column(1).AutoFit();
                    oWsVisibilidadPresencia.Column(2).AutoFit();

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        //public ActionResult CnsEfectividad(string _a, string _b, string _c, string _d, string _e, int _f, string _g)
        //{
        //    return Json(new { response = new NwRepVisibilidadAASS().Consulta_Efectividad(new Request_NWRepStd_General { 
        //        subcanal = _a,cadena=_b,categoria=_c,empresa=_d,marca=_e,periodo = _f ,zona=_g            
        //    }) });
        //}

        public JsonResult VisibilidadExcelArriendo(String __a, String __b, String __c, String __d, String __e, int __f, String __g)
        {
            string _fileServer = "";
            string _filePath = "";

            _fileServer = String.Format("Visibilidad_Arriendo" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Visibilidad Presencia >>

                E_Visibilidad_AJE_AASS_Efectividad oVisibilidadArriendo = new NwRepVisibilidadAASS().Consulta_Efectividad(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    empresa = __d,
                    marca = __e,
                    periodo = __f,
                    zona = __g,
                    Persona = ((Persona)Session["Session_Login"]).Person_id
                });

                #endregion

                #endregion

                #region <<< Reporte Visibilidad Arriendo >>>
                if ((oVisibilidadArriendo != null))
                {
                    Excel.ExcelWorksheet oWsVisibilidadArriendo = oEx.Workbook.Worksheets.Add("Reporte Visibilidad Arriendo");


                    oWsVisibilidadArriendo.Cells[1, 1].Value = "ELEMENTO";
                    oWsVisibilidadArriendo.Cells[1, 2].Value = "PDV OBJETIVO";
                    oWsVisibilidadArriendo.Cells[1, 3].Value = "REAL";

                    oWsVisibilidadArriendo.Row(1).Style.Font.Bold = true;

                    //alineado vertical al medio
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 3].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;


                    //dando borde
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 3].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 3].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 3].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 3].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                    //dando ancho a columnas
                    //oWsVisibilidadPresencia.Column(1).Width = 22;

                    int colcuerpo = 1;
                    int rowcuerpo = 2;
                    foreach (var item in oVisibilidadArriendo.Grilla2)
                    {
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo].Value = (item.Elemento).ToUpper();
                        oWsVisibilidadArriendo.Cells[rowcuerpo, (colcuerpo + 1)].Value = (item.PDV).ToUpper();
                        if (item.Real == "1")
                        {
                            oWsVisibilidadArriendo.Cells[rowcuerpo, (colcuerpo + 2)].Value = "X";
                        }
                        else
                        {
                            oWsVisibilidadArriendo.Cells[rowcuerpo, (colcuerpo + 2)].Value = "";
                        }

                        //dando borde
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 2)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 2)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 2)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 2)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;


                        //alineado vertical al medio
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 2)].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //oWsPrecio.SelectedRange[2, (sw + 1), 2, (sw + 4)].AutoFilter = true;

                        //dando ancho a columnas
                        oWsVisibilidadArriendo.Column(colcuerpo).Width = 17;
                        oWsVisibilidadArriendo.Column((colcuerpo + 1)).Width = 38;
                        oWsVisibilidadArriendo.Column((colcuerpo + 2)).Width = 11;

                        rowcuerpo += 1;
                        colcuerpo = 1;

                    }

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult VisibilidadExcelArriendoResumen(String __a, String __b, String __c, String __d, String __e, int __f, String __g)
        {
            string _fileServer = "";
            string _filePath = "";

            _fileServer = String.Format("Visibilidad_Arriendo_Resumen" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Visibilidad Presencia >>

                E_Visibilidad_AJE_AASS_Efectividad oVisibilidadArriendo = new NwRepVisibilidadAASS().Consulta_Efectividad(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    empresa = __d,
                    marca = __e,
                    periodo = __f,
                    zona = __g,
                    Persona = ((Persona)Session["Session_Login"]).Person_id
                });

                #endregion

                #endregion

                #region <<< Reporte Visibilidad Arriendo >>>
                if ((oVisibilidadArriendo != null))
                {
                    Excel.ExcelWorksheet oWsVisibilidadArriendo = oEx.Workbook.Worksheets.Add("Reporte Visibilidad Arriendo Resumen");


                    oWsVisibilidadArriendo.Cells[1, 1].Value = "ELEMENTO";
                    oWsVisibilidadArriendo.Cells[1, 2].Value = "OBJETIVO";
                    oWsVisibilidadArriendo.Cells[1, 3].Value = "REAL";
                    oWsVisibilidadArriendo.Cells[1, 4].Value = "EFECTIVIDAD %";
                    oWsVisibilidadArriendo.Cells[1, 2, 1, 4].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                    oWsVisibilidadArriendo.Row(1).Style.Font.Bold = true;

                    //alineado vertical al medio
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 4].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;


                    //dando borde
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 4].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 4].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 4].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[1, 1, 1, 4].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                    //dando ancho a columnas
                    oWsVisibilidadArriendo.Column(4).Width = 22;

                    int colcuerpo = 1;
                    int rowcuerpo = 2;
                    Int32 vobj = 0, vreal = 0;
                    foreach (var item in oVisibilidadArriendo.Grilla1)
                    {
                       
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo].Value = (item.Elemento).ToUpper();
                        oWsVisibilidadArriendo.Cells[rowcuerpo, (colcuerpo + 1)].Value = (item.Objetivo).ToUpper();
                        
                        if (item.Real == "1")
                        {
                            oWsVisibilidadArriendo.Cells[rowcuerpo, (colcuerpo + 2)].Value = "X";
                        }
                        else
                        {
                            oWsVisibilidadArriendo.Cells[rowcuerpo, (colcuerpo + 2)].Value = "0";
                        }

                        if (item.Efectividad == "")
                        {
                            oWsVisibilidadArriendo.Cells[rowcuerpo, (colcuerpo + 3)].Value = "0" + "%";
                        }
                        else
                        {
                            oWsVisibilidadArriendo.Cells[rowcuerpo, (colcuerpo + 3)].Value = (item.Efectividad).ToUpper() + "%";
                        }



                        //dando borde
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;


                        //alineado vertical al medio
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 2)].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo + 1, rowcuerpo, (colcuerpo + 3)].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;


                        //oWsPrecio.SelectedRange[2, (sw + 1), 2, (sw + 4)].AutoFilter = true;

                        //dando ancho a columnas
                        oWsVisibilidadArriendo.Column(colcuerpo).Width = 20;
                        oWsVisibilidadArriendo.Column((colcuerpo + 1)).Width = 38;
                        oWsVisibilidadArriendo.Column((colcuerpo + 2)).Width = 11;

                        rowcuerpo += 1;
                        colcuerpo = 1;
                        vobj = vobj+Convert.ToInt32(item.Objetivo);
                        vreal = vreal+Convert.ToInt32(item.Real);
                      
                    }
             
                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo].Value = "TOTAL";
                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo + 1].Value = vobj;
                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo + 2].Value = vreal;

                    if (vreal == 0)
                    {
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo + 3].Value = 0 +"%";
                    }
                    else
                    {
                        oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo + 3].Value = ((vreal / vobj) * 100) + "%";
                    }

                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;
                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo + 1, rowcuerpo, (colcuerpo + 3)].Style.HorizontalAlignment =Style.ExcelHorizontalAlignment.Center;

                    // Dando color de letra
                    oWsVisibilidadArriendo.Cells[rowcuerpo, colcuerpo, rowcuerpo, (colcuerpo + 3)].Style.Font.Bold = true;
            

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }




        #endregion

        #region Power BI
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-05-09
        /// </summary>
        /// <returns></returns>
        public ActionResult PowerBISos()
        {
            return View();
        }
        #endregion
    }
}