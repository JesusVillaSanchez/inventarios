﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

using Excel = OfficeOpenXml;
using ExcelStyle = OfficeOpenXml.Style;
using Style = OfficeOpenXml.Style;
using OfficeOpenXml.Drawing;

//using Lucky.Business;
//using Lucky.Business.Common;
//using Negocio = Lucky.Business.Common.Servicio;

//using Lucky.Entity;
//using Lucky.Entity.Common;
//using Entidad = Lucky.Entity.Common.Servicio;

//using Lucky.Business;
//using Lucky.Business.Common;
//using Lucky.Business.Common.Servicio;

//using Lucky.Entity;
//using Lucky.Entity.Common;
//using Lucky.Entity.Common.Servicio;
//using Lucky.Entity.Common.Servicio.DM_Alicorp;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Alicorp;

namespace Lucky.Xplora.Controllers
{
    public class AlicorpController : Controller
    {
        string LocalFoto = Convert.ToString(ConfigurationManager.AppSettings["LocalFoto"]);
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        string LocalFormat = Convert.ToString(ConfigurationManager.AppSettings["LocalFormat"]);
        string Xplora = Convert.ToString(ConfigurationManager.AppSettings["Xplora"]);
        string XploraTemp = Convert.ToString(ConfigurationManager.AppSettings["XploraTemp"]);
        string XploraFoto = Convert.ToString(ConfigurationManager.AppSettings["XploraFoto"]);
        string Contraseña = "juanjolucerogoicochea";

        #region Alicorp
        ////
        //// GET: /Alicorp/

        //public ActionResult Inicio()
        //{
        //    //Se registra visita
        //    Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);

        //    return View();
        //}

        //[HttpGet]
        //public ActionResult Datamercaderista()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult GetOficina() {
        //    return View(new Oficina().Lista(new Request_Oficinas_Por_Campania() { campania = "813711382010" }));
        //}

        //[HttpPost]
        //public ActionResult GetDepartamento(string __a)
        //{
        //    return View(new Departamento().Lista(new Request_Departamento_Por_Compania_Campania_Pais_Oficina() { 
        //        compania = "1562",
        //        campania = "813711382010",
        //        pais = "589",
        //        oficina = __a
        //    }));
        //}

        //[HttpPost]
        //public ActionResult GetNodoComercial(string __a, string __b)
        //{
        //    return View(new NodoComercial().Lista(new Request_GetCadena_Campania_Oficina_Departamento()
        //    {
        //        campania = "813711382010",
        //        oficina = Int32.Parse(__a),
        //        departamento = __b
        //    }));
        //}

        //[HttpPost]
        //public ActionResult GetPuntoVenta(string __a, string __b)
        //{
        //    return View(new PuntoVenta().Lista(new Request_PuntoVenta_Campania_Departamento_Provincia_NodoComercial()
        //    {
        //        campania = "813711382010",
        //        departamento = __a,
        //        provincia = "0",
        //        nodocomercial = __b
        //    }));
        //}

        //[HttpPost]
        //public ActionResult GetCategoria()
        //{
        //    return View(new Models.Categoria().Lista(new Request_GetCategoria_Opcion_Campania()
        //    {
        //        opcion = "501",
        //        campania = "813711382010"
        //    }));
        //}

        //[HttpPost]
        //public ActionResult GetMarca(string __a)
        //{
        //    return View(new Models.Marca().Lista(new Request_GetMarca_Opcion_Campania_Categoria()
        //    {
        //        opcion = 521,
        //        campania = "813711382010",
        //        categoria = Int32.Parse(__a)
        //    }));
        //}

        //[HttpPost]
        //public ActionResult GetMarcaAASS(string __a)
        //{
        //    return View(new Models.Marca().Lista(new Request_GetMarca_Opcion_Categoria()
        //    {
        //        opcion = 82,
        //        categoria = __a
        //    }));
        //}

        //[HttpPost]
        //public ActionResult GetSupervisor(string __a)
        //{
        //    return View(new Models.Persona().Supervisor(new Request_GetSupervisor_Campania_Departamento_Provincia_Distrito()
        //    {
        //        campania = "813711382010",
        //        departamento = __a,
        //        provincia = "0",
        //        distrito = "0"
        //    }));
        //}

        //[HttpPost]
        //public ActionResult GetGenerador(string __a, string __b, string __c)
        //{
        //    return View(new Models.Persona().Generador(new Request_GetGenerador_Campania_Oficina_Departamento_Supervisor()
        //    {
        //        campania = "813711382010",
        //        oficina = Int32.Parse(__a),
        //        departamento = __b,
        //        supervisor = Int32.Parse(__c)
        //    }));
        //}

        ///// <summary>
        ///// Autor: jlucero
        ///// Fecha: 2015-01-06
        ///// </summary>
        ///// <param name="__a">fecha_inicio</param>
        ///// <param name="__b">fecha_fin</param>
        ///// <param name="__c">oficina</param>
        ///// <param name="__d">departamento</param>
        ///// <param name="__e">nodocomercial</param>
        ///// <param name="__f">pdv</param>
        ///// <param name="__g">categoria</param>
        ///// <param name="__h">marca</param>
        ///// <param name="__i">supervisor</param>
        ///// <param name="__j">gie</param>
        ///// <param name="__k">estado</param>
        ///// <returns></returns>
        //[HttpGet]
        //public ActionResult Consolidado(string __a, string __b, int __c, string __d, int __e, string __f, int __g, int __h, int __i, int __j, string __ca, string __da, string __ea, string __fa, string __ga, string __ha, string __ia, string __ja, int __z)
        //{
        //    TempData["fecha_inicio"] = DateTime.Parse(__a).ToString("dd-MM-yyyy HH:mm");
        //    TempData["fecha_fin"] = DateTime.Parse(__b).ToString("dd-MM-yyyy HH:mm");
        //    TempData["oficina"] = __c;
        //    TempData["departamento"] = __d;
        //    TempData["nodocomercial"] = __e;
        //    TempData["pdv"] = __f;
        //    TempData["categoria"] = __g;
        //    TempData["marca"] = __h;
        //    TempData["supervisor"] = __i;
        //    TempData["gie"] = __j;

        //    TempData["oficina-descripcion"] = __ca;
        //    TempData["departamento-descripcion"] = __da;
        //    TempData["nodocomercial-descripcion"] = __ea;
        //    TempData["pdv-descripcion"] = __fa;
        //    TempData["categoria-descripcion"] = __ga;
        //    TempData["marca-descripcion"] = __ha;
        //    TempData["supervisor-descripcion"] = __ia;
        //    TempData["gie-descripcion"] = __ja;

        //    /*
        //     * numeracion de reportes
        //     * 1  -> Precio
        //     * 2  -> Stock
        //     * 3  -> SOD
        //     */
        //    if (__z == 1) {
        //        return RedirectToAction("ConsolidadoPrecio", "Alicorp");
        //    }

        //    return View();
        //}

        /////// <summary>
        /////// Autor: jlucero
        /////// Fecha: 2015-01-08
        /////// </summary>
        /////// <returns></returns>
        ////[HttpGet]
        //public ActionResult ConsolidadoPrecio()
        //{
        //    string canal = "1000";
        //    string campania = "813711382010";

        //    return View(new BL_GES_Operativa().BWebXplora_Alicorp_Consolidado_Precio_Cabecera(Convert.ToString(TempData["fecha_inicio"]), Convert.ToString(TempData["fecha_fin"]), canal, campania, Convert.ToString(TempData["oficina"]), Convert.ToString(TempData["departamento"]), Convert.ToString(TempData["nodocomercial"]), Convert.ToString(TempData["pdv"]), Convert.ToString(TempData["categoria"]), Convert.ToString(TempData["marca"]), Convert.ToString(TempData["supervisor"]), Convert.ToString(TempData["gie"]), 1));
        //}

        ///// <summary>
        ///// Autor: jlucero
        ///// Fecha: 2015-01-07
        ///// </summary>
        ///// <param name="__a">fecha_inicio</param>
        ///// <param name="__b">fecha_fin</param>
        ///// <param name="__c">oficina</param>
        ///// <param name="__d">departamento</param>
        ///// <param name="__e">nodocomercial</param>
        ///// <param name="__f">pdv</param>
        ///// <param name="__g">categoria</param>
        ///// <param name="__h">marca</param>
        ///// <param name="__i">supervisor</param>
        ///// <param name="__j">gie</param>
        ///// <param name="__k">estado</param>
        ///// <returns></returns>
        //[HttpPost]
        //public ActionResult ConsolidadoPrecioDetalle(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j)
        //{
        //    string canal = "1000";
        //    string campania = "813711382010";

        //    return new ContentResult
        //    {
        //        Content = MvcApplication._Serialize(new BL_GES_Operativa().BWebXplora_Alicorp_Consolidado_Precio_Detalle(__a, __b, canal, campania, __c, __d, __e, __f, __g, __h, __i, __j, 1)),
        //        ContentType = "application/json"
        //    };
        //}

        ///// <summary>
        ///// Autor: jlucero
        ///// Fecha: 2015-01-13
        ///// </summary>
        ///// <param name="__a"></param>
        ///// <param name="__b"></param>
        ///// <param name="__c"></param>
        ///// <param name="__d"></param>
        ///// <param name="__e"></param>
        ///// <param name="__f"></param>
        ///// <param name="__g"></param>
        ///// <param name="__h"></param>
        ///// <param name="__i"></param>
        ///// <param name="__j"></param>
        ///// <param name="__z">Tipo de descarga</param>
        ///// <returns></returns>
        //[HttpPost]
        //public JsonResult ConsolidadoPrecioDescarga(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, int __z)
        //{
        //    string canal = "1000";
        //    string campania = "813711382010";

        //    /*
        //     * Contraseña -> juanjolucerog
        //     * Para el tipo de descarga, variable "__z", se consideran las siguientes opciones:
        //     * 1 -> Descarga para desarrollo de CRUD
        //     * 2 -> Descarga para visualizacion de informacion, se considera solo la informacion valida
        //     */

        //    string _filePath = "";
        //    //string _categoria = "";
        //    string _fileServer = "";
        //    string _fileFormatServer = "";
        //    string _columnaNombre = "";

        //    int _fila = 0;
        //    int _columna = 0;

        //    _fileServer = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
        //    _fileFormatServer = Path.Combine(Server.MapPath("/Format"), "lucky-Alicorp-formato.xlsx");
        //    _filePath = Path.Combine(Server.MapPath("/Temp"), _fileServer);

        //    /*copia formato y lo ubica en carpeta temporal*/
        //    System.IO.File.Copy(_fileFormatServer, _filePath, true);

        //    /*instancia archivo para su uso*/
        //    FileInfo _fileNew = new FileInfo(_filePath);

        //    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew)) {
        //        foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets) {
        //            if (oWs.Name == "Consolidado") {
        //                if (__z == 1) {
        //                    DataTable oDt = new BL_GES_Operativa().BWebXplora_Alicorp_Consolidado_Precio_Detalle(__a, __b, canal, campania, __c, __d, __e, __f, __g, __h, __i, __j, 2);
        //                    List<E_Consolidado_Precio> oDl = new BL_GES_Operativa().BWebXplora_Alicorp_Consolidado_Precio_Cabecera(__a, __b, canal, campania, __c, __d, __e, __f, __g, __h, __i, __j, 2);

        //                    _columna = 14;
        //                    foreach (E_Consolidado_Precio oDa in oDl)
        //                    {
        //                        if (oDa.Id >= 14)
        //                        {
        //                            oWs.Cells[1, _columna].Value = oDa.CategoriaDescripcion;
        //                            oWs.Cells[2, _columna].Value = oDa.ProductoDescripcion;
        //                            oWs.Cells[7, _columna].Value = Convert.ToString(oDa.Categoria) + "_" + oDa.Producto;
        //                            oWs.Cells[8, _columna].Value = oDa.CategoriaDescripcion;
        //                            oWs.Cells[9, _columna].Value = oDa.ProductoDescripcion;

        //                            oWs.Cells[10, _columna + 1].Value = "Precio Mayorista";
        //                            oWs.Cells[10, _columna + 2].Value = "Precio Reventa";
        //                            oWs.Cells[10, _columna + 3].Value = "Comentario";

        //                            oWs.SelectedRange[2, _columna, 2, _columna + 3].Merge = true;
        //                            oWs.SelectedRange[9, _columna, 9, _columna + 3].Merge = true;

        //                            _columnaNombre = Convert.ToString(oWs.Cells[1, _columna + 1].Address);
        //                            //oWs.Cells[3, _columna + 1].Formula = "count(" + _columnaNombre.Remove(_columnaNombre.Length - 1, 1) + "10:" + _columnaNombre.Remove(_columnaNombre.Length - 1, 1) + (_columnaNombre.Remove(_columnaNombre.Length - 1, 1) + iFila + 1) + ")";

        //                            oWs.Column(_columna).Hidden = true;

        //                            _columna = _columna + 4;
        //                        }
        //                    }

        //                    _fila = 11;
        //                    foreach (DataRow r in oDt.Rows)
        //                    {
        //                        oWs.Cells[_fila, 1].Value = Int32.Parse(r["__a"].ToString());
        //                        oWs.Cells[_fila, 2].Value = r["__k"].ToString();
        //                        oWs.Cells[_fila, 3].Value = r["__l"].ToString();
        //                        oWs.Cells[_fila, 4].Value = r["__b"].ToString();
        //                        oWs.Cells[_fila, 5].Value = r["__c"].ToString();
        //                        oWs.Cells[_fila, 6].Value = r["__d"].ToString();
        //                        oWs.Cells[_fila, 7].Value = r["__e"].ToString();
        //                        oWs.Cells[_fila, 8].Value = r["__f"].ToString();
        //                        oWs.Cells[_fila, 9].Value = r["__g"].ToString();
        //                        oWs.Cells[_fila, 10].Value = r["__i"].ToString();
        //                        oWs.Cells[_fila, 11].Value = r["__h"].ToString();
        //                        oWs.Cells[_fila, 12].Value = r["__j"].ToString();

        //                        _columna = 14;
        //                        foreach (E_Consolidado_Precio oDa in oDl) {
        //                            if (oDa.Id >= 14) {
        //                                oWs.Cells[_fila, _columna].Value = r["id_" + oDa.Producto].ToString();
        //                                oWs.Cells[_fila, _columna + 1].Value = (r["pm_" + oDa.Producto].ToString().Length == 0 ? (decimal?)null : decimal.Parse(r["pm_" + oDa.Producto].ToString()));
        //                                oWs.Cells[_fila, _columna + 2].Value = (r["pr_" + oDa.Producto].ToString().Length == 0 ? (decimal?)null : decimal.Parse(r["pr_" + oDa.Producto].ToString()));
        //                                oWs.Cells[_fila, _columna + 3].Value = r["ob_" + oDa.Producto].ToString();

        //                                _columna = _columna + 4;
        //                            }
        //                        }

        //                        _fila++;
        //                    }
        //                }

        //                oWs.Protection.AllowInsertRows = false;
        //                oWs.Protection.AllowInsertColumns = false;
        //                oWs.Protection.AllowDeleteColumns = false;
        //                oWs.Protection.AllowPivotTables = false;
        //                oWs.Protection.AllowSelectLockedCells = true;
        //                oWs.Protection.AllowSelectUnlockedCells = true;
        //                oWs.Protection.AllowAutoFilter = true;
        //                oWs.Protection.AllowSort = false;
        //                oWs.Protection.IsProtected = true;
        //                oWs.Protection.SetPassword("maldito_quien_te_dio_la_contraseña!!!!");
        //            }
        //        }

        //        oEx.Save();
        //    }

        //    return Json(new { Archivo = _fileServer });
        //}
        #endregion

        #region Alicorp Gestion de precios
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-25
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Inicio() {
            Persona oPersona = ((Persona)Session["Session_Login"]);

            TempData["ListaStatus"] = new AlicorpGestionPrecios().Status(new AlicorpGestionPreciosParametro() { opcion = 15, parametro = Convert.ToString(oPersona.Person_id) });

            ViewBag.Xplora = Xplora;

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-26
        /// </summary>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult Inicio(int opcion, string parametro) 
        {
            if (opcion == 3)
            {
                #region Informacion de precios
                return new ContentResult
                {
                    Content = MvcApplication._Serialize(
                        new AlicorpGestionPrecios().Precio(new AlicorpGestionPreciosParametro()
                        {
                            opcion = opcion,
                            parametro = parametro
                        })),
                    ContentType = "application/json"
                };
                #endregion
            }
            else if (opcion == 4)
            {
                #region Actualiza Informacion de Precios
                if (parametro.Length == 0) return View();

                int Resultado = 0;
                string Evento = Request["evento"];

                //List<AlicorpGestionPreciosModelo> lAlicorpGestionPreciosModelo = MvcApplication._Deserialize<List<AlicorpGestionPreciosModelo>>(parametro);

                //foreach (AlicorpGestionPreciosModelo oAlicorpGestionPreciosModelo in lAlicorpGestionPreciosModelo)
                //{
                //    List<string> lString = new List<string>();
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.TrpId));
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.PrecioMpm));
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.PrecioRvtMin));
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.PrecioRvtMay));
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.Chat));
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.FasId));
                //    lString.Add(Evento);
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.CatId));
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.OfiId));
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.OfiPerId));
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.CatPerId));
                //    lString.Add(Convert.ToString(oAlicorpGestionPreciosModelo.Estado));

                //    Resultado += new AlicorpGestionPrecios().GuardaEnvia(new AlicorpGestionPreciosParametro()
                //    {
                //        opcion = opcion,
                //        parametro = String.Join(",", lString.ToArray())
                //    });
                //}

                Resultado = new AlicorpGestionPrecios().GuardaEnvia(new AlicorpGestionPreciosParametro() { opcion = opcion, parametro = parametro, comentario = Evento });

                return Content(new ContentResult
                {
                    Content = "{ \"Resultado\": " + Resultado + " }",
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (opcion == 5)
            {
                #region Informe final
                bool Descarga = Convert.ToBoolean(Request["Descarga"]);

                if (Descarga == true) 
                {
                    #region Descarga Informe final
                    int Fila = 4;
                    string ArchivoNombre = "";
                    string RutaFormato = "";
                    string RutaTemporal = "";

                    ArchivoNombre = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);
                    RutaFormato = Path.Combine(LocalFormat, "Lucky-Alicorp-Informe_Final.v1.2.xlsx");
                    RutaTemporal = Path.Combine(LocalTemp, ArchivoNombre);

                    System.IO.File.Copy(RutaFormato, RutaTemporal, true);

                    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(RutaTemporal)))
                    {
                        Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets["Informe Final"];

                        List<AlicorpGestionPreciosModelo> lAlicorpGestionPreciosModelo = MvcApplication._Deserialize<List<AlicorpGestionPreciosModelo>>(parametro);

                        foreach (AlicorpGestionPreciosModelo oAlicorpGestionPreciosModelo in lAlicorpGestionPreciosModelo)
                        {
                            oWs.Cells[Fila, 2].Value = oAlicorpGestionPreciosModelo.OfiDescripcion;
                            oWs.Cells[Fila, 3].Value = oAlicorpGestionPreciosModelo.ProDescripcion;
                            oWs.Cells[Fila, 4].Value = oAlicorpGestionPreciosModelo.PrecioMpm;
                            oWs.Cells[Fila, 5].Value = oAlicorpGestionPreciosModelo.VarPrecioMpm;
                            oWs.Cells[Fila, 6].Value = oAlicorpGestionPreciosModelo.PrecioRvtMay;
                            oWs.Cells[Fila, 7].Value = oAlicorpGestionPreciosModelo.VarPrecioRvtMay;
                            oWs.Cells[Fila, 8].Value = oAlicorpGestionPreciosModelo.MargenMayorista;
                            oWs.Cells[Fila, 9].Value = oAlicorpGestionPreciosModelo.VarMargenMayorista;
                            oWs.Cells[Fila, 10].Value = oAlicorpGestionPreciosModelo.PrecioDexMinimayorista;
                            oWs.Cells[Fila, 11].Value = oAlicorpGestionPreciosModelo.VarPrecioDexMinimayorista;
                            oWs.Cells[Fila, 12].Value = oAlicorpGestionPreciosModelo.BrechaMinimayorista;
                            oWs.Cells[Fila, 13].Value = oAlicorpGestionPreciosModelo.VarBrechaMinimayorista;
                            oWs.Cells[Fila, 14].Value = oAlicorpGestionPreciosModelo.PrecioDexMinorista;
                            oWs.Cells[Fila, 15].Value = oAlicorpGestionPreciosModelo.VarPrecioDexMinorista;
                            oWs.Cells[Fila, 16].Value = oAlicorpGestionPreciosModelo.BrechaMinorista;
                            oWs.Cells[Fila, 17].Value = oAlicorpGestionPreciosModelo.VarBrechaMinorista;
                            oWs.Cells[Fila, 18].Value = oAlicorpGestionPreciosModelo.PrecioPvp;
                            oWs.Cells[Fila, 19].Value = oAlicorpGestionPreciosModelo.VarPrecioPvp;
                            oWs.Cells[Fila, 20].Value = oAlicorpGestionPreciosModelo.MargenMinorista;
                            oWs.Cells[Fila, 21].Value = oAlicorpGestionPreciosModelo.VarMargenMinorista;
                            oWs.Cells[Fila, 22].Value = oAlicorpGestionPreciosModelo.MargenTotalComercio;
                            oWs.Cells[Fila, 23].Value = oAlicorpGestionPreciosModelo.VarMargenTotalComercio;
                            oWs.Cells[Fila, 24].Value = oAlicorpGestionPreciosModelo.RpaCosto;
                            oWs.Cells[Fila, 25].Value = oAlicorpGestionPreciosModelo.RpaPvp;

                            Fila++;
                        }

                        oWs.Column(3).AutoFit();

                        oEx.Save();
                    }

                    return Content(new ContentResult { Content = "{ \"Archivo\": \"" + ArchivoNombre + "\" }", ContentType = "application/json", ContentEncoding = Encoding.UTF8 }.Content);
                    #endregion
                }else{
                    #region Informe final
                    return new ContentResult
                    {
                        Content = MvcApplication._Serialize(
                            new AlicorpGestionPrecios().Precio(new AlicorpGestionPreciosParametro()
                            {
                                opcion = opcion,
                                parametro = parametro
                            })),
                        ContentType = "application/json"
                    };
                    #endregion
                }
                #endregion
            }
            else if (opcion == 8)
            {
                #region Panel principal
                return new ContentResult
                {
                    Content = MvcApplication._Serialize(
                        new AlicorpGestionPrecios().Panel(new AlicorpGestionPreciosParametro()
                        {
                            opcion = opcion,
                            parametro = parametro
                        })),
                    ContentType = "application/json"
                };
                #endregion
            }
            else if (opcion == 9)
            {
                #region Status de fases
                return new ContentResult
                {
                    Content = MvcApplication._Serialize(
                        new AlicorpGestionPrecios().FaseStatus(new AlicorpGestionPreciosParametro()
                        {
                            opcion = opcion,
                            parametro = parametro
                        })),
                    ContentType = "application/json"
                };
                #endregion
            }
            else if (opcion == 10)
            {
                #region Datos de personal
                return new ContentResult
                {
                    Content = MvcApplication._Serialize(
                        new AlicorpGestionPrecios().PersonaDatos(new AlicorpGestionPreciosParametro()
                        {
                            opcion = opcion,
                            parametro = parametro
                        })),
                    ContentType = "application/json"
                };
                #endregion
            }
            else if (opcion == 11)
            {
                #region Ranking dias atrasados
                return new ContentResult
                {
                    Content = MvcApplication._Serialize(
                        new AlicorpGestionPrecios().RankingAtrasados(new AlicorpGestionPreciosParametro()
                        {
                            opcion = opcion,
                            parametro = parametro
                        })),
                    ContentType = "application/json"
                };
                #endregion
            }
            else if (opcion == 12)
            {
                #region Visor de status
                string Evento = Request["evento"];

                if (Evento == "Descarga")
                {
                    #region Descarga visor de status
                    int Fila = 3;
                    string ArchivoNombre = "";
                    string RutaFormato = "";
                    string RutaTemporal = "";

                    ArchivoNombre = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);
                    RutaFormato = Path.Combine(LocalFormat, "Formato-Retraso-Dias.v1.1.xlsx");
                    RutaTemporal = Path.Combine(LocalTemp, ArchivoNombre);

                    System.IO.File.Copy(RutaFormato, RutaTemporal, true);

                    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(RutaTemporal)))
                    {
                        Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets["Visor Status"];

                        List<string> lPeriodo = new List<string>(parametro.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries));

                        foreach (string SPeriodo in lPeriodo)
                        {
                            List<string> lItem = new List<string>(SPeriodo.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries));

                            oWs.Cells[Fila, 3].Value = lItem[1];
                            oWs.Cells[Fila, 2].Value = lItem[0];
                            oWs.Cells[Fila, 4].Value = lItem[2];
                            oWs.Cells[Fila, 5].Value = lItem[3].Replace("-", Environment.NewLine);
                            oWs.Cells[Fila, 5].Style.WrapText = true;
                            oWs.Cells[Fila, 6].Value = lItem[4];

                            oWs.Cells[Fila, 2].Style.Border.Left.Style = ExcelStyle.ExcelBorderStyle.Thin;
                            oWs.Cells[Fila, 6].Style.Border.Right.Style = ExcelStyle.ExcelBorderStyle.Thin;
                            oWs.Cells[Fila, 2, Fila, 6].Style.Border.Top.Style = ExcelStyle.ExcelBorderStyle.Dotted;
                            oWs.Cells[Fila, 2, Fila, 6].Style.Border.Left.Style = ExcelStyle.ExcelBorderStyle.Dotted;
                            oWs.Cells[Fila, 2, Fila, 6].Style.Border.Right.Style = ExcelStyle.ExcelBorderStyle.Dotted;
                            oWs.Cells[Fila, 2, Fila, 6].Style.Border.Bottom.Style = ExcelStyle.ExcelBorderStyle.Dotted;

                            Fila++;
                        }

                        oEx.Save();
                    }

                    return Content(new ContentResult { Content = "{ \"Archivo\": \"" + ArchivoNombre + "\" }", ContentType = "application/json", ContentEncoding = Encoding.UTF8 }.Content);
                    #endregion
                }
                else
                {
                    #region Visor de status
                    return new ContentResult
                    {
                        Content = MvcApplication._Serialize(
                            new AlicorpGestionPrecios().VisorStatus(new AlicorpGestionPreciosParametro()
                            {
                                opcion = opcion,
                                parametro = parametro
                            })),
                        ContentType = "application/json"
                    };
                    #endregion
                }
                #endregion
            }
            else if (opcion == 14)
            {
                #region Carga masiva de informacion
                int Resultado = 0;
                bool Descarga = Convert.ToBoolean(Request["descarga"]);
                string Informe = Convert.ToString(Request["informe"]);
                string ArchivoNombre = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);

                if (Descarga == true)
                {
                    #region Descarga de informacion
                    int Fila = 2;
                    string RutaFormato = "";
                    string RutaTemporal = "";

                    RutaFormato = Path.Combine(LocalFormat, "Lucky-Alicorp-GestionPrecios.v1.0.xlsx");
                    RutaTemporal = Path.Combine(LocalTemp, ArchivoNombre);

                    System.IO.File.Copy(RutaFormato, RutaTemporal, true);

                    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(RutaTemporal)))
                    {
                        Excel.ExcelWorksheet oWs = null;

                        #region 20,21,80 y PVP
                        if (Informe == "202180")
                        {
                            oWs = oEx.Workbook.Worksheets["20-21-80"];

                            List<AlicorpGestionPreciosModelo> lAlicorpGestionPreciosModelo = new AlicorpGestionPrecios().Precio(new AlicorpGestionPreciosParametro()
                            {
                                opcion = 3,
                                parametro = parametro
                            });

                            foreach (AlicorpGestionPreciosModelo vAlicorpGestionPreciosModelo in lAlicorpGestionPreciosModelo)
                            {
                                oWs.Cells[Fila, 1].Value = vAlicorpGestionPreciosModelo.TrpId;
                                oWs.Cells[Fila, 2].Value = vAlicorpGestionPreciosModelo.OfiDescripcion;
                                oWs.Cells[Fila, 3].Value = vAlicorpGestionPreciosModelo.CatDescripcion;
                                oWs.Cells[Fila, 4].Value = vAlicorpGestionPreciosModelo.ComDescripcion;
                                oWs.Cells[Fila, 5].Value = vAlicorpGestionPreciosModelo.ProCodigo;
                                oWs.Cells[Fila, 6].Value = vAlicorpGestionPreciosModelo.ProDescripcion;
                                oWs.Cells[Fila, 7].Value = vAlicorpGestionPreciosModelo.PrecioMpm;
                                oWs.Cells[Fila, 8].Value = vAlicorpGestionPreciosModelo.PrecioDexMinimayorista;
                                oWs.Cells[Fila, 9].Value = vAlicorpGestionPreciosModelo.PrecioDexMinorista;
                                oWs.Cells[Fila, 10].Value = vAlicorpGestionPreciosModelo.PrecioPvp;

                                oWs.Cells[Fila, 7].Style.Locked = false;
                                oWs.Cells[Fila, 8].Style.Locked = false;
                                oWs.Cells[Fila, 9].Style.Locked = false;
                                oWs.Cells[Fila, 10].Style.Locked = false;

                                Fila++;
                            }

                            //oWs.View.FreezePanes(2, 1);

                            oWs.Column(1).Hidden = true;
                            oWs.Column(2).AutoFit();
                            oWs.Column(3).AutoFit();
                            oWs.Column(4).AutoFit();
                            oWs.Column(5).AutoFit();
                            oWs.Column(6).AutoFit();
                        }
                        #endregion

                        #region Descuento
                        if (Informe == "Descuento")
                        {
                            oWs = oEx.Workbook.Worksheets["Descuento"];

                            List<AlicorpGestionPreciosModelo> lAlicorpGestionPreciosModelo = new AlicorpGestionPrecios().Precio(new AlicorpGestionPreciosParametro()
                            {
                                opcion = 3,
                                parametro = parametro
                            }).Where(a => a.ComId == 1562).ToList();

                            foreach (AlicorpGestionPreciosModelo vAlicorpGestionPreciosModelo in lAlicorpGestionPreciosModelo)
                            {
                                oWs.Cells[Fila, 1].Value = vAlicorpGestionPreciosModelo.TrpId;
                                oWs.Cells[Fila, 2].Value = vAlicorpGestionPreciosModelo.OfiDescripcion;
                                oWs.Cells[Fila, 3].Value = vAlicorpGestionPreciosModelo.CatDescripcion;
                                oWs.Cells[Fila, 4].Value = vAlicorpGestionPreciosModelo.ComDescripcion;
                                oWs.Cells[Fila, 5].Value = vAlicorpGestionPreciosModelo.ProCodigo;
                                oWs.Cells[Fila, 6].Value = vAlicorpGestionPreciosModelo.ProDescripcion;
                                oWs.Cells[Fila, 7].Value = vAlicorpGestionPreciosModelo.PrecioMpm;
                                oWs.Cells[Fila, 12].Formula = "=1-(1-H" + Fila + ")*(1-I" + Fila + ")*(1-J" + Fila + ")*(1-K" + Fila + ")";

                                oWs.Cells[Fila, 8].Style.Locked = false;
                                oWs.Cells[Fila, 9].Style.Locked = false;
                                oWs.Cells[Fila, 10].Style.Locked = false;
                                oWs.Cells[Fila, 11].Style.Locked = false;

                                Fila++;
                            }

                            oWs.Column(1).Hidden = true;
                            oWs.Column(2).AutoFit();
                            oWs.Column(3).AutoFit();
                            oWs.Column(4).AutoFit();
                            oWs.Column(5).AutoFit();
                            oWs.Column(6).AutoFit();
                        }
                        #endregion

                        oWs.Protection.AllowInsertRows = false;
                        oWs.Protection.AllowInsertColumns = false;
                        oWs.Protection.AllowDeleteColumns = false;
                        oWs.Protection.AllowPivotTables = false;
                        oWs.Protection.AllowSelectLockedCells = true;
                        oWs.Protection.AllowSelectUnlockedCells = true;
                        oWs.Protection.AllowAutoFilter = true;
                        oWs.Protection.AllowSort = false;
                        oWs.Protection.IsProtected = true;
                        oWs.Protection.SetPassword(Contraseña);

                        oEx.Save();
                    }

                    return Content(new ContentResult { Content = "{ \"Archivo\": \"" + ArchivoNombre + "\" }", ContentType = "application/json", ContentEncoding = Encoding.UTF8 }.Content);
                    #endregion
                }
                else
                {
                    #region Carga de informacion
                    int FilaMaxima = 0;
                    HttpPostedFileBase Archivo = Request.Files["Archivo"];
                    List<AlicorpGestionPreciosModelo> lAlicorpGestionPreciosModelo = new List<AlicorpGestionPreciosModelo>();

                    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(Archivo.InputStream))
                    {
                        Excel.ExcelWorksheet oWs = null;

                        #region 20,21,80 y PVP
                        if (Informe == "202180")
                        {
                            oWs = oEx.Workbook.Worksheets["20-21-80"];

                            FilaMaxima = oWs.Dimension.End.Row;

                            for (int i = 2; i <= FilaMaxima; i++)
                            {
                                AlicorpGestionPreciosModelo oAlicorpGestionPreciosModelo = new AlicorpGestionPreciosModelo();

                                oAlicorpGestionPreciosModelo.TrpId = Convert.ToInt32(oWs.Cells[i, 1].Value);
                                oAlicorpGestionPreciosModelo.PrecioMpm = Convert.ToSingle(oWs.Cells[i, 7].Value);
                                oAlicorpGestionPreciosModelo.PrecioDexMinimayorista = Convert.ToSingle(oWs.Cells[i, 8].Value);
                                oAlicorpGestionPreciosModelo.PrecioDexMinorista = Convert.ToSingle(oWs.Cells[i, 9].Value);
                                oAlicorpGestionPreciosModelo.PrecioPvp = Convert.ToSingle(oWs.Cells[i, 10].Value);
                                oAlicorpGestionPreciosModelo.Archivo = ArchivoNombre;

                                lAlicorpGestionPreciosModelo.Add(oAlicorpGestionPreciosModelo);
                            }
                        }
                        #endregion

                        #region Descuento
                        if (Informe == "Descuento")
                        {
                            oWs = oEx.Workbook.Worksheets["Descuento"];

                            FilaMaxima = oWs.Dimension.End.Row;

                            for (int i = 2; i <= FilaMaxima; i++)
                            {
                                AlicorpGestionPreciosModelo oAlicorpGestionPreciosModelo = new AlicorpGestionPreciosModelo();

                                oAlicorpGestionPreciosModelo.TrpId = Convert.ToInt32(oWs.Cells[i, 1].Value);
                                oAlicorpGestionPreciosModelo.PrecioMpm = Convert.ToSingle(oWs.Cells[i, 7].Value);
                                oAlicorpGestionPreciosModelo.PrecioDexMinimayorista = Convert.ToSingle(oWs.Cells[i, 8].Value); // Descuento contado
                                oAlicorpGestionPreciosModelo.PrecioDexMinorista = Convert.ToSingle(oWs.Cells[i, 9].Value); // Descuento linealidad
                                oAlicorpGestionPreciosModelo.PrecioRvtMin = Convert.ToSingle(oWs.Cells[i, 10].Value); // Descuento rebate
                                oAlicorpGestionPreciosModelo.PrecioRvtMay = Convert.ToSingle(oWs.Cells[i, 11].Value); // Descuento bonificacion
                                oAlicorpGestionPreciosModelo.Archivo = ArchivoNombre;

                                lAlicorpGestionPreciosModelo.Add(oAlicorpGestionPreciosModelo);
                            }
                        }
                        #endregion

                        #region Bulkcopy
                        Resultado = new AlicorpGestionPrecios().BulkCopy(lAlicorpGestionPreciosModelo);

                        if (Resultado > 0)
                        {
                            new AlicorpGestionPrecios().Guarda(new AlicorpGestionPreciosParametro() { opcion = opcion, parametro = ArchivoNombre + "," + Informe });
                        }
                        #endregion
                    }

                    return Content(new ContentResult { Content = "{ \"Resultado\": " + Resultado + " }", ContentType = "application/json", ContentEncoding = Encoding.UTF8 }.Content);
                    #endregion
                }
                #endregion
            }
            else if (opcion == 18)
            {
                #region Informacion de descuentos
                int Resultado = 0;
                string Evento = Request["evento"];
                string ArchivoNombre = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);

                if (Evento == "Guarda")
                {
                    #region Guarda
                    List<string> lDescuentos = new List<string>(parametro.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries));

                    List<AlicorpGestionPreciosModelo> lAlicorpGestionPreciosModelo = new List<AlicorpGestionPreciosModelo>();
                    AlicorpGestionPreciosModelo oAlicorpGestionPreciosModelo = new AlicorpGestionPreciosModelo();

                    oAlicorpGestionPreciosModelo.TrpId = Convert.ToInt32(lDescuentos[0]); // TrpId
                    oAlicorpGestionPreciosModelo.PrecioMpm = Convert.ToSingle(lDescuentos[1]); // PrecioMpm
                    oAlicorpGestionPreciosModelo.PrecioDexMinimayorista = Convert.ToSingle(lDescuentos[2]); // Descuento contado
                    oAlicorpGestionPreciosModelo.PrecioDexMinorista = Convert.ToSingle(lDescuentos[3]); // Descuento linealidad
                    oAlicorpGestionPreciosModelo.PrecioRvtMin = Convert.ToSingle(lDescuentos[4]); // Descuento rebate
                    oAlicorpGestionPreciosModelo.PrecioRvtMay = Convert.ToSingle(lDescuentos[5]); // Descuento bonificacion
                    oAlicorpGestionPreciosModelo.Archivo = ArchivoNombre;

                    lAlicorpGestionPreciosModelo.Add(oAlicorpGestionPreciosModelo);

                    #region Bulkcopy
                    Resultado = new AlicorpGestionPrecios().BulkCopy(lAlicorpGestionPreciosModelo);

                    if (Resultado > 0)
                    {
                        new AlicorpGestionPrecios().Guarda(new AlicorpGestionPreciosParametro() { opcion = 14, parametro = ArchivoNombre + ",Descuento" });
                    }
                    #endregion

                    return Content(new ContentResult { Content = "{ \"Resultado\": " + Resultado + " }", ContentType = "application/json", ContentEncoding = Encoding.UTF8 }.Content);
                    #endregion
                } else {
                    #region Descuentos
                    return new ContentResult
                    {
                        Content = MvcApplication._Serialize(
                        new AlicorpGestionPrecios().Descuentos(new AlicorpGestionPreciosParametro()
                        {
                            opcion = opcion,
                            parametro = parametro
                        })),
                        ContentType = "application/json"
                    };
                    #endregion
                }
                #endregion
            }
            else if (opcion == 19)
            {
                #region Descarga Informe final
                //int Fila = 2;
                //int Columna = 1;
                //string ArchivoNombre = "";
                //string RutaTemporal = "";

                //ArchivoNombre = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);
                //RutaTemporal = Path.Combine(LocalTemp, ArchivoNombre);

                //#region Informacion
                //DataTable oDataTable = (DataTable)new Negocio.BL_GES_Operativa().BAlicorpGestionPrecios(new Entidad.AlicorpGestionPreciosParametro() { opcion = opcion, parametro = parametro });

                //if (oDataTable == null || oDataTable.Rows.Count == 0)
                //{
                //    return Content(new ContentResult { Content = "{ \"Archivo\": \"\", \"Mensaje\": \"No se encontro informacion.\" }", ContentType = "application/json", ContentEncoding = Encoding.UTF8 }.Content);
                //}
                //#endregion

                //#region Excel
                //using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(RutaTemporal)))
                //{
                //    Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Informe");

                //    #region Filas
                //    foreach (DataRow oDataRow in oDataTable.Rows) {
                //        Columna = 1;

                //        foreach (DataColumn oDataColumn in oDataTable.Columns)
                //        {
                //            #region Cabecera
                //            if (Fila == 2)
                //            {
                //                oWs.Cells[1, Columna].Value = oDataColumn.ColumnName;
                //            }
                //            #endregion

                //            oWs.Cells[Fila, Columna].Value = Convert.ToString(oDataRow[oDataColumn.ColumnName]);

                //            Columna++;
                //        }

                //        for (int i = 1; i <= oDataTable.Columns.Count; i++)
                //        {
                //            if (Fila == 2)
                //            {
                //               //Bordes Cabecera
                //                oWs.Cells[1, i].Style.Border.Top.Style = ExcelStyle.ExcelBorderStyle.Thin;
                //                oWs.Cells[1, i].Style.Border.Left.Style = ExcelStyle.ExcelBorderStyle.Thin;
                //                oWs.Cells[1, i].Style.Border.Right.Style = ExcelStyle.ExcelBorderStyle.Thin;
                //                oWs.Cells[1, i].Style.Border.Bottom.Style = ExcelStyle.ExcelBorderStyle.Thin;
                //                //Background color celeste
                //                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#B8CCE4");
                //                oWs.Cells[1, 1, 1, oDataTable.Columns.Count].Style.Fill.PatternType = ExcelStyle.ExcelFillStyle.Solid;
                //                oWs.Cells[1, 1, 1, oDataTable.Columns.Count].Style.Fill.BackgroundColor.SetColor(colFromHex);
                //            }
                //            //Bordes Cuerpo
                //            oWs.Cells[Fila, i].Style.Border.Top.Style = ExcelStyle.ExcelBorderStyle.Thin;
                //            oWs.Cells[Fila, i].Style.Border.Left.Style = ExcelStyle.ExcelBorderStyle.Thin;
                //            oWs.Cells[Fila, i].Style.Border.Right.Style = ExcelStyle.ExcelBorderStyle.Thin;
                //            oWs.Cells[Fila, i].Style.Border.Bottom.Style = ExcelStyle.ExcelBorderStyle.Thin;
                //        }

                //        Fila++;
                //    }
                //    #endregion

                //    #region Diseño de hoja
                //    //Negrita a la cabecera
                //    //oWs.Row(1).Style.Font.Bold = true;
                //    //Negrita a la cabecera
                //    oWs.SelectedRange[1, 1, 1, oDataTable.Columns.Count].Style.Font.Bold = true;
                //    //Filtros
                //    oWs.SelectedRange[1, 1, 1, oDataTable.Columns.Count].AutoFilter = true;
                //    //Alineado Vertialmente al medio
                //    oWs.SelectedRange[1, 1, 1, oDataTable.Columns.Count].Style.VerticalAlignment = ExcelStyle.ExcelVerticalAlignment.Center;

                //    for (int i = 1; i <= oDataTable.Columns.Count; i++)
                //    {
                //        //Autoajustable
                //        oWs.Column(i).AutoFit();
                //    }

                //    //Alto a la fila 1
                //    oWs.Row(1).Height = 26;
                //    #endregion

                //    oEx.Save();
                //}
                //#endregion
                string TextoResultado = new AlicorpGestionPrecios().DescargaVariable(new AlicorpGestionPreciosParametro() { opcion = opcion, parametro = parametro });
                return Content(new ContentResult { Content = "{ \"Archivo\": \"" + TextoResultado + "\" }", ContentType = "application/json", ContentEncoding = Encoding.UTF8 }.Content);
                #endregion
            }
            else
            {
                #region Filtros
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(
                        new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro()
                        {
                            opcion = opcion,
                            parametro = parametro
                        })),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-25
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Relevo()
        {
            Persona oPersona = ((Persona)Session["Session_Login"]);

            TempData["ListaPeriodo"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 2, parametro = Convert.ToString(oPersona.Person_id) + ",0" });

            ViewBag.PerId = oPersona.Person_id;
            ViewBag.PerNombre = oPersona.Person_Firtsname + " " + oPersona.Person_Surname;
            ViewBag.XploraTemp = XploraTemp;

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-13
        /// </summary>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult Relevo(string parametro) {
            string Evento = Request["evento"];
            string PerNombre = Request["nombre"];
            string Oficina = Request["oficina"];

            if (Evento == "Descarga")
            {
                #region Descarga archivo de relevo
                int Fila = 8;
                string ArchivoNombre = "";
                string RutaFormato = "";
                string RutaTemporal = "";

                Color ColorCell = ColorTranslator.FromHtml("#000000");
                Color ColorFondo = ColorTranslator.FromHtml("#FF0000");
                Color ColorTexto = ColorTranslator.FromHtml("#ffffff");

                ArchivoNombre = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);
                RutaFormato = Path.Combine(LocalFormat, "Lucky-Alicorp-Relevo.v1.2.xlsx");
                RutaTemporal = Path.Combine(LocalTemp, ArchivoNombre);

                System.IO.File.Copy(RutaFormato, RutaTemporal, true);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(RutaTemporal)))
                {
                    Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets["Relevo"];

                    List<AlicorpGestionPreciosModelo> lAlicorpGestionPreciosModelo = MvcApplication._Deserialize<List<AlicorpGestionPreciosModelo>>(parametro);

                    foreach (AlicorpGestionPreciosModelo oAlicorpGestionPreciosModelo in lAlicorpGestionPreciosModelo)
                    {
                        oWs.Cells[Fila, 2].Value = oAlicorpGestionPreciosModelo.TrpId;
                        oWs.Cells[Fila, 3].Value = oAlicorpGestionPreciosModelo.Chat;
                        oWs.Cells[Fila, 4].Value = oAlicorpGestionPreciosModelo.FasId;
                        oWs.Cells[Fila, 5].Value = oAlicorpGestionPreciosModelo.CatId;
                        oWs.Cells[Fila, 6].Value = oAlicorpGestionPreciosModelo.OfiId;
                        oWs.Cells[Fila, 7].Value = oAlicorpGestionPreciosModelo.OfiPerId;
                        oWs.Cells[Fila, 8].Value = oAlicorpGestionPreciosModelo.CatPerId;
                        oWs.Cells[Fila, 9].Value = oAlicorpGestionPreciosModelo.Estado;

                        oWs.Cells[Fila, 10].Value = oAlicorpGestionPreciosModelo.ComDescripcion;
                        oWs.Cells[Fila, 11].Value = oAlicorpGestionPreciosModelo.CatDescripcion;
                        oWs.Cells[Fila, 12].Value = oAlicorpGestionPreciosModelo.MarDescripcion;
                        oWs.Cells[Fila, 13].Value = oAlicorpGestionPreciosModelo.ProDescripcion;
                        oWs.Cells[Fila, 14].Value = oAlicorpGestionPreciosModelo.PrecioMpm;
                        oWs.Cells[Fila, 15].Value = oAlicorpGestionPreciosModelo.PrecioRvtMay;

                        #region ChatTexto
                        XDocument XDocumentChat = XDocument.Parse(oAlicorpGestionPreciosModelo.Chat);
                        var errors = (from e in XDocumentChat.Descendants("Chat")
                                      where (e.Element("ChatActivo").Value == "1")
                                      select new
                                      {
                                          ChatTexto = e.Element("ChatTexto").Value
                                      }).First();

                        oWs.Cells[Fila, 16].Value = errors.ChatTexto;
                        #endregion

                        if (oAlicorpGestionPreciosModelo.ComId == 1565)
                        {
                            oWs.Cells[Fila, 14].Style.Locked = false;
                        }
                        else 
                        {
                            oWs.Cells[Fila, 14].Style.Fill.PatternType = ExcelStyle.ExcelFillStyle.Solid;
                            oWs.Cells[Fila, 14].Style.Fill.BackgroundColor.SetColor(ColorFondo);
                        }
                        oWs.Cells[Fila, 15].Style.Locked = false;
                        oWs.Cells[Fila, 16].Style.Locked = false;

                        oWs.Cells[Fila, 17].Formula = "IF(J" + Fila + "=\"ALICORP\",IF(IF(AND(O" + Fila + "<>\"\",O" + Fila + "<>0),1,0)+IF(    P" + Fila + "<>\"\"                  ,1,0)=0,0,1),IF(IF(AND(N" + Fila + "<>\"\",O" + Fila + "<>\"\"),1,0)+IF(AND(O" + Fila + "<>\"\",P" + Fila + "<>\"\"),1,0)+IF(AND(N" + Fila + "<>\"\",P" + Fila + "<>\"\"),1,0)+IF(P" + Fila + "<>\"\",1,0)+IF(AND(N" + Fila + "<>\"\",O" + Fila + "<>\"\",P" + Fila + "<>\"\"),1,0)=0,0,1))";

                        Fila++;
                    }

                    oWs.Cells[4, 16].Formula = "=SUM($Q$8:$Q$" + Fila + ")/COUNTA($M$8:$M$" + Fila + ")";

                    oWs.Cells[4, 11].Value = PerNombre;
                    oWs.Cells[5, 11].Value = Oficina;

                    oWs.Column(2).Hidden = true;
                    oWs.Column(3).Hidden = true;
                    oWs.Column(4).Hidden = true;
                    oWs.Column(5).Hidden = true;
                    oWs.Column(6).Hidden = true;
                    oWs.Column(7).Hidden = true;
                    oWs.Column(8).Hidden = true;
                    oWs.Column(9).Hidden = true;
                    oWs.Column(17).Hidden = true;

                    oWs.Column(10).AutoFit();
                    oWs.Column(11).AutoFit();
                    oWs.Column(12).AutoFit();
                    oWs.Column(13).AutoFit();

                    oWs.Protection.AllowInsertRows = false;
                    oWs.Protection.AllowInsertColumns = false;
                    oWs.Protection.AllowDeleteColumns = false;
                    oWs.Protection.AllowPivotTables = false;
                    oWs.Protection.AllowSelectLockedCells = true;
                    oWs.Protection.AllowSelectUnlockedCells = true;
                    oWs.Protection.AllowAutoFilter = true;
                    oWs.Protection.AllowSort = false;
                    oWs.Protection.IsProtected = true;
                    oWs.Protection.SetPassword(Contraseña);

                    oEx.Save();
                }

                return new ContentResult
                {
                    Content = "{ \"Archivo\": \"" + ArchivoNombre + "\" }",
                    ContentType = "application/json"
                };
                #endregion
            }
            else if (Evento == "Carga")
            {
                #region Carga archivo de relevo
                int Fila = 8;
                int Resultado = 0;

                HttpPostedFileBase Archivo = Request.Files["archivo"];
                List<AlicorpGestionPreciosModelo> lAlicorpGestionPreciosModelo = new List<AlicorpGestionPreciosModelo>();

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(Archivo.InputStream))
                {
                    #region Excel
                    Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets["Relevo"];

                    while (Convert.ToString(oWs.Cells[Fila, 2].Value).Length > 0)
                    {
                        AlicorpGestionPreciosModelo oAlicorpGestionPreciosModelo = new AlicorpGestionPreciosModelo();

                        oAlicorpGestionPreciosModelo.TrpId = Convert.ToInt32(oWs.Cells[Fila, 2].Value);
                        oAlicorpGestionPreciosModelo.Chat = Convert.ToString(oWs.Cells[Fila, 3].Value);
                        oAlicorpGestionPreciosModelo.FasId = Convert.ToInt32(oWs.Cells[Fila, 4].Value);
                        oAlicorpGestionPreciosModelo.CatId = Convert.ToInt32(oWs.Cells[Fila, 5].Value);
                        oAlicorpGestionPreciosModelo.OfiId = Convert.ToInt32(oWs.Cells[Fila, 6].Value);
                        oAlicorpGestionPreciosModelo.OfiPerId = Convert.ToInt32(oWs.Cells[Fila, 7].Value);
                        oAlicorpGestionPreciosModelo.CatPerId = Convert.ToInt32(oWs.Cells[Fila, 8].Value);
                        oAlicorpGestionPreciosModelo.Estado = Convert.ToInt32(oWs.Cells[Fila, 9].Value);
                        oAlicorpGestionPreciosModelo.PrecioMpm = Convert.ToSingle(oWs.Cells[Fila, 14].Value);
                        oAlicorpGestionPreciosModelo.PrecioRvtMay = Convert.ToSingle(oWs.Cells[Fila, 15].Value);

                        #region ChatTexto
                        XDocument XDocumentChat = XDocument.Parse(oAlicorpGestionPreciosModelo.Chat);

                        foreach (XElement XElementItem in (from a in XDocumentChat.Descendants("Chat")
                                                           where a.Element("ChatActivo").Value == "1"
                                                           select a))
                        {
                            XElementItem.SetElementValue("ChatTexto", Convert.ToString(oWs.Cells[Fila, 16].Value));
                        }

                        oAlicorpGestionPreciosModelo.Chat = XDocumentChat.Root.ToString();

                        XDocumentChat = null;
                        #endregion

                        lAlicorpGestionPreciosModelo.Add(oAlicorpGestionPreciosModelo);

                        Fila++;
                    }

                    if (lAlicorpGestionPreciosModelo.Count > 0)
                    {
                        Resultado = new AlicorpGestionPrecios().GuardaEnvia(new AlicorpGestionPreciosParametro() { opcion = 4, parametro = MvcApplication._Serialize(lAlicorpGestionPreciosModelo), comentario = "Guarda" });
                    }
                    #endregion
                }

                return Content(new ContentResult { Content = "{ \"Resultado\": " + Resultado + " }", ContentType = "application/json", ContentEncoding = Encoding.UTF8 }.Content);
                #endregion
            }

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-02
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Validacion() {
            Persona oPersona = ((Persona)Session["Session_Login"]);

            TempData["TempPersona"] = oPersona;
            TempData["TempPeriodo"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 2, parametro = "0," + Convert.ToString(oPersona.Person_id) });

            ViewBag.XploraTemp = XploraTemp;

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-04-28
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Validacion(int opcion, string parametro)
        {
            int Fila = 2;
            string ArchivoNombre = "";
            string ArchivoRuta = "";

            if (opcion == 1)
            {
                #region Descarga de validacion
                List<AlicorpGestionPreciosModelo> lAlicorpGestionPreciosModelo = MvcApplication._Deserialize<List<AlicorpGestionPreciosModelo>>(parametro);

                ArchivoNombre = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);
                ArchivoRuta = Path.Combine(LocalTemp, ArchivoNombre);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(ArchivoRuta)))
                {
                    Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Validacion");

                    oWs.Cells[1, 1].Value = "Oficina";
                    oWs.Cells[1, 2].Value = "Material";
                    oWs.Cells[1, 3].Value = "Mejor Precio";
                    oWs.Cells[1, 4].Value = "Precio reventa";
                    oWs.Cells[1, 5].Value = "Margen Mayorista";
                    oWs.Cells[1, 6].Value = "Precio Dex";
                    oWs.Cells[1, 7].Value = "Comentarios";
                    oWs.Cells[1, 8].Value = "Estado";

                    foreach (AlicorpGestionPreciosModelo oBj in lAlicorpGestionPreciosModelo)
                    {
                        oWs.Cells[Fila, 1].Value = oBj.OfiDescripcion;
                        oWs.Cells[Fila, 2].Value = oBj.ProDescripcion;
                        oWs.Cells[Fila, 3].Value = oBj.PrecioMpm;
                        oWs.Cells[Fila, 4].Value = oBj.PrecioRvtMay;
                        oWs.Cells[Fila, 5].Value = oBj.MargenMayorista;
                        oWs.Cells[Fila, 6].Value = oBj.PrecioDexMinorista;
                        oWs.Cells[Fila, 7].Value = oBj.Chat;
                        if (oBj.Estado == 1)
                        {
                            oWs.Cells[Fila, 8].Value = "Validado";
                        }
                        else
                        {
                            oWs.Cells[Fila, 8].Value = "Invalidado";
                        }

                        Fila++;
                    }

                    oWs.Row(1).Style.WrapText = true;
                    oWs.Row(1).Style.Font.Bold = true;

                    oWs.Row(1).Style.HorizontalAlignment = ExcelStyle.ExcelHorizontalAlignment.Center;
                    oWs.Row(1).Style.VerticalAlignment = ExcelStyle.ExcelVerticalAlignment.Center;

                    oWs.Column(1).Style.Font.Bold = true;
                    oWs.Column(1).AutoFit();
                    oWs.Column(2).Style.Font.Bold = true;
                    oWs.Column(2).AutoFit();
                    oWs.Column(3).Style.HorizontalAlignment = ExcelStyle.ExcelHorizontalAlignment.Right;
                    oWs.Column(3).AutoFit();
                    oWs.Column(4).Style.HorizontalAlignment = ExcelStyle.ExcelHorizontalAlignment.Right;
                    oWs.Column(4).AutoFit();
                    oWs.Column(5).Style.HorizontalAlignment = ExcelStyle.ExcelHorizontalAlignment.Right;
                    oWs.Column(5).AutoFit();
                    oWs.Column(6).Style.HorizontalAlignment = ExcelStyle.ExcelHorizontalAlignment.Right;
                    oWs.Column(6).AutoFit();
                    oWs.Column(7).AutoFit();
                    oWs.Column(8).AutoFit();

                    oEx.Save();
                }

                return Content(new ContentResult { Content = "{ \"Archivo\": \"" + ArchivoNombre + "\" }", ContentType = "application/json", ContentEncoding = Encoding.UTF8 }.Content);
                #endregion
            }

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-03
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Subsanacion()
        {
            Persona oPersona = ((Persona)Session["Session_Login"]);

            TempData["TempPersona"] = oPersona;
            TempData["TempPeriodo"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 2, parametro = Convert.ToString(oPersona.Person_id) + ",0" });

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-06
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult InformeFinal() {
            Persona oPersona = ((Persona)Session["Session_Login"]);

            TempData["ListaCadena"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 6 });
            TempData["ListaPeriodo"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 2, parametro = "0,0" });
            TempData["ListaGrupoCategoria"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 7 });

            ViewBag.PerId = oPersona.Person_id;
            ViewBag.XploraTemp = XploraTemp;
            ViewBag.Xplora = Xplora;

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-08
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Panel() {
            TempData["ListaPeriodo"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 2, parametro = "0,0" });
            TempData["ListaGrupoCategoria"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 7 });

            //List<AlicorpGestionPreciosVisorStatusNegocio> lAlicorpGestionPreciosVisorStatusNegocio = new AlicorpGestionPrecios().VisorStatus(new AlicorpGestionPreciosParametro() { opcion = 12 });

            //TempData["VisorStatus"] = lAlicorpGestionPreciosVisorStatusNegocio;

            ViewBag.Xplora = Xplora;
            ViewBag.XploraTemp = XploraTemp;

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-14
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Ranking() {
            TempData["ListaGrupoCategoria"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 7 });

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-20
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Carga() {
            TempData["ListaPeriodo"] = new AlicorpGestionPrecios().Filtro(new AlicorpGestionPreciosParametro() { opcion = 2, parametro = "0,0" });

            ViewBag.XploraTemp = XploraTemp;

            return View();
        }
        #endregion

        #region AASS - Lavanderia
        /// <summary>
        /// Autor: dpastor
        /// Fecha: 2017-03-09
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Lavanderia() {
            List<AlicorpAASSLavanderiaSelecciona> lAnios = new AlicorpAASSLavanderia().Selecciona(new AlicorpAASSLavanderiaParametro() { opcion = 1 });
            List<AlicorpAASSLavanderiaSelecciona> lMes = new AlicorpAASSLavanderia().Selecciona(new AlicorpAASSLavanderiaParametro() { opcion = 2, parametro = (lAnios.Count == 0 ? Convert.ToString(DateTime.Now.Year) : (lAnios.Where(a => a.SelActivo == 1).Select(a => a.SelCodigo).First())) });

            TempData["ListaAnios"] = lAnios;
            TempData["ListaMes"] = lMes;

            ViewBag.XploraFoto = XploraFoto;

            return View();
        }

        /// <summary>
        /// Autor: dpastor
        /// Fecha: 2017-03-09
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Lavanderia(string p) {
            AlicorpAASSLavanderiaParametro oParametro = MvcApplication._Deserialize<AlicorpAASSLavanderiaParametro>(p);

            if (oParametro.opcion == 0)
            {
                #region Informacion lavanderia
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new AlicorpAASSLavanderia().Lavanderia(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else
            {
                #region Listados
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new AlicorpAASSLavanderia().Selecciona(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
        }
        #endregion

        #region Proyecto 5
        public ActionResult Proyecto5() {
            ViewBag.Xplora = Xplora;
            ViewBag.XploraFoto = XploraFoto;
            ViewBag.XploraTemp = XploraTemp;

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-08
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Descarga(string __a)
        {
            Int32 _fila = 2;
            string _fileServer = "";
            string _filePath = "";
            string _FileRuta = "";


            if (__a != "")
            {
                List<AlicorpInteligenciaComercial> oLs = MvcApplication._Deserialize<List<AlicorpInteligenciaComercial>>(__a);

                _fileServer = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = Path.Combine(LocalTemp, _fileServer);
                _FileRuta = Convert.ToString(ConfigurationManager.AppSettings["LocalFoto"]);

                //https://www.xplora.com.pe/foto/fotoandroid/' +  + '.jpg
                /*instancia archivo para su uso*/

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    // ===================================================================
                    // crea hoja de trabajo
                    Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Inteligencia Comercial");
                    #region Cabecera
                    // ===================================================================
                    // registro de cabecera
                    oWs.Cells["A1:L1"].Style.Font.Bold = true;
                    oWs.Cells["A1:L1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    Color color = System.Drawing.ColorTranslator.FromHtml("#084B8A");
                    oWs.Cells["A1:L1"].Style.Fill.BackgroundColor.SetColor(color);
                    oWs.Cells["A1:L1"].Style.Font.Color.SetColor(Color.White);

                    oWs.Cells["A1:L1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWs.Cells["A1:L1"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWs.Cells["A1:L1"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWs.Cells["A1:L1"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                    oWs.Cells[1, 1].Value = "CATEGORIA";
                    oWs.Cells[1, 2].Value = "OFICINA";
                    oWs.Cells[1, 3].Value = "CANAL";
                    oWs.Cells[1, 4].Value = "CADENA";
                    oWs.Cells[1, 5].Value = "EMPRESA";
                    oWs.Cells[1, 6].Value = "MARCA";
                    oWs.Cells[1, 7].Value = "ACTIVIDAD";
                    oWs.Cells[1, 8].Value = "DESCRIPCION";
                    oWs.Cells[1, 9].Value = "PRECIO COMPETENCIA";
                    oWs.Cells[1, 10].Value = "PRECIO ALICORP";
                    oWs.Cells[1, 11].Value = "FOTOS";
                    oWs.Cells[1, 12].Value = "COMENTARIOS";

                    #endregion
                    //bool imagenEstado = true;
                    foreach (var oBj in (from a in oLs where a.valida == true select a))
                    {
                        #region Contenido
                        oWs.Cells[_fila, 1].Value = oBj.cat_descripcion.ToUpper();
                        oWs.Cells[_fila, 2].Value = oBj.oficina.ToUpper();
                        oWs.Cells[_fila, 3].Value = oBj.can_descripcion.ToUpper();
                        oWs.Cells[_fila, 4].Value = oBj.mercado.ToUpper();

                        #region Img Empresas
                        oWs.Row(_fila).Height = 270.00D;
                        oWs.Column(5).Width = 30;
                        if (Convert.ToString(oBj.empresa).IndexOf(".jpg") == -1)
                        {
                            oWs.Cells[_fila, 5].Value = oBj.empresa.ToUpper();
                        }
                        else
                        {
                            string _fileFoto = "";
                            _fileFoto = Path.Combine(_FileRuta, @"c1562\813711382010\Media\Imagen\Compania", oBj.empresa);
                            //_fileFoto = Path.Combine(_FileRuta, "FotoAndroid", oBj.empresa);
                            using (Image img = Image.FromFile(_fileFoto))
                            {
                                int w = 400;
                                int h = 220;

                                if (img != null)
                                {
                                    var picture = oWs.Drawings.AddPicture(oBj.id_rcompe.ToString(), img);

                                    picture.From.Column = 4;
                                    picture.From.Row = _fila - 1;
                                    picture.To.Column = 4;
                                    picture.To.Row = _fila - 1;
                                    picture.SetSize(Convert.ToInt32(w * 0.4), Convert.ToInt32(h * 0.4));
                                    picture.SetPosition(_fila - 1, 10, 4, 10);
                                    picture.EditAs = eEditAs.TwoCell;
                                }
                            }
                        }
                        #endregion

                        oWs.Cells[_fila, 6].Value = oBj.marca.ToUpper();
                        oWs.Cells[_fila, 7].Value = oBj.actividad.ToUpper();
                        //Quitar <div> de la descripcion
                        string a = oBj.descripcion_actividad.Replace("<div>", "");
                        string b = a.Replace("</div>", "");
                        string c = b.Replace("<br>", "");
                        oWs.Cells[_fila, 8].Value = c;
                        //
                        oWs.Cells[_fila, 9].Value = oBj.precio_competencia.Replace(",", Environment.NewLine);
                        oWs.Cells[_fila, 9].Style.WrapText = true;
                        oWs.Cells[_fila, 10].Value = oBj.precio_alicorp.Replace(",", Environment.NewLine);
                        oWs.Cells[_fila, 10].Style.WrapText = true;

                        #region Fotos
                        //Imagenes
                        oWs.Column(11).Width = 235.00D;
                        string[] fotos = (oBj.nombre_foto.Length == 0 ? null : oBj.nombre_foto.Split(','));
                        int fila_alto = 0;
                        int columna_ancho = 0;
                        double espacio = 0;
                        if (fotos == null)
                        {
                            oWs.Cells[_fila, 11].Value = "";
                        }
                        else
                        {
                            for (int i = 0; i < fotos.Length; i++)
                            {
                                string _fileFoto = "";

                                _fileFoto = Path.Combine(_FileRuta, "FotoAndroid", fotos[i] + ".jpg");


                                //if (imagenEstado == true)
                                //{
                                //    _fileFoto = Path.Combine(_FileRuta, "FotoAndroid", "20170317095233" + ".jpg");
                                //    imagenEstado = false;
                                //}
                                //else
                                //{
                                //    _fileFoto = Path.Combine(_FileRuta, "FotoAndroid", "201732310172689721327" + ".jpg");
                                //    imagenEstado = true;
                                //}
                                bool fileexist = System.IO.File.Exists(_fileFoto);
                                if (fileexist == true)
                                {
                                    using (Image img = Image.FromFile(_fileFoto))
                                    {
                                        int w = img.Width;
                                        int h = img.Height;
                                        

                                        if (img != null)
                                        {
                                            var Foto = oWs.Drawings.AddPicture(_fila.ToString() + i, img);
                                            Foto.From.Column = 10;
                                            Foto.From.Row = _fila - 1;
                                            Foto.To.Column = 10;
                                            Foto.To.Row = _fila - 1;
                                            if (w < h)
                                            {
                                                #region Verticales
                                                if ((w > 91 && h > 232) || (w > 91 && h < 232) || (w < 91 && h > 232) || (w < 91 && h < 232))
                                                {
                                                    
                                                    w = 505; h = 685;
                                                    Foto.SetSize(Convert.ToInt32(w * 0.5), Convert.ToInt32(h * 0.5));
                                                    Foto.SetPosition(_fila - 1, 10, 10, Convert.ToInt32((espacio * 0.6)));
                                                    Foto.EditAs = eEditAs.TwoCell;
                                                    columna_ancho = columna_ancho + w;
                                                    espacio += 505;
                                                }
                                                #endregion
                                            }
                                            else if (w > h)
                                            {
                                                #region Horizonales
                                                if ((w > 78 && h > 135) || (w > 78 && h < 135) || (w < 78 && h > 135) || (w < 78 && h < 135))
                                                {
                                                    
                                                    w = 770; h = 600;
                                                    Foto.SetSize(Convert.ToInt32(w * 0.5), Convert.ToInt32(h * 0.5));
                                                    Foto.SetPosition(_fila - 1, 10, 10, Convert.ToInt32((espacio * 0.6)));
                                                    Foto.EditAs = eEditAs.TwoCell;
                                                    //fila_alto = fila_alto + h;
                                                    espacio += 730;
                                                }
                                                #endregion
                                            }
                                        }
                                        fila_alto = fila_alto + h;
                                    }
                                }
                            }
                        #endregion
                        }
                        oWs.Cells[_fila, 12].Value = oBj.observacion;
                        //oWs.Row(_fila).Height = 75;

                        #region DiseñoContenido
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.Border.Top.Color.SetColor(Color.Gray);
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.Border.Left.Color.SetColor(Color.Gray);
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.Border.Right.Color.SetColor(Color.Gray);
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.Border.Bottom.Color.SetColor(Color.Gray);
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                        oWs.Cells["A" + _fila.ToString() + ":L" + _fila.ToString()].Style.Font.Size = 15;
                        #endregion

                        _fila++;
                        #endregion
                    }

                    oWs.Row(1).Style.WrapText = true;
                    oWs.Row(1).Style.Font.Bold = true;
                    oWs.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWs.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    oWs.Column(1).Width = 20;
                    oWs.Column(2).AutoFit();
                    oWs.Column(3).Width = 24;
                    oWs.Column(3).Style.WrapText = true;
                    oWs.Column(4).Width = 24;
                    oWs.Column(4).Style.WrapText = true;
                    oWs.Column(5).Width = 25;
                    oWs.Column(5).Style.WrapText = true;
                    oWs.Column(6).Width = 18;
                    oWs.Column(6).Style.WrapText = true;
                    oWs.Column(7).Width = 25;
                    oWs.Column(7).Style.WrapText = true;
                    oWs.Column(8).Width = 38;
                    oWs.Column(8).Style.WrapText = true;
                    oWs.Column(9).Width = 21;
                    oWs.Column(10).Width = 22;
                    //oWs.Column(11).Width = 50;
                    oWs.Column(12).Width = 48;
                    oWs.Column(12).Style.WrapText = true;
                    oWs.View.ZoomScale = 60;
                    oEx.Save();
                }
            }

            return Json(new { Archivo = _fileServer });
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-08
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Mensaje(string __a)
        {
            Int32 estado = 0;

            if (__a != "" && __a != null)
            {
                List<AlicorpInteligenciaComercial> oLs = MvcApplication._Deserialize<List<AlicorpInteligenciaComercial>>(__a);
                List<AlicorpInteligenciaComercial> oLo = new List<AlicorpInteligenciaComercial>();

                foreach (var oBj in (from a in oLs select new { a.cat_descripcion, a.oficina, a.can_descripcion, a.mercado, a.empresa, a.marca, a.actividad, a.descripcion_actividad, a.precio_competencia, a.precio_alicorp, a.nombre_foto, a.observacion, a.id_rcompe }))
                {
                    oLo.Add(
                            new AlicorpInteligenciaComercial()
                            {
                                id_rcompe = oBj.id_rcompe,
                                cat_descripcion = oBj.cat_descripcion,
                                oficina = oBj.oficina,
                                can_descripcion = oBj.can_descripcion,
                                mercado = oBj.mercado,
                                empresa = oBj.empresa,
                                marca = oBj.marca,
                                actividad = oBj.actividad,
                                descripcion_actividad = oBj.descripcion_actividad,
                                precio_competencia = oBj.precio_competencia,
                                precio_alicorp = oBj.precio_alicorp,
                                nombre_foto = oBj.nombre_foto,
                                observacion = oBj.observacion
                            }
                        );
                }

                estado = new AlicorpInteligenciaComercial().Mensaje(
                        new Request_Alicorp_Inteligencia_Mensaje()
                        {
                            Lista = oLo
                        }
                    );
            }

            return Json(new { Estado = estado });
        }

        public ActionResult GetFiltros(int __a, string __b)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpInteligenciaComercial().GetFiltros(
                new Request_Alicorp_InteligenciaComer_Filtro() { op = __a, parametros = __b })),
                ContentType = "application/json"
            };
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase archivo)
        {
            string archivoNombre;
            string pathArchivo;
            //string TipoArchivo = Convert.ToString(Request["TipoEnvio"]);

            archivoNombre = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now) + Path.GetExtension(archivo.FileName);

            pathArchivo = Path.Combine(LocalFoto, "FotoAndroid", archivoNombre);

            try
            {
                #region Crea archivo
                archivo.SaveAs(pathArchivo);
                #endregion

                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"" + archivoNombre + "\", \"_b\":true,\"_c\":\"Ok\" }",
                    ContentType = "application/json"
                }.Content);
            }
            catch (Exception e)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"\", \"_b\":false,\"_c\":\"" + e.Message + "\" }",
                    ContentType = "application/json"
                }.Content);
            }
        }

        /// <summary>
        /// Autor: dpastor
        /// Fecha: 2017-04-26
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Mantenimiento_Proyecto5(int _a, string _b)
        {
            if (_a == 0 || _a == 1 || _a == 2 || _a == 3 || _a == 4 || _a == 5 || _a == 6)
            {
                return new ContentResult
                {
                    Content = MvcApplication._Serialize(new AlicorpInteligenciaComercial().Proyecto5_Mantenimiento(
                    new Request_Proyecto5_Parametros() { Opcion = _a, Parametros = _b })),
                    ContentType = "application/json"
                };
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}