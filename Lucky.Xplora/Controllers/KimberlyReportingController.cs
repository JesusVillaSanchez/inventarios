﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Kimberly;
using Lucky.Xplora.Models.NwRepStdAASS;
using Lucky.Xplora.Models.Aje;
using Lucky.Xplora.Models.Kimberly.Reporting;
using Lucky.Xplora.Models.AlicorpMulticategoriaCanje;
using Lucky.Xplora.Security;
using System.IO;
using System.Configuration;
using OfficeOpenXml;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using Microsoft.Office.Interop.PowerPoint;

namespace Lucky.Xplora.Controllers
{
    public class KimberlyReportingController : Controller
    {
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

        #region Kimberly - Conecta App
        #region Kimberly - Panel
        /// <summary>
        /// Autor:Aldo darrigo
        /// Fecha:28/09/2016
        /// </summary>
        /// <returns></returns> 
        [HttpGet]
        public ActionResult Panel()
        {
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-02-01
        /// </summary>
        /// <returns></returns> 
        [HttpPost]
        public ActionResult Panel(int opcion, string parametro)
        {
            if (opcion == 100) {
                return View();
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(
                        new Trade_Marketing().Filtro(new Parametro() { 
                            opcion = opcion, 
                            parametro = parametro
                        })),
                    ContentType = "application/json"
                }.Content);
            }
        }
        #endregion

        #region Kimberly - Actividades del Mes
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-01-27
        /// </summary>
        /// <returns></returns>}
        [HttpGet]
        [CustomAuthorize]
        public ActionResult TradeMarketing()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-01-27
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize]
        public ActionResult TradeMarketing(string __a, string __b) {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Trade_Marketing().Lista(new Parametro()
                    {
                        opcion = Convert.ToInt32(__a),
                        parametro = __b
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-01-27
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Marketing()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-30
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Marketing(string __a, string __b)
        {
            //return Content(new ContentResult
            //{
            //    Content = MvcApplication._Serialize(new Trade_Marketing().Marketing(new Parametro()
            //    {
            //        opcion = Convert.ToInt32(__a),
            //        parametro = __b
            //    })),
            //    ContentType = "application/json"
            //}.Content);
            return View(new Trade_Marketing().Marketing(new Parametro() { opcion = Convert.ToInt32(__a), parametro = __b }));
        }
        #endregion

        #region Kimberly - Precios y Condiciones
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-02-02
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult PrecioCondicion() {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-03-03
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize]
        public ActionResult PrecioCondicion(string __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Trade_Marketing().PrecioCondicion(new Parametro()
                {
                    opcion = Convert.ToInt32(__a),
                    parametro = __b
                })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-26
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Precio()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }
        #endregion

        #region Kimberly - Prioridad
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-02-02
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Prioridad() {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        #endregion

        #region Kimberly - Foto de exito
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-02-02
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult FotoExito()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        #endregion

        #region Kimberly - Drivers Fuerza de Venta
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-02-02
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult DriverFuerzaVenta()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        #endregion

        #region Kimberly - Catologo de productos
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-02-02
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult CatalogoProducto()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        #endregion

        #region Kimberly - Manuales
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-03-02
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Manual()
        {
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            return View();
        }
        #endregion

        #region Kimberly - Objetivo del Mes
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-18
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Objetivo()
        {
            Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-08-17
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize]
        public ActionResult Objetivo(string __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new ObjetivoVenta().Lista(new Parametro()
                {
                    opcion = Convert.ToInt32(__a),
                    parametro = __b
                })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-08-18
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult ObjetivoCarga()
        {
            Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-08-18
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize]
        public ActionResult ObjetivoCarga(FormCollection formCollection)
        {
            //string anio = Request.Form["_anio"];
            //string mes = Request.Form["_mes"];
            DateTime fecha;
            HttpPostedFileBase FileBaseExcel = Request.Files["archivo"];

            if (FileBaseExcel == null)
            {
                return View();
            }

            if ((FileBaseExcel != null) && (FileBaseExcel.ContentLength > 0) && !string.IsNullOrEmpty(FileBaseExcel.FileName)) {
                List<ObjetivoMes> lObjetivo = new List<ObjetivoMes>();
                fecha = Convert.ToDateTime(Request.Form["fecha"]);

                using (var oPackage = new ExcelPackage(FileBaseExcel.InputStream))
                {
                    var oSheets = oPackage.Workbook.Worksheets;
                    var oSheet = oSheets["base"];
                    int Ultimafila = oSheet.Dimension.End.Row;

                    for (int i = 2; i <= Ultimafila; i++)
                    {
                        ObjetivoMes oObjetivo = new ObjetivoMes();

                        oObjetivo.anio = fecha.Year;
                        oObjetivo.mes = fecha.Month;
                        oObjetivo.dia = fecha.Day;
                        oObjetivo.canal = Convert.ToString(oSheet.Cells[i, 1].Value);
                        oObjetivo.desc_gba_ic = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 2].Value).Trim());         //GBA//
                        oObjetivo.desc_gba2_ic = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 3].Value).Trim());        //KAM//
                        oObjetivo.desc_rep_ship_to = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 4].Value).Trim());    //Ejecutivo
                        oObjetivo.tipo_cliente = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 5].Value).Trim());        //Tipo Cliente
                        oObjetivo.cliente_pareto = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 6].Value).Trim());      //Cliente Pareto
                        oObjetivo.business_category = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 7].Value).Trim());  //Categoria
                        oObjetivo.desc_business_category = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 7].Value).Trim());//Subcategoria
                        oObjetivo.cust_ship_to = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 8].Value).Trim());         //Ship to
                        oObjetivo.desc_ship_to = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 9].Value).Trim());        //Desc Ship to
                        oObjetivo.cust_sold_to = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 10].Value).Trim());        //Sold to
                        oObjetivo.desc_sold_to = MvcApplication.Replace(Convert.ToString(oSheet.Cells[i, 11].Value).Trim());       //Cliente
                        oObjetivo.projected_net_sales_cs = (oSheet.Cells[i, 14].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 14].Value).Trim()); //Projected Net Sales CS
                        oObjetivo.projected_net_sales_gsu = (oSheet.Cells[i, 15].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 15].Value).Trim());//Projected Net Sales GSU
                        oObjetivo.projected_net_sales_usd = (oSheet.Cells[i, 16].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 16].Value).Trim());//Projected Net Sales USD
                        oObjetivo.forecast_cs = (oSheet.Cells[i, 17].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 17].Value).Trim()); //Forecast CS
                        oObjetivo.forecast_gsu = (oSheet.Cells[i, 18].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 18].Value).Trim());//Forecast GSU
                        oObjetivo.forecast_usd = (oSheet.Cells[i, 19].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 19].Value).Trim());//Forecast USD
                        oObjetivo.ns_cs = (oSheet.Cells[i, 20].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 20].Value).Trim());       //NS-CS    
                        oObjetivo.o_cs = (oSheet.Cells[i, 21].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 21].Value).Trim());        //O-CS
                        oObjetivo.niv_usd = (oSheet.Cells[i, 22].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 22].Value).Trim());     //NIV-USD
                        oObjetivo.niv_lc = (oSheet.Cells[i, 23].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 23].Value).Trim());      //NIV-LC
                        oObjetivo.forecast_lc = (oSheet.Cells[i, 24].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 24].Value).Trim()); //FORECAST-LC
                        oObjetivo.forecast_niv = (oSheet.Cells[i, 25].Value == null ? "0" : Convert.ToString(oSheet.Cells[i, 25].Value).Trim()); //FORECAST-NIV


                        oObjetivo.sales_gba_ship_to = "";//En caso no se encuentre en "CASE"
                        oObjetivo.desc_gba = ""; //En caso no se encuentre en "CASE"

                        if (oObjetivo.desc_gba2_ic == "DTT 2 - NORTE" || oObjetivo.desc_gba2_ic == "DTT 3 - SUR" || oObjetivo.desc_gba2_ic == "DTT 4 - C. ORIENTE" || oObjetivo.desc_gba2_ic == "DTT 1 -LIMA")
                        {
                            oObjetivo.desc_gba2_ic = oObjetivo.desc_gba2_ic + ".";
                        }

                        oObjetivo.sales_rep_ship_to = "";
                        oObjetivo.business_sector = "";
                        oObjetivo.desc_business_sector = "";
                        oObjetivo.fec_hora = DateTime.Now;

                        lObjetivo.Add(oObjetivo);
                    }
                }

                //ViewBag.fecha = fecha; // Fecha mm-dd-yyyy
                ViewBag.fec_hora = DateTime.Now.ToString("dd/MM/yy hh:mm");

                int respuesta = new ObjetivoVenta().Volcado(new Parametro() { opcion = 9, parametro = MvcApplication._Serialize(lObjetivo) });

                return View(lObjetivo);
            }

            return View();
        }
        #endregion

        #region Kimberly - Agrenda de Evolucion
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-23
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Agenda()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }
        #endregion

        #region Kimberly - Mecanica
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-24
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Mecanica()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }
        #endregion

        #region Kimberly - Innovacion
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-05
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Innovacion()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }
        #endregion
        #endregion

        #region Kimberly - Reporting
        /// <summary>
        /// Autor: ¿?
        /// Fecha: 2017-02-06
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Reporting() {
            return View();
        }
        public ActionResult GetFiltro(string _a, int _b)
        {
            return Json(new { response = new NwRep_Kc_AASS().Get_Filtro(new Request_NWRepStd_General { Parametros = _a, op = _b }) });
        }
        #region EEAA
        public ActionResult KrEEAA() {
            return View();
        }
        [HttpPost]
        public ActionResult GetKrEEAA(string __a, string __b)
        {
            string[] StringParametro = __b.Split(',');

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new NwRep_Kc_AASS().Get_EEAA(new E_Parametros_Aje_AASS()
                {
                    Cod_Equipo = "040820160936",
                    Cod_SubCanal = "0",
                    Cod_Empresa = "1987",
                    Cod_Categoria = Convert.ToString(StringParametro[1]),
                    Cod_Marca = Convert.ToString(StringParametro[2]),
                    Cod_Zona = "0",
                    Cod_Distrito = "0",
                    Cod_PDV = "0",
                    segmento = "0",
                    Cod_Cadena = Convert.ToString(StringParametro[10]),
                    Cod_Elemento = "0",
                    opcion = 0,
                    Anio = Convert.ToString(StringParametro[8]),
                    Mes = Convert.ToString(StringParametro[9])
                })),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult GetBdKcEEAA(string __a, string __b, int __c, string __d, int __e, int __f, int __g)
        {
            #region
            string _fileServer = "";
            string _filePath = "";
            int _fila = 0;

            _fileServer = String.Format("Exhibicion_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            #endregion
            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Encarte >>

                List<E_Reporting_Excibicion_AASS> oEncarte = new NwRep_Kc_AASS().Get_BD_EEAA(
                 new Request_AjeReporting_Parametros()
                 {
                     cod_equipo = "040820160936",
                     subcanal2 = "0",
                     cadena = "0",
                     categoria = Convert.ToString(__a),
                     empresa = "1987",
                     marca = Convert.ToString(__b),
                     periodo2 = Convert.ToInt32(__c),
                     zona = Convert.ToString(__d),
                     distrito = "0",
                     cod_pdv = "0",
                     segmento = Convert.ToString(__e),
                     cod_material = "0",
                     opcion = 0,
                     anio = Convert.ToInt32(__f),
                     mes = Convert.ToInt32(__g),
                 });

                #endregion

                #endregion

                #region <<< Reporte Encarte >>>
                if ((oEncarte != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Exhibicion");


                    oWsPrecio.Cells[1, 1].Value = "Mat descripcion";
                    oWsPrecio.Cells[1, 2].Value = "Mar descripcion";
                    oWsPrecio.Cells[1, 3].Value = "Mar cantidad";
                    oWsPrecio.Cells[1, 4].Value = "Mar valorizado";
                    oWsPrecio.Cells[1, 5].Value = "Cadena";
                    oWsPrecio.Cells[1, 6].Value = "Nombre Pdv";
                    oWsPrecio.Cells[1, 7].Value = "Mes";
                    oWsPrecio.Cells[1, 8].Value = "Periodo";

                    oWsPrecio.Cells[1, 9].Value = "foto";


                    _fila = 2;
                    foreach (E_Reporting_Excibicion_AASS oBj in oEncarte)
                    {

                        oWsPrecio.Cells[_fila, 1].Value = oBj.mat_descripcion;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.mar_descripcion;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.mar_cantidad;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.mar_valorizado;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.commercialNodeName;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.nombre_pdv;

                        #region
                        if (oBj.foto == "")
                        {
                            oWsPrecio.Cells[_fila, 9].Value = "SIN FOTO";
                        }

                        if (oBj.id_Month == "01")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "ENERO";
                            oWsPrecio.Cells[_fila, 8].Value = "ENERO - " + oBj.ReportsPlanning_Periodo;

                        }
                        else if (oBj.id_Month == "02")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "FEBRERO";
                            oWsPrecio.Cells[_fila, 8].Value = "FEBRERO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "03")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "MARZO";
                            oWsPrecio.Cells[_fila, 8].Value = "MARZO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "04")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "ABRIL";
                            oWsPrecio.Cells[_fila, 8].Value = "ABRIL - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "05")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "MAYO";
                            oWsPrecio.Cells[_fila, 8].Value = "MAYO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "06")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "JUNIO";
                            oWsPrecio.Cells[_fila, 8].Value = "JUNIO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "07")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "JULIO";
                            oWsPrecio.Cells[_fila, 8].Value = "JULIO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "08")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "AGOSTO";
                            oWsPrecio.Cells[_fila, 8].Value = "AGOSTO - " + oBj.ReportsPlanning_Periodo;

                        }
                        else if (oBj.id_Month == "09")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "SETIEMBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "SETIEMBRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "10")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "OCTUBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "OCTUBRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "11")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "NOVIEMBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "NOVIEMNRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "12")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "DICIEMBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "DICIEMBRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        _fila++;

                        #endregion

                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
        public ActionResult GetKcExcelDetEEAA(string __a, string __b, int __c, string __d, int __e, int __f, int __g)
        {
            #region
            string _fileServer = "";
            string _filePath = "";
            //int _fila = 0;

            _fileServer = String.Format("Exhibicion_Detalle_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            #endregion
            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Encarte >>

                List<KC_EEAA_DetalleMaterialReporte> oExhibicionAdicional = new NwRep_Kc_AASS().Get_Excel_Det_EEAA(
                 new Request_AjeReporting_Parametros()
                 {
                     cod_equipo = "040820160936",
                     subcanal2 = "0",
                     cadena = "0",
                     categoria = Convert.ToString(__a),
                     empresa = "1987",
                     marca = Convert.ToString(__b),
                     periodo2 = Convert.ToInt32(__c),
                     zona = Convert.ToString(__d),
                     distrito = "0",
                     cod_pdv = "0",
                     segmento = Convert.ToString(__e),
                     cod_material = "0",
                     opcion = 0,
                     anio = Convert.ToInt32(__f),
                     mes = Convert.ToInt32(__g),
                 });

                #endregion

                #endregion

                #region <<< Reporte Encarte >>>

                if ((oExhibicionAdicional != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Exhibicion Detalle");

                    oWsPrecio.Cells[1, 1].Value = "";
                    oWsPrecio.Cells[1, 2].Value = "";

                    //Fila 2
                    oWsPrecio.Cells[2, 1].Value = "";
                    oWsPrecio.Cells[2, 2].Value = "";

                    int CuentaReg = 0;

                    int sw = 6;
                    //int Fila = 6;
                    //int con = 0;

                    var list_PDV = (from v_campos in oExhibicionAdicional
                                    where v_campos.pdv_codigo != "0"
                                    select new
                                    {
                                        cod_pdv = v_campos.pdv_codigo,
                                        pdv_nombre = v_campos.pdv_descripcion,
                                        anio = v_campos.anio,
                                        mes = v_campos.mes,
                                        cadena = v_campos.cad_nom
                                    }).Distinct().ToList();

                    var list_material = (from v_campos in oExhibicionAdicional
                                         where v_campos.pdv_codigo == "0"
                                         select new
                                         {
                                             codigo = v_campos.mat_codigo,
                                             descrip = v_campos.mat_descripcion,
                                             existe = v_campos.existe
                                         }).Distinct().ToList();

                    foreach (var item in list_material)
                    {
                        oWsPrecio.Cells[1, sw].Value = item.descrip;
                        oWsPrecio.Cells[2, sw].Value = Convert.ToInt32(item.existe);
                        sw++;
                        CuentaReg++;
                    }
                    int swFila = 3;
                    int swFilanum = 1;
                    int swcolumn = 6;
                    foreach (var item in list_PDV)
                    {
                        oWsPrecio.Cells[swFila, 1].Value = swFilanum;
                        oWsPrecio.Cells[swFila, 1].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsPrecio.Cells[swFila, 2].Value = Convert.ToString(item.cadena);
                        oWsPrecio.Cells[swFila, 3].Value = Convert.ToString(item.pdv_nombre);
                        oWsPrecio.Cells[swFila, 4].Value = Convert.ToString(item.mes);
                        oWsPrecio.Cells[swFila, 5].Value = Convert.ToString(item.anio);

                        foreach (var vitem in list_material)
                        {
                            String vexiste = "0";

                            var list_data = (from v_campos in oExhibicionAdicional
                                             where v_campos.pdv_codigo == Convert.ToString(item.cod_pdv) && v_campos.mat_codigo == Convert.ToString(vitem.codigo)
                                             select new
                                             {
                                                 valor = v_campos.existe
                                             }).Distinct().ToList();

                            foreach (var xitem in list_data)
                            {
                                vexiste = Convert.ToString(xitem.valor);
                            }

                            oWsPrecio.Cells[swFila, swcolumn].Value = Convert.ToInt32(vexiste);
                            oWsPrecio.Cells[swFila, swcolumn].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWsPrecio.Cells[swFila, swcolumn].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                            swcolumn++;
                        }
                        swcolumn = 6;
                        swFila++;
                        swFilanum++;
                    }

                    //Formato Cabecera
                    //oWsPrecio.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                    oWsPrecio.Row(2).Height = 25;
                    oWsPrecio.Row(2).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.View.FreezePanes(3, 1);
                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
        public ActionResult GetKcPowerPoint() {
            Application pptApplication = new Application();
            Microsoft.Office.Interop.PowerPoint.Slides slides;
            Microsoft.Office.Interop.PowerPoint._Slide slide;
            Microsoft.Office.Interop.PowerPoint.TextRange objText;

            Presentation pptPresentacion = pptApplication.Presentations.Add(Microsoft.Office.Core.MsoTriState.msoTrue);
            Microsoft.Office.Interop.PowerPoint.CustomLayout customLayout = pptPresentacion.SlideMaster.CustomLayouts[Microsoft.Office.Interop.PowerPoint.PpSlideLayout.ppLayoutText];

            slides = pptPresentacion.Slides;
            slide = slides.AddSlide(1, customLayout);

            objText = slide.Shapes[1].TextFrame.TextRange;
            objText.Text = "FPPT.com";
            objText.Font.Name = "Arial";
            objText.Font.Size = 32;
            objText = slide.Shapes[2].TextFrame.TextRange;
            objText.Text = "Content goes here";

            slide.NotesPage.Shapes[2].TextFrame.TextRange.Text = "Este es un comentario";

            pptPresentacion.SaveAs(System.IO.Path.Combine(LocalTemp, "Demoppt.pptx"), Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsDefault, Microsoft.Office.Core.MsoTriState.msoTrue);

            pptPresentacion.Close();
            pptApplication.Quit();

            return Json("{ nombre: 'datos'}");
        }
        #endregion
        #region <<Precios>>
        #region Reporting Precios
        public ActionResult KcPrecio()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            return View();
        }
        public ActionResult KcGetGrilla(String __a, String __b, String __c, String __d, String __e, String __f, int __g, String __h, int __i, String __j, String __k, String __l, String __m, int __o, int __p , int __x)
        {
            return View(new E_Precio_Kc().Get_KcPrecio(new Request_KimberlyReporting_Precio 
            {
                subcanal = 0,
                cadena = "0",
                categoria = __c,
                empresa = "0",
                marca = "0",
                SKU = "0",
                periodo = __i,
                zona = "0",
                distrito = "0",
                tienda = "0",
                segmento = "0",
                Persona = ((Persona)Session["Session_Login"]).Person_id,
                mes = __p,
                anio = __o,
                opcion = __x
            }));
        }
        public ActionResult KcGetGrilla_det(String __a, String __b, String __c, String __d, String __e, String __f, int __g, String __h, int __i, String __j, String __k, String __l, String __m, int __o, int __p, int __x)
         {
             return View(new E_Precio_Kc().Get_KcPrecio_det(new Request_KimberlyReporting_Precio 
            {
                subcanal = 0,
                cadena = "0",
                categoria = __c,
                empresa = "0",
                marca = "0",
                SKU = "0",
                periodo = __i,
                zona = "0",
                distrito = "0",
                tienda = "0",
                segmento = "0",
                Persona = ((Persona)Session["Session_Login"]).Person_id,
                mes = __p,
                anio = __o,
                opcion = __x
            }));

        }
        #region Stock Out
        public ActionResult KcStockOut() {
            
            return View();
        }
        [HttpPost]
        public ActionResult KcJsonDataStockOut(int op, int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h)
        {
            E_StockOut_Kc_Consulta_Brand oResper = new E_StockOut_Kc_Consulta_Brand();
            oResper = new NwRep_Kc_AASS().Get_StockOut_Marca(new Request_Sos_Aje_Brand { Text = __h });
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new NwRep_Kc_AASS().Get_StockOut(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = op,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        producto = __c.ToString(),
                        periodo = __d.ToString(),
                        zona = __e.ToString(),
                        tienda = __f.ToString(),
                        segmento = __g.ToString(),
                        marca = (__h == "Total" ? "0" : oResper.Cod_Marca.ToString())
                        //marca = "0"
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }
        [HttpPost]
        public ActionResult KcJsonStockOutBrand(int op, int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h)
        {
            E_StockOut_Kc_Consulta_Brand oResper = new E_StockOut_Kc_Consulta_Brand();
            oResper = new NwRep_Kc_AASS().Get_StockOut_Marca(new Request_Sos_Aje_Brand { Text = __h });
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new NwRep_Kc_AASS().Get_StockOut(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = op,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        producto = __c.ToString(),
                        periodo = __d.ToString(),
                        zona = __e.ToString(),
                        tienda = __f.ToString(),
                        segmento = __g.ToString(),
                        marca = (__h == "Total" ? "0" : oResper.Cod_Marca.ToString())
                       // marca = "0"
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }
        [HttpPost]
        public ActionResult KcJsonDataStockOutModal(int __a, string __b, string __c, string __d, string __e, string __f, string __g)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new NwRep_Kc_AASS().Get_StockOut_Detalle(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 0,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        dias = __g.ToString().Trim(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }
        [HttpPost] //JsonDataOosRankingPdv
        public ActionResult KcJsonDataStockOutRankingPdv(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new NwRep_Kc_AASS().Get_StockOut_Det_RangPDV(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 1,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }
        [HttpPost] //JsonDataOosRankingPdvmonto
        public ActionResult KcJsonDataStockOutRankingPdvmonto(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new NwRep_Kc_AASS().Get_StockOut_Det_RangPdvMonto(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 3,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }
        [HttpPost] //JsonDataOosRankingSku
        public ActionResult KcJsonDataStockOutRankingSku(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new NwRep_Kc_AASS().Get_StockOut_Det_RangSKU(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 2,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }
        [HttpPost] //JsonDataOosRankingSkumonto
        public ActionResult KcJsonDataStockOutRankingSkumonto(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new NwRep_Kc_AASS().Get_StockOut_Det_RangSKUMonto(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 4,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        #region Excel

        public JsonResult KcStockOutExcel(int __a, string __b, string __c, string __d, string __e, string __f, string __g)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Oos >>

                List<E_Oos_Data_Export> oOos = new NwRep_Kc_AASS().ExportatDataStockOutBd(
                   new Request_AjeReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       producto = __c.ToString(),
                       periodo = __d.ToString(),
                       zona = __e.ToString(),
                       tienda = __f.ToString(),
                       segmento = __g.ToString(),
                   });

                #endregion

                #endregion

                #region <<< Reporte Oos >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Oos");
                    oWsOos.Cells[1, 1].Value = "Fecha Celular";
                    oWsOos.Cells[1, 2].Value = "Ciudad";
                    oWsOos.Cells[1, 3].Value = "Cadena";
                    oWsOos.Cells[1, 4].Value = "ClientPDV_Code";
                    oWsOos.Cells[1, 5].Value = "Tienda";
                    oWsOos.Cells[1, 6].Value = "Categoria";
                    oWsOos.Cells[1, 7].Value = "Marca";
                    oWsOos.Cells[1, 8].Value = "Presentación";
                    oWsOos.Cells[1, 9].Value = "Sku";
                    oWsOos.Cells[1, 10].Value = "Producto";
                    oWsOos.Cells[1, 11].Value = "Quiebres";

                    _fila = 2;
                    foreach (E_Oos_Data_Export oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.fechacel;
                        oWsOos.Cells[_fila, 2].Value = oBj.ciudad;
                        oWsOos.Cells[_fila, 3].Value = oBj.cadena;
                        oWsOos.Cells[_fila, 4].Value = oBj.cod_pdv;
                        oWsOos.Cells[_fila, 5].Value = oBj.pdv;
                        oWsOos.Cells[_fila, 6].Value = oBj.categoria;
                        oWsOos.Cells[_fila, 7].Value = oBj.marca;
                        oWsOos.Cells[_fila, 8].Value = oBj.presentacion;
                        oWsOos.Cells[_fila, 9].Value = oBj.sku;
                        oWsOos.Cells[_fila, 10].Value = oBj.producto;
                        oWsOos.Cells[_fila, 11].Value = oBj.quiebres;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
        //OosExcelQuiebre
        public JsonResult KcStockOutExcelQuiebre(int __a, string __b, string __c, string __d, string __e, string __f, string __g)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle Quiebre >>

                List<E_StockOut_Kc_Det_StockOut> oOos = new NwRep_Kc_AASS().Get_StockOut_Detalle(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 0,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        dias = __g.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle Quiebre >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det Quiebre");
                    oWsOos.Cells[1, 1].Value = "Cadena";
                    oWsOos.Cells[1, 2].Value = "Fecha";
                    oWsOos.Cells[1, 3].Value = "PDV";
                    oWsOos.Cells[1, 4].Value = "Categoria";
                    oWsOos.Cells[1, 5].Value = "Presentación";
                    oWsOos.Cells[1, 6].Value = "Descripcion SKU";
                    //oWsOos.Cells[1, 7].Value = "SKU";
                    _fila = 2;
                    foreach (E_StockOut_Kc_Det_StockOut oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.cadena;
                        oWsOos.Cells[_fila, 2].Value = oBj.fecha;
                        oWsOos.Cells[_fila, 3].Value = oBj.pdv;
                        oWsOos.Cells[_fila, 4].Value = oBj.categoria;
                        oWsOos.Cells[_fila, 5].Value = oBj.presentacion;
                        oWsOos.Cells[_fila, 6].Value = oBj.producto;
                        //oWsOos.Cells[_fila, 7].Value = oBj.sku;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 6].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult KcStockOutExcelRankingPDV(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle RankingPDV >>

                List<E_StockOut_Kc_Det_StockOut_RangkingPDV> oOos = new NwRep_Kc_AASS().Get_StockOut_Det_RangPDV(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 1,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle RankingPDV >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det RankingPDV");
                    oWsOos.Cells[1, 1].Value = "Ciudad";
                    oWsOos.Cells[1, 2].Value = "PDV";
                    oWsOos.Cells[1, 3].Value = "Ene";
                    oWsOos.Cells[1, 4].Value = "Feb";
                    oWsOos.Cells[1, 5].Value = "Mar";
                    oWsOos.Cells[1, 6].Value = "Abr";
                    oWsOos.Cells[1, 7].Value = "May";
                    oWsOos.Cells[1, 8].Value = "Jun";
                    oWsOos.Cells[1, 9].Value = "Jul";
                    oWsOos.Cells[1, 10].Value = "Ago";
                    oWsOos.Cells[1, 11].Value = "Sep";
                    oWsOos.Cells[1, 12].Value = "Oct";
                    oWsOos.Cells[1, 13].Value = "Nov";
                    oWsOos.Cells[1, 14].Value = "Dic";
                    oWsOos.Cells[1, 15].Value = "Total";

                    _fila = 2;
                    foreach (E_StockOut_Kc_Det_StockOut_RangkingPDV oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.ciudad;
                        oWsOos.Cells[_fila, 2].Value = oBj.pdv;
                        oWsOos.Cells[_fila, 3].Value = oBj.mes_1;
                        oWsOos.Cells[_fila, 4].Value = oBj.mes_2;
                        oWsOos.Cells[_fila, 5].Value = oBj.mes_3;
                        oWsOos.Cells[_fila, 6].Value = oBj.mes_4;
                        oWsOos.Cells[_fila, 7].Value = oBj.mes_5;
                        oWsOos.Cells[_fila, 8].Value = oBj.mes_6;
                        oWsOos.Cells[_fila, 9].Value = oBj.mes_7;
                        oWsOos.Cells[_fila, 10].Value = oBj.mes_8;
                        oWsOos.Cells[_fila, 11].Value = oBj.mes_9;
                        oWsOos.Cells[_fila, 12].Value = oBj.mes_10;
                        oWsOos.Cells[_fila, 13].Value = oBj.mes_11;
                        oWsOos.Cells[_fila, 14].Value = oBj.mes_12;
                        oWsOos.Cells[_fila, 15].Value = oBj.mes_13;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 15].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.Column(12).AutoFit();
                    oWsOos.Column(13).AutoFit();
                    oWsOos.Column(14).AutoFit();
                    oWsOos.Column(15).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult KcStockOutExcelRankingPDVmonto(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle RankingPDV Montos>>

                List<E_StockOut_Kc_Det_RangkingPDVMonto> oOos = new NwRep_Kc_AASS().Get_StockOut_Det_RangPdvMonto(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 3,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle RankingPDV Montos >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det RankingPDV Montos");
                    oWsOos.Cells[1, 1].Value = "Ciudad";
                    oWsOos.Cells[1, 2].Value = "PDV";
                    oWsOos.Cells[1, 3].Value = "Ene";
                    oWsOos.Cells[1, 4].Value = "Feb";
                    oWsOos.Cells[1, 5].Value = "Mar";
                    oWsOos.Cells[1, 6].Value = "Abr";
                    oWsOos.Cells[1, 7].Value = "May";
                    oWsOos.Cells[1, 8].Value = "Jun";
                    oWsOos.Cells[1, 9].Value = "Jul";
                    oWsOos.Cells[1, 10].Value = "Ago";
                    oWsOos.Cells[1, 11].Value = "Sep";
                    oWsOos.Cells[1, 12].Value = "Oct";
                    oWsOos.Cells[1, 13].Value = "Nov";
                    oWsOos.Cells[1, 14].Value = "Dic";
                    oWsOos.Cells[1, 15].Value = "Total";

                    _fila = 2;
                    foreach (E_StockOut_Kc_Det_RangkingPDVMonto oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.ciudad;
                        oWsOos.Cells[_fila, 2].Value = oBj.pdv;
                        oWsOos.Cells[_fila, 3].Value = oBj.mes_1;
                        oWsOos.Cells[_fila, 4].Value = oBj.mes_2;
                        oWsOos.Cells[_fila, 5].Value = oBj.mes_3;
                        oWsOos.Cells[_fila, 6].Value = oBj.mes_4;
                        oWsOos.Cells[_fila, 7].Value = oBj.mes_5;
                        oWsOos.Cells[_fila, 8].Value = oBj.mes_6;
                        oWsOos.Cells[_fila, 9].Value = oBj.mes_7;
                        oWsOos.Cells[_fila, 10].Value = oBj.mes_8;
                        oWsOos.Cells[_fila, 11].Value = oBj.mes_9;
                        oWsOos.Cells[_fila, 12].Value = oBj.mes_10;
                        oWsOos.Cells[_fila, 13].Value = oBj.mes_11;
                        oWsOos.Cells[_fila, 14].Value = oBj.mes_12;
                        oWsOos.Cells[_fila, 15].Value = oBj.mes_13;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 15].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.Column(12).AutoFit();
                    oWsOos.Column(13).AutoFit();
                    oWsOos.Column(14).AutoFit();
                    oWsOos.Column(15).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult KcStockOutExcelRankingSKU(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle RankingSku >>

                List<E_StockOut_Kc_Det_StockOut_RangkingSKU> oOos = new NwRep_Kc_AASS().Get_StockOut_Det_RangSKU(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 2,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle RankingSku >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det RankingSku");
                    oWsOos.Cells[1, 1].Value = "Presentación";
                    oWsOos.Cells[1, 2].Value = "Ene";
                    oWsOos.Cells[1, 3].Value = "Feb";
                    oWsOos.Cells[1, 4].Value = "Mar";
                    oWsOos.Cells[1, 5].Value = "Abr";
                    oWsOos.Cells[1, 6].Value = "May";
                    oWsOos.Cells[1, 7].Value = "Jun";
                    oWsOos.Cells[1, 8].Value = "Jul";
                    oWsOos.Cells[1, 9].Value = "Ago";
                    oWsOos.Cells[1, 10].Value = "Sep";
                    oWsOos.Cells[1, 11].Value = "Oct";
                    oWsOos.Cells[1, 12].Value = "Nov";
                    oWsOos.Cells[1, 13].Value = "Dic";
                    oWsOos.Cells[1, 14].Value = "Total";

                    _fila = 2;
                    foreach (E_StockOut_Kc_Det_StockOut_RangkingSKU oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.sku;
                        oWsOos.Cells[_fila, 2].Value = oBj.mes_1;
                        oWsOos.Cells[_fila, 3].Value = oBj.mes_2;
                        oWsOos.Cells[_fila, 4].Value = oBj.mes_3;
                        oWsOos.Cells[_fila, 5].Value = oBj.mes_4;
                        oWsOos.Cells[_fila, 6].Value = oBj.mes_5;
                        oWsOos.Cells[_fila, 7].Value = oBj.mes_6;
                        oWsOos.Cells[_fila, 8].Value = oBj.mes_7;
                        oWsOos.Cells[_fila, 9].Value = oBj.mes_8;
                        oWsOos.Cells[_fila, 10].Value = oBj.mes_9;
                        oWsOos.Cells[_fila, 11].Value = oBj.mes_10;
                        oWsOos.Cells[_fila, 12].Value = oBj.mes_11;
                        oWsOos.Cells[_fila, 13].Value = oBj.mes_12;
                        oWsOos.Cells[_fila, 14].Value = oBj.mes_13;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 14].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.Column(12).AutoFit();
                    oWsOos.Column(13).AutoFit();
                    oWsOos.Column(14).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult KcStockOutExcelRankingSKUmonto(int __a, string __b, string __c, string __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Oos_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Oos Detalle RankingSku Montos >>

                List<E_StockOut_Kc_Det_RangkingSKUMonto> oOos = new NwRep_Kc_AASS().Get_StockOut_Det_RangSKUMonto(
                    new Request_AjeReporting_Parametros()
                    {
                        opcion = 4,
                        subcanal = Convert.ToInt32(__a),
                        periodo = __b.ToString(),
                        cadena = __c.ToString(),
                        fecha_incidencia = __d.ToString(),
                        categoria = __e.ToString(),
                        marca = __f.ToString()
                    });

                #endregion

                #endregion

                #region <<< Oos Detalle RankingSku Montos >>>
                if ((oOos != null))
                {
                    Excel.ExcelWorksheet oWsOos = oEx.Workbook.Worksheets.Add("Reporte Det RankingSku Montos");
                    oWsOos.Cells[1, 1].Value = "Presentación";
                    oWsOos.Cells[1, 2].Value = "Ene";
                    oWsOos.Cells[1, 3].Value = "Feb";
                    oWsOos.Cells[1, 4].Value = "Mar";
                    oWsOos.Cells[1, 5].Value = "Abr";
                    oWsOos.Cells[1, 6].Value = "May";
                    oWsOos.Cells[1, 7].Value = "Jun";
                    oWsOos.Cells[1, 8].Value = "Jul";
                    oWsOos.Cells[1, 9].Value = "Ago";
                    oWsOos.Cells[1, 10].Value = "Sep";
                    oWsOos.Cells[1, 11].Value = "Oct";
                    oWsOos.Cells[1, 12].Value = "Nov";
                    oWsOos.Cells[1, 13].Value = "Dic";
                    oWsOos.Cells[1, 14].Value = "Total";

                    _fila = 2;
                    foreach (E_StockOut_Kc_Det_RangkingSKUMonto oBj in oOos)
                    {
                        oWsOos.Cells[_fila, 1].Value = oBj.sku;
                        oWsOos.Cells[_fila, 2].Value = oBj.mes_1;
                        oWsOos.Cells[_fila, 3].Value = oBj.mes_2;
                        oWsOos.Cells[_fila, 4].Value = oBj.mes_3;
                        oWsOos.Cells[_fila, 5].Value = oBj.mes_4;
                        oWsOos.Cells[_fila, 6].Value = oBj.mes_5;
                        oWsOos.Cells[_fila, 7].Value = oBj.mes_6;
                        oWsOos.Cells[_fila, 8].Value = oBj.mes_7;
                        oWsOos.Cells[_fila, 9].Value = oBj.mes_8;
                        oWsOos.Cells[_fila, 10].Value = oBj.mes_9;
                        oWsOos.Cells[_fila, 11].Value = oBj.mes_10;
                        oWsOos.Cells[_fila, 12].Value = oBj.mes_11;
                        oWsOos.Cells[_fila, 13].Value = oBj.mes_12;
                        oWsOos.Cells[_fila, 14].Value = oBj.mes_13;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsOos.SelectedRange[1, 1, 1, 14].AutoFilter = true;
                    oWsOos.Row(1).Height = 25;
                    oWsOos.Row(1).Style.Font.Bold = true;
                    oWsOos.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsOos.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsOos.Column(1).AutoFit();
                    oWsOos.Column(2).AutoFit();
                    oWsOos.Column(3).AutoFit();
                    oWsOos.Column(4).AutoFit();
                    oWsOos.Column(5).AutoFit();
                    oWsOos.Column(6).AutoFit();
                    oWsOos.Column(7).AutoFit();
                    oWsOos.Column(8).AutoFit();
                    oWsOos.Column(9).AutoFit();
                    oWsOos.Column(10).AutoFit();
                    oWsOos.Column(11).AutoFit();
                    oWsOos.Column(12).AutoFit();
                    oWsOos.Column(13).AutoFit();
                    oWsOos.Column(14).AutoFit();
                    oWsOos.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
        #endregion
        #endregion
        #endregion
        
        #endregion
        #region Reporting Competencia

        public ActionResult Competencia()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            return View();
        }
        [HttpPost]
        public ActionResult JsonChainCompany(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Competencia_Kimberly().Objeto_Competencia(
                    new Request_KimberlyReporting_Parametros()
                    {
                        opcion = 0,
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        categoria = __c.ToString(),
                        empresa = __d.ToString(),
                        marca = __e.ToString(),
                        periodo = __f.ToString(),
                        zona = __g.ToString(),
                        distrito = __h.ToString(),
                        tienda = __i.ToString(),
                        segmento = __j.ToString(),
                        tipoactividad = __k.ToString()
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult JsonDetalleCompetencia(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            List<E_Reporting_Competencia_Detalle_Kimberly> oLs = new E_Reporting_Competencia_Detalle_Kimberly().Lista_CompetenciaDetalle(
                    new Request_KimberlyReporting_Parametros()
                    {
                        //edwin
                        subcanal = Convert.ToInt32(__a),
                        cadena = __b.ToString(),
                        categoria = __c.ToString(),
                        empresa = __d.ToString(),
                        marca = __e.ToString(),
                        periodo = __f.ToString(),
                        zona = __g.ToString(),
                        distrito = __h.ToString(),
                        tienda = __i.ToString(),
                        segmento = __j.ToString(),
                        tipoactividad = __k.ToString()
                    });

            return Json(new { Archivo = oLs });
        }

        public JsonResult Exportar_Competencia_Detalle(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Competencia_Detalle_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Competencia >>

                List<E_Reporting_Competencia_Detalle_Kimberly> oCompetencia = new E_Reporting_Competencia_Detalle_Kimberly().Lista_CompetenciaDetalle(
                   new Request_KimberlyReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       categoria = __c.ToString(),
                       empresa = __d.ToString(),
                       marca = __e.ToString(),
                       periodo = __f.ToString(),
                       zona = __g.ToString(),
                       distrito = __h.ToString(),
                       tienda = __i.ToString(),
                       segmento = __j.ToString(),
                       tipoactividad = __k.ToString()
                   });

                #endregion

                #endregion

                #region <<< Reporte Competencia >>>
                if ((oCompetencia != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Competencia");
                    oWsPrecio.Cells[1, 1].Value = "Oficina";
                    oWsPrecio.Cells[1, 2].Value = "Cadena";
                    oWsPrecio.Cells[1, 3].Value = "Fecha";
                    oWsPrecio.Cells[1, 4].Value = "Empresa";
                    oWsPrecio.Cells[1, 5].Value = "Marca";
                    oWsPrecio.Cells[1, 6].Value = "Tipo de Actividad";
                    oWsPrecio.Cells[1, 7].Value = "Descripcion Comercial";
                    oWsPrecio.Cells[1, 8].Value = "Foto";



                    _fila = 2;
                    foreach (E_Reporting_Competencia_Detalle_Kimberly oBj in oCompetencia)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.oficina;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.cadena;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.fecha;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.empresa;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.marca;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.tipo_actividad;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.descripcion_comercial;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.foto;

                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 8].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();


                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public JsonResult CompetenciaExcel(int __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Competencia_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Competencia >>

                List<E_Reporting_Competencia_Detalle_Kimberly> oCompetencia = new E_Reporting_Competencia_Detalle_Kimberly().Lista_CompetenciaDetalle(
                   new Request_KimberlyReporting_Parametros()
                   {
                       subcanal = Convert.ToInt32(__a),
                       cadena = __b.ToString(),
                       categoria = __c.ToString(),
                       empresa = __d.ToString(),
                       marca = __e.ToString(),
                       periodo = __f.ToString(),
                       zona = __g.ToString(),
                       distrito = __h.ToString(),
                       tienda = __i.ToString(),
                       segmento = __j.ToString(),
                       tipoactividad = __k.ToString()
                   });

                #endregion

                #endregion

                #region <<< Reporte Competencia >>>
                if ((oCompetencia != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Competencia");
                    oWsPrecio.Cells[1, 1].Value = "Oficina";
                    oWsPrecio.Cells[1, 2].Value = "Cadena";
                    oWsPrecio.Cells[1, 3].Value = "Fecha";
                    oWsPrecio.Cells[1, 4].Value = "Empresa";
                    oWsPrecio.Cells[1, 5].Value = "Marca";
                    oWsPrecio.Cells[1, 6].Value = "Tipo de Actividad";
                    oWsPrecio.Cells[1, 7].Value = "Descripcion Comercial";
                    oWsPrecio.Cells[1, 8].Value = "Categoria";
                    //oWsPrecio.Cells[1, 9].Value = "Foto";
                    oWsPrecio.Cells[1, 9].Value = "Alcance";
                    oWsPrecio.Cells[1, 10].Value = "Vigencia";
                    oWsPrecio.Cells[1, 11].Value = "Codigo";
                    oWsPrecio.Cells[1, 12].Value = "Precio";
                    oWsPrecio.Cells[1, 13].Value = "Gramaje";
                    oWsPrecio.Cells[1, 14].Value = "Impulso";
                    oWsPrecio.Cells[1, 15].Value = "POP";
                    oWsPrecio.Cells[1, 16].Value = "Comentarios Adicionales";


                    _fila = 2;
                    foreach (E_Reporting_Competencia_Detalle_Kimberly oBj in oCompetencia)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.oficina;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.cadena;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.fecha;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.empresa;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.marca;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.tipo_actividad;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.descripcion_comercial;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.categoria;
                        //oWsPrecio.Cells[_fila, 9].Value = oBj.foto;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.alcance;
                        oWsPrecio.Cells[_fila, 10].Value = oBj.vigencia;
                        oWsPrecio.Cells[_fila, 11].Value = oBj.codigo;
                        oWsPrecio.Cells[_fila, 12].Value = oBj.precio;
                        oWsPrecio.Cells[_fila, 13].Value = oBj.gramaje;
                        oWsPrecio.Cells[_fila, 14].Value = oBj.impulso;
                        oWsPrecio.Cells[_fila, 15].Value = oBj.pop;
                        oWsPrecio.Cells[_fila, 16].Value = oBj.comentarios_adicionales;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 16].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }


        
        #endregion
        #endregion
    }
}