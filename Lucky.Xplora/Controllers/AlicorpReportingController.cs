﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Reporting;
using Lucky.Xplora.Models.Map;
using Lucky.Xplora.Models.Alicorp.AASS;
using Lucky.Xplora.Models.NwRepStdAASS;
using Lucky.Xplora.Models.NwRepStdTRAD;
using Lucky.Xplora.Security;
using Lucky.Xplora.Models.Aje;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using System.Net;
using System.Configuration;
using Lucky.Xplora.Models.Alicorp;


namespace Lucky.Xplora.Controllers
{
    public class AlicorpReportingController : Controller
    {
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        //
        // GET: /AlicorpReporting/
        
        public ActionResult Index(string _a)
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);

            ViewBag.vEquipo = _a;
            return View();
        }

        #region <<< Dashboard Minorista >>>

        public ActionResult DshbReporteVenta(string _vequipo, int _vanio, int _vmes, int _voficina, int _vcluster, int _vgiro, string _vcategoria, int _vunidad, int _vespejo)
        {
            ViewBag.vgunidad = _vunidad;
            return View(new Tablero_Grilla_Venta().Grilla_ReporteVenta(new Request_BI_Tablero_Rep_Venta()
            {
                oEquipo = _vequipo
                ,
                oAnio = _vanio
                ,
                oMes = _vmes
                ,
                oOficina = _voficina
                ,
                oCluster = _vcluster
                ,
                oTipoNodo = _vgiro
                ,
                oCategoria = _vcategoria
                ,
                oUnidad = _vunidad
                ,
                oEspejo = _vespejo
                ,
                oPeriodo = 0
            }));
        }

        public ActionResult DshbIndicadorPrimario(string _vAnio, string _vMes)
        {
            ViewBag.vAnio = _vAnio;
            ViewBag.vMes = _vMes;
            return View();
        }

        public ActionResult DshbReporteVenta_Cantidad(string _vequipo, int _vcluster, int _vtiponodo, int _voficina, string _vcategoria, string _vparam)
        {
            return new ContentResult
               {
                   Content = MvcApplication._Serialize(new Tablero_Grilla_Venta().Grilla_ReporteVenta_Cantidad(new Request_BI_Tablero_Rep_Venta()
                   {
                       oEquipo = _vequipo,
                       oCluster = _vcluster,
                       oTipoNodo = _vtiponodo,
                       oOficina = _voficina,
                       oCategoria = _vcategoria,
                       oParametros = _vparam
                   })),
                   ContentType = "application/json"
               };
        }

        public ActionResult DshbGrillaGraficoResumen(int __a, int __b)
        {
            E_Tablero_Indicadores_Request oParametros = new E_Tablero_Indicadores_Request();
            oParametros.cod_oficina = __a;
            oParametros.cod_periodo = __b;

            ViewBag.vMes = __b;
            return View(new E_Tablero_Minorista_Grilla_Grafico().Tablero_Minorista_Grilla_Grafico(new Request_Indicador_Presencia_Sku()
            {
                oParametros = oParametros
            }));
            //return View();
        }

        #endregion

        #region <<< Indicadores Primarios >>>

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-15
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        public ActionResult ListaProducto(string __a, string __b)
        {
            if (__b == null || __b == "")
            {
                __b = "0";
            }

            List<E_Producto> oLs = new E_Producto().Lista(
                    new Listar_Producto_Por_CodCampania_CodCategoria_CodSubCategoria_CodMarca_Request()
                    {
                        CodCampania = "002892382010",
                        CodCategoria = __a,
                        CodSubCategoria = "0",
                        CodMarca = __b
                    });

            return Json(new { Archivo = oLs });
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-15
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <returns></returns>
        public ActionResult DataPresenciaProducto(int __a, int __b, int __c, string __d, int __e, int __f, string __g, int __h, int __i)
        {
            E_Tablero_Indicadores_Request oParametros = new E_Tablero_Indicadores_Request();
            oParametros.cod_oficina = __a;
            oParametros.cod_cluster = __b;
            oParametros.cod_tipo_nodo = __c;
            oParametros.cod_categoria = __d;
            oParametros.cod_marca = __e;
            oParametros.cod_tipo_canal = __f;
            oParametros.cod_sku = __g;
            oParametros.cod_mes = __h;
            oParametros.cod_periodo = __i;


            List<Indicador_Presencia_Sku> oLs = new Indicador_Presencia_Sku().Presencia_Producto(
                    new Request_Indicador_Presencia_Sku()
                    {
                        oParametros = oParametros
                    });

            return Json(new { DtPresenciaProducto = oLs });
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-15
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FiltrosCategoria()
        {

            List<M_Combo> oLs = new ReporteAliMenor_Filtro_Service()
                    .Filtro(new Resquest_AlicorpMinoristaCobertura()
                    {
                        opcion = 5,
                        parametros = "002892382010,21"
                    });

            return Json(new { Archivo = oLs });
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-15
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <returns></returns>
        public ActionResult DataExhibicionVentana(int __a, int __b, int __c, string __d, int __e, int __f, int __g)
        {
            E_Tablero_Indicadores_Request oParametros = new E_Tablero_Indicadores_Request();
            oParametros.cod_oficina = __a;
            oParametros.cod_cluster = __b;
            oParametros.cod_tipo_nodo = __c;
            oParametros.cod_categoria = __d;
            oParametros.cod_tipo_canal = __e;
            oParametros.cod_mes = __f;
            oParametros.cod_periodo = __g;


            List<Indicador_Primario> oLs = new Indicador_Primario().Exhibicion_Ventana(
                    new Request_Indicador_Presencia_Sku()
                    {
                        oParametros = oParametros
                    });

            return Json(new { DtExhibicionVentana = oLs });
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-15
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <returns></returns>
        public ActionResult DataElementosVisibilidad(int __a, int __b, int __c, string __d, int __e, string __f, int __g, int __h)
        {
            E_Tablero_Indicadores_Request oParametros = new E_Tablero_Indicadores_Request();
            oParametros.cod_oficina = __a;
            oParametros.cod_cluster = __b;
            oParametros.cod_tipo_nodo = __c;
            oParametros.cod_categoria = __d;
            oParametros.cod_tipo_canal = __e;
            oParametros.cod_pop = __f;
            oParametros.cod_mes = __g;
            oParametros.cod_periodo = __h;


            List<Indicador_Primario> oLs = new Indicador_Primario().Elemento_Visibilidad(
                    new Request_Indicador_Presencia_Sku()
                    {
                        oParametros = oParametros
                    });

            return Json(new { DtElementoVisibilidad = oLs });
        }

        [HttpPost]
        public ActionResult FiltrosElemVisibilidad()
        {

            List<Material> oLs = new Material()
                    .Lista(new Request_GetTipoMaterial_Opcion_Parametro()
                    {
                        opcion = 39,
                        parametro = "002892382010,140"
                    });

            return Json(new { Archivo = oLs });
        }

        [HttpPost]
        public ActionResult GetTermometroSku(int __a, int __b, int __c, string __d, int __e, int __f, int __g, int __h, string __i)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpMinoristaPresenciaPdv()
                    .Presencia(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = "002892382010",
                        grupo = (__a == 0 ? 1 : 5),
                        ubigeo = (__a == 0 ? "589" : "589," + Convert.ToString(__a)),
                        anio = Convert.ToInt32(__i),
                        mes = __g,
                        periodo = __h,
                        giro = __c,
                        pdv = "",
                        categoria = Convert.ToInt32(__d),
                        marca = __e,
                        segmento = __b,
                        producto = "",
                        opcion = 0,
                        tipoGiro = __f
                    })),
                ContentType = "application/json"
            }.Content);
            //return Content(new ContentResult
            //{
            //    Content = MvcApplication._Serialize(new Termometros().TermometroSku(new Request_GetTermometro_Sku()
            //        {
            //            cod_oficina = __a,
            //            cod_cluster = __b,
            //            cod_tipo_nodo = __c,
            //            cod_categoria = __d,
            //            cod_marca = __e,
            //            cod_tipo_canal = __f,
            //            cod_mes = __g ,
            //            cod_periodo = __h
            //        })),
            //    ContentType = "application/json"
            //}.Content);
        }

        [HttpPost]
        public ActionResult GetTermometroElmvisb(int __a, int __b, int __c, string __d, int __e, int __f, int __g, string __h)
        {
            return Content(new ContentResult
            {
                //Content = MvcApplication._Serialize(new Termometros().TermometroElmvisb(new Request_GetTermometro_Elmvisb()
                //{
                //    cod_oficina = __a,
                //    cod_cluster = __b,
                //    cod_tipo_nodo = __c,
                //    cod_categoria = __d,
                //    cod_tipo_canal = __e,
                //    cod_mes = __f,
                //    cod_periodo= __g
                //})),
                Content = MvcApplication._Serialize(
                    new AlicorpMinoristaPopPdv()
                    .Pop(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = "002892382010",
                        grupo = (__a == 0 ? 1 : 5),
                        ubigeo = (__a == 0 ? "589" : "589," + Convert.ToString(__a)),
                        anio = Convert.ToInt32(__h),
                        mes = __f,
                        periodo = __g,
                        giro = __c,
                        pdv = "",
                        categoria = Convert.ToInt32(__d),
                        segmento = __b,
                        elemento = 0,
                        opcion = 0,
                        tipoGiro = __e
                    })
                ),
                ContentType = "application/json"
            }.Content);

        }

        [HttpPost]
        public ActionResult GetTermometroVentana(int __a, int __b, int __c, string __d, int __e, int __f, int __g, string __h)
        {
            return Content(new ContentResult
            {
                //Content = MvcApplication._Serialize(new Termometros().TermometroVentana(new Request_GetTermometro_ventana()
                //    {
                //        cod_oficina = __a,
                //        cod_cluster = __b,
                //        cod_tipo_nodo = __c,
                //        cod_categoria = __d,
                //        cod_tipo_canal = __e,
                //        cod_mes= __f,
                //        cod_periodo = __g
                //    })),
                Content = MvcApplication._Serialize(
                    new AlicorpMinoristaVentanaPdv()
                    .Ventana(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = "002892382010",
                        grupo = (__a == 0 ? 1 : 5),
                        ubigeo = (__a == 0 ? "589" : "589," + Convert.ToString(__a)),
                        anio = Convert.ToInt32(__h),
                        mes = __f,
                        periodo = __g,
                        giro = __c,
                        pdv = "",
                        categoria = Convert.ToInt32(__d),
                        segmento = __b,
                        opcion = 0,
                        tipoGiro = __e
                    })
                ),
                ContentType = "application/json"
            }.Content);
        }
        #endregion

        #region << Indicadores Secundarios >>

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-17
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        public ActionResult DataIndicadorSecundario(int __a, int __b, int __c, string __d, int __e, int __f, int __g, int __h, string __i)
        {
            //E_Tablero_Indicadores_Request oParametros = new E_Tablero_Indicadores_Request();
            //oParametros.cod_oficina = __a;
            //oParametros.cod_cluster = __b;
            //oParametros.cod_tipo_nodo = __c;
            //oParametros.cod_categoria = __d;
            //oParametros.cod_marca = __e;
            //oParametros.cod_tipo_canal = __f;
            //oParametros.cod_mes = __g;
            //oParametros.cod_periodo = __h;


            //E_Tablero_Minorista_Indi_Secun oLs = new E_Tablero_Minorista_Indi_Secun().Indicador_Secundario(
            //        new Request_Indicador_Presencia_Sku()
            //        {
            //            oParametros = oParametros
            //        });

            AlicorpMinoristaSod oBj = new AlicorpMinoristaSodCategoria()
                    .SOD(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = "002892382010",//"002892382010",//__a,
                        grupo = (__a == 0 ? 1 : 5),
                        ubigeo = (__a == 0 ? "589" : "589," + Convert.ToString(__a)),
                        anio = Convert.ToInt32(__i), //2015, // 
                        mes = __g,// 5, // ,
                        periodo = __h, //30257, // 
                        giro = __c, //0, // 
                        pdv = "", // __h
                        categoria = Convert.ToInt32(__d), // Convert.toInt32(__i),
                        segmento = __b, //17  // 
                        tipoGiro = __f
                    });

            E_Tablero_Minorista_Indi_Secun oLs = new E_Tablero_Minorista_Indi_Secun();

            oLs.oListaPrecio = new AlicorpMinoristaPrecioPdv()
                    .Precio(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = "002892382010",
                        grupo = (__a == 0 ? 1 : 5),
                        ubigeo = (__a == 0 ? "589" : "589," + Convert.ToString(__a)),
                        anio = Convert.ToInt32(__i),
                        mes = __g,
                        periodo = __h,
                        giro = __c,
                        pdv = "",
                        categoria = Convert.ToInt32(__d),
                        marca = __e,
                        segmento = __b,
                        producto = "",
                        opcion = 0,
                        tipoGiro = __f
                    }).porcentaje_promedio;

            oLs.oListaSOV = (oBj == null ? 0 : oBj.porcentaje_promedio);
            oLs.oListaSOVV = 0;

            return Json(new { DtIndicadorSecundario = oLs });
        }

        #endregion

       #region Alicorp Reporting AASS
        /// <summary>
        /// Autor: dsanchez
        /// Fecha: 09/09/2016
        /// </summary>
        /// <returns></returns>
        ///
        #region Precio
        [HttpGet]
        public ActionResult Precio()
        {
            Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }

        /// <summary>
        /// Autor: dsanchez             
        /// Fecha: 09/09/2016
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Precio(string __a, string __b)
        {
            AlicorpAASSReportingParametro a = new AlicorpAASSReportingParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            if (Convert.ToInt32(__a) == 0 || Convert.ToInt32(__a) == 1 || Convert.ToInt32(__a) == 2 || Convert.ToInt32(__a) == 3 || Convert.ToInt32(__a) == 4 || Convert.ToInt32(__a) == 5)
            {
                #region Filtros
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Precio().Lista(a)),

                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (Convert.ToInt32(__a) == 6)
            {
                #region ReportingPrecios
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Precio().ListaCompania(a)),
                    ContentType = "application/json"

                }.Content);
                #endregion
            }

            return View();
        }

        #endregion
        #region OSA

        /// <summary>
        /// Autor:wlopez
        /// Fecha:30/09/2016
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OSA()
        {
            Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }

        /// <summary>
        /// Autor:wlopez
        /// Fecha:30/09/2016
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OSA(string __a, string __b)
        {
            AlicorpAASSReportingParametro a = new AlicorpAASSReportingParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            if (Convert.ToInt32(__a) == 0 || Convert.ToInt32(__a) == 1 || Convert.ToInt32(__a) == 2 || Convert.ToInt32(__a) == 3 || Convert.ToInt32(__a) == 4 || Convert.ToInt32(__a) == 5)
            {
                #region Filtros
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new OSA().Lista(a)),

                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (Convert.ToInt32(__a) == 7)
            {
                
            }

            return View();
        }

        #endregion
        #endregion

        #region New - Alicorp Repoting AASS
        #region <<Quiebres>>
        public ActionResult RpQuiebres()
        {
            return View();
        }

        public ActionResult GrillaPrecio(string __a, string __b, string __c, int __d, string __e, string __f)
        {
            return View(new NwRepPrecioAASS().Consulta_ReporteAlicorpAASS(new Request_NWRepStd_General
            {
                subcanal = __a,
                categoria = __b,
                marca = __c,
                periodo = __d,
                zona = __e,
                cadena = __f
            }));
        }

        public ActionResult IndexPrecio(String __a, String __b, String __c, int __d, String __e)
        {
            return View(new NwRepPrecioAASS().Consulta_IndexPrecio(new Request_NWRepStd_General
            {
                subcanal = __a,
                cadena = __b,
                categoria = __c,
                empresa = "0",
                marca = __e,
                SKU = "0",
                periodo = __d,
                zona = "",
                distrito = "",
                tienda = "",
                segmento = "",
                Persona = ((Persona)Session["Session_Login"]).Person_id
            }));
        }

        public JsonResult QuiebresExcelalicorp(string __a, string __b, string __c, int __d, string __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Quiebres_Alicorp_AASS_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            //_filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);
            
            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Precio >>

                List<E_Excel_Precio_AASS> oPrecio = new Rep_Alicorp_AASS().Excel_Quiebrealicorp(
                   new Request_NWRepStd_General()
                   {
                       subcanal = __a,
                       categoria = __b,
                       marca = __c,
                       periodo = __d,
                       zona = __e,
                       cadena = __f
                   });

                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Quiebre");
                    oWsPrecio.Cells[1, 1].Value = "Categoria";
                    oWsPrecio.Cells[1, 2].Value = "Marca";
                    oWsPrecio.Cells[1, 3].Value = "Producto";
                    oWsPrecio.Cells[1, 4].Value = "Cadena";
                    oWsPrecio.Cells[1, 5].Value = "Tienda";
                    oWsPrecio.Cells[1, 6].Value = "Cantidad";
                    oWsPrecio.Cells[1, 7].Value = "Mes";
                    oWsPrecio.Cells[1, 8].Value = "Año";
            

                    _fila = 2;
                    foreach (E_Excel_Precio_AASS oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.Categoria;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.Marca;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.Producto;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.Cadena;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.Tienda;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.Cantidad;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.Mes;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.Anio;
                  
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }


        //guia
      
        //haciendo
        public JsonResult PrecioIndexExcel(String __a, String __b, String __c, int __d, String __e)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Index_Precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region << Reporte Precio >>

                List<E_excel_index_price_AA_SS_edw> oPrecio = new NwRepPrecioAASS().Excel_index_Precio_edw(
                   new Request_NWRepStd_General()
                   {
                       subcanal = __a,
                       cadena = __b,
                       categoria = __c,
                       empresa = "0",
                       marca = __e,
                       SKU = "0",
                       periodo = __d,
                       zona = "",
                       distrito = "",
                       tienda = "",
                       segmento = "",
                       Persona = ((Persona)Session["Session_Login"]).Person_id
                   });
                #endregion
                
                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Index Precio");
                    //oWsPrecio.Cells[1, 1].Value = "Cadena";
                    //oWsPrecio.Cells[1, 2].Value = "Codigo PDV";
                    //oWsPrecio.Cells[1, 3].Value = "Nombre PDV";
                    //oWsPrecio.Cells[1, 4].Value = "Empresa";
                    //oWsPrecio.Cells[1, 5].Value = "Presentacion";


                    //AB CDEF GHIJ KLMN OPQR STUV
                    //Combinacion de celdas

                    oWsPrecio.Cells["C1:F1"].Merge = true;
                    oWsPrecio.Cells["G1:J1"].Merge = true;
                    oWsPrecio.Cells["K1:N1"].Merge = true;
                    oWsPrecio.Cells["O1:R1"].Merge = true;
                    oWsPrecio.Cells["S1:V1"].Merge = true;
                    
                    //dando valor a celdas combinadas
                    
                    oWsPrecio.Cells["C1:F1"].Value = "METRO";
                    oWsPrecio.Cells["G1:J1"].Value = "VIVANDA";
                    oWsPrecio.Cells["K1:N1"].Value = "PLAZA VEA";
                    oWsPrecio.Cells["O1:R1"].Value = "WONG";
                    oWsPrecio.Cells["S1:V1"].Value = "TOTUS";

                    oWsPrecio.Cells["A2"].Value = "Presentacion";
                    oWsPrecio.Cells["B2"].Value = "Descripcion";

                    oWsPrecio.Cells["C2"].Value = "P. Reg.";
                    oWsPrecio.Cells["D2"].Value = "Index";
                    oWsPrecio.Cells["E2"].Value = "P. Peso";
                    oWsPrecio.Cells["F2"].Value = "N.Index.";

                    oWsPrecio.Cells["G2"].Value = "P. Reg.";
                    oWsPrecio.Cells["H2"].Value = "Index";
                    oWsPrecio.Cells["I2"].Value = "P. Peso";
                    oWsPrecio.Cells["J2"].Value = "N.Index.";

                    oWsPrecio.Cells["K2"].Value = "P. Reg.";
                    oWsPrecio.Cells["L2"].Value = "Index";
                    oWsPrecio.Cells["M2"].Value = "P. Peso";
                    oWsPrecio.Cells["N2"].Value = "N.Index.";

                    oWsPrecio.Cells["O2"].Value = "P. Reg.";
                    oWsPrecio.Cells["P2"].Value = "Index";
                    oWsPrecio.Cells["Q2"].Value = "P. Peso";
                    oWsPrecio.Cells["R2"].Value = "N.Index.";

                    oWsPrecio.Cells["S2"].Value = "P. Reg.";
                    oWsPrecio.Cells["T2"].Value = "Index";
                    oWsPrecio.Cells["U2"].Value = "P. Peso";
                    oWsPrecio.Cells["V2"].Value = "N.Index.";

                    _fila = 3;
                    foreach (E_excel_index_price_AA_SS_edw oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.codigo;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.orden;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.nombre;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.codigo;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.orden;

                        //oWsPrecio.Cells[_fila, 6].Value = oBj.Cod_periodo;
                        //oWsPrecio.Cells[_fila, 7].Value = oBj.Cod_cadena ;
                        //oWsPrecio.Cells[_fila, 8].Value = oBj.Id_product ;
                        //oWsPrecio.Cells[_fila, 9].Value = oBj.Propio ;
                        //oWsPrecio.Cells[_fila, 10].Value = oBj.Cod_padre;
                        
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 1].AutoFilter = true;
                    //oWsUna.SelectedRange["A2:M2"].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    oWsPrecio.Row(2).Height = 25;
                    oWsPrecio.Row(2).Style.Font.Bold = true;
                    oWsPrecio.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    //oWsPrecio.Column(6).AutoFit();
                    //oWsPrecio.Column(7).AutoFit();
                    //oWsPrecio.Column(8).AutoFit();
                    //oWsPrecio.Column(9).AutoFit();
                    //oWsPrecio.Column(10).AutoFit();
                    //oWsPrecio.Column(11).AutoFit();
                    //oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(4, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        //duplicado
        public JsonResult PrecioIndexExcel2(String __a, String __b, String __c, int __d, String __e)
        {
            string _fileServer = "";
            string _filePath = "";

            _fileServer = String.Format("Precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {

                #region << Reporte Precio >>

                E_Index_Precio_AASS oPrecio = new NwRepPrecioAASS().Consulta_IndexPrecio(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    empresa = "0",
                    marca = __e,
                    SKU = "0",
                    periodo = __d,
                    zona = "",
                    distrito = "",
                    tienda = "",
                    segmento = "",
                    Persona = ((Persona)Session["Session_Login"]).Person_id
                });

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Precio");
                    oWsPrecio.Cells[1, 1, 1, 2].Merge = true;
                    oWsPrecio.Cells[1, 1, 1, 2].Value = "";
                    oWsPrecio.Cells[2, 1].Value = "Presentacion";
                    oWsPrecio.Cells[2, 2].Value = "Descripcion";

                    for (int k = 1; k <= 2; k++)
                    {
                        oWsPrecio.Cells[2, k].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, k].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, k].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, k].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;
                    }

                    int colcab = 3;
                    foreach (var item in oPrecio.Cadenas)
                    {

                        oWsPrecio.Cells[1, colcab].Value = "";
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Merge = true;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Value = item.ToString();

                        //dando borde
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;


                        Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#9BC2E6");
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsPrecio.Cells[1, (colcab + 1), 1, (colcab + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex1);

                        oWsPrecio.Cells[2, colcab].Value = "";
                        oWsPrecio.Cells[2, (colcab + 1)].Value = "Precio";
                        oWsPrecio.Cells[2, (colcab + 2)].Value = "Index";
                        oWsPrecio.Cells[2, (colcab + 3)].Value = "P. Peso";
                        oWsPrecio.Cells[2, (colcab + 4)].Value = "N. Index";

                        //background color negro
                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#000000");
                        oWsPrecio.Cells[2, (colcab + 3), 2, (colcab + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsPrecio.Cells[2, (colcab + 3), 2, (colcab + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex2);
                        //pintando color a blanco
                        Color colFromHex3 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                        oWsPrecio.Cells[2, (colcab + 3), 2, (colcab + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsPrecio.Cells[2, (colcab + 3), 2, (colcab + 4)].Style.Font.Color.SetColor(colFromHex3);

                        //dando borde
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                        //alineando al centro
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        //alineado vertical al medio
                        oWsPrecio.Cells[2, (colcab + 1), 2, (colcab + 4)].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //oWsPrecio.SelectedRange[2, (sw + 1), 2, (sw + 4)].AutoFilter = true;

                        //dando ancho a columnas
                        oWsPrecio.Column(colcab).Width = 1;
                        oWsPrecio.Column(colcab + 1).Width = 8;
                        oWsPrecio.Column(colcab + 2).Width = 8;
                        oWsPrecio.Column(colcab + 3).Width = 8;
                        oWsPrecio.Column(colcab + 4).Width = 8;

                        colcab = colcab + 5;

                    }
                    int colcuerpo = 3;
                    int rowcuerpo = 3;
                    foreach (var item2 in oPrecio.Grupos)
                    {
                        foreach (var row in item2.Filas)
                        {
                            if (row.Propio=="1")
                            {
                                Color colFromHex4 = System.Drawing.ColorTranslator.FromHtml("#C6E0B4");
                                oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Fill.BackgroundColor.SetColor(colFromHex4);
                            }
                            else
                            {
                                Color colFromHex4 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                                oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Fill.BackgroundColor.SetColor(colFromHex4);
                            }
                            oWsPrecio.Cells[rowcuerpo, 1].Value = row.Presentacion;
                            oWsPrecio.Cells[rowcuerpo, 2].Value = row.Producto;

                            //dando borde
                            oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                            oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                            oWsPrecio.Cells[rowcuerpo, 1, rowcuerpo, 2].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                            int conta = 0;
                            foreach (var vfg in row.Cadenas)
                            {
                                if (row.Propio == "1")
                                {
                                    Color colFromHex5 = System.Drawing.ColorTranslator.FromHtml("#C6E0B4");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex5);
                                }
                                else
                                {
                                    Color colFromHex5 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex5);
                                }

                                if (vfg.T_precio=="1")
                                {
                                    Color colFromHex6 = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Style.Font.Color.SetColor(colFromHex6);
                                }
                                else
                                {
                                    Color colFromHex6 = System.Drawing.ColorTranslator.FromHtml("#000000");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Style.Font.Color.SetColor(colFromHex6);
                                }
                                oWsPrecio.Cells[rowcuerpo, colcuerpo].Value = "";

                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Value = decimal.Parse(vfg.P_Regular);
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 2)].Value = decimal.Parse(vfg.Index);
                                if (vfg.P_Peso == "")
                                {
                                    vfg.P_Peso = "0";
                                }
                                else
                                {
                                    vfg.P_Peso = vfg.P_Peso;
                                }
                                if (vfg.N_Index == "")
                                {
                                    vfg.N_Index = "0";
                                }
                                else
                                {
                                    vfg.N_Index = vfg.N_Index;
                                }
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 3)].Value = decimal.Parse(vfg.P_Peso);
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 4)].Value = decimal.Parse(vfg.N_Index);

                                //dando borde
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;


                                //alineando al centro
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                                //alineado vertical al medio
                                oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;


                                colcuerpo += 5;
                                conta += 1;
                            }
                            if (conta < Convert.ToInt32(oPrecio.Cadenas.Count()))
                            {
                                for (int i = 1; conta < Convert.ToInt32(oPrecio.Cadenas.Count()); i++)
                                {
                                    oWsPrecio.Cells[rowcuerpo, colcuerpo].Value = "";
                                    Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#C0C0C0");
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex2);


                                    //dando borde
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                    oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1), rowcuerpo, (colcuerpo + 4)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Value = "";
                                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo + 2)].Value = "";
                                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo + 3)].Value = "";
                                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo + 4)].Value = "";

                                    colcuerpo += 5;
                                    conta += 1;
                                }
                            }
                            colcuerpo = 3;
                            rowcuerpo += 1;
                        }
                        oWsPrecio.Cells[rowcuerpo, 1].Value = "";
                        oWsPrecio.Cells[rowcuerpo, 2].Value = "";
                        for (int j = 1; j < Convert.ToInt32(oPrecio.Cadenas.Count()); j++)
                        {
                            oWsPrecio.Cells[rowcuerpo, colcuerpo].Value = "";
                            oWsPrecio.Cells[rowcuerpo, (colcuerpo + 1)].Value = "";
                            oWsPrecio.Cells[rowcuerpo, (colcuerpo + 2)].Value = "";
                            oWsPrecio.Cells[rowcuerpo, (colcuerpo + 3)].Value = "";
                            oWsPrecio.Cells[rowcuerpo, (colcuerpo + 4)].Value = "";

                            colcuerpo += 5;
                        }
                        colcuerpo = 3;
                        rowcuerpo += 1;
                    }

                    ////leyenda
                    //rowcuerpo += 1;
                    //int colcuerpo2 = 4;

                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, rowcuerpo, (colcuerpo2 + 1)].Merge = true;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2].Value = "letra roja";
                    

                    //oWsPrecio.Cells[(rowcuerpo + 1), colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 1)].Merge = true;
                    //oWsPrecio.Cells[(rowcuerpo + 1), colcuerpo2].Value = "letra roja";
                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo2 + 2), (rowcuerpo + 1), (colcuerpo2 + 3)].Merge = true;
                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo2 + 2), (rowcuerpo + 1), (colcuerpo2 + 3)].Value = "P. Oferta";

                    ////estilos de leyenda
                    ////dando borde
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;
                    ////alineando al centro
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                    ////alineado vertical al medio
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    ////pintando color a rojo
                    //Color colFromHex7 = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Font.Color.SetColor(colFromHex7);
                    ////pintando fondo a verde bajo
                    //Color colFromHex8 = System.Drawing.ColorTranslator.FromHtml("#C6E0B4");
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, rowcuerpo, (colcuerpo2 + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsPrecio.Cells[rowcuerpo, colcuerpo2, rowcuerpo, (colcuerpo2 + 1)].Style.Fill.BackgroundColor.SetColor(colFromHex8);
                    ////pintando fondo a blanco
                    //Color colFromHex9 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                    //oWsPrecio.Cells[(rowcuerpo + 1), colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsPrecio.Cells[(rowcuerpo + 1), colcuerpo2, (rowcuerpo + 1), (colcuerpo2 + 1)].Style.Fill.BackgroundColor.SetColor(colFromHex9);
                    //Color colFromHex10 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo2 + 2), (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    //oWsPrecio.Cells[rowcuerpo, (colcuerpo2 + 2), (rowcuerpo + 1), (colcuerpo2 + 3)].Style.Fill.BackgroundColor.SetColor(colFromHex10);


                    //////////////////////////////////////////////////////////////
                    ////dando alto
                    //oWsPrecio.Row(1).Height = 25;
                    //oWsPrecio.Row(2).Height = 20;

                    ////dando estilo negrita
                    //oWsPrecio.Row(1).Style.Font.Bold = true;
                    //oWsPrecio.Row(2).Style.Font.Bold = true;

                    ////alineacion al centro
                    //oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;


                    ////alineacion vertical al medio
                    //oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    ////Columnas auto ajustadas
                    //oWsPrecio.Column(1).AutoFit();
                    //oWsPrecio.Column(2).AutoFit();

                    //oWsPrecio.View.FreezePanes(3, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public ActionResult Filtro(String parametro, int oop)
        {
            ViewBag.opcion = oop;

            return View(new NwRepPrecioAASS().Filtro(new Request_NWRepStd_General { Parametros = parametro, op = oop }));
        }

        #endregion

        #region Exhibixion Adicional
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-07-24
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExhibicionAdicional()
        {
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-07-24
        /// </summary>
        /// <returns></returns>
        /// 
        //viernes 1
        public JsonResult Exportar_exhibiciones_2()
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Exhibicion_Adicional_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Precio >>

                List<AjeExhibicionAdicionalParticipacionMaterial> oPrecio = new AjeExhibicionAdicional().Excel_Exportar_exhibiciones_2(
                   new E_Parametros_Aje_AASS()
                   {
                       Cod_Equipo = "55052016652016",
                       Cod_SubCanal = "0",
                       Cod_Cadena = "0",
                       Cod_Categoria = "0",
                       Cod_Empresa = "1646",
                       Cod_Marca = "",
                       Periodo = 36125,
                       Cod_Zona = "0",
                       Cod_Distrito = "0",
                       Cod_PDV = "0",
                       segmento = "0",
                       Cod_Elemento = "0",
                       opcion = 0
                   });
                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Exhibicion Adicional");
                    oWsPrecio.Cells[1, 1].Value = "mat_codigo";
                    oWsPrecio.Cells[1, 2].Value = "mat_descripcion";
                    oWsPrecio.Cells[1, 3].Value = "mat_cantidad";
                    oWsPrecio.Cells[1, 4].Value = "Empresa";
                    oWsPrecio.Cells[1, 5].Value = "Presentacion";
                    _fila = 2;
                    foreach (AjeExhibicionAdicionalParticipacionMaterial oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.mat_codigo;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.mat_descripcion;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.mat_cantidad;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.mat_codigo;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.mat_codigo;

                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 1].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;


                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    //oWsPrecio.Column(6).AutoFit();
                    //oWsPrecio.Column(7).AutoFit();
                    //oWsPrecio.Column(8).AutoFit();
                    //oWsPrecio.Column(9).AutoFit();
                    //oWsPrecio.Column(10).AutoFit();
                    //oWsPrecio.Column(11).AutoFit();
                    //oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
       
        [HttpPost]
        public ActionResult ExhibicionAdicional(string __a, string __b)
        {
            string[] StringParametro = __b.Split(',');

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AjeExhibicionAdicional().AlicorpExhibicionAdicional(new E_Parametros_Aje_AASS()
                {
                    Cod_Equipo = "000361782010",
                         
                       //Cod_Equipo = "55052016652016",
                    Cod_SubCanal = "0",
                    //Cod_Empresa = Convert.ToString(StringParametro[0]),
                    Cod_Empresa = "1562",
                    Cod_Categoria = Convert.ToString(StringParametro[1]),
                    Cod_Marca = Convert.ToString(StringParametro[2]),
                    //Periodo = Convert.ToInt32(StringParametro[3]),
                    Cod_Zona = "0",
                    Cod_Distrito = "0",
                    Cod_PDV = "0",
                    segmento = "0",
                    Cod_Cadena = Convert.ToString(StringParametro[10]),
                    //Cod_Cadena = Convert.ToString(29),
                    Cod_Elemento = "0",
                    opcion = 0,
                    Anio = Convert.ToString(StringParametro[8]),
                    Mes = Convert.ToString(StringParametro[9])

                })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult ExportarDetalle(string __a, string __b)
        {
            string[] StringParametro = __b.Split(',');

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AjeExhibicionAdicional().ExhibicionAdicional(new E_Parametros_Aje_AASS()
                {
                    Cod_Equipo = "55052016652016",
                    Cod_SubCanal = "0",
                    Cod_Empresa = Convert.ToString(StringParametro[0]),
                    Cod_Categoria = Convert.ToString(StringParametro[1]),
                    Cod_Marca = Convert.ToString(StringParametro[2]),
                    Periodo = Convert.ToInt32(StringParametro[3]),
                    Cod_Zona = Convert.ToString(StringParametro[4]),
                    Cod_Distrito = Convert.ToString(StringParametro[5]),
                    Cod_PDV = Convert.ToString(StringParametro[6]),
                    segmento = Convert.ToString(StringParametro[7]),
                    Cod_Cadena = Convert.ToString(StringParametro[8]),
                    Cod_Elemento = "0",
                    opcion = 0
                })),
                ContentType = "application/json"
            }.Content);


        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-08-02
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        /// //edwin ayala
        [HttpPost]
        public ActionResult ExhibicionDescarga(string __a)
        {
            int fila = 2;
            int columna = 2;
            string ruta = "";
            string archivo = "";

            if (__a.Length == 0) return View();

            List<AjeExhibicionAdicionalDetalle> lDetalle = MvcApplication._Deserialize<List<AjeExhibicionAdicionalDetalle>>(__a);

            archivo = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            ruta = System.IO.Path.Combine(Server.MapPath("/Temp"), archivo);

            FileInfo archivoNuevo = new FileInfo(ruta);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(archivoNuevo))
            {
                Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Exhibicion");

                foreach (AjeExhibicionAdicionalDetalle oBj in lDetalle)
                {
                    columna = 2;
                    if (lDetalle.First() != oBj)
                    {
                        oWs.Cells[fila, 1].Value = oBj.pdv_descripcion;
                    }

                    foreach (AjeExhibicionAdicionalDetalleMaterial oMa in oBj.DetalleMaterial)
                    {
                        if (lDetalle.First() == oBj)
                        {
                            oWs.Cells[1, columna].Value = oMa.mat_descripcion;
                            oWs.Cells[2, columna].Value = Convert.ToInt32(oMa.existe);

                            oWs.Column(columna).Width = 15;
                            oWs.Row(1).Style.WrapText = true;
                        }
                        else
                        {
                            oWs.Cells[fila, columna].Value = Convert.ToInt32(oMa.existe);
                            oWs.Cells[fila, columna].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWs.Cells[fila, columna].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                        }

                        columna++;
                    }

                    fila++;
                }

                oWs.Column(1).AutoFit();
                oWs.Column(1).Style.Font.Bold = true;

                oWs.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                oWs.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                oWs.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                oWs.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                oEx.Save();
            }

            return Content(new ContentResult
            {
                Content = "{ \"_a\":\"/Temp/" + archivo + "\" }",
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult ExportarJsonExhibicion(string __a, string __b, int __c, string __d, int __e,int __f,int __g)
        { 
            #region
            string _fileServer = "";
            string _filePath = "";
            int _fila = 0;

            _fileServer = String.Format("Exhibicion_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            //_filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);
            

            FileInfo _fileNew = new FileInfo(_filePath);
            #endregion
            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Encarte >>

                List<E_Reporting_Excibicion_AASS> oEncarte = new E_Reporting_Excibicion_AASS().Exportar_Reporting_Exhibicion_AASS_Alicorp(
                 new Request_AjeReporting_Parametros()
                 {


                     cod_equipo = "000361782010",
                     subcanal2 = "0",
                     cadena = "0",
                     categoria = Convert.ToString(__a),
                     empresa = "1562",
                     marca = Convert.ToString(__b),
                     periodo2 = Convert.ToInt32(__c),
                     zona = Convert.ToString(__d),
                     distrito = "0",
                     cod_pdv = "0",
                     segmento = Convert.ToString(__e),
                     cod_material = "0",
                     opcion = 0,
                     anio = Convert.ToInt32(__f),
                     mes = Convert.ToInt32(__g),
                 });

                #endregion

                #endregion

                #region <<< Reporte Encarte >>>
                if ((oEncarte != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Exhibicion");


                    oWsPrecio.Cells[1, 1].Value = "Mat descripcion";
                    oWsPrecio.Cells[1, 2].Value = "Mar descripcion";
                    oWsPrecio.Cells[1, 3].Value = "Mar cantidad";
                    oWsPrecio.Cells[1, 4].Value = "Mar valorizado";
                    oWsPrecio.Cells[1, 5].Value = "Cadena";
                    oWsPrecio.Cells[1, 6].Value = "Nombre Pdv";
                    oWsPrecio.Cells[1, 7].Value = "Mes";
                    oWsPrecio.Cells[1, 8].Value = "Periodo";

                    oWsPrecio.Cells[1, 9].Value = "foto";


                    _fila = 2;
                    foreach (E_Reporting_Excibicion_AASS oBj in oEncarte)
                    {

                        oWsPrecio.Cells[_fila, 1].Value = oBj.mat_descripcion;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.mar_descripcion;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.mar_cantidad;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.mar_valorizado;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.commercialNodeName;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.nombre_pdv;

                        #region
                        if (oBj.foto == "")
                        {
                            oWsPrecio.Cells[_fila, 9].Value = "SIN FOTO";
                        }

                        if (oBj.id_Month == "01" || oBj.id_Month == "1")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "ENERO";
                            oWsPrecio.Cells[_fila, 8].Value = "ENERO - " + oBj.ReportsPlanning_Periodo;

                        }
                        else if (oBj.id_Month == "02" || oBj.id_Month == "2")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "FEBRERO";
                            oWsPrecio.Cells[_fila, 8].Value = "FEBRERO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "03" || oBj.id_Month == "3")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "MARZO";
                            oWsPrecio.Cells[_fila, 8].Value = "MARZO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "04" || oBj.id_Month == "4")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "ABRIL";
                            oWsPrecio.Cells[_fila, 8].Value = "ABRIL - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "05" || oBj.id_Month == "5")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "MAYO";
                            oWsPrecio.Cells[_fila, 8].Value = "MAYO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "06" || oBj.id_Month == "6")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "JUONI";
                            oWsPrecio.Cells[_fila, 8].Value = "JUNIO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "07" || oBj.id_Month == "7")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "JULIO";
                            oWsPrecio.Cells[_fila, 8].Value = "JULIO - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "08" || oBj.id_Month == "8")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "AGOSTO";
                            oWsPrecio.Cells[_fila, 8].Value = "AGOSTO - " + oBj.ReportsPlanning_Periodo;

                        }
                        else if (oBj.id_Month == "09" || oBj.id_Month == "9")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "SETIEMBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "SETIEMBRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "10")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "OCTUBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "OCTUBRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "11")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "NOVIEMBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "NOVIEMNRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        else if (oBj.id_Month == "12")
                        {
                            oWsPrecio.Cells[_fila, 7].Value = "DICIEMBRE";
                            oWsPrecio.Cells[_fila, 8].Value = "DICIEMBRE - " + oBj.ReportsPlanning_Periodo;
                        }
                        _fila++;

                        #endregion

                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        public ActionResult ExportarDuple(string __a, string __b, int __c, string __d, int __e, int __f, int __g)
        {
            #region
            string _fileServer = "";
            string _filePath = "";
            //int _fila = 0;

            _fileServer = String.Format("Exhibicion_Detalle_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            #endregion
            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Encarte >>

                List<AjeExhibicionAdicionalDetalleMaterialReporte> oExhibicionAdicional = new AjeExhibicionAdicionalDetalleMaterialReporte().Exportar_Detalle_Reporting_Exhibicion_AASS_alicorp(
                 new Request_AjeReporting_Parametros()
                 {
                     cod_equipo = "000361782010",
                     subcanal2 = "0",
                     cadena = "0",
                     categoria = Convert.ToString(__a),
                     empresa = "1562",
                     marca = Convert.ToString(__b),
                     periodo2 = Convert.ToInt32(__c),
                     zona = Convert.ToString(__d),
                     distrito = "0",
                     cod_pdv = "0",
                     segmento = Convert.ToString(__e),
                     cod_material = "0",
                     opcion = 0,
                     anio = Convert.ToInt32(__f),
                     mes = Convert.ToInt32(__g),
                 });

                #endregion

                #endregion

                #region <<< Reporte exhi detalle >>>

                if ((oExhibicionAdicional != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Exhibicion Detalle");

                    oWsPrecio.Cells[1, 1].Value = "";
                    oWsPrecio.Cells[1, 2].Value = "";

                    //Fila 2
                    oWsPrecio.Cells[2, 1].Value = " ";
                    oWsPrecio.Cells[2, 2].Value = "Cantidad de Puntos de Ventas con Exhibiciones";
                    oWsPrecio.Cells[3, 1].Value = " ";
                    oWsPrecio.Cells[3, 2].Value = "Cantidad Total de Elementos ";
                    //oWsPrecio.Cells[3, 2].Value = "d";

                    int CuentaReg = 0;

                    int sw = 6;
                    //int Fila = 6;
                    //int con = 0;

                    var list_PDV = (from v_campos in oExhibicionAdicional
                                    where v_campos.pdv_codigo != "0"
                                    select new
                                    {
                                        cod_pdv = v_campos.pdv_codigo,
                                        pdv_nombre = v_campos.pdv_descripcion,
                                        anio = v_campos.anio,
                                        mes =v_campos.mes,
                                        cadena = v_campos.cad_nom
                                    }).Distinct().ToList();

                    var list_material = (from v_campos in oExhibicionAdicional
                                         where v_campos.pdv_codigo == "0"
                                         select new
                                         {
                                             codigo = v_campos.mat_codigo,
                                             descrip = v_campos.mat_descripcion,
                                             existe = v_campos.existe,
                                             cantidad = v_campos.cantidad
                                         }).Distinct().ToList();

                    foreach (var item in list_material)
                    {
                        oWsPrecio.Cells[1, sw].Value = item.descrip;
                        oWsPrecio.Cells[2, sw].Value = Convert.ToInt32(item.existe);
                        oWsPrecio.Cells[3, sw].Value = Convert.ToInt32(item.cantidad);
                        sw++;
                        CuentaReg++;
                    }
                    int swFila = 4;
                    int swFilanum = 1;
                    int swcolumn = 6;
                    foreach (var item in list_PDV)
                    {
                        oWsPrecio.Cells[swFila, 1].Value = swFilanum;
                        oWsPrecio.Cells[swFila, 1].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsPrecio.Cells[swFila, 2].Value = Convert.ToString(item.cadena);
                        oWsPrecio.Cells[swFila, 3].Value = Convert.ToString(item.pdv_nombre);
                        oWsPrecio.Cells[swFila, 4].Value = Convert.ToString(item.mes);
                        oWsPrecio.Cells[swFila, 5].Value = Convert.ToString(item.anio);

                        foreach (var vitem in list_material)
                        {
                            String vexiste = "0";

                            var list_data = (from v_campos in oExhibicionAdicional
                                             where v_campos.pdv_codigo == Convert.ToString(item.cod_pdv) && v_campos.mat_codigo == Convert.ToString(vitem.codigo)
                                             select new
                                             {
                                                 valor = v_campos.existe
                                             }).Distinct().ToList();

                            foreach (var xitem in list_data)
                            {
                                vexiste = Convert.ToString(xitem.valor);
                            }

                            oWsPrecio.Cells[swFila, swcolumn].Value = Convert.ToInt32(vexiste);
                            oWsPrecio.Cells[swFila, swcolumn].Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWsPrecio.Cells[swFila, swcolumn].Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                            swcolumn++;
                        }
                        swcolumn = 6;
                        swFila++;
                        swFilanum++;
                    }

                    //Formato Cabecera
                    //oWsPrecio.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                    oWsPrecio.Row(2).Height = 25;
                    oWsPrecio.Row(2).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    oWsPrecio.Row(3).Height = 25;
                    oWsPrecio.Row(3).Style.Font.Bold = true;
                    oWsPrecio.Row(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(3).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                    ////Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    //oWsPrecio.Column(3).AutoFit();
                    //oWsPrecio.Column(4).AutoFit();
                    //oWsPrecio.Column(5).AutoFit();
                    //oWsPrecio.Column(6).AutoFit();
                    //oWsPrecio.Column(7).AutoFit();
                    //oWsPrecio.Column(8).AutoFit();
                    //oWsPrecio.Column(9).AutoFit();
                    //oWsPrecio.Column(10).AutoFit();
                    //oWsPrecio.Column(11).AutoFit();
                    //oWsPrecio.Column(12).AutoFit();

                    oWsPrecio.View.FreezePanes(4, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        //no vale
        //public ActionResult ExbAdicional()
        //{
        //    return View();
        //}
        //[HttpPost]
        //public ActionResult getEXhAdicional_AASS(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 1;
        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
        //    return Content(new ContentResult
        //    {
        //        Content = MvcApplication._Serialize(oObj),
        //        ContentType = "application/json"
        //    }.Content);
        //}
        ////cod_equipo: _a,cod_subcanal: _b,canal: _c,cod_categoria: _d,cod_empresa: _e,cod_marca: _f,periodo: _g,zona: _h,cod_distrito: _i,cod_pdv: _j,segmento: _k,cod_material: _l
        //[HttpPost]
        //public ActionResult GetGraficoParticipacionEXhAdicional(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 1;
        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
        //    return View(oObj);
        //}
        //[HttpPost]
        //public ActionResult GetGraficoParticipacionMarcaEXhAdicional(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 4;
        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_AASS(parametros);
        //    return View("~/Views/Aje/GetGraficoParticipacionEXhAdicional.cshtml", oObj);
        //}

        //[HttpPost]
        //public ActionResult GetEXhAdicional_Tarifario(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 2;
        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_Tarifario_AASS(parametros);
        //    try
        //    {
        //        #region
        //        List<E_ExhAdicional_Material_AASS> oList_E_tabla = new List<E_ExhAdicional_Material_AASS>();

        //        var v_cabeceras = (from v_cabecera in oObj.ListTarifarioMaterial
        //                           orderby v_cabecera.DescMaterial ascending
        //                           select new
        //                           {
        //                               v_cabecera.CodMaterial,
        //                               v_cabecera.DescMaterial,
        //                               v_cabecera.TipoMaterial
        //                           }).Distinct();
        //        foreach (var v_cab in v_cabeceras)
        //        {
        //            E_ExhAdicional_Material_AASS objMat = new E_ExhAdicional_Material_AASS();
        //            List<E_ExhAdicional_Hijo> oList_Hijo = new List<E_ExhAdicional_Hijo>();
        //            objMat.CodMaterial = v_cab.CodMaterial;
        //            objMat.DescMaterial = v_cab.DescMaterial;
        //            objMat.TipoMaterial = v_cab.TipoMaterial;

        //            //List<E_ExhAdicional_Material_AASS> oE_tabla = new List<E_ExhAdicional_Material_AASS>();

        //            var v_detalles = (from v_detalle in oObj.ListTarifarioMaterial
        //                              where v_detalle.CodMaterial == v_cab.CodMaterial
        //                              select new
        //                              {
        //                                  v_detalle.cod_cadena,
        //                                  v_detalle.valor
        //                              }).Distinct();

        //            foreach (var v_det2 in v_detalles)
        //            {
        //                E_ExhAdicional_Hijo oHijo = new E_ExhAdicional_Hijo();
        //                oHijo.Valor = v_det2.valor;
        //                oHijo.Id = v_det2.cod_cadena;

        //                oList_Hijo.Add(oHijo);
        //            }
        //            objMat.Hijo = oList_Hijo;
        //            //oList_E_tabla.Hijo = oList_Hijo;
        //            oList_E_tabla.Add(objMat);
        //        }
        //        #endregion
        //        oObj.ListTarifarioMaterial = oList_E_tabla;
        //    }
        //    catch
        //    {
        //        oObj = null;
        //    }
        //    return View(oObj);
        //}
        //[HttpPost]
        //public ActionResult GetEXhAdicional_Detalle(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{

        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 2;

        //    E_ExhAdicional_AASS oObj = mExhAd.ExhAdicional_Detalle_AASS(parametros);
        //    try
        //    {
        //        #region

        //        int totalCab = oObj.ListTarifarioMaterial.Count();
        //        oObj.ListTarifarioMaterial = oObj.ListTarifarioMaterial.OrderBy(o => o.CodMaterial).ToList();

        //        List<E_ExhAdicional_Detalle_AASS> oList_E_tabla = new List<E_ExhAdicional_Detalle_AASS>();

        //        var v_cabeceras = (from v_cabecera in oObj.ListExhAdDetalle
        //                           orderby v_cabecera.PDV_Name ascending
        //                           select new
        //                           {
        //                               v_cabecera.Cod_PDV,
        //                               v_cabecera.PDV_Name
        //                           }).Distinct();

        //        foreach (var v_cab in v_cabeceras)
        //        {
        //            E_ExhAdicional_Detalle_AASS objMat = new E_ExhAdicional_Detalle_AASS();
        //            List<E_ExhAdicional_Hijo> oList_Hijo = new List<E_ExhAdicional_Hijo>();
        //            objMat.Cod_PDV = v_cab.Cod_PDV;
        //            objMat.PDV_Name = v_cab.PDV_Name;

        //            //List<E_ExhAdicional_Material_AASS> oE_tabla = new List<E_ExhAdicional_Material_AASS>();

        //            var v_detalles = (from v_detalle in oObj.ListExhAdDetalle
        //                              where v_detalle.Cod_PDV == v_cab.Cod_PDV
        //                              select new
        //                              {
        //                                  v_detalle.Cod_Material,
        //                                  v_detalle.Desc_Material,
        //                                  v_detalle.valor,
        //                                  v_detalle.Foto
        //                              }).Distinct();

        //            foreach (var v_det in v_detalles)
        //            {
        //                E_ExhAdicional_Hijo oHijo = new E_ExhAdicional_Hijo();
        //                oHijo.Id = v_det.Cod_Material;
        //                oHijo.Valor = v_det.valor;
        //                oHijo.Foto = v_det.Foto;

        //                oList_Hijo.Add(oHijo);
        //            }
        //            /////////////////////////////////////////////////////
        //            //Completar de materiales que no esten en la lista
        //            //int filaHijo = oList_Hijo.Count();
        //            int filas = totalCab - oList_Hijo.Count();//cantidad que falta
        //            if (filas < totalCab)
        //            {

        //                List<E_ExhAdicional_Material_AASS> listMaterial = new List<E_ExhAdicional_Material_AASS>();

        //                foreach (E_ExhAdicional_Material_AASS material in oObj.ListTarifarioMaterial)
        //                {
        //                    var result = (from t in oList_Hijo
        //                                  where t.Id == material.CodMaterial
        //                                  select t).Count();
        //                    if (result == 0)
        //                    {
        //                        E_ExhAdicional_Hijo oHijo = new E_ExhAdicional_Hijo();
        //                        oHijo.Id = material.CodMaterial;
        //                        oHijo.Valor = " - ";
        //                        oList_Hijo.Add(oHijo);
        //                    }
        //                }
        //                //for (int i = 0; i < filas; i++)
        //                //{
        //                //    E_ExhAdicional_Hijo oHijo = new E_ExhAdicional_Hijo();
        //                //    oHijo.Id = "0";
        //                //    oHijo.Valor = " - ";
        //                //    oList_Hijo.Add(oHijo);
        //                //}
        //            }
        //            /////////////////////////////////////////////////////
        //            List<E_ExhAdicional_Hijo> hijos = oList_Hijo.OrderBy(o => o.Id).ToList();//Ordenando
        //            objMat.Hijo = hijos;
        //            //objMat.Hijo = oList_Hijo;

        //            oList_E_tabla.Add(objMat);
        //        }
        //        #endregion
        //        oObj.ListExhAdDetalle = oList_E_tabla;
        //    }
        //    catch
        //    {
        //        oObj = null;
        //    }
        //    return View(oObj);
        //}
        //[HttpPost]
        //public ActionResult GetEXhAdicional_Evolutivo(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l, string _m)
        //{
        //    M_ExhAdicional_Aje_AASS_service mExhAd = new M_ExhAdicional_Aje_AASS_service();
        //    E_Parametros_Aje_AASS parametros = new E_Parametros_Aje_AASS();
        //    parametros.Cod_Equipo = _a;
        //    parametros.Cod_SubCanal = _b;
        //    parametros.Cod_Cadena = _c;
        //    parametros.Cod_Categoria = _d;
        //    parametros.Cod_Empresa = _e;
        //    parametros.Cod_Marca = _f;
        //    parametros.Periodo = _g;
        //    parametros.Cod_Zona = _h;
        //    parametros.Cod_Distrito = _i;
        //    parametros.Cod_PDV = _j;
        //    parametros.segmento = _k;
        //    parametros.Cod_Elemento = _l;
        //    parametros.opcion = 3;

        //    ViewBag.Exhibicion = _m;

        //    List<E_ExhAdicional_Evolutivo_AASS> oObj = mExhAd.ExhAdicional_Evolutivo_AASS(parametros);
        //    return View(oObj);
        //}
        //public JsonResult ExhibicionExcel(string _a, string _b, string _c, string _d, string _e, string _f, int _g, string _h, string _i, string _j, string _k, string _l)
        //{
        //    string _fileServer = "";
        //    string _filePath = "";

        //    int _fila = 0;

        //    _fileServer = String.Format("Exhibicion_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
        //    _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

        //    FileInfo _fileNew = new FileInfo(_filePath);

        //    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
        //    {
        //        #region <<< Instancias >>>
        //        #region << Reporte Exhibicion >>
        //        List<E_Excel_Exhibicion_AASS> oExhibicion = new M_ExhAdicional_Aje_AASS().Excel_Exhibicion(
        //           new E_Parametros_Aje_AASS()
        //           {
        //               Cod_Equipo = _a,
        //               Cod_SubCanal = _b,
        //               Cod_Cadena = _c,
        //               Cod_Categoria = _d,
        //               Cod_Empresa = _e,
        //               Cod_Marca = _f,
        //               Periodo = _g,
        //               Cod_Zona = _h,
        //               Cod_Distrito = _i,
        //               Cod_PDV = _j,
        //               segmento = _k,
        //               Cod_Elemento = _l,
        //               opcion = 0
        //           }); 

        //        #endregion

        //        #endregion

        //        #region <<< Reporte Exhibicion >>>
        //        if ((oExhibicion != null))
        //        {
        //            Excel.ExcelWorksheet oWsExhibicion = oEx.Workbook.Worksheets.Add("Reporte Exhibicion");
        //            oWsExhibicion.Cells[1, 1].Value = "Cadena";
        //            oWsExhibicion.Cells[1, 2].Value = "Codigo PDV";
        //            oWsExhibicion.Cells[1, 3].Value = "Nombre PDV";
        //            oWsExhibicion.Cells[1, 4].Value = "Empresa";
        //            oWsExhibicion.Cells[1, 5].Value = "Categoria";
        //            oWsExhibicion.Cells[1, 6].Value = "Marca";
        //            oWsExhibicion.Cells[1, 7].Value = "Material";
        //            oWsExhibicion.Cells[1, 8].Value = "Precio";
        //            oWsExhibicion.Cells[1, 9].Value = "Periodo";

        //            _fila = 2;
        //            foreach (E_Excel_Exhibicion_AASS oBj in oExhibicion)
        //            {
        //                oWsExhibicion.Cells[_fila, 1].Value = oBj.Cadena;
        //                oWsExhibicion.Cells[_fila, 2].Value = oBj.Cod_PDV;
        //                oWsExhibicion.Cells[_fila, 3].Value = oBj.PDV;
        //                oWsExhibicion.Cells[_fila, 4].Value = oBj.Empresa;
        //                oWsExhibicion.Cells[_fila, 5].Value = oBj.Categoria;
        //                oWsExhibicion.Cells[_fila, 6].Value = oBj.Marca;
        //                oWsExhibicion.Cells[_fila, 7].Value = oBj.Material;
        //                oWsExhibicion.Cells[_fila, 8].Value = oBj.P_Regular;
        //                oWsExhibicion.Cells[_fila, 9].Value = oBj.Periodo;
        //                _fila++;
        //            }

        //            //Formato Cabecera
        //            oWsExhibicion.SelectedRange[1, 1, 1, 9].AutoFilter = true;
        //            oWsExhibicion.Row(1).Height = 25;
        //            oWsExhibicion.Row(1).Style.Font.Bold = true;
        //            oWsExhibicion.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
        //            oWsExhibicion.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

        //            //Columnas auto ajustadas
        //            oWsExhibicion.Column(1).AutoFit();
        //            oWsExhibicion.Column(2).AutoFit();
        //            oWsExhibicion.Column(3).AutoFit();
        //            oWsExhibicion.Column(4).AutoFit();
        //            oWsExhibicion.Column(5).AutoFit();
        //            oWsExhibicion.Column(6).AutoFit();
        //            oWsExhibicion.Column(7).AutoFit();
        //            oWsExhibicion.Column(8).AutoFit();
        //            oWsExhibicion.Column(9).AutoFit();

        //            oWsExhibicion.View.FreezePanes(2, 1);


        //            oEx.Save();
        //        }
        //        else
        //        {
        //            _fileServer = "0";
        //        }
        //        #endregion
        //    }
        //    return Json(new { Archivo = _fileServer });
        //}
        #endregion
        
        #region Precio
        public ActionResult RpPrecio() {
            return View();
        }
        public ActionResult RpGrillaPrecio(string __a, string __b, string __c, int __d , string __e,string __f ) {
            
            return View(new Rep_Alicorp_AASS().Consulta_Reporte(new Request_NWRepStd_General
            {
                subcanal = __a,
                categoria = __b,
                marca = __c,
                periodo = __d,
                zona = __e,
                cadena = __f
            }));
        }
        public JsonResult PrecioExcel(string __a, string __b, string __c, int __d, string __e,string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Precio_Alicorp_AASS_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Precio >>

                List<E_Excel_Precio_AASS> oPrecio = new Rep_Alicorp_AASS().Excel_Precio(
                   new Request_NWRepStd_General()
                   {
                       subcanal = __a,
                       categoria = __b,
                       marca = __c,
                       periodo = __d,
                       zona = __e,
                       cadena = __f
                   });

                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte Precio");
                    oWsPrecio.Cells[1, 1].Value = "Cadena";
                    oWsPrecio.Cells[1, 2].Value = "Codigo PDV";
                    oWsPrecio.Cells[1, 3].Value = "Nombre PDV";
                    oWsPrecio.Cells[1, 4].Value = "Empresa";
                    oWsPrecio.Cells[1, 5].Value = "Categoria";
                    oWsPrecio.Cells[1, 6].Value = "Marca";
                    oWsPrecio.Cells[1, 7].Value = "Producto";
                    oWsPrecio.Cells[1, 8].Value = "Precio_Regular";
                    oWsPrecio.Cells[1, 9].Value = "Precio_Oferta";
                    oWsPrecio.Cells[1, 10].Value = "Tipo_Precio";
                    oWsPrecio.Cells[1, 11].Value = "Periodo";
                    oWsPrecio.Cells[1, 12].Value = "Oficina";

                    _fila = 2;
                    foreach (E_Excel_Precio_AASS oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.Cadena;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.Cod_PDV;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.PDV;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.Empresa;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.Categoria;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.Marca;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.Producto;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.P_Regular;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.P_Oferta;
                        oWsPrecio.Cells[_fila, 10].Value = oBj.Tipoprecio;
                        oWsPrecio.Cells[_fila, 11].Value = oBj.Periodo;
                        oWsPrecio.Cells[_fila, 12].Value = oBj.nombre;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 12].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.Column(12).AutoFit();
                    oWsPrecio.View.FreezePanes(2, 1);

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        #endregion
        #region OSA
        public ActionResult RpOsa() {
            return View();
        }
        public ActionResult RpGetOsa(string __a,string __b,string __c,int __d,int __e,int __f,string __g) {
            return Json(new
            {
                response = new Rep_Alicorp_AASS().Consulta_OSA(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    anio = __d,
                    mes = __e,
                    periodo = __f,
                    zona = __g
                })
            });
        }
        public ActionResult RpGetOsaCausal(string __a, string __b, string __c, int __d, int __e, int __f, string __g)
        {
            return Json(new
            {
                response = new Rep_Alicorp_AASS().Causal_OSA(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    anio = __d,
                    mes = __e,
                    periodo = __f,
                    zona = __g
                })
            });
        }
        public ActionResult RpGetOsaCausalOOS(string __a, string __b, string __c, int __d, int __e, int __f, string __g)
        {
            return Json(new
            {
                response = new Rep_Alicorp_AASS().Causal_OSA_OOS(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    anio = __d,
                    mes = __e,
                    periodo = __f,
                    zona = __g
                })
            });
        }

        
        #endregion
        #region SPOT
        public ActionResult RpSpot() {
            return View();
        }
        public ActionResult RpGetSpot(string __a,string __b,string __c,string __d,int __e,string __f) {
            return Json(new
            {
                response = new Rep_Alicorp_AASS().Consulta_SPOT(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    marca = __d,
                    periodo = __e,
                    zona = __f
                })
            });
        }
        public JsonResult RpExcelSpot(string __a, string __b, string __c, string __d, int __e, string __f)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("SPOT_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Visibi >>

                List<E_Spot_Alicorp_AASS_Excel> oPrecio = new Rep_Alicorp_AASS().Excel_SPOT(new Request_NWRepStd_General
                {
                    subcanal = __a,
                    cadena = __b,
                    categoria = __c,
                    marca = __d,
                    periodo = __e,
                    zona = __f
                });

                #endregion

                #endregion

                #region <<< Reporte Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Reporte SPOT");
                    oWsPrecio.Cells[1, 1].Value = "Año";
                    oWsPrecio.Cells[1, 2].Value = "Mes";
                    oWsPrecio.Cells[1, 3].Value = "Quincena";
                    oWsPrecio.Cells[1, 4].Value = "Cadena";
                    oWsPrecio.Cells[1, 5].Value = "Empresa";
                    oWsPrecio.Cells[1, 6].Value = "Categoria";
                    oWsPrecio.Cells[1, 7].Value = "Familia";
                    oWsPrecio.Cells[1, 8].Value = "Tipo Actividad";
                    oWsPrecio.Cells[1, 9].Value = "Descuento";
                    oWsPrecio.Cells[1, 10].Value = "Precio_Normal";
                    oWsPrecio.Cells[1, 11].Value = "Precio_Oferta";

                    _fila = 2;
                    foreach (E_Spot_Alicorp_AASS_Excel oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.anio;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.mes;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.semana;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.cadena;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.empresa;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.categoria;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.familia;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.formato;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.descuento;
                        oWsPrecio.Cells[_fila, 10].Value = "S/."+oBj.precio_normal;
                        oWsPrecio.Cells[_fila, 11].Value = "S/."+oBj.precio_oferta;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();
                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
        public ActionResult RPFiltro(string _a, int _b)
        {
            return Json(new { response = new Rep_Alicorp_AASS().Consulta_Filtro(new Request_NWRepStd_General { Parametros = _a, op = _b }) });
        }
       
        #endregion
        #endregion

        #region <<Carga de Planos>>
        /// <summary>
        /// Autor: pabrigo
        /// Fecha: 2017-01-20
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Carga() {
            ViewBag.CamCodigo = Convert.ToString(Request["_a"]);
            ViewBag.VisId = Convert.ToInt32(Request["_b"]);

            return View();
        }

        /// <summary>
        /// Autor: pabrigo
        /// Fecha: 2017-01-20
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Carga(int opcion, string parametro)
        {
            int VisId = 0;
            string CamCodigo = "";
            string _vreporte = "";
            Persona oPersona = (Persona)Session["Session_Login"];
            AlicorpAASSCargaRespuesta obj = new AlicorpAASSCargaRespuesta();
            CamCodigo = Convert.ToString(Request["_a"]);
             _vreporte= Convert.ToString(Request["cob_reporte"]);
            VisId = Convert.ToInt32(Request["_b"]);

            if (opcion == 0)
            {
                #region Carga de informacion
                int FinFila = 0;
                int NumeroRegistros = 0;
                string NombreArchivo = "";
                List<PointOfSalePlanningOperating> lPointOfSalePlanningOperating = new List<PointOfSalePlanningOperating>();
                
                NombreArchivo = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now);
                HttpPostedFileBase FileBaseExcel = Request.Files["CargaArchivo"];

                if ((FileBaseExcel == null) || ((FileBaseExcel.ContentLength == 0) && string.IsNullOrEmpty(FileBaseExcel.FileName)))
                {
                    return RedirectToAction("Carga", "AlicorpReporting", new { _a = CamCodigo, _b = VisId });
                }

                using (var oPackage = new Excel.ExcelPackage(FileBaseExcel.InputStream))
                {
                    Excel.ExcelWorksheets oSheets = oPackage.Workbook.Worksheets;
                    List<AlicorpAASSCargaEEAA> lAlicorpAASSCargaEEAA = new List<AlicorpAASSCargaEEAA>();
                    List<AlicorpAASSCargaOSA> lAlicorpAASSCargaOSA = new List<AlicorpAASSCargaOSA>();
                    List<AlicorpAASSCargaSPOT> lAlicorpAASSCargaSPOT = new List<AlicorpAASSCargaSPOT>();

                    foreach (Excel.ExcelWorksheet oSheet in oSheets) {
                        if (oSheet.Name == "EEAA" && _vreporte =="1")
                        {
                            #region Exhibicion adicional
                            FinFila = oSheet.Dimension.End.Row;

                            for (int i = 2; i <= FinFila; i++)
                            {
                                AlicorpAASSCargaEEAA oAlicorpAASSCargaEEAA = new AlicorpAASSCargaEEAA();

                                oAlicorpAASSCargaEEAA.PdvCodigo = Convert.ToString(oSheet.Cells[i, 1].Value);
                                oAlicorpAASSCargaEEAA.CatId = Convert.ToInt32(oSheet.Cells[i, 2].Value);
                                oAlicorpAASSCargaEEAA.MarId = Convert.ToInt32(oSheet.Cells[i, 3].Value);
                                oAlicorpAASSCargaEEAA.Fecha = Convert.ToDateTime(oSheet.Cells[i, 4].Value);
                                oAlicorpAASSCargaEEAA.TmaCodigo = Convert.ToString(oSheet.Cells[i, 5].Value);
                                oAlicorpAASSCargaEEAA.MatId = Convert.ToInt32(oSheet.Cells[i, 6].Value);
                                oAlicorpAASSCargaEEAA.Cantidad = Convert.ToInt32(oSheet.Cells[i, 7].Value);
                                oAlicorpAASSCargaEEAA.Archivo = NombreArchivo;
                                oAlicorpAASSCargaEEAA.UsuId = oPersona.Person_id;
                                oAlicorpAASSCargaEEAA.UsuUsuario = oPersona.name_user;

                                lAlicorpAASSCargaEEAA.Add(oAlicorpAASSCargaEEAA);
                            }
                            
                            obj = new AlicorpAASSCargaEEAA_Modulo().Bulk(lAlicorpAASSCargaEEAA);
                            obj.Nom_Reporte = "Rep EEAAA";
                           
                            #endregion
                        }
                        else if (oSheet.Name == "OSA" && _vreporte =="2")
                        {
                            #region OSA
                            FinFila = oSheet.Dimension.End.Row;
                            if (FinFila < 5000)
                            {

                                for (int i = 2; i <= FinFila; i++)
                                {
                                    AlicorpAASSCargaOSA oAlicorpAASSCargaOSA = new AlicorpAASSCargaOSA();

                                    oAlicorpAASSCargaOSA.cod_equipo = Convert.ToString(oSheet.Cells[i, 1].Value);
                                    oAlicorpAASSCargaOSA.cod_oficina = Convert.ToInt32(oSheet.Cells[i, 2].Value);
                                    oAlicorpAASSCargaOSA.fecha = Convert.ToDateTime(oSheet.Cells[i, 3].Value);
                                    oAlicorpAASSCargaOSA.anio = Convert.ToInt32(oSheet.Cells[i, 4].Value);
                                    oAlicorpAASSCargaOSA.mes = Convert.ToInt32(oSheet.Cells[i, 5].Value);
                                    oAlicorpAASSCargaOSA.semana = Convert.ToInt32(oSheet.Cells[i, 6].Value);
                                    oAlicorpAASSCargaOSA.cadena = Convert.ToInt32(oSheet.Cells[i, 7].Value);
                                    oAlicorpAASSCargaOSA.cod_categoria = Convert.ToString(oSheet.Cells[i, 8].Value);
                                    oAlicorpAASSCargaOSA.Tiporesultado = Convert.ToString(oSheet.Cells[i, 9].Value);
                                    oAlicorpAASSCargaOSA.Causal = Convert.ToString(oSheet.Cells[i, 10].Value);
                                    oAlicorpAASSCargaOSA.cluster = Convert.ToString(oSheet.Cells[i, 11].Value);
                                    oAlicorpAASSCargaOSA.medicion = Convert.ToString(oSheet.Cells[i, 12].Value);
                                    oAlicorpAASSCargaOSA.marca = Convert.ToString(oSheet.Cells[i, 14].Value);
                                    oAlicorpAASSCargaOSA.archivo = NombreArchivo;
                                    oAlicorpAASSCargaOSA.UsuId = oPersona.Person_id;
                                    oAlicorpAASSCargaOSA.UsuUsuario = oPersona.name_user;

                                    lAlicorpAASSCargaOSA.Add(oAlicorpAASSCargaOSA);
                                }

                                obj = new AlicorpAASSCargaEEAA_Modulo().Bulkosa(lAlicorpAASSCargaOSA);
                            #endregion
                            }
                            else {
                                obj.codigo = -1;
                                obj.descripcion = "La Cantidad de Registros Excede el Limite (5000)";
                            }
                        }
                        else if (oSheet.Name == "SPOT" && _vreporte == "3")
                        {
                            #region SPOT
                            FinFila = oSheet.Dimension.End.Row;
                            if (FinFila < 5000)
                            {
                                for (int i = 2; i <= FinFila; i++)
                                {
                                    AlicorpAASSCargaSPOT oAlicorpAASSCargaSPOT = new AlicorpAASSCargaSPOT();

                                    oAlicorpAASSCargaSPOT.anio = Convert.ToInt32(oSheet.Cells[i, 1].Value);
                                    oAlicorpAASSCargaSPOT.mes = Convert.ToInt32(oSheet.Cells[i, 2].Value);
                                    oAlicorpAASSCargaSPOT.semana = Convert.ToInt32(oSheet.Cells[i, 3].Value);
                                    oAlicorpAASSCargaSPOT.periodo = Convert.ToInt32(oSheet.Cells[i, 4].Value);
                                    oAlicorpAASSCargaSPOT.cod_cadena = Convert.ToInt32(oSheet.Cells[i, 5].Value);
                                    oAlicorpAASSCargaSPOT.cod_empresa = Convert.ToInt32(oSheet.Cells[i, 6].Value);
                                    oAlicorpAASSCargaSPOT.nom_empresa = Convert.ToString(oSheet.Cells[i, 7].Value);
                                    oAlicorpAASSCargaSPOT.cod_categoria = Convert.ToString(oSheet.Cells[i, 8].Value);
                                    oAlicorpAASSCargaSPOT.familia = Convert.ToString(oSheet.Cells[i, 9].Value);
                                    oAlicorpAASSCargaSPOT.precio_normal = Convert.ToString(oSheet.Cells[i, 10].Value);
                                    oAlicorpAASSCargaSPOT.precio_oferta = Convert.ToString(oSheet.Cells[i, 11].Value);
                                    oAlicorpAASSCargaSPOT.formato_promo = Convert.ToString(oSheet.Cells[i, 12].Value);
                                    oAlicorpAASSCargaSPOT.vs = Convert.ToString(oSheet.Cells[i, 13].Value);
                                    oAlicorpAASSCargaSPOT.cod_formato = Convert.ToInt32(oSheet.Cells[i, 14].Value);
                                    oAlicorpAASSCargaSPOT.Archivo = NombreArchivo;
                                    oAlicorpAASSCargaSPOT.UsuId = oPersona.Person_id;
                                    oAlicorpAASSCargaSPOT.UsuUsuario = oPersona.name_user;


                                    lAlicorpAASSCargaSPOT.Add(oAlicorpAASSCargaSPOT);
                                }

                                obj = new AlicorpAASSCargaEEAA_Modulo().Bulkspot(lAlicorpAASSCargaSPOT);
                            #endregion
                            }
                            else {
                                obj.codigo = -1;
                                obj.descripcion = "La Cantidad de Registros Excede el Limite (5000)";

                               }
                            }

                        ViewBag.CamCodigo = CamCodigo;
                        ViewBag.VisId = VisId;
                        ViewBag.NombreArchivo = NombreArchivo;
                        ViewBag.NumeroRegistros = NumeroRegistros;
                        ViewBag._vreporte = _vreporte;

                    }
                }
                #endregion
            }
            return View(obj);
            //return Content(new ContentResult{ Content = MvcApplication._Serialize(objReportes),ContentType = "application/json"}.Content);
        }
        #endregion
    }
}