﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Diagnostics;
using IO = System.IO;

//using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.LuckyGantt;
//using Lucky.Xplora.Models.Publicacion;
using Lucky.Xplora.Models.Reporting;

using Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Web.Script.Serialization;

using Excel = OfficeOpenXml;
using ExcelStyle = OfficeOpenXml.Style;
using Style = OfficeOpenXml.Style;
using System.IO;
using Lucky.Xplora.Models.Administrador;
using Lucky.Xplora.Security;


namespace Lucky.Xplora.Controllers
{
    public class LuckyGanttController : Controller
    {
        //
        // GET: /LuckyGantt/

        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
            return View();
        }

        public ActionResult GetEditGrantt()
        {
            if (Request["ganttidprom"] != null)
            {
                ViewBag.cod_promocion = Convert.ToString(Request["ganttidprom"]);
                ViewBag.origen = Convert.ToString(Request["ganttori"]);
            }
            return View();
        }

        public ActionResult GetIframeGantt()
        {
            if (Request["ganttidprom"] != null)
            {
                ViewBag.cod_promocion = Convert.ToString(Request["ganttidprom"]);
                ViewBag.origen = Convert.ToString(Request["ganttori"]);
            }
            return View();
        }

        public ActionResult GetActivityPromotional()
        {
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
            
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult PRT_Filtros_Gantt(int _vobj, string _vparam, int _vop, string _vselect)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Parametros = _vparam;
            oParametros.Cod_Op = _vop;
            ViewBag.cod_obj = _vobj;
            ViewBag.cod_select = _vselect;
            return View(new E_Nw_Filtros_Backus().Consul_Filtros(new Request_NW_Reporting() { oParametros = oParametros }));
        }

        public ActionResult PRT_Filtros_Gantt_Check(int _vobj, string _vparam, int _vop, string _vselect)
        {
            E_Hig_Grafico_Parametros oParametros = new E_Hig_Grafico_Parametros();
            oParametros.Parametros = _vparam;
            oParametros.Cod_Op = _vop;
            ViewBag.cod_obj = _vobj;
            ViewBag.cod_select = _vselect;

            List<E_Nw_Filtros_Backus> oLs = new E_Nw_Filtros_Backus().Consul_Filtros(
                    new Request_NW_Reporting()
                    {
                        oParametros = oParametros
                    });

            return Json(new { Archivo = oLs });

        }
        
        [CustomAuthorize]
        public ActionResult GetMantenimiento()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult Consulta_Consolidado_ActividadesPromocionales(int _vmarca, int _vanio, int _vactividad, int _vestado, int _vmes, int _vsolicitante,int _vcompania)
        {
            E_Nw_PromotionalActivities oParametros = new E_Nw_PromotionalActivities();

            oParametros.id_Brand = _vmarca;
            oParametros.anio = _vanio;
            oParametros.id_typeactivity = _vactividad;
            oParametros.id_state = _vestado;
            oParametros.mes = _vmes;
            oParametros.id_solicitante = _vsolicitante;
            oParametros.company_id = _vcompania;


            List<E_Nw_Consolidado_PromotionalActivities> oLs = new E_Nw_Consolidado_PromotionalActivities().Consul_ActividadesPromocionales(
                    new Request_ActividadesPromocionales()
                    {
                        oParametros = oParametros
                    });

            return Json(new { Archivo = oLs });
        }

        public ActionResult Consulta_Nw_InsActividadesPromocionales(string name_promocion, int cod_marca, int cod_typePromotion, int cod_solicitante, string rd, string cod_cobertura, string premio, string documento_apertura, string da_insumo, string da_url, string documento_cierre, string dc_insumo, string dc_url, int estado_promocion, string pendiente_apertura, string pendiente_cierre)
        {
            ViewBag.i_name_promocion = name_promocion;
            ViewBag.i_cod_marca = cod_marca;
            ViewBag.i_cod_typePromotion = cod_typePromotion;
            ViewBag.i_cod_solicitante = cod_solicitante;
            ViewBag.i_rd = rd;
            ViewBag.i_cod_cobertura = cod_cobertura;
            ViewBag.i_premio = premio;
            ViewBag.i_documento_apertura = documento_apertura;
            ViewBag.i_da_insumo = da_insumo;
            ViewBag.i_da_url = da_url;
            ViewBag.i_documento_cierre = documento_cierre;
            ViewBag.i_dc_insumo = dc_insumo;
            ViewBag.i_dc_url = dc_url;
            ViewBag.i_estado_promocion = estado_promocion;
            ViewBag.i_pendiente_apertura = pendiente_apertura;
            ViewBag.i_pendiente_cierre = pendiente_cierre;
            ViewBag.i_USER = ((Persona)Session["Session_Login"]).name_user.ToString();
            ViewBag.i_company_id = Convert.ToInt32(((Persona)Session["Session_Login"]).Company_id);
            return View();
        }

        [HttpPost]
        public ActionResult Carga_Documento(HttpPostedFileBase __f)
        {
            string _carpeta = "";
            string _fileLocal = "";
            string _doc_url = "";
            
            if (__f != null)
            {
                _fileLocal = IO.Path.GetFileName(__f.FileName);
                _carpeta = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                /*Crea directorio para nuevo Promocion*/
                IO.Directory.CreateDirectory(IO.Path.Combine(Server.MapPath("~/Temp/Promocion/"), _carpeta));

                string vtipoarchi = _fileLocal.Substring(_fileLocal.IndexOf("."), 4);
                if (vtipoarchi ==".xls")
                {
                    /*Guarda archivo en nueva ubicacion de Promocion*/
                    __f.SaveAs(IO.Path.Combine(Server.MapPath("~/Temp/Promocion/"), _carpeta + "/Excel.xlsx"));
                    _doc_url = "/Temp/Promocion/" + _carpeta + "/Excel.xlsx";
                }
                else {
                    __f.SaveAs(IO.Path.Combine(Server.MapPath("~/Temp/Promocion/"), _carpeta + "/PDF.pdf"));
                    _doc_url = "/Temp/Promocion/" + _carpeta + "/PDF.pdf";
                }
            }


            var headerAcceptVariable1 = this.Request.Headers["ACCEPT"] as string;

            if (headerAcceptVariable1 != null && headerAcceptVariable1.Contains("application/json"))
            {
                return Json(_doc_url);
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var response = serializer.Serialize(_doc_url);

                return Content(response);
            }
        }

        //Gantt
        public JsonResult Guardar_DetalleGantt(string DataItems, int cod_promocion)
        {
            List<E_Nw_Detail_Gantt> oListDetailGantt = new List<E_Nw_Detail_Gantt>();

            var jss = new System.Web.Script.Serialization.JavaScriptSerializer();
            dynamic Items = jss.Deserialize<dynamic>(DataItems);
            var vidgantt = "";
            foreach (var item in Items["tasks"])
            {
                E_Nw_Detail_Gantt oDetailGantt = new E_Nw_Detail_Gantt();
                vidgantt += Convert.ToString(item["id"]) + ",";
                oDetailGantt.id = Convert.ToString(item["id"]);
                oDetailGantt.name = item["name"];
                oDetailGantt.code = item["code"];
                oDetailGantt.level = Convert.ToInt32(item["level"]);
                oDetailGantt.status = item["status"];
                oDetailGantt.canWrite = Convert.ToBoolean(item["canWrite"]);
                oDetailGantt.start = Convert.ToString(item["start"]);
                oDetailGantt.duration = Convert.ToInt32(item["duration"]);
                oDetailGantt.gend = Convert.ToString(item["end"]);
                oDetailGantt.startIsMilestone = Convert.ToBoolean(item["startIsMilestone"]);
                oDetailGantt.endIsMilestone = Convert.ToBoolean(item["endIsMilestone"]);
                if (item.ContainsKey("collapsed"))
                {
                    oDetailGantt.collapsed = Convert.ToBoolean(item["collapsed"]);
                }
                else {
                    oDetailGantt.collapsed = false;
                }
                
                oDetailGantt.assigs = null;
                oDetailGantt.hasChild = Convert.ToBoolean(item["hasChild"]);
                oDetailGantt.progress = Convert.ToInt32(item["progress"]);
                oDetailGantt.description = Convert.ToString(item["description"]);
                oDetailGantt.depends = Convert.ToString(item["depends"]);
                oDetailGantt.Promotion_CreateBy = ((Persona)Session["Session_Login"]).name_user.ToString();
                oListDetailGantt.Add(oDetailGantt);                
            }
            vidgantt = vidgantt.Substring(0, vidgantt.Length - 1);

            E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
            if (oListDetailGantt.Count > 0)
            {
                StatusRegistro = new E_Nw_DetailGanttRegistro().Registrar_DetailGantt(
                   new Request_DetailGantt()
                   {
                       oDetailGantt = oListDetailGantt,
                       cod_promocion = cod_promocion,
                       strlist_promocion = vidgantt
                   });
            }
            else
            {
                StatusRegistro.CodStatus = E_Nw_DetailGanttRegistro.GENERAL_ERROR;
                StatusRegistro.Mensaje = "No ha digitado ningún dato, Columna Exhibición es Obligatorio.";
            }
            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
            //return Json("");
        }

        //Llenar Gantt
        [HttpPost]
        public ActionResult JsonLlenarGantt(int cod_promocion)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new E_Nw_DetailGanttAssingLlenado().LlenarGantt(
                    new Request_GanttActivProm()
                    {
                        cod_promocion = cod_promocion,
                        cod_analyst = ((Persona)(Session["Session_Login"])).tpf_id
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Consulta_DocumentosPendientes_AP(int cod_promocion)
        {
            E_Nw_PromotionalActivities oParam_ActProm = new E_Nw_PromotionalActivities();

            oParam_ActProm.id_promotional = cod_promocion;

            List<E_Nw_Consolidado_PromotionalActivities> oLs = new E_Nw_Consolidado_PromotionalActivities().Consul_DocumentosPendientes(
                    new Request_ActividadesPromocionales()
                    {
                        oParametros = oParam_ActProm
                    });

            return Json(new { Archivo = oLs });
        }

        public ActionResult GetEditActivityPromotional(int cod_promocion) {
            E_Nw_PromotionalActivities oParametros = new E_Nw_PromotionalActivities();

            oParametros.id_promotional = cod_promocion;

            return View(new E_NW_Edit_PromotionalActivities().EditPromotionalActivities(
                    new Request_ActividadesPromocionales()
                    {
                        oParametros = oParametros
                    }
                ));
        }

        public JsonResult Deshabilitar_Promocion(int cod_promocion)
        {
            E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
            StatusRegistro = new E_Nw_DetailGanttRegistro().Delete_Promotion(
                   new Request_DeletePromotion()
                   {
                       cod_promocion = cod_promocion,
                       user_modiby = ((Persona)Session["Session_Login"]).name_user.ToString()
                   });
            
            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Editar_Promocion(int id_promotion, string name_promotion, int id_brand, int id_typepromotion, int id_applicant, string rd, string cod_cobertura, string premios, string doc_apertura, string da_insumo, string da_url, string documento_cierre, string dc_insumo, string dc_url, string estado_promocion, string pendiente_apertura, string pendiente_cierre)
        {
            
            E_NW_Editar_PromotionalActivities oParametros = new E_NW_Editar_PromotionalActivities();
            oParametros.id_promotion = id_promotion;
            oParametros.name_promotion = name_promotion;
            oParametros.id_brand =id_brand;
            oParametros.id_typepromotion = id_typepromotion;
            oParametros.id_applicant = id_applicant;
            oParametros.rd = rd;
            oParametros.cod_cobertura = cod_cobertura;
            oParametros.premios = premios;
            oParametros.doc_apertura = doc_apertura;
            oParametros.da_insumo = da_insumo;
            oParametros.da_url = da_url;
            oParametros.documento_cierre = documento_cierre;
            oParametros.dc_insumo = dc_insumo;
            oParametros.dc_url = dc_url;
            oParametros.estado_promocion = estado_promocion;
            oParametros.pendiente_apertura = pendiente_apertura;
            oParametros.pendiente_cierre=pendiente_cierre;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
            StatusRegistro = new E_Nw_DetailGanttRegistro().Edit_Promotion(
                   new Request_EditPromotion()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }
        
        /*[HttpPost]
        public JsonResult Descarga_Promocion()
        {
            string _filePath = "";
            string _fileServer = "";
            string _fileFormatServer = "";

            try
            {
                _fileServer = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);
                _fileFormatServer = System.IO.Path.Combine(Server.MapPath("/Temp/Promocion/"), "06062015_013749" + "\\PDF.pdf");
                _filePath = System.IO.Path.Combine(Server.MapPath("/Temp/Promocion/"), _fileServer);
                System.IO.File.Copy(_fileFormatServer, _filePath + ".pdf", true);
            }
            catch (Exception e)
            {
                e.Message.ToString();
                _fileServer = "";
            }

            return Json(new { Archivo = _fileServer });
        }*/

        [HttpPost]
        public JsonResult ConsolidadoPromocionDescarga(int _vmarca, int _vanio, int _vactividad, int _vestado, int _vmes, int _vsolicitante)
        {
            E_Nw_PromotionalActivities oParametros = new E_Nw_PromotionalActivities();

            oParametros.id_Brand = _vmarca;
            oParametros.anio = _vanio;
            oParametros.id_typeactivity = _vactividad;
            oParametros.id_state = _vestado;
            oParametros.mes = _vmes;
            oParametros.id_solicitante = _vsolicitante;
            oParametros.company_id = ((Persona)Session["Session_Login"]).Company_id;

            string _filePath = "";
            string _fileServer = "";
            string _fileFormatServer = "";

            int _fila = 2;

            _fileServer = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _fileFormatServer = Path.Combine(Server.MapPath("/Format"), "lucky-Act-Promocional.xlsx");
            _filePath = Path.Combine(Server.MapPath("/Temp/Promocion/Excel"), _fileServer);

            /*copia formato y lo ubica en carpeta temporal*/
            System.IO.File.Copy(_fileFormatServer, _filePath, true);

            /*instancia archivo para su uso*/
            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                {
                    if (oWs.Name == "Consolidado")
                    {
                        List<E_Nw_Consolidado_PromotionalActivities> oDl = new E_Nw_Consolidado_PromotionalActivities().Consul_ActividadesPromocionales(
                                    new Request_ActividadesPromocionales()
                                    {
                                        oParametros = oParametros
                                    });

                            foreach (E_Nw_Consolidado_PromotionalActivities oDa in oDl)
                            {
                                oWs.Cells[_fila, 1].Value = oDa.id_row;
                                oWs.Cells[_fila, 2].Value = oDa.promocion;
                                oWs.Cells[_fila, 3].Value = oDa.marca;
                                oWs.Cells[_fila, 4].Value = oDa.tipo_promocion;
                                oWs.Cells[_fila, 5].Value = oDa.solicitante;

                                oWs.Cells[_fila, 6].Value = oDa.nrd;
                                oWs.Cells[_fila, 7].Value = oDa.cobertura;
                                oWs.Cells[_fila, 8].Value = oDa.fechainicio;
                                oWs.Cells[_fila, 9].Value = oDa.fechatermino;
                                oWs.Cells[_fila, 10].Value = oDa.estado;
                                oWs.Cells[_fila, 11].Value = ((Convert.ToString(oDa.rd) == null || Convert.ToString(oDa.rd) == "") ? "NO" : "SI");
                                oWs.Cells[_fila, 12].Value = ((Convert.ToString(oDa.brief) == null || Convert.ToString(oDa.brief) == "") ? "NO" : "SI");
                                oWs.Cells[_fila, 13].Value = ((Convert.ToString(oDa.pendiente) == "1") ? "SI" : "NO");
                                _fila++;
                            }
                        //}

                        //Formato Cabecera
                        oWs.Row(1).Height = 25;
                        oWs.Row(1).Style.Font.Bold = true;
                        oWs.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWs.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWs.Column(1).AutoFit();
                        oWs.Column(2).Width = 50;
                        oWs.Column(3).Width = 30;
                        oWs.Column(4).Width = 40;
                        oWs.Column(5).Width = 20;
                        oWs.Column(6).Width = 40;
                        oWs.Column(7).Width = 50;
                        oWs.Column(8).AutoFit();
                        oWs.Column(9).AutoFit();
                        oWs.Column(10).Width = 20;
                        oWs.Column(11).AutoFit();
                        oWs.Column(12).AutoFit();
                        oWs.Column(13).AutoFit();

                        oWs.Protection.AllowInsertRows = false;
                        oWs.Protection.AllowInsertColumns = false;
                        oWs.Protection.AllowDeleteColumns = false;
                        oWs.Protection.AllowPivotTables = false;
                        oWs.Protection.AllowSelectLockedCells = true;
                        oWs.Protection.AllowSelectUnlockedCells = true;
                        oWs.Protection.AllowAutoFilter = true;
                        oWs.Protection.AllowSort = false;
                        oWs.Protection.IsProtected = true;
                    }
                }

                oEx.Save();
            }

            return Json(new { Archivo = _fileServer });
        }

        public JsonResult Generar_Cod_Promocion()
        {
            E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
            StatusRegistro = new E_Nw_DetailGanttRegistro().Generar_Cod_Promotion();

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Actualizados_Promocion(int cod_promocion)
        {
            //return Content(new ContentResult
            //{
            //    Content = MvcApplication._Serialize(new E_NW_Actualizados_PromotionalActivities().ActualizadosPromotionalActivities(
            //        new Request_ActualizadosActivProm()
            //        {
            //            cod_promocion = cod_promocion
            //        }
            //    )),
            //    ContentType = "application/json"
            //}.Content);

            List<E_NW_Actualizados_PromotionalActivities> oLs = new E_NW_Actualizados_PromotionalActivities().ActualizadosPromotionalActivities(
                   new Request_ActualizadosActivProm()
                   {
                       cod_promocion = cod_promocion
                   });

            return Json(new { Archivo = oLs });

        }

        #region Mantenimiento

        public ActionResult Listado_Marca()
        {
            List<E_NW_Brand> oLs = new E_NW_Brand().Listar_Marca();
            return Json(new { Archivo = oLs });
        }

        public JsonResult Insertar_Marca(int cod_company, string name_brand)
        {
            E_NW_Request_Brand oParametros = new E_NW_Request_Brand();
            oParametros.Company_id = cod_company;
            oParametros.Brand_Name = name_brand;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Insert_Marca(
                   new Request_InsertBrand()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Consulta_Marca(int cod_marca)
        {
            E_NW_Request_Brand oParametros = new E_NW_Request_Brand();
            oParametros.id_Brand = cod_marca;
            List<E_NW_Brand> oLs = new E_NW_Brand().Consulta_Marca(
                new Request_InsertBrand()
                {
                    oParametros = oParametros
                });
            return Json(new { Archivo = oLs });
        }

        public JsonResult Actualizar_Marca(int cod_marca, string name_brand)
        {
            E_NW_Request_Brand oParametros = new E_NW_Request_Brand();
            oParametros.id_Brand = cod_marca;
            oParametros.Brand_Name = name_brand;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Update_Marca(
                   new Request_InsertBrand()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Eliminar_Marca(int cod_marca, string opcion)
        {
            E_NW_Request_Brand oParametros = new E_NW_Request_Brand();
            oParametros.id_Brand = cod_marca;
            oParametros.opcion = "0";
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Delete_Marca(
                   new Request_InsertBrand()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Listado_TipoPromocion()
        {
            List<E_NW_TypePromotion> oLs = new E_NW_TypePromotion().Listar_TipoPromocion();
            return Json(new { Archivo = oLs });
        }
        public JsonResult Insertar_TipoPromocion(int cod_company, string name_typepromotion)
        {
            E_NW_Request_TypePromotion oParametros = new E_NW_Request_TypePromotion();
            oParametros.Company_id = cod_company;
            oParametros.Name_typepromotion = name_typepromotion;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Insert_TipoPromocion(
                   new Request_InsertTipoPromocion()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Consulta_TipoPromocion(int cod_tipopromocion)
        {
            E_NW_Request_TypePromotion oParametros = new E_NW_Request_TypePromotion();
            oParametros.Id_typePromotion = cod_tipopromocion;
            List<E_NW_TypePromotion> oLs = new E_NW_TypePromotion().Consulta_TipoPromocion(
                new Request_InsertTipoPromocion()
                {
                    oParametros = oParametros
                });
            return Json(new { Archivo = oLs });
        }
        public JsonResult Actualizar_TipoPromocion(int cod_tipopromocion, string name_typepromotion)
        {
            E_NW_Request_TypePromotion oParametros = new E_NW_Request_TypePromotion();
            oParametros.Id_typePromotion = cod_tipopromocion;
            oParametros.Name_typepromotion = name_typepromotion;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Update_TipoPromocion(
                   new Request_InsertTipoPromocion()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Eliminar_TipoPromocion(int cod_tipopromocion)
        {
            E_NW_Request_TypePromotion oParametros = new E_NW_Request_TypePromotion();
            oParametros.Id_typePromotion = cod_tipopromocion;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Delete_TipoPromocion(
                   new Request_InsertTipoPromocion()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Listado_Solicitante()
        {
            List<E_NW_Area> oLs = new E_NW_Area().Listar_Solicitante();
            return Json(new { Archivo = oLs });
        }
        public JsonResult Insertar_Solicitante(int cod_company, string name_solicitante)
        {
            E_NW_Request_Area oParametros = new E_NW_Request_Area();
            oParametros.Company_id = cod_company;
            oParametros.Name_Area = name_solicitante;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Insert_Solicitante(
                   new Request_InsertArea()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Consulta_Solicitante(int cod_solicitante)
        {
            E_NW_Request_Area oParametros = new E_NW_Request_Area();
            oParametros.Id_Area = cod_solicitante;
            List<E_NW_Area> oLs = new E_NW_Area().Consulta_Solicitante(
                new Request_InsertArea()
                {
                    oParametros = oParametros
                });
            return Json(new { Archivo = oLs });
        }
        public JsonResult Actualizar_Solicitante(int cod_solicitante, string name_solicitante)
        {
            E_NW_Request_Area oParametros = new E_NW_Request_Area();
            oParametros.Id_Area = cod_solicitante;
            oParametros.Name_Area = name_solicitante;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Update_Solicitante(
                   new Request_InsertArea()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Eliminar_Solicitante(int cod_solicitante)
        {
            E_NW_Request_Area oParametros = new E_NW_Request_Area();
            oParametros.Id_Area = cod_solicitante;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Delete_Solicitante(
                   new Request_InsertArea()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Listado_Document()
        {
            List<E_NW_Document> oLs = new E_NW_Document().Listar_Document();
            return Json(new { Archivo = oLs });
        }
        public JsonResult Insertar_Document(int cod_estado, int cod_company, string name_document)
        {
            E_NW_Request_Document oParametros = new E_NW_Request_Document();
            oParametros.id_StateDocument = cod_estado;
            oParametros.Name_Document = name_document;
            oParametros.Company_id = cod_company;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Insert_Document(
                   new Request_InsertDocument()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Consulta_Document(int cod_document)
        {
            E_NW_Request_Document oParametros = new E_NW_Request_Document();
            oParametros.Id_Document = cod_document;
            List<E_NW_Document> oLs = new E_NW_Document().Consulta_Document(
                new Request_InsertDocument()
                {
                    oParametros = oParametros
                });
            return Json(new { Archivo = oLs });
        }
        public JsonResult Actualizar_Document(int cod_document, int cod_estado, string name_Document)
        {
            E_NW_Request_Document oParametros = new E_NW_Request_Document();
            oParametros.Id_Document = cod_document;
            oParametros.id_StateDocument = cod_estado;
            oParametros.Name_Document = name_Document;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Update_Document(
                   new Request_InsertDocument()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Eliminar_Document(int cod_document)
        {
            E_NW_Request_Document oParametros = new E_NW_Request_Document();
            oParametros.Id_Document = cod_document;
            oParametros.user = ((Persona)Session["Session_Login"]).name_user.ToString();

            E_Nw_DetailGanttMantenimiento StatusRegistro = new E_Nw_DetailGanttMantenimiento();
            StatusRegistro = new E_Nw_DetailGanttMantenimiento().Delete_Document(
                   new Request_InsertDocument()
                   {
                       oParametros = oParametros
                   });

            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
