﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora.Models;
using Lucky.Xplora.Models.AlicorpMulticategoriaCanje;

using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
//using Microsoft.Office.Interop.Excel;
using System.Drawing;

namespace Lucky.Xplora.Controllers
{
    public class AlicorpMulticategoriaCanjeController : Controller
    {
        string LocalFormat = Convert.ToString(ConfigurationManager.AppSettings["LocalFormat"]);
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        const string ArchivoFormatoCanje = "Lucky-Alicorp-Canje.xlsx";

        #region Canje multicategoria
        /// <summary>
        /// Autor: curiarte
        /// Fecha: 2015-12-22
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Datamercaderista()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            //Se registra visita
            Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        /// <summary>
        /// Autor: curiarte
        /// Fecha: 2015-12-22
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Datamercaderista(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __j, string __k, string __l)
        {
            if (__f.Equals("0") || __f.Equals("2"))
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Datamercaderista().
                        Lista(new Request_AlicorpMulticategoriaCanjeData()
                        {
                            campania = "",
                            anio = Convert.ToInt32(__a),
                            mes = Convert.ToInt32(__b),
                            periodo = Convert.ToInt32(__c),
                            oficina = Convert.ToInt32(__d),
                            tipoperf = Convert.ToInt32(__e),
                            opcion = Convert.ToInt32(__f),
                            coddetalle = Convert.ToInt32(__g),
                            validado = Convert.ToInt32(__h),
                            dia = Convert.ToInt32(__j),
                            codgie = Convert.ToInt32(__k),
                            pdv = __l
                        })),
                    ContentType = "application/json"
                }.Content);
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Datamercaderista().
                        Actualiza(new Request_AlicorpMulticategoriaCanjeData()
                        {
                            campania = "",
                            anio = Convert.ToInt32(__a),
                            mes = Convert.ToInt32(__b),
                            periodo = Convert.ToInt32(__c),
                            oficina = Convert.ToInt32(__d),
                            tipoperf = Convert.ToInt32(__e),
                            opcion = Convert.ToInt32(__f),
                            coddetalle = Convert.ToInt32(__g),
                            validado = Convert.ToInt32(__h),
                            dia = Convert.ToInt32(__j),
                            codgie = Convert.ToInt32(__k)
                        })),
                    ContentType = "application/json"
                }.Content);
            }
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-12-23
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Reporting()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                ViewBag.campania_descripcion = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).First().Planning_Name;
            }

            //Se registra visita
            Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-01-11
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Reporting(string __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Reporting_Campania().Objeto(
                    new Request_AlicorpMulticategoriaCanjeParametro()
                    {
                        Objeto = new Parametro()
                        {
                            opcion = Convert.ToInt32(__a),
                            parametro = __b
                        }
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-01-29
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Descarga(string __a, string __b, string __opcion) {
            if (__a == null || __a.Length == 0) return null;

            int fila = 0;
            int columna = 0;
            int columnaAnterior = 0;
            string ruta_origen = "";
            string ruta_destino = "";
            string archivo_nombre = "";
            List<string> parametro;
            int perfil = ((Persona)Session["Session_Login"]).tpf_id;

            archivo_nombre = DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";

            if (Convert.ToInt32(__opcion) == 0)
            {
                #region Reporting
                Reporting oRe = MvcApplication._Deserialize<Reporting>(__a);

                #region Crea copia de formato
                ruta_origen = Path.Combine(LocalFormat, ArchivoFormatoCanje);
                ruta_destino = Path.Combine(LocalTemp, archivo_nombre);
                System.IO.File.Copy(ruta_origen, ruta_destino, true);
                #endregion

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(ruta_destino)))
                {
                    #region One page
                    Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets["Avance de Canjes"];

                    oWs.Cells[3, 4].Value = __b;

                    #region Mensual
                    fila = 7;
                    foreach (Reporting_Campania oBj in oRe.rotacion)
                    {
                        oWs.InsertRow(fila, 6);
                        columna = 4;

                        foreach (Reporting_Campania_Mes oMe in oBj.meses)
                        {
                            if (oRe.rotacion.First() == oBj)
                            {
                                oWs.Cells[fila - 1, columna].Value = oMe.mes;
                                oWs.Cells[fila - 1, columna].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila - 1, columna].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#1ABC9C"));
                            }
                            if (oBj.meses.First() == oMe)
                            {
                                oWs.Cells[fila, 2].Value = oBj.cam_descripcion;
                                oWs.Cells[fila, 2, fila + 5, 2].Merge = true;

                                oWs.Cells[fila, 3].Value = "PLAN";
                                oWs.Cells[fila + 1, 3].Value = "SELL OUT";
                                oWs.Cells[fila + 2, 3].Value = "OBJETIVO";
                                oWs.Cells[fila + 3, 3].Value = "ROT VS PLAN";
                                oWs.Cells[fila + 4, 3].Value = "ROTACION S/.";
                                oWs.Cells[fila + 4, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila + 4, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#2ECC71"));
                                oWs.Cells[fila + 5, 3].Value = "RATIO S/. x Tn";
                                oWs.Cells[fila + 5, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila + 5, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#E67E22"));
                            }

                            oWs.Cells[fila, columna].Value = oMe.mes_pla_monto.Replace(" ton", "");
                            oWs.Cells[fila + 1, columna].Value = oMe.mes_rotacion_peso.Replace(" ton", "");
                            oWs.Cells[fila + 2, columna].Value = oMe.mes_obj_monto;
                            oWs.Cells[fila + 3, columna].Value = oMe.mes_rotacion_plan.Split(new string[] { "</i>" }, StringSplitOptions.None).Last().ToString();
                            oWs.Cells[fila + 4, columna].Value = oMe.mes_rotacion_moneda;
                            oWs.Cells[fila + 4, columna].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            oWs.Cells[fila + 4, columna].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#2ECC71"));
                            oWs.Cells[fila + 5, columna].Value = oMe.mes_ratio;
                            oWs.Cells[fila + 5, columna].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            oWs.Cells[fila + 5, columna].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#E67E22"));

                            columna += 1;
                        }

                        fila += 6;
                    }
                    #endregion

                    #region Periodo
                    fila += 3;
                    columna = 4;
                    foreach (Reporting_Campania oBj in oRe.rotacion)
                    {
                        oWs.InsertRow(fila, 5);
                        columna = 4;

                        foreach (Reporting_Campania_Mes oMe in oBj.meses)
                        {
                            if (oRe.rotacion.First() == oBj)
                            {
                                oWs.Cells[fila - 2, columna].Value = oMe.mes;
                                oWs.Cells[fila - 2, columna, fila - 2, columna + (oMe.periodo.Count - 1)].Merge = true;
                                oWs.Cells[fila - 2, columna].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila - 2, columna].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#1ABC9C"));
                            }
                            if (oBj.meses.First() == oMe)
                            {
                                oWs.Cells[fila, 2].Value = oBj.cam_descripcion;
                                oWs.Cells[fila, 2, fila + 4, 2].Merge = true;

                                oWs.Cells[fila, 3].Value = "PLAN";
                                oWs.Cells[fila + 1, 3].Value = "SELL OUT";
                                oWs.Cells[fila + 2, 3].Value = "ROT VS PLAN";
                                oWs.Cells[fila + 3, 3].Value = "ROTACION S/.";
                                oWs.Cells[fila + 3, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila + 3, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#2ECC71"));
                                oWs.Cells[fila + 4, 3].Value = "RATIO S/. x Tn";
                                oWs.Cells[fila + 4, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila + 4, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#E67E22"));
                            }

                            foreach (Reporting_Campania_Mes_Periodo oPe in oMe.periodo)
                            {
                                if (oRe.rotacion.First() == oBj)
                                {
                                    oWs.Cells[fila - 1, columna].Value = oPe.orden;
                                    oWs.Cells[fila - 1, columna].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                    oWs.Cells[fila - 1, columna].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#1ABC9C"));
                                }

                                oWs.Cells[fila, columna].Value = oMe.mes_pla_monto.Replace(" ton", "");
                                oWs.Cells[fila + 1, columna].Value = oPe.periodo_rotacion_peso.Replace(" ton", "");
                                oWs.Cells[fila + 2, columna].Value = oPe.periodo_rotacion_plan.Split(new string[] { "</i>" }, StringSplitOptions.None).Last().ToString();
                                oWs.Cells[fila + 3, columna].Value = oPe.periodo_rotacion_moneda;
                                oWs.Cells[fila + 3, columna].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila + 3, columna].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#2ECC71"));
                                oWs.Cells[fila + 4, columna].Value = oPe.periodo_ratio;
                                oWs.Cells[fila + 4, columna].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila + 4, columna].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#E67E22"));

                                columna += 1;
                            }
                        }

                        fila += 5;
                    }
                    #endregion
                    #endregion

                    #region Desglose por ciudad
                    Excel.ExcelWorksheet oWsDc = oEx.Workbook.Worksheets["Desgloce por ciudad"];

                    fila = 4;
                    columna = 2;
                    columnaAnterior = 2;

                    foreach (Reporting_Campania_Oficina oDe in oRe.desglose)
                    { 
                        foreach (ReportingCampaniaOficinaPdv oPd in oDe.pdv) {
                            if (oPd.pdv_codigo == "0") {
                                oWsDc.Cells[fila, 1].Value = oDe.ofi_descripcion;
                            }else{
                                oWsDc.Cells[fila, 1].Value = oPd.pdv_descripcion;
                            }

                            foreach (ReportingCampaniaOficinaPdvCampania oCa in oPd.campania) {
                                columnaAnterior = columna;

                                if (oDe == oRe.desglose.First() && oPd == oDe.pdv.First())
                                {
                                    oWsDc.Cells[1, columna].Value = oCa.cam_descripcion;
                                }

                                foreach (ReportingCampaniaOficinaPdvCampaniaCategoria oCt in oCa.categoria) {
                                    if (oDe == oRe.desglose.First() && oPd == oDe.pdv.First()) { 
                                        oWsDc.Cells[2, columna].Value = oCt.cat_descripcion;
                                        oWsDc.Cells[2, columna, 2, columna + 2].Merge = true;
                                        oWsDc.Cells[3, columna].Value = "PLAN";
                                        oWsDc.Cells[3, columna + 1].Value = "SELL OUT";
                                        oWsDc.Cells[3, columna + 2].Value = "ROT VS PLAN";
                                    }

                                    oWsDc.Cells[fila, columna].Value = oCt.mes_pla_monto;
                                    oWsDc.Cells[fila, columna + 1].Value = oCt.mes_rotacion_peso;
                                    oWsDc.Cells[fila, columna + 2].Value = oCt.mes_rotacion_plan.Replace("<i class=\"fa fa-check-circle\"></i>","").Replace("<i class=\"fa fa-exclamation-circle\"></i>","").Replace("<i class=\"fa fa-times-circle\"></i>","");

                                    columna += 3;
                                }

                                if (oDe == oRe.desglose.First() && oPd == oDe.pdv.First())
                                {
                                    oWsDc.Cells[1, columnaAnterior, 1, columna -1].Merge = true;
                                }
                            }

                            fila++;
                            columna = 2;
                        }
                    }
                    #endregion

                    #region Canje diario - Oficina
                    Excel.ExcelWorksheet oWsCd = oEx.Workbook.Worksheets["Canje diario - Oficina"];

                    fila = 2;
                    columna = 2;

                    foreach (ReportingCampaniaCanjeCabecera oCa in oRe.canje.oficina) {
                        oWsCd.Cells[fila, 1].Value = oCa.descripcion;

                        foreach (ReportingCampaniaCanjeDetalle oDe in oCa.detalle) {
                            if (oCa == oRe.canje.oficina.First())
                            {
                                oWsCd.Cells[1, columna].Value = oDe.fecha;
                            }

                            oWsCd.Cells[fila, columna].Value = oDe.canje;

                            columna++;
                        }

                        fila++;
                        columna = 2;
                    }
                    #endregion

                    #region Canje diario - Gestor
                    Excel.ExcelWorksheet oWsGe = oEx.Workbook.Worksheets["Canje diario - Gestor"];

                    fila = 2;
                    columna = 2;

                    foreach (ReportingCampaniaCanjeCabecera oCa in oRe.canje.gestor)
                    {
                        oWsGe.Cells[fila, 1].Value = oCa.descripcion;

                        foreach (ReportingCampaniaCanjeDetalle oDe in oCa.detalle)
                        {
                            if (oCa == oRe.canje.gestor.First())
                            {
                                oWsGe.Cells[1, columna].Value = oDe.fecha;
                            }

                            oWsGe.Cells[fila, columna].Value = oDe.canje;

                            columna++;
                        }

                        fila++;
                        columna = 2;
                    }
                    #endregion

                    oEx.Save();
                }
                #endregion
            }
            else if (Convert.ToInt32(__opcion) == 1) {
                #region Datamercaderista
                parametro = __b.Split(',').ToList<string>();

                List<Datamercaderista> lOb = new Datamercaderista().
                        Lista(new Request_AlicorpMulticategoriaCanjeData()
                        {
                            campania = "",
                            anio = Convert.ToInt32(parametro[0]),
                            mes = Convert.ToInt32(parametro[1]),
                            periodo = Convert.ToInt32(parametro[2]),
                            oficina = Convert.ToInt32(parametro[3]),
                            tipoperf = Convert.ToInt32(parametro[4]),
                            opcion = Convert.ToInt32(parametro[5]),
                            coddetalle = Convert.ToInt32(parametro[6]),
                            validado = Convert.ToInt32(parametro[7]),
                            dia = Convert.ToInt32(parametro[8]),
                            codgie = Convert.ToInt32(parametro[9]),
                            pdv = Convert.ToString(parametro[10])
                        });

                #region Crea copia de formato
                ruta_destino = Path.Combine(LocalTemp, archivo_nombre);
                #endregion

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(ruta_destino))) {
                    Excel.ExcelWorksheet oWs= oEx.Workbook.Worksheets.Add("Data");

                    if (perfil == 3) {
                        oWs.Cells[1, 1].Value = "ID";
                        oWs.Cells[1, 2].Value = "Fecha Relevo";
                        oWs.Cells[1, 3].Value = "Gie: ID";
                        oWs.Cells[1, 4].Value = "Gie: Usuario";
                        oWs.Cells[1, 5].Value = "Oficina";
                        oWs.Cells[1, 6].Value = "Mercado";
                        oWs.Cells[1, 7].Value = "PDV: Descripcion";
                        oWs.Cells[1, 8].Value = "PDV: Codigo";
                        oWs.Cells[1, 9].Value = "DNI";
                        oWs.Cells[1, 10].Value = "Negocio";
                        oWs.Cells[1, 11].Value = "Locacion";
                        oWs.Cells[1, 12].Value = "Nro. Factura";
                        oWs.Cells[1, 13].Value = "Campaña";
                        oWs.Cells[1, 14].Value = "Marca";
                        oWs.Cells[1, 15].Value = "Categoria";
                        oWs.Cells[1, 16].Value = "Presentacion";
                        oWs.Cells[1, 17].Value = "Producto";
                        oWs.Cells[1, 18].Value = "Sell Out";
                        oWs.Cells[1, 19].Value = "Cantidad";
                        oWs.Cells[1, 20].Value = "Monto";
                        oWs.Cells[1, 21].Value = "Premio";
                        oWs.Cells[1, 22].Value = "Validado";

                        fila = 2;
                        foreach (Datamercaderista oBj in lOb)
                        {
                            oWs.Cells[fila, 1].Value = oBj.codrepcanje;
                            oWs.Cells[fila, 2].Value = oBj.fecha_relevo;
                            oWs.Cells[fila, 3].Value = oBj.codgie;
                            oWs.Cells[fila, 4].Value = oBj.nomgie;
                            oWs.Cells[fila, 5].Value = oBj.oficina;
                            oWs.Cells[fila, 6].Value = oBj.mercado;
                            oWs.Cells[fila, 7].Value = oBj.tienda;
                            oWs.Cells[fila, 8].Value = oBj.codpdv;
                            oWs.Cells[fila, 9].Value = oBj.dni;
                            oWs.Cells[fila, 10].Value = oBj.negocio;
                            oWs.Cells[fila, 11].Value = oBj.locacion;
                            oWs.Cells[fila, 12].Value = oBj.nroboletafact;
                            oWs.Cells[fila, 13].Value = oBj.campaña;
                            oWs.Cells[fila, 14].Value = oBj.marca;
                            oWs.Cells[fila, 15].Value = oBj.categoria;
                            oWs.Cells[fila, 16].Value = oBj.presentacion;
                            oWs.Cells[fila, 17].Value = oBj.producto;
                            oWs.Cells[fila, 18].Value = oBj.gramaje.Replace(" ton", "");
                            oWs.Cells[fila, 19].Value = oBj.cantidad;
                            oWs.Cells[fila, 20].Value = oBj.monto;
                            oWs.Cells[fila, 21].Value = oBj.premio;
                            oWs.Cells[fila, 22].Value = oBj.validado;

                            fila++;
                        }
                    } else {
                        oWs.Cells[1, 1].Value = "ID";
                        oWs.Cells[1, 2].Value = "Fecha Relevo";
                        oWs.Cells[1, 3].Value = "Gie: Usuario";
                        oWs.Cells[1, 4].Value = "Oficina";
                        oWs.Cells[1, 5].Value = "Mercado";
                        oWs.Cells[1, 6].Value = "PDV: Descripcion";
                        oWs.Cells[1, 7].Value = "PDV: Codigo";
                        oWs.Cells[1, 8].Value = "DNI";
                        oWs.Cells[1, 9].Value = "Negocio";
                        oWs.Cells[1, 10].Value = "Locacion";
                        oWs.Cells[1, 11].Value = "Nro. Factura";
                        oWs.Cells[1, 12].Value = "Campaña";
                        oWs.Cells[1, 13].Value = "Marca";
                        oWs.Cells[1, 14].Value = "Categoria";
                        oWs.Cells[1, 15].Value = "Producto";
                        oWs.Cells[1, 16].Value = "Sell Out";
                        oWs.Cells[1, 17].Value = "Cantidad";
                        oWs.Cells[1, 18].Value = "Monto";
                        oWs.Cells[1, 19].Value = "Premio";
                        oWs.Cells[1, 20].Value = "Validado";

                        fila = 2;
                        foreach (Datamercaderista oBj in lOb)
                        {
                            oWs.Cells[fila, 1].Value = oBj.codrepcanje;
                            oWs.Cells[fila, 2].Value = oBj.fecha_relevo;
                            oWs.Cells[fila, 3].Value = oBj.nomgie;
                            oWs.Cells[fila, 4].Value = oBj.oficina;
                            oWs.Cells[fila, 5].Value = oBj.mercado;
                            oWs.Cells[fila, 6].Value = oBj.tienda;
                            oWs.Cells[fila, 7].Value = oBj.codpdv;
                            oWs.Cells[fila, 8].Value = oBj.dni;
                            oWs.Cells[fila, 9].Value = oBj.negocio;
                            oWs.Cells[fila, 10].Value = oBj.locacion;
                            oWs.Cells[fila, 11].Value = oBj.nroboletafact;
                            oWs.Cells[fila, 12].Value = oBj.campaña;
                            oWs.Cells[fila, 13].Value = oBj.marca;
                            oWs.Cells[fila, 14].Value = oBj.categoria;
                            oWs.Cells[fila, 15].Value = oBj.producto;
                            oWs.Cells[fila, 16].Value = oBj.gramaje.Replace(" ton", "");
                            oWs.Cells[fila, 17].Value = oBj.cantidad;
                            oWs.Cells[fila, 18].Value = oBj.monto;
                            oWs.Cells[fila, 19].Value = oBj.premio;
                            oWs.Cells[fila, 20].Value = oBj.validado;

                            fila++;
                        }
                    }

                    oEx.Save();
                }
                #endregion
            }

            return Content(new ContentResult
            {
                Content = "{ \"_a\":\"" + archivo_nombre + "\" }",
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: curiarte
        /// Fecha: 2016-01-13
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult StockCanje()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
            ViewBag.person_id= ((Persona)Session["Session_Login"]).Person_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: curiarte
        /// Fecha: 2016-01-13
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult StockCanje(int __a, int __b, string __c,int __d, int __e, int __f, int __g,int __h, int __i,int __j)
        {
            if (__a == 0 || __a == 1)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new StockCanje_PDV().
                        lista(new Request_AlicorpStockCanje()
                            {
                                opcion = __a,
                                escala = __b,
                                codpdv = __c,
                                codperson = __d,
                                codperiodo = __e,
                                codpremio = __f,
                                stockini = __g,
                                campania = __h,
                                stockhist = __i,
                                codsuperv = __j
                            })),
                    ContentType = "application/json"
                }.Content);
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new StockCanje_PDV().
                        Actualiza(new Request_AlicorpStockCanje()
                        {
                            opcion = __a,
                            escala = __b,
                            codpdv = __c,
                            codperson = __d,
                            codperiodo = __e,
                            codpremio = __f,
                            stockini = __g,
                            campania = __h,
                            stockhist = __i,
                            codsuperv = __j
                        })),
                    ContentType = "application/json"
                }.Content);
            }
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-12-23
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Filtro(string __a, string __b) {
            return Content(
                    new ContentResult
                    {
                        Content = MvcApplication._Serialize(
                            new Datamercaderista().Lista(new Request_AlicorpMulticategoriaCanjeParametro()
                            {
                                Objeto = new Parametro() { 
                                    opcion = Convert.ToInt32(__a),
                                    parametro = __b
                                }
                            })
                        ),
                        ContentType = "application/json"
                    }.Content
                );
        }
        #endregion

        #region Canje workflow
        /// <summary>
        /// Autor: amamani
        /// Fecha: 2017-01-17
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WorkFlow() {
            return View();
        }

        /// <summary>
        /// Autor: amamani
        /// Fecha: 2017-01-17
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WorkFlowPeriodo()
        {
            ViewBag.name_user = ((Persona)Session["Session_Login"]).name_user;
            ViewBag.id_concurso = WorkFlowPeriodoGetPeriodo();
            return View();
        }

        [HttpPost]
        public ActionResult WorkFlowPeriodoInsertar(string __a, string __b)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new AlicorpCanjePeriodo().InsertPeriodo(oParametro)),
                    ContentType = "application/json"
                }.Content);
        }

        [HttpPost]
        public string WorkFlowPeriodoGetPeriodo()
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = 3, parametro = "" };
            string rpl_id = (new AlicorpCanjePeriodo().InsertPeriodo(oParametro));
            return rpl_id;
        }

        [HttpPost]
        public ActionResult WorkFlowPeriodoListar(string __a, string __b)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpCanjeListaPeriodo().ListaPeriodo(oParametro)),
                ContentType = "application/json"
            }.Content);

        }

        [HttpPost]
        public ActionResult WorkFlowPeriodoExportar(string __a, string __b)
        {
            int fila = 4;
            string _fileServer = "";
            string _filePath = "";
            string _fileFormatServer = "";
            Color _colorCell = ColorTranslator.FromHtml("#000000");
            Color _colorFondo = ColorTranslator.FromHtml("#FF0000");
            Color _colorTexto = ColorTranslator.FromHtml("#ffffff");

            _fileServer = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _fileFormatServer = Path.Combine(Server.MapPath("/Format"), "Lucky_AlicorpCanjePeriodo.v1.0.xlsx");
            _filePath = Path.Combine(Server.MapPath("/Temp"), _fileServer);

            /*copia formato y lo ubica en carpeta temporal*/
            System.IO.File.Copy(_fileFormatServer, _filePath, true);

            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };
            List<AlicorpCanjeListaPeriodo> lPeriodo = new AlicorpCanjeListaPeriodo().ListaPeriodo(oParametro);

            #region Excel
            FileInfo archivo = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(archivo))
            {

                //Recorremos el excel para buscar la hoja de calculo
                foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                {
                    if (oWs.Name == "Alicorp Canje Periodo")
                    {
                        foreach (AlicorpCanjeListaPeriodo oPeriodo in lPeriodo)
                        {
                            oWs.Cells[fila, 2].Value = oPeriodo.item;
                            oWs.Cells[fila, 3].Value = oPeriodo.rpl_id;
                            oWs.Cells[fila, 4].Value = oPeriodo.descripcion_periodo;
                            oWs.Cells[2, 5].Value = Convert.ToDateTime(oPeriodo.fecha_inicial).Date.Year;
                            

                            int cont =1;
                            for (int x = 1; x <= 12; x++)
                            {
                                if (cont >= oPeriodo.mes_inicial && cont <= oPeriodo.mes_cierre) {
                                    /*oWs.Cells[fila, (cont+4)].Value = oPeriodo.descripcion_periodo;*/
                                    Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#FCE95D");
                                    oWs.Cells[fila, (cont + 4)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                    oWs.Cells[fila, (cont + 4)].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                                } else {

                                }
                                cont = cont + 1;
                            }
                            fila++;
                        }
                    }
                    /*oWs.Column(2).AutoFit();
                    oWs.Column(3).AutoFit();
                    oWs.Column(4).AutoFit();*/
                }
                oEx.Save();
            }
            #endregion

            return new ContentResult
            {
                Content = "{ \"archivo\": \"" + _fileServer + "\" }",
                ContentType = "application/json"
            };


        }

        [HttpGet]
        public ActionResult WorkFlowMecanica()
        {
            ViewBag.name_user = ((Persona)Session["Session_Login"]).name_user;
            ViewBag.id_concurso = WorkFlowPeriodoGetPeriodo();
            return View();
        }


        [HttpPost]
        public ActionResult WorkFlowMecanicaPremioLista(string __a, string __b)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpCanjeMecanica().ListaPremio(oParametro)),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult WorkFlowMecanicaPremioEnvia(string __a, string __b)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpCanjeMecanica().ListaPremio(oParametro)),
                ContentType = "application/json"
            }.Content);
        }
        
        [HttpPost]
        public ActionResult WorkFlowMecanicaListaFiltro(string __a, string __b)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpCanjeMecanicaFiltro().ListaFiltro(oParametro)),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult WorkFlowMecanicaDetalleLista(string __a, string __b)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpCanjeMecanicaFiltro().ListaFiltro(oParametro)),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public int WorkFlowMecanicaCargaPremio(string _periodo, HttpPostedFileBase FilesInput)
        {

            // ==============================================
            // Instancia objetos

            // ==============================================
            // Declaracion de variables
            int _Fila = 0;
            string _sCelda = "";
            string _sFileCliente = "";
            string _sFileServidor = "";
            string _sPath = "";
            bool _Estado = false;
            int cant_pre = 0;
            _periodo = WorkFlowPeriodoGetPeriodo();
            if (_sFileServidor != null)
            {
                // ==========================================
                // ==========================================
                // Asignacion de variables
                _sFileCliente = Path.GetFileName(FilesInput.FileName);
                _sFileServidor = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);
                // ==========================================
                // asigna la ruta de ubicacion en el servidor.
                _sPath = Path.Combine(Server.MapPath("/Temp"), _sFileServidor + ".xlsx");
                // ==========================================
                // guarda el archivo en la ruta indicada del servidor.
                FilesInput.SaveAs(_sPath);
                // ==============================================
                // instancia archivo subido
                FileInfo oFile = new FileInfo(_sPath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(oFile))
                {
                    foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                    {
                        #region Carga_Premio

                        if (oWs.Name == "AlicorpCanjeCargaPremio")
                        {
                            _Fila = 4;
                            _Estado = true;
                            do
                            {
                                _sCelda = oWs.Cells[_Fila, 2].Text;

                                if (_sCelda.Length != 0)
                                {
                                    string cadena = "";
                                            cadena = cadena + _periodo + ",";
                                            cadena = cadena + oWs.Cells[_Fila, 2].Text + ",";
                                            cadena = cadena + oWs.Cells[_Fila, 3].Text + ",";
                                            cadena = cadena + oWs.Cells[_Fila, 4].Text + ",";
                                            cadena = cadena + Convert.ToInt32(oWs.Cells[_Fila, 5].Text) + ",";
                                            cadena = cadena + Convert.ToDecimal(oWs.Cells[_Fila, 6].Text) + ",";
                                            cadena = cadena + Convert.ToDecimal(oWs.Cells[_Fila, 7].Text) + ",";
                                            cadena = cadena + ((Persona)Session["Session_Login"]).name_user + ",";
                                            cadena = cadena + DateTime.Today.ToShortDateString();

                                            ActionResult vista = WorkFlowMecanicaPremioLista("8", cadena);
                                            vista = null;

                                    _Fila++;
                                    cant_pre++;
                                }
                                else
                                {
                                    _Estado = false;
                                }
                            } while (_Estado == true);
                        }
                        #endregion

                    }
                }
            }
            //return View(/*_Informe*/);
            return cant_pre;
        }

        [HttpPost]
        public int WorkFlow_Carga_Alicorp_Canje(string _periodo, HttpPostedFileBase FilesInput)
        {
            // ==============================================
            // Instancia objetos
            List<Reporte_Response_Alicorp_Canje> _Informe = new List<Reporte_Response_Alicorp_Canje>();
            List<AlicorpCanjeListaPremio> _Reporte_Premio_Alicorp_Canje = new List<AlicorpCanjeListaPremio>();
            List<Reporte_Plan_Alicorp_Canje> _Reporte_Plan_Alicorp_Canje = new List<Reporte_Plan_Alicorp_Canje>();
            List<Reporte_Mecanica_Escala_Alicorp_Canje> _Reporte_Mecanica_Escala_Alicorp_Canje = new List<Reporte_Mecanica_Escala_Alicorp_Canje>();
            List<Reporte_Mecanica_Escala_Alicorp_Canje> _Reporte_Mecanica_Escala_Premio_Alicorp_Canje = new List<Reporte_Mecanica_Escala_Alicorp_Canje>();
            // ==============================================
            // Declaracion de variables
            DateTime _dateValue;
            Decimal _decimalValue;
            Int32 _integerValue;
            int _Fila = 0;
            string _sCelda = "";
            string _sCelda2 = "";
            string _sCelda3 = "";
            string _sFileCliente = "";
            string _sFileServidor = "";
            string _sPath = "";
            bool _Estado = false;
            string cadena = "";
            /*_periodo = "C017-03";*/
            /*_periodo = WorkFlowPeriodoGetPeriodo();*/
            if (_sFileServidor != null)
            {
            //    // ==========================================
            //    // asigna la ruta de ubicacion en el servidor.
            //    _sPath = Path.Combine(Server.MapPath("/Temp"), _sFileServidor + ".xlsx");
            //    // ==============================================
            //    // instancia archivo subido
            //    FileInfo oFile = new FileInfo(_sPath);

                // ==========================================
                // ==========================================
                // Asignacion de variables
                _sFileCliente = Path.GetFileName(FilesInput.FileName);
                _sFileServidor = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);
                // ==========================================
                // asigna la ruta de ubicacion en el servidor.
                _sPath = Path.Combine(Server.MapPath("/Temp"), _sFileServidor + ".xlsx");
                // ==========================================
                // guarda el archivo en la ruta indicada del servidor.
                FilesInput.SaveAs(_sPath);
                // ==============================================
                // instancia archivo subido
                FileInfo oFile = new FileInfo(_sPath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(oFile))
                {
                    foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                    {
                        #region Premio
                        if (oWs.Name == "AlicorpCanjeCargaPremio")
                        {
                            _Fila = 4;
                            _Estado = true;

                            do
                            {
                                _sCelda = oWs.Cells[_Fila, 2].Text;
                                if (_sCelda.Length != 0)
                                {
                                    _Reporte_Premio_Alicorp_Canje.Add(
                                        new AlicorpCanjeListaPremio()
                                        {
                                            descripcion = oWs.Cells[_Fila, 2].Text,
                                            tipo = oWs.Cells[_Fila, 3].Text,
                                            uni_med = oWs.Cells[_Fila, 4].Text,
                                            tonelada = (Decimal.TryParse(oWs.Cells[_Fila, 5].Text, out _decimalValue) ? _decimalValue : 0),
                                            uni_com = (Int32.TryParse(oWs.Cells[_Fila, 6].Text,out _integerValue) ?_integerValue : 0),
                                            precio_costo = (Decimal.TryParse(oWs.Cells[_Fila, 7].Text, out _decimalValue) ? _decimalValue : 0),
                                            precio_percibido = (Decimal.TryParse(oWs.Cells[_Fila, 8].Text, out _decimalValue) ? _decimalValue : 0),

                                        }
                                    );

                                    _Fila++;
                                }
                                else
                                {
                                    _Estado = false;
                                }
                            } while (_Estado == true);

                            if (_Reporte_Premio_Alicorp_Canje.Count > 0)
                            {
                                cadena = "";
                                cadena = cadena + _periodo + ", ";
                                cadena = cadena + (((Persona)Session["Session_Login"]).Person_id) + "";
                                foreach (Reporte_Response_Alicorp_Canje oBj in new AlicorpCanjeListaPremio().BulkCopy_Premio_Alicorp_Canje(_Reporte_Premio_Alicorp_Canje, 0, cadena))
                                {
                                    _Informe.Add(oBj);
                                }
                            }
                        }

                        #endregion

                        #region Plan
                        else if (oWs.Name == "F_PLAN")
                        {
                            _Fila = 2;
                            _Estado = true;

                            do
                            {
                                _sCelda = oWs.Cells[_Fila, 1].Text;
                                if (_sCelda.Length != 0)
                                {
                                    _Reporte_Plan_Alicorp_Canje.Add(
                                        new Reporte_Plan_Alicorp_Canje()
                                        {
                                            id_canal = (Int32.TryParse(oWs.Cells[_Fila, 1].Text, out _integerValue) ? _integerValue : 0),
                                            id_region = (Int32.TryParse(oWs.Cells[_Fila, 3].Text, out _integerValue) ? _integerValue : 0),
                                            id_oficina = (Int32.TryParse(oWs.Cells[_Fila, 5].Text, out _integerValue) ? _integerValue : 0),
                                            id_categoria = (Int32.TryParse(oWs.Cells[_Fila, 9].Text, out _integerValue) ? _integerValue : 0),
                                            cod_cliente = oWs.Cells[_Fila, 7].Text,
                                            plan_soles =  (Decimal.TryParse(oWs.Cells[_Fila, 11].Text, out _decimalValue) ? _decimalValue : 0),
                                            real_soles = (Decimal.TryParse(oWs.Cells[_Fila, 12].Text, out _decimalValue) ? _decimalValue : 0),
                                            plan_tonelada = (Decimal.TryParse(oWs.Cells[_Fila, 13].Text, out _decimalValue) ? _decimalValue : 0),
                                            real_tonelada = (Decimal.TryParse(oWs.Cells[_Fila, 14].Text, out _decimalValue) ? _decimalValue : 0)
                                        }
                                    );

                                    _Fila++;
                                }
                                else
                                {
                                    _Estado = false;
                                }
                            } while (_Estado == true);

                            if (_Reporte_Plan_Alicorp_Canje.Count > 0)
                            {
                                cadena = "";
                                cadena = cadena + _periodo + ", ";
                                cadena = cadena + (((Persona)Session["Session_Login"]).Person_id) + "";
                                foreach (Reporte_Response_Alicorp_Canje oBj in new Reporte_Plan_Alicorp_Canje().BulkCopy_Plan_Alicorp_Canje(_Reporte_Plan_Alicorp_Canje, 1,cadena))
                                {
                                    _Informe.Add(oBj);
                                }
                            }
                        }

                        #endregion

                        #region Mecanica Escala
                        else if (oWs.Name == "F_MECANICA_MAYO" || oWs.Name == "F_MECANICA_CODIS")
                        {
                            _Fila = 3;
                            _Estado = true;

                            int fila_total = 6;
                            int v_fila = 0;
                            int nro_valor_1 = 0;
                            int nro_valor_2 = 0;

                            do
                            {
                                int cont_column = 0;
                                int valor_categoria = 1;
                                int recorre = 0;
                                string v_cat = "";
                                decimal v_esc = 0;


                                oWs.Cells[_Fila, 3].Style.Numberformat.Format = "General";
                                _sCelda = oWs.Cells[_Fila, 1].Text;
                                _sCelda2 = oWs.Cells[_Fila, 2].Text;
                                _sCelda3 = oWs.Cells[_Fila, 3].Text;
                                //if ((_sCelda.Length != 0) && (_sCelda2.Length != 0) && (_sCelda3.Length != 0))
                                //{
                                    v_fila++;
                                    nro_valor_1++;
                                    nro_valor_2++;
                                    for (int i = 1; i <= 2; i++)
                                    {
                                        for (int x = 1; x <= 3; x++)
                                        {
                                            
                                            if (cont_column == 0)
                                            {

                                            }
                                            else
                                            {
                                                if (oWs.Cells[(_Fila - v_fila), (cont_column + 1)].Text == "Bonif.")
                                                    {
                                                        valor_categoria = 2;
                                                        cont_column = cont_column + 2;
                                                        break;
                                                    }
                                                else if ((oWs.Cells[(_Fila - v_fila), (cont_column + 1)].Text == "+") || (oWs.Cells[(_Fila - v_fila), (cont_column + 1)].Text == ""))
                                                    {
                                                        cont_column = cont_column + 1;
                                                    }
                                                     
                                            }

                                            if (valor_categoria == 1)
                                            {
                                                #region Galletas
                                                oWs.Cells[_Fila, 3].Style.Numberformat.Format = "General";
                                                oWs.Cells[_Fila, (cont_column == 0 ? cont_column + 5 : cont_column +2)].Style.Numberformat.Format = "General";
                                                oWs.Cells[_Fila, (cont_column == 0 ? cont_column + 7 : cont_column + 4)].Style.Numberformat.Format = "General";
                                                oWs.Cells[_Fila, (cont_column == 0 ? cont_column + 8 : cont_column + 5)].Style.Numberformat.Format = "General";
                                                _Reporte_Mecanica_Escala_Alicorp_Canje.Add(
                                                new Reporte_Mecanica_Escala_Alicorp_Canje()
                                                {
                                                    descripcion_canal = (oWs.Name == "F_MECANICA_MAYO") ? "Mayoristas" : "Codistribuidores",
                                                    descripcion_region = (oWs.Cells[_Fila, 1].Text).Trim(),
                                                    descripcion_categoria = (oWs.Cells[_Fila, 2].Text).Trim(),
                                                    nro_escala = nro_valor_1,
                                                    escala = (Decimal.TryParse(oWs.Cells[_Fila, 3].Text, out _decimalValue) ? _decimalValue : 0),
                                                    premio = (oWs.Cells[_Fila, (cont_column == 0 ? cont_column + 4 : cont_column + 1)].Text).Trim(),
                                                    cantidad = (Int32.TryParse(oWs.Cells[_Fila, (cont_column == 0 ? cont_column + 5 : cont_column + 2)].Text, out _integerValue) ? _integerValue : 0),
                                                    uni_med = oWs.Cells[_Fila, (cont_column == 0 ? cont_column + 6 : cont_column + 3)].Text,
                                                    precio = (Decimal.TryParse(oWs.Cells[_Fila, (cont_column == 0 ? cont_column + 7 : cont_column + 4)].Text, out _decimalValue) ? _decimalValue : 0),
                                                    tg = (Decimal.TryParse(oWs.Cells[_Fila, (cont_column == 0 ? cont_column + 8 : cont_column + 5)].Text, out _decimalValue) ? _decimalValue : 0)
                                                }
                                                );
                                                #endregion

                                            }
                                            else if (valor_categoria == 2)
                                            {
                                                #region Caramelos
                                                oWs.Cells[_Fila, (cont_column + 2)].Style.Numberformat.Format = "General";

                                                oWs.Cells[_Fila, (recorre == 0 ? (cont_column + 2) : (cont_column + 3))].Style.Numberformat.Format = "General";
                                                oWs.Cells[_Fila, (recorre == 0 ? (cont_column + 6) : (cont_column + 4))].Style.Numberformat.Format = "General";
                                                oWs.Cells[_Fila, (recorre == 0 ? (cont_column + 7) : (cont_column + 5))].Style.Numberformat.Format = "General";
                                                _Reporte_Mecanica_Escala_Alicorp_Canje.Add(
                                                new Reporte_Mecanica_Escala_Alicorp_Canje()
                                                {
                                                    descripcion_canal = (oWs.Name == "F_MECANICA_MAYO") ? "Mayoristas" : "Codistribuidores",
                                                    descripcion_region = (oWs.Cells[_Fila, 1].Text).Trim(),
                                                    descripcion_categoria = ((recorre == 0 ? oWs.Cells[_Fila, (cont_column + 1)].Text : v_cat)).Trim(),
                                                    nro_escala = nro_valor_2,
                                                    escala = ( recorre == 0 ? (Decimal.TryParse(oWs.Cells[_Fila, (cont_column + 2)].Text, out _decimalValue) ? _decimalValue : 0) : v_esc),
                                                    premio = (oWs.Cells[_Fila, (recorre == 0 ? (cont_column + 3) : (cont_column + 1))].Text).Trim(),
                                                    cantidad = (Int32.TryParse(oWs.Cells[_Fila, (recorre == 0 ? (cont_column + 4) : (cont_column + 2))].Text, out _integerValue) ? _integerValue : 0),
                                                    uni_med = oWs.Cells[_Fila, (recorre == 0 ? (cont_column + 5) : (cont_column + 3))].Text,
                                                    precio = (Decimal.TryParse(oWs.Cells[_Fila, (recorre == 0 ? (cont_column + 6) : (cont_column + 4))].Text, out _decimalValue) ? _decimalValue : 0),
                                                    tg = (Decimal.TryParse(oWs.Cells[_Fila, (recorre == 0 ? (cont_column + 7) : (cont_column + 5))].Text, out _decimalValue) ? _decimalValue : 0)
                                                }
                                                );
                                                #endregion
                                            }
                                            if (cont_column == 0 && valor_categoria == 1)
                                            {
                                                cont_column = cont_column + 8;
                                            }
                                            else if (recorre == 0  && valor_categoria == 2)
                                            {
                                                v_cat = oWs.Cells[_Fila, (cont_column + 1)].Text;
                                                v_esc = (Decimal.TryParse(oWs.Cells[_Fila, (cont_column + 2)].Text, out _decimalValue) ? _decimalValue : 0);
                                                recorre = 1;
                                                cont_column = cont_column + 7;
                                            }
                                            else
                                            {
                                                cont_column = cont_column + 5;
                                            }
                                        }
                                    }

                                    if (_Fila == fila_total)
                                    {
                                        fila_total = fila_total + 6;
                                        v_fila = v_fila + 2;
                                        _Fila = _Fila + 2;
                                        nro_valor_1 = 0;
                                        nro_valor_2 = 0;
                                        if (oWs.Cells[(fila_total-4), ((cont_column - cont_column) + 1)].Text == "")
                                        {
                                            _Estado = false;
                                        }

                                    }
                                    
                                    _Fila++;
                                //}
                                //else
                                //{
                                //    _Estado = false;
                                //}
                            } while (_Estado == true);

                            if (_Reporte_Mecanica_Escala_Alicorp_Canje.Count > 0)
                            {
                                cadena = "";
                                cadena = cadena + _periodo + ", ";
                                cadena = cadena + (((Persona)Session["Session_Login"]).Person_id) + "";
                                foreach (Reporte_Response_Alicorp_Canje oBj in new Reporte_Mecanica_Escala_Alicorp_Canje().BulkCopy_Mecanica_Escala_Alicorp_Canje(_Reporte_Mecanica_Escala_Alicorp_Canje, 2, cadena))
                                {
                                    _Informe.Add(oBj);
                                }
                            }
                        }

                        #endregion

                        #region Carga Mecanica Mayoristas y Codistribuidores
                        else if (oWs.Name == "F_MECANICA")
                        {
                            _Fila = 2;
                            _Estado = true;

                            int valor = 0;
                            do
                            {

                                oWs.Cells[_Fila, 16].Style.Numberformat.Format = "General";
                                oWs.Cells[_Fila, 18].Style.Numberformat.Format = "General";

                                oWs.Cells[_Fila, 19].Style.Numberformat.Format = "General";
                                oWs.Cells[_Fila, 17].Style.Numberformat.Format = "General";

                                oWs.Cells[_Fila, 20].Style.Numberformat.Format = "General";

                                _sCelda = oWs.Cells[_Fila, 1].Text;

                                /*_sCelda = oWs.Cells[_Fila, 2].Text;*/
                                if (_sCelda.Length != 0)
                                {

                                    if ((oWs.Cells[_Fila, 1].Text == oWs.Cells[(_Fila - 1), 1].Text) && (oWs.Cells[_Fila, 3].Text == oWs.Cells[(_Fila - 1), 3].Text) &&
                                        (oWs.Cells[_Fila, 5].Text == oWs.Cells[(_Fila - 1), 5].Text) && (oWs.Cells[_Fila, 7].Text == oWs.Cells[(_Fila - 1), 7].Text))
                                    {
                                        
                                    }
                                    else if ((oWs.Cells[_Fila, 1].Text == oWs.Cells[(_Fila - 1), 1].Text) && (oWs.Cells[_Fila, 2].Text == oWs.Cells[(_Fila - 1), 2].Text) &&
                                       (oWs.Cells[_Fila, 5].Text == oWs.Cells[(_Fila - 1), 5].Text) && (oWs.Cells[_Fila, 7].Text != oWs.Cells[(_Fila - 1), 7].Text))
                                    {
                                        valor++;
                                    }
                                    else
                                    {
                                        valor = 1;
                                    }


                                    _Reporte_Mecanica_Escala_Premio_Alicorp_Canje.Add(
                                        new Reporte_Mecanica_Escala_Alicorp_Canje()
                                        {

                                            
                                            id_region = (Int32.TryParse(oWs.Cells[_Fila, 3].Text, out _integerValue) ? _integerValue : 0),
                                            id_canal = (Int32.TryParse(oWs.Cells[_Fila, 1].Text, out _integerValue) ? _integerValue : 0),
                                            id_categoria = (Int32.TryParse(oWs.Cells[_Fila, 5].Text, out _integerValue) ? _integerValue : 0),
                                            cod_premio = oWs.Cells[_Fila, 11].Text,
                                            nro_escala = valor,
                                            escala = (Decimal.TryParse(oWs.Cells[_Fila, 7].Text, out _decimalValue) ? _decimalValue : 0),
                                            cantidad = (Int32.TryParse(oWs.Cells[_Fila, 14].Text, out _integerValue) ? _integerValue : 0),
                                            precio = (Decimal.TryParse(oWs.Cells[_Fila, 16].Text, out _decimalValue) ? _decimalValue : 0),

                                            tg = (Decimal.TryParse(oWs.Cells[_Fila, 18].Text, out _decimalValue) ? _decimalValue : 0),
                                            bonificacion = (Decimal.TryParse(oWs.Cells[_Fila, 19].Text, out _decimalValue) ? _decimalValue : 0),
                                            peso = (Decimal.TryParse(oWs.Cells[_Fila, 17].Text, out _decimalValue) ? _decimalValue : 0),
                                            presupuesto_region = (Decimal.TryParse(oWs.Cells[_Fila, 20].Text, out _decimalValue) ? _decimalValue : 0)

                                        }
                                    );

                                    _Fila++;
                                }
                                else
                                {
                                    _Estado = false;
                                }
                            } while (_Estado == true);

                            if (_Reporte_Mecanica_Escala_Premio_Alicorp_Canje.Count > 0)
                            {
                                cadena = "";
                                cadena = cadena + _periodo + ", ";
                                cadena = cadena + (((Persona)Session["Session_Login"]).Person_id) + "";
                                foreach (Reporte_Response_Alicorp_Canje oBj in new Reporte_Mecanica_Escala_Alicorp_Canje().BulkCopy_Mecanica_Escala_Alicorp_Canje(_Reporte_Mecanica_Escala_Premio_Alicorp_Canje, 3, cadena))
                                {
                                    _Informe.Add(oBj);
                                }
                            }
                        }

                        #endregion

                    }
                }
            }
            return _Fila;
        }


        [HttpPost]
        public int WorkFlowMecanicaCargaMecanica(string _periodo, HttpPostedFileBase FilesInput)
        {

            // ==============================================
            // Instancia objetos

            // ==============================================
            // Declaracion de variables
            int _Fila = 0;
            string _sCelda = "";
            string _sFileCliente = "";
            string _sFileServidor = "";
            string _sPath = "";
            bool _Estado = false;
            int cant_pre = 0;
            _periodo = WorkFlowPeriodoGetPeriodo();
            if (_sFileServidor != null)
            {
                // ==========================================
                // ==========================================
                // Asignacion de variables
                _sFileCliente = Path.GetFileName(FilesInput.FileName);
                _sFileServidor = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);
                // ==========================================
                // asigna la ruta de ubicacion en el servidor.
                _sPath = Path.Combine(Server.MapPath("/Temp"), _sFileServidor + ".xlsx");
                // ==========================================
                // guarda el archivo en la ruta indicada del servidor.
                FilesInput.SaveAs(_sPath);
                // ==============================================
                // instancia archivo subido
                FileInfo oFile = new FileInfo(_sPath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(oFile))
                {
                    foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                    {
                        #region Carga_Mecanica

                        if (oWs.Name == "F_MECANICA")
                        {
                            _Fila = 2;
                            _Estado = true;

                            int valor = 0;
                            do
                            {
                                oWs.Cells[_Fila, 16].Style.Numberformat.Format = "General";
                                oWs.Cells[_Fila, 18].Style.Numberformat.Format = "General";

                                oWs.Cells[_Fila, 19].Style.Numberformat.Format = "General";
                                oWs.Cells[_Fila, 17].Style.Numberformat.Format = "General";

                                _sCelda = oWs.Cells[_Fila, 1].Text;

                                if (_sCelda.Length != 0)
                                {
                                    string cadena = "";
                                    cadena = cadena + _periodo + ",";
                                    cadena = cadena + oWs.Cells[_Fila, 3].Text + ",";//Region
                                    cadena = cadena + oWs.Cells[_Fila, 1].Text + ",";//Canal
                                    cadena = cadena + oWs.Cells[_Fila, 5].Text + ",";//Categoria
                                    cadena = cadena + oWs.Cells[_Fila, 11].Text + ",";//premio

                                    //cadena = cadena + oWs.Cells[_Fila, 7].Text + ",";//nro escala

                                    if ((oWs.Cells[_Fila, 1].Text == oWs.Cells[(_Fila - 1), 1].Text) && (oWs.Cells[_Fila, 3].Text == oWs.Cells[(_Fila - 1), 3].Text) && 
                                        (oWs.Cells[_Fila, 5].Text == oWs.Cells[(_Fila - 1), 5].Text) &&  (oWs.Cells[_Fila, 7].Text == oWs.Cells[(_Fila - 1), 7].Text))
                                    {
                                        cadena = cadena + "" + valor + ",";
                                    }
                                    else if ((oWs.Cells[_Fila, 1].Text == oWs.Cells[(_Fila - 1), 1].Text) && (oWs.Cells[_Fila, 2].Text == oWs.Cells[(_Fila - 1), 2].Text) &&
                                       (oWs.Cells[_Fila, 5].Text == oWs.Cells[(_Fila - 1), 5].Text) && (oWs.Cells[_Fila, 7].Text != oWs.Cells[(_Fila - 1), 7].Text))
                                    {
                                        valor++;
                                        cadena = cadena + "" + valor + ",";
                                    }
                                    else
                                    {
                                        valor=1;
                                        cadena = cadena + "" + valor + ",";
                                    }



                                    cadena = cadena + oWs.Cells[_Fila, 7].Text + ",";//escala
                                    cadena = cadena + oWs.Cells[_Fila, 14].Text + ",";//cantidad

                                    cadena = cadena + oWs.Cells[_Fila, 16].Text + ",";//precio
                                    cadena = cadena + oWs.Cells[_Fila, 18].Text + ",";//tg
                                    cadena = cadena + oWs.Cells[_Fila, 19].Text + ",";//boni
                                    cadena = cadena + oWs.Cells[_Fila, 17].Text + ",";//peso

                                    cadena = cadena + ((Persona)Session["Session_Login"]).name_user + ",";
                                    cadena = cadena + DateTime.Today.ToShortDateString();

                                    ActionResult vista = WorkFlowMecanicaDetalleLista("6", cadena);
                                    vista = null;
                                    
                                    _Fila++;
                                    cant_pre++;
                                    
                                }
                                else
                                {
                                    _Estado = false;
                                }
                            } while (_Estado == true);
                        }
                        #endregion
                    }
                }
            }
            //return View(/*_Informe*/);
            return cant_pre;
        }

        [HttpPost]
        public ActionResult WorkFlowMecanicaExportar(string __a, string __b)
        {
            int fila = 2;
            int _position = 0;
            int cont_cat = 0;
            string _fileServer = "";
            string _filePath = "";
            string _fileFormatServer = "";
            string _periodo = "";
            _periodo = WorkFlowPeriodoGetPeriodo();
            Color _colorCell = ColorTranslator.FromHtml("#000000");
            Color _colorFondo = ColorTranslator.FromHtml("#FF0000");
            Color _colorTexto = ColorTranslator.FromHtml("#ffffff");

            _fileServer = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _fileFormatServer = Path.Combine(Server.MapPath("/Format"), "Lucky_AlicorpCanjeMecanica.v1.0.xlsx");
            _filePath = Path.Combine(Server.MapPath("/Temp"), _fileServer);

            /*copia formato y lo ubica en carpeta temporal*/
            System.IO.File.Copy(_fileFormatServer, _filePath, true);
            
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };
            List<AlicorpCanjeMecanicaFiltro> lMecanica = new AlicorpCanjeMecanicaFiltro().ListaFiltro(oParametro);
            AlicorpCanjeParametro oParametro1 = new AlicorpCanjeParametro() { opcion = 11, parametro = _periodo };
            List<AlicorpCanjeMecanicaDetalleEscala> lMecanicaDetalle = new AlicorpCanjeMecanicaDetalleEscala().ListaEscalas(oParametro1);

            #region Excel
            FileInfo archivo = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(archivo))
            {

                //Recorremos el excel para buscar la hoja de calculo
                foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                {
                    #region Alicorp Mecanica
                    if (oWs.Name == "Alicorp Canje Mecanica")
                    {
                        fila = 5;
                        foreach (AlicorpCanjeMecanicaFiltro oMecanica in lMecanica)//General
                        {
                            //oWs.Cells[fila, 2].Value = oMecanica.;

                            foreach (AlicorpCanjeCanal oCanal in oMecanica.lCanal)//Canal
                            {
                                oWs.Cells[fila, 2].Value = oCanal.descripcion_canal;
                                _position = fila;
                                foreach (AlicorpCanjeRegion oRegion in oCanal.lRegion)//Region
                                {

                                    oWs.Cells[fila, 3].Value = oRegion.descripcion_region;
                                    cont_cat = 0;
                                    foreach (AlicorpCanjeCategoria oCategoria in oRegion.lCategoria)//Categoria
                                    {

                                        if (cont_cat == 0)
                                        {

                                            foreach (AlicorpCanjeMecanicaDetalleLista oDetalle in oCategoria.lDetalleLista)
                                            {
                                                oWs.Cells[fila, 4].Value = oDetalle.escala;
                                                oWs.Cells[fila, 5].Value = oDetalle.premio;
                                                oWs.Cells[fila, 6].Value = oDetalle.total_tg;
                                            }
                                            
                                        }
                                        else
                                        {
                                            foreach (AlicorpCanjeMecanicaDetalleLista oDetalle in oCategoria.lDetalleLista)
                                            {
                                                oWs.Cells[fila, 7].Value = oDetalle.escala;
                                                oWs.Cells[fila, 8].Value = oDetalle.premio;
                                                oWs.Cells[fila, 9].Value = oDetalle.total_tg;

                                                oWs.Cells[fila, 10].Value = oDetalle.totales_escalas;
                                                oWs.Cells[fila, 11].Value = oDetalle.totales_premios;
                                                oWs.Cells[fila, 12].Value = oDetalle.totales_tgs;
                                            }
                                        }
                                        cont_cat++;
                                    }
                                    fila++;
                                }
                                oWs.Cells[fila, 3].Value = "Total";

                                oWs.Cells[fila, 6].Formula = "=SUMA(F" + _position + ":F" + (fila - 1) + ")";

                                oWs.Cells[fila, 9].Formula = "=SUMA(I" + _position + ":I" + (fila - 1) + ")";

                                oWs.Cells[fila, 12].Formula = "=SUMA(L" + _position + ":L" + (fila - 1) + ")";

                                oWs.Cells[_position, 2, (fila), 2].Merge = true;

                                Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#337AB7");
                                oWs.Cells[fila, 2, fila, 12].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila, 2, fila, 12].Style.Fill.BackgroundColor.SetColor(colFromHex1);

                                Color colFromHex3 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                                oWs.Cells[fila, 2, fila, 12].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                oWs.Cells[fila, 2, fila, 12].Style.Font.Color.SetColor(colFromHex3);


                                fila++;

                            }
                            
                        }
                    }
                    #endregion

                    #region Alicorp Detalle Mecanica
                    if (oWs.Name == "F_MECANICA")
                    {
                        fila = 2;
                        foreach (AlicorpCanjeMecanicaDetalleEscala oMecanicaDetalle in lMecanicaDetalle)//General
                        {
                            
                            oWs.Cells[fila, 1].Value = oMecanicaDetalle.id_canal;
                            oWs.Cells[fila, 2].Value = oMecanicaDetalle.descripcion_canal;
                            oWs.Cells[fila, 3].Value = oMecanicaDetalle.id_region;
                            oWs.Cells[fila, 4].Value = oMecanicaDetalle.descripcion_region;
                            oWs.Cells[fila, 5].Value = oMecanicaDetalle.id_categoria;
                            oWs.Cells[fila, 6].Value = oMecanicaDetalle.descripcion_categoria;
                            oWs.Cells[fila, 7].Value = oMecanicaDetalle.escala;

                            oWs.Cells[fila, 10].Value = oMecanicaDetalle.tipo;
                            oWs.Cells[fila, 11].Value = oMecanicaDetalle.cod_premio;
                            oWs.Cells[fila, 12].Value = oMecanicaDetalle.descripcion_premio;
                            oWs.Cells[fila, 13].Value = oMecanicaDetalle.uni_med;
                            oWs.Cells[fila, 14].Value = oMecanicaDetalle.uni_com;
                            oWs.Cells[fila, 15].Value = oMecanicaDetalle.precio_costo;
                            oWs.Cells[fila, 16].Value = oMecanicaDetalle.precio_percibido;
                            oWs.Cells[fila, 17].Value = oMecanicaDetalle.peso;
                            oWs.Cells[fila, 18].Value = oMecanicaDetalle.total_tg;
                            oWs.Cells[fila, 19].Value = oMecanicaDetalle.bonificacion;
                            fila++;
                        }
                        //Dando ajuste de columna
                        oWs.Column(2).AutoFit();
                        oWs.Column(4).AutoFit();
                        oWs.Column(6).AutoFit();
                        oWs.Column(12).AutoFit();
                    }
                    #endregion
                }
                oEx.Save();
            }
            #endregion

            return new ContentResult
            {
                Content = "{ \"archivo\": \"" + _fileServer + "\" }",
                ContentType = "application/json"
            };


        }

        [HttpGet]
        public ActionResult WorkFlowPlan()
        {
            ViewBag.id_concurso = WorkFlowPeriodoGetPeriodo();
            return View();
        }

        [HttpPost]
        public ActionResult WorkFlowPlanExportar(string __a , string __b)
        {
            int fila = 4;
            int _position = 0;
            int cont_cat = 0;
            string _fileServer = "";
            string _filePath = "";
            string _fileFormatServer = "";
            string _periodo = "";
            Color _colorCell = ColorTranslator.FromHtml("#000000");
            Color _colorFondo = ColorTranslator.FromHtml("#FF0000");
            Color _colorTexto = ColorTranslator.FromHtml("#ffffff");

            _fileServer = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _fileFormatServer = Path.Combine(Server.MapPath("/Format"), "Lucky_AlicorpCanjePlan.v1.0.xlsx");
            _filePath = Path.Combine(Server.MapPath("/Temp"), _fileServer);

            /*copia formato y lo ubica en carpeta temporal*/
            System.IO.File.Copy(_fileFormatServer, _filePath, true);

            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };
            List<AlicorpCanjeMecanicaFiltro> lMecanica = new AlicorpCanjeMecanicaFiltro().ListaDetallePlan(oParametro);
            
            #region Excel
            FileInfo archivo = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(archivo))
            {

                //Recorremos el excel para buscar la hoja de calculo
                foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                {

                    #region Alicorp Mecanica
                    if (oWs.Name == "Alicorp Canje Plan")
                    {
                        
                        foreach (AlicorpCanjeMecanicaFiltro oMecanica in lMecanica)//General
                        {
                            //oWs.Cells[fila, 2].Value = oMecanica.;

                            foreach (AlicorpCanjeCanal oCanal in oMecanica.lCanal)//Canal
                            {
                                oWs.Cells[fila, 2].Value = oCanal.descripcion_canal;
                                foreach (AlicorpCanjeCategoria oCategoria in oCanal.lCategoria)//Categoria
                                    {
                                        oWs.Cells[fila, 3].Value = oCategoria.descripcion_categoria;
                                        foreach (AlicorpCanjePlanDetalle oDetalle in oCategoria.lDetallePlan)//Detalle
                                        {
                                            oWs.Cells[fila, 4].Value = oDetalle.total_cliente;
                                            oWs.Cells[fila, 5].Value = oDetalle.plan_soles;
                                            oWs.Cells[fila, 6].Value = oDetalle.real_soles;
                                            oWs.Cells[fila, 7].Value = (oDetalle.variedad_soles/100);
                                            oWs.Cells[fila, 8].Value = oDetalle.plan_tonelada;
                                            oWs.Cells[fila, 9].Value = oDetalle.real_tonelada;
                                            oWs.Cells[fila, 10].Value = (oDetalle.variedad_tonelada/100);
                                        }
                                        fila++;
                                    }
                                fila++;
                                
                            }

                        }

                        oWs.Column(5).AutoFit();
                        oWs.Column(6).AutoFit();

                    }
                    #endregion
                } 
                oEx.Save();
            }
            #endregion
            return new ContentResult
            {
                Content = "{ \"archivo\": \"" + _fileServer + "\" }",
                ContentType = "application/json"
            };
        }

        [HttpPost]
        public ActionResult WorkFlowPlanDetallePlan(string __a, string __b)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpCanjeMecanicaFiltro().ListaDetallePlan(oParametro)),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult WorkFlowPlanObtenerPeriodo(string __a, string __b)
        {

            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpCanjeMecanicaDetalleEscala().ListaPeriodoFiltro(oParametro)),
                ContentType = "application/json"
            }.Content);

            //ViewBag.v_periodo_seleccionado = p_seleccionado;
            //return ViewBag.v_periodo_seleccionado;

        }

        [HttpPost]
        public int WorkFlowPlanCargaPlan(string _periodo, HttpPostedFileBase FilesInput)
        {

            // ==============================================
            // Instancia objetos

            // ==============================================
            // Declaracion de variables
            int _Fila = 0;
            string _sCelda = "";
            string _sFileCliente = "";
            string _sFileServidor = "";
            string _sPath = "";
            bool _Estado = false;
            int cant_pre = 0;
            _periodo = WorkFlowPeriodoGetPeriodo();
            if (_sFileServidor != null)
            {
                // ==========================================
                // ==========================================
                // Asignacion de variables
                _sFileCliente = Path.GetFileName(FilesInput.FileName);
                _sFileServidor = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);
                // ==========================================
                // asigna la ruta de ubicacion en el servidor.
                _sPath = Path.Combine(Server.MapPath("/Temp"), _sFileServidor + ".xlsx");
                // ==========================================
                // guarda el archivo en la ruta indicada del servidor.
                FilesInput.SaveAs(_sPath);
                // ==============================================
                // instancia archivo subido
                FileInfo oFile = new FileInfo(_sPath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(oFile))
                {
                    foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                    {
                        #region Carga_Plan

                        if (oWs.Name == "F_PLAN")
                        {
                            _Fila = 2;
                            _Estado = true;
                            do
                            {
                                _sCelda = oWs.Cells[_Fila, 1].Text;

                                if (_sCelda.Length != 0)
                                {
                                    string cadena = "";
                                    cadena = cadena + _periodo + ",";
                                    cadena = cadena + oWs.Cells[_Fila, 1].Text + ",";//Canal
                                    cadena = cadena + oWs.Cells[_Fila, 3].Text + ",";//Region
                                    cadena = cadena + oWs.Cells[_Fila, 5].Text + ",";//Oficina
                                    cadena = cadena + oWs.Cells[_Fila, 9].Text + ",";//Categoria
                                    cadena = cadena + oWs.Cells[_Fila, 7].Text + ",";//Cliente
                                    cadena = cadena + "0" /*oWs.Cells[_Fila, 8].Text */+ ",";//familia
                                    cadena = cadena + oWs.Cells[_Fila, 13].Text + ",";//Plan_Soles
                                    cadena = cadena + oWs.Cells[_Fila, 14].Text + ",";//Real_Soles
                                    cadena = cadena + "0" /*oWs.Cells[_Fila, 17].Text */+ ",";//Variedad_Soles
                                    cadena = cadena + oWs.Cells[_Fila, 15].Text + ",";//Plan_Toneladas
                                    cadena = cadena + oWs.Cells[_Fila, 16].Text + ",";//Real_Toneladas
                                    cadena = cadena + "0" /*oWs.Cells[_Fila, 17].Text */+ ",";//Variedad_Toneladas
                                    cadena = cadena + "1" /*oWs.Cells[_Fila, 17].Text */+ ",";//Presencia

                                    //cadena = cadena + ((Persona)Session["Session_Login"]).name_user + ",";
                                    //cadena = cadena + DateTime.Today.ToShortDateString();

                                    ActionResult vista = WorkFlowPlanDetallePlan("1", cadena);
                                    vista = null;

                                    _Fila++;
                                    cant_pre++;

                                }
                                else
                                {
                                    _Estado = false;
                                }
                            } while (_Estado == true);
                        }
                        #endregion
                    }
                }
            }
            //return View(/*_Informe*/);
            return cant_pre;
        }

        [HttpGet]
        public ActionResult WorkFlowPanel()
        {
            ViewBag.name_user = ((Persona)Session["Session_Login"]).name_user;
            ViewBag.id_concurso = WorkFlowPeriodoGetPeriodo();
            return View();
        }

        [HttpPost]
        public ActionResult WorkFlowPanelListaCliente(string __a, string __b)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpCanjePanelCliente().ListaCliente(oParametro)),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult WorkFlowPanelListaDetalleCliente(string __a, string __b)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpCanjePanelListaCliente().ListaPanelCliente(oParametro)),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult WorkFlowPanelExportar(string __a, string __b, int __c)
        {
            int fila = 4;
            int v_col = 7;
            int v_col_det = 7;
            int cont_cat = 0;
            string v_periodo = "";
            string _fileServer = "";
            string _filePath = "";
            string _fileFormatServer = "";
            Color _colorCell = ColorTranslator.FromHtml("#000000");
            Color _colorFondo = ColorTranslator.FromHtml("#FF0000");
            Color _colorTexto = ColorTranslator.FromHtml("#ffffff");

            _fileServer = String.Format("{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _fileFormatServer = Path.Combine(Server.MapPath("/Format"), "Lucky_AlicorpCanjePanel.v1.0.xlsx");
            _filePath = Path.Combine(Server.MapPath("/Temp"), _fileServer);

            /*copia formato y lo ubica en carpeta temporal*/
            System.IO.File.Copy(_fileFormatServer, _filePath, true);

            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(__a), parametro = __b };
            List<AlicorpCanjePanelListaCliente> lCliente = new AlicorpCanjePanelListaCliente().ListaPanelCliente(oParametro);

            #region Excel
            FileInfo archivo = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(archivo))
            {

                //Recorremos el excel para buscar la hoja de calculo
                foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                {
                    int valor = 0;
                    #region Alicorp Mecanica
                    if (oWs.Name == "Alicorp Canje Panel")
                    {

                        foreach (AlicorpCanjePanelListaCliente oCliente in lCliente)//General
                        {
                            oWs.Cells[fila, 2].Value = oCliente.segmentacion;
                            oWs.Cells[fila, 3].Value = oCliente.descripcion_region;
                            oWs.Cells[fila, 4].Value = oCliente.descripcion_oficina;
                            oWs.Cells[fila, 5].Value = oCliente.cod_principal;
                            oWs.Cells[fila, 6].Value = oCliente.nombre;

                            if (valor == 0)
                            {
                                foreach (AlicorpCanjePanelDetalleCliente oDetalle in oCliente.lDetalleCliente)
                                {

                                    oWs.Cells[3, v_col].Value = oDetalle.rpl_id;
                                    v_periodo = oDetalle.rpl_id;
                                    v_col = v_col + 2;
                                }
                                valor++;
                            }

                            foreach (AlicorpCanjePanelDetalleCliente oDetalleCliente in oCliente.lDetalleCliente)
                            {

                                /*if (v_periodo == oDetalleCliente.rpl_id)
                                {*/
                                    if (__c == 1)
                                    {
                                        Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                                        oWs.Cells[fila, v_col_det, fila, (v_col_det + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                        oWs.Cells[fila, v_col_det, fila, (v_col_det + 1)].Style.Fill.BackgroundColor.SetColor(colFromHex1);

                                        Color colFromHex3 = oDetalleCliente.presencia == 1 ? System.Drawing.ColorTranslator.FromHtml("#00C209") : System.Drawing.ColorTranslator.FromHtml("#FC0101");
                                        oWs.Cells[fila, v_col_det, fila, (v_col_det + 1)].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                                        oWs.Cells[fila, v_col_det, fila, (v_col_det + 1)].Style.Font.Color.SetColor(colFromHex3);
                                            
                                        oWs.Cells[fila,v_col_det, fila, (v_col_det+1)].Merge = true;
                                        oWs.Cells[fila, v_col_det].Value = oDetalleCliente.presencia == 1 ? "✔" : "X";
                                        v_col_det = v_col_det + 2;
                                    }
                                    else if (__c == 2)
                                    {
                                        oWs.Cells[fila, v_col_det].Value = oDetalleCliente.plan_soles;
                                        oWs.Cells[fila, v_col_det].Style.Numberformat.Format = "\"S/.\"#,##0.00;\"S/.\"-#,##0.00";

                                        v_col_det++;

                                        oWs.Cells[fila, v_col_det].Value = oDetalleCliente.real_soles;
                                        oWs.Cells[fila, v_col_det].Style.Numberformat.Format = "\"S/.\"#,##0.00;\"S/.\"-#,##0.00";
                                        v_col_det++;
                                    }
                                    else if (__c == 3)
                                    {
                                        oWs.Cells[fila, v_col_det].Value = oDetalleCliente.plan_tonelada;
                                        v_col_det++;
                                        oWs.Cells[fila, v_col_det].Value = oDetalleCliente.real_tonelada;
                                        v_col_det++;
                                    }
                                /*}*/
                            }
                            v_col_det = 7;
                            fila++;
                        }
                        oWs.Column(6).AutoFit();

                    }
                    #endregion
                }
                oEx.Save();
            }
            #endregion
            return new ContentResult
            {
                Content = "{ \"archivo\": \"" + _fileServer + "\" }",
                ContentType = "application/json"
            };
        }


        public int WorkFlowPanelCargaCliente(string _periodo, HttpPostedFileBase FilesInput)
        {

            // ==============================================
            // Instancia objetos

            // ==============================================
            // Declaracion de variables
            int _Fila = 0;
            string _sCelda = "";
            string _sFileCliente = "";
            string _sFileServidor = "";
            string _sPath = "";
            bool _Estado = false;
            int cant_pre = 0;
            _periodo = "C001-17";
            if (_sFileServidor != null)
            {
                // ==========================================
                // ==========================================
                // Asignacion de variables
                _sFileCliente = Path.GetFileName(FilesInput.FileName);
                _sFileServidor = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);
                // ==========================================
                // asigna la ruta de ubicacion en el servidor.
                _sPath = Path.Combine(Server.MapPath("/Temp"), _sFileServidor + ".xlsx");
                // ==========================================
                // guarda el archivo en la ruta indicada del servidor.
                FilesInput.SaveAs(_sPath);
                // ==============================================
                // instancia archivo subido
                FileInfo oFile = new FileInfo(_sPath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(oFile))
                {
                    foreach (Excel.ExcelWorksheet oWs in oEx.Workbook.Worksheets)
                    {
                        #region Carga_Plan

                        if (oWs.Name == "F_PANEL")
                        {
                            _Fila = 2;
                            _Estado = true;
                            do
                            {
                                _sCelda = oWs.Cells[_Fila, 1].Text;

                                if (_sCelda.Length != 0)
                                {
                                    string cadena = "";
                                    /*cadena = cadena + _periodo + ",";*/
                                    cadena = cadena + oWs.Cells[_Fila, 1].Text + ",";//Canal
                                    cadena = cadena + oWs.Cells[_Fila, 3].Text + ",";//Region
                                    cadena = cadena + oWs.Cells[_Fila, 5].Text + ",";//Oficina
                                    cadena = cadena + oWs.Cells[_Fila, 10].Text + ",";//Categoria
                                    cadena = cadena + oWs.Cells[_Fila, 8].Text + ",";//Cliente
                                    cadena = cadena + oWs.Cells[_Fila, 9].Text + ",";//Cliente
                                    cadena = cadena + oWs.Cells[_Fila, 7].Text + ",";//Segmento
                                    cadena = cadena + ((Persona)Session["Session_Login"]).name_user + ",";
                                    cadena = cadena + DateTime.Today.ToShortDateString();

                                    ActionResult vista = WorkFlowPanelListaCliente("3", cadena);
                                    vista = null;

                                    _Fila++;
                                    cant_pre++;

                                }
                                else
                                {
                                    _Estado = false;
                                }
                            } while (_Estado == true);
                        }
                        #endregion
                    }
                }
            }
            //return View(/*_Informe*/);
            return cant_pre;
        }


        [HttpGet]
        public ActionResult WorkFlowResultado()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.ind_vista = Request["_c"] == null ? "0" : Request["_c"];
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GetResultadoSeleccionado(string opcion, string parametro)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(opcion), parametro = parametro };

            if (Convert.ToInt32(opcion) == 1)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new AlicorpCanjeDistribucion().Distribucion(oParametro)),
                    ContentType = "application/json"
                }.Content);
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new AlicorpCanjeDistribucion().Selecciona(oParametro)),
                    ContentType = "application/json"
                }.Content);
            }

            //return View();
        }

        [HttpPost, ValidateInput(false)]
        public int GetResultadoSeleccionadoModal(string opcion, string parametro)
        {
            AlicorpCanjeParametro oParametro = new AlicorpCanjeParametro() { opcion = Convert.ToInt32(opcion), parametro = parametro };
            int rs = (new AlicorpCanjeDistribucion().InsertaEnvio(oParametro));
            return rs;
        }


        [HttpPost]
        public ActionResult GetResultadoG1(int __a,string __b) {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            return View(new E_Resultados().GetGrilla1(new Request_AlicorpCanjeResultadosObj() { Anio = __a, Periodo = __b }));
        }

        [HttpPost]
        public ActionResult GetResultadoG2(int __a,string __b)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            return View(new E_Resultados().GetGrilla2(new Request_AlicorpCanjeResultadosObj() { Codigo = __a, Periodo = __b }));
        }

        [HttpPost]
        public int UpdG2(int __a, string __b, int __c, string __d)
        {
            //if (Session["Session_Login"] == null)
            //{
            //    return RedirectToAction("login", "LogOn");
            //}
            //ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            return (new E_Resultados().Update_Grilla2(new Request_AlicorpCanjeResultadosObj() { Op = __a, valores = __b , Codigo = __c,Observacion = __d}));
        }

        [HttpGet]
        public ActionResult WorkFlowValidacion()
        {
            return View();
        }

        #endregion
    }
}