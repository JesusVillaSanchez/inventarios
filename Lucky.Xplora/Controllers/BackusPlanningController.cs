﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Administrador;
using Lucky.Xplora.Models.BackusPlanning;
using Lucky.Xplora.Security;

namespace Lucky.Xplora.Controllers
{
    public class BackusPlanningController : Controller
    {
        //
        // GET: /BackusPlanning/

        [CustomAuthorize]
        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.Tipo_Perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
                                                                                
            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }

        [HttpPost]
        public ActionResult GetSupervisor(string __a)
        {
            return PartialView(
            "../Alicorp/GetSupervisor",
            new Models.Persona().ListarSupervisores(new Request_GetSupervisor_Campania()
            {
                campania = __a
            }));
        }

        [HttpPost]
        public ActionResult _GetClasificacionBackus()
        {
            return View(new Clasificacion().Lista());
        }

        ///// <summary>
        ///// Autor: rcontreras
        ///// Fecha: 25-06-2015
        ///// </summary>
        ///// <param name="__a"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public ActionResult GetCadena(string __a, int __b, int __c)
        //{
        //    return View(
        //        new TipoCanalPlanning().ListaTipoCanal(new Request_GetTipoCanalBackusPlanning_Campania_Anio_Mes()
        //        {
        //            campania = __a,
        //            anio = __b,
        //            mes = __c
        //        }));
        //}

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 22-06-2015
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetDepartamento(string __a)
        {
            return PartialView(
                "../Alicorp/GetDepartamento",
                new Departamento().ListaBackus(new Request_GetDepartamento()
                {
                    Cod_Compania = __a,
                }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 22-06-2015
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCiudad(string __a, string __b)
        {
            return View(
                new Lucky.Xplora.Models.BackusPlanning.Ciudad().ListaCiudades(new Request_GetCiudad()
                {
                    Cod_Campania = __a,
                    Cod_Departamento = __b
                }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 22-06-2015
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetDistrito(string __a, string __b, string __c)
        {
            return View(
                new Distrito().ListaDistritos(new Request_GetDistrito()
                {
                    pais = __a,
                    departamento = __b,
                    provincia = __c
                }));
        }

        [HttpPost]
        public ActionResult GetReportPlanning(int periodo, int canal, int cadena, string departamento, string ciudad, string distrito, int supervisor, int clasiBackus)
        {
            return View(
                new PlanningReport().ListaPlanning(new Request_PlanningReport()
                {
                    periodo = periodo,
                    canal = canal,
                    cadena = cadena,
                    departamento = departamento,
                    ciudad = ciudad,
                    distrito = distrito,
                    supervisor = supervisor,
                    clasiBackus = clasiBackus
                }));
        }



    }
}
