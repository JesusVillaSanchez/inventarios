﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Web.Script.Serialization;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.BackusDataMercaderista;
using Lucky.Xplora.Models.Administrador;
using System.IO;
using System.Net;
using Lucky.Xplora.Security;

namespace Lucky.Xplora.Controllers
{
    public class BackusDataMercaderistaController : Controller
    {
        //
        // GET: /BackusDataMercaderista/
        [CustomAuthorize]
        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.Tipo_Perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;
            //ViewBag.compania = "1637";
            //ViewBag.campania = "9020110152013";


            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }


            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            

            return View();
        }


        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 12-05-2015
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetSector(string __a)
        {
            return View(new Sector().Lista(new Request_Listar_Sector_OficinaEQ_Reports()
            {
                compania = 1637,
                oficina = Convert.ToInt32(__a),
            }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 02-06-2015
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetEmpresa(int __a)
        {
            return View(new Empresa().Lista(new Request_GetEmpresa()
            {
                company = __a,
            }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 01-06-2015
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetDepartamento(string __a)
        {
            return PartialView(
                "../Alicorp/GetDepartamento",
                new Departamento().ListaBackus(new Request_GetDepartamento()
            {
                Cod_Compania = __a,
            }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 12-05-2015
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetProducto(string __a, string __b, string __c, string __d)
        {
            return View(
                new Lucky.Xplora.Models.BackusDataMercaderista.Producto().Lista(new Request_GetProducto()
                {
                    cliente = __a,
                    categoria = __b,
                    subCategoria = __c,
                    marca = __d
                })
            );
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-06-01
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetMarca(string __a, string __b, string __c, string __d)
        {
            return PartialView(
                "../Alicorp/GetMarca",
                    new Marca().ListaBackus(new Request_GetMarca_Campania_Reporte_Categoria_ProductoFamiliaBackus() { campania = __a, reporte = __d, categoria = Convert.ToInt32(__b), producto_familia = __c })
                );
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 08-06-2015
        /// </summary>
        /// <param name="__a">campania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCategoria(string __a, int __b)
        {
            if (__b != 23) {
                __b = 163;
            }
            return PartialView(
                "../Alicorp/GetCategoria",
                new Categoria().Lista(new Listar_Categoria_Por_CodCampania_y_CodReporte_Request()
                {
                    campania = __a,
                    reporte = __b
                }));
        }


        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 08-06-2015
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetProductoFamilia(string __a, string __b)
        {
            return View(
                new Producto_Familia().Lista(new Request_GetProductoFamilia_Campania_Reporte_Categoria()
                {
                    campania = __a,
                    reporte = 163,
                    categoria = Convert.ToInt32(__b)
                }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 06-06-2015
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GirarFoto(string __a, string __b)
        {
            /* Opciones
             * 1 - Izquierda
             * 2 - Derecha
             */
            bool estado = false;
            Int32 opcion = Convert.ToInt32(__a);
            if (__b != "")
            {
                //__b = __b.Substring(69, 17);
                __b = __b.Substring(73, 17);
                try
                {
                    using (Image oImg = Image.FromFile(ConfigurationManager.AppSettings["Rutafoto"] + @"" + __b + ".jpg"))
                    {
                        if (opcion == 1)
                        {
                            oImg.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        }
                        else if (opcion == 2)
                        {
                            oImg.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        }

                        oImg.Save(ConfigurationManager.AppSettings["Rutafoto"] + @"" + __b + ".jpg", ImageFormat.Jpeg);
                    }

                    estado = true;
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    estado = false;
                }
            }

            return new ContentResult
            {
                Content = MvcApplication._Serialize(new { estado = estado }),
                ContentType = "application/json"
            };
        }


        #region PRECIO
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 22-05-2015
        /// </summary> Reporte Precio Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReportePrecio(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.producto = producto;
            ViewBag.empresa = empresa;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 22-05-2015
        /// </summary> Reporte PRECIO
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReportePrecioJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = producto;
            oParametros.empresa = empresa;

            List<E_Bks_Precio> oLs = new E_Bks_Precio().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-05-2015
        /// </summary> Reporte Precio Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelPrecio(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Precio >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = producto;
                oParametros.empresa = empresa;

                List<E_Bks_Precio> oPrecio = new E_Bks_Precio().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Precios >>>
                if ((oPrecio != null) || (oPrecio.Count > 0))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Precio");
                    oWsPrecio.Cells[1, 1].Value = "CANAL";
                    oWsPrecio.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsPrecio.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsPrecio.Cells[1, 4].Value = "CIUDAD";
                    oWsPrecio.Cells[1, 5].Value = "DISTRITO";
                    oWsPrecio.Cells[1, 6].Value = "CONO";
                    oWsPrecio.Cells[1, 7].Value = "PERFIL";
                    oWsPrecio.Cells[1, 8].Value = "GIE NOM";
                    oWsPrecio.Cells[1, 9].Value = "GIE USER";
                    oWsPrecio.Cells[1, 10].Value = "SUPERVISOR";
                    oWsPrecio.Cells[1, 11].Value = "CADENA";
                    oWsPrecio.Cells[1, 12].Value = "PDV BACKUS";
                    oWsPrecio.Cells[1, 13].Value = "PDV";
                    oWsPrecio.Cells[1, 14].Value = "CATEGORIA";
                    oWsPrecio.Cells[1, 15].Value = "NIVEL";
                    oWsPrecio.Cells[1, 16].Value = "MARCA";
                    oWsPrecio.Cells[1, 17].Value = "PRODUCTO";
                    oWsPrecio.Cells[1, 18].Value = "PRECIO OFERTA";
                    oWsPrecio.Cells[1, 19].Value = "FEC. INICIO";
                    oWsPrecio.Cells[1, 20].Value = "FEC. FIN";
                    oWsPrecio.Cells[1, 21].Value = "OBSERVACION";
                    oWsPrecio.Cells[1, 22].Value = "FOTO";
                    oWsPrecio.Cells[1, 23].Value = "COD. MECANICA";
                    oWsPrecio.Cells[1, 24].Value = "DESC. MECANICA";
                    oWsPrecio.Cells[1, 25].Value = "TIPO DE OFERTA";
                    oWsPrecio.Cells[1, 26].Value = "DESC. DE OFERTA";
                    oWsPrecio.Cells[1, 27].Value = "PRECIO PUBLICO";
                    oWsPrecio.Cells[1, 28].Value = "MODIFICADO POR";
                    oWsPrecio.Cells[1, 29].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Precio oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsPrecio.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsPrecio.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsPrecio.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsPrecio.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsPrecio.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsPrecio.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsPrecio.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsPrecio.Cells[_fila, 17].Value = oBj.des_producto;
                        oWsPrecio.Cells[_fila, 18].Value = oBj.precio_oferta;
                        oWsPrecio.Cells[_fila, 19].Value = oBj.fec_inicio;
                        oWsPrecio.Cells[_fila, 20].Value = oBj.fec_fin;
                        oWsPrecio.Cells[_fila, 21].Value = oBj.des_observacion;
                        oWsPrecio.Cells[_fila, 22].Value = oBj.des_foto;
                        oWsPrecio.Cells[_fila, 23].Value = oBj.cod_mecanica;
                        oWsPrecio.Cells[_fila, 24].Value = oBj.des_mecanica;
                        oWsPrecio.Cells[_fila, 25].Value = oBj.cod_tipo_oferta;
                        oWsPrecio.Cells[_fila, 26].Value = oBj.des_tipo_oferta;
                        oWsPrecio.Cells[_fila, 27].Value = oBj.precio_publico;
                        oWsPrecio.Cells[_fila, 28].Value = oBj.modificado_por;
                        oWsPrecio.Cells[_fila, 29].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 29].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).Width = 15;
                    oWsPrecio.Column(2).Width = 25;
                    oWsPrecio.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(5).Width = 30;
                    oWsPrecio.Column(7).Width = 20;
                    oWsPrecio.Column(8).Width = 10;
                    oWsPrecio.Column(9).Width = 20;
                    oWsPrecio.Column(10).Width = 15;
                    oWsPrecio.Column(10).Width = 40;
                    oWsPrecio.Column(11).Width = 20;
                    oWsPrecio.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }
       
        [HttpPost]
        public ActionResult validacionPrecioBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().UpdateValPrecio(
                    new Request_ValidacionPrecioBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionPrecioBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaPrecio(
                    new Request_InValidacionPrecioBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updatePrecioBackus(string Datos)
        {
            List<E_Bks_Precio> oLs = MvcApplication._Deserialize<List<E_Bks_Precio>>(Datos);

            foreach (E_Bks_Precio oOb in oLs)
            {
                Request_UpdatePrecioBackus oRq = new Request_UpdatePrecioBackus();
                Response_UpdatePrecioBackus oRp;

                oRq.codigoDet = oOb.cod_det;
                oRq.precio = Convert.ToString(oOb.precio_oferta);
                oRq.precioPublico = Convert.ToString(oOb.precio_publico);
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                if (oOb.cod_tipo_oferta == "") { oRq.tipoOferta = 0; } else { oRq.tipoOferta = Convert.ToInt32(oOb.cod_tipo_oferta); }
                //oRq.tipoOferta = Convert.ToInt32(oOb.cod_tipo_oferta);
                oRq.validado = oOb.validado;
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdatePrecioBackus>(MvcApplication._Servicio_Operativa.Update_precio_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 03-07-2015
        /// </summary> 
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Listar_Combo_Precio_BackusDM(int reporte)
        {
            Models.BackusDataMercaderista.Combo_Precio_Bks_Request oRq = new Models.BackusDataMercaderista.Combo_Precio_Bks_Request();
            oRq.reporte = reporte;
            var oModelo = MvcApplication._Deserialize<Models.BackusDataMercaderista.Combo_Precio_Bks_Response>(MvcApplication._Servicio_Operativa.Listar_Combo_Quiebre_BackusDM(MvcApplication._Serialize(oRq)));

            if (oModelo == null || oModelo.Lista == null || oModelo.Lista.Count == 0)
            {
                List<Models.BackusDataMercaderista.Combo_Precio_Bks> oLs = new List<Models.BackusDataMercaderista.Combo_Precio_Bks>();
                Models.BackusDataMercaderista.Combo_Precio_Bks oOb = new Models.BackusDataMercaderista.Combo_Precio_Bks();
                oOb.cod_obs = "0";
                oOb.des_obs = "Sin datos disponibles.";
                oLs.Add(oOb);
                oModelo.Lista = oLs;
            }

            var modelData = oModelo.Lista.Select(u => new SelectListItem()
            {
                Text = u.des_obs,
                Value = u.cod_obs
            });
            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updatePrecioFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo  = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        #endregion

        #region QUIEBRE

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-05-2015
        /// </summary> Reporte Quiebre Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteQuiebre(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.producto = producto;
            ViewBag.empresa = empresa;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-05-2015
        /// </summary> Reporte Quiebre
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteQuiebreJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = producto;
            oParametros.empresa = empresa;

            List<E_Bks_Quiebre> oLs = new E_Bks_Quiebre().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-05-2015
        /// </summary> Reporte Quiebre Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        
        [HttpPost]
        public JsonResult descargarExcelQuiebre(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_quiebre_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Quiebre >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = producto;
                oParametros.empresa = empresa;

                List<E_Bks_Quiebre> oQuiebre = new E_Bks_Quiebre().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Quiebre >>>
                if ((oQuiebre != null) || (oQuiebre.Count > 0))
                {
                    Excel.ExcelWorksheet oWsQuiebre = oEx.Workbook.Worksheets.Add("Quiebre");
                    oWsQuiebre.Cells[1, 1].Value = "CANAL";
                    oWsQuiebre.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsQuiebre.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsQuiebre.Cells[1, 4].Value = "CIUDAD";
                    oWsQuiebre.Cells[1, 5].Value = "DISTRITO";
                    oWsQuiebre.Cells[1, 6].Value = "CONO";
                    oWsQuiebre.Cells[1, 7].Value = "PERFIL";
                    oWsQuiebre.Cells[1, 8].Value = "GIE NOM";
                    oWsQuiebre.Cells[1, 9].Value = "GIE USER";
                    oWsQuiebre.Cells[1, 10].Value = "SUPERVISOR";
                    oWsQuiebre.Cells[1, 11].Value = "CADENA";
                    oWsQuiebre.Cells[1, 12].Value = "PDV BACKUS";
                    oWsQuiebre.Cells[1, 13].Value = "PDV";
                    oWsQuiebre.Cells[1, 14].Value = "CATEGORIA";
                    oWsQuiebre.Cells[1, 15].Value = "NIVEL";
                    oWsQuiebre.Cells[1, 16].Value = "MARCA";
                    oWsQuiebre.Cells[1, 17].Value = "PRODUCTO";
                    oWsQuiebre.Cells[1, 18].Value = "QUIEBRE 1";
                    oWsQuiebre.Cells[1, 19].Value = "COMENTARIO FOTO";
                    oWsQuiebre.Cells[1, 20].Value = "FOTO";
                    oWsQuiebre.Cells[1, 21].Value = "MODIFICADO POR";
                    oWsQuiebre.Cells[1, 22].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Quiebre oBj in oQuiebre)
                    {
                        oWsQuiebre.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsQuiebre.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsQuiebre.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsQuiebre.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsQuiebre.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsQuiebre.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsQuiebre.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsQuiebre.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsQuiebre.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsQuiebre.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsQuiebre.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsQuiebre.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsQuiebre.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsQuiebre.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsQuiebre.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsQuiebre.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsQuiebre.Cells[_fila, 17].Value = oBj.des_producto;
                        oWsQuiebre.Cells[_fila, 18].Value = oBj.quiebre1_desc;
                        oWsQuiebre.Cells[_fila, 19].Value = oBj.comentario;
                        oWsQuiebre.Cells[_fila, 20].Value = oBj.des_foto;
                        oWsQuiebre.Cells[_fila, 21].Value = oBj.modificado_por;
                        oWsQuiebre.Cells[_fila, 22].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsQuiebre.SelectedRange[1, 1, 1, 22].AutoFilter = true;
                    oWsQuiebre.Row(1).Height = 25;
                    oWsQuiebre.Row(1).Style.Font.Bold = true;
                    oWsQuiebre.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsQuiebre.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsQuiebre.Column(1).Width = 15;
                    oWsQuiebre.Column(2).Width = 25;
                    oWsQuiebre.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsQuiebre.Column(5).Width = 30;
                    oWsQuiebre.Column(7).Width = 20;
                    oWsQuiebre.Column(8).Width = 10;
                    oWsQuiebre.Column(9).Width = 20;
                    oWsQuiebre.Column(10).Width = 15;
                    oWsQuiebre.Column(10).Width = 40;
                    oWsQuiebre.Column(11).Width = 20;
                    oWsQuiebre.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionQuiebreBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaQuiebre(
                    new Request_ValidacionQuiebreBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionQuiebreBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaQuiebre(
                    new Request_InValidacionQuiebreBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateQuiebreBackus(string Datos)
        {
            List<E_Bks_Quiebre> oLs = MvcApplication._Deserialize<List<E_Bks_Quiebre>>(Datos);

            foreach (E_Bks_Quiebre oOb in oLs)
            {
                Request_UpdateQuiebreBackus oRq = new Request_UpdateQuiebreBackus();
                Response_UpdateQuiebreBackus oRp;

                oRq.codigoDet = oOb.cod_det;
                oRq.quiebre = oOb.quiebre1;
                oRq.validado = oOb.validado;
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateQuiebreBackus>(MvcApplication._Servicio_Operativa.Update_quiebre_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 28-05-2015
        /// </summary> 
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Listar_Combo_Quiebre_BackusDM(int reporte)
        {
            Models.BackusDataMercaderista.Combo_Quiebre_Bks_Request oRq = new Models.BackusDataMercaderista.Combo_Quiebre_Bks_Request();
            oRq.reporte = reporte;
            var oModelo = MvcApplication._Deserialize<Models.BackusDataMercaderista.Combo_Quiebre_Bks_Response>(MvcApplication._Servicio_Operativa.Listar_Combo_Quiebre_BackusDM(MvcApplication._Serialize(oRq)));

            if (oModelo == null || oModelo.Lista == null || oModelo.Lista.Count == 0)
            {
                List<Models.BackusDataMercaderista.Combo_Quiebre_Bks> oLs = new List<Models.BackusDataMercaderista.Combo_Quiebre_Bks>();
                Models.BackusDataMercaderista.Combo_Quiebre_Bks oOb = new Models.BackusDataMercaderista.Combo_Quiebre_Bks();
                oOb.cod_obs = "0";
                oOb.des_obs = "Sin datos disponibles.";
                oLs.Add(oOb);
                oModelo.Lista = oLs;
            }

            var modelData = oModelo.Lista.Select(u => new SelectListItem()
            {
                Text = u.des_obs,
                Value = u.cod_obs
            });
            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateQuiebreFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        #endregion

        #region STOCK
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-05-2015
        /// </summary> Reporte Stock Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteStock(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.producto = producto;
            ViewBag.empresa = empresa;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-05-2015
        /// </summary> Reporte STOCK
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteStockJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = producto;
            oParametros.empresa = empresa;

            List<E_Bks_Stock> oLs = new E_Bks_Stock().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-05-2015
        /// </summary> Reporte stock Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelStock(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_stock_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Stock >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = producto;
                oParametros.empresa = empresa;

                List<E_Bks_Stock> oStock = new E_Bks_Stock().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Stock >>>
                if ((oStock != null) || (oStock.Count > 0))
                {
                    Excel.ExcelWorksheet oWsStock = oEx.Workbook.Worksheets.Add("Stock");
                    oWsStock.Cells[1, 1].Value = "CANAL";
                    oWsStock.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsStock.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsStock.Cells[1, 4].Value = "CIUDAD";
                    oWsStock.Cells[1, 5].Value = "DISTRITO";
                    oWsStock.Cells[1, 6].Value = "CONO";
                    oWsStock.Cells[1, 7].Value = "PERFIL";
                    oWsStock.Cells[1, 8].Value = "GIE NOM";
                    oWsStock.Cells[1, 9].Value = "GIE USER";
                    oWsStock.Cells[1, 10].Value = "SUPERVISOR";
                    oWsStock.Cells[1, 11].Value = "CADENA";
                    oWsStock.Cells[1, 12].Value = "PDV BACKUS";
                    oWsStock.Cells[1, 13].Value = "PDV";
                    oWsStock.Cells[1, 14].Value = "CATEGORIA";
                    oWsStock.Cells[1, 15].Value = "NIVEL";
                    oWsStock.Cells[1, 16].Value = "MARCA";
                    oWsStock.Cells[1, 17].Value = "PRODUCTO";
                    oWsStock.Cells[1, 18].Value = "CANTIDAD";
                    oWsStock.Cells[1, 19].Value = "TIPO";
                    oWsStock.Cells[1, 20].Value = "FOTO";
                    oWsStock.Cells[1, 21].Value = "COMENTARIO";
                    oWsStock.Cells[1, 22].Value = "MODIFICADO POR";
                    oWsStock.Cells[1, 23].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Stock oBj in oStock)
                    {
                        oWsStock.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsStock.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsStock.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsStock.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsStock.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsStock.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsStock.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsStock.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsStock.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsStock.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsStock.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsStock.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsStock.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsStock.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsStock.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsStock.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsStock.Cells[_fila, 17].Value = oBj.des_producto;
                        oWsStock.Cells[_fila, 18].Value = oBj.cantidad;
                        oWsStock.Cells[_fila, 19].Value = oBj.tipo;
                        oWsStock.Cells[_fila, 20].Value = oBj.des_foto;
                        oWsStock.Cells[_fila, 21].Value = oBj.comentario;
                        oWsStock.Cells[_fila, 22].Value = oBj.modificado_por;
                        oWsStock.Cells[_fila, 23].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsStock.SelectedRange[1, 1, 1, 23].AutoFilter = true;
                    oWsStock.Row(1).Height = 25;
                    oWsStock.Row(1).Style.Font.Bold = true;
                    oWsStock.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsStock.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsStock.Column(1).Width = 15;
                    oWsStock.Column(2).Width = 25;
                    oWsStock.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsStock.Column(5).Width = 30;
                    oWsStock.Column(7).Width = 20;
                    oWsStock.Column(8).Width = 10;
                    oWsStock.Column(9).Width = 20;
                    oWsStock.Column(10).Width = 15;
                    oWsStock.Column(10).Width = 40;
                    oWsStock.Column(11).Width = 20;
                    oWsStock.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionStockBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaStock(
                    new Request_ValidacionStockBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionStockBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaStock(
                    new Request_InValidacionStockBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateStockBackus(string Datos)
        {
            List<E_Bks_Stock> oLs = MvcApplication._Deserialize<List<E_Bks_Stock>>(Datos);

            foreach (E_Bks_Stock oOb in oLs)
            {
                Request_UpdateStockBackus oRq = new Request_UpdateStockBackus();
                Response_UpdateStockBackus oRp;

                oRq.codigoDet = oOb.cod_det;
                oRq.cantidad = oOb.cantidad;
                oRq.validado = oOb.validado;
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateStockBackus>(MvcApplication._Servicio_Operativa.Update_stock_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateStockFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }
        #endregion


        #region VISIBILIDAD
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 31-05-2015
        /// </summary> Reporte Visibilidad Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVisibilidad(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.empresa = empresa;
            //ViewBag.producto = producto;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 31-05-2015
        /// </summary> Reporte visibilidad
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVisibilidadJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = "";
            oParametros.empresa = empresa;

            List<E_Bks_Visibilidad> oLs = new E_Bks_Visibilidad().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte participacion Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelVisibilidad(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_visibilidad_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Visibilidad >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = "";
                oParametros.empresa = empresa;

                List<E_Bks_Visibilidad> oVisibilidad = new E_Bks_Visibilidad().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Visibilidad >>>
                if ((oVisibilidad != null) || (oVisibilidad.Count > 0))
                {
                    Excel.ExcelWorksheet oWsVisibilidad = oEx.Workbook.Worksheets.Add("Visibilidad");
                    oWsVisibilidad.Cells[1, 1].Value = "CANAL";
                    oWsVisibilidad.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsVisibilidad.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsVisibilidad.Cells[1, 4].Value = "CIUDAD";
                    oWsVisibilidad.Cells[1, 5].Value = "DISTRITO";
                    oWsVisibilidad.Cells[1, 6].Value = "CONO";
                    oWsVisibilidad.Cells[1, 7].Value = "PERFIL";
                    oWsVisibilidad.Cells[1, 8].Value = "GIE NOM";
                    oWsVisibilidad.Cells[1, 9].Value = "GIE USER";
                    oWsVisibilidad.Cells[1, 10].Value = "SUPERVISOR";
                    oWsVisibilidad.Cells[1, 11].Value = "CADENA";
                    oWsVisibilidad.Cells[1, 12].Value = "PDV BACKUS";
                    oWsVisibilidad.Cells[1, 13].Value = "PDV";
                    oWsVisibilidad.Cells[1, 14].Value = "CATEGORIA";
                    oWsVisibilidad.Cells[1, 15].Value = "NIVEL";
                    oWsVisibilidad.Cells[1, 16].Value = "MARCA";
                    oWsVisibilidad.Cells[1, 17].Value = "COD. MATERIAL";
                    oWsVisibilidad.Cells[1, 18].Value = "MATERIAL";
                    oWsVisibilidad.Cells[1, 19].Value = "CANTIDAD";
                    oWsVisibilidad.Cells[1, 20].Value = "COD. TIPO";
                    oWsVisibilidad.Cells[1, 21].Value = "TIPO";
                    oWsVisibilidad.Cells[1, 22].Value = "COD. TIPO A";
                    oWsVisibilidad.Cells[1, 23].Value = "TIPO A";
                    oWsVisibilidad.Cells[1, 24].Value = "COMENTARIO";
                    oWsVisibilidad.Cells[1, 25].Value = "FOTO";
                    oWsVisibilidad.Cells[1, 26].Value = "MODIFICADO POR";
                    oWsVisibilidad.Cells[1, 27].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Visibilidad oBj in oVisibilidad)
                    {
                        oWsVisibilidad.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsVisibilidad.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsVisibilidad.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsVisibilidad.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsVisibilidad.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsVisibilidad.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsVisibilidad.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsVisibilidad.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsVisibilidad.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsVisibilidad.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsVisibilidad.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsVisibilidad.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsVisibilidad.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsVisibilidad.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsVisibilidad.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsVisibilidad.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsVisibilidad.Cells[_fila, 17].Value = oBj.cod_material;
                        oWsVisibilidad.Cells[_fila, 18].Value = oBj.des_material;
                        oWsVisibilidad.Cells[_fila, 19].Value = oBj.cantidad;
                        oWsVisibilidad.Cells[_fila, 20].Value = oBj.cod_tipo;
                        oWsVisibilidad.Cells[_fila, 21].Value = oBj.des_tipo;
                        oWsVisibilidad.Cells[_fila, 22].Value = oBj.cod_tipo_a;
                        oWsVisibilidad.Cells[_fila, 23].Value = oBj.des_tipo_a;
                        oWsVisibilidad.Cells[_fila, 24].Value = oBj.des_comentario;
                        oWsVisibilidad.Cells[_fila, 25].Value = oBj.des_foto;
                        oWsVisibilidad.Cells[_fila, 26].Value = oBj.modificado_por;
                        oWsVisibilidad.Cells[_fila, 27].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsVisibilidad.SelectedRange[1, 1, 1, 27].AutoFilter = true;
                    oWsVisibilidad.Row(1).Height = 25;
                    oWsVisibilidad.Row(1).Style.Font.Bold = true;
                    oWsVisibilidad.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsVisibilidad.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsVisibilidad.Column(1).Width = 15;
                    oWsVisibilidad.Column(2).Width = 25;
                    oWsVisibilidad.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsVisibilidad.Column(5).Width = 30;
                    oWsVisibilidad.Column(7).Width = 20;
                    oWsVisibilidad.Column(8).Width = 10;
                    oWsVisibilidad.Column(9).Width = 20;
                    oWsVisibilidad.Column(10).Width = 15;
                    oWsVisibilidad.Column(10).Width = 40;
                    oWsVisibilidad.Column(11).Width = 20;
                    oWsVisibilidad.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionVisibilidadBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaVisibilidad(
                    new Request_ValidacionVisibilidadBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionVisibilidadBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaVisibilidad(
                    new Request_InValidacionVisibilidadBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 06-07-2015
        /// </summary> 
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Listar_Combo_Visibilidad_BackusDM_Tipo(int reporte)
        {
            Models.BackusDataMercaderista.Combo_Visibilidad_Bks_Request oRq = new Models.BackusDataMercaderista.Combo_Visibilidad_Bks_Request();
            oRq.reporte = reporte;
            var oModelo = MvcApplication._Deserialize<Models.BackusDataMercaderista.Combo_Visibilidad_Bks_Response>(MvcApplication._Servicio_Operativa.Listar_Combo_Quiebre_BackusDM(MvcApplication._Serialize(oRq)));

            Models.BackusDataMercaderista.Combo_Visibilidad_Bks oValida = new Models.BackusDataMercaderista.Combo_Visibilidad_Bks();

            if (oModelo == null || oModelo.Lista == null || oModelo.Lista.Count == 0)
            {
                List<Models.BackusDataMercaderista.Combo_Visibilidad_Bks> oLs = new List<Models.BackusDataMercaderista.Combo_Visibilidad_Bks>();
                Models.BackusDataMercaderista.Combo_Visibilidad_Bks oOb = new Models.BackusDataMercaderista.Combo_Visibilidad_Bks();
                oOb.cod_obs = "0";
                oOb.des_obs = "Sin datos disponibles.";
                oLs.Add(oOb);
                oModelo.Lista = oLs;
            }
            var modelDataTipo = oModelo.Lista.Select(u => new SelectListItem()
                {
                    Text = u.des_obs,
                    Value = u.cod_obs
                });
            return Json(modelDataTipo, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateVisibilidadBackus(string Datos)
        {
            List<E_Bks_Visibilidad> oLs = MvcApplication._Deserialize<List<E_Bks_Visibilidad>>(Datos);

            foreach (E_Bks_Visibilidad oOb in oLs)
            {
                Request_UpdateVisibilidadBackus oRq = new Request_UpdateVisibilidadBackus();
                Response_UpdateVisibilidadBackus oRp;

                oRq.codigoDet = oOb.cod_det;
                oRq.cantidad = oOb.cantidad;
                oRq.validado = oOb.validado;
                if (oOb.cod_tipo == "") { oRq.codTipo = 0; } else { oRq.codTipo = Convert.ToInt32(oOb.cod_tipo); }
                if (oOb.cod_tipo_a == "") { oRq.codTipoA = 0; } else { oRq.codTipoA = Convert.ToInt32(oOb.cod_tipo_a); }
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateVisibilidadBackus>(MvcApplication._Servicio_Operativa.Update_visibilidad_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateVisibilidadFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        #endregion

        #region PARTICIPACION
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte Participacion Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteParticipacion(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.empresa = empresa;

            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte participacion
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteParticipacionJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = "";
            oParametros.empresa = empresa;

            List<E_Bks_Participacion> oLs = new E_Bks_Participacion().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte participacion Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelParticipacion(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_participacion_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Participacion >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = "";
                oParametros.empresa = empresa;

                List<E_Bks_Participacion> oParticipacion = new E_Bks_Participacion().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Participacion >>>
                if ((oParticipacion != null) || (oParticipacion.Count > 0))
                {
                    Excel.ExcelWorksheet oWsParticipacion = oEx.Workbook.Worksheets.Add("Participacion");
                    oWsParticipacion.Cells[1, 1].Value = "CANAL";
                    oWsParticipacion.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsParticipacion.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsParticipacion.Cells[1, 4].Value = "CIUDAD";
                    oWsParticipacion.Cells[1, 5].Value = "DISTRITO";
                    oWsParticipacion.Cells[1, 6].Value = "CONO";
                    oWsParticipacion.Cells[1, 7].Value = "PERFIL";
                    oWsParticipacion.Cells[1, 8].Value = "GIE NOM";
                    oWsParticipacion.Cells[1, 9].Value = "GIE USER";
                    oWsParticipacion.Cells[1, 10].Value = "SUPERVISOR";
                    oWsParticipacion.Cells[1, 11].Value = "CADENA";
                    oWsParticipacion.Cells[1, 12].Value = "PDV BACKUS";
                    oWsParticipacion.Cells[1, 13].Value = "PDV";
                    oWsParticipacion.Cells[1, 14].Value = "CATEGORIA";
                    oWsParticipacion.Cells[1, 15].Value = "NIVEL";
                    oWsParticipacion.Cells[1, 16].Value = "MARCA";
                    oWsParticipacion.Cells[1, 17].Value = "COD. MATERIAL";
                    oWsParticipacion.Cells[1, 18].Value = "MATERIAL";
                    oWsParticipacion.Cells[1, 19].Value = "CANTIDAD";
                    oWsParticipacion.Cells[1, 20].Value = "COD. TIPO";
                    oWsParticipacion.Cells[1, 21].Value = "TIPO";
                    oWsParticipacion.Cells[1, 22].Value = "COD. TIPO A";
                    oWsParticipacion.Cells[1, 23].Value = "TIPO A";

                    oWsParticipacion.Cells[1, 24].Value = "COD. MEDIDA";
                    oWsParticipacion.Cells[1, 25].Value = "MEDIDA";

                    oWsParticipacion.Cells[1, 26].Value = "COMENTARIO";
                    oWsParticipacion.Cells[1, 27].Value = "FOTO";
                    oWsParticipacion.Cells[1, 28].Value = "MODIFICADO POR";
                    oWsParticipacion.Cells[1, 29].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Participacion oBj in oParticipacion)
                    {
                        oWsParticipacion.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsParticipacion.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsParticipacion.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsParticipacion.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsParticipacion.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsParticipacion.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsParticipacion.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsParticipacion.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsParticipacion.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsParticipacion.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsParticipacion.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsParticipacion.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsParticipacion.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsParticipacion.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsParticipacion.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsParticipacion.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsParticipacion.Cells[_fila, 17].Value = oBj.cod_material;
                        oWsParticipacion.Cells[_fila, 18].Value = oBj.des_material;
                        oWsParticipacion.Cells[_fila, 19].Value = oBj.cantidad;
                        oWsParticipacion.Cells[_fila, 20].Value = oBj.cod_tipo;
                        oWsParticipacion.Cells[_fila, 21].Value = oBj.des_tipo;
                        oWsParticipacion.Cells[_fila, 22].Value = oBj.cod_tipo_a;
                        oWsParticipacion.Cells[_fila, 23].Value = oBj.des_tipo_a;

                        oWsParticipacion.Cells[_fila, 24].Value = oBj.cod_medida;
                        oWsParticipacion.Cells[_fila, 25].Value = oBj.des_medida;

                        oWsParticipacion.Cells[_fila, 26].Value = oBj.des_comentario;
                        oWsParticipacion.Cells[_fila, 27].Value = oBj.des_foto;
                        oWsParticipacion.Cells[_fila, 28].Value = oBj.modificado_por;
                        oWsParticipacion.Cells[_fila, 29].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsParticipacion.SelectedRange[1, 1, 1, 29].AutoFilter = true;
                    oWsParticipacion.Row(1).Height = 25;
                    oWsParticipacion.Row(1).Style.Font.Bold = true;
                    oWsParticipacion.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsParticipacion.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsParticipacion.Column(1).Width = 15;
                    oWsParticipacion.Column(2).Width = 25;
                    oWsParticipacion.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsParticipacion.Column(5).Width = 30;
                    oWsParticipacion.Column(7).Width = 20;
                    oWsParticipacion.Column(8).Width = 10;
                    oWsParticipacion.Column(9).Width = 20;
                    oWsParticipacion.Column(10).Width = 15;
                    oWsParticipacion.Column(10).Width = 40;
                    oWsParticipacion.Column(11).Width = 20;
                    oWsParticipacion.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionParticipacionBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaParticipacion(
                    new Request_ValidacionParticipacionBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionParticipacionBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaParticipacion(
                    new Request_InValidacionParticipacionBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateParticipacionBackus(string Datos)
        {
            List<E_Bks_Participacion> oLs = MvcApplication._Deserialize<List<E_Bks_Participacion>>(Datos);

            foreach (E_Bks_Participacion oOb in oLs)
            {
                Request_UpdateParticipacionBackus oRq = new Request_UpdateParticipacionBackus();
                Response_UpdateParticipacionBackus oRp;

                oRq.codigoDet = oOb.cod_det;
                oRq.cantidad = oOb.cantidad;
                oRq.validado = oOb.validado;
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateParticipacionBackus>(MvcApplication._Servicio_Operativa.Update_participacion_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateParticipacionFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        #endregion

        #region ACTIVIDAD
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte Actividad Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteActividad(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.producto = producto;
            ViewBag.empresa =  empresa;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte ACTIVIDAD
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteActividadJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = producto;
            oParametros.empresa = empresa;

            List<E_Bks_Actividad> oLs = new E_Bks_Actividad().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte actividad Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelActividad(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_actividad_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Actividad >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = producto;
                oParametros.empresa = empresa;

                List<E_Bks_Actividad> oActividad = new E_Bks_Actividad().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Actividad >>>
                if ((oActividad != null) || (oActividad.Count > 0))
                {
                    Excel.ExcelWorksheet oWsActividad = oEx.Workbook.Worksheets.Add("Stock");
                    oWsActividad.Cells[1, 1].Value = "CANAL";
                    oWsActividad.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsActividad.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsActividad.Cells[1, 4].Value = "CIUDAD";
                    oWsActividad.Cells[1, 5].Value = "DISTRITO";
                    oWsActividad.Cells[1, 6].Value = "CONO";
                    oWsActividad.Cells[1, 7].Value = "PERFIL";
                    oWsActividad.Cells[1, 8].Value = "GIE NOM";
                    oWsActividad.Cells[1, 9].Value = "GIE USER";
                    oWsActividad.Cells[1, 10].Value = "SUPERVISOR";
                    oWsActividad.Cells[1, 11].Value = "CADENA";
                    oWsActividad.Cells[1, 12].Value = "PDV BACKUS";
                    oWsActividad.Cells[1, 13].Value = "PDV";
                    oWsActividad.Cells[1, 14].Value = "CATEGORIA";
                    oWsActividad.Cells[1, 15].Value = "NIVEL";
                    oWsActividad.Cells[1, 16].Value = "MARCA";
                    oWsActividad.Cells[1, 17].Value = "PRODUCTO";

                    oWsActividad.Cells[1, 18].Value = "OBJETIVO";
                    oWsActividad.Cells[1, 19].Value = "ACTIVIDAD";

                    oWsActividad.Cells[1, 20].Value = "CANTIDAD";
                    oWsActividad.Cells[1, 21].Value = "MECANICA";
                    oWsActividad.Cells[1, 22].Value = "FEC. INICIO ACT.";
                    oWsActividad.Cells[1, 23].Value = "FEC. FIN ACT.";
                    oWsActividad.Cells[1, 24].Value = "MATERIAL";
                    oWsActividad.Cells[1, 25].Value = "PRECIO REGULAR";
                    oWsActividad.Cells[1, 26].Value = "PRECIO OFERTA";
                    oWsActividad.Cells[1, 27].Value = "MODIFICADO POR";
                    oWsActividad.Cells[1, 28].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Actividad oBj in oActividad)
                    {
                        oWsActividad.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsActividad.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsActividad.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsActividad.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsActividad.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsActividad.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsActividad.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsActividad.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsActividad.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsActividad.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsActividad.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsActividad.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsActividad.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsActividad.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsActividad.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsActividad.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsActividad.Cells[_fila, 17].Value = oBj.des_producto;

                        oWsActividad.Cells[_fila, 18].Value = oBj.cod_objetivo;
                        oWsActividad.Cells[_fila, 19].Value = oBj.cod_actividad;

                        oWsActividad.Cells[_fila, 20].Value = oBj.cantidad;
                        oWsActividad.Cells[_fila, 21].Value = oBj.des_mecanica;
                        oWsActividad.Cells[_fila, 22].Value = oBj.fec_inicio_act;
                        oWsActividad.Cells[_fila, 23].Value = oBj.fec_fin_act;

                        oWsActividad.Cells[_fila, 24].Value = oBj.cod_material;

                        oWsActividad.Cells[_fila, 25].Value = oBj.precio_regular;
                        oWsActividad.Cells[_fila, 26].Value = oBj.precio_oferta;

                        oWsActividad.Cells[_fila, 27].Value = oBj.modificado_por;
                        oWsActividad.Cells[_fila, 28].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsActividad.SelectedRange[1, 1, 1, 28].AutoFilter = true;
                    oWsActividad.Row(1).Height = 25;
                    oWsActividad.Row(1).Style.Font.Bold = true;
                    oWsActividad.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsActividad.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsActividad.Column(1).Width = 15;
                    oWsActividad.Column(2).Width = 25;
                    oWsActividad.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsActividad.Column(5).Width = 30;
                    oWsActividad.Column(7).Width = 20;
                    oWsActividad.Column(8).Width = 10;
                    oWsActividad.Column(9).Width = 20;
                    oWsActividad.Column(10).Width = 15;
                    oWsActividad.Column(10).Width = 40;
                    oWsActividad.Column(11).Width = 20;
                    oWsActividad.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionActividadBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaActividad(
                    new Request_ValidacionActividadBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionActividadBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaActividad(
                    new Request_InValidacionActividadBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateActividadBackus(string Datos)
        {
            List<E_Bks_Actividad> oLs = MvcApplication._Deserialize<List<E_Bks_Actividad>>(Datos);

            foreach (E_Bks_Actividad oOb in oLs)
            {
                Request_UpdateActividadBackus oRq = new Request_UpdateActividadBackus();
                Response_UpdateActividadBackus oRp;

                oRq.codigoDet = oOb.cod_cab;
                oRq.cantidad = oOb.cantidad;
                oRq.validado = oOb.validado;
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateActividadBackus>(MvcApplication._Servicio_Operativa.Update_actividad_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateActividadFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        #endregion

        #region VENCIMIENTO PRODUCTO
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte Vencimiento Producto
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVencProducto(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.producto = producto;
            ViewBag.empresa = empresa;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte Vencimiento Producto
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVencProductoJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = producto;
            oParametros.empresa = empresa;

            List<E_Bks_VencProducto> oLs = new E_Bks_VencProducto().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte vencimiento producto Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        
        public JsonResult descargarExcelVencProducto(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_vencproducto_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Vencimiento Producto >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = producto;
                oParametros.empresa = empresa;

                List<E_Bks_VencProducto> oVencProducto = new E_Bks_VencProducto().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Vencimiento Producto >>>
                if ((oVencProducto != null) || (oVencProducto.Count > 0))
                {
                    Excel.ExcelWorksheet oWsVencProducto = oEx.Workbook.Worksheets.Add("Stock");
                    oWsVencProducto.Cells[1, 1].Value = "CANAL";
                    oWsVencProducto.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsVencProducto.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsVencProducto.Cells[1, 4].Value = "CIUDAD";
                    oWsVencProducto.Cells[1, 5].Value = "DISTRITO";
                    oWsVencProducto.Cells[1, 6].Value = "CONO";
                    oWsVencProducto.Cells[1, 7].Value = "PERFIL";
                    oWsVencProducto.Cells[1, 8].Value = "GIE NOM";
                    oWsVencProducto.Cells[1, 9].Value = "GIE USER";
                    oWsVencProducto.Cells[1, 10].Value = "SUPERVISOR";
                    oWsVencProducto.Cells[1, 11].Value = "CADENA";
                    oWsVencProducto.Cells[1, 12].Value = "PDV BACKUS";
                    oWsVencProducto.Cells[1, 13].Value = "PDV";
                    oWsVencProducto.Cells[1, 14].Value = "CATEGORIA";
                    oWsVencProducto.Cells[1, 15].Value = "NIVEL";
                    oWsVencProducto.Cells[1, 16].Value = "MARCA";
                    oWsVencProducto.Cells[1, 17].Value = "COD PRODUCTO";
                    oWsVencProducto.Cells[1, 18].Value = "PRODUCTO";
                    oWsVencProducto.Cells[1, 19].Value = "FECHA VENCIMIENTO";
                    oWsVencProducto.Cells[1, 20].Value = "CANTIDAD";
                    oWsVencProducto.Cells[1, 21].Value = "FOTO";
                    oWsVencProducto.Cells[1, 22].Value = "COMENTARIO";
                    oWsVencProducto.Cells[1, 23].Value = "MODIFICADO POR";
                    oWsVencProducto.Cells[1, 24].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_VencProducto oBj in oVencProducto)
                    {
                        oWsVencProducto.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsVencProducto.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsVencProducto.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsVencProducto.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsVencProducto.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsVencProducto.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsVencProducto.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsVencProducto.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsVencProducto.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsVencProducto.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsVencProducto.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsVencProducto.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsVencProducto.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsVencProducto.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsVencProducto.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsVencProducto.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsVencProducto.Cells[_fila, 17].Value = oBj.cod_producto;
                        oWsVencProducto.Cells[_fila, 18].Value = oBj.des_producto;
                        oWsVencProducto.Cells[_fila, 19].Value = oBj.fecha_vencimiento;
                        oWsVencProducto.Cells[_fila, 20].Value = oBj.cantidad;
                        oWsVencProducto.Cells[_fila, 21].Value = oBj.des_foto;
                        oWsVencProducto.Cells[_fila, 22].Value = oBj.comentario;
                        oWsVencProducto.Cells[_fila, 23].Value = oBj.modificado_por;
                        oWsVencProducto.Cells[_fila, 24].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsVencProducto.SelectedRange[1, 1, 1, 24].AutoFilter = true;
                    oWsVencProducto.Row(1).Height = 25;
                    oWsVencProducto.Row(1).Style.Font.Bold = true;
                    oWsVencProducto.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsVencProducto.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsVencProducto.Column(1).Width = 15;
                    oWsVencProducto.Column(2).Width = 25;
                    oWsVencProducto.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsVencProducto.Column(5).Width = 30;
                    oWsVencProducto.Column(7).Width = 20;
                    oWsVencProducto.Column(8).Width = 10;
                    oWsVencProducto.Column(9).Width = 20;
                    oWsVencProducto.Column(10).Width = 15;
                    oWsVencProducto.Column(10).Width = 40;
                    oWsVencProducto.Column(11).Width = 20;
                    oWsVencProducto.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionVencProductoBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaVencProducto(
                    new Request_ValidacionVencProductoBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionVencProductoBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaVencProducto(
                    new Request_InValidacionVencProductoBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateVencProductoBackus(string Datos)
        {
            List<E_Bks_VencProducto> oLs = MvcApplication._Deserialize<List<E_Bks_VencProducto>>(Datos);

            foreach (E_Bks_VencProducto oOb in oLs)
            {
                Request_UpdateVencProductoBackus oRq = new Request_UpdateVencProductoBackus();
                Response_UpdateVencProductoBackus oRp;

                oRq.codigoDet = Convert.ToInt32(oOb.cod_det);
                oRq.cantidad = oOb.cantidad;
                oRq.fechaVencimiento = Convert.ToDateTime(oOb.fecha_vencimiento);
                oRq.validado = oOb.validado;
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateVencProductoBackus>(MvcApplication._Servicio_Operativa.Update_vencproducto_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateVencProductoFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        #endregion

        #region CODIGO PDV
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 31-05-2015
        /// </summary> Reporte cod pdv Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteCodPDV(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 31-05-2015
        /// </summary> Reporte video
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteCodPDVJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = "";

            List<E_Bks_CodPDV> oLs = new E_Bks_CodPDV().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 31-05-2015
        /// </summary> Reporte video Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelCodPDV(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_codpdv_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Cod PDV >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = "";

                List<E_Bks_CodPDV> oCodPDV = new E_Bks_CodPDV().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Cod PDV >>>
                if ((oCodPDV != null) || (oCodPDV.Count > 0))
                {
                    Excel.ExcelWorksheet oWsCodPDV = oEx.Workbook.Worksheets.Add("CodPDV");
                    oWsCodPDV.Cells[1, 1].Value = "CANAL";
                    oWsCodPDV.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsCodPDV.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsCodPDV.Cells[1, 4].Value = "CIUDAD";
                    oWsCodPDV.Cells[1, 5].Value = "DISTRITO";
                    oWsCodPDV.Cells[1, 6].Value = "CONO";
                    oWsCodPDV.Cells[1, 7].Value = "PERFIL";
                    oWsCodPDV.Cells[1, 8].Value = "GIE NOM";
                    oWsCodPDV.Cells[1, 9].Value = "GIE USER";
                    oWsCodPDV.Cells[1, 10].Value = "SUPERVISOR";
                    oWsCodPDV.Cells[1, 11].Value = "CADENA";
                    oWsCodPDV.Cells[1, 12].Value = "PDV BACKUS";
                    oWsCodPDV.Cells[1, 13].Value = "PDV";
                    oWsCodPDV.Cells[1, 14].Value = "CATEGORIA";
                    oWsCodPDV.Cells[1, 15].Value = "NIVEL";
                    oWsCodPDV.Cells[1, 16].Value = "MARCA";
                    oWsCodPDV.Cells[1, 17].Value = "VIDEO";
                    oWsCodPDV.Cells[1, 18].Value = "COMENTARIO";
                    oWsCodPDV.Cells[1, 19].Value = "MODIFICADO POR";
                    oWsCodPDV.Cells[1, 20].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_CodPDV oBj in oCodPDV)
                    {
                        oWsCodPDV.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsCodPDV.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsCodPDV.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsCodPDV.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsCodPDV.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsCodPDV.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsCodPDV.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsCodPDV.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsCodPDV.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsCodPDV.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsCodPDV.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsCodPDV.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsCodPDV.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsCodPDV.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsCodPDV.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsCodPDV.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsCodPDV.Cells[_fila, 17].Value = oBj.des_video;
                        oWsCodPDV.Cells[_fila, 18].Value = oBj.des_video_comentario;
                        oWsCodPDV.Cells[_fila, 19].Value = oBj.modificado_por;
                        oWsCodPDV.Cells[_fila, 20].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsCodPDV.SelectedRange[1, 1, 1, 20].AutoFilter = true;
                    oWsCodPDV.Row(1).Height = 25;
                    oWsCodPDV.Row(1).Style.Font.Bold = true;
                    oWsCodPDV.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsCodPDV.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsCodPDV.Column(1).Width = 15;
                    oWsCodPDV.Column(2).Width = 25;
                    oWsCodPDV.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsCodPDV.Column(5).Width = 30;
                    oWsCodPDV.Column(7).Width = 20;
                    oWsCodPDV.Column(8).Width = 10;
                    oWsCodPDV.Column(9).Width = 20;
                    oWsCodPDV.Column(10).Width = 15;
                    oWsCodPDV.Column(10).Width = 40;
                    oWsCodPDV.Column(11).Width = 20;
                    oWsCodPDV.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionCodPDVBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaCodPDV(
                    new Request_ValidacionCodPDVBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionCodPDVBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaCodPDV(
                    new Request_InValidacionCodPDVBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }
        #endregion

        #region VIDEO
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 31-05-2015
        /// </summary> Reporte VIDEO Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVideo(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.empresa = empresa;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 31-05-2015
        /// </summary> Reporte video
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVideoJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = "";
            oParametros.empresa = empresa;

            List<E_Bks_Video> oLs = new E_Bks_Video().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 31-05-2015
        /// </summary> Reporte video Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelVideo(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_video_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Video >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = "";
                oParametros.empresa = empresa;

                List<E_Bks_Video> oVideo = new E_Bks_Video().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Video >>>
                if ((oVideo != null) || (oVideo.Count > 0))
                {
                    Excel.ExcelWorksheet oWsVideo = oEx.Workbook.Worksheets.Add("Video");
                    oWsVideo.Cells[1, 1].Value = "CANAL";
                    oWsVideo.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsVideo.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsVideo.Cells[1, 4].Value = "CIUDAD";
                    oWsVideo.Cells[1, 5].Value = "DISTRITO";
                    oWsVideo.Cells[1, 6].Value = "CONO";
                    oWsVideo.Cells[1, 7].Value = "PERFIL";
                    oWsVideo.Cells[1, 8].Value = "GIE NOM";
                    oWsVideo.Cells[1, 9].Value = "GIE USER";
                    oWsVideo.Cells[1, 10].Value = "SUPERVISOR";
                    oWsVideo.Cells[1, 11].Value = "CADENA";
                    oWsVideo.Cells[1, 12].Value = "PDV BACKUS";
                    oWsVideo.Cells[1, 13].Value = "PDV";
                    oWsVideo.Cells[1, 14].Value = "CATEGORIA";
                    oWsVideo.Cells[1, 15].Value = "NIVEL";
                    oWsVideo.Cells[1, 16].Value = "MARCA";
                    oWsVideo.Cells[1, 17].Value = "VIDEO";
                    oWsVideo.Cells[1, 18].Value = "COMENTARIO";
                    oWsVideo.Cells[1, 19].Value = "MODIFICADO POR";
                    oWsVideo.Cells[1, 20].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Video oBj in oVideo)
                    {
                        oWsVideo.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsVideo.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsVideo.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsVideo.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsVideo.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsVideo.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsVideo.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsVideo.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsVideo.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsVideo.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsVideo.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsVideo.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsVideo.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsVideo.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsVideo.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsVideo.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsVideo.Cells[_fila, 17].Value = oBj.des_video;
                        oWsVideo.Cells[_fila, 18].Value = oBj.des_video_comentario;
                        oWsVideo.Cells[_fila, 19].Value = oBj.modificado_por;
                        oWsVideo.Cells[_fila, 20].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsVideo.SelectedRange[1, 1, 1, 20].AutoFilter = true;
                    oWsVideo.Row(1).Height = 25;
                    oWsVideo.Row(1).Style.Font.Bold = true;
                    oWsVideo.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsVideo.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsVideo.Column(1).Width = 15;
                    oWsVideo.Column(2).Width = 25;
                    oWsVideo.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsVideo.Column(5).Width = 30;
                    oWsVideo.Column(7).Width = 20;
                    oWsVideo.Column(8).Width = 10;
                    oWsVideo.Column(9).Width = 20;
                    oWsVideo.Column(10).Width = 15;
                    oWsVideo.Column(10).Width = 40;
                    oWsVideo.Column(11).Width = 20;
                    oWsVideo.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionVideoBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaVideo(
                    new Request_ValidacionVideoBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionVideoBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaVideo(
                    new Request_InValidacionVideoBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateVideoBackus(string Datos)
        {
            List<E_Bks_Video> oLs = MvcApplication._Deserialize<List<E_Bks_Video>>(Datos);

            foreach (E_Bks_Video oOb in oLs)
            {
                Request_UpdateVideoBackus oRq = new Request_UpdateVideoBackus();
                Response_UpdateVideoBackus oRp;

                oRq.codigoDet = oOb.cod_det;
                oRq.comentario = oOb.des_video_comentario;
                oRq.validado = oOb.validado;
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateVideoBackus>(MvcApplication._Servicio_Operativa.Update_video_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateVideoFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }
        #endregion

        #region FOTOGRAFICO
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 29-05-2015
        /// </summary> Reporte Fotografico Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteFotografico(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.empresa = empresa;
            //ViewBag.producto = producto;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 29-05-2015
        /// </summary> Reporte fotografico
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteFotograficoJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = "";
            oParametros.empresa = empresa;

            List<E_Bks_Fotografico> oLs = new E_Bks_Fotografico().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 29-05-2015
        /// </summary> Reporte fotografico Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelFotografico(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_fotografico_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Fotografico >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = "";
                oParametros.empresa = empresa;

                List<E_Bks_Fotografico> oFotografico = new E_Bks_Fotografico().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Fotografico >>>
                if ((oFotografico != null) || (oFotografico.Count > 0))
                {
                    Excel.ExcelWorksheet oWsFotografico = oEx.Workbook.Worksheets.Add("Comentario");
                    oWsFotografico.Cells[1, 1].Value = "CANAL";
                    oWsFotografico.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsFotografico.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsFotografico.Cells[1, 4].Value = "CIUDAD";
                    oWsFotografico.Cells[1, 5].Value = "DISTRITO";
                    oWsFotografico.Cells[1, 6].Value = "CONO";
                    oWsFotografico.Cells[1, 7].Value = "PERFIL";
                    oWsFotografico.Cells[1, 8].Value = "GIE NOM";
                    oWsFotografico.Cells[1, 9].Value = "GIE USER";
                    oWsFotografico.Cells[1, 10].Value = "SUPERVISOR";
                    oWsFotografico.Cells[1, 11].Value = "CADENA";
                    oWsFotografico.Cells[1, 12].Value = "PDV BACKUS";
                    oWsFotografico.Cells[1, 13].Value = "PDV";
                    oWsFotografico.Cells[1, 14].Value = "CATEGORIA";
                    oWsFotografico.Cells[1, 15].Value = "NIVEL";
                    oWsFotografico.Cells[1, 16].Value = "MARCA";
                    oWsFotografico.Cells[1, 17].Value = "COMENTARIO";
                    oWsFotografico.Cells[1, 18].Value = "FOTO";
                    oWsFotografico.Cells[1, 19].Value = "MODIFICADO POR";
                    oWsFotografico.Cells[1, 20].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Fotografico oBj in oFotografico)
                    {
                        oWsFotografico.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsFotografico.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsFotografico.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsFotografico.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsFotografico.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsFotografico.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsFotografico.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsFotografico.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsFotografico.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsFotografico.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsFotografico.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsFotografico.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsFotografico.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsFotografico.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsFotografico.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsFotografico.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsFotografico.Cells[_fila, 17].Value = oBj.comentario;
                        oWsFotografico.Cells[_fila, 18].Value = oBj.des_foto;
                        oWsFotografico.Cells[_fila, 19].Value = oBj.modificado_por;
                        oWsFotografico.Cells[_fila, 20].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsFotografico.SelectedRange[1, 1, 1, 20].AutoFilter = true;
                    oWsFotografico.Row(1).Height = 25;
                    oWsFotografico.Row(1).Style.Font.Bold = true;
                    oWsFotografico.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsFotografico.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsFotografico.Column(1).Width = 15;
                    oWsFotografico.Column(2).Width = 25;
                    oWsFotografico.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsFotografico.Column(5).Width = 30;
                    oWsFotografico.Column(7).Width = 20;
                    oWsFotografico.Column(8).Width = 10;
                    oWsFotografico.Column(9).Width = 20;
                    oWsFotografico.Column(10).Width = 15;
                    oWsFotografico.Column(10).Width = 40;
                    oWsFotografico.Column(11).Width = 20;
                    oWsFotografico.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionFotograficoBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaFotografico(
                    new Request_ValidacionFotograficoBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionFotograficoBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaFotografico(
                    new Request_InValidacionFotograficoBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateFotograficoBackus(string Datos)
        {
            List<E_Bks_Fotografico> oLs = MvcApplication._Deserialize<List<E_Bks_Fotografico>>(Datos);

            foreach (E_Bks_Fotografico oOb in oLs)
            {
                Request_UpdateFotograficoBackus oRq = new Request_UpdateFotograficoBackus();
                Response_UpdateFotograficoBackus oRp;

                oRq.codigoDet = oOb.cod_det;
                oRq.comentario = oOb.comentario;
                oRq.validado = oOb.validado;
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateFotograficoBackus>(MvcApplication._Servicio_Operativa.Update_fotografico_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateFotograficoFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        #endregion

        #region VENTA

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 29-05-2015
        /// </summary> Reporte venta Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVenta(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            ViewBag.categoria = categoria;
            ViewBag.nivel = nivel;
            ViewBag.marca = marca;
            ViewBag.producto = producto;
            ViewBag.empresa = empresa;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 29-05-2015
        /// </summary> Reporte Venta
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVentaJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = categoria;
            oParametros.nivel = nivel;
            oParametros.marca = marca;
            oParametros.producto = producto;
            oParametros.empresa = empresa;

            List<E_Bks_Venta> oLs = new E_Bks_Venta().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 29-05-2015
        /// </summary> Reporte Venta Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        
        [HttpPost]
        public JsonResult descargarExcelVenta(int reporte, int canal, string fec_ini, string fec_fin, int cadena, int categoria, string nivel, int marca, string producto, string empresa)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_venta_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Venta >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = categoria;
                oParametros.nivel = nivel;
                oParametros.marca = marca;
                oParametros.producto = producto;
                oParametros.empresa = empresa;

                List<E_Bks_Venta> oVenta = new E_Bks_Venta().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Venta >>>
                if ((oVenta != null) || (oVenta.Count > 0))
                {
                    Excel.ExcelWorksheet oWsVenta = oEx.Workbook.Worksheets.Add("Venta");
                    oWsVenta.Cells[1, 1].Value = "CANAL";
                    oWsVenta.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsVenta.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsVenta.Cells[1, 4].Value = "CIUDAD";
                    oWsVenta.Cells[1, 5].Value = "DISTRITO";
                    oWsVenta.Cells[1, 6].Value = "CONO";
                    oWsVenta.Cells[1, 7].Value = "PERFIL";
                    oWsVenta.Cells[1, 8].Value = "GIE NOM";
                    oWsVenta.Cells[1, 9].Value = "GIE USER";
                    oWsVenta.Cells[1, 10].Value = "SUPERVISOR";
                    oWsVenta.Cells[1, 11].Value = "CADENA";
                    oWsVenta.Cells[1, 12].Value = "PDV BACKUS";
                    oWsVenta.Cells[1, 13].Value = "PDV";
                    oWsVenta.Cells[1, 14].Value = "CATEGORIA";
                    oWsVenta.Cells[1, 15].Value = "NIVEL";
                    oWsVenta.Cells[1, 16].Value = "MARCA";
                    oWsVenta.Cells[1, 17].Value = "PRODUCTO";
                    oWsVenta.Cells[1, 18].Value = "CANTIDAD";
                    oWsVenta.Cells[1, 19].Value = "TIPO";
                    oWsVenta.Cells[1, 20].Value = "FOTO";
                    oWsVenta.Cells[1, 21].Value = "MODIFICADO POR";
                    oWsVenta.Cells[1, 22].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Venta oBj in oVenta)
                    {
                        oWsVenta.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsVenta.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsVenta.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsVenta.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsVenta.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsVenta.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsVenta.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsVenta.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsVenta.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsVenta.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsVenta.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsVenta.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsVenta.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsVenta.Cells[_fila, 14].Value = oBj.des_categoria;
                        oWsVenta.Cells[_fila, 15].Value = oBj.des_nivel;
                        oWsVenta.Cells[_fila, 16].Value = oBj.des_marca;
                        oWsVenta.Cells[_fila, 17].Value = oBj.des_producto;
                        oWsVenta.Cells[_fila, 18].Value = oBj.cantidad;
                        oWsVenta.Cells[_fila, 19].Value = oBj.desc_tipo;
                        oWsVenta.Cells[_fila, 20].Value = oBj.des_foto;
                        oWsVenta.Cells[_fila, 21].Value = oBj.modificado_por;
                        oWsVenta.Cells[_fila, 22].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsVenta.SelectedRange[1, 1, 1, 22].AutoFilter = true;
                    oWsVenta.Row(1).Height = 25;
                    oWsVenta.Row(1).Style.Font.Bold = true;
                    oWsVenta.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsVenta.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsVenta.Column(1).Width = 15;
                    oWsVenta.Column(2).Width = 25;
                    oWsVenta.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsVenta.Column(5).Width = 30;
                    oWsVenta.Column(7).Width = 20;
                    oWsVenta.Column(8).Width = 10;
                    oWsVenta.Column(9).Width = 20;
                    oWsVenta.Column(10).Width = 15;
                    oWsVenta.Column(10).Width = 40;
                    oWsVenta.Column(11).Width = 20;
                    oWsVenta.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionVentaBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaVenta(
                    new Request_ValidacionVentaBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionVentaBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaVenta(
                    new Request_InValidacionVentaBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateVentaBackus(string Datos)
        {
            List<E_Bks_Venta> oLs = MvcApplication._Deserialize<List<E_Bks_Venta>>(Datos);

            foreach (E_Bks_Venta oOb in oLs)
            {
                Request_UpdateVentaBackus oRq = new Request_UpdateVentaBackus();
                Response_UpdateVentaBackus oRp;

                oRq.codigoDet = Convert.ToInt32(oOb.cod_det);
                oRq.cantidad = oOb.cantidad;
                oRq.tipo = oOb.tipo;
                oRq.validado = oOb.validado;
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateVentaBackus>(MvcApplication._Servicio_Operativa.Update_venta_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateVentaFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }
        #endregion

        #region COMENTARIO
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-05-2015
        /// </summary> Reporte Stock Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteComentario(int reporte, int canal, string fec_ini, string fec_fin, int cadena)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            //ViewBag.categoria = categoria;
            //ViewBag.nivel = nivel;
            //ViewBag.marca = marca;
            //ViewBag.producto = producto;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 26-05-2015
        /// </summary> Reporte STOCK
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteComentarioJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = 0;
            oParametros.nivel = "";
            oParametros.marca = 0;
            oParametros.producto = "";
            oParametros.empresa = "";

            List<E_Bks_Comentario> oLs = new E_Bks_Comentario().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 29-05-2015
        /// </summary> Reporte comentario Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelComentario(int reporte, int canal, string fec_ini, string fec_fin, int cadena)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_comentario_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Comentario >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = 0;
                oParametros.nivel = "";
                oParametros.marca = 0;
                oParametros.producto = "";
                oParametros.empresa = "";

                List<E_Bks_Comentario> oComentario = new E_Bks_Comentario().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Comentario >>>
                if ((oComentario != null) || (oComentario.Count > 0))
                {
                    Excel.ExcelWorksheet oWsComentario = oEx.Workbook.Worksheets.Add("Comentario");
                    oWsComentario.Cells[1, 1].Value = "CANAL";
                    oWsComentario.Cells[1, 2].Value = "FEC. RE. CEL";
                    oWsComentario.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsComentario.Cells[1, 4].Value = "CIUDAD";
                    oWsComentario.Cells[1, 5].Value = "DISTRITO";
                    oWsComentario.Cells[1, 6].Value = "CONO";
                    oWsComentario.Cells[1, 7].Value = "PERFIL";
                    oWsComentario.Cells[1, 8].Value = "GIE NOM";
                    oWsComentario.Cells[1, 9].Value = "GIE USER";
                    oWsComentario.Cells[1, 10].Value = "SUPERVISOR";
                    oWsComentario.Cells[1, 11].Value = "CADENA";
                    oWsComentario.Cells[1, 12].Value = "PDV BACKUS";
                    oWsComentario.Cells[1, 13].Value = "PDV";
                    oWsComentario.Cells[1, 14].Value = "COMENTARIO";
                    oWsComentario.Cells[1, 15].Value = "FOTO";
                    oWsComentario.Cells[1, 16].Value = "MODIFICADO POR";
                    oWsComentario.Cells[1, 17].Value = "VALIDADO";

                    _fila = 2;
                    foreach (E_Bks_Comentario oBj in oComentario)
                    {
                        oWsComentario.Cells[_fila, 1].Value = oBj.des_canal;
                        oWsComentario.Cells[_fila, 2].Value = oBj.fec_cel;
                        oWsComentario.Cells[_fila, 3].Value = oBj.fec_bd;
                        oWsComentario.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsComentario.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsComentario.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsComentario.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsComentario.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsComentario.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsComentario.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsComentario.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsComentario.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsComentario.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsComentario.Cells[_fila, 14].Value = oBj.comentario;
                        oWsComentario.Cells[_fila, 15].Value = oBj.des_foto;
                        oWsComentario.Cells[_fila, 16].Value = oBj.modificado_por;
                        oWsComentario.Cells[_fila, 17].Value = oBj.validado;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsComentario.SelectedRange[1, 1, 1, 17].AutoFilter = true;
                    oWsComentario.Row(1).Height = 25;
                    oWsComentario.Row(1).Style.Font.Bold = true;
                    oWsComentario.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsComentario.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsComentario.Column(1).Width = 15;
                    oWsComentario.Column(2).Width = 25;
                    oWsComentario.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsComentario.Column(5).Width = 30;
                    oWsComentario.Column(7).Width = 20;
                    oWsComentario.Column(8).Width = 10;
                    oWsComentario.Column(9).Width = 20;
                    oWsComentario.Column(10).Width = 15;
                    oWsComentario.Column(10).Width = 40;
                    oWsComentario.Column(11).Width = 20;
                    oWsComentario.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult validacionComentarioBackus(int reporte, string validacion)
        {
            if (validacion == null) validacion = "";

            return Json(new E_Bks_Validacion().ValidaComentario(
                    new Request_ValidacionComentarioBackus()
                    {
                        reporte = reporte,
                        validado = validacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [HttpPost]
        public ActionResult inValidacionComentarioBackus(int reporte, string invalidacion)
        {
            if (invalidacion == null) invalidacion = "";

            return Json(new E_Bks_Validacion().InvalidaComentario(
                    new Request_InValidacionComentarioBackus()
                    {
                        reporte = reporte,
                        invalidado = invalidacion,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateComentarioBackus(string Datos)
        {
            List<E_Bks_Comentario> oLs = MvcApplication._Deserialize<List<E_Bks_Comentario>>(Datos);

            foreach (E_Bks_Comentario oOb in oLs)
            {
                Request_UpdateComentarioBackus oRq = new Request_UpdateComentarioBackus();
                Response_UpdateComentarioBackus oRp;

                oRq.codigoDet = oOb.cod_cab;
                oRq.comentario = oOb.comentario;
                oRq.validado = oOb.validado;
                oRq.fechCel = Convert.ToDateTime(oOb.fec_cel);
                oRq.user_modif = ((Persona)Session["Session_Login"]).name_user.ToString();

                oRp = MvcApplication._Deserialize<Response_UpdateComentarioBackus>(MvcApplication._Servicio_Operativa.Update_comentario_backus_DM(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updatecomentarioFechaBackus(int reporte, string codigo, DateTime fechCel, string usuario)
        {
            if (codigo == null) codigo = "";

            return Json(new E_Bks_Validacion().UpdateFechas(
                    new Request_UpdateFechaBackusDM()
                    {
                        reporte = reporte,
                        codigo = codigo,
                        fechCel = fechCel,
                        usuario = ((Persona)Session["Session_Login"]).name_user.ToString()
                    }
                ));
        }
        #endregion

        #region VISITA
        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte Visita Backus
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVisita(int reporte, int canal, string fec_ini, string fec_fin, int cadena)
        {
            ViewBag.reporte = reporte;
            ViewBag.canal = canal;
            ViewBag.fec_ini = fec_ini;
            ViewBag.fec_fin = fec_fin;
            ViewBag.cadena = cadena;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte visita
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVisitaJson(int reporte, int canal, string fec_ini, string fec_fin, int cadena)
        {

            E_Bks_Parametros oParametros = new E_Bks_Parametros();
            oParametros.id_reporte = reporte;
            oParametros.id_tipo_canal = canal;
            oParametros.fec_inicio = fec_ini;
            oParametros.fec_fin = fec_fin;
            oParametros.cadena = cadena;
            oParametros.categoria = 0;
            oParametros.nivel = "";
            oParametros.marca = 0;
            oParametros.producto = "";
            oParametros.empresa = "";

            List<E_Bks_Visita> oLs = new E_Bks_Visita().Lista(
                new Request_E_Bks_Parametros()
                {
                    oParametros = oParametros
                });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }


        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 30-05-2015
        /// </summary> Reporte visita Backus (Excel)
        /// <param name="__a"></param>
        /// <returns></returns>        [HttpPost]
        public JsonResult descargarExcelVisita(int reporte, int canal, string fec_ini, string fec_fin, int cadena)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("reporte_backus_visita_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>

                #region << Reporte Visita >>


                E_Bks_Parametros oParametros = new E_Bks_Parametros();
                oParametros.id_reporte = reporte;
                oParametros.id_tipo_canal = canal;
                oParametros.fec_inicio = fec_ini;
                oParametros.fec_fin = fec_fin;
                oParametros.cadena = cadena;
                oParametros.categoria = 0;
                oParametros.nivel = "";
                oParametros.marca = 0;
                oParametros.producto = "";
                oParametros.empresa = "";

                List<E_Bks_Visita> oVisita = new E_Bks_Visita().Lista(
                    new Request_E_Bks_Parametros()
                    {
                        oParametros = oParametros
                    }
                );

                #endregion

                #endregion

                #region <<< Reporte Comentario >>>
                if ((oVisita != null) || (oVisita.Count > 0))
                {
                    Excel.ExcelWorksheet oWsVisita = oEx.Workbook.Worksheets.Add("Visita");
                    oWsVisita.Cells[1, 1].Value = "ID";
                    oWsVisita.Cells[1, 2].Value = "CANAL";
                    oWsVisita.Cells[1, 3].Value = "FEC. REG. BD";
                    oWsVisita.Cells[1, 4].Value = "CIUDAD";
                    oWsVisita.Cells[1, 5].Value = "DISTRITO";
                    oWsVisita.Cells[1, 6].Value = "CONO";
                    oWsVisita.Cells[1, 7].Value = "PERFIL";
                    oWsVisita.Cells[1, 8].Value = "GIE NOM";
                    oWsVisita.Cells[1, 9].Value = "GIE USER";
                    oWsVisita.Cells[1, 10].Value = "SUPERVISOR";
                    oWsVisita.Cells[1, 11].Value = "CADENA";
                    oWsVisita.Cells[1, 12].Value = "PDV BACKUS";
                    oWsVisita.Cells[1, 13].Value = "PDV";
                    oWsVisita.Cells[1, 14].Value = "COD NOVISITA";
                    oWsVisita.Cells[1, 15].Value = "DES NOVISITA";

                    oWsVisita.Cells[1, 16].Value = "FEC. REG. INICIO";
                    oWsVisita.Cells[1, 17].Value = "LATITUD INICIO";
                    oWsVisita.Cells[1, 18].Value = "LONGITUD INICIO";
                    oWsVisita.Cells[1, 19].Value = "FEC. REG. FIN";
                    oWsVisita.Cells[1, 20].Value = "LATITUD FIN";
                    oWsVisita.Cells[1, 21].Value = "LONGITUD FIN";

                    oWsVisita.Cells[1, 22].Value = "MODIFICADO POR";

                    _fila = 2;
                    foreach (E_Bks_Visita oBj in oVisita)
                    {
                        oWsVisita.Cells[_fila, 1].Value = oBj.cod_cab;
                        oWsVisita.Cells[_fila, 1].Style.Locked = true;
                        oWsVisita.Cells[_fila, 2].Value = oBj.des_canal;
                        oWsVisita.Cells[_fila, 3].Value = oBj.fecha;
                        oWsVisita.Cells[_fila, 4].Value = oBj.des_ciudad;
                        oWsVisita.Cells[_fila, 5].Value = oBj.des_distrito;
                        oWsVisita.Cells[_fila, 6].Value = oBj.des_cono;
                        oWsVisita.Cells[_fila, 7].Value = oBj.des_perfil;
                        oWsVisita.Cells[_fila, 8].Value = oBj.des_gie_nom;
                        oWsVisita.Cells[_fila, 9].Value = oBj.des_gie_use;
                        oWsVisita.Cells[_fila, 10].Value = oBj.des_supervisor;
                        oWsVisita.Cells[_fila, 11].Value = oBj.des_cadena;
                        oWsVisita.Cells[_fila, 12].Value = oBj.cod_pdv_backus;
                        oWsVisita.Cells[_fila, 13].Value = oBj.des_pdv;
                        oWsVisita.Cells[_fila, 14].Value = oBj.cod_novisita;
                        oWsVisita.Cells[_fila, 15].Value = oBj.des_novista;

                        oWsVisita.Cells[_fila, 16].Value = oBj.fec_reg_ini;
                        oWsVisita.Cells[_fila, 17].Value = oBj.latitud_ini;
                        oWsVisita.Cells[_fila, 18].Value = oBj.longitud_ini;

                        oWsVisita.Cells[_fila, 19].Value = oBj.fec_reg_fin;
                        oWsVisita.Cells[_fila, 20].Value = oBj.latitud_fin;
                        oWsVisita.Cells[_fila, 21].Value = oBj.longitud_fin;

                        oWsVisita.Cells[_fila, 22].Value = oBj.modificado_por;
                        _fila++;

                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsVisita.SelectedRange[1, 1, 1, 22].AutoFilter = true;
                    oWsVisita.Row(1).Height = 25;
                    oWsVisita.Row(1).Style.Font.Bold = true;
                    oWsVisita.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsVisita.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsVisita.Column(1).Width = 0;
                    oWsVisita.Column(2).Width = 25;
                    oWsVisita.Column(3).Width = 25;
                    //oWsPrecio.Column(3).AutoFit();
                    oWsVisita.Column(5).Width = 30;
                    oWsVisita.Column(7).Width = 20;
                    oWsVisita.Column(8).Width = 10;
                    oWsVisita.Column(9).Width = 20;
                    oWsVisita.Column(10).Width = 15;
                    oWsVisita.Column(10).Width = 40;
                    oWsVisita.Column(11).Width = 20;
                    oWsVisita.Column(12).Width = 20;
                }
                #endregion
                oEx.Save();
            }
            return Json(new { Archivo = _fileServer });
        }

        #endregion

    }
}
