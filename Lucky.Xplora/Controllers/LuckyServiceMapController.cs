﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Map;

namespace Lucky.Xplora.Controllers
{
    public class LuckyServiceMapController : Controller
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-22
        /// Demos: @Scripts.Render("~/LuckyServiceMap/MapJson?__a=1023&__b=1")
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MapJson(string __a, string __b,int __c)
        {
            return View(
                new MapJson().Objeto(
                    new Request_MapJson() { 
                        compania = __c,//1562,//((Persona)Session["Session_Login"]).Company_id, 
                        canal = Convert.ToInt32(__a), 
                        grupo = Convert.ToInt32(__b) 
                    }
                )
            );
        }
        
        public ActionResult MapMenuCapa()
        {
            return View(
                new Map_Menu_Capa().Objeto(
                    new Request_Map_Menu_Capa()
                    {
                        compania = 1562,//((Persona)Session["Session_Login"]).Company_id, 
                        canal = 1023                        
                    }
                )
            );
        }
        public ActionResult MapMenuCapaApiG(string __a)
        {
            int vcompania = 1562;
            int canal = 1023;
            if (__a == "9020110152013" || __a == "9020116052016")
            {
                vcompania = 1637;
                canal = 1241;
            }
            ViewBag.vgcanal = canal;
            ViewBag.vgempresa = vcompania;
            return View(
                new Map_Menu_Capa().Objeto(
                    new Request_Map_Menu_Capa()
                    {
                        compania = vcompania,//((Persona)Session["Session_Login"]).Company_id, 
                        canal = canal
                    }
                )
            );
        }

        public ActionResult MapMenuCapaApiGFusionColg()
        {
            int vcompania = 1561;
            int canal = 1000;

            ViewBag.vgcanal = canal;
            ViewBag.vgempresa = vcompania;
            return View(
                new Map_Menu_Capa().Objeto(
                    new Request_Map_Menu_Capa()
                    {
                        compania = 1561,
                        canal = 1000
                    }
                )
            );
        }
    }
}
