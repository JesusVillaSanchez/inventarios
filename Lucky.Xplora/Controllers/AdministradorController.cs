﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Administrador;
using Lucky.Xplora.Models.ColgateReporting;
using Lucky.Xplora.Models.Module;
using Lucky.Xplora.Security;

using Excel = OfficeOpenXml;

namespace Lucky.Xplora.Controllers
{
    public class AdministradorController : Controller
    {
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

        #region Administrador
        //
        // GET: /Administrador/
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            if (Request["_a"] != null)
            {
                Session["Session_Campania"] = Request["_a"];
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();

            }
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            ViewBag.Company = ((Persona)Session["Session_Login"]).Company_id;
            ViewBag.tpf_id = ((Persona)Session["Session_Login"]).tpf_id;
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-21
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize]
        public ActionResult Index(string __a) {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new ControlAcceso().Lista(new RequestControlAcceso()
                {
                    anio = 0,
                    mes = 0,
                    usu = 0,
                    per = 0,
                    prf = 0,
                    parametro = __a
                })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-06-27
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Descarga(string __a) {
            int _fila = 2;
            int _columna = 2;
            string _fileServer = "";
            string _filePath = "";

            List<ControlAcceso> lCo = MvcApplication._Deserialize<List<ControlAcceso>>(__a);

            _fileServer = String.Format("{0:yyyyMMdd_hhmm}.xlsx", DateTime.Now);
            _filePath = Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);
            if (_fileNew.Exists)
            {
                _fileNew.Delete();
                _fileNew = new FileInfo(_filePath);
            }

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region Reporte
                Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Visita");

                foreach(ControlAcceso oCo in lCo){
                    if (lCo.First() == oCo) {
                        oWs.Cells[1, 1].Value = "USUARIOS";
                    }

                    oWs.Cells[_fila, 1].Value = oCo.per_nombre;

                    foreach (ControlAccesoVista oVi in oCo.vista) {
                        if (lCo.First() == oCo)
                        {
                            oWs.Cells[1, _columna].Value = oVi.vis_descripcion;
                        }

                        oWs.Cells[_fila, _columna].Value = oVi.visita;

                        _columna++;
                    }

                    if (lCo.First() == oCo)
                    {
                        oWs.Cells[1, _columna].Value = "TOTAL";
                    }

                    oWs.Cells[_fila, _columna].Value = oCo.ingreso;

                    _fila++;
                    _columna = 2;
                }
                #endregion

                oEx.Save();
            }

            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult _GetAnio()
        {
            return View(new Anio().Lista());
        }

        [HttpPost]
        public ActionResult _GetMMes(int __a)
        {
            //return View(new Lucky.Xplora.Models.Administrador.Mes().Lista());            

            List<Models.Mes> lista = (new Lucky.Xplora.Models.Mes()).Lista(
                    new Request_GetMes_por_anio()
                    {
                        anio = __a
                    }
                );
            ViewBag.lista = lista;
             return View();            
        }

        [HttpPost]
        public ActionResult _GetUsuarios(int __b) {
            int a = (((Persona)Session["Session_Login"]).Person_id);
            return View(new Usuario().Lista(new Request_usuario(){
                    persona = a,
                    tipo_perfil = __b
            }));
        }

        [HttpPost]
        public ActionResult _GetTipo_Perfil()
        {
            int __a = (((Persona)Session["Session_Login"]).Company_id);
            var lista = (new Perfil().Lista(new Request_GetListTipoPerfil(){
                company = __a
            }));
            return Json(lista);
        }

        [HttpPost]
        public ActionResult Listar_Usuarios_Acceso(int anio, int mes, int usuario, int t_perfil)
        {
            ViewBag.Anio = anio;
            ViewBag.Mes = mes;
            ViewBag.Usuario = usuario;
            ViewBag.cod_analista = ((Persona)Session["Session_Login"]).Person_id;            
            ViewBag.t_perfil = t_perfil;
            return View();
        }

        [HttpPost]
        public ActionResult Insertar_Visitas_Person(string modulo_id, string fecha_ingreso)
        {
            /*  return Content(new ContentResult
              {
                  Content = MvcApplication._Serialize(new Insert_Visita_Persona_Response().Inserta_Visita_Modulo(
                      new Insert_Visita_Persona_Request()
                      {
                          person_id = ((Persona)Session["Session_Login"]).Person_id,
                          modulo_id = modulo_id,
                          company_id = ((Persona)Session["Session_Login"]).Company_id.ToString(),
                          fecha_ingreso = fecha_ingreso,
                          visitas = 1,
                          createBy = ((Persona)Session["Session_Login"]).name_user
                      }
                  )),
                  ContentType = "application/json"
              }.Content);*/
            return Json(true);
        }
        [CustomAuthorize]
        public ActionResult FormatosAcceso() {
            return View();
        }
        #endregion

        #region Carga de Ruta
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Ruta() {
            Campania oCampania = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { 
                    campania = Convert.ToString(Request["_a"]) 
                });

            ViewBag.CamCodigo = Request["_a"];
            ViewBag.VisId = Request["_b"];
            ViewBag.CamDescripcion = oCampania.cam_descripcion;

            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult Ruta(int opcion = 0, string parametro = "")
        {
            int FinFila = 0;
            int NumeroRegistros = 0;
            int VisId = Convert.ToInt32(Request["_b"]);
            string NombreArchivo = "";
            string RutaArchivo = "";

            Persona oPersona = ((Persona)Session["Session_Login"]);

            if (opcion == 0)
            {
                #region Carga de ruta
                string CamCodigo = Request["CamCodigo"];
                HttpPostedFileBase FileBaseExcel = Request.Files["CargaArchivo"];//CargaArchivo;
                List<PointOfSalePlanningOperating> lPointOfSalePlanningOperating = new List<PointOfSalePlanningOperating>();
                Campania oCampania = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request()
                {
                    campania = Convert.ToString(Request["_a"])
                });

                NombreArchivo = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now);

                if ((FileBaseExcel == null) || ((FileBaseExcel.ContentLength == 0) && string.IsNullOrEmpty(FileBaseExcel.FileName)))
                {
                    //return View();
                    return RedirectToAction("Ruta", "Administrador", new { _a = Request["_a"], _b = Request["_b"] });
                }

                using (var oPackage = new Excel.ExcelPackage(FileBaseExcel.InputStream))
                {
                    var oSheets = oPackage.Workbook.Worksheets;
                    var oSheet = oSheets["ruta"];

                    FinFila = oSheet.Dimension.End.Row;

                    for (int i = 2; i <= FinFila; i++)
                    {
                        PointOfSalePlanningOperating oPointOfSalePlanningOperating = new PointOfSalePlanningOperating();

                        oPointOfSalePlanningOperating.CamCodigo = CamCodigo;
                        oPointOfSalePlanningOperating.PdvCodigo = Convert.ToString(oSheet.Cells[i, 1].Value);
                        oPointOfSalePlanningOperating.PerId = Convert.ToInt32(oSheet.Cells[i, 2].Value);
                        oPointOfSalePlanningOperating.FechaDesde = Convert.ToDateTime(oSheet.Cells[i, 3].Value);
                        oPointOfSalePlanningOperating.FechaHasta = Convert.ToDateTime(oSheet.Cells[i, 4].Value);
                        oPointOfSalePlanningOperating.IdMposPlanning = 0;
                        oPointOfSalePlanningOperating.UsuId = oPersona.Person_id;
                        oPointOfSalePlanningOperating.UsuUsuario = oPersona.name_user;
                        oPointOfSalePlanningOperating.NombreArchivo = NombreArchivo;

                        lPointOfSalePlanningOperating.Add(oPointOfSalePlanningOperating);
                    }
                }

                ViewBag.NombreArchivo = NombreArchivo;
                ViewBag.CamCodigo = CamCodigo;
                ViewBag.CamDescripcion = oCampania.cam_descripcion;
                ViewBag.VisId = VisId;
                NumeroRegistros = new PointOfSalePlanningOperating().Bulk(lPointOfSalePlanningOperating);

                return View(new PointOfSalePlanningOperating().Lista(
                    new PointOfSalePlanningOperatingParametro()
                    {
                        opcion = 0,
                        parametro = NombreArchivo
                    }));
                #endregion
            }
            else if (opcion == 1)
            {
                #region Elimina registros
                NumeroRegistros = new PointOfSalePlanningOperating().Elimina(
                        new PointOfSalePlanningOperatingParametro() { opcion = opcion, parametro = parametro }
                    );

                return Content(new ContentResult
                {
                    Content = "{ \"cantidad\": " + NumeroRegistros + " }",
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (opcion == 2)
            {
                #region Graba registros
                NumeroRegistros = new PointOfSalePlanningOperating().Graba(
                        new PointOfSalePlanningOperatingParametro() { opcion = opcion, parametro = parametro }
                    );

                return Content(new ContentResult
                {
                    Content = "{ \"cantidad\": " + NumeroRegistros + " }",
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (opcion == 3)
            {
                #region Infomacion de ruta cargada
                FinFila = 2;
                NombreArchivo = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now);
                RutaArchivo = System.IO.Path.Combine(LocalTemp, NombreArchivo + ".xlsx");

                #region Rutas
                List<PointOfSalePlanningOperating> lPointOfSalePlanningOperating = new PointOfSalePlanningOperating().Lista(new PointOfSalePlanningOperatingParametro() { opcion = opcion, parametro = parametro });
                #endregion

                if (lPointOfSalePlanningOperating.Count > 0)
                {
                    #region Inserta informacion a libro
                    FileInfo Archivo = new FileInfo(RutaArchivo);

                    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(Archivo))
                    {
                        Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Ruta");

                        oWs.Cells[1, 1].Value = "Ruta: Id";
                        oWs.Cells[1, 2].Value = "Persona: Id";
                        oWs.Cells[1, 3].Value = "Persona: Usuario";
                        oWs.Cells[1, 4].Value = "P.D.V.: Codigo";
                        oWs.Cells[1, 5].Value = "P.D.V.: Descripción";
                        oWs.Cells[1, 6].Value = "Fecha Desde";
                        oWs.Cells[1, 7].Value = "Fecha Hasta";
                        oWs.Cells[1, 8].Value = "Estado";

                        foreach (PointOfSalePlanningOperating oPointOfSalePlanningOperating in lPointOfSalePlanningOperating)
                        {
                            oWs.Cells[FinFila, 1].Value = oPointOfSalePlanningOperating.IdPosPlanningOperating;
                            oWs.Cells[FinFila, 2].Value = oPointOfSalePlanningOperating.PerId;
                            oWs.Cells[FinFila, 3].Value = oPointOfSalePlanningOperating.PerUsuario;
                            oWs.Cells[FinFila, 4].Value = oPointOfSalePlanningOperating.PdvCodigo;
                            oWs.Cells[FinFila, 5].Value = oPointOfSalePlanningOperating.PdvDescripcion;
                            oWs.Cells[FinFila, 6].Style.Numberformat.Format = "dd/mm/yyyy";
                            oWs.Cells[FinFila, 6].Value = oPointOfSalePlanningOperating.FechaDesde;
                            oWs.Cells[FinFila, 7].Style.Numberformat.Format = "dd/mm/yyyy";
                            oWs.Cells[FinFila, 7].Value = oPointOfSalePlanningOperating.FechaHasta;
                            oWs.Cells[FinFila, 8].Value = oPointOfSalePlanningOperating.PosPlanningOperatingStatus;

                            FinFila++;
                        }

                        oWs.Cells[1, 1, 1, 8].Style.Font.Bold = true;

                        oWs.Column(1).AutoFit();
                        oWs.Column(2).AutoFit();
                        oWs.Column(3).AutoFit();
                        oWs.Column(4).AutoFit();
                        oWs.Column(5).AutoFit();
                        oWs.Column(6).AutoFit();
                        oWs.Column(7).AutoFit();
                        oWs.Column(8).AutoFit();

                        oEx.Save();
                    }
                    #endregion
                }

                return Content(new ContentResult
                {
                    Content = "{ \"Archivo\": \"" + NombreArchivo + ".xlsx\", \"Error\": " + (lPointOfSalePlanningOperating.Count > 0 ? "false" : "true") + " }",
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (opcion == 4)
            {
                #region Carga de ruta
                string CamCodigo = Request["CamCodigo"];
                HttpPostedFileBase FileBaseExcel = Request.Files["CargaArchivo"];
                List<PointOfSalePlanningOperating> lPointOfSalePlanningOperating = new List<PointOfSalePlanningOperating>();
                Campania oCampania = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request()
                {
                    campania = Convert.ToString(Request["_a"])
                });

                NombreArchivo = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now);

                if ((FileBaseExcel == null) || ((FileBaseExcel.ContentLength == 0) && string.IsNullOrEmpty(FileBaseExcel.FileName)))
                {
                    return RedirectToAction("Ruta", "Administrador", new { _a = Request["_a"], _b = Request["_b"] });
                }

                using (var oPackage = new Excel.ExcelPackage(FileBaseExcel.InputStream))
                {
                    var oSheets = oPackage.Workbook.Worksheets;
                    var oSheet = oSheets["ruta"];

                    FinFila = oSheet.Dimension.End.Row;

                    for (int i = 2; i <= FinFila; i++)
                    {
                        PointOfSalePlanningOperating oPointOfSalePlanningOperating = new PointOfSalePlanningOperating();

                        oPointOfSalePlanningOperating.IdPosPlanningOperating = Convert.ToInt32(oSheet.Cells[i, 1].Value);
                        oPointOfSalePlanningOperating.PerId = Convert.ToInt32(oSheet.Cells[i, 2].Value);
                        oPointOfSalePlanningOperating.PerUsuario = Convert.ToString(oSheet.Cells[i, 3].Value);
                        oPointOfSalePlanningOperating.PdvCodigo = Convert.ToString(oSheet.Cells[i, 4].Value);
                        oPointOfSalePlanningOperating.PdvDescripcion = Convert.ToString(oSheet.Cells[i, 5].Value);
                        oPointOfSalePlanningOperating.FechaDesde = Convert.ToDateTime(oSheet.Cells[i, 6].Value);
                        oPointOfSalePlanningOperating.FechaHasta = Convert.ToDateTime(oSheet.Cells[i, 7].Value);
                        oPointOfSalePlanningOperating.PosPlanningOperatingStatus = Convert.ToInt32(oSheet.Cells[i, 8].Value);
                        oPointOfSalePlanningOperating.IdMposPlanning = 0;
                        oPointOfSalePlanningOperating.CamCodigo = CamCodigo;
                        oPointOfSalePlanningOperating.UsuId = oPersona.Person_id;
                        oPointOfSalePlanningOperating.UsuUsuario = oPersona.name_user;
                        oPointOfSalePlanningOperating.NombreArchivo = NombreArchivo;

                        lPointOfSalePlanningOperating.Add(oPointOfSalePlanningOperating);
                    }
                }

                ViewBag.NombreArchivo = NombreArchivo;
                ViewBag.CamCodigo = CamCodigo;
                ViewBag.CamDescripcion = oCampania.cam_descripcion;
                ViewBag.VisId = VisId;
                NumeroRegistros = new PointOfSalePlanningOperating().Bulk(lPointOfSalePlanningOperating);
                ViewBag.ColumnaEliminarEsconde = true;
                ViewBag.Respuesta = new PointOfSalePlanningOperating().Modifica(new PointOfSalePlanningOperatingParametro() { opcion = 4, parametro = NombreArchivo });

                if (NumeroRegistros > 0)
                {
                    return View(lPointOfSalePlanningOperating);
                }
                #endregion
            }

            return View();
        }
        #endregion
    }
}