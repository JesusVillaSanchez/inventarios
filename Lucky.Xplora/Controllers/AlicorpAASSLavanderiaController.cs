﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucky.Xplora.Models;

using Lucky.Xplora.Models.Map;

namespace Lucky.Xplora.Controllers
{
    public class AlicorpAASSLavanderiaController : Controller
    {
        string XploraFoto = Convert.ToString(ConfigurationManager.AppSettings["XploraFoto"]);

        /// <summary>
        /// Autor: dpastor
        /// Fecha: 2017-03-07
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<AlicorpAASSLavanderiaSelecciona> lAnios = new AlicorpAASSLavanderia().Selecciona(new AlicorpAASSLavanderiaParametro() { opcion = 1 });
            List<AlicorpAASSLavanderiaSelecciona> lMes = new AlicorpAASSLavanderia().Selecciona(new AlicorpAASSLavanderiaParametro() { opcion = 2, parametro = (lAnios.Count == 0 ? Convert.ToString(DateTime.Now.Year) : (lAnios.Where(a => a.SelActivo == 1).Select(a => a.SelCodigo).First())) });

            TempData["ListaAnios"] = lAnios;
            TempData["ListaMes"] = lMes;

            ViewBag.XploraFoto = XploraFoto;

            return View();
        }

        [HttpPost]
        public ActionResult Index(string __a)
        {
            AlicorpAASSLavanderiaParametro oParametro = MvcApplication._Deserialize<AlicorpAASSLavanderiaParametro>(__a);

            if (oParametro.opcion == 0)
            {
                return Content(new ContentResult
                    {
                        Content = MvcApplication._Serialize(new AlicorpAASSLavanderia().Lavanderia(oParametro)),
                        ContentType = "application/json"
                    }.Content);
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new AlicorpAASSLavanderia().Selecciona(oParametro)),
                    ContentType = "application/json"
                }.Content);
            }
        }

    }
}
