﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lucky.Xplora.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Unauthorized()
        {
            return View();
        }
        [HttpGet]
        public ActionResult NotFound(string vmsj)
        {
            ViewBag.msj = vmsj;
            return View();
        }   

    }
}
