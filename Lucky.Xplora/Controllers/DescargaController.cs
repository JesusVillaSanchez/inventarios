﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Lucky.Xplora.Models;
using IO = System.IO;
using Lucky.Xplora.Models.Auditoria;

namespace Lucky.Xplora.Controllers
{
    public class DescargaController : Controller
    {
        string LocalFachada = Convert.ToString(ConfigurationManager.AppSettings["LocalFachada"]);

        //
        // GET: /Descarga/
        [HttpGet]
        public ActionResult WFUnacem(string vdoc)
        {
            if (Session["Session_Login"] == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            }
            Persona ObjPer = ((Persona)Session["Session_Login"]);

            //if (ObjPer.Company_id != 1560)
            //{
            //    return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            //}

            string _rt1 = "";
            string _ext = "";
            _ext = vdoc.Substring(vdoc.IndexOf("."), vdoc.Length - vdoc.IndexOf("."));

            switch (_ext)
            {
                case ".jpg":
                    _rt1 = "Img/";
                    break;
                case ".png":
                    _rt1 = "Img/";
                    break;
                case ".doc":
                    _rt1 = "Word/";
                    break;
                case ".pptx":
                    _rt1 = "Ppt/";
                    break;
                case ".pdf":
                    _rt1 = "Pdf/";
                    break;
                case ".ppt":
                    _rt1 = "Ppt/";
                    break;
                case ".xls":
                    _rt1 = "Excel/";
                    break;
                case ".xlsx":
                    _rt1 = "Excel/";
                    break;
            }

            if (System.IO.File.Exists(Path.Combine(LocalFachada, _rt1, vdoc.ToString())))
            {
                Auditoria ObjAudit = new Auditoria();
                bool vestado = ObjAudit.Audit_Descarga(new RequestParametrosAuditoriaUpDownLoad
                {
                    aud_ruta = (Path.Combine(LocalFachada, _rt1, vdoc.ToString())),
                    aud_tipo_carga = 2,
                    aud_usuario = ObjPer.name_user,
                    aud_comentario = "Proyecto WORK FLOW"
                });

                return File(Path.Combine(LocalFachada, _rt1, vdoc.ToString()), System.Net.Mime.MediaTypeNames.Application.Octet, vdoc.ToString());
            }
            else
            {
                return RedirectToAction("NotFound", "Error", new { vmsj = "Archivo no encontrado!!!" });
            }
        }
    }
}