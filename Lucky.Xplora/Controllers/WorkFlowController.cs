﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Unacem;

using IO = System.IO;
using System.Web.Script.Serialization;
using Word = Microsoft.Office.Interop.Word;
using System.IO;
using Lucky.Xplora.Security;
using System.Web.Security;

using System.Globalization;

using System.Drawing;
using System.Drawing.Imaging;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using System.Net;
using System.Web.UI;
using System.Configuration;

namespace Lucky.Xplora.Controllers
{
    public class WorkFlowController : Controller
    {
        string Local = Convert.ToString(ConfigurationManager.AppSettings["Local"]);
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        string LocalFachada = Convert.ToString(ConfigurationManager.AppSettings["LocalFachada"]);

        #region Work Flow
        [CustomAuthorize]
        public ActionResult Index(string _a)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.cod_persona = ((Persona)Session["Session_Login"]).Person_id;

            Persona lstPerson = ((Persona)Session["Session_Login"]);
            ViewBag.nombre_persona = lstPerson.Person_Firtsname + " " + lstPerson.Person_LastName + " " + lstPerson.Person_Surname;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            Session["Session_Campania"] = _a;
            ViewBag.cod_equipo = _a;
            return View();
        }

        public ActionResult GetGrilla(int cod_zona, string fecha_solicitud, string fecha_foto, string distribuidora, int cod_persona, string fecha_presupuesto, string id_planning, int estado_solicitud)
        {
            int vcod_persona;
            vcod_persona = ((Persona)Session["Session_Login"]).Person_id;
            E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
            oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = vcod_persona });
            ViewBag.cod_grupo = oResper.Cod_Grupo;
            ViewBag.nombre_grupo = oResper.Grupo;
            return View(new WorkFlow().Consulta_Solicitud(new Request_WF_General
            {
                Cod_Grupo = oResper.Cod_Grupo,
                cod_zona = cod_zona,
                fecha_solicitud = fecha_solicitud,
                fecha_foto = fecha_foto,
                distribuidora = distribuidora,
                Cod_Persona = cod_persona,
                fecha_presupuesto = fecha_presupuesto,
                cod_equipo = id_planning,
                Estado = estado_solicitud
            }));

        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 28-06-2016
        /// </summary>
        /// <param name="cod_zona"></param>
        /// <param name="fecha_solicitud"></param>
        /// <param name="fecha_foto"></param>
        /// <param name="distribuidora"></param>
        /// <param name="cod_persona"></param>
        /// <returns></returns>
        public ActionResult GetPresupuestoGrilla(int cod_zona, string fecha_solicitud, string fecha_foto, string distribuidora, int cod_persona, string fecha_presupuesto, int estado_solicitud)
        {
            int vcod_persona;
            vcod_persona = ((Persona)Session["Session_Login"]).Person_id;
            E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
            oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = vcod_persona });

            List<E_Fachada_Unacem> oLsWf = new WorkFlow().Consulta_Solicitud(
                new Request_WF_General()
                {
                    Cod_Grupo = oResper.Cod_Grupo,
                    cod_zona = cod_zona,
                    fecha_solicitud = fecha_solicitud,
                    fecha_foto = fecha_foto,
                    distribuidora = distribuidora,
                    Cod_Persona = cod_persona,
                    fecha_presupuesto = fecha_presupuesto,
                    cod_equipo = Convert.ToString(Session["Session_Campania"]),
                    Estado = estado_solicitud
                });

            return Json(new { Archivo = oLsWf });
            //return Content(new ContentResult
            //{
            //    Content = MvcApplication._Serialize(
            //        new WorkFlow().Consulta_Solicitud(new Request_WF_General { Cod_Grupo = oResper.Cod_Grupo, cod_zona = cod_zona, fecha_solicitud = fecha_solicitud, fecha_foto = fecha_foto, distribuidora = distribuidora, Cod_Persona = cod_persona })   
            //    ),
            //    ContentType = "application/json"
            //}.Content);
        }

        public ActionResult AddSolicitud(int vids, string vpdv, string vruc, string vcodigo, string vdireccion, string vdistrito, string vprovincia, string vdepartamento, string vcontacto, string vnumtelef, string vnumcel,
            string vdistribuidora, string vfoto, string vven_prom_soles, string vven_prom_ton, string vlinea_credito, string vdeu_actual, string vop_grabado, int vop_opera, int p1_codperson, string p1_codTipo, int p1_codZona, string p1_comentarios)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Agregar_solicitud(
                    new Request_FW_add_solicitud()
                    {
                        cod_equipo = Convert.ToString(Session["Session_Campania"]),
                        ids = vids,
                        pdv = vpdv,
                        ruc = vruc,
                        codigo = vcodigo,
                        direccion = vdireccion,
                        distrito = vdistrito,
                        provincia = vprovincia,
                        departamento = vdepartamento,
                        contacto = vcontacto,
                        numtelef = vnumtelef,
                        numcel = vnumcel,
                        distribuidora = vdistribuidora,
                        foto = vfoto,
                        ven_prom_soles = vven_prom_soles,
                        ven_prom_ton = vven_prom_ton,
                        linea_credito = vlinea_credito,
                        deu_actual = vdeu_actual,
                        op_grabado = vop_grabado,
                        op_opera = vop_opera,
                        p1_codperson = p1_codperson,
                        p1_codTipo = p1_codTipo,
                        p1_codZona = p1_codZona,
                        p1_comentarios = p1_comentarios
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult AproSolicitudp2(int vnumero, int vestado, string vcomentario)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().P2_Aprobacion_Solicitud_Trade(
                    new Request_WF_General()
                    {
                        Num_Solicitud = vnumero,
                        Estado = vestado,
                        Comen_Apro = vcomentario
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult GetEditGrantt()
        {
            if (Request["_a"] != null)
            {
                ViewBag.cod_equipo = Convert.ToString(Request["_a"]);
                ViewBag.cod_reg_tabla = Convert.ToString(Request["_b"]);
                ViewBag.cod_op = Convert.ToString(Request["_c"]);// 1 fotomontaje - 2 implementacion
                ViewBag.cod_area = Convert.ToString(Request["_d"]);// codigo de area
                ViewBag.cod_persona = Convert.ToString(Request["_e"]);// codigo de persona
            }
            return View();
        }
        [HttpPost]
        public ActionResult JsonLlenarGantt(string __a, int __b, int __c, int __d, string __e, int __f)
        {
            //  Word_Insertar_Img();
            //string vcarta = Word_Generar();
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().LlenarGantt(
                    new Request_WF_General()
                    {
                        Cod_Equipo = __a,
                        Id_Reg = __b,
                        Cod_Op = __c,
                        Cod_Area = __d,
                        Fecha = __e,
                        Cod_Persona = __f
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public JsonResult Guardar_DetalleGantt(string DataItems, string cod_equipo, int cod_reg_tb, int cod_opcion, int cod_p)
        {
            List<E_Nw_Detail_Gantt> oListDetailGantt = new List<E_Nw_Detail_Gantt>();

            var jss = new System.Web.Script.Serialization.JavaScriptSerializer();
            dynamic Items = jss.Deserialize<dynamic>(DataItems);
            var vidgantt = "";
            foreach (var item in Items["tasks"])
            {
                E_Nw_Detail_Gantt oDetailGantt = new E_Nw_Detail_Gantt();
                vidgantt += Convert.ToString(item["id"]) + ",";
                oDetailGantt.id = Convert.ToString(item["id"]);
                oDetailGantt.name = item["name"];
                oDetailGantt.code = item["code"];
                oDetailGantt.level = Convert.ToInt32(item["level"]);
                oDetailGantt.status = item["status"];
                oDetailGantt.canWrite = Convert.ToBoolean(item["canWrite"]);
                oDetailGantt.start = Convert.ToString(item["start"]);
                oDetailGantt.duration = Convert.ToInt32(item["duration"]);
                oDetailGantt.gend = Convert.ToString(item["end"]);
                oDetailGantt.startIsMilestone = Convert.ToBoolean(item["startIsMilestone"]);
                oDetailGantt.endIsMilestone = Convert.ToBoolean(item["endIsMilestone"]);
                if (item.ContainsKey("collapsed"))
                {
                    oDetailGantt.collapsed = Convert.ToBoolean(item["collapsed"]);
                }
                else
                {
                    oDetailGantt.collapsed = false;
                }

                oDetailGantt.assigs = null;
                oDetailGantt.hasChild = Convert.ToBoolean(item["hasChild"]);
                oDetailGantt.progress = Convert.ToInt32(item["progress"]);
                oDetailGantt.description = Convert.ToString(item["description"]);
                oDetailGantt.depends = Convert.ToString(item["depends"]);
                oDetailGantt.Promotion_CreateBy = Convert.ToString(cod_p);//((Persona)Session["Session_Login"]).name_user.ToString();
                oListDetailGantt.Add(oDetailGantt);
            }
            vidgantt = vidgantt.Substring(0, vidgantt.Length - 1);

            E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
            if (oListDetailGantt.Count > 0)
            {
                StatusRegistro = new WorkFlow().Registrar_DetailGantt(
                   new Request_WF_DetailGantt()
                   {
                       oDetailGantt = oListDetailGantt,
                       Cod_Equipo = cod_equipo,
                       Cod_Reg_Tabla = cod_reg_tb,
                       strlist_promocion = vidgantt,
                       Cod_Opcion = cod_opcion
                   });
            }
            else
            {
                StatusRegistro.CodStatus = E_Nw_DetailGanttRegistro.GENERAL_ERROR;
                StatusRegistro.Mensaje = "No ha digitado ningún dato, Columna Exhibición es Obligatorio.";
            }
            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
            //return Json("");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Carga_Archivo(HttpPostedFileBase __f, string _b)
        {
            string _archivo = "";
            string _fileLocal = "";
            string _doc_url = "";

            if (__f != null)
            {
                _fileLocal = IO.Path.GetFileName(__f.FileName);
                _archivo = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                string vtipoarchi = _fileLocal.Substring(_fileLocal.LastIndexOf("."), 4).ToLower();

                switch (vtipoarchi)
                {
                    case ".xls":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(Path.Combine(LocalFachada, "Excel", "Excel_" + _archivo + ".xlsx"));
                            _doc_url = "/Fachada/Excel" + "/Excel_" + _archivo + ".xlsx";
                        }

                        break;
                    case ".pdf":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(Path.Combine(LocalFachada, "Pdf", "PDF_" + _archivo + ".pdf"));
                            _doc_url = "/Fachada/Pdf" + "/PDF_" + _archivo + ".pdf";
                        }
                        break;
                    case ".jpg":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".jpg"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".jpg";
                        break;
                    case ".png":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".png"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".png";
                        break;
                    case ".pptx":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(IO.Path.Combine(LocalFachada, "Ppt", "PPTX_" + _archivo + ".pptx"));
                            _doc_url = "/Fachada/Ppt" + "/PPTX_" + _archivo + ".pptx";
                        }
                        break;
                    case ".ppt":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(IO.Path.Combine(LocalFachada, "Ppt", "PPT_" + _archivo + ".ppt"));
                            _doc_url = "/Fachada/Ppt" + "/PPT_" + _archivo + ".ppt";
                        }
                        break;
                }
            }

            var headerAcceptVariable1 = this.Request.Headers["ACCEPT"] as string;

            if (headerAcceptVariable1 != null && headerAcceptVariable1.Contains("application/json"))
            {
                return Json(_doc_url);
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var response = serializer.Serialize(_doc_url);
                return Content(response);
            }
        }


        [HttpPost]
        public ActionResult Carga_Img_FotoMon(HttpPostedFileBase __f, int __a, int __b)
        {
            string _archivo = "";
            string _fileLocal = "";
            string _doc_url = "";

            if (__f != null)
            {
                _fileLocal = IO.Path.GetFileName(__f.FileName);
                _archivo = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                string vtipoarchi = _fileLocal.Substring(_fileLocal.LastIndexOf("."), 4);

                switch (vtipoarchi)
                {
                    case ".jpg":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".jpg"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".jpg";
                        break;
                    case ".png":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".png"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".png";
                        break;
                }
            }

            WorkFlow objService = new WorkFlow();
            E_WF_Fotomontaje objresponse = objService.AddUpt_Fotomontaje(new Request_WF_General
            {
                Id_Reg = __a,
                img = _doc_url,
                orden = __b,
                Cod_Op = 1
            });
            var headerAcceptVariable1 = this.Request.Headers["ACCEPT"] as string;

            if (headerAcceptVariable1 != null && headerAcceptVariable1.Contains("application/json"))
            {
                return Json(_doc_url);
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var response = serializer.Serialize(_doc_url);
                return Content(response);
            }
        }

        public ActionResult Eliminar_Img_FotoMon(int __a, int __b, string __c)
        {
            WorkFlow objService = new WorkFlow();
            E_WF_Fotomontaje objresponse = objService.AddUpt_Fotomontaje(new Request_WF_General { Id_Reg = __a, img = "", orden = __b, Cod_Op = 2 });

            string sms = "0";
            if (objresponse.Cod_Reg_Table == 0)
            {
                sms = "1";
                //if (System.IO.File.Exists(IO.Path.Combine(Server.MapPath("~" + __c))))
                if (System.IO.File.Exists(Path.Combine(LocalTemp, __c)))
                {
                    try
                    {
                        //System.IO.File.Delete(IO.Path.Combine(Server.MapPath("~" + __c)));
                        System.IO.File.Delete(IO.Path.Combine(LocalTemp, __c));
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            return Json(sms);
        }
        
        [HttpPost]
        public JsonResult Generar_Documento(string __a, string __b, string __c, string __d, string __e, int __f, string __g)
        {

            string vdocumento = Word_Generar();

            //if (__b == null || __b == "") { __b="/Temp/Fachada/Img/blanco.jpg"; }
            if (__b == null || __b == "") { __b = "/Fachada/Img/blanco.jpg"; }
            if (__c == null || __c == "") { __c = "/Fachada/Img/blanco.jpg"; }
            if (__d == null || __d == "") { __d = "/Fachada/Img/blanco.jpg"; }
            if (__e == null || __e == "") { __e = "/Fachada/Img/blanco.jpg"; }


            string vresp = Word_Insertar_Img(vdocumento, __b, __c, __d, __e, __a);
            if (vdocumento == null || vdocumento == "" || vdocumento == " ") { vdocumento = "0"; }
            else
            {
                vdocumento = vresp;
                WorkFlow objService = new WorkFlow();
                string objresponse = objService.AddFotomont_PrePresupuesto(new Request_WF_General { Id_Reg = __f, Comen_Apro = vdocumento, Cod_Op = 1, Coment_Fotomontaje = __g });
            }
            return Json(vdocumento, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Consultar_Fotomontaje(int __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Consul_FotoMontaje(
                    new Request_WF_General() { Id_Reg = __a }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_PrePresupuesto(int __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AddFotomont_PrePresupuesto(
                    new Request_WF_General() { Id_Reg = __a, Comen_Apro = __b, Cod_Op = 2 }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Asociacion_PrePresupuesto(string __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AsociacionPdv_PrePresupuesto(
                    new Request_WF_General() { str_codigo_Presupuesto = __a, Comen_Apro = __b }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Eliminar_SolicitudVentas(int __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Remove_SolicitudVentas(
                    new Request_WF_General() { Num_Solicitud = __a }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 27/06/2016
        /// Descripcion : <>
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        public ActionResult RegDetalle_PrePresupuesto(int __a, string __b, string __c, string __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AddFotomont_PrePresupuesto(
                    new Request_WF_General() { Id_Reg = __a, Monto_Presupuesto = __b, Codigo_Presupuesto = __c, Cod_Op = 3, str_codigo_Presupuesto = __d }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult RegDetalle_PrePresupuesto_Final(int __a, string __b, string __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AddPresupuesto_Final(
                    new Request_WF_General() { Id_Reg = __a, Monto_Presupuesto_Final = __b, Codigo_Presupuesto_Final = __c, Cod_Op = 4 }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso4_Ventas(int __a, int __b, string __c, int __d)
        {
            if ((__d == 2 && __b == 2) || (__d == 3 && __b == 2))
            {
                if (System.IO.File.Exists(Path.Combine(LocalFachada, __c)))
                {
                    try
                    {
                        System.IO.File.Delete(Path.Combine(LocalFachada, __c));
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso4_Ventas(
                    new Request_WF_General() { Id_Reg = __a, Estado = __b, Comen_Apro = __c, Cod_Op = __d }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso5_Trade(int __a, string __b, string __c, string __d, int __e, int __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso5_Trade(
                    new Request_WF_General() { Id_Reg = __a, Ord_Pago = __b, Hes = __c, Ord_Comentario = __d, Estado = __e, Cod_Op = __f }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso6_LuckyAnalis(int __a, string __b, string __c, int __d, int __e)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso6_LuckyAnalista(
                    new Request_WF_General() { Id_Reg = __a, Documento = __b, Fecha = __c, orden = __d, Cod_Op = __e }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso7_LuckyTramite(int __a, string __b, string __c, string __d, string __e, string __f, string __g, int __h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso7_LuckyTramite(
                    new Request_WF_General()
                    {
                        Id_Reg = __a
                        ,
                        fec_ing_expe = __b
                        ,
                        ruta_recibo = __c
                        ,
                        obs_recibo = __d
                        ,
                        fec_licencia = __e
                        ,
                        fec_vencimiento = __f
                        ,
                        ruta_licencia = __g
                        ,
                        Cod_Op = __h
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso7_LuckyAnalista(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso7_LuckyAnalista(
                    new Request_WF_General()
                    {
                        Id_Reg = __a,
                        Documento = __b,
                        Cod_Op = __c
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }
        public ActionResult Reg_Paso8_Trade(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso8_Trade(
                    new Request_WF_General()
                    {
                        Id_Reg = __a,
                        Documento = __b,
                        Cod_Op = __c
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult VMModal(int __a)
        {
            ViewBag.cod_grupo = __a;
            // Modal de visita y mantenimiento
            return View();
        }

        public ActionResult Reg_Vista(int __a, string __b, string __c, int __d, int __e, int __f, int __g)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Visita(
                    new Request_WF_General()
                    {
                        Id_Reg = __a
                        ,
                        fec_ing_expe = __b
                        ,
                        img = __c
                        ,
                        Estado = __d
                        ,
                        Cod_Grupo = __e
                        ,
                        orden = __f
                        ,
                        Cod_Op = __g
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult GridMan(int __a, int __b, string __c)
        {
            ViewBag.cod_grupo = __a;
            ViewBag.nom_pdv = __c;
            return View(new WorkFlow().Consulta_Mantenimiento(new Request_WF_General { Cod_Grupo = __a, Id_Reg = __b }));
        }

        public ActionResult RegNewMantenimiento(int __a, string __b, string __c, string __d, string __e, string __f, int __g, int __h, int __i)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Mantenimiento_Nuevo(
                    new Request_FW_add_Mantenimiento()
                    {
                        Id_Reg = __a
                       ,
                        Cod_Detalle = __i
                       ,
                        foto = __b
                       ,
                        ven_prom_soles = __c
                       ,
                        ven_prom_ton = __d
                       ,
                        linea_credito = __e
                       ,
                        deu_actual = __f
                       ,
                        Estado = __g
                       ,
                        Cod_Op = __h
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult RegMatenimientoPasos(int __a, int __b, string __c, string __d, string __e, string __f, int __g, int __h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Mantenimiento_Pasos(
                    new Request_WF_General()
                    {
                        Id_Reg = __a
                        ,
                        Estado = __b
                        ,
                        Comen_Apro = __c
                        ,
                        Documento = __d
                        ,
                        Ord_Pago = __e
                        ,
                        Hes = __f
                        ,
                        Cod_Grupo = __g
                        ,
                        Cod_Op = __h
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 06/09/2016
        /// Descripcion: Descarga Excel
        /// </summary>
        /// <param name="nomdoc"></param>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        public ActionResult Reg_Paso6_lucky(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso6_lucky(
                    new Request_WF_General() { Cod_Op = __a, comen_expediente = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }
        public ActionResult Reg_Unacem(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Unacem(
                    new Request_WF_General() { Cod_Op = __a, chk_fila = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }
        public JsonResult WFExcel(int __a, string __b, string __c, string __d, int __e, string __f, int __g)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            //_fileServer = String.Format("WF_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            //_filePath = System.IO.Path.Combine(Server.MapPath("/Temp"), _fileServer);
            _fileServer = String.Format("WF_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte WorkFlow >>

                int vcod_persona;
                vcod_persona = ((Persona)Session["Session_Login"]).Person_id;
                E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
                oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = vcod_persona });

                List<E_Fachada_Unacem> oWfUna = new WorkFlow().Consulta_Solicitud(
                   new Request_WF_General()
                   {
                       Cod_Grupo = oResper.Cod_Grupo,
                       cod_zona = __a,
                       fecha_solicitud = __b,
                       fecha_foto = __c,
                       distribuidora = __d,
                       Cod_Persona = __e,
                       fecha_presupuesto = __f,
                       cod_equipo = Convert.ToString(Session["Session_Campania"]),
                       Estado = __g
                   });

                #endregion

                #endregion


                #region <<< Reporte WorkFlow >>>

                if ((oWfUna != null))
                {
                    int columnsuser1 = 29;
                    int columnsuser2 = 35;
                    int columnsuser3 = 33;
                    int columnsuser4 = 13;
                    if (oResper.Cod_Grupo == 1)
                    {
                        #region Perfil 1

                        Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Ventas");

                        //Combinacion de celdas
                        oWsUna.Cells["A1:E1"].Merge = true;
                        oWsUna.Cells["F1:H1"].Merge = true;
                        oWsUna.Cells["I1:K1"].Merge = true;
                        oWsUna.Cells["L1:O1"].Merge = true;
                        oWsUna.Cells["P1:Q1"].Merge = true;
                        oWsUna.Cells["R1:Z1"].Merge = true;
                        oWsUna.Cells["AA1:AB1"].Merge = true;

                        //Dando valor a celdas combinadas
                        oWsUna.Cells["A1:E1"].Value = "VENTAS";
                        oWsUna.Cells["F1:H1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["I1:K1"].Value = "LUCKY";
                        oWsUna.Cells["L1:O1"].Value = "VENTAS";
                        oWsUna.Cells["P1:Q1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["R1:Z1"].Value = "LUCKY";
                        oWsUna.Cells["AA1:AB1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["AC1"].Value = "LUCKY";

                        //Dando valor a celdas individuales
                        oWsUna.Cells["A2"].Value = "Usuario de Solicitud";
                        oWsUna.Cells["B2"].Value = "Fecha de Solicitud";
                        oWsUna.Cells["C2"].Value = "Punto de Venta";
                        oWsUna.Cells["D2"].Value = "Direccion";
                        oWsUna.Cells["E2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["F2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["G2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["H2"].Value = "Comentario";
                        oWsUna.Cells["I2"].Value = "Fecha de Visita";
                        oWsUna.Cells["J2"].Value = "Comentario";
                        oWsUna.Cells["K2"].Value = "Fotomontaje";
                        oWsUna.Cells["L2"].Value = "Aprobacion de Fotomontaje";
                        oWsUna.Cells["M2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["N2"].Value = "Comentario";
                        oWsUna.Cells["O2"].Value = "Carta de Aprobacion";
                        oWsUna.Cells["P2"].Value = "Aprobacion de Pre-Presupuesto";
                        oWsUna.Cells["Q2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["R2"].Value = "Fecha de implementacion";
                        oWsUna.Cells["S2"].Value = "Programacion y Status";
                        oWsUna.Cells["T2"].Value = "Fecha de Ingreso de Expediente";
                        oWsUna.Cells["U2"].Value = "Recibo de Tramite Municipal";
                        oWsUna.Cells["V2"].Value = "Observacion";
                        oWsUna.Cells["W2"].Value = "Fecha de Entrega de Licencia";
                        oWsUna.Cells["X2"].Value = "Fecha de Caducidad de Licencia";
                        oWsUna.Cells["Y2"].Value = "Licencia";
                        oWsUna.Cells["Z2"].Value = "Reporte Fotografico";
                        oWsUna.Cells["AA2"].Value = "Validacion de Fachada";
                        oWsUna.Cells["AB2"].Value = "Fecha de Validacion";
                        oWsUna.Cells["AC2"].Value = "Visita / Mantenimiento";


                        _fila = 3;

                        foreach (E_Fachada_Unacem oBj in oWfUna)
                        {
                            oWsUna.Cells[_fila, 1].Value = oBj.P1_Usuario_Solicita;
                            oWsUna.Cells[_fila, 2].Value = oBj.Fec_Solicitud;
                            oWsUna.Cells[_fila, 3].Value = oBj.Nom_PDV;
                            oWsUna.Cells[_fila, 4].Value = oBj.Direccion;
                            oWsUna.Cells[_fila, 5].Value = oBj.Est_Solicitud;
                            switch (oBj.Apro_Solicitud.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 6].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 6].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 7].Value = oBj.Fec_Apro_Solicitud;
                            oWsUna.Cells[_fila, 8].Value = oBj.Comen_Apro_Solicitud;
                            oWsUna.Cells[_fila, 9].Value = oBj.P2_Fecha_Entrega_Foto;
                            oWsUna.Cells[_fila, 10].Value = oBj.comen_expediente;
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                oWsUna.Cells[_fila, 11].Value = "Con foto";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 11].Value = "";
                            }
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                switch (oBj.Apro_Fotomontaje.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 12].Value = "Aprobado";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 12].Value = "Desaprobado";
                                        break;
                                }
                            }
                            oWsUna.Cells[_fila, 13].Value = oBj.Fec_Apro_Fotomonataje;
                            oWsUna.Cells[_fila, 14].Value = oBj.Comen_Apro_Fotomontaje;
                            if (oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje.Length > 0)
                            {
                                if (oBj.Url_Carta_Fotomontaje != null && oBj.Url_Carta_Fotomontaje.Replace(" ", "").Length > 0)
                                {
                                    oWsUna.Cells[_fila, 15].Value = "Con carta";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 15].Value = "Falta cargar carta";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 15].Value = "";
                            }
                            switch (oBj.Apro_pre_Presu.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 16].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 16].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 17].Value = oBj.Fec_Apro_pre_Presu;
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 18].Value = oBj.fecha_implementacion;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 18].Value = "";
                            }
                            string _vcolor = "";
                            if (oBj.Cantidad == 0)
                            {
                                _vcolor = "Pendientes";
                            }
                            else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                            {
                                _vcolor = "Algunos pendientes";
                            }
                            else if (oBj.Cantidad == 10)
                            {
                                _vcolor = "Revisado";
                            }
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 19].Value = _vcolor;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 19].Value = _vcolor;
                            }
                            if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                            {
                                oWsUna.Cells[_fila, 20].Value = oBj.Fec_ingre_expe;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 21].Value = "Con recibo";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 21].Value = "";
                                }
                                oWsUna.Cells[_fila, 22].Value = oBj.Obs_Tramite;
                                oWsUna.Cells[_fila, 23].Value = oBj.Fec_Entre_Licencia;
                                oWsUna.Cells[_fila, 24].Value = oBj.Fec_Cadu_Licencia;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 25].Value = "Con Licencia";

                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 25].Value = "";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 20].Value = "";
                                oWsUna.Cells[_fila, 21].Value = "";
                                oWsUna.Cells[_fila, 22].Value = "";
                                oWsUna.Cells[_fila, 23].Value = "";
                                oWsUna.Cells[_fila, 24].Value = "";
                                oWsUna.Cells[_fila, 25].Value = "";
                            }
                            if (oBj.Url_Report_Fotografico != null && oBj.Url_Report_Fotografico != "")
                            {
                                oWsUna.Cells[_fila, 26].Value = "Con reporte fotografico";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 26].Value = "";
                            }

                            switch (oBj.Vali_Fechada_Imple.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 27].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 27].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 28].Value = oBj.Fec_Vali_Fachada_Imple;
                            string _vmante = " ";
                            switch (oBj.Vali_Fechada_Imple.ToUpper())
                            {
                                case "TRUE":
                                    _vmante = "Con mantenimiento";
                                    break;
                            }
                            oWsUna.Cells[_fila, 29].Value = _vmante;



                            for (int i = 1; i <= columnsuser1; i++)
                            {
                                oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                //ajustando texto
                                oWsUna.Cells[_fila, i].Style.WrapText = true;
                            }



                            _fila++;
                        }

                        //Formato Cabecera 1
                        oWsUna.Row(1).Height = 25;
                        oWsUna.Row(1).Style.Font.Bold = true;
                        oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                        oWsUna.Cells["A1:AC1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A1:AC1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                        //Formato Cabecera 2
                        oWsUna.Row(2).Height = 20;
                        oWsUna.Row(2).Style.Font.Bold = true;
                        oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                        oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                        oWsUna.Cells["A2:AC2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A2:AC2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                        oWsUna.SelectedRange["A2:AC2"].AutoFilter = true;


                        //Formato ambas cabeceras
                        oWsUna.Cells["A1:AC2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AC2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AC2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AC2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;




                        //Formato por columnas
                        //Ventas
                        oWsUna.Column(1).Width = 21;
                        oWsUna.Column(2).Width = 19;
                        oWsUna.Column(3).Width = 30;
                        oWsUna.Column(4).Width = 35;
                        oWsUna.Column(5).Width = 20;
                        //trade marketing
                        oWsUna.Column(6).Width = 20;
                        oWsUna.Column(7).Width = 22;
                        oWsUna.Column(8).Width = 35;
                        //lucky
                        oWsUna.Column(9).Width = 22;
                        oWsUna.Column(10).Width = 35;
                        oWsUna.Column(11).Width = 13;
                        //ventas
                        oWsUna.Column(12).Width = 28;
                        oWsUna.Column(13).Width = 22;
                        oWsUna.Column(14).Width = 35;
                        oWsUna.Column(15).Width = 21;
                        //trade marketing
                        oWsUna.Column(16).Width = 32;
                        oWsUna.Column(17).Width = 22;
                        //lucky
                        oWsUna.Column(18).Width = 26;
                        oWsUna.Column(19).Width = 23;
                        oWsUna.Column(20).Width = 32;
                        oWsUna.Column(21).Width = 29;
                        oWsUna.Column(22).Width = 35;
                        oWsUna.Column(23).Width = 29;
                        oWsUna.Column(24).Width = 31;
                        oWsUna.Column(25).Width = 10;
                        oWsUna.Column(26).Width = 21;
                        //trade marketing
                        oWsUna.Column(27).Width = 23;
                        oWsUna.Column(28).Width = 21;
                        //lucky
                        oWsUna.Column(29).Width = 24;

                        //Columnas auto ajustadas
                        /*oWsUna.Column(1).AutoFit();*/

                        oWsUna.View.FreezePanes(3, 5);


                        oEx.Save();

                        #endregion
                    }
                    if (oResper.Cod_Grupo == 2)
                    {
                        #region Perfil 2

                        Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Trade");

                        //Combinacion de celdas
                        oWsUna.Cells["A1:E1"].Merge = true;
                        oWsUna.Cells["F1:H1"].Merge = true;
                        oWsUna.Cells["I1:M1"].Merge = true;
                        oWsUna.Cells["N1:Q1"].Merge = true;
                        oWsUna.Cells["R1:V1"].Merge = true;
                        oWsUna.Cells["W1:AF1"].Merge = true;
                        oWsUna.Cells["AG1:AH1"].Merge = true;

                        //Dando valor a celdas combinadas
                        oWsUna.Cells["A1:E1"].Value = "VENTAS";
                        oWsUna.Cells["F1:H1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["I1:M1"].Value = "LUCKY";
                        oWsUna.Cells["N1:Q1"].Value = "VENTAS";
                        oWsUna.Cells["R1:V1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["W1:AF1"].Value = "LUCKY";
                        oWsUna.Cells["AG1:AH1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["AI1"].Value = "LUCKY";

                        //Dando valor a celdas individuales
                        oWsUna.Cells["A2"].Value = "Usuario de Solicitud";
                        oWsUna.Cells["B2"].Value = "Fecha de Solicitud";
                        oWsUna.Cells["C2"].Value = "Punto de Venta";
                        oWsUna.Cells["D2"].Value = "Direccion";
                        oWsUna.Cells["E2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["F2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["G2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["H2"].Value = "Comentario";
                        oWsUna.Cells["I2"].Value = "Fecha de Visita";
                        oWsUna.Cells["J2"].Value = "Comentario";
                        oWsUna.Cells["K2"].Value = "Fotomontaje";
                        oWsUna.Cells["L2"].Value = "Monto (S/.)";
                        oWsUna.Cells["M2"].Value = "Pre-Presupuesto";
                        oWsUna.Cells["N2"].Value = "Aprobacion de Fotomontaje";
                        oWsUna.Cells["O2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["P2"].Value = "Comentario";
                        oWsUna.Cells["Q2"].Value = "Carta de Aprobacion";
                        oWsUna.Cells["R2"].Value = "Aprobacion de Pre-Presupuesto";
                        oWsUna.Cells["S2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["T2"].Value = "Orden de Compra";
                        oWsUna.Cells["U2"].Value = "H.E.S.";
                        oWsUna.Cells["V2"].Value = "Comentario";
                        oWsUna.Cells["W2"].Value = "Fecha de implementacion";
                        oWsUna.Cells["X2"].Value = "Programacion y Status";
                        oWsUna.Cells["Y2"].Value = "Fecha de Ingreso de Expediente";
                        oWsUna.Cells["Z2"].Value = "Recibo de Tramite Municipal";
                        oWsUna.Cells["AA2"].Value = "Observacion";
                        oWsUna.Cells["AB2"].Value = "Fecha de Entrega de Licencia";
                        oWsUna.Cells["AC2"].Value = "Fecha de Caducidad de Licencia";
                        oWsUna.Cells["AD2"].Value = "Licencia";
                        oWsUna.Cells["AE2"].Value = "Reporte Fotografico";
                        oWsUna.Cells["AF2"].Value = "Presupuesto Final";
                        oWsUna.Cells["AG2"].Value = "Validacion de Fachada";
                        oWsUna.Cells["AH2"].Value = "Fecha de Validacion";
                        oWsUna.Cells["AI2"].Value = "Visita / Mantenimiento";


                        _fila = 3;

                        foreach (E_Fachada_Unacem oBj in oWfUna)
                        {
                            oWsUna.Cells[_fila, 1].Value = oBj.P1_Usuario_Solicita;
                            oWsUna.Cells[_fila, 2].Value = oBj.Fec_Solicitud;
                            oWsUna.Cells[_fila, 3].Value = oBj.Nom_PDV;
                            oWsUna.Cells[_fila, 4].Value = oBj.Direccion;
                            oWsUna.Cells[_fila, 5].Value = oBj.Est_Solicitud;
                            switch (oBj.Apro_Solicitud.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 6].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 6].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 7].Value = oBj.Fec_Apro_Solicitud;
                            oWsUna.Cells[_fila, 8].Value = oBj.Comen_Apro_Solicitud;
                            oWsUna.Cells[_fila, 9].Value = oBj.P2_Fecha_Entrega_Foto;
                            oWsUna.Cells[_fila, 10].Value = oBj.comen_expediente;
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                oWsUna.Cells[_fila, 11].Value = "Con foto";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 11].Value = "";
                            }
                            oWsUna.Cells[_fila, 12].Value = oBj.p3_monto_presupuesto;
                            if (oBj.Url_Presupuesto != "" && oBj.Url_Presupuesto != null)
                            {
                                oWsUna.Cells[_fila, 13].Value = "Con presupuesto";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 13].Value = "";
                            }
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                switch (oBj.Apro_Fotomontaje.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 14].Value = "Aprobado";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 14].Value = "Desaprobado";
                                        break;
                                }
                            }
                            oWsUna.Cells[_fila, 15].Value = oBj.Fec_Apro_Fotomonataje;
                            oWsUna.Cells[_fila, 16].Value = oBj.Comen_Apro_Fotomontaje;
                            if (oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje.Length > 0)
                            {
                                if (oBj.Url_Carta_Fotomontaje != null && oBj.Url_Carta_Fotomontaje.Replace(" ", "").Length > 0)
                                {
                                    oWsUna.Cells[_fila, 17].Value = "Con carta";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 17].Value = "Falta cargar carta";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 17].Value = "";
                            }
                            switch (oBj.Apro_pre_Presu.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 18].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 18].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 19].Value = oBj.Fec_Apro_pre_Presu;
                            oWsUna.Cells[_fila, 20].Value = oBj.Orden_Compra;
                            oWsUna.Cells[_fila, 21].Value = oBj.HES;
                            oWsUna.Cells[_fila, 22].Value = oBj.Comen_Apro_Pre_Presu;
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 23].Value = oBj.fecha_implementacion;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 23].Value = "";
                            }
                            string _vcolor = "";
                            if (oBj.Cantidad == 0)
                            {
                                _vcolor = "Pendientes";
                            }
                            else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                            {
                                _vcolor = "Algunos pendientes";
                            }
                            else if (oBj.Cantidad == 10)
                            {
                                _vcolor = "Revisado";
                            }
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 24].Value = _vcolor;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 24].Value = _vcolor;
                            }
                            if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                            {
                                oWsUna.Cells[_fila, 25].Value = oBj.Fec_ingre_expe;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 26].Value = "Con recibo";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 26].Value = "";
                                }
                                oWsUna.Cells[_fila, 27].Value = oBj.Obs_Tramite;
                                oWsUna.Cells[_fila, 28].Value = oBj.Fec_Entre_Licencia;
                                oWsUna.Cells[_fila, 29].Value = oBj.Fec_Cadu_Licencia;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 30].Value = "Con Licencia";

                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 30].Value = "";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 25].Value = "";
                                oWsUna.Cells[_fila, 26].Value = "";
                                oWsUna.Cells[_fila, 27].Value = "";
                                oWsUna.Cells[_fila, 28].Value = "";
                                oWsUna.Cells[_fila, 29].Value = "";
                                oWsUna.Cells[_fila, 30].Value = "";
                            }
                            if (oBj.Url_Report_Fotografico != null && oBj.Url_Report_Fotografico != "")
                            {
                                oWsUna.Cells[_fila, 31].Value = "Con reporte fotografico";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 31].Value = "";
                            }
                            if (oBj.Url_Presu_Final != null && oBj.Url_Presu_Final != "")
                            {
                                oWsUna.Cells[_fila, 32].Value = "Con presupuesto final";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 32].Value = "";
                            }
                            switch (oBj.Vali_Fechada_Imple.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 33].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 33].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 34].Value = oBj.Fec_Vali_Fachada_Imple;
                            string _vmante = " ";
                            switch (oBj.Vali_Fechada_Imple.ToUpper())
                            {
                                case "TRUE":
                                    _vmante = "Con mantenimiento";
                                    break;
                            }
                            oWsUna.Cells[_fila, 35].Value = _vmante;



                            for (int i = 1; i <= columnsuser2; i++)
                            {
                                oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                //ajustando texto
                                oWsUna.Cells[_fila, i].Style.WrapText = true;
                            }



                            _fila++;
                        }


                        //Formato Cabecera 1
                        oWsUna.Row(1).Height = 25;
                        oWsUna.Row(1).Style.Font.Bold = true;
                        oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                        oWsUna.Cells["A1:AI1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A1:AI1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                        //Formato Cabecera 2
                        oWsUna.Row(2).Height = 20;
                        oWsUna.Row(2).Style.Font.Bold = true;
                        oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                        oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                        oWsUna.Cells["A2:AI2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A2:AI2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                        oWsUna.SelectedRange["A2:AI2"].AutoFilter = true;


                        //Formato ambas cabeceras
                        oWsUna.Cells["A1:AI2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AI2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AI2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AI2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;




                        //Formato por columnas
                        //Ventas
                        oWsUna.Column(1).Width = 21;
                        oWsUna.Column(2).Width = 19;
                        oWsUna.Column(3).Width = 30;
                        oWsUna.Column(4).Width = 35;
                        oWsUna.Column(5).Width = 20;
                        //trade marketing
                        oWsUna.Column(6).Width = 20;
                        oWsUna.Column(7).Width = 22;
                        oWsUna.Column(8).Width = 35;
                        //lucky
                        oWsUna.Column(9).Width = 22;
                        oWsUna.Column(10).Width = 35;
                        oWsUna.Column(11).Width = 13;
                        oWsUna.Column(12).Width = 13;
                        oWsUna.Column(13).Width = 18;
                        //ventas
                        oWsUna.Column(14).Width = 28;
                        oWsUna.Column(15).Width = 22;
                        oWsUna.Column(16).Width = 35;
                        oWsUna.Column(17).Width = 21;
                        //trade marketing
                        oWsUna.Column(18).Width = 32;
                        oWsUna.Column(19).Width = 22;
                        oWsUna.Column(20).Width = 22;
                        oWsUna.Column(21).Width = 22;
                        oWsUna.Column(22).Width = 35;
                        //lucky
                        oWsUna.Column(23).Width = 26;
                        oWsUna.Column(24).Width = 23;
                        oWsUna.Column(25).Width = 32;
                        oWsUna.Column(26).Width = 29;
                        oWsUna.Column(27).Width = 35;
                        oWsUna.Column(28).Width = 29;
                        oWsUna.Column(29).Width = 31;
                        oWsUna.Column(30).Width = 10;
                        oWsUna.Column(31).Width = 21;
                        oWsUna.Column(32).Width = 21;
                        //trade marketing
                        oWsUna.Column(33).Width = 23;
                        oWsUna.Column(34).Width = 21;
                        //lucky
                        oWsUna.Column(35).Width = 24;

                        //Columnas auto ajustadas
                        /*oWsUna.Column(1).AutoFit();*/

                        oWsUna.View.FreezePanes(3, 5);


                        oEx.Save();

                        #endregion
                    }
                    if (oResper.Cod_Grupo == 3)
                    {
                        #region Perfil 3

                        Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Analista");

                        //Combinacion de celdas
                        oWsUna.Cells["A1:E1"].Merge = true;
                        oWsUna.Cells["F1:H1"].Merge = true;
                        oWsUna.Cells["I1:M1"].Merge = true;
                        oWsUna.Cells["N1:Q1"].Merge = true;
                        oWsUna.Cells["R1:T1"].Merge = true;
                        oWsUna.Cells["U1:AD1"].Merge = true;
                        oWsUna.Cells["AE1:AF1"].Merge = true;

                        //Dando valor a celdas combinadas
                        oWsUna.Cells["A1:E1"].Value = "VENTAS";
                        oWsUna.Cells["F1:H1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["I1:M1"].Value = "LUCKY";
                        oWsUna.Cells["N1:Q1"].Value = "VENTAS";
                        oWsUna.Cells["R1:T1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["U1:AD1"].Value = "LUCKY";
                        oWsUna.Cells["AE1:AF1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["AG1"].Value = "LUCKY";

                        //Dando valor a celdas individuales
                        oWsUna.Cells["A2"].Value = "Usuario de Solicitud";
                        oWsUna.Cells["B2"].Value = "Fecha de Solicitud";
                        oWsUna.Cells["C2"].Value = "Punto de Venta";
                        oWsUna.Cells["D2"].Value = "Direccion";
                        oWsUna.Cells["E2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["F2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["G2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["H2"].Value = "Comentario";
                        oWsUna.Cells["I2"].Value = "Fecha de Visita";
                        oWsUna.Cells["J2"].Value = "Comentario";
                        oWsUna.Cells["K2"].Value = "Fotomontaje";
                        oWsUna.Cells["L2"].Value = "Monto (S/.)";
                        oWsUna.Cells["M2"].Value = "Pre-Presupuesto";
                        oWsUna.Cells["N2"].Value = "Aprobacion de Fotomontaje";
                        oWsUna.Cells["O2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["P2"].Value = "Comentario";
                        oWsUna.Cells["Q2"].Value = "Carta de Aprobacion";
                        oWsUna.Cells["R2"].Value = "Aprobacion de Pre-Presupuesto";
                        oWsUna.Cells["S2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["T2"].Value = "Comentario";
                        oWsUna.Cells["U2"].Value = "Fecha de implementacion";
                        oWsUna.Cells["V2"].Value = "Programacion y Status";
                        oWsUna.Cells["W2"].Value = "Fecha de Ingreso de Expediente";
                        oWsUna.Cells["X2"].Value = "Recibo de Tramite Municipal";
                        oWsUna.Cells["Y2"].Value = "Observacion";
                        oWsUna.Cells["Z2"].Value = "Fecha de Entrega de Licencia";
                        oWsUna.Cells["AA2"].Value = "Fecha de Caducidad de Licencia";
                        oWsUna.Cells["AB2"].Value = "Licencia";
                        oWsUna.Cells["AC2"].Value = "Reporte Fotografico";
                        oWsUna.Cells["AD2"].Value = "Presupuesto Final";
                        oWsUna.Cells["AE2"].Value = "Validacion de Fachada";
                        oWsUna.Cells["AF2"].Value = "Fecha de Validacion";
                        oWsUna.Cells["AG2"].Value = "Visita / Mantenimiento";


                        _fila = 3;

                        foreach (E_Fachada_Unacem oBj in oWfUna)
                        {
                            oWsUna.Cells[_fila, 1].Value = oBj.P1_Usuario_Solicita;
                            oWsUna.Cells[_fila, 2].Value = oBj.Fec_Solicitud;
                            oWsUna.Cells[_fila, 3].Value = oBj.Nom_PDV;
                            oWsUna.Cells[_fila, 4].Value = oBj.Direccion;
                            oWsUna.Cells[_fila, 5].Value = oBj.Est_Solicitud;
                            switch (oBj.Apro_Solicitud.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 6].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 6].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 7].Value = oBj.Fec_Apro_Solicitud;
                            oWsUna.Cells[_fila, 8].Value = oBj.Comen_Apro_Solicitud;
                            oWsUna.Cells[_fila, 9].Value = oBj.P2_Fecha_Entrega_Foto;
                            oWsUna.Cells[_fila, 10].Value = oBj.comen_expediente;
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                oWsUna.Cells[_fila, 11].Value = "Con foto";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 11].Value = "";
                            }
                            oWsUna.Cells[_fila, 12].Value = oBj.p3_monto_presupuesto;
                            if (oBj.Url_Presupuesto != "" && oBj.Url_Presupuesto != null)
                            {
                                oWsUna.Cells[_fila, 13].Value = "Con presupuesto";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 13].Value = "";
                            }
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                switch (oBj.Apro_Fotomontaje.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 14].Value = "Aprobado";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 14].Value = "Desaprobado";
                                        break;
                                }
                            }
                            oWsUna.Cells[_fila, 15].Value = oBj.Fec_Apro_Fotomonataje;
                            oWsUna.Cells[_fila, 16].Value = oBj.Comen_Apro_Fotomontaje;
                            if (oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje.Length > 0)
                            {
                                if (oBj.Url_Carta_Fotomontaje != null && oBj.Url_Carta_Fotomontaje.Replace(" ", "").Length > 0)
                                {
                                    oWsUna.Cells[_fila, 17].Value = "Con carta";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 17].Value = "Falta cargar carta";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 17].Value = "";
                            }
                            switch (oBj.Apro_pre_Presu.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 18].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 18].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 19].Value = oBj.Fec_Apro_pre_Presu;
                            oWsUna.Cells[_fila, 20].Value = oBj.Comen_Apro_Pre_Presu;
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 21].Value = oBj.fecha_implementacion;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 21].Value = "";
                            }
                            string _vcolor = "";
                            if (oBj.Cantidad == 0)
                            {
                                _vcolor = "Pendientes";
                            }
                            else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                            {
                                _vcolor = "Algunos pendientes";
                            }
                            else if (oBj.Cantidad == 10)
                            {
                                _vcolor = "Revisado";
                            }
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 22].Value = _vcolor;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 22].Value = _vcolor;
                            }
                            if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                            {
                                oWsUna.Cells[_fila, 23].Value = oBj.Fec_ingre_expe;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 24].Value = "Con recibo";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 24].Value = "";
                                }
                                oWsUna.Cells[_fila, 25].Value = oBj.Obs_Tramite;
                                oWsUna.Cells[_fila, 26].Value = oBj.Fec_Entre_Licencia;
                                oWsUna.Cells[_fila, 27].Value = oBj.Fec_Cadu_Licencia;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 28].Value = "Con Licencia";

                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 28].Value = "";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 23].Value = "";
                                oWsUna.Cells[_fila, 24].Value = "";
                                oWsUna.Cells[_fila, 25].Value = "";
                                oWsUna.Cells[_fila, 26].Value = "";
                                oWsUna.Cells[_fila, 27].Value = "";
                                oWsUna.Cells[_fila, 28].Value = "";
                            }
                            if (oBj.Url_Report_Fotografico != null && oBj.Url_Report_Fotografico != "")
                            {
                                oWsUna.Cells[_fila, 29].Value = "Con reporte fotografico";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 29].Value = "";
                            }
                            if (oBj.Url_Presu_Final != null && oBj.Url_Presu_Final != "")
                            {
                                oWsUna.Cells[_fila, 30].Value = "Con presupuesto final";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 30].Value = "";
                            }

                            switch (oBj.Vali_Fechada_Imple.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 31].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 31].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 32].Value = oBj.Fec_Vali_Fachada_Imple;
                            string _vmante = " ";
                            switch (oBj.Vali_Fechada_Imple.ToUpper())
                            {
                                case "TRUE":
                                    _vmante = "Con mantenimiento";
                                    break;
                            }
                            oWsUna.Cells[_fila, 33].Value = _vmante;



                            for (int i = 1; i <= columnsuser3; i++)
                            {
                                oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                //ajustando texto
                                oWsUna.Cells[_fila, i].Style.WrapText = true;
                            }



                            _fila++;
                        }

                        //Formato Cabecera 1
                        oWsUna.Row(1).Height = 25;
                        oWsUna.Row(1).Style.Font.Bold = true;
                        oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                        oWsUna.Cells["A1:AG1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A1:AG1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                        //Formato Cabecera 2
                        oWsUna.Row(2).Height = 20;
                        oWsUna.Row(2).Style.Font.Bold = true;
                        oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                        oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                        oWsUna.Cells["A2:AG2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A2:AG2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                        oWsUna.SelectedRange["A2:AG2"].AutoFilter = true;


                        //Formato ambas cabeceras
                        oWsUna.Cells["A1:AG2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AG2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AG2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AG2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;




                        //Formato por columnas
                        //Ventas
                        oWsUna.Column(1).Width = 21;
                        oWsUna.Column(2).Width = 19;
                        oWsUna.Column(3).Width = 30;
                        oWsUna.Column(4).Width = 35;
                        oWsUna.Column(5).Width = 20;
                        //trade marketing
                        oWsUna.Column(6).Width = 20;
                        oWsUna.Column(7).Width = 22;
                        oWsUna.Column(8).Width = 35;
                        //lucky
                        oWsUna.Column(9).Width = 22;
                        oWsUna.Column(10).Width = 35;
                        oWsUna.Column(11).Width = 13;
                        oWsUna.Column(12).Width = 13;
                        oWsUna.Column(13).Width = 18;
                        //ventas
                        oWsUna.Column(14).Width = 28;
                        oWsUna.Column(15).Width = 22;
                        oWsUna.Column(16).Width = 35;
                        oWsUna.Column(17).Width = 21;
                        //trade marketing
                        oWsUna.Column(18).Width = 32;
                        oWsUna.Column(19).Width = 22;
                        oWsUna.Column(20).Width = 35;
                        //lucky
                        oWsUna.Column(21).Width = 26;
                        oWsUna.Column(22).Width = 23;
                        oWsUna.Column(23).Width = 32;
                        oWsUna.Column(24).Width = 29;
                        oWsUna.Column(25).Width = 35;
                        oWsUna.Column(26).Width = 29;
                        oWsUna.Column(27).Width = 31;
                        oWsUna.Column(28).Width = 10;
                        oWsUna.Column(29).Width = 21;
                        oWsUna.Column(30).Width = 21;
                        //trade marketing
                        oWsUna.Column(31).Width = 23;
                        oWsUna.Column(32).Width = 21;
                        //lucky
                        oWsUna.Column(33).Width = 24;

                        //Columnas auto ajustadas
                        /*oWsUna.Column(1).AutoFit();*/

                        oWsUna.View.FreezePanes(3, 5);


                        oEx.Save();

                        #endregion
                    }
                    if (oResper.Cod_Grupo == 4)
                    {
                        #region Perfil 4

                        Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Tramite");

                        //Combinacion de celdas
                        oWsUna.Cells["A1:E1"].Merge = true;
                        oWsUna.Cells["F1:M1"].Merge = true;

                        //Dando valor a celdas combinadas
                        oWsUna.Cells["A1:E1"].Value = "VENTAS";
                        oWsUna.Cells["F1:M1"].Value = "LUCKY";

                        //Dando valor a celdas individuales
                        oWsUna.Cells["A2"].Value = "Usuario de Solicitud";
                        oWsUna.Cells["B2"].Value = "Fecha de Solicitud";
                        oWsUna.Cells["C2"].Value = "Punto de Venta";
                        oWsUna.Cells["D2"].Value = "Direccion";
                        oWsUna.Cells["E2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["F2"].Value = "Fecha de implementacion";
                        oWsUna.Cells["G2"].Value = "Programacion y Status";
                        oWsUna.Cells["H2"].Value = "Fecha de Ingreso de Expediente";
                        oWsUna.Cells["I2"].Value = "Recibo de Tramite Municipal";
                        oWsUna.Cells["J2"].Value = "Observacion";
                        oWsUna.Cells["K2"].Value = "Fecha de Entrega de Licencia";
                        oWsUna.Cells["L2"].Value = "Fecha de Caducidad de Licencia";
                        oWsUna.Cells["M2"].Value = "Licencia";


                        _fila = 3;

                        foreach (E_Fachada_Unacem oBj in oWfUna)
                        {
                            oWsUna.Cells[_fila, 1].Value = oBj.P1_Usuario_Solicita;
                            oWsUna.Cells[_fila, 2].Value = oBj.Fec_Solicitud;
                            oWsUna.Cells[_fila, 3].Value = oBj.Nom_PDV;
                            oWsUna.Cells[_fila, 4].Value = oBj.Direccion;
                            oWsUna.Cells[_fila, 5].Value = oBj.Est_Solicitud;
                            //Url_Gantt_Imple
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 6].Value = oBj.fecha_implementacion;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 6].Value = "";
                            }
                            string _vcolor = "";
                            if (oBj.Cantidad == 0)
                            {
                                _vcolor = "Pendientes";
                            }
                            else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                            {
                                _vcolor = "Algunos pendientes";
                            }
                            else if (oBj.Cantidad == 10)
                            {
                                _vcolor = "Revisado";
                            }
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 7].Value = _vcolor;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 7].Value = _vcolor;
                            }
                            if (ViewBag.cod_grupo == 4 && oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                            {
                                if (oBj.Fec_ingre_expe != null && oBj.Fec_ingre_expe != "" && oBj.Fec_ingre_expe != "01/01/1900" && oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 8].Value = oBj.Fec_ingre_expe;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 8].Value = "__________";
                                }
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 9].Value = "Con recibo";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 9].Value = "__________";
                                }
                                if (oBj.Obs_Tramite != null && oBj.Obs_Tramite != "")
                                {
                                    oWsUna.Cells[_fila, 10].Value = oBj.Obs_Tramite;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 10].Value = "__________";
                                }
                                if (oBj.Fec_Entre_Licencia != null && oBj.Fec_Entre_Licencia != "" && oBj.Fec_Entre_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                {
                                    oWsUna.Cells[_fila, 11].Value = oBj.Fec_Entre_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 11].Value = "__________";
                                }
                                if (oBj.Fec_Cadu_Licencia != null && oBj.Fec_Cadu_Licencia != "" && oBj.Fec_Cadu_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                {
                                    oWsUna.Cells[_fila, 12].Value = oBj.Fec_Cadu_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 12].Value = "__________";
                                }
                                if (oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                {
                                    oWsUna.Cells[_fila, 13].Value = "Con licencia";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 13].Value = "__________";
                                }
                            }
                            else if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                            {
                                oWsUna.Cells[_fila, 8].Value = oBj.Fec_ingre_expe;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 9].Value = "Con recibo";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 9].Value = "";
                                }
                                oWsUna.Cells[_fila, 10].Value = oBj.Obs_Tramite;
                                oWsUna.Cells[_fila, 11].Value = oBj.Fec_Entre_Licencia;
                                oWsUna.Cells[_fila, 12].Value = oBj.Fec_Cadu_Licencia;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 13].Value = "Con Licencia";

                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 13].Value = "";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 8].Value = "";
                                oWsUna.Cells[_fila, 9].Value = "";
                                oWsUna.Cells[_fila, 10].Value = "";
                                oWsUna.Cells[_fila, 11].Value = "";
                                oWsUna.Cells[_fila, 12].Value = "";
                                oWsUna.Cells[_fila, 13].Value = "";
                            }




                            for (int i = 1; i <= columnsuser4; i++)
                            {
                                oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                //ajustando texto
                                oWsUna.Cells[_fila, i].Style.WrapText = true;
                            }



                            _fila++;
                        }

                        //Formato Cabecera 1
                        oWsUna.Row(1).Height = 25;
                        oWsUna.Row(1).Style.Font.Bold = true;
                        oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                        oWsUna.Cells["A1:M1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A1:M1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                        //Formato Cabecera 2
                        oWsUna.Row(2).Height = 20;
                        oWsUna.Row(2).Style.Font.Bold = true;
                        oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                        oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                        oWsUna.Cells["A2:M2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A2:M2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                        oWsUna.SelectedRange["A2:M2"].AutoFilter = true;


                        //Formato ambas cabeceras
                        oWsUna.Cells["A1:M2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:M2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:M2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:M2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;




                        //Formato por columnas
                        //Ventas
                        oWsUna.Column(1).Width = 21;
                        oWsUna.Column(2).Width = 19;
                        oWsUna.Column(3).Width = 30;
                        oWsUna.Column(4).Width = 35;
                        oWsUna.Column(5).Width = 20;
                        //lucky
                        oWsUna.Column(6).Width = 26;
                        oWsUna.Column(7).Width = 23;
                        oWsUna.Column(8).Width = 32;
                        oWsUna.Column(9).Width = 29;
                        oWsUna.Column(10).Width = 35;
                        oWsUna.Column(11).Width = 29;
                        oWsUna.Column(12).Width = 31;
                        oWsUna.Column(13).Width = 10;

                        //Columnas auto ajustadas
                        /*oWsUna.Column(1).AutoFit();*/

                        oWsUna.View.FreezePanes(3, 5);


                        oEx.Save();

                        #endregion
                    }

                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }


        #endregion

        #region <<< WORD >>>


        private string Word_Insertar_Img(string nomdoc, string __a, string __b, string __c, string __d, string __e)
        {
            // __e : nombre de punto de venta
            object objMiss = System.Reflection.Missing.Value;
            Word.Application objWord = new Word.Application();

            objWord.Visible = false;
            objWord.DisplayAlerts = Word.WdAlertLevel.wdAlertsNone;

            try
            {
                string ruta = Path.Combine(LocalFachada, "Word/" + nomdoc);
                //string ruta2 = HttpContext.Server.MapPath("~/Temp/Fachada/Img/Img_13012016_030632.jpg");
                //string rtimg1 = HttpContext.Server.MapPath("~" + __a);
                //string rtimg2 = HttpContext.Server.MapPath("~" + __b);
                //string rtimg3 = HttpContext.Server.MapPath("~" + __c);
                //string rtimg4 = HttpContext.Server.MapPath("~" + __d);
                string rtimg1 = Path.Combine(new string[] { Local, __a.Replace("/Fachada", "Fachada") });
                string rtimg2 = Path.Combine(new string[] { Local, __b.Replace("/Fachada", "Fachada") });
                string rtimg3 = Path.Combine(new string[] { Local, __c.Replace("/Fachada", "Fachada") });
                string rtimg4 = Path.Combine(new string[] { Local, __d.Replace("/Fachada", "Fachada") });

                DateTime thisDay = DateTime.Today;
                string fecha_generacon = thisDay.ToString("D").Replace(thisDay.ToString("dddd", CultureInfo.CreateSpecificCulture("es-ES")), "Lima");

                //object parametro = ruta;

                Word.Document objDoc = objWord.Documents.Open(ruta);

                Console.Write(ruta);

                object nombre1 = "Img1";
                object nombre2 = "Img2";
                object nombre3 = "Img3";
                object nombre4 = "Img4";
                //object nombre5 = "nompdv";
                //object nombre6 = "fechagen";

                Word.Range nom = objDoc.Bookmarks.get_Item(ref nombre1).Range;
                Word.Range nom2 = objDoc.Bookmarks.get_Item(ref nombre2).Range;
                Word.Range nom3 = objDoc.Bookmarks.get_Item(ref nombre3).Range;
                Word.Range nom4 = objDoc.Bookmarks.get_Item(ref nombre4).Range;
                //Word.Range nom5 = objDoc.Bookmarks.get_Item(ref nombre5).Range;
                //Word.Range nom6 = objDoc.Bookmarks.get_Item(ref nombre6).Range;

                nom.Text = "";
                nom2.Text = "";
                nom3.Text = "";
                nom4.Text = "";
                //nom5.Text = __e;
                //nom6.Text = fecha_generacon;

                //object rango1 = nom;
                //objDoc.Bookmarks.Add("nombre",ref rango1);

                //objWord.InlineShapes.AddPicture();
                var obj1 = objDoc.InlineShapes.AddPicture(rtimg1, Type.Missing, Type.Missing, nom);
                var obj2 = objDoc.InlineShapes.AddPicture(rtimg2, Type.Missing, Type.Missing, nom2);
                var obj3 = objDoc.InlineShapes.AddPicture(rtimg3, Type.Missing, Type.Missing, nom3);
                var obj4 = objDoc.InlineShapes.AddPicture(rtimg4, Type.Missing, Type.Missing, nom4);

                obj1.Width = 150; obj1.Height = 100;
                obj2.Width = 150; obj2.Height = 100;
                obj3.Width = 150; obj3.Height = 100;
                obj4.Width = 150; obj4.Height = 100;

                objWord.Documents.Save();
                objWord.Documents.Close();
                objWord.Quit();

                GC.Collect();
                //return "/Temp/Fachada/Word/" + nomdoc;
            }
            catch (System.IO.IOException)
            {
                objWord.Documents.Close();
                objWord.Quit();
                //objWord.Visible = false;
                GC.Collect();
            }
            return nomdoc;
        }
        private string Word_Generar()
        {

            //string fileName = "Carta_de_conformidad_de_fotomontaje.docx";
            string fileName = "Carta_de_presentacion.docx";
            string vnom = String.Format("{0:yyyyMMdd_HHmmss}", DateTime.Now);
            //string vnom = String.Format("{0:ddMMyyyy}", DateTime.Now);
            string fileName2 = "Carta_presentacion_" + vnom + ".doc";
            //string sourcePath = @"\Temp\Fachada\Word";
            //string targetPath = @"C:\Users\Public\TestFolder\SubDir";

            string sourceFile = System.IO.Path.Combine(LocalFachada, "Word", fileName);
            string destFile = System.IO.Path.Combine(LocalFachada, "Word", fileName2);
            //string destFile = System.IO.Path.Combine(targetPath, fileName);

            System.IO.File.Copy(sourceFile, destFile, true);

            return fileName2;
        }
        #endregion

        #region << Observaciones >>

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 13/06/2016
        /// Descripcion: 
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        public ActionResult WF_Una_Filtro(string __a, string __b, int __c)
        {
            ViewBag.cod_elemento = __a;
            return View(new WorkFlow()
                    .Filtro(new Resquest_New_XPL_Una_WF()
                    {
                        opcion = __c,
                        parametros = __b
                    }));
        }

        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 19/06/2016
        /// Descripcion: Permite Actualizar Fecha de Entrega en la malla de Fotomontaje
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        public JsonResult WF_Una_FechaEntrega_Fotomontaje(int __a, string __b)
        {
            E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
            StatusRegistro = new WorkFlow().Actualizar_FechaEntrega_Foto(
                   new Request_WF_DetailGantt()
                   {
                       Cod_Reg_Tabla = __a,
                       fecha_entrega = __b
                   });
            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult WF_Una_Fechaimplementacion(int __a, string __b)
        //{
        //    E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
        //    StatusRegistro = new WorkFlow().Actualizar_FechaImplementacion(
        //           new Request_WF_DetailGantt()
        //           {
        //               Cod_Reg_Tabla = __a,
        //               fecha_implementacion = __b
        //           });
        //    return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult WF_Una_Fechaimplementacion(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Actualizar_FechaImplementacion(
                    new Request_WF_General() { Cod_Op = __a, fecha_implementacion = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }

        #endregion

    }
}