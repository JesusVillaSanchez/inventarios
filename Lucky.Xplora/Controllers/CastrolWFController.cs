﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Unacem;

using IO = System.IO;
using System.Web.Script.Serialization;
using Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.IO;
using Lucky.Xplora.Security;
using System.Web.Security;

using System.Globalization;

using System.Drawing;
using System.Drawing.Imaging;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using System.Net;
using System.Web.UI;
using System.Configuration;

namespace Lucky.Xplora.Controllers
{
    public class CastrolWFController : Controller
    {
        string Local = Convert.ToString(ConfigurationManager.AppSettings["Local"]);
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        string LocalFachada = Convert.ToString(ConfigurationManager.AppSettings["LocalFachada"]);
        string LocalFormat = Convert.ToString(ConfigurationManager.AppSettings["LocalFormat"]);

        #region Work Flow
        [CustomAuthorize]
        public ActionResult Index(string _a)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.cod_persona = ((Persona)Session["Session_Login"]).Person_id;

            Persona lstPerson = ((Persona)Session["Session_Login"]);
            ViewBag.nombre_persona = lstPerson.Person_Firtsname + " " + lstPerson.Person_LastName + " " + lstPerson.Person_Surname;

            E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
            oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = ((Persona)Session["Session_Login"]).Person_id });
            ViewBag.cod_grupo = oResper.Cod_Grupo;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            Session["Session_Campania"] = _a;
            ViewBag.cod_equipo = _a;
            return View();
        }

        public ActionResult GetGrilla(int cod_zona, string fecha_solicitud, string fecha_foto, string distribuidora, int cod_persona, string fecha_presupuesto, string id_planning, int estado_solicitud, int cod_marca, int status)
        {
            int vcod_persona;
            vcod_persona = ((Persona)Session["Session_Login"]).Person_id;
            E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
            oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = vcod_persona });
            ViewBag.cod_grupo = oResper.Cod_Grupo;
            ViewBag.nombre_grupo = oResper.Grupo;
            return View(new WorkFlow().Consulta_Solicitud(new Request_WF_General
            {
                Cod_Grupo = oResper.Cod_Grupo,
                cod_zona = cod_zona,
                fecha_solicitud = fecha_solicitud,
                fecha_foto = fecha_foto,
                distribuidora = distribuidora,
                Cod_Persona = cod_persona,
                fecha_presupuesto = fecha_presupuesto,
                cod_equipo = id_planning,
                Estado = estado_solicitud,
                Cod_Marca = cod_marca,
                status = status
            }));

        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 28-06-2016
        /// </summary>
        /// <param name="cod_zona"></param>
        /// <param name="fecha_solicitud"></param>
        /// <param name="fecha_foto"></param>
        /// <param name="distribuidora"></param>
        /// <param name="cod_persona"></param>
        /// <returns></returns>
        public ActionResult GetPresupuestoGrilla(int cod_zona, string fecha_solicitud, string fecha_foto, string distribuidora, int cod_persona, string fecha_presupuesto, string id_planning, int estado_solicitud, int cod_marca)
        {
            int vcod_persona;
            vcod_persona = ((Persona)Session["Session_Login"]).Person_id;
            E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
            oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = vcod_persona });

            List<E_Fachada_Unacem> oLsWf = new WorkFlow().Consulta_Solicitud(
                new Request_WF_General()
                {
                    Cod_Grupo = oResper.Cod_Grupo,
                    cod_zona = cod_zona,
                    fecha_solicitud = fecha_solicitud,
                    fecha_foto = fecha_foto,
                    distribuidora = distribuidora,
                    Cod_Persona = cod_persona,
                    fecha_presupuesto = fecha_presupuesto,
                    cod_equipo = id_planning,
                    Estado = estado_solicitud,
                    Cod_Marca = cod_marca
                });

            return Json(new { Archivo = oLsWf });
            //return Content(new ContentResult
            //{
            //    Content = MvcApplication._Serialize(
            //        new WorkFlow().Consulta_Solicitud(new Request_WF_General { Cod_Grupo = oResper.Cod_Grupo, cod_zona = cod_zona, fecha_solicitud = fecha_solicitud, fecha_foto = fecha_foto, distribuidora = distribuidora, Cod_Persona = cod_persona })   
            //    ),
            //    ContentType = "application/json"
            //}.Content);
        }

        public ActionResult AddSolicitud(int vids, string vpdv, string vruc, string vcodigo, string vdireccion, string vdistrito, string vprovincia, string vdepartamento, string vcontacto, string vnumtelef, string vnumcel,
            string vdistribuidora, string vfoto, string vven_prom_soles, string vven_prom_ton, string vlinea_credito, string vdeu_actual, string vop_grabado, int vop_opera, int p1_codperson, string p1_codTipo, int p1_codZona,
            string p1_comentarios, int vmarca, int v_tipo_gral_solicitud)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Agregar_solicitud(
                    new Request_FW_add_solicitud()
                    {
                        cod_equipo = Convert.ToString(Session["Session_Campania"]),
                        ids = vids,
                        pdv = vpdv,
                        ruc = vruc,
                        codigo = vcodigo,
                        direccion = vdireccion,
                        distrito = vdistrito,
                        provincia = vprovincia,
                        departamento = vdepartamento,
                        contacto = vcontacto,
                        numtelef = vnumtelef,
                        numcel = vnumcel,
                        distribuidora = vdistribuidora,
                        foto = vfoto,
                        ven_prom_soles = vven_prom_soles,
                        ven_prom_ton = vven_prom_ton,
                        linea_credito = vlinea_credito,
                        deu_actual = vdeu_actual,
                        op_grabado = vop_grabado,
                        op_opera = vop_opera,
                        p1_codperson = p1_codperson,
                        p1_codTipo = p1_codTipo,
                        p1_codZona = p1_codZona,
                        p1_comentarios = p1_comentarios,
                        marca = vmarca,
                        tipo_gralSolicitud = v_tipo_gral_solicitud
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult AproSolicitudp2(int vnumero, int vestado, string vcomentario)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().P2_Aprobacion_Solicitud_Trade(
                    new Request_WF_General()
                    {
                        Num_Solicitud = vnumero,
                        Estado = vestado,
                        Comen_Apro = vcomentario
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult GetEditGrantt()
        {
            if (Request["_a"] != null)
            {
                ViewBag.cod_equipo = Convert.ToString(Request["_a"]);
                ViewBag.cod_reg_tabla = Convert.ToString(Request["_b"]);
                ViewBag.cod_op = Convert.ToString(Request["_c"]);// 1 fotomontaje - 2 implementacion
                ViewBag.cod_area = Convert.ToString(Request["_d"]);// codigo de area
                ViewBag.cod_persona = Convert.ToString(Request["_e"]);// codigo de persona
            }
            return View();
        }
        [HttpPost]
        public ActionResult JsonLlenarGantt(string __a, int __b, int __c, int __d, string __e, int __f)
        {
            //  Word_Insertar_Img();
            //string vcarta = Word_Generar();
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().LlenarGantt(
                    new Request_WF_General()
                    {
                        Cod_Equipo = __a,
                        Id_Reg = __b,
                        Cod_Op = __c,
                        Cod_Area = __d,
                        Fecha = __e,
                        Cod_Persona = __f
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public JsonResult Guardar_DetalleGantt(string DataItems, string cod_equipo, int cod_reg_tb, int cod_opcion, int cod_p)
        {
            List<E_Nw_Detail_Gantt> oListDetailGantt = new List<E_Nw_Detail_Gantt>();

            var jss = new System.Web.Script.Serialization.JavaScriptSerializer();
            dynamic Items = jss.Deserialize<dynamic>(DataItems);
            var vidgantt = "";
            foreach (var item in Items["tasks"])
            {
                E_Nw_Detail_Gantt oDetailGantt = new E_Nw_Detail_Gantt();
                vidgantt += Convert.ToString(item["id"]) + ",";
                oDetailGantt.id = Convert.ToString(item["id"]);
                oDetailGantt.name = item["name"];
                oDetailGantt.code = item["code"];
                oDetailGantt.level = Convert.ToInt32(item["level"]);
                oDetailGantt.status = item["status"];
                oDetailGantt.canWrite = Convert.ToBoolean(item["canWrite"]);
                oDetailGantt.start = Convert.ToString(item["start"]);
                oDetailGantt.duration = Convert.ToInt32(item["duration"]);
                oDetailGantt.gend = Convert.ToString(item["end"]);
                oDetailGantt.startIsMilestone = Convert.ToBoolean(item["startIsMilestone"]);
                oDetailGantt.endIsMilestone = Convert.ToBoolean(item["endIsMilestone"]);
                if (item.ContainsKey("collapsed"))
                {
                    oDetailGantt.collapsed = Convert.ToBoolean(item["collapsed"]);
                }
                else
                {
                    oDetailGantt.collapsed = false;
                }

                oDetailGantt.assigs = null;
                oDetailGantt.hasChild = Convert.ToBoolean(item["hasChild"]);
                oDetailGantt.progress = Convert.ToInt32(item["progress"]);
                oDetailGantt.description = Convert.ToString(item["description"]);
                oDetailGantt.depends = Convert.ToString(item["depends"]);
                oDetailGantt.Promotion_CreateBy = Convert.ToString(cod_p);//((Persona)Session["Session_Login"]).name_user.ToString();
                oListDetailGantt.Add(oDetailGantt);
            }
            vidgantt = vidgantt.Substring(0, vidgantt.Length - 1);

            E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
            if (oListDetailGantt.Count > 0)
            {
                StatusRegistro = new WorkFlow().Registrar_DetailGantt(
                   new Request_WF_DetailGantt()
                   {
                       oDetailGantt = oListDetailGantt,
                       Cod_Equipo = cod_equipo,
                       Cod_Reg_Tabla = cod_reg_tb,
                       strlist_promocion = vidgantt,
                       Cod_Opcion = cod_opcion
                   });
            }
            else
            {
                StatusRegistro.CodStatus = E_Nw_DetailGanttRegistro.GENERAL_ERROR;
                StatusRegistro.Mensaje = "No ha digitado ningún dato, Columna Exhibición es Obligatorio.";
            }
            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
            //return Json("");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Carga_Archivo(HttpPostedFileBase __f, string _b)
        {
            string _archivo = "";
            string _fileLocal = "";
            string _doc_url = "";

            if (__f != null)
            {
                _fileLocal = IO.Path.GetFileName(__f.FileName);
                _archivo = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                string vtipoarchi = _fileLocal.Substring(_fileLocal.LastIndexOf("."), 4).ToLower();

                switch (vtipoarchi)
                {
                    case ".xls":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(Path.Combine(LocalFachada, "Excel", "Excel_" + _archivo + ".xlsx"));
                            _doc_url = "/Fachada/Excel" + "/Excel_" + _archivo + ".xlsx";
                        }

                        break;
                    case ".pdf":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(Path.Combine(LocalFachada, "Pdf", "PDF_" + _archivo + ".pdf"));
                            _doc_url = "/Fachada/Pdf" + "/PDF_" + _archivo + ".pdf";
                        }
                        break;
                    case ".jpg":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".jpg"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".jpg";
                        break;
                    case ".png":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".png"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".png";
                        break;
                    case ".pptx":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(IO.Path.Combine(LocalFachada, "Ppt", "PPTX_" + _archivo + ".pptx"));
                            _doc_url = "/Fachada/Ppt" + "/PPTX_" + _archivo + ".pptx";
                        }
                        break;
                    case ".ppt":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(IO.Path.Combine(LocalFachada, "Ppt", "PPT_" + _archivo + ".ppt"));
                            _doc_url = "/Fachada/Ppt" + "/PPT_" + _archivo + ".ppt";
                        }
                        break;
                }
            }

            var headerAcceptVariable1 = this.Request.Headers["ACCEPT"] as string;

            if (headerAcceptVariable1 != null && headerAcceptVariable1.Contains("application/json"))
            {
                return Json(_doc_url);
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var response = serializer.Serialize(_doc_url);
                return Content(response);
            }
        }

        [HttpPost]
        public ActionResult Carga_Img_FotoMon(HttpPostedFileBase __f, int __a, int __b)
        {
            string _archivo = "";
            string _fileLocal = "";
            string _doc_url = "";

            if (__f != null)
            {
                _fileLocal = IO.Path.GetFileName(__f.FileName);
                _archivo = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                string vtipoarchi = _fileLocal.Substring(_fileLocal.LastIndexOf("."), 4);

                switch (vtipoarchi)
                {
                    case ".jpg":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".jpg"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".jpg";
                        break;
                    case ".png":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".png"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".png";
                        break;
                }
            }

            WorkFlow objService = new WorkFlow();
            E_WF_Fotomontaje objresponse = objService.AddUpt_Fotomontaje(new Request_WF_General
            {
                Id_Reg = __a,
                img = _doc_url,
                orden = __b,
                Cod_Op = 1
            });
            var headerAcceptVariable1 = this.Request.Headers["ACCEPT"] as string;

            if (headerAcceptVariable1 != null && headerAcceptVariable1.Contains("application/json"))
            {
                return Json(_doc_url);
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var response = serializer.Serialize(_doc_url);
                return Content(response);
            }
        }

        public ActionResult Eliminar_Img_FotoMon(int __a, int __b, string __c)
        {
            WorkFlow objService = new WorkFlow();
            E_WF_Fotomontaje objresponse = objService.AddUpt_Fotomontaje(new Request_WF_General { Id_Reg = __a, img = "", orden = __b, Cod_Op = 2 });

            string sms = "0";
            if (objresponse.Cod_Reg_Table == 0)
            {
                sms = "1";
                if (System.IO.File.Exists(Path.Combine(LocalTemp, __c)))
                {
                    try
                    {
                        System.IO.File.Delete(IO.Path.Combine(LocalTemp, __c));
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            return Json(sms);
        }
        [HttpPost]
        public JsonResult Generar_Documento(string __a, string __b, string __c, string __d, string __e, int __f, string __g)
        {
            string vdocumento = Word_Generar();

            if (__b == null || __b == "") { __b = "/Fachada/Img/blanco.jpg"; }
            if (__c == null || __c == "") { __c = "/Fachada/Img/blanco.jpg"; }
            if (__d == null || __d == "") { __d = "/Fachada/Img/blanco.jpg"; }
            if (__e == null || __e == "") { __e = "/Fachada/Img/blanco.jpg"; }


            string vresp = Word_Insertar_Img(vdocumento, __b, __c, __d, __e, __a);
            if (vdocumento == null || vdocumento == "" || vdocumento == " ") { vdocumento = "0"; }
            else
            {
                vdocumento = vresp;
                WorkFlow objService = new WorkFlow();
                string objresponse = objService.AddFotomont_PrePresupuesto(new Request_WF_General { Id_Reg = __f, Comen_Apro = vdocumento, Cod_Op = 1, Coment_Fotomontaje = __g });
            }
            return Json(vdocumento, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Consultar_Fotomontaje(int __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Consul_FotoMontaje(
                    new Request_WF_General() { Id_Reg = __a }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_PrePresupuesto(int __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AddFotomont_PrePresupuesto(
                    new Request_WF_General() { Id_Reg = __a, Comen_Apro = __b, Cod_Op = 2 }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor:WLOPEZ
        /// Fecha: 23/02/2017
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        public ActionResult Reg_PrePresupuesto_final(int __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AddFotomont_PrePresupuesto(
                    new Request_WF_General() { Id_Reg = __a, Comen_Apro = __b, Cod_Op = 5 }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Asociacion_PrePresupuesto(string __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AsociacionPdv_PrePresupuesto(
                    new Request_WF_General() { str_codigo_Presupuesto = __a, Comen_Apro = __b }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Eliminar_SolicitudVentas(int __a, int __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Remove_SolicitudVentas(
                    new Request_WF_General() { Num_Solicitud = __a, Cod_Op = __b }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 27/06/2016
        /// Descripcion : <>
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        public ActionResult RegDetalle_PrePresupuesto(int __a, string __b, string __c, string __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AddFotomont_PrePresupuesto(
                    new Request_WF_General() { Id_Reg = __a, Monto_Presupuesto = __b, Codigo_Presupuesto = __c, Cod_Op = 3, str_codigo_Presupuesto = __d }
                )),
                ContentType = "application/json"
            }.Content);
        }


        public ActionResult RegDetalle_PrePresupuesto_Final(int __a, string __b, string __c, string __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AddFotomont_PrePresupuesto(
                    new Request_WF_General() { Id_Reg = __a, Monto_Presupuesto = __b, Codigo_Presupuesto = __c, Cod_Op = 4, str_codigo_Presupuesto = __d }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso4_Ventas(int __a, int __b, string __c, int __d)
        {
            if (__d == 2 && __b == 2)
            {
                if (System.IO.File.Exists(Path.Combine(LocalFachada, __c)))
                {
                    try
                    {
                        System.IO.File.Delete(Path.Combine(LocalFachada, __c));
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso4_Ventas(
                    new Request_WF_General() { Id_Reg = __a, Estado = __b, Comen_Apro = __c, Cod_Op = __d }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso5_Trade(int __a, string __b, string __c, string __d, int __e, int __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso5_Trade(
                    new Request_WF_General() { Id_Reg = __a, Ord_Pago = __b, Hes = __c, Ord_Comentario = __d, Estado = __e, Cod_Op = __f }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso6_LuckyAnalis(int __a, string __b, string __c, int __d, int __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso6_LuckyAnalista(
                    new Request_WF_General() { Id_Reg = __a, Documento = __b, Fecha = __c, orden = __d, Cod_Op = __e, Nombre_Registro = __f }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso7_LuckyTramite(int __a, string __b, string __c, string __d, string __e, string __f, string __g, int __h, string __i, string __j, string __k)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso7_LuckyTramite(
                    new Request_WF_General()
                    {
                        Id_Reg = __a
                        ,
                        fec_ing_expe = __b
                        ,
                        ruta_recibo = __c
                        ,
                        obs_recibo = __d
                        ,
                        fec_licencia = __e
                        ,
                        fec_vencimiento = __f
                        ,
                        ruta_licencia = __g
                        ,
                        Cod_Op = __h
                        ,
                        Monto_Presupuesto_Final = __i
                        ,
                        Codigo_Presupuesto_Final = __j
                        ,
                        Url_Presu_Final = __k
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso7_LuckyAnalista(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso7_LuckyAnalista(
                    new Request_WF_General()
                    {
                        Id_Reg = __a,
                        Documento = __b,
                        Cod_Op = __c
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }
        public ActionResult Reg_Paso8_Trade(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso8_Trade(
                    new Request_WF_General()
                    {
                        Id_Reg = __a,
                        Documento = __b,
                        Cod_Op = __c
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult VMModal(int __a)
        {
            ViewBag.cod_grupo = __a;
            // Modal de visita y mantenimiento
            return View();
        }

        public ActionResult Reg_Vista(int __a, string __b, string __c, int __d, int __e, int __f, int __g)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Visita(
                    new Request_WF_General()
                    {
                        Id_Reg = __a
                        ,
                        fec_ing_expe = __b
                        ,
                        img = __c
                        ,
                        Estado = __d
                        ,
                        Cod_Grupo = __e
                        ,
                        orden = __f
                        ,
                        Cod_Op = __g
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult GridMan(int __a, int __b, string __c)
        {
            ViewBag.cod_grupo = __a;
            ViewBag.nom_pdv = __c;
            return View(new WorkFlow().Consulta_Mantenimiento(new Request_WF_General { Cod_Grupo = __a, Id_Reg = __b }));
        }

        public ActionResult RegNewMantenimiento(int __a, string __b, string __c, string __d, string __e, string __f, int __g, int __h, int __i)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Mantenimiento_Nuevo(
                    new Request_FW_add_Mantenimiento()
                    {
                        Id_Reg = __a
                       ,
                        Cod_Detalle = __i
                       ,
                        foto = __b
                       ,
                        ven_prom_soles = __c
                       ,
                        ven_prom_ton = __d
                       ,
                        linea_credito = __e
                       ,
                        deu_actual = __f
                       ,
                        Estado = __g
                       ,
                        Cod_Op = __h
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult RegMatenimientoPasos(int __a, int __b, string __c, string __d, string __e, string __f, int __g, int __h)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Mantenimiento_Pasos(
                    new Request_WF_General()
                    {
                        Id_Reg = __a
                        ,
                        Estado = __b
                        ,
                        Comen_Apro = __c
                        ,
                        Documento = __d
                        ,
                        Ord_Pago = __e
                        ,
                        Hes = __f
                        ,
                        Cod_Grupo = __g
                        ,
                        Cod_Op = __h
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 06/09/2016
        /// Descripcion: Descarga Excel
        /// </summary>
        /// <param name="nomdoc"></param>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        /// 

        public ActionResult Reg_Paso6_lucky(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso6_lucky(
                    new Request_WF_General() { Cod_Op = __a, comen_expediente = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }
        public ActionResult Reg_Paso3_Lucky(int __a, int __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso3_lucky(
                    new Request_WF_General() { Cod_Op = __a, Provider_code = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }
        public ActionResult Reg_Unacem(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Unacem(
                    new Request_WF_General() { Cod_Op = __a, chk_fila = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }
        public JsonResult WFExcel(int __a, string __b, string __c, string __d, int __e, string __f, int __g, int __h)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("WF_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte WorkFlow >>

                int vcod_persona;
                vcod_persona = ((Persona)Session["Session_Login"]).Person_id;
                E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
                oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = vcod_persona });

                List<E_Fachada_Unacem> oWfUna = new WorkFlow().Consulta_Solicitud(
                   new Request_WF_General()
                   {
                       Cod_Grupo = oResper.Cod_Grupo,
                       cod_zona = __a,
                       fecha_solicitud = __b,
                       fecha_foto = __c,
                       distribuidora = __d,
                       Cod_Persona = __e,
                       fecha_presupuesto = __f,
                       cod_equipo = Convert.ToString(Session["Session_Campania"]),
                       Estado = __g,
                       Cod_Marca = __h
                   });

                #endregion

                #endregion


                #region <<< Reporte WorkFlow >>>

                if ((oWfUna != null))
                {
                    int columnsuser1 = 45;
                    int columnsuser2 = 49;
                    int columnsuser3 = 48;
                    int columnsuser4 = 26;
                    if (oResper.Cod_Grupo == 1)
                    {
                        #region Perfil 1

                        Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Ventas");

                        //Combinacion de celdas
                        oWsUna.Cells["A1:P1"].Merge = true;
                        oWsUna.Cells["Q1:S1"].Merge = true;
                        oWsUna.Cells["T1:V1"].Merge = true;
                        oWsUna.Cells["W1:Z1"].Merge = true;
                        oWsUna.Cells["AA1:AC1"].Merge = true;
                        oWsUna.Cells["AD1:AQ1"].Merge = true;
                        oWsUna.Cells["AR1:AS1"].Merge = true;

                        //Dando valor a celdas combinadas
                        oWsUna.Cells["A1:P1"].Value = "VENTAS";
                        oWsUna.Cells["Q1:S1"].Value = "FUERZA VENTAS";
                        oWsUna.Cells["T1:V1"].Value = "TRADE MARKETING";
                        
                        oWsUna.Cells["W1:Z1"].Value = "LUCKY";
                        oWsUna.Cells["AA1:AC1"].Value = "TRADE MAKETING";
                        oWsUna.Cells["AD1:AQ1"].Value = "LUCKY";
                        oWsUna.Cells["AR1:AS1"].Value = "TRADE MARKETING";

                        //Dando valor a celdas individuales
                        //COMIENZA PRIMERA PARTE VENTAS
                        oWsUna.Cells["A2"].Value = "Usuario de Solicitud";
                        oWsUna.Cells["B2"].Value = "Fecha de Solicitud";
                        oWsUna.Cells["C2"].Value = "Punto de Venta";
                        oWsUna.Cells["D2"].Value = "Codigo de PDV";
                        oWsUna.Cells["E2"].Value = "Departamento";
                        oWsUna.Cells["F2"].Value = "Provincia";
                        oWsUna.Cells["G2"].Value = "Distrito";
                        oWsUna.Cells["H2"].Value = "Direccion";
                        oWsUna.Cells["I2"].Value = "Distribuidora";
                        oWsUna.Cells["J2"].Value = "Zona";
                        oWsUna.Cells["K2"].Value = "RUC";
                        oWsUna.Cells["L2"].Value = "Celular";
                        oWsUna.Cells["M2"].Value = "Contacto";
                        oWsUna.Cells["N2"].Value = "Marca";
                        oWsUna.Cells["O2"].Value = "Tipo";
                        oWsUna.Cells["P2"].Value = "Estado de Solicitud";
                        //FIN PRIMERA PARTE VENTAS
                        //COMIENZA PRIMERA FUERZA DE VENTAS
                        oWsUna.Cells["Q2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["R2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["S2"].Value = "Comentario";

                        //COMIENZA PRIMERA PARTE TRADE
                        oWsUna.Cells["T2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["U2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["V2"].Value = "Comentario";
                        //FIN PRIMERA PARTE TRADE
                        //COMINZA PRIMERA PARTE LUCKYANALIS
                        oWsUna.Cells["W2"].Value = "Fecha de Visita";
                        oWsUna.Cells["X2"].Value = "Comentario";
                        oWsUna.Cells["Y2"].Value = "Fotomontaje";
                        oWsUna.Cells["Z2"].Value = "Monto (S/.)";
                        //FIN PRIMERA PARTE LUCKYANALIS
                        //COMIENZA SEGUNDA PARTE VENTAS
                        oWsUna.Cells["AA2"].Value = "Aprobacion de Fotomontaje";
                        oWsUna.Cells["AB2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["AC2"].Value = "Comentario";
                        oWsUna.Cells["AD2"].Value = "Carta de Aprobacion";
                        //FIN SEGUNDA PARTE VENTAS
                        //COMINEZA SEGUNDA PARTE TRADE
                        oWsUna.Cells["AE2"].Value = "Aprobacion de Pre-Presupuesto";
                        oWsUna.Cells["AF2"].Value = "Fecha de Aprobacion";
                        //FIN SEGUNDA PARTE TRADE
                        //COMENZA SEGUNDA PARTE LUCKYANALIS
                        oWsUna.Cells["AG2"].Value = "Fecha de implementacion";
                        oWsUna.Cells["AH2"].Value = "Programacion y Status";
                        oWsUna.Cells["AI2"].Value = "Tipo de Tramite";
                        oWsUna.Cells["AJ2"].Value = "Tiempo de Permiso";
                        oWsUna.Cells["AK2"].Value = "Fecha de Ingreso de Expediente";
                        oWsUna.Cells["AL2"].Value = "Recibo de Tramite Municipal";
                        oWsUna.Cells["AM2"].Value = "Observacion";
                        oWsUna.Cells["AN2"].Value = "Fecha de Entrega de Permiso";
                        oWsUna.Cells["AO2"].Value = "Fecha de Caducidad de Permiso";
                        oWsUna.Cells["AP2"].Value = "Permiso";
                        oWsUna.Cells["AQ2"].Value = "Reporte Fotografico";
                        //FIN SEGUNDA PARTE LUCKYANALIS
                        //COMIENZA TERCERA PARTE TRADE
                        oWsUna.Cells["AR2"].Value = "Validacion de Fachada";
                        oWsUna.Cells["AS2"].Value = "Fecha de Validacion";
                        //FIN TERCERA PARTE TRADE


                        _fila = 3;

                        foreach (E_Fachada_Unacem oBj in oWfUna)
                        {
                            oWsUna.Cells[_fila, 1].Value = oBj.P1_Usuario_Solicita;
                            oWsUna.Cells[_fila, 2].Value = oBj.Fec_Solicitud;
                            oWsUna.Cells[_fila, 3].Value = oBj.Nom_PDV;
                            oWsUna.Cells[_fila, 4].Value = oBj.Codigo;
                            oWsUna.Cells[_fila, 5].Value = oBj.Departamento;
                            oWsUna.Cells[_fila, 6].Value = oBj.Provincia;
                            oWsUna.Cells[_fila, 7].Value = oBj.Distrito;
                            oWsUna.Cells[_fila, 8].Value = oBj.Direccion;
                            oWsUna.Cells[_fila, 9].Value = oBj.Distribuidora;
                            oWsUna.Cells[_fila, 10].Value = oBj.DescripcionZona;
                            oWsUna.Cells[_fila, 11].Value = oBj.Num_RUC;
                            oWsUna.Cells[_fila, 12].Value = oBj.Num_Cel;
                            oWsUna.Cells[_fila, 13].Value = oBj.Contacto;
                            oWsUna.Cells[_fila, 14].Value = oBj.DescripcionMarca;
                            oWsUna.Cells[_fila, 15].Value = oBj.DescripcionTipoSolicitud;
                            oWsUna.Cells[_fila, 16].Value = oBj.Est_Solicitud;
                            //oWsUna.Cells[_fila, 17].Value = oBj.Est_Proc_Solicitud;
                            switch (oBj.Apro_Solicitud.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 17].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 17].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 18].Value = oBj.Fec_Solicitud;
                            oWsUna.Cells[_fila, 19].Value = oBj.P1_Comentarios;
                            switch (oBj.Apro_Solicitud.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 20].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 20].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 21].Value = oBj.Fec_Apro_Solicitud;
                            oWsUna.Cells[_fila, 22].Value = oBj.Comen_Apro_Solicitud;
                            oWsUna.Cells[_fila, 23].Value = oBj.P2_Fecha_Entrega_Foto;
                            oWsUna.Cells[_fila, 24].Value = oBj.comen_expediente;
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                oWsUna.Cells[_fila, 25].Value = "Con foto";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 25].Value = "";
                            }
                            oWsUna.Cells[_fila, 26].Value = oBj.p3_monto_presupuesto;
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                switch (oBj.Apro_Fotomontaje.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 27].Value = "Aprobado";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 27].Value = "Desaprobado";
                                        break;
                                }
                            }
                            oWsUna.Cells[_fila, 28].Value = oBj.Fec_Apro_Fotomonataje;
                            oWsUna.Cells[_fila, 29].Value = oBj.Comen_Apro_Fotomontaje;
                            if (oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje.Length > 0)
                            {
                                if (oBj.Url_Carta_Fotomontaje != null && oBj.Url_Carta_Fotomontaje.Replace(" ", "").Length > 0)
                                {
                                    oWsUna.Cells[_fila, 30].Value = "Con carta";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 30].Value = "Falta cargar carta";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 30].Value = "";
                            }
                            switch (oBj.Apro_pre_Presu.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 31].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 31].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 32].Value = oBj.Fec_Apro_pre_Presu;
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 33].Value = oBj.fecha_implementacion;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 33].Value = "";
                            }
                            string _vcolor = "";
                            if (oBj.Cantidad == 0)
                            {
                                _vcolor = "Pendientes";
                            }
                            else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                            {
                                _vcolor = "Algunos pendientes";
                            }
                            else if (oBj.Cantidad == 10)
                            {
                                _vcolor = "Revisado";
                            }
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 34].Value = _vcolor;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 34].Value = _vcolor;
                            }
                            oWsUna.Cells[_fila, 35].Value = oBj.DescripcionTipoTramite;
                            oWsUna.Cells[_fila, 36].Value = oBj.DescripcionTiempoPermiso;
                            if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                            {
                                oWsUna.Cells[_fila, 37].Value = oBj.Fec_ingre_expe;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 38].Value = "Con recibo";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 38].Value = "";
                                }
                                oWsUna.Cells[_fila, 39].Value = oBj.Obs_Tramite;
                                if (oBj.Fec_Entre_Licencia != "" && oBj.Fec_Entre_Licencia != "01/01/1900")
                                {
                                    oWsUna.Cells[_fila, 40].Value = oBj.Fec_Entre_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 40].Value = "";
                                }
                                if (oBj.Fec_Cadu_Licencia != "" && oBj.Fec_Cadu_Licencia != "01/01/1900")
                                {
                                    oWsUna.Cells[_fila, 41].Value = oBj.Fec_Cadu_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 41].Value = "";
                                }
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 42].Value = "Con Permiso";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 42].Value = "";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 37].Value = "";
                                oWsUna.Cells[_fila, 38].Value = "";
                                oWsUna.Cells[_fila, 39].Value = "";
                                oWsUna.Cells[_fila, 40].Value = "";
                                oWsUna.Cells[_fila, 41].Value = "";
                                oWsUna.Cells[_fila, 42].Value = "";
                            }
                            if (oBj.Url_Report_Fotografico != null && oBj.Url_Report_Fotografico != "")
                            {
                                oWsUna.Cells[_fila, 43].Value = "Con reporte fotografico";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 43].Value = "";
                            }

                            switch (oBj.Vali_Fechada_Imple.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 44].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 44].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 45].Value = oBj.Fec_Vali_Fachada_Imple;



                            for (int i = 1; i <= columnsuser1; i++)
                            {
                                oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                //ajustando texto
                                oWsUna.Cells[_fila, i].Style.WrapText = true;
                            }



                            _fila++;
                        }

                        //Formato Cabecera 1
                        oWsUna.Row(1).Height = 25;
                        oWsUna.Row(1).Style.Font.Bold = true;
                        oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                        oWsUna.Cells["A1:AS1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A1:AS1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                        //Formato Cabecera 2
                        oWsUna.Row(2).Height = 20;
                        oWsUna.Row(2).Style.Font.Bold = true;
                        oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                        oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                        oWsUna.Cells["A2:AS2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A2:AS2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                        oWsUna.SelectedRange["A2:AS2"].AutoFilter = true;


                        //Formato ambas cabeceras
                        oWsUna.Cells["A1:AS2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AS2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AS2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AS2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;




                        //Formato por columnas
                        //Ventas
                        oWsUna.Column(1).Width = 21;
                        oWsUna.Column(2).Width = 19;
                        oWsUna.Column(3).Width = 30;
                        oWsUna.Column(4).Width = 10;
                        oWsUna.Column(5).Width = 18;
                        oWsUna.Column(6).Width = 13;
                        oWsUna.Column(7).Width = 11;
                        oWsUna.Column(8).Width = 17;
                        oWsUna.Column(9).Width = 13;
                        oWsUna.Column(10).Width = 17;
                        oWsUna.Column(11).Width = 13;
                        oWsUna.Column(12).Width = 15;
                        oWsUna.Column(13).Width = 17;
                        oWsUna.Column(14).Width = 25;
                        oWsUna.Column(15).Width = 35;
                        oWsUna.Column(16).Width = 20;
                        //trade marketing
                        oWsUna.Column(17).Width = 20;
                        oWsUna.Column(18).Width = 22;
                        oWsUna.Column(19).Width = 35;
                        //lucky
                        oWsUna.Column(20).Width = 22;
                        oWsUna.Column(21).Width = 35;
                        oWsUna.Column(22).Width = 13;
                        oWsUna.Column(23).Width = 18;
                        //ventas
                        oWsUna.Column(24).Width = 28;
                        oWsUna.Column(25).Width = 22;
                        oWsUna.Column(26).Width = 35;
                        oWsUna.Column(27).Width = 21;
                        //trade marketing
                        oWsUna.Column(28).Width = 32;
                        oWsUna.Column(29).Width = 22;
                        //lucky
                        oWsUna.Column(30).Width = 26;
                        oWsUna.Column(31).Width = 26;
                        oWsUna.Column(32).Width = 23;
                        oWsUna.Column(33).Width = 23;
                        oWsUna.Column(34).Width = 32;
                        oWsUna.Column(35).Width = 29;
                        oWsUna.Column(36).Width = 35;
                        oWsUna.Column(37).Width = 29;
                        oWsUna.Column(38).Width = 31;
                        oWsUna.Column(39).Width = 10;
                        oWsUna.Column(40).Width = 21;
                        //trade marketing
                        oWsUna.Column(41).Width = 23;
                        oWsUna.Column(42).Width = 21;

                        oWsUna.Column(43).Width = 35;
                        oWsUna.Column(44).Width = 35;
                        oWsUna.Column(45).Width = 35;
                     
                        //Columnas auto ajustadas
                        /*oWsUna.Column(1).AutoFit();*/

                        oWsUna.View.FreezePanes(3, 5);


                        oEx.Save();

                        #endregion
                    }
                    else if (oResper.Cod_Grupo == 2 || oResper.Cod_Grupo == 5 || oResper.Cod_Grupo == 3)
                    {
                        #region Perfil 2

                        Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Trade");

                        //Combinacion de celdas
                        oWsUna.Cells["A1:P1"].Merge = true;
                        oWsUna.Cells["Q1:S1"].Merge = true;
                        oWsUna.Cells["T1:V1"].Merge = true;
                        oWsUna.Cells["W1:AA1"].Merge = true;
                        oWsUna.Cells["AB1:AE1"].Merge = true;
                        oWsUna.Cells["AF:AU1"].Merge = true;
                        oWsUna.Cells["AV1:AW1"].Merge = true;

                        //Dando valor a celdas combinadas
                        oWsUna.Cells["A1:P1"].Value = "VENTAS";
                        oWsUna.Cells["Q1:S1"].Value = "FUERZA DE VENTAS";
                        oWsUna.Cells["T1:V1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["W1:AA1"].Value = "LUCKY";
                        oWsUna.Cells["AB1:AE1"].Value = "TRADE MARKETING";
                        oWsUna.Cells["AF1:AU1"].Value = "LUCKY";
                        oWsUna.Cells["AV1:AW1"].Value = "TRADE MARKETING";

                        //Dando valor a celdas individuales
                        //COMIENZA PRIMERA PARTE VENTAS
                        oWsUna.Cells["A2"].Value = "Usuario de Solicitud";
                        oWsUna.Cells["B2"].Value = "Fecha de Solicitud";
                        oWsUna.Cells["C2"].Value = "Punto de Venta";
                        oWsUna.Cells["D2"].Value = "Codigo de PDV";
                        oWsUna.Cells["E2"].Value = "Departamento";
                        oWsUna.Cells["F2"].Value = "Provincia";
                        oWsUna.Cells["G2"].Value = "Distrito";
                        oWsUna.Cells["H2"].Value = "Direccion";
                        oWsUna.Cells["I2"].Value = "Distribuidora";
                        oWsUna.Cells["J2"].Value = "Zona";
                        oWsUna.Cells["K2"].Value = "RUC";
                        oWsUna.Cells["L2"].Value = "Celular";
                        oWsUna.Cells["M2"].Value = "Contacto";
                        oWsUna.Cells["N2"].Value = "Marca";
                        oWsUna.Cells["O2"].Value = "Tipo";
                        oWsUna.Cells["P2"].Value = "Estado de Solicitud";
                        //FIN PRIMERA PARTE VENTAS
                        //COMIENZA PRIMERA PARTE TRADE
                        oWsUna.Cells["Q2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["R2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["S2"].Value = "Comentario";
                        oWsUna.Cells["T2"].Value = "Estado de Solicitud";
                        oWsUna.Cells["U2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["V2"].Value = "Comentario";
                        //FIN PRIMERA PARTE TRADE
                        //COMIENZA PRIMERA PARTE LUCKYANALIS
                        oWsUna.Cells["W2"].Value = "Fecha de Visita";
                        oWsUna.Cells["X2"].Value = "Comentario";
                        oWsUna.Cells["Y2"].Value = "Fotomontaje";
                        oWsUna.Cells["Z2"].Value = "Monto (S/.)";
                        oWsUna.Cells["AA2"].Value = "Pre-Presupuesto";
                        //FIN PRIMERA PARTE LUCKYANALIS
                        //COMIENZA SEGUNDA PARTE VENTAS
                        oWsUna.Cells["AB2"].Value = "Aprobacion de Fotomontaje";
                        oWsUna.Cells["AC2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["AD2"].Value = "Comentario";
                        oWsUna.Cells["AE2"].Value = "Aprobacion de Pre-Presupuesto";
                        oWsUna.Cells["AF2"].Value = "Carta de Aprobacion";
                        //FIN SEGUNDA PARTE VENTAS
                        //COMIENZA SEGUNDA PARTE TRADE
                        
                        oWsUna.Cells["AG2"].Value = "Fecha de Aprobacion";
                        oWsUna.Cells["AH2"].Value = "Orden de Compra";
                        oWsUna.Cells["AI2"].Value = "H.E.S.";
                        oWsUna.Cells["AJ2"].Value = "Comentario";
                        //FIN SEGUNDA PARTE TRADE
                        //COMIENZA SEGUNDA PARTE LUCKYANALIS
                        oWsUna.Cells["AK2"].Value = "Fecha de implementacion";
                        oWsUna.Cells["AL2"].Value = "Programacion y Status";
                        oWsUna.Cells["AM2"].Value = "Tipo de Tramite";
                        oWsUna.Cells["AN2"].Value = "Tiempo de Permiso";
                        oWsUna.Cells["AO2"].Value = "Fecha de Ingreso de Expediente";
                        oWsUna.Cells["AP2"].Value = "Recibo de Tramite Municipal";
                        oWsUna.Cells["AQ2"].Value = "Observacion";
                        oWsUna.Cells["AR2"].Value = "Fecha de Entrega de Permiso";
                        oWsUna.Cells["AS2"].Value = "Fecha de Caducidad de Permiso";
                        oWsUna.Cells["AT2"].Value = "Permiso";
                        oWsUna.Cells["AU2"].Value = "Reporte Fotografico";
                        //FIN SEGUNDA PARTE LUCKYANALIS
                        //COMIENZA TERCERA PARTE TRADE
                        oWsUna.Cells["AV2"].Value = "Validacion de Fachada";
                        oWsUna.Cells["AW2"].Value = "Fecha de Validacion";
                        //FIN TERCERA PARTE TRADE


                        _fila = 3;

                        foreach (E_Fachada_Unacem oBj in oWfUna)
                        {
                            //COMIENZA PRIMERA PARTE VENTAS
                            oWsUna.Cells[_fila, 1].Value = oBj.P1_Usuario_Solicita;
                            oWsUna.Cells[_fila, 2].Value = oBj.Fec_Solicitud;
                            oWsUna.Cells[_fila, 3].Value = oBj.Nom_PDV;
                            oWsUna.Cells[_fila, 4].Value = oBj.Codigo;
                            oWsUna.Cells[_fila, 5].Value = oBj.Departamento;
                            oWsUna.Cells[_fila, 6].Value = oBj.Provincia;
                            oWsUna.Cells[_fila, 7].Value = oBj.Distrito;
                            oWsUna.Cells[_fila, 8].Value = oBj.Direccion;
                            oWsUna.Cells[_fila, 9].Value = oBj.Distribuidora;
                            oWsUna.Cells[_fila, 10].Value = oBj.DescripcionZona;
                            oWsUna.Cells[_fila, 11].Value = oBj.Num_RUC;
                            oWsUna.Cells[_fila, 12].Value = oBj.Num_Cel;
                            oWsUna.Cells[_fila, 13].Value = oBj.Contacto;
                            oWsUna.Cells[_fila, 14].Value = oBj.DescripcionMarca;
                            oWsUna.Cells[_fila, 15].Value = oBj.DescripcionTipoSolicitud;
                            oWsUna.Cells[_fila, 16].Value = oBj.Est_Solicitud;
                            switch (oBj.Apro_Solicitud.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 17].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 17].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 18].Value = oBj.Fec_Solicitud;
                            oWsUna.Cells[_fila, 19].Value = oBj.P1_Comentarios;
                            
                            //TERMINA PRIMERA PARTE VENTAS
                            //COMIENZA PRIMERA PARTE TRADE
                            switch (oBj.Apro_Solicitud.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 20].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 20].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 21].Value = oBj.Fec_Apro_Solicitud;
                            oWsUna.Cells[_fila, 22].Value = oBj.Comen_Apro_Solicitud;
                            //TERMINA PRIMERA PARTE TRADE
                            //COMIENZA PRIMERA PARTE LUCKYANALIS
                            oWsUna.Cells[_fila, 23].Value = oBj.P2_Fecha_Entrega_Foto;
                            oWsUna.Cells[_fila, 24].Value = oBj.comen_expediente;
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                oWsUna.Cells[_fila, 25].Value = "Con foto";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 25].Value = "";
                            }
                            oWsUna.Cells[_fila, 26].Value = oBj.p3_monto_presupuesto;
                            if (oBj.Url_Presupuesto != "" && oBj.Url_Presupuesto != null)
                            {
                                oWsUna.Cells[_fila, 27].Value = "Con presupuesto";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 27].Value = "";
                            }
                            if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                            {
                                switch (oBj.Apro_Fotomontaje.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 28].Value = "Aprobado";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 28].Value = "Desaprobado";
                                        break;
                                }
                            }
                            oWsUna.Cells[_fila, 29].Value = oBj.Fec_Apro_Fotomonataje;
                            oWsUna.Cells[_fila, 30].Value = oBj.Comen_Apro_Fotomontaje;
                            if (oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje.Length > 0)
                            {
                                if (oBj.Url_Carta_Fotomontaje != null && oBj.Url_Carta_Fotomontaje.Replace(" ", "").Length > 0)
                                {
                                    oWsUna.Cells[_fila, 31].Value = "Con carta";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 31].Value = "Falta cargar carta";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 31].Value = "";
                            }
                            switch (oBj.Apro_pre_Presu.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 32].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 32].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 33].Value = oBj.Fec_Apro_pre_Presu;
                            oWsUna.Cells[_fila, 34].Value = oBj.Orden_Compra;
                            oWsUna.Cells[_fila, 35].Value = oBj.HES;
                            oWsUna.Cells[_fila, 36].Value = oBj.Comen_Apro_Pre_Presu;
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 37].Value = oBj.fecha_implementacion;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 37].Value = "";
                            }
                            string _vcolor = "";
                            if (oBj.Cantidad == 0)
                            {
                                _vcolor = "Pendientes";
                            }
                            else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                            {
                                _vcolor = "Algunos pendientes";
                            }
                            else if (oBj.Cantidad == 10)
                            {
                                _vcolor = "Revisado";
                            }
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 38].Value = _vcolor;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 38].Value = _vcolor;
                            }
                            oWsUna.Cells[_fila, 39].Value = oBj.DescripcionTipoTramite;
                            oWsUna.Cells[_fila, 40].Value = oBj.DescripcionTiempoPermiso;
                            if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                            {
                                oWsUna.Cells[_fila, 41].Value = oBj.Fec_ingre_expe;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 42].Value = "Con recibo";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 42].Value = "";
                                }
                                oWsUna.Cells[_fila, 43].Value = oBj.Obs_Tramite;
                                if (oBj.Fec_Entre_Licencia != "" && oBj.Fec_Entre_Licencia != "01/01/1900")
                                {
                                    oWsUna.Cells[_fila, 44].Value = oBj.Fec_Entre_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 44].Value = "";
                                }
                                if (oBj.Fec_Cadu_Licencia != "" && oBj.Fec_Cadu_Licencia != "01/01/1900")
                                {
                                    oWsUna.Cells[_fila, 45].Value = oBj.Fec_Cadu_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 45].Value = "";
                                }
                                if (oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                {
                                    oWsUna.Cells[_fila, 46].Value = "Con Permiso";

                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 46].Value = "";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 41].Value = "";
                                oWsUna.Cells[_fila, 42].Value = "";
                                oWsUna.Cells[_fila, 43].Value = "";
                                oWsUna.Cells[_fila, 44].Value = "";
                                oWsUna.Cells[_fila, 45].Value = "";
                                oWsUna.Cells[_fila, 46].Value = "";
                            }
                            if (oBj.Url_Report_Fotografico != null && oBj.Url_Report_Fotografico != "")
                            {
                                oWsUna.Cells[_fila, 47].Value = "Con reporte fotografico";
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 47].Value = "";
                            }
                            switch (oBj.Vali_Fechada_Imple.ToUpper())
                            {
                                case "TRUE":
                                    oWsUna.Cells[_fila, 48].Value = "Aprobado";
                                    break;
                                case "FALSE":
                                    oWsUna.Cells[_fila, 48].Value = "Desaprobado";
                                    break;
                            }
                            oWsUna.Cells[_fila, 49].Value = oBj.Fec_Vali_Fachada_Imple;



                            for (int i = 1; i <= columnsuser2; i++)
                            {
                                oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                //ajustando texto
                                oWsUna.Cells[_fila, i].Style.WrapText = true;
                            }



                            _fila++;
                        }


                        //Formato Cabecera 1
                        oWsUna.Row(1).Height = 25;
                        oWsUna.Row(1).Style.Font.Bold = true;
                        oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                        oWsUna.Cells["A1:AW1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A1:AW1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                        //Formato Cabecera 2
                        oWsUna.Row(2).Height = 20;
                        oWsUna.Row(2).Style.Font.Bold = true;
                        oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                        oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                        oWsUna.Cells["A2:AW2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A2:AW2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                        oWsUna.SelectedRange["A2:AW2"].AutoFilter = true;


                        //Formato ambas cabeceras
                        oWsUna.Cells["A1:AW2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AW2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AW2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:AW2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;




                        //Formato por columnas
                        //Ventas
                        oWsUna.Column(1).Width = 21;
                        oWsUna.Column(2).Width = 19;
                        oWsUna.Column(3).Width = 30;
                        oWsUna.Column(4).Width = 17;
                        oWsUna.Column(5).Width = 20;
                        oWsUna.Column(6).Width = 18;
                        oWsUna.Column(7).Width = 18;
                        oWsUna.Column(8).Width = 35;
                        oWsUna.Column(9).Width = 35;
                        oWsUna.Column(10).Width = 35;
                        oWsUna.Column(11).Width = 35;
                        oWsUna.Column(12).Width = 35;
                        oWsUna.Column(13).Width = 35;
                        oWsUna.Column(14).Width = 35;
                        oWsUna.Column(15).Width = 35;
                        oWsUna.Column(16).Width = 20;
                        //trade marketing
                        oWsUna.Column(17).Width = 20;
                        oWsUna.Column(18).Width = 22;
                        oWsUna.Column(19).Width = 35;
                        //lucky
                        oWsUna.Column(20).Width = 22;
                        oWsUna.Column(21).Width = 35;
                        oWsUna.Column(22).Width = 13;
                        oWsUna.Column(23).Width = 13;
                        oWsUna.Column(24).Width = 18;
                        //ventas
                        oWsUna.Column(25).Width = 28;
                        oWsUna.Column(26).Width = 22;
                        oWsUna.Column(27).Width = 35;
                        oWsUna.Column(28).Width = 21;
                        //trade marketing
                        oWsUna.Column(29).Width = 32;
                        oWsUna.Column(30).Width = 22;
                        oWsUna.Column(31).Width = 22;
                        oWsUna.Column(32).Width = 22;
                        oWsUna.Column(33).Width = 35;
                        //lucky
                        oWsUna.Column(34).Width = 26;
                        oWsUna.Column(35).Width = 23;
                        oWsUna.Column(36).Width = 23;
                        oWsUna.Column(37).Width = 23;
                        oWsUna.Column(38).Width = 32;
                        oWsUna.Column(39).Width = 29;
                        oWsUna.Column(40).Width = 35;
                        oWsUna.Column(41).Width = 29;
                        oWsUna.Column(42).Width = 31;
                        oWsUna.Column(43).Width = 10;
                        oWsUna.Column(44).Width = 21;
                        //trade marketing
                        oWsUna.Column(45).Width = 23;
                        oWsUna.Column(46).Width = 21;

                        oWsUna.Column(44).Width = 21;
                        //trade marketing
                        oWsUna.Column(45).Width = 23;
                        oWsUna.Column(46).Width = 21;
                        //Columnas auto ajustadas
                        /*oWsUna.Column(1).AutoFit();*/

                        oWsUna.View.FreezePanes(3, 5);


                        oEx.Save();

                        #endregion
                    }
                   
                    else if (oResper.Cod_Grupo == 4)
                    {
                        #region Perfil 4

                        Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Tramite");

                        //Combinacion de celdas
                        oWsUna.Cells["A1:P1"].Merge = true;
                        oWsUna.Cells["Q1:Z1"].Merge = true;

                        //Dando valor a celdas combinadas
                        oWsUna.Cells["A1:P1"].Value = "VENTAS";
                        oWsUna.Cells["Q1:Z1"].Value = "LUCKY";

                        //Dando valor a celdas individuales
                        //COMIENZA PRIMERA PARTE VENTAS
                        oWsUna.Cells["A2"].Value = "Usuario de Solicitud";
                        oWsUna.Cells["B2"].Value = "Fecha de Solicitud";
                        oWsUna.Cells["C2"].Value = "Punto de Venta";
                        oWsUna.Cells["D2"].Value = "Codigo de PDV";
                        oWsUna.Cells["E2"].Value = "Departamento";
                        oWsUna.Cells["F2"].Value = "Provincia";
                        oWsUna.Cells["G2"].Value = "Distrito";
                        oWsUna.Cells["H2"].Value = "Direccion";
                        oWsUna.Cells["I2"].Value = "Distribuidora";
                        oWsUna.Cells["J2"].Value = "Zona";
                        oWsUna.Cells["K2"].Value = "RUC";
                        oWsUna.Cells["L2"].Value = "Celular";
                        oWsUna.Cells["M2"].Value = "Contacto";
                        oWsUna.Cells["N2"].Value = "Marca";
                        oWsUna.Cells["O2"].Value = "Tipo";
                        oWsUna.Cells["P2"].Value = "Estado de Solicitud";
                        //TERMINA PRIMERA PARTE VENTAS
                        //COMIENZA PRIMERA PARTE LUCKYANALIS
                        oWsUna.Cells["Q2"].Value = "Fecha de implementacion";
                        oWsUna.Cells["R2"].Value = "Programacion y Status";
                        oWsUna.Cells["S2"].Value = "Tipo de Tramite";
                        oWsUna.Cells["T2"].Value = "Tiempo de Permiso";
                        oWsUna.Cells["U2"].Value = "Fecha de Ingreso de Expediente";
                        oWsUna.Cells["V2"].Value = "Recibo de Tramite Municipal";
                        oWsUna.Cells["W2"].Value = "Observacion";
                        oWsUna.Cells["X2"].Value = "Fecha de Entrega de Permiso";
                        oWsUna.Cells["Y2"].Value = "Fecha de Caducidad de Permiso";
                        oWsUna.Cells["Z2"].Value = "Permiso";
                        //TERMINA PRIMERA PARTE LUCKYANALIS


                        _fila = 3;

                        foreach (E_Fachada_Unacem oBj in oWfUna)
                        {
                            //COMIENZA PRIMERA PARTE VENTAS
                            oWsUna.Cells[_fila, 1].Value = oBj.P1_Usuario_Solicita;
                            oWsUna.Cells[_fila, 2].Value = oBj.Fec_Solicitud;
                            oWsUna.Cells[_fila, 3].Value = oBj.Nom_PDV;
                            oWsUna.Cells[_fila, 4].Value = oBj.Codigo;
                            oWsUna.Cells[_fila, 5].Value = oBj.Departamento;
                            oWsUna.Cells[_fila, 6].Value = oBj.Provincia;
                            oWsUna.Cells[_fila, 7].Value = oBj.Distrito;
                            oWsUna.Cells[_fila, 8].Value = oBj.Direccion;
                            oWsUna.Cells[_fila, 9].Value = oBj.Distribuidora;
                            oWsUna.Cells[_fila, 10].Value = oBj.DescripcionZona;
                            oWsUna.Cells[_fila, 11].Value = oBj.Num_RUC;
                            oWsUna.Cells[_fila, 12].Value = oBj.Num_Cel;
                            oWsUna.Cells[_fila, 13].Value = oBj.Contacto;
                            oWsUna.Cells[_fila, 14].Value = oBj.DescripcionMarca;
                            oWsUna.Cells[_fila, 15].Value = oBj.DescripcionTipoSolicitud;
                            oWsUna.Cells[_fila, 16].Value = oBj.Est_Solicitud;
                            //TERMINA PRIMERA PARTE VENTAS
                            //COMIENZA PRIMERA PARTE LUCKYANALIS
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 17].Value = oBj.fecha_implementacion;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 17].Value = "";
                            }
                            string _vcolor = "";
                            if (oBj.Cantidad == 0)
                            {
                                _vcolor = "Pendientes";
                            }
                            else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                            {
                                _vcolor = "Algunos pendientes";
                            }
                            else if (oBj.Cantidad == 10)
                            {
                                _vcolor = "Revisado";
                            }
                            if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                            {
                                oWsUna.Cells[_fila, 18].Value = _vcolor;
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 18].Value = _vcolor;
                            }
                            oWsUna.Cells[_fila, 19].Value = oBj.DescripcionTipoTramite;
                            oWsUna.Cells[_fila, 20].Value = oBj.DescripcionTiempoPermiso;
                            if (ViewBag.cod_grupo == 4 && oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                            {
                                if (oBj.Fec_ingre_expe != null && oBj.Fec_ingre_expe != "" && oBj.Fec_ingre_expe != "01/01/1900" && oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 21].Value = oBj.Fec_ingre_expe;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 21].Value = "__________";
                                }
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 22].Value = "Con recibo";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 22].Value = "__________";
                                }
                                if (oBj.Obs_Tramite != null && oBj.Obs_Tramite != "")
                                {
                                    oWsUna.Cells[_fila, 23].Value = oBj.Obs_Tramite;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 23].Value = "__________";
                                }
                                if (oBj.Fec_Entre_Licencia != null && oBj.Fec_Entre_Licencia != "" && oBj.Fec_Entre_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                {
                                    oWsUna.Cells[_fila, 24].Value = oBj.Fec_Entre_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 24].Value = "__________";
                                }
                                if (oBj.Fec_Cadu_Licencia != null && oBj.Fec_Cadu_Licencia != "" && oBj.Fec_Cadu_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                {
                                    oWsUna.Cells[_fila, 25].Value = oBj.Fec_Cadu_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 25].Value = "__________";
                                }
                                if (oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                {
                                    oWsUna.Cells[_fila, 26].Value = "Con Permiso";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 26].Value = "__________";
                                }
                            }
                            else if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                            {
                                oWsUna.Cells[_fila, 21].Value = oBj.Fec_ingre_expe;
                                if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                {
                                    oWsUna.Cells[_fila, 22].Value = "Con recibo";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 22].Value = "";
                                }
                                oWsUna.Cells[_fila, 23].Value = oBj.Obs_Tramite;
                                if (oBj.Fec_Entre_Licencia != "" && oBj.Fec_Entre_Licencia != "01/01/1900")
                                {
                                    oWsUna.Cells[_fila, 24].Value = oBj.Fec_Entre_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 24].Value = "";
                                }
                                if (oBj.Fec_Cadu_Licencia != "" && oBj.Fec_Cadu_Licencia != "01/01/1900")
                                {
                                    oWsUna.Cells[_fila, 25].Value = oBj.Fec_Cadu_Licencia;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 25].Value = "";
                                }
                                if (oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                {
                                    oWsUna.Cells[_fila, 26].Value = "Con Permiso";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 26].Value = "";
                                }
                            }
                            else
                            {
                                oWsUna.Cells[_fila, 21].Value = "";
                                oWsUna.Cells[_fila, 22].Value = "";
                                oWsUna.Cells[_fila, 23].Value = "";
                                oWsUna.Cells[_fila, 24].Value = "";
                                oWsUna.Cells[_fila, 25].Value = "";
                                oWsUna.Cells[_fila, 26].Value = "";
                            }




                            for (int i = 1; i <= columnsuser4; i++)
                            {
                                oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                //ajustando texto
                                oWsUna.Cells[_fila, i].Style.WrapText = true;
                            }



                            _fila++;
                        }

                        //Formato Cabecera 1
                        oWsUna.Row(1).Height = 25;
                        oWsUna.Row(1).Style.Font.Bold = true;
                        oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                        oWsUna.Cells["A1:Z1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A1:Z1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                        //Formato Cabecera 2
                        oWsUna.Row(2).Height = 20;
                        oWsUna.Row(2).Style.Font.Bold = true;
                        oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                        oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                        oWsUna.Cells["A2:Z2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A2:Z2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                        oWsUna.SelectedRange["A2:Z2"].AutoFilter = true;


                        //Formato ambas cabeceras
                        oWsUna.Cells["A1:Z2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:Z2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:Z2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:Z2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;




                        //Formato por columnas
                        //Ventas
                        oWsUna.Column(1).Width = 21;
                        oWsUna.Column(2).Width = 19;
                        oWsUna.Column(3).Width = 30;
                        oWsUna.Column(4).Width = 17;
                        oWsUna.Column(5).Width = 20;
                        oWsUna.Column(6).Width = 18;
                        oWsUna.Column(7).Width = 18;
                        oWsUna.Column(8).Width = 35;
                        oWsUna.Column(9).Width = 35;
                        oWsUna.Column(10).Width = 35;
                        oWsUna.Column(11).Width = 35;
                        oWsUna.Column(12).Width = 35;
                        oWsUna.Column(13).Width = 35;
                        oWsUna.Column(14).Width = 35;
                        oWsUna.Column(15).Width = 35;
                        oWsUna.Column(16).Width = 20;
                        //lucky
                        oWsUna.Column(17).Width = 26;
                        oWsUna.Column(18).Width = 23;
                        oWsUna.Column(19).Width = 23;
                        oWsUna.Column(20).Width = 23;
                        oWsUna.Column(21).Width = 32;
                        oWsUna.Column(22).Width = 29;
                        oWsUna.Column(23).Width = 35;
                        oWsUna.Column(24).Width = 29;
                        oWsUna.Column(25).Width = 31;
                        oWsUna.Column(26).Width = 10;

                        //Columnas auto ajustadas
                        /*oWsUna.Column(1).AutoFit();*/

                        oWsUna.View.FreezePanes(3, 5);


                        oEx.Save();

                        #endregion
                    }

                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        /// <summary>
        /// Autor:wlopez
        /// Fecha:09/12/2016
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        public ActionResult Eliminar_Archivo(int __a, string __b, int __c, int __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().DeleteArchive(
                    new Request_WF_General() { Id_Reg = __a, img = "", orden = 0, Cod_Op = __d }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult GetResumenPresuGrilla()
        {

            List<E_Resumen_Presupuesto> oLsWf = new WorkFlow().Consulta_Resumen_Presupuesto(
                new Request_WF_General()
                {
                    cod_equipo = Convert.ToString(Session["Session_Campania"])
                });

            return Json(new { Archivo = oLsWf });
        }

        public ActionResult Reg_Paso6_Lucky_TipoTramite(int __a, int __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso3_lucky_Tipo_Tramite(
                    new Request_WF_General() { Cod_Op = __a, Tipo_Tramite = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Reg_Paso6_Lucky_TipoPermiso(int __a, int __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso3_lucky_Tiempo_Permiso(
                    new Request_WF_General() { Cod_Op = __a, Tiempo_Permiso = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Carga_Img_FotoSolicitud(HttpPostedFileBase __f, int __a, int __b)
        {
            string _archivo = "";
            string _fileLocal = "";
            string _doc_url = "";

            if (__f != null)
            {
                _fileLocal = IO.Path.GetFileName(__f.FileName);
                _archivo = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                string vtipoarchi = _fileLocal.Substring(_fileLocal.LastIndexOf("."), 4);

                switch (vtipoarchi)
                {
                    case ".jpg":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".jpg"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".jpg";
                        break;
                    case ".png":
                        __f.SaveAs(IO.Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".png"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".png";
                        break;
                }
            }

            WorkFlow objService = new WorkFlow();
            E_WF_Fotomontaje objresponse = objService.AddUpt_FotoSolicitud(new Request_WF_General
            {
                Id_Reg = __a,
                img = _doc_url,
                orden = __b,
                Cod_Op = 4
            });
            var headerAcceptVariable1 = this.Request.Headers["ACCEPT"] as string;

            if (headerAcceptVariable1 != null && headerAcceptVariable1.Contains("application/json"))
            {
                return Json(_doc_url);
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var response = serializer.Serialize(_doc_url);
                return Content(response);
            }
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2016-12-22
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CargaFoto(HttpPostedFileBase FileImagenSolicitud)
        {
            string archivoIdentificardor;
            string archivoNombre;
            string pathArchivo;

            archivoIdentificardor = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now);
            archivoNombre = "Img_" + archivoIdentificardor + Path.GetExtension(FileImagenSolicitud.FileName);
            pathArchivo = Path.Combine(LocalFachada, "Img", archivoNombre);

            try
            {
                #region Crea imagen
                FileImagenSolicitud.SaveAs(pathArchivo);
                #endregion

                return Content(new ContentResult
                {
                    Content = "{ \"id\":\"" + archivoIdentificardor + "\",\"archivo\":\"" + archivoNombre + "\", \"estado\":true,\"mensaje\":\"Ok\" }",
                    ContentType = "application/json"
                }.Content);
            }
            catch (Exception e)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"estado\":false,\"mensaje\":\"" + e.Message + "\" }",
                    ContentType = "application/json"
                }.Content);
            }
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2016-12-22
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EliminaFoto(string ArchivoNombre) {
            //string path = Server.MapPath("~/Temp/Foto/Automecatronica");
            bool Estado = false;
            string Mensaje = "";
            string Ruta = Path.Combine(LocalFachada, "Img", ArchivoNombre);

            #region Elimina en directorio
            try
            {
                System.IO.File.Delete(Ruta);
                Estado = true;
                Mensaje = "Carga correcta.";
            }
            catch (Exception e)
            {
                Mensaje = e.Message;
                Estado = false;
            }
            #endregion

            return Content(new ContentResult
            {
                Content = "{ \"Resultado\":" + Estado.ToString().ToLower() + ", \"Mensaje\":\"" + Mensaje + "\" }",
                ContentType = "application/json"
            }.Content);
        }

        #region <<< WORD >>>

        private string Word_Insertar_Img(string nomdoc, string __a, string __b, string __c, string __d, string __e)
        {
            // __e : nombre de punto de venta
            object objMiss = System.Reflection.Missing.Value;
            Word.Application objWord = new Word.Application();

            objWord.Visible = false;
            objWord.DisplayAlerts = Word.WdAlertLevel.wdAlertsNone;

            try
            {
                string ruta = Path.Combine(LocalFachada, "Word/" + nomdoc);
                //string ruta2 = HttpContext.Server.MapPath("~/Temp/Fachada/Img/Img_13012016_030632.jpg");
                //string rtimg1 = HttpContext.Server.MapPath("~" + __a);
                //string rtimg2 = HttpContext.Server.MapPath("~" + __b);
                //string rtimg3 = HttpContext.Server.MapPath("~" + __c);
                //string rtimg4 = HttpContext.Server.MapPath("~" + __d);
                string rtimg1 = Path.Combine(new string[] { Local, __a.Replace("/Fachada", "Fachada") });
                string rtimg2 = Path.Combine(new string[] { Local, __b.Replace("/Fachada", "Fachada") });
                string rtimg3 = Path.Combine(new string[] { Local, __c.Replace("/Fachada", "Fachada") });
                string rtimg4 = Path.Combine(new string[] { Local, __d.Replace("/Fachada", "Fachada") });

                DateTime thisDay = DateTime.Today;
                string fecha_generacon = thisDay.ToString("D").Replace(thisDay.ToString("dddd", CultureInfo.CreateSpecificCulture("es-ES")), "Lima");

                //object parametro = ruta;

                Word.Document objDoc = objWord.Documents.Open(ruta);

                object nombre1 = "Img1";
                object nombre2 = "Img2";
                object nombre3 = "Img3";
                object nombre4 = "Img4";
                object nombre5 = "nompdv";
                object nombre6 = "fechagen";

                Word.Range nom = objDoc.Bookmarks.get_Item(ref nombre1).Range;
                Word.Range nom2 = objDoc.Bookmarks.get_Item(ref nombre2).Range;
                Word.Range nom3 = objDoc.Bookmarks.get_Item(ref nombre3).Range;
                Word.Range nom4 = objDoc.Bookmarks.get_Item(ref nombre4).Range;
                Word.Range nom5 = objDoc.Bookmarks.get_Item(ref nombre5).Range;
                Word.Range nom6 = objDoc.Bookmarks.get_Item(ref nombre6).Range;

                nom.Text = "";
                nom2.Text = "";
                nom3.Text = "";
                nom4.Text = "";
                nom5.Text = __e;
                nom6.Text = fecha_generacon;

                //object rango1 = nom;
                //objDoc.Bookmarks.Add("nombre",ref rango1);

                //objWord.InlineShapes.AddPicture();
                var obj1 = objDoc.InlineShapes.AddPicture(rtimg1, Type.Missing, Type.Missing, nom);
                var obj2 = objDoc.InlineShapes.AddPicture(rtimg2, Type.Missing, Type.Missing, nom2);
                var obj3 = objDoc.InlineShapes.AddPicture(rtimg3, Type.Missing, Type.Missing, nom3);
                var obj4 = objDoc.InlineShapes.AddPicture(rtimg4, Type.Missing, Type.Missing, nom4);

                obj1.Width = 150; obj1.Height = 100;
                obj2.Width = 150; obj2.Height = 100;
                obj3.Width = 150; obj3.Height = 100;
                obj4.Width = 150; obj4.Height = 100;

                objWord.Documents.Save();
                objWord.Documents.Close();
                objWord.Quit();
                objWord = null;

                GC.Collect();
                //objWord.Visible = false;
                //return "/Temp/Fachada/Word/" + nomdoc;
            }
            catch (System.IO.IOException)
            {
                objWord.Documents.Close();
                //objWord.Quit(ref saveChanges, ref Unknown, ref Unknown);
                objWord = null;
                GC.Collect();
            }
            return nomdoc;
        }
        private string Word_Generar()
        {

            string fileName = "Carta_de_conformidad_de_fotomontaje.docx";
            string vnom = String.Format("{0:yyyyMMdd_HHmmss}", DateTime.Now);
            //string vnom = String.Format("{0:ddMMyyyy}", DateTime.Now);
            string fileName2 = "Carta_" + vnom + ".doc";
            //string sourcePath = @"\Temp\Fachada\Word";
            //string targetPath = @"C:\Users\Public\TestFolder\SubDir";

            string sourceFile = System.IO.Path.Combine(LocalFachada, "Word", fileName);
            string destFile = System.IO.Path.Combine(LocalFachada, "Word", fileName2);
            //string destFile = System.IO.Path.Combine(targetPath, fileName);

            System.IO.File.Copy(sourceFile, destFile, true);

            return fileName2;
        }
        #endregion

        #region << Observaciones >>

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 13/06/2016
        /// Descripcion: 
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        public ActionResult WF_Una_Filtro(string __a, string __b, int __c)
        {
            ViewBag.cod_elemento = __a;
            return View(new WorkFlow()
                    .Filtro(new Resquest_New_XPL_Una_WF()
                    {
                        opcion = __c,
                        parametros = __b
                    }));
        }

        /// <summary>
        /// Autor: yrodriguez
        /// Fecha: 19/06/2016
        /// Descripcion: Permite Actualizar Fecha de Entrega en la malla de Fotomontaje
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        public JsonResult WF_Una_FechaEntrega_Fotomontaje(int __a, string __b)
        {
            E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
            StatusRegistro = new WorkFlow().Actualizar_FechaEntrega_Foto(
                   new Request_WF_DetailGantt()
                   {
                       Cod_Reg_Tabla = __a,
                       fecha_entrega = __b
                   });
            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult WF_Una_Fechaimplementacion(int __a, string __b)
        //{
        //    E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
        //    StatusRegistro = new WorkFlow().Actualizar_FechaImplementacion(
        //           new Request_WF_DetailGantt()
        //           {
        //               Cod_Reg_Tabla = __a,
        //               fecha_implementacion = __b
        //           });
        //    return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult WF_Una_Fechaimplementacion(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Actualizar_FechaImplementacion(
                    new Request_WF_General() { Cod_Op = __a, fecha_implementacion = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult Autocomplete(int opcion, string term, int tipo)
        {
            Request_Autocomplete_Solicitud oParametro = new Request_Autocomplete_Solicitud() { Opcion = opcion, Parametro = term, tipo = tipo };
            List<Autocomp_Nueva_Solicitud> lObj = new Autocomp_Nueva_Solicitud().ListaData_Nueva_solicitud(oParametro);


            return Json(lObj, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region grilla tramite

        public ActionResult GetGrillaTramite(int __a, string __b, string __c, string __d, string __e, string __f, string __g, int __h, string __i, string __j, string __k)
        {
            List<E_WF_Tramite> oLsTr = new WorkFlow().Consulta_Grilla_Tramite(
                new Request_WF_General()
                {
                    Id_Reg = __a
                        ,
                    fec_ing_expe = __b
                        ,
                    ruta_recibo = __c
                        ,
                    obs_recibo = __d
                        ,
                    fec_licencia = __e
                        ,
                    fec_vencimiento = __f
                        ,
                    ruta_licencia = __g
                        ,
                    Cod_Op = __h
                        ,
                    Monto_Presupuesto_Final = __i
                        ,
                    Codigo_Presupuesto_Final = __j
                        ,
                    Url_Presu_Final = __k
                });

            return Json(new { Archivo = oLsTr });
        }

        #endregion

        #endregion

        #region Reporting
        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2017-01-14
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Reporting()
        {
            ViewBag.cod_equipo = Request.QueryString["_a"];
            return View();
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2017-01-14
        /// </summary>
        /// <param name="opcion"></param>
        /// <param name="parametro"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Reporting(int opcion, string parametro)
        {
            if (opcion == 0 || opcion == 1)
            {
                #region Status fachada y Status Permiso
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new UnacemReporting().ListaStatusFachada(new UnacemReportingParametro()
                    {
                        opcion = opcion,
                        parametro = parametro
                    })),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (opcion == 2)
            {
                #region Status Presupuestal
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new UnacemReporting().ListaStatusPresupuestal(new UnacemReportingParametro()
                    {
                        opcion = opcion,
                        parametro = parametro
                    })),
                    ContentType = "application/json"
                }.Content);
                #endregion
           

            }

            return View();
        }

        public JsonResult ReportingExcel(int opcion, string parametro)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("WF_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte WorkFlow >>

                int vcod_persona;
                vcod_persona = ((Persona)Session["Session_Login"]).Person_id;
                E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
                oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = vcod_persona });

                if (opcion == 3 || opcion == 4)
                {
                    List<UnacemReportingStatusFachadaDescarga> oWfUna = new UnacemReporting().ListaStatusFachadaDescarga(new UnacemReportingParametro()
                        {
                            opcion = opcion,
                            parametro = parametro
                        });

                    var columnsuser1 = '4';
                    Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Ventas");

                    oWsUna.Cells["A1"].Value = "VENTAS";
                    oWsUna.Cells["B1"].Value = "FUERZA VENTAS";
                    oWsUna.Cells["C1"].Value = "TRADE MARKETING";
                    oWsUna.Cells["D1"].Value = "LUCKY";


                    _fila = 3;

                    foreach (UnacemReportingStatusFachadaDescarga oBj in oWfUna)
                    {
                            oWsUna.Cells[_fila, 1].Value = oBj.EreDescripcion;
                            oWsUna.Cells[_fila, 2].Value = oBj.ZonDescripcion;
                            oWsUna.Cells[_fila, 3].Value = oBj.Cantidad;
                            oWsUna.Cells[_fila, 4].Value = oBj.Porcentaje;

                            for (int i = 1; i <= columnsuser1; i++)
                            {
                                oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                //ajustando texto
                                oWsUna.Cells[_fila, i].Style.WrapText = true;
                            }

                            _fila++;
                       }

                        //Formato Cabecera 1
                        oWsUna.Row(1).Height = 25;
                        //oWsUna.Row(1).Style.Font.Bold = true;
                        oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                        oWsUna.Cells["A1:D1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A1:D1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                        //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                        //Formato Cabecera 2
                        oWsUna.Row(2).Height = 20;
                        oWsUna.Row(2).Style.Font.Bold = true;
                        oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                        oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                        oWsUna.Cells["A1:D1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                        oWsUna.Cells["A1:D1"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                        oWsUna.SelectedRange["A1:D1"].AutoFilter = true;


                        //Formato ambas cabeceras
                        oWsUna.Cells["A1:D1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:D1"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:D1"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                        oWsUna.Cells["A1:D1"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;




                        //Formato por columnas
                        //Ventas
                        oWsUna.Column(1).Width = 21;
                        oWsUna.Column(2).Width = 19;
                        oWsUna.Column(3).Width = 30;
                        oWsUna.Column(4).Width = 10;


                        oEx.Save();

                #endregion
                    }
                else if (opcion == 5)
                {
                    List<UnacemReportingStatusPresupuestal> oWfUna = new UnacemReporting().ListaStatusPresupuestal(new UnacemReportingParametro()
                    {
                        opcion = opcion,
                        parametro = parametro
                    });

                    //List<UnacemReportingStatusFachadaDescarga> oWfUna = new UnacemReporting().ListaStatusFachadaDescarga(new UnacemReportingParametro()
                    //{
                    //    opcion = opcion,
                    //    parametro = parametro
                    //});

                    var columnsuser1 = '8';
                    Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Ventas");

                    oWsUna.Cells["A1"].Value = "AÑO";
                    oWsUna.Cells["B1"].Value = "MES";
                    oWsUna.Cells["C1"].Value = "SOLICITADA";
                    oWsUna.Cells["D1"].Value = "APROBADA";
                    oWsUna.Cells["E1"].Value = "INVERSION";
                    oWsUna.Cells["F1"].Value = "IMPLEMENTADA";
                    oWsUna.Cells["G1"].Value = "INVERSION FACHADA";
                    oWsUna.Cells["H1"].Value = "INVERSION ACUMULADA";


                    _fila = 3;

                    foreach (UnacemReportingStatusPresupuestal oBj in oWfUna)
                    {
                        oWsUna.Cells[_fila, 1].Value = oBj.Anio;
                        oWsUna.Cells[_fila, 2].Value = oBj.MesDescripcion;
                        oWsUna.Cells[_fila, 3].Value = oBj.Solicitada;
                        oWsUna.Cells[_fila, 4].Value = oBj.Aprobada;
                        oWsUna.Cells[_fila, 5].Value = oBj.Inversion;
                        oWsUna.Cells[_fila, 6].Value = oBj.Implementada;
                        oWsUna.Cells[_fila, 7].Value = oBj.InversionFachada;
                        oWsUna.Cells[_fila, 8].Value = oBj.InversionAcumulada;

                        for (int i = 1; i <= columnsuser1; i++)
                        {
                            oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                            //ajustando texto
                            oWsUna.Cells[_fila, i].Style.WrapText = true;
                        }

                        _fila++;
                    }

                    //Formato Cabecera 1
                    oWsUna.Row(1).Height = 25;
                    //oWsUna.Row(1).Style.Font.Bold = true;
                    oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                    oWsUna.Cells["A1:H1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    oWsUna.Cells["A1:H1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                    //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                    //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                    //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                    //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                    //Formato Cabecera 2
                    oWsUna.Row(2).Height = 20;
                    oWsUna.Row(2).Style.Font.Bold = true;
                    oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                    oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                    oWsUna.Cells["A1:H1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                    oWsUna.Cells["A1:H1"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                    oWsUna.SelectedRange["A1:H1"].AutoFilter = true;


                    //Formato ambas cabeceras
                    oWsUna.Cells["A1:H1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                    oWsUna.Cells["A1:H1"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                    oWsUna.Cells["A1:H1"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                    oWsUna.Cells["A1:H1"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;




                    //Formato por columnas
                    //Ventas
                    oWsUna.Column(1).Width = 21;
                    oWsUna.Column(2).Width = 19;
                    oWsUna.Column(3).Width = 30;
                    oWsUna.Column(4).Width = 10;


                    oEx.Save();

                }
                //else
                //{
                //    _fileServer = "0";
                //}
                #endregion
            }
            return Json(new { Archivo = _fileServer });
          }
     
        #endregion

        
        #region Fotografico
        /// <summary>
        /// Autor: wlopez
        /// fecha: 24/01/2017
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Fotografico()
        {
            ViewBag.cod_equipo = Request.QueryString["_a"];
            return View();
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 26/01/2016
        /// </summary>
        /// <param name="opcion"></param>
        /// <param name="parametro"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Fotografico(int opcion, string parametro)
        {
            if (opcion == 0)
            {
                #region Registros de fachada
                int vcod_persona;
                vcod_persona = ((Persona)Session["Session_Login"]).Person_id;
                E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
                oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = vcod_persona });

                Request_WF_General oParametro = MvcApplication._Deserialize<Request_WF_General>(parametro);
                oParametro.Cod_Grupo = oResper.Cod_Grupo;

                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new WorkFlow().Consulta_Solicitud(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }

            return View();
        }

        [HttpPost]
        public JsonResult DescargaArchivo(string __a, string __b)
        {
            string error = "";
            string _filePath = "";
            string _fileServer = "";
            string _fileFormatServer = "";
            int iCompania = ((Persona)Session["Session_Login"]).Company_id;
            string extension = __b;
            //string textoCampania = Convert.ToString(Request.QueryString["__campania"]);

            try
            {
                List<E_Fachada_Unacem> oLs = MvcApplication._Deserialize<List<E_Fachada_Unacem>>(__a);

                _fileServer = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                _fileFormatServer = System.IO.Path.Combine(LocalFormat, "lucky-Formato-Fotografico-Castrol.pptx");

                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                /*copia formato y lo ubica en carpeta temporal*/
                System.IO.File.Copy(_fileFormatServer, _filePath + ".pptx", true);

                PowerPoint.Application App = new PowerPoint.Application();
                PowerPoint.Presentation Pre = App.Presentations.Open(_filePath + ".pptx", MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);

                foreach (E_Fachada_Unacem oBj in oLs)
                {
                    PowerPoint.Slide Sli;

                    //Sli = Pre.Slides.Add(Pre.Slides.Count + 1, PowerPoint.PpSlideLayout.ppLayoutPictureWithCaption);
                    Sli = Pre.Slides.Add(Pre.Slides.Count + 1, PowerPoint.PpSlideLayout.ppLayoutTwoObjectsAndText);

                    var ruta_fotoSolicitud = oBj.RutaCompletaFotoSolicitud;
                    ruta_fotoSolicitud = ruta_fotoSolicitud.Replace("/Fachada", "");

                    var ruta_foto_fotomontaje = oBj.RutaCompletaFotoFotomontaje;
                    ruta_foto_fotomontaje = ruta_foto_fotomontaje.Replace("/Fachada", "");

                    var ruta_foto_fotografico = oBj.RutaCompletaFotoFotografico;
                    ruta_foto_fotografico = ruta_foto_fotografico.Replace("/Fachada", "");


                    if (ruta_fotoSolicitud != "")
                    {
                        PowerPoint.Shape Sha = Sli.Shapes[2];
                        //Sli.Shapes.AddPicture(ConfigurationManager.AppSettings["LocalFachada"] + ruta_fotoSolicitud, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                        Sli.Shapes.AddPicture("https://www.xplora.com.pe/xplora/Fachada" + ruta_fotoSolicitud, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                    }
                    else
                    {
                        PowerPoint.Shape Sha = Sli.Shapes[2];
                        Sli.Shapes.AddPicture("https://www.xplora.com.pe/xplora/Fachada/Img/foto_nodisponible.jpg", MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                    }

                    if (ruta_foto_fotografico != "")
                    {
                        PowerPoint.Shape Sha2 = Sli.Shapes[3];
                        //Sli.Shapes.AddPicture(ConfigurationManager.AppSettings["LocalFachada"] + ruta_foto_fotografico, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha2.Left, Sha2.Top, Sha2.Width, Sha2.Height);
                        Sli.Shapes.AddPicture("https://www.xplora.com.pe/xplora/Fachada" + ruta_foto_fotografico, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha2.Left, Sha2.Top, Sha2.Width, Sha2.Height);
                    }
                    else
                    {
                        if (ruta_foto_fotomontaje != "")
                        {
                            PowerPoint.Shape Sha2 = Sli.Shapes[3];
                            //Sli.Shapes.AddPicture(ConfigurationManager.AppSettings["LocalFachada"] + ruta_foto_fotomontaje, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha2.Left, Sha2.Top, Sha2.Width, Sha2.Height);
                            Sli.Shapes.AddPicture("https://www.xplora.com.pe/xplora/Fachada" + ruta_foto_fotomontaje, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha2.Left, Sha2.Top, Sha2.Width, Sha2.Height);
                        }
                        else
                        {
                            PowerPoint.Shape Sha2 = Sli.Shapes[3];
                            Sli.Shapes.AddPicture("https://www.xplora.com.pe/xplora/Fachada/Img/foto_nodisponible.jpg", MsoTriState.msoFalse, MsoTriState.msoTrue, Sha2.Left, Sha2.Top, Sha2.Width, Sha2.Height);
                        }
                    }

                    //Sli.Shapes.AddPicture(ConfigurationManager.AppSettings["Rutafoto"] + "slide.jpg", MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                    Sli.Shapes[1].TextFrame.TextRange.Text = oBj.Nom_PDV;
                    Sli.Shapes[4].TextFrame.TextRange.Text = "Zona: " + oBj.DescripcionZona + "    Supervisor: " + oBj.PrimerNombrePrimerApellidoUsuario + (char)13 + @"Distribuidora: " + oBj.Distribuidora + (char)13 + @"Dirección: " + oBj.Direccion + @" \ " + oBj.Distrito + (char)13
                    + @"Marca: " + oBj.DescripcionMarca + (char)13 + @"Tipo: " + oBj.DescripcionTipoSolicitud + (char)13 + @"Estado de Tramite: " + oBj.DescripcionEstadoTramite +
                    "   Tipo de Tramite: " + oBj.DescripcionTipoTramite2 + (char)13 + @"Observaciones Solicitud: " + oBj.P1_Comentarios + (char)13 + @"Observaciones (Tramite): " + oBj.P1_Comentarios;
                    Sli.Shapes[4].TextEffect.FontSize = 10;

                    //Sli.Shapes[2].ScaleHeight(1, MsoTriState.msoTrue);
                    //Sli.Shapes[2].ScaleWidth(1, MsoTriState.msoTrue);
                }

                if (extension == ".pdf")
                {
                    Pre.ExportAsFixedFormat(_filePath + ".pdf", PowerPoint.PpFixedFormatType.ppFixedFormatTypePDF, PowerPoint.PpFixedFormatIntent.ppFixedFormatIntentScreen);
                }

                Pre.Save();
            }
            catch (Exception e)
            {
                error = e.Message;
            }


            return Json(new { Archivo = _fileServer, Error = error });
        }
        #endregion
    }
}