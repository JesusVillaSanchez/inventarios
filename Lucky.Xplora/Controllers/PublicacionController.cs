﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using IO = System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Publicacion;
using Lucky.Xplora.Models.Ecuador;

using Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Lucky.Xplora.Models.Administrador;

namespace Lucky.Xplora.Controllers
{
    public class PublicacionController : Controller
    {
        //string RutaXplora = Convert.ToString(ConfigurationManager.AppSettings["xplora"]);
        string LocalFormat = Convert.ToString(ConfigurationManager.AppSettings["LocalFormat"]);
        string LocalEncarte = Convert.ToString(ConfigurationManager.AppSettings["LocalEncarte"]);

        //
        // GET: /Publicacion/

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-30
        /// </summary>
        /// <returns></returns>
        public ActionResult Publicacion()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            ViewBag.Tipo_Perfil = ((Persona)Session["Session_Login"]).tpf_id;
            ViewBag.compania = ((Persona)Session["Session_Login"]).Company_id;

            if (Request["_a"] != null)
            {
                ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
                        
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-31
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Catalogo(string __a, string __b, string __c, string __d, string __e)
        {

            ViewBag.Tipo_Perfil = ((Persona)Session["Session_Login"]).tpf_id;

            return View(
                    new Encarte().Lista(
                        new Request_Encarte()
                        {


                            compania = Convert.ToInt32(((Persona)Session["Session_Login"]).Company_id),
                            campania = __a,
                            tipo_canal = Convert.ToInt32(__b),
                            cadena = Convert.ToInt32(__c),
                            anio = Convert.ToInt32(__d),
                            mes = Convert.ToInt32(__e)
                            //compania = 1637,
                            //campania = "9020110152013",
                            //tipo_canal = 25,
                            //cadena = 25,
                            //anio = 2015,
                            //mes = 3
                        }
                    )
                );
        }

        [HttpPost]
        public ActionResult UpdateCatalogo(string _a)
        {
            int iResultado = 0;
            //int _pagina = 0;

            iResultado = new Encarte().UpdateCatalogo(new UpdateCatalago_Request()
            {
                id_catalogo = Convert.ToString(_a)
            });

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new { Catalogo = iResultado }),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-04-03
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Carga_Encarte(string __a, string __b, string __c, string __d, string __e, HttpPostedFileBase __f)
        {
            int conteo = 0;
            string _carpeta = "";
            string _fileLocal = "";
            int _catalogo = 0;
            int _pagina = 0;

            if (__f != null)
            {
                /*Nombre de archivo de local y en servidor*/
                _fileLocal = IO.Path.GetFileName(__f.FileName);
                _carpeta = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                /*Crea directorio para nuevo encarte*/
                IO.Directory.CreateDirectory(IO.Path.Combine(LocalEncarte, _carpeta));
                //IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["RutaEncarte"] + "/Temp/Encarte/" + _carpeta);

                /*Graba archivo en nueva ubicacion de encarte*/
                __f.SaveAs(IO.Path.Combine(LocalEncarte, _carpeta + "/PowerPoint"));
                //__f.SaveAs(ConfigurationManager.AppSettings["RutaEncarte"] + "/Temp/Encarte/" + _carpeta + "/PowerPoint.pptx");

                /*Visual Basic Script(VBS), se hace una copia en la carpeta de encarte*/
                IO.File.Copy(IO.Path.Combine(LocalFormat, "Lucky-Exporta-PowerPoint-Jpg.vbs"), IO.Path.Combine(LocalEncarte, _carpeta, "foto.vbs"));
                //IO.File.Copy(Server.MapPath("~/Format/Lucky-Exporta-PowerPoint-Jpg.vbs"), ConfigurationManager.AppSettings["RutaEncarte"] + "/Temp/Encarte/" + _carpeta + "/foto.vbs");

                #region Ejecuta Visual Basic Script(VBS)
                //Process p = Process.Start(IO.Path.Combine(LocalEncarte, _carpeta, "foto.vbs"));
                ////Process p = Process.Start(ConfigurationManager.AppSettings["RutaEncarte"] + "/Temp/Encarte/" + _carpeta + "/foto.vbs");
                ////p.StartInfo.FileName = @"C:\text.vbs";
                ////p.Start();
                //p.WaitForExit();
                ////p.Close();
                PowerPoint.Application oPpt = new PowerPoint.Application();
                PowerPoint.Presentation oPre = oPpt.Presentations.Open(IO.Path.Combine(LocalEncarte, _carpeta, "PowerPoint"), Microsoft.Office.Core.MsoTriState.msoCTrue, Microsoft.Office.Core.MsoTriState.msoTriStateMixed, Microsoft.Office.Core.MsoTriState.msoFalse);
                oPre.SaveAs(IO.Path.Combine(LocalEncarte, _carpeta), PowerPoint.PpSaveAsFileType.ppSaveAsPNG);
                oPre.Close();
                oPpt.Quit();
                #endregion

                #region Registra archivos

                /*Cuenta cuantos archivos PNG existen*/
                string[] lsFiles = IO.Directory.GetFiles(IO.Path.Combine(LocalEncarte, _carpeta), "*.PNG");
                //string[] lsFiles = IO.Directory.GetFiles(ConfigurationManager.AppSettings["RutaEncarte"] + "/Temp/Encarte/" + _carpeta, "*.PNG");

                if (lsFiles.Length > 0)
                {
                    conteo = 1;

                    foreach (string obFile in lsFiles)
                    {
                        if (lsFiles.First() == obFile)
                        {
                            _catalogo = new Encarte().Inserta_Catalogo(new Request_Inserta_Catalogo()
                            {
                                catalogo_nombre = IO.Path.GetFileNameWithoutExtension(__f.FileName),
                                catalogo_imagen = @"Encarte/" + _carpeta + @"/" + new IO.FileInfo(obFile).Name,
                                tipo_canal = Convert.ToInt32(__b),
                                cadena = Convert.ToInt32(__c),
                                anio = Convert.ToInt32(__d),
                                mes = Convert.ToInt32(__e),
                                catalogo_direccion = _fileLocal,
                                campania = __a
                            });
                        }

                        _pagina += new Encarte().Inserta_Pagina(new Request_Inserta_Pagina()
                        {
                            catalogo_id = _catalogo,
                            pagina_orden = conteo,
                            pagina_imagen = @"Encarte/" + _carpeta + @"/" + new IO.FileInfo(obFile).Name
                        });

                        conteo++;
                    }
                }

                #endregion
            }

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new { pagina = _pagina }),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public JsonResult Descarga_Encarte(string __a, string __b,string __c)
        {
            string _filePath = "";
            string _fileServer = "";
            string _fileFormatServer = "";
            string mensaje = "";

            string ruta = "";
            //ruta = __a.Substring(10, 15);

            int first = __a.IndexOf("/"); 
            int last = __a.LastIndexOf("/");

            ruta = __a.Substring(first, last - first);
            ruta = ruta.Replace("/", "");

            try
            {
                _fileServer = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);
                _fileFormatServer = System.IO.Path.Combine(LocalEncarte, ruta + "\\PowerPoint");
                //_fileFormatServer = System.IO.Path.Combine(Server.MapPath("/Temp/Encarte/"), ruta + "\\PowerPoint");
                //_filePath = System.IO.Path.Combine(Server.MapPath("/Temp/Encarte/"), _fileServer);
                _filePath = System.IO.Path.Combine(LocalEncarte, _fileServer);
                System.IO.File.Copy(_fileFormatServer, _filePath + ".pptx", true);
            }
            catch (Exception e)
            {
                mensaje = e.Message;
                _fileServer = "";
            }

            return Json(new { Archivo = _fileServer });
            //return Json(new { Archivo = __c,  });
        }
    }
}