﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using LuckyXplora = Lucky.Xplora.Models;
using Lucky.Xplora.Models.Alicorp;
using Newtonsoft.Json;

namespace Lucky.Xplora.Controllers
{
    public class AlicorpDataMercaderistaController : Controller
    {
        //
        // GET: /AlicorpDataMercaderista/

        #region RQ 190 v1.0 AlicorpDataMercaderista - Mayorista

        public ActionResult Index()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            //LuckyXplora.Persona oBj = ((LuckyXplora.Persona)Session["Session_Login"]);
            //oBj.tpf_id = 4;
            //Session["Session_Login"] = oBj;

            if (Request["_a"] != null)
            {
                Session["Session_Campania"] = Request["_a"];
                //ViewBag.campania = Request["_a"];
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] }).cam_descripcion;

            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 14-03-2015
        /// </summary>
        /// <param name="__a">campania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAnio(string __a)
        {
            return View(new LuckyXplora.Año().Lista(new LuckyXplora.Request_Anio_Por_Planning_Reports()
            {
                campania = __a
            }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 14-03-2015
        /// </summary>
        /// <param name="__a">campania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _GetMMes(int anio)
        {
            return PartialView(
                "../BackusReporting/GetMes",
                new LuckyXplora.Mes().Lista(
                    new LuckyXplora.M_Request_Mes_Por_Planning_Reports_Anio()
                    {
                        id_planning = Convert.ToString(Session["Session_Campania"]),
                        id_report = 28,
                        anio = anio
                    }
                ));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 14-03-2015
        /// </summary>
        /// <param name="__a">campania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _GetMPeriodo(int anio, int mes)
        {
            return PartialView(
                "../ColgateReporting/GetPeriodo",
                new LuckyXplora.Periodo().Lista(
                    new LuckyXplora.Periodo_Por_Planning_Reports_Anio_Mes_Request()
                    {
                        campania = Convert.ToString(Session["Session_Campania"]),
                        reporte = 28,
                        anio = anio,
                        mes = mes
                    }
                ));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 16-03-2015
        /// </summary>
        /// <param name="__a">campania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetOficina(string __a)
        {
            return View(
                    new LuckyXplora.Oficina().Lista(new LuckyXplora.Request_Listar_Oficinas_Por_CodCompania_Reports()
                    {
                        compania = __a
                    })
                );
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 16-03-2015
        /// </summary>
        /// <param name="__a">campania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCategoria(int __a)
        {
            return PartialView(
                "../Alicorp/GetCategoria",
                new LuckyXplora.Categoria().Lista(new LuckyXplora.Request_GetCategoria_Compania()
                {
                    compania = __a
                }));
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 16-03-2015
        /// </summary> Reporte Vista SKU Familias
        /// <param name="__a">campania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteSKUFamilias(int anio, int mes, int periodo, int oficina, string categoria) {
            ViewBag.anio = anio;
            ViewBag.mes = mes;
            ViewBag.periodo = periodo;
            ViewBag.oficina = oficina;
            ViewBag.categoria = categoria;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 16-03-2015
        /// </summary> Reporte Vista SKU Familias
        /// <param name="__a">campania</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteSKUFamiliasJson(int anio, int mes, int periodo, int oficina, string categoria, int estado)
        {
            List<ReporteSKUFamilias> oLs = new ReporteSKUFamilias().Lista(
                new Request_ReporteSKUFamilias()
                {
                    anio = anio,
                    mes = mes,
                    periodo = periodo,
                    oficina = oficina,
                    categoria = categoria
                });

            if (estado != 2)
            {
                foreach (ReporteSKUFamilias oBj in oLs)
                {
                    oBj.validCliente = Convert.ToBoolean(estado);
                }
            }

            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 18-03-2015
        /// </summary> Combo Observaciones
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Combo_Observacion_Alicorp_Mayorista(int report)
        {
            Models.Alicorp.Pla_MObservacion_Request oRq = new Models.Alicorp.Pla_MObservacion_Request();

            oRq.planning = Convert.ToString(Session["Session_Campania"]);
            oRq.report = report;

            var oModelo = MvcApplication._Deserialize<Models.Alicorp.Pla_MObservacion_Response>(MvcApplication._Servicio_Operativa.Pla_MObservacion_Por_Planning_Report(MvcApplication._Serialize(oRq)));

            if (oModelo == null || oModelo.Lista == null || oModelo.Lista.Count == 0)
            {
                List<Models.Alicorp.Pla_MObservacion> oLs = new List<Models.Alicorp.Pla_MObservacion>();
                Models.Alicorp.Pla_MObservacion oOb = new Models.Alicorp.Pla_MObservacion();
                oOb.pmo_id = "0";
                oOb.pmo_descripcion = "Sin datos disponibles.";
                oLs.Add(oOb);
                oModelo.Lista = oLs;
            }

            var modelData = oModelo.Lista.Select(u => new SelectListItem()
            {
                Text = u.pmo_descripcion,
                Value = u.pmo_id
            });
            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update_Obs_Vali(string Datos)
        {
            List<ReporteSKUFamilias> oLs = MvcApplication._Deserialize<List<ReporteSKUFamilias>>(Datos);

            foreach (ReporteSKUFamilias oOb in oLs)
            {
                Alicorp_Mayor_Actualiza_Request oRq = new Alicorp_Mayor_Actualiza_Request();
                Alicorp_Mayor_Actualiza_Response oRp;

                oRq.id_cab = oOb.oro_id;
                oRq.id_obs = oOb.obs_id;
                oRq.vali = oOb.validCliente;
                oRq.user_modif = ((LuckyXplora.Persona)Session["Session_Login"]).name_user;

                oRp = MvcApplication._Deserialize<Alicorp_Mayor_Actualiza_Response>(MvcApplication._Servicio_Operativa.Alicorp_Mayorista_SKU_Familia_Actualiza(MvcApplication._Serialize(oRq)));

            }
            return Json(oLs, JsonRequestBehavior.AllowGet);
        }



        //public string Alicorp_Mayorista_Analista_ActualizaCheck(string valida, string invalida)
        //{
        //    Valida_Request oRq = new Valida_Request();

        //    if (valida == null) valida = "";
        //    if (invalida == null) invalida = "";

        //    oRq.valida = valida;
        //    oRq.invalida = invalida;
        //    oRq.usuario = ((LuckyXplora.Persona)Session["Session_Login"]).name_user;

        //    return MvcApplication._Servicio_Operativa.Alicorp_Mayorista_Analista_ActualizaCheck(MvcApplication._Serialize(oRq));
        //}

        [HttpPost]
        public ActionResult Alicorp_Mayorista_Analista_ActualizaCheck(string valida, string invalida)
        {
            if (valida == null) valida = "";
            if (invalida == null) invalida = "";

            return Json(new ReporteSKUFamilias().ResponseSkuFamilia(
                    new Valida_Request() {
                        valida = valida,
                        invalida = invalida,
                        usuario = ((LuckyXplora.Persona)Session["Session_Login"]).name_user
                    }
                ));
        }



        [HttpPost]
        public ActionResult GetReporteMarca(int anio, int mes, int periodo, int oficina, string categoria)
        {
            ViewBag.anio = anio;
            ViewBag.mes = mes;
            ViewBag.periodo = periodo;
            ViewBag.oficina = oficina;
            ViewBag.categoria = categoria;
            return View();
        }


        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 19-03-2015
        /// </summary> Reporte Vista Marca
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteMarcaJson(int anio, int mes, int periodo, int oficina, string categoria, int estado)
        {

            List<ReporteSKUFamilias> oLs = new ReporteSKUFamilias().Lista(
                new Request_ReporteSKUFamilias()
                {
                    anio = anio,
                    mes = mes,
                    periodo = periodo,
                    oficina = oficina,
                    categoria = categoria

                });
            if (estado != 2) {
                foreach (ReporteSKUFamilias oBj in oLs) {
                    oBj.validCliente = Convert.ToBoolean(estado);
                }
            
            }
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }


        [HttpPost]
        public ActionResult GetReporteCategoria(int anio, int mes, int periodo, int oficina, string categoria)
        {
            ViewBag.anio = anio;
            ViewBag.mes = mes;
            ViewBag.periodo = periodo;
            ViewBag.oficina = oficina;
            ViewBag.categoria = categoria;
            return View();
        }


        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 19-03-2015
        /// </summary> Reporte Vista Categoria
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteCategoriaJson(int anio, int mes, int periodo, int oficina, string categoria, int estado)
        {
            List<ReporteSKUFamilias> oLs = new ReporteSKUFamilias().Lista(
                new Request_ReporteSKUFamilias()
                {
                    anio = anio,
                    mes = mes,
                    periodo = periodo,
                    oficina = oficina,
                    categoria = categoria

                });
            if (estado != 2)
            {
                foreach (ReporteSKUFamilias oBj in oLs)
                {
                    oBj.validCliente = Convert.ToBoolean(estado);
                }

            }
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }

        [HttpPost]
        public ActionResult GetReporteVistaAnalista(int anio, int mes, int oficina, string categoria, int periodo)
        {
            ViewBag.anio = anio;
            ViewBag.mes = mes;
            ViewBag.periodo = periodo;
            ViewBag.oficina = oficina;
            ViewBag.categoria = categoria;
            return View();
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 19-03-2015
        /// </summary> Reporte Vista Analista
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReporteVistaAnalistaJson(int anio, int mes, int oficina, string categoria, int periodo, int estado)
        {
            List<ReporteSKUFamilias> oLs = new ReporteSKUFamilias().ListaVistaAnalista(
                new Request_ReporteSKUFamilias()
                {
                    anio = anio,
                    mes = mes,
                    periodo = periodo,
                    oficina = oficina,
                    categoria = categoria

                });
            if (estado != 2)
            {
                foreach (ReporteSKUFamilias oBj in oLs)
                {
                    oBj.Validado = Convert.ToBoolean(estado);
                }

            }
            return new ContentResult
            {
                Content = MvcApplication._Serialize(oLs),
                ContentType = "application/json"
            };
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update_Obs_Vali_Analista(string Datos)
        {
            List<ReporteSKUFamilias> oLs = MvcApplication._Deserialize<List<ReporteSKUFamilias>>(Datos);

            foreach (ReporteSKUFamilias oOb in oLs)
            {
                Alicorp_Mayor_Actualiza_Request oRq = new Alicorp_Mayor_Actualiza_Request();
                Alicorp_Mayor_Actualiza_Response oRp;

                oRq.id_cab = oOb.oro_id;
                oRq.id_obs = oOb.obs_id;
                oRq.vali = oOb.Validado;
                oRq.user_modif = ((LuckyXplora.Persona)Session["Session_Login"]).name_user;

                oRp = MvcApplication._Deserialize<Alicorp_Mayor_Actualiza_Response>(MvcApplication._Servicio_Operativa.Alicorp_Mayorista_Analista_Actualiza(MvcApplication._Serialize(oRq)));
            }

            return Json(oLs, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Ali_Mayor_Valida_Ana(string Datos)
        {
            List<ReporteSKUFamilias> oLs = MvcApplication._Deserialize<List<ReporteSKUFamilias>>(Datos);

            foreach (ReporteSKUFamilias oOb in oLs)
            {
                Alicorp_Mayor_Actualiza_Request oRq = new Alicorp_Mayor_Actualiza_Request();
                Alicorp_Mayor_Actualiza_Response oRp;

                oRq.id_cab = Convert.ToInt32(oOb.oro_id);
                //oRq.id_obs = oOb.obs_id;
                oRq.vali = oOb.Validado;
                oRq.user_modif = ((LuckyXplora.Persona)Session["Session_Login"]).name_user;

                oRp = MvcApplication._Deserialize<Alicorp_Mayor_Actualiza_Response>(MvcApplication._Servicio_Operativa.Alicorp_Mayorista_Analista_ActualizaCheck(MvcApplication._Serialize(oRq)));
            }

            return Json(oLs, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Alicorp_Mayorista_Cliente_ActualizaCheck(string valida, string invalida)
        {

            if (valida == null) valida = "";
            if (invalida == null) invalida = "";

            return Json(new ReporteSKUFamilias().ResponseAnalista(
                    new Valida_Request()
                    {
                        valida = valida,
                        invalida = invalida,
                        usuario = ((LuckyXplora.Persona)Session["Session_Login"]).name_user
                    }
                ));
        }


        #endregion

    }
}