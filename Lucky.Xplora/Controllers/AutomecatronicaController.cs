﻿using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Auditoria;
using Lucky.Xplora.Models.Automecatronica;
using Lucky.Xplora.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;

namespace Lucky.Xplora.Controllers
{
    public class AutomecatronicaController : Controller
    {
        private string LocalFoto = Convert.ToString(ConfigurationManager.AppSettings["LocalFoto"]);
        private string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        private string XploraTemp = Convert.ToString(ConfigurationManager.AppSettings["XploraTemp"]);

        #region Guillermo Ruiz
        //
        // GET: /Automecatronica/
        [CustomAuthorize]
        public ActionResult Index()
        {
            ViewBag.company = ((Persona)Session["Session_Login"]).Company_id;
            return View();
        }

        [HttpPost]
        public ActionResult Lista_AutoMantenimiento(string __a, string __b, string __c, string __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    Lista(new Request_AutoMantenimiento()
                    {
                        placa = __a,
                        asesor = Convert.ToInt32(__b),
                        cliente = Convert.ToInt32(__c),
                        estado = Convert.ToInt32(__d)
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Obtener_Val(string __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    Obtener_Val_Orden(new Request_ObtenerValOrd()
                    {
                        id_auto = Convert.ToInt32(__a),
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Obtener_Orden(string __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    Obtener_Orden(new Request_Orden()
                    {
                        id_auto = Convert.ToInt32(__a),
                    })),
                ContentType = "application/json"
            }.Content);
        }


        [HttpPost]
        public ActionResult obtener_comboFiltro(string __a, string __b, string __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new ComboFiltro().
                    Lista(new Request_comboFiltro()
                    {
                        condicion = Convert.ToInt32(__a),
                        id_company = Convert.ToInt32(__b),
                        dato = Convert.ToInt32(__c),
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Registrar_Val(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l = "0", string __m = "0")
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    Registro_Valorizacion(new Request_Valorizacion()
                    {
                        id_val = Convert.ToInt32(__a),
                        id_actividad = Convert.ToInt32(__b),
                        costo = __c,
                        id_auto = Convert.ToInt32(__d),
                        fecha = __e,
                        actual = Convert.ToBoolean(__f),
                        descrip_trab = __g,
                        valorizacion = __h,
                        ajust = __i,
                        fec_ajuste = __j,
                        csajuste = Convert.ToBoolean(__k),
                        costo_ajuste = __l,
                        total_val = __m
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Registrar_Orden(string __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    Registro_Orden(new Request_Orden()
                    {
                        id_orden = Convert.ToInt32(__a),
                        id_auto = Convert.ToInt32(__b),
                        fecha_oc = __c,
                        oc = __d,
                        costo = __e,
                        referencia = __f,
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Registrar_Auto(int __a, string __b, int __c, int __d, int __e, int __f,
            string __g, string __h, string __i = "01/01/1900", int __j = 0, int __k = 0, int __l = 0, string __m = "", int __n = 0, string __o = "",
            int __p = 0, string __q = "", string __r = "", string __s = "", string __t = "", string __u = "", string __v = "", int __w = 0, string __x = "", string __y = "", string __z = "", string __aa = "", Boolean __ab = false)
        {
            // return Json(new { status = "SUCCESS", count = 0 }, JsonRequestBehavior.AllowGet); ;

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    Registro(new Request_AutoMantenimiento_Registro()
                    {
                        id_auto = __a,
                        placa = __b,
                        cliente = __c,
                        marca = __d,
                        modelo = __e,
                        color = __f,
                        aseguradora = __g,
                        fec_recep = __h,
                        fec_taller = __i,
                        id_fotos_inicio = __j,
                        id_valorizacion = __k,
                        id_orden_compra = __l,
                        descripcion_trabajo = __m,
                        estado = __n,
                        fecha_estimada_entrega = __o,
                        proceso_actual = __p,
                        tecnico = __q,
                        sede_t = __r,
                        telefono_t = __s,
                        asesor = __t,
                        sede_a = __u,
                        telefono_a = __v,
                        id_fotos_final = __w,
                        fecha_proc_act = __x,
                        observacion = __y,
                        en_taller = __ab
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult eliminar_Valor(string __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    eliminar_Val_Orden(new Request_ObtenerValOrd()
                    {
                        id_auto = Convert.ToInt32(__a),
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult eliminar_Orden(string __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    eliminar_Orden(new Request_Orden()
                    {
                        id_orden = Convert.ToInt32(__a),
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult eliminar_Registro(string __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    eliminar_Registro(new Request_ObtenerRegistro()
                    {
                        id_auto = Convert.ToInt32(__a),
                    })),
                ContentType = "application/json"
            }.Content);
        }

        //Registrar_Foto
        [HttpPost]
        public ActionResult Registrar_Foto(string __a, string __b, string __c, string __d, string __e)
        {
            if (Session["Session_Login"] == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            }
            Persona ObjPer = ((Persona)Session["Session_Login"]);

            string _archivo = "";
            string rootpath = "";
            string _doc_url = "";
            string _doc_nom = "";
            _archivo = __a + "_" + String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now) + __e;

            switch (__d)
            {
                case "image/jpeg":
                    _doc_nom = _archivo + ".jpg";
                    rootpath = System.IO.Path.Combine(Server.MapPath("~/Temp/Automecatronica/Images/"), _doc_nom);
                    _doc_url = "Temp/Automecatronica/Images/" + _doc_nom;
                    break;
                case "image/png":
                    _doc_nom = _archivo + ".png";
                    rootpath = System.IO.Path.Combine(Server.MapPath("~/Temp/Automecatronica/Images/"), _doc_nom);
                    _doc_url = "Temp/Automecatronica/Images/" + _doc_nom;
                    break;
            }

            if (ConvertToFile(rootpath, __b.Split(',')[1]))
            {
                Auditoria ObjAudit = new Auditoria();
                bool vestado = ObjAudit.Audit_Descarga(new RequestParametrosAuditoriaUpDownLoad
                {
                    aud_ruta = rootpath,//(System.IO.Path.Combine(Server.MapPath("~" + _rt1 + vdoc.ToString()))),
                    aud_tipo_carga = 1, // Carga de archivo 
                    aud_usuario = ObjPer.name_user,
                    aud_comentario = "Proyecto Automecatronica"
                });
            }


            var Content = MvcApplication._Serialize(new Auto().
            Registro_Foto(new Request_Auto_Foto()
            {
                id_auto = Convert.ToInt32(__a),
                foto = _doc_nom,
                tipo = __d,
                ingreso = Convert.ToBoolean(__c)
            }));

            return Json(new { data = _doc_url, cont = Content }, JsonRequestBehavior.AllowGet);
        }

        public static bool ConvertToFile(string location, string file)
        {
            try
            {
                byte[] bytes = System.Convert.FromBase64String(file);
                System.IO.File.WriteAllBytes(location, bytes);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        [HttpPost]
        public ActionResult obtener_Fotos(string __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    Obtener_Fotos(new Request_Auto_Foto()
                    {
                        id_auto = Convert.ToInt32(__a),
                        ingreso = Convert.ToBoolean(__b),
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult eliminar_Foto(string __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Auto().
                    eliminar_Foto(new Request_Auto_Foto()
                    {
                        id_foto = Convert.ToInt32(__a),
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult agregar_archivo(HttpPostedFileBase file, Boolean tipo, int id, Boolean or, int num)
        {
            if (Session["Session_Login"] == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            }
            Persona ObjPer = ((Persona)Session["Session_Login"]);


            string _archivo = "";
            string _fileLocal = "";
            string _doc_url = "";
            if (file != null)
            {
                _fileLocal = System.IO.Path.GetFileName(file.FileName);
                _archivo = "PDF_" + id + "_" + String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now) + "_" + ((tipo == false) ? "1" : "2") + num + "_" + ((or == false) ? "val" : "ord");

                string vtipoarchi = _fileLocal.Substring(_fileLocal.IndexOf("."), 4);

                switch (vtipoarchi)
                {
                    case ".pdf":
                        file.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Temp/Automecatronica/Documents/"), _archivo + ".pdf"));
                        _doc_url = System.IO.Path.Combine(Server.MapPath("~/Temp/Automecatronica/Documents/"), _archivo + ".pdf");
                        _archivo = _archivo + ".pdf";
                        break;
                }
            }

            Auditoria ObjAudit = new Auditoria();
            bool vestado = ObjAudit.Audit_Descarga(new RequestParametrosAuditoriaUpDownLoad
            {
                aud_ruta = _doc_url,//(System.IO.Path.Combine(Server.MapPath("~" + _rt1 + vdoc.ToString()))),
                aud_tipo_carga = 1, // Carga de archivo 
                aud_usuario = ObjPer.name_user,
                aud_comentario = "Proyecto Automecatronica"
            });

            if (or == false)
            {
                var Content = MvcApplication._Serialize(new Auto().
                Actualizar_archivo_val(new Request_Valorizacion()
                {
                    id_val = id,
                    tipo = Convert.ToBoolean(tipo),
                    valorizacion = _archivo
                }));
            }
            else
            {
                var Content = MvcApplication._Serialize(new Auto().
                Actualizar_archivo_orden(new Request_Valorizacion()
                {
                    id_val = id,
                    tipo = Convert.ToBoolean(tipo),
                    valorizacion = _archivo
                }));
            }

            return Json(_archivo);

        }
        #endregion

        #region Mecatronica v2
        /// <summary>
        /// Autor: adarrigo
        /// Fecha: 2016-08-11
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Automecatronica()
        {
            return View();
        }

        /// <summary>
        /// Autor: adarrigo
        /// Fecha: 2016-08-11
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Automecatronica(string __a, string __b)
        {
            int iRespuesta = 0;
            AutomecatronicaParametro oParametro = new AutomecatronicaParametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            if (oParametro.opcion == 0 || oParametro.opcion == 1)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Automecatronica().Lista(oParametro)),
                    ContentType = "application/json"
                }.Content);
            }

            if (oParametro.opcion == 9)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Automecatronica().ListaAutomecatronica(oParametro)),
                    ContentType = "application/json"
                }.Content);
            }
            else if (oParametro.opcion == 2)
            {
                string id = new Automecatronica().GeneraId(oParametro);

                return Json(new { id = id });
            }
            else if (oParametro.opcion == 8)
            {
                string path = Server.MapPath("~/Temp/Foto/Automecatronica");
                string fullpath = Path.Combine(path, oParametro.parametro);

              

                #region Elimina en directorio
                try
                {
                    System.IO.File.Delete(fullpath);
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }

                
                #endregion

             
                    #region Elimina en BD


           
                    iRespuesta+= new Automecatronica().InsertUpdate(oParametro);
                   

                    #endregion

                    return Json(new { respuesta = iRespuesta });
           
            }

            return View();
        }

        /// <summary>
        /// Autor: Aldo D'Arrigo
        /// Fecha:22-08-2016
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AutomecatronicaCrear()
        {
          
            ViewBag.GeneradoId = new Automecatronica().GeneraId(new AutomecatronicaParametro() { opcion = 2, parametro = "" });
            return View();
        }

        /// <summary>
        /// Autor: Aldo D'Arrigo
        /// Fecha:22-08-2016
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AutomecatronicaCrear(FormCollection coleccion)
        {
            int iRespuesta = 0;
            List<string> lArray = new List<string>();

            string id = Convert.ToString(coleccion["input-id"]);
            string placa = Convert.ToString(coleccion["Placa"]);
            string fechaIng = Convert.ToString(coleccion["IngresoH"]);
            string fechaAju = Convert.ToString(coleccion["FechaAju"]);
            string fechaIni = Convert.ToString(coleccion["InicioPro"]);
            string fechaAdua = Convert.ToString(coleccion["FechaAdua"]);
            string fechaApro = Convert.ToString(coleccion["FechaAprob"]);
            string fechaCotRep = Convert.ToString(coleccion["CotiRep"]);
            string fechaEntreOC = Convert.ToString(coleccion["EntregaOC"]);
            string NroOrden = Convert.ToString(coleccion["NroOrden"]);
            string id_trabajo = Convert.ToString(coleccion["id_trabajo"]);
            string hrschapa = Convert.ToString(coleccion["HrsChapa"]);
            string HrsPintura = Convert.ToString(coleccion["HrsPintura"]);
            string HrsArma = Convert.ToString(coleccion["HrsArma"]);
            string HrsMecan = Convert.ToString(coleccion["HrsMecan"]);
            string Importe = Convert.ToString(coleccion["Importe"]);
            string MontoChapa = Convert.ToString(coleccion["MontoChapa"]);
            string MontoPintura = Convert.ToString(coleccion["MontoPintura"]);
            string HHprepar = Convert.ToString(coleccion["HHprepar"]);
            string MontoMecan = Convert.ToString(coleccion["MontoMecan"]);
            string cbProceso = Convert.ToString(coleccion["cbProceso"]);
            string UtArmado = Convert.ToString(coleccion["Ut-Armado"]);
            string UtMecan = Convert.ToString(coleccion["Ut-Mecan"]);
            string TFechaEstimada = Convert.ToString(coleccion["T-FechaEstimada"]);
            string Tobserv = MvcApplication.Replace(Convert.ToString(coleccion["T-observ"]));
            string entregalima = Convert.ToString(coleccion["entrega-lima"]);
            string id_taller = Convert.ToString(coleccion["id_taller"]);
            string diasrep = Convert.ToString(coleccion["dias-rep"]);
            string Asesor = Convert.ToString(coleccion["Asesor"]);
            string Sede = Convert.ToString(coleccion["Sede"]);
            string Cobserv = MvcApplication.Replace(Convert.ToString(coleccion["C-observ"]));
            string Paños = Convert.ToString(coleccion["Paños"]);
            string repuestos = Convert.ToString(coleccion["Repuestos"]);
            string id_seguro = Convert.ToString(coleccion["cia_seguro"]);
            string servicio = Convert.ToString(coleccion["data-servicio"]);
            string foto = Convert.ToString(coleccion["foto"]);
            string cliente = Convert.ToString(coleccion["id_cliente"]);
            string fecha_solic_repues =  Convert.ToString(coleccion["fecha_solicitud_repues"]);
            string fecha_entrega_repues = Convert.ToString(coleccion["fecha_entrega_repues"]);
            string mat_pintura = Convert.ToString(coleccion["mat_pintura"]);

            Tobserv.Replace(",", "°");
            Cobserv.Replace(",", "°");
            
            lArray.Add(id); lArray.Add(placa); lArray.Add(fechaIng);
            lArray.Add(fechaAju); lArray.Add(fechaIni); lArray.Add(fechaAdua);
            lArray.Add(fechaApro); lArray.Add(fechaCotRep); lArray.Add(fechaEntreOC);
            lArray.Add(NroOrden); lArray.Add(id_trabajo); lArray.Add(hrschapa);
            lArray.Add(HrsPintura); lArray.Add(HrsArma); lArray.Add(HrsMecan);
            lArray.Add(Importe); lArray.Add(MontoChapa); lArray.Add(MontoPintura);
            lArray.Add(HHprepar); lArray.Add(MontoMecan); lArray.Add(cbProceso);
            lArray.Add(UtArmado); lArray.Add(UtMecan); lArray.Add(TFechaEstimada);
            lArray.Add(Tobserv); lArray.Add(cliente); lArray.Add(fecha_solic_repues);
            lArray.Add(entregalima); lArray.Add(fecha_entrega_repues); lArray.Add(mat_pintura);
            lArray.Add(id_taller); lArray.Add(Asesor); lArray.Add(Sede);
            lArray.Add(Cobserv); lArray.Add(Paños); lArray.Add(repuestos);
            lArray.Add(id_seguro);
            lArray.Add(servicio);lArray.Add(foto);  

            AutomecatronicaParametro oParametro = new AutomecatronicaParametro();

            oParametro.opcion = 4;
            oParametro.parametro = String.Join(",", lArray.ToArray());

            iRespuesta = new Automecatronica().InsertUpdate(oParametro);

            if (iRespuesta == 1)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"correcto\" }",
                    ContentType = "application/json"
                }.Content);
            }
            else {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"incorrecto\" }",
                    ContentType = "application/json"
                }.Content);
            }
        }

        /// <summary>
        /// Autor: Aldo D'Arrigo
        /// Fecha:24-08-2016
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AutomecatronicaUpdate(string a)
        {
            return View(new Automecatronica().Operacion(new AutomecatronicaParametro() { opcion = 5, parametro = a }));
        }

        /// <summary>
        /// Autor: Aldo D'Arrigo
        /// Fecha:24-08-2016
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AutomecatronicaUpdate(FormCollection coleccion)
        {
            int iRespuesta = 0;
            List<string> lArray = new List<string>();

            string id = Convert.ToString(coleccion["input-id"]);
            string placa = Convert.ToString(coleccion["Placa"]);
            string fechaIng = Convert.ToString(coleccion["IngresoH"]);
            string fechaAju = Convert.ToString(coleccion["FechaAju"]);
            string fechaIni = Convert.ToString(coleccion["InicioPro"]);
            string fechaAdua = Convert.ToString(coleccion["FechaAdua"]);
            string fechaApro = Convert.ToString(coleccion["FechaAprob"]);
            string fechaCotRep = Convert.ToString(coleccion["CotiRep"]);
            string fechaEntreOC = Convert.ToString(coleccion["EntregaOC"]);
            string NroOrden = Convert.ToString(coleccion["NroOrden"]);
            string trabajos = Convert.ToString(coleccion["Trabajos"]);
            string hrschapa = Convert.ToString(coleccion["HrsChapa"]);
            string HrsPintura = Convert.ToString(coleccion["HrsPintura"]);
            string HrsArma = Convert.ToString(coleccion["HrsArma"]);
            string HrsMecan = Convert.ToString(coleccion["HrsMecan"]);
            string Importe = Convert.ToString(coleccion["Importe"]);
            string MontoChapa = Convert.ToString(coleccion["MontoChapa"]);
            string MontoPintura = Convert.ToString(coleccion["MontoPintura"]);
            string HHprepar = Convert.ToString(coleccion["HHprepar"]);
            string MontoMecan = Convert.ToString(coleccion["MontoMecan"]);
            string cbProceso = Convert.ToString(coleccion["cbProceso"]);
            string UtArmado = Convert.ToString(coleccion["Ut-Armado"]);
            string UtMecan = Convert.ToString(coleccion["Ut-Mecan"]);
            string TFechaEstimada = Convert.ToString(coleccion["T-FechaEstimada"]);
            string Tobserv = Convert.ToString(coleccion["T-observ"]);
            string permtaller = Convert.ToString(coleccion["perm-taller"]);
            string diasaprob = Convert.ToString(coleccion["dias-aprob"]);
            string entregalima = Convert.ToString(coleccion["entrega-lima"]);
            string diaspptpo = Convert.ToString(coleccion["dias-pptpo"]);
            string diasoc = Convert.ToString(coleccion["dias-oc"]);
            string diaspptporep = Convert.ToString(coleccion["dias-pptpo-rep"]);
            string diasrep = Convert.ToString(coleccion["dias-rep"]);
            string Asesor = Convert.ToString(coleccion["Asesor"]);
            string Sede = Convert.ToString(coleccion["Sede"]);
            string Cobserv = Convert.ToString(coleccion["C-observ"]);
            string Paños = Convert.ToString(coleccion["Paños"]);
            string repuestos = Convert.ToString(coleccion["Repuestos"]);
            string thoras_chapa = Convert.ToString(coleccion["T-HrsChapa"]);
            string thoras_pint = Convert.ToString(coleccion["T-HrsPint"]);
            string id_seguro = Convert.ToString(coleccion["cia_seguro"]);
            string servicio = Convert.ToString(coleccion["data-servicio"]);


            lArray.Add(id); lArray.Add(placa); lArray.Add(fechaIng);
            lArray.Add(fechaAju); lArray.Add(fechaIni); lArray.Add(fechaAdua);
            lArray.Add(fechaApro); lArray.Add(fechaCotRep); lArray.Add(fechaEntreOC);
            lArray.Add(NroOrden); lArray.Add(trabajos); lArray.Add(hrschapa);
            lArray.Add(HrsPintura); lArray.Add(HrsArma); lArray.Add(HrsMecan);
            lArray.Add(Importe); lArray.Add(MontoChapa); lArray.Add(MontoPintura);
            lArray.Add(HHprepar); lArray.Add(MontoMecan); lArray.Add(cbProceso);
            lArray.Add(UtArmado); lArray.Add(UtMecan); lArray.Add(TFechaEstimada);
            lArray.Add(Tobserv); lArray.Add(permtaller); lArray.Add(diasaprob);
            lArray.Add(entregalima); lArray.Add(diaspptpo); lArray.Add(diasoc);
            lArray.Add(diaspptporep); lArray.Add(diasrep); lArray.Add(Asesor);
            lArray.Add(Sede); lArray.Add(Cobserv); lArray.Add(Paños);
            lArray.Add(repuestos); lArray.Add(thoras_chapa); lArray.Add(thoras_pint);
            lArray.Add(id_seguro);
            lArray.Add(servicio);

            AutomecatronicaParametro oParametro = new AutomecatronicaParametro();

            oParametro.opcion = 4;
            oParametro.parametro = String.Join(",", lArray.ToArray());

            iRespuesta = new Automecatronica().InsertUpdate(oParametro);

            if (iRespuesta == 1)
            {
                //Response.Redirect("Automecatronica/Automecatronica");
                 RedirectToAction("Automecatronica", "Automecatronica");
            }

            return View();
        }

        public ActionResult ValidaPlaca(string placa)
        {
            AutomecatronicaParametro oParametro = new AutomecatronicaParametro();

            oParametro.opcion = 10;
            oParametro.parametro = placa;
            int iRespuesta = 0;
            iRespuesta = new Automecatronica().InsertUpdate(oParametro);

            if (iRespuesta == 1)
            {
                return Json("existe", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("noexiste", JsonRequestBehavior.AllowGet);
                
            }

        }

        /// <summary>
        /// Autor: Aldo D'Arrigo
        /// Fecha:24-08-2016
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AutomecatronicaVehiculo(string a)
        {
            //return View(new Automecatronica().Operacion(new AutomecatronicaParametro() { opcion = 5, parametro = a }));
            return View();
        }

        /// <summary>
        /// Autor: Aldo D'Arrigo
        /// Fecha:24-08-2016
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AutomecatronicaVehiculo(FormCollection coleccion)
        {
            int iRespuesta = 0;
            List<string> lArray = new List<string>();

            string placa = Convert.ToString(coleccion["nroplaca"]);
            string color = Convert.ToString(coleccion["color"]);
            string marca = Convert.ToString(coleccion["marca"]);
            string modelo = Convert.ToString(coleccion["modelo"]);
            string nro_chasis = Convert.ToString(coleccion["nrochasis"]);
           

            lArray.Add(placa); lArray.Add(color); lArray.Add(marca);
            lArray.Add(modelo); lArray.Add(nro_chasis);
            AutomecatronicaParametro oParametro = new AutomecatronicaParametro();

            oParametro.opcion = 6;
            oParametro.parametro = String.Join(",", lArray.ToArray());

            iRespuesta = new Automecatronica().InsertUpdate(oParametro);

            if (iRespuesta == 1)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"correcto\" }",
                    ContentType = "application/json"
                }.Content);
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"incorrecto\" }",
                    ContentType = "application/json"
                }.Content);
            }

       


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CargaFoto(HttpPostedFileBase __a)
        {
            string archivoNombre;
            string pathArchivo;

            archivoNombre = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now) + Path.GetExtension(__a.FileName);
            pathArchivo = Path.Combine(LocalFoto, "Automecatronica", archivoNombre);

            try
            {
                #region Crea imagen
                __a.SaveAs(pathArchivo);
                
                #endregion

                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"" + archivoNombre + "\", \"_b\":true,\"_c\":\"Ok\" }",
                    ContentType = "application/json"
                }.Content);
            }
            catch (Exception e)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"\", \"_b\":false,\"_c\":\"" + e.Message + "\" }",
                    ContentType = "application/json"
                }.Content);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// Autor:Aldo D'Arrigo
        /// Fecha:02-09-2016
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AutomecatronicaExportableExcel(string param)
        {
            string nom_archivo = "";
            string ruta_destino = "";
            int fila=0;

            List<AutomecatronicaLista> lServicio = new Automecatronica().Lista(new AutomecatronicaParametro() { opcion = 0, parametro = "4" });
            AutomecatronicaParametro oParametro = new AutomecatronicaParametro() { opcion = 3, parametro = param };
            List<Automecatronica> lObj = new Automecatronica().ListaAutomecatronica(oParametro);

            nom_archivo = DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";
            ruta_destino =Path.Combine(LocalTemp, nom_archivo);
      
            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(ruta_destino)))
            {
                Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Data");

                int col = 1;
                int col2 = 1;

                oWs.Cells[1, col++].Value = "Nro";
                oWs.Cells[1, col++].Value = "F.Ingreso";
                oWs.Cells[1, col++].Value = "F.Audatex";
                oWs.Cells[1, col++].Value = "F.Cot.Repuestos";
                oWs.Cells[1, col++].Value = "F.Ajuste";
                oWs.Cells[1, col++].Value = "F.Aprob";
                oWs.Cells[1, col++].Value = "O.Compra";
                oWs.Cells[1, col++].Value = "I.Proceso";
                oWs.Cells[1, col++].Value = "Marca";
                oWs.Cells[1, col++].Value = "Modelo";
                oWs.Cells[1, col++].Value = "Color";
                oWs.Cells[1, col++].Value = "Placa";
                oWs.Cells[1, col++].Value = "Proceso";
                oWs.Cells[1, col++].Value = "F.Estimada";
                oWs.Cells[1, col++].Value = "Asesor";
                oWs.Cells[1, col++].Value = "Taller";
                oWs.Cells[1, col++].Value = "Importe";
                oWs.Cells[1, col++].Value = "Cliente";
                oWs.Cells[1, col++].Value = "Nro.Orden";
                oWs.Cells[1, col++].Value = "Cia.Seg";
                oWs.Cells[1, col++].Value = "F.Solic.Rep.";
                oWs.Cells[1, col++].Value = "F.Entrega.Rep";
                oWs.Cells[1, col++].Value = "Trabajo";
                oWs.Cells[1, col++].Value = "Margen Total";
                oWs.Cells[1, col++].Value = "Horas Chapa";
                oWs.Cells[1, col++].Value = "Horas Pintura";
                oWs.Cells[1, col++].Value = "Horas Mecanica";
                oWs.Cells[1, col++].Value = "Horas Armado";
                oWs.Cells[1, col++].Value = "Repuestos";
                oWs.Cells[1, col++].Value = "Monto Chapa";
                oWs.Cells[1, col++].Value = "Monto Pintura";
                oWs.Cells[1, col++].Value = "Monto Mecanica";
                oWs.Cells[1, col++].Value = "HH.Prepar";
                oWs.Cells[1, col++].Value = "T.Horas Chapa";
                oWs.Cells[1, col++].Value = "T.Horas Pintura";
                oWs.Cells[1, col++].Value = "UT.Armado";
                oWs.Cells[1, col++].Value = "UT.Mecanica";
                oWs.Cells[1, col++].Value = "Paños";
                oWs.Cells[1, col++].Value = "Costo.H.Mecanica";
                oWs.Cells[1, col++].Value = "Costo.H.Chapa";
                oWs.Cells[1, col++].Value = "Costo.H.Pintura";
                oWs.Cells[1, col++].Value = "Mat.Pintura";
                oWs.Cells[1, col++].Value = "Valor.Observaciones";
                oWs.Cells[1, col++].Value = "Perm.Taller";
                oWs.Cells[1, col++].Value = "Dias Aprob";
                oWs.Cells[1, col++].Value = "F.Entrega";
                oWs.Cells[1, col++].Value = "Elab.PPTPO";
                oWs.Cells[1, col++].Value = "Dias para Entrega";
                oWs.Cells[1, col++].Value = "Dias entrega OC";
                oWs.Cells[1, col++].Value = "Dias entrega Repuestos";
                oWs.Cells[1, col++].Value = "Sede";
                oWs.Cells[1, col++].Value = "Contacto Observaciones";

                fila = 2;

                foreach (Automecatronica oBj in lObj)
                {
                    col2 = 1;

                    oWs.Cells[fila, col2++].Value = oBj.orden;
                    oWs.Cells[fila, col2++].Value = oBj.fecha_Ing;
                    oWs.Cells[fila, col2++].Value = oBj.fecha_Audatex;
                    oWs.Cells[fila, col2++].Value = oBj.fecha_Cot_Rep;
                    oWs.Cells[fila, col2++].Value = oBj.fecha_Aju;
                    oWs.Cells[fila, col2++].Value = oBj.fecha_Aprob;
                    oWs.Cells[fila, col2++].Value = oBj.fecha_Entrega_Op;
                    oWs.Cells[fila, col2++].Value = oBj.fecha_Inicio;
                    oWs.Cells[fila, col2++].Value = oBj.ve_marca;
                    oWs.Cells[fila, col2++].Value = oBj.ve_modelo;
                    oWs.Cells[fila, col2++].Value = oBj.ve_color;
                    oWs.Cells[fila, col2++].Value = oBj.placa_Op;
                    oWs.Cells[fila, col2++].Value = oBj.valor_proceso;
                    oWs.Cells[fila, col2++].Value = oBj.valor_fecha_entrega;
                    oWs.Cells[fila, col2++].Value = oBj.cont_asesor;
                    oWs.Cells[fila, col2++].Value = oBj.nom_taller;
                    oWs.Cells[fila, col2++].Value = oBj.valor_impor;
                    oWs.Cells[fila, col2++].Value = oBj.nom_cliente;
                    oWs.Cells[fila, col2++].Value = oBj.nro_Orden;
                    oWs.Cells[fila, col2++].Value = oBj.cia_seguro;
                    oWs.Cells[fila, col2++].Value = oBj.fecha_solic_repues;
                    oWs.Cells[fila, col2++].Value = oBj.fecha_entrega_repues;
                    oWs.Cells[fila, col2++].Value = oBj.nom_trabajo;
                    oWs.Cells[fila, col2++].Value = oBj.margen_total;
                    oWs.Cells[fila, col2++].Value = oBj.valor_hor_chapa;
                    oWs.Cells[fila, col2++].Value = oBj.valor_hor_pint;
                    oWs.Cells[fila, col2++].Value = oBj.valor_hor_mecan;
                    oWs.Cells[fila, col2++].Value = oBj.valor_hor_arma;
                    oWs.Cells[fila, col2++].Value = oBj.valor_repuestos;
                    oWs.Cells[fila, col2++].Value = oBj.valor_monto_chapa;
                    oWs.Cells[fila, col2++].Value = oBj.valor_monto_pint;
                    oWs.Cells[fila, col2++].Value = oBj.valor_monto_mecan;
                    oWs.Cells[fila, col2++].Value = oBj.valor_hh_prepar;
                    oWs.Cells[fila, col2++].Value = oBj.thoras_chapa;
                    oWs.Cells[fila, col2++].Value = oBj.thoras_pintura;
                    oWs.Cells[fila, col2++].Value = oBj.valor_ut_armado;
                    oWs.Cells[fila, col2++].Value = oBj.valor_ut_mecan;
                    oWs.Cells[fila, col2++].Value = oBj.paños;
                    oWs.Cells[fila, col2++].Value = oBj.costo_mecanica;
                    oWs.Cells[fila, col2++].Value = oBj.costo_chapa;
                    oWs.Cells[fila, col2++].Value = oBj.costo_pintura;
                    oWs.Cells[fila, col2++].Value = oBj.mat_pintura;
                    oWs.Cells[fila, col2++].Value = oBj.valor_observ;
                    oWs.Cells[fila, col2++].Value = oBj.time_perm_taller;
                    oWs.Cells[fila, col2++].Value = oBj.time_dias_aprob;
                    oWs.Cells[fila, col2++].Value = oBj.time_entrega_lima;
                    oWs.Cells[fila, col2++].Value = oBj.time_pptpo;
                    oWs.Cells[fila, col2++].Value = oBj.dias_para_entrega;
                    oWs.Cells[fila, col2++].Value = oBj.time_entrega_oc;
                    oWs.Cells[fila, col2++].Value = oBj.time_entrega_repue;
                    oWs.Cells[fila, col2++].Value = oBj.cont_sede;
                    oWs.Cells[fila, col2++].Value = oBj.cont_observ;

                    foreach (AutomecatronicaLista oLista in lServicio) {
                        List<AutomecatronicaTecnico> lTecnico = (from a in oBj.tecnico where a.id_servicio == oLista.id select a).ToList();

                        if (lObj.First() == oBj)
                        {
                            oWs.Cells[1, ++col2].Value = oLista.texto;
                        }

                        if (lTecnico.Count > 0)
                        {
                            var oTecnico = lTecnico.First();
                            if (lObj.First() == oBj)
                            {
                                oWs.Cells[fila, col2].Value = oTecnico.tec_nomnbre;
                            }
                            else
                            {
                                oWs.Cells[fila, ++col2].Value = oTecnico.tec_nomnbre;
                            }
                        }
                    }

                    fila++;
                }

                oEx.Save();
            }

            return Content(new ContentResult
            {
                Content = "{ \"_a\":\"" + nom_archivo + "\" }",
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor:
        /// Fecha:
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public ActionResult Autocomplete(string term)
        {
            AutomecatronicaParametro oParametro = new AutomecatronicaParametro() { opcion = 7, parametro =term};
            List<Vehiculo> lObj = new Vehiculo().ListaVehiculo(oParametro);

            //string[] arrayPlacas ;
            List<string> lplaca = new List<string>();
                       

            return Json(lObj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Tecnicos

        public ActionResult Tecnicos()
        {
            return View();

        }

        [HttpPost]
        public ActionResult Tecnicos(string _a, string _b)
        {
            AutomecatronicaParametro oParametro = new AutomecatronicaParametro() { opcion = Convert.ToInt32(_a), parametro = _b };

            if (oParametro.opcion == 11)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Automecatronica().ListaTecnicosXservicio(oParametro)),
                    ContentType = "application/json"
                }.Content);
            }
        



            return View();
        }

        [HttpPost]
        public ActionResult GuardarTecnico(FormCollection form)
        {
            List<string> lArray = new List<string>();

            string pNombre = Convert.ToString(form["pNombre"]);
            string sNombre = Convert.ToString(form["sNombre"]);
            string pApellido = Convert.ToString(form["pApellido"]);
            string sApellido = Convert.ToString(form["sApellido"]);
            string servicio = Convert.ToString(form["servicio"]);

            string nombres = pNombre.ToUpper() + " " + sNombre.ToUpper() + " " + pApellido.ToUpper() + " " + sApellido.ToUpper();

            lArray.Add(servicio);
            lArray.Add(nombres);
     
            AutomecatronicaParametro oParametro = new AutomecatronicaParametro();

            oParametro.opcion = 12;
            oParametro.parametro = String.Join(",", lArray.ToArray());
            Int32 iRespuesta = 0;
            iRespuesta = new Automecatronica().InsertUpdate(oParametro);

            if (iRespuesta == 1)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"correcto\" }",
                    ContentType = "application/json"
                }.Content);
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"incorrecto\" }",
                    ContentType = "application/json"
                }.Content);
            }

        
        }

        [HttpPost]
        public ActionResult DesabilitarTecnicos(string _a)
        {


             AutomecatronicaParametro oParametro = new AutomecatronicaParametro();

            oParametro.opcion = 13;
            oParametro.parametro = _a;
            Int32 iRespuesta = 0;
            iRespuesta = new Automecatronica().InsertUpdate(oParametro);


            if (iRespuesta == 1)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"__a\":\"correcto\" }",
                    ContentType = "application/json"
                }.Content);
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = "{ \"__a\":\"incorrecto\" }",
                    ContentType = "application/json"
                }.Content);
            }
        }

        #endregion 


        #region Cálculos

        public ActionResult Calculos()
        {
            return View();
        
        }

        [HttpPost]
        public ActionResult Calculos(FormCollection form)
        {     
            List<string> lArray = new List<string>();
            string tipo = Convert.ToString(form["tipo"]);
            string montoSeguroChapa = Convert.ToString(form["montoSeguroChapa"]);
            string montoSeguroPintura = Convert.ToString(form["montoSeguroPintura"]);
            string montoSeguroMecanica = Convert.ToString(form["montoSeguroMecanica"]);
            string precioChapa = Convert.ToString(form["precioChapa"]);
            string precioPaños = Convert.ToString(form["precioPaños"]);
            string precioPintura = Convert.ToString(form["precioPintura"]);
            string precioMecanica = Convert.ToString(form["precioMecanica"]);
            string porcentajeChapa = Convert.ToString(form["porcentajeChapa"]);
            string porcentajeArmado = Convert.ToString(form["porcentajeArmado"]);
            string formulaPintura = Convert.ToString(form["formulaPintura"]);

            lArray.Add(tipo); lArray.Add(montoSeguroChapa); lArray.Add(montoSeguroPintura); lArray.Add(montoSeguroMecanica);
            lArray.Add(precioChapa); lArray.Add(precioPaños); lArray.Add(precioPintura); lArray.Add(precioMecanica); lArray.Add(porcentajeChapa);
            lArray.Add(porcentajeArmado); lArray.Add(formulaPintura);

            AutomecatronicaParametro oParametro = new AutomecatronicaParametro();

            oParametro.opcion = 14;
            oParametro.parametro = String.Join(",", lArray.ToArray());
            Int32 iRespuesta = 0;
            iRespuesta = new Automecatronica().InsertUpdate(oParametro);

            if (iRespuesta == 1)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"correcto\" }",
                    ContentType = "application/json"
                }.Content);
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"incorrecto\" }",
                    ContentType = "application/json"
                }.Content);
            }

        }
        #endregion

        [HttpPost]
        public ActionResult ObtenerCalculos(string _a , string _b)
        {
            AutomecatronicaParametro oParametro = new AutomecatronicaParametro() { opcion = Convert.ToInt32(_a), parametro = _b };
            if (oParametro.opcion == 15)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Automecatronica().Lista(oParametro)),
                    ContentType = "application/json"
                }.Content);
            }

            return View();
        }
    }
}