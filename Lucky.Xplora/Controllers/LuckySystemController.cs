﻿using System;
using System.Collections.Generic;
using System.Configuration;
using IO = System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.LuckySystem;
using Lucky.Xplora.Models.Administrador;

namespace Lucky.Xplora.Controllers
{
    public class LuckySystemController : Controller
    {
        //
        // GET: /LuckySystem/

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-03-18
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        public ActionResult Explora(string path)
        {
            //Session["Session_Login"] = new Persona() { 
            //    Company_id = 1562
            //};

            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
                       

            string _ruta = ConfigurationManager.AppSettings["DirectorioArchivo"] + (path == null ? path : path.Replace("___", "."));

            if (IO.File.Exists(_ruta))
            {
                return base.File(_ruta, "application/octet-stream");
            }
            else if (IO.Directory.Exists(_ruta))
            {
                List<Folder> ListFolder = new List<Folder>();

                IEnumerable<string> dirList = IO.Directory.EnumerateDirectories(_ruta);
                foreach (string dir in dirList)
                {
                    IO.DirectoryInfo d = new IO.DirectoryInfo(dir);

                    Folder folder = new Folder();

                    folder.Nombre = IO.Path.GetFileName(dir);
                    folder.Fecha = d.LastAccessTime;

                    ListFolder.Add(folder);
                }


                List<Archivo> ListArchivo = new List<Archivo>();

                IEnumerable<string> fileList = IO.Directory.EnumerateFiles(_ruta);
                foreach (string file in fileList)
                {
                    IO.FileInfo f = new IO.FileInfo(file);

                    Archivo archivo = new Archivo();

                    if (f.Extension.ToLower() != "php" && f.Extension.ToLower() != "aspx" && f.Extension.ToLower() != "asp")
                    {
                        archivo.NombreExtension = IO.Path.GetFileName(file);
                        archivo.Nombre = IO.Path.GetFileNameWithoutExtension(file);
                        archivo.Extension = IO.Path.GetExtension(file);
                        archivo.Fecha = f.LastAccessTime;
                        archivo.Tamaño = (f.Length < 1024) ? f.Length.ToString() + " B" : f.Length / 1024 + " KB";

                        ListArchivo.Add(archivo);
                    }
                }

                Explorador explorerModel = new Explorador(ListFolder, ListArchivo, path);

                return View(explorerModel);
            }
            else
            {
                return Content(path + " is not a valid file or directory.");
            }
        }

        [HttpGet]
        public ActionResult Descarga(string path)
        {
            string _ruta = ConfigurationManager.AppSettings["DirectorioArchivo"] + path.Replace("___",".");

            IO.FileInfo f = new IO.FileInfo(_ruta);

            return base.File(_ruta, "application/octet-stream", f.Name);
        }
    }
}