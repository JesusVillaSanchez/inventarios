﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Encuesta;

namespace Lucky.Xplora.Controllers
{
    public class EncuestaController : Controller
    {
        #region Encuesta
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-14
        /// </summary>
        /// <returns></returns>
        public ActionResult Inicio()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-11
        /// </summary>
        /// <returns></returns>
        public ActionResult Encuesta()
        {
            if (Request.QueryString["__a"] != null || Request.QueryString["__a"] == "")
            {
                ViewBag.enc_id =  Convert.ToInt32(Request.QueryString["__a"]);
            }

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-11
        /// </summary>
        /// <returns></returns>
        public ActionResult Resultado() {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-14
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Graba(string __a)
        {
            var iResultado = 0;

            List<Encuesta_Respuesta> lRespuesta = MvcApplication._Deserialize<List<Encuesta_Respuesta>>(__a);

            foreach (var oRespuesta in lRespuesta) {
                oRespuesta.ere_fecha = DateTime.Now;
                oRespuesta.ere_estado = 1;
            }

            iResultado = new Encuesta().Graba(new Request_Encuesta_Graba() { Lista = lRespuesta });

            return Content(new ContentResult {
                Content = "{ \"__a\": " + iResultado + " }",
                    ContentType = "application/json"
                }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-14
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Puntaje(string __a, string __b) {
            return View(
                    new Encuesta().Puntaje(new Request_Encuesta() {
                        persona = Convert.ToInt32(__a),
                        encuesta = Convert.ToInt32(__b),
                        opcion = 2
                    })
                );
        }
        #endregion
    }
}
