﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
//using System.Web.Script.Serialization;

using System.Drawing;
using System.Drawing.Imaging;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using System.IO;
using System.Net;

using Lucky.Xplora.Models.Map;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Administrador;
using Lucky.Xplora.Security;

using System.Data.SqlClient;
//using System.Configuration;
namespace Lucky.Xplora.Controllers
{
    public class MapsController : Controller
    {
        public static List<E_Reportes_StockOut_MapBK> objListRepOperativo;
        public static List<E_Reportes_RangPDV_StockOut_MapBK> objListRepOpeRangPDV;
        public static List<Ranking_Sku> objListRepOpeRangSKU;
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

        #region Xplora Maps

        //
        // GET: /Maps/

        public ActionResult Inicio()
        {
            ViewBag.veq = "002892382010";
            ViewBag.url_img_map = ConfigurationManager.AppSettings["url_foto_maps"]; ;
            return View();
        }

        public ActionResult Home(string _a)
        {
            //ViewBag.veq = "002892382010";
            ViewBag.veq =  _a;
            ViewBag.url_img_map = ConfigurationManager.AppSettings["url_foto_maps"]; ;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult GetJsonCapa(int _vgrupo)
        {
            MapJson Obj_json = new MapJson();
            Response_MapJson oResponse = new Response_MapJson();
            Request_MapJson oRequest = new Request_MapJson();
            oRequest.compania = 1562;
            oRequest.canal = 1023;
            oRequest.grupo= _vgrupo;

            oResponse.Objeto = Obj_json.Objeto(oRequest);

            //var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
            //var result = new ContentResult
            //{
            //    Content = serializer.Serialize(oResponse.Objeto),
            //    ContentType = "application/json"
            //};
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(oResponse.Objeto),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-28
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Cobertura(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __k)
        {
            return View(new AlicorpMinoristaCobertura()
                    .Cobertura(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = __h,
                        tipoGiro = Convert.ToInt32(__k)

                    })
                );         
            //return View(new AlicorpMinoristaCobertura()
            //        .Cobertura(new Resquest_AlicorpMinoristaCobertura()
            //        {
            //            campania = "002892382010",
            //            grupo = 2,
            //            ubigeo = "589,15",
            //            anio = 2015,
            //            mes = 5,
            //            periodo = 32138,
            //            giro = 0,
            //            pdv = ""
            //        })
            //    );
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-05-28
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CoberturaPdv(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            return Content(new ContentResult{
                Content = MvcApplication._Serialize(new AlicorpMinoristaCobertura()
                    .Pdv(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = __h,
                        cluster = Convert.ToInt32(__i),
                        segmento = Convert.ToInt32(__j),
                        tipoGiro = Convert.ToInt32(__k)
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-09-11
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        public JsonResult CoberturaPdvExcel(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            string LocalTemp;
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;
            LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

            _fileServer = String.Format("Reporte_Cobertura_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Cobertura >>

                List<AlicorpMinoristaCoberturaExcel> oCobertura = new AlicorpMinoristaCoberturaExcel().PdvExcel(
                   new Resquest_AlicorpMinoristaCobertura()
                   {
                       campania = __a,
                       grupo = Convert.ToInt32(__b),
                       ubigeo = __c,
                       anio = Convert.ToInt32(__d),
                       mes = Convert.ToInt32(__e),
                       periodo = Convert.ToInt32(__f),
                       giro = Convert.ToInt32(__g),
                       pdv = __h,
                       cluster = Convert.ToInt32(__i),
                       segmento = Convert.ToInt32(__j),
                       tipoGiro = Convert.ToInt32(__k)
                   });

                #endregion

                #endregion

                #region <<< Reporte Cobertura >>>
                if ((oCobertura != null))
                {
                    Excel.ExcelWorksheet oWsCobertura = oEx.Workbook.Worksheets.Add("Rep_Cobertura");
                    oWsCobertura.Cells[1, 1].Value = "ULTIMA_VISITA";
                    oWsCobertura.Cells[1, 2].Value = "COD_PDV";
                    oWsCobertura.Cells[1, 3].Value = "NOMBRE_PDV";
                    oWsCobertura.Cells[1, 4].Value = "DEX";
                    oWsCobertura.Cells[1, 5].Value = "DISTRITO";
                    oWsCobertura.Cells[1, 6].Value = "CIUDAD";
                    oWsCobertura.Cells[1, 7].Value = "GIRO";
                    oWsCobertura.Cells[1, 8].Value = "GIE";
                    oWsCobertura.Cells[1, 9].Value = "PROGRAMA";

                    _fila = 2;
                    foreach (AlicorpMinoristaCoberturaExcel oBj in oCobertura)
                    {

                        oWsCobertura.Cells[_fila, 1].Value = oBj.fech_ini;  //Ultima Visita CH 16/09/2015
                        oWsCobertura.Cells[_fila, 2].Value = oBj.pdv_cod;
                        oWsCobertura.Cells[_fila, 3].Value = oBj.pdv_desc;
                        oWsCobertura.Cells[_fila, 4].Value = oBj.distribuidora;
                        oWsCobertura.Cells[_fila, 5].Value = oBj.distrito;
                        oWsCobertura.Cells[_fila, 6].Value = oBj.ciudad;
                        oWsCobertura.Cells[_fila, 7].Value = oBj.giro;
                        oWsCobertura.Cells[_fila, 8].Value = oBj.nom_gie; //Gie add Ch 16/09/2015
                        //oWsPrecio.Cells[_fila, 8].Value = oBj.cat_descripcion;
                        oWsCobertura.Cells[_fila, 9].Value = oBj.seg_descripcion;

                        _fila++;
                    }

                    //Formato Cabecera
                    oWsCobertura.SelectedRange[1, 1, 1, 9].AutoFilter = true;
                    oWsCobertura.Row(1).Height = 25;
                    oWsCobertura.Row(1).Style.Font.Bold = true;
                    oWsCobertura.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsCobertura.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsCobertura.Column(1).AutoFit();
                    oWsCobertura.Column(2).AutoFit();
                    oWsCobertura.Column(3).AutoFit();
                    oWsCobertura.Column(4).AutoFit();
                    oWsCobertura.Column(5).AutoFit();
                    oWsCobertura.Column(6).AutoFit();
                    oWsCobertura.Column(7).AutoFit();
                    oWsCobertura.Column(8).AutoFit();
                    oWsCobertura.Column(9).AutoFit();

                    oWsCobertura.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Presencia(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m)
        {
            return View(new AlicorpMinoristaPresenciaPdv()
                    .Presencia(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //grupo = 2,
                        //ubigeo = "589,15",
                        //anio = 2015,
                        //mes = 5,
                        //periodo = 0,
                        //giro = 0,
                        //pdv = "",
                        //categoria = 12,
                        //marca = 44,
                        //segmento = 17,
                        //producto = "",
                        //opcion = 0
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = "",
                        categoria = Convert.ToInt32(__i),
                        marca = Convert.ToInt32(__j),
                        segmento = Convert.ToInt32(__k),
                        producto = __l,
                        opcion = 0,
                        tipoGiro = Convert.ToInt32(__m)
                    })
                );
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PresenciaPdv(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpMinoristaPresenciaPdv()
                    .Pdv(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //grupo = 2,
                        //ubigeo = "589,15",
                        //anio = 2015,
                        //mes = 5,
                        //periodo = 0,
                        //giro = 0,
                        //pdv = "",
                        //categoria = 12,
                        //marca = 44,
                        //segmento = 17,
                        //producto = "SMA00133",
                        //opcion = 1
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = "",
                        categoria = Convert.ToInt32(__i),
                        marca = Convert.ToInt32(__j),
                        segmento = Convert.ToInt32(__k),
                        producto = __l,
                        opcion = 1,
                        tipoGiro = Convert.ToInt32(__m)
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: rcontreras
        /// Fecha: 2015-09-11
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        public JsonResult PresenciaPdvExcel(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Presencia_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Presencia >>

                List<AlicorpMinoristaPresenciaPdvExcel> oPresencia = new AlicorpMinoristaPresenciaPdv().Descarga(
                   new Resquest_AlicorpMinoristaCobertura()
                   {
                       campania = __a,
                       grupo = Convert.ToInt32(__b),
                       ubigeo = __c,
                       anio = Convert.ToInt32(__d),
                       mes = Convert.ToInt32(__e),
                       periodo = Convert.ToInt32(__f),
                       giro = Convert.ToInt32(__g),
                       pdv = "",
                       categoria = Convert.ToInt32(__i),
                       marca = Convert.ToInt32(__j),
                       segmento = Convert.ToInt32(__k),
                       producto = __l,
                       opcion = 3,
                       tipoGiro = Convert.ToInt32(__m)
                   });

                #endregion

                #endregion

                #region <<< Reporte Presencia >>>
                if ((oPresencia != null))
                {
                    Excel.ExcelWorksheet oWsPresencia = oEx.Workbook.Worksheets.Add("Precio y Presencia");
                    oWsPresencia.Cells[1, 1].Value = "ULTIMA_VISITA";
                    oWsPresencia.Cells[1, 2].Value = "COD PDV";
                    oWsPresencia.Cells[1, 3].Value = "NOMBRE PDV";
                    oWsPresencia.Cells[1, 4].Value = "DISTRITO";
                    oWsPresencia.Cells[1, 5].Value = "OFICINA";
                    oWsPresencia.Cells[1, 6].Value = "GIRO";
                    oWsPresencia.Cells[1, 7].Value = "GIE";
                    oWsPresencia.Cells[1, 8].Value = "SEGMENTO";
                    oWsPresencia.Cells[1, 9].Value = "CATEGORIA";
                    oWsPresencia.Cells[1, 10].Value = "MARCA";
                    oWsPresencia.Cells[1, 11].Value = "SKU";
                    oWsPresencia.Cells[1, 12].Value = "PRODUCTO";
                    oWsPresencia.Cells[1, 13].Value = "PRESENCIA";


                    _fila = 2;
                    foreach (AlicorpMinoristaPresenciaPdvExcel oBj in oPresencia)
                    {

                        oWsPresencia.Cells[_fila, 1].Value = oBj.fec_ini;
                        oWsPresencia.Cells[_fila, 2].Value = oBj.pdv_cod;
                        oWsPresencia.Cells[_fila, 3].Value = oBj.pdv_desc;
                        oWsPresencia.Cells[_fila, 4].Value = oBj.distrito;
                        oWsPresencia.Cells[_fila, 5].Value = oBj.oficina;
                        oWsPresencia.Cells[_fila, 6].Value = oBj.giro;
                        oWsPresencia.Cells[_fila, 7].Value = oBj.gie;
                        oWsPresencia.Cells[_fila, 8].Value = oBj.segmento;
                        oWsPresencia.Cells[_fila, 9].Value = oBj.categoria;
                        oWsPresencia.Cells[_fila, 10].Value = oBj.marca;
                        oWsPresencia.Cells[_fila, 11].Value = oBj.sku;
                        oWsPresencia.Cells[_fila, 12].Value = oBj.producto;
                        oWsPresencia.Cells[_fila, 13].Value = oBj.presencia;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPresencia.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    oWsPresencia.Row(1).Height = 25;
                    oWsPresencia.Row(1).Style.Font.Bold = true;
                    oWsPresencia.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPresencia.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPresencia.Column(1).AutoFit();
                    oWsPresencia.Column(2).AutoFit();
                    oWsPresencia.Column(3).AutoFit();
                    oWsPresencia.Column(4).AutoFit();
                    oWsPresencia.Column(5).AutoFit();
                    oWsPresencia.Column(6).AutoFit();
                    oWsPresencia.Column(7).AutoFit();
                    oWsPresencia.Column(8).AutoFit();
                    oWsPresencia.Column(9).AutoFit();
                    oWsPresencia.Column(10).AutoFit();

                    oWsPresencia.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }

        //[HttpPost]
        //public JsonResult PresenciaPdvExcel(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m)
        //{
        //    //string _fileServer = "";
        //    //string _filePath = "";
        //    //int _fila = 0;

        //    //_fileServer = String.Format("presencia_pdv_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
        //    //_filePath = System.IO.Path.Combine(ruta + "/Temp", _fileServer);

        //    //FileInfo _fileNew = new FileInfo(_filePath);

        //    //using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
        //    //{
        //    //    #region <<< Instancias >>>
        //    //    #region << Reporte Consolidado >>
        //    //    AlicorpMinoristaPresenciaPdv oConsolidado = new AlicorpMinoristaPresenciaPdv().Pdv(
        //    //        new Resquest_AlicorpMinoristaCobertura()
        //    //        {
        //    //            campania = __a,
        //    //            grupo = Convert.ToInt32(__b),
        //    //            ubigeo = __c,
        //    //            anio = Convert.ToInt32(__d),
        //    //            mes = Convert.ToInt32(__e),
        //    //            periodo = Convert.ToInt32(__f),
        //    //            giro = Convert.ToInt32(__g),
        //    //            pdv = "",
        //    //            categoria = Convert.ToInt32(__i),
        //    //            marca = Convert.ToInt32(__j),
        //    //            segmento = Convert.ToInt32(__k),
        //    //            producto = __l,
        //    //            opcion = 1,
        //    //            tipoGiro = Convert.ToInt32(__m)
        //    //        }
        //    //    );
        //    //    #endregion
        //    //    #endregion

        //    //    #region <<< Reporte PRESENCIA PDV >>>


        //    //    if ((oConsolidado != null) || (oConsolidado.Count > 0))
        //    //    {
        //    //    Excel.ExcelWorksheet oWsConsolidado = oEx.Workbook.Worksheets.Add("ReporteConsolidado_" + nom_hoja);
             

        //    //        oWsConsolidado.Cells[3, cnt_cant].Value = total;
        //    //        oWsConsolidado.Cells[4, cnt_max].Value = max;

                

        //    //        foreach (ColgateConsolidado_DataBase oBj in oConsolidado)
        //    //        {

        //    //            string v_imprime = "";
        //    //            v_imprime = oBj.tmp_cabecera.ToString();
        //    //            if (v_imprime == "Id" ||
        //    //                v_imprime == "Fecha" ||
        //    //                v_imprime == "Ciudad" ||
        //    //                v_imprime == "Supervisor" ||
        //    //                v_imprime == "Mercado" ||
        //    //                v_imprime == "P.D.V." ||
        //    //                v_imprime == "Cliente")
        //    //            {
        //    //                v_imprime = v_imprime.ToUpper();
        //    //                oWsConsolidado.Cells[7, cnt2].Value = v_imprime;
        //    //                cnt2++;
        //    //            }
        //    //        }

                  

        //    //        oWsConsolidado.Cells.AutoFitColumns();
        //    //        oWsConsolidado.Row(1).Style.Font.Bold = true;
        //    //        oWsConsolidado.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
        //    //        oWsConsolidado.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

        //    //        oWsConsolidado.Row(2).Style.Font.Bold = true;
        //    //        oWsConsolidado.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
        //    //        oWsConsolidado.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

        //    //        oWsConsolidado.Row(7).Style.Font.Bold = true;
        //    //        oWsConsolidado.Row(7).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
        //    //        oWsConsolidado.Row(7).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

        //    //    }
        //    //}
        //    //    #endregion
        //    //    oEx.Save();
        //    //}

        //    //return Json(new { Archivo = _fileServer });
        //}

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PresenciaSku(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m)
        {
            return View(new AlicorpMinoristaPresenciaPdv()
                    .Sku(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //grupo = 2,
                        //ubigeo = "589,15",
                        //anio = 2015,
                        //mes = 5,
                        //periodo = 0,
                        //giro = 0,
                        //pdv = "1001085",
                        //categoria = 12,
                        //marca = 44,
                        //segmento = 17,
                        //producto = "",
                        //opcion = 2
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = __h,
                        categoria = Convert.ToInt32(__i),
                        marca = Convert.ToInt32(__j),
                        segmento = Convert.ToInt32(__k),
                        producto = "",
                        opcion = 2,
                        tipoGiro = Convert.ToInt32(__m)
                    }));

            //return Content(new ContentResult
            //{
            //    Content = MvcApplication._Serialize(new AlicorpMinoristaPresencia()
            //        .Sku(new Resquest_AlicorpMinoristaCobertura()
            //        {
            //            campania = "002892382010",
            //            grupo = 2,
            //            ubigeo = "589,15",
            //            anio = 2015,
            //            mes = 5,
            //            periodo = 0,
            //            giro = 0,
            //            pdv = "1001085",
            //            categoria = 12,
            //            marca = 44,
            //            segmento = 17,
            //            producto = "",
            //            opcion = 2
            //            //campania = __a,
            //            //grupo = Convert.ToInt32(__b),
            //            //ubigeo = __c,
            //            //anio = Convert.ToInt32(__d),
            //            //mes = Convert.ToInt32(__e),
            //            //periodo = Convert.ToInt32(__f),
            //            //giro = Convert.ToInt32(__g),
            //            //pdv = __h,
            //            //categoria = Convert.ToInt32(__i),
            //            //marca = Convert.ToInt32(__j),
            //            //segmento = Convert.ToInt32(__k),
            //            //producto = __l,
            //            //opcion = 2
            //        })),
            //    ContentType = "application/json"
            //}.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Precio(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m)
        {
            return View(new AlicorpMinoristaPrecioPdv()
                    .Precio(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //grupo = 2,
                        //ubigeo = "589,15",
                        //anio = 2015,
                        //mes = 5,
                        //periodo = 0,
                        //giro = 0,
                        //pdv = "",
                        //categoria = 12,
                        //marca = 44,
                        //segmento = 17,
                        //producto = "",
                        //opcion = 0
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = "",
                        categoria = Convert.ToInt32(__i),
                        marca = Convert.ToInt32(__j),
                        segmento = Convert.ToInt32(__k),
                        producto = "",
                        opcion = 0,
                        tipoGiro = Convert.ToInt32(__m)
                    })
                );
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        public JsonResult ExportarprecioAlicorpMenor(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m, string __n)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Presencia_Precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Presencia Precio >>

                List<AlicorpMinoristaExportaPrecio> oPrecio = new AlicorpMinoristaPrecioPdv().ExportaPrecio(
                   new Resquest_AlicorpMinoristaCobertura()
                   {
                       campania = __a,
                       grupo = Convert.ToInt32(__b),
                       ubigeo = __c,
                       anio = Convert.ToInt32(__d),
                       mes = Convert.ToInt32(__e),
                       periodo = Convert.ToInt32(__f),
                       giro = Convert.ToInt32(__g),
                       pdv = "",
                       categoria = Convert.ToInt32(__i),
                       marca = Convert.ToInt32(__j),
                       segmento = Convert.ToInt32(__k),
                       producto = Convert.ToString(__n),
                       opcion = 3,
                       tipoGiro = Convert.ToInt32(__m)
                   });

                #endregion

                #endregion

                #region <<< Reporte Presencia Precio >>>
                if ((oPrecio != null))
                {
                    Excel.ExcelWorksheet oWsPrecio = oEx.Workbook.Worksheets.Add("Rep_Presencia_Precio");
                    oWsPrecio.Cells[1, 1].Value = "ULTIMA_VISITA";
                    oWsPrecio.Cells[1, 2].Value = "COD_PDV";
                    oWsPrecio.Cells[1, 3].Value = "NOMBRE_PDV";
                    oWsPrecio.Cells[1, 4].Value = "DISTRITO";
                    oWsPrecio.Cells[1, 5].Value = "CIUDAD";
                    oWsPrecio.Cells[1, 6].Value = "GIRO";
                    oWsPrecio.Cells[1, 7].Value = "GIE";
                    oWsPrecio.Cells[1, 8].Value = "CATEGORIA";
                    oWsPrecio.Cells[1, 9].Value = "MARCA";
                    oWsPrecio.Cells[1, 10].Value = "PRECIO";
                    oWsPrecio.Cells[1, 11].Value = "CUMPLE";

                    _fila = 2;
                    foreach (AlicorpMinoristaExportaPrecio oBj in oPrecio)
                    {
                        oWsPrecio.Cells[_fila, 1].Value = oBj.ultima_visita;
                        oWsPrecio.Cells[_fila, 2].Value = oBj.pdv_codigo;
                        oWsPrecio.Cells[_fila, 3].Value = oBj.pdv_descripcion;
                        oWsPrecio.Cells[_fila, 4].Value = oBj.distrito;
                        oWsPrecio.Cells[_fila, 5].Value = oBj.ciudad;
                        oWsPrecio.Cells[_fila, 6].Value = oBj.giro;
                        oWsPrecio.Cells[_fila, 7].Value = oBj.gie;
                        oWsPrecio.Cells[_fila, 8].Value = oBj.cat_descripcion;
                        oWsPrecio.Cells[_fila, 9].Value = oBj.Name_Brand;
                        oWsPrecio.Cells[_fila, 10].Value = oBj.precio_sugerido;
                        oWsPrecio.Cells[_fila, 11].Value = oBj.cumple;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsPrecio.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                    oWsPrecio.Row(1).Height = 25;
                    oWsPrecio.Row(1).Style.Font.Bold = true;
                    oWsPrecio.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPrecio.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPrecio.Column(1).AutoFit();
                    oWsPrecio.Column(2).AutoFit();
                    oWsPrecio.Column(3).AutoFit();
                    oWsPrecio.Column(4).AutoFit();
                    oWsPrecio.Column(5).AutoFit();
                    oWsPrecio.Column(6).AutoFit();
                    oWsPrecio.Column(7).AutoFit();
                    oWsPrecio.Column(8).AutoFit();
                    oWsPrecio.Column(9).AutoFit();
                    oWsPrecio.Column(10).AutoFit();
                    oWsPrecio.Column(11).AutoFit();

                    oWsPrecio.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PrecioPdv(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpMinoristaPrecioPdv()
                    .Pdv(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //grupo = 2,
                        //ubigeo = "589,15",
                        //anio = 2015,
                        //mes = 5,
                        //periodo = 0,
                        //giro = 0,
                        //pdv = "",
                        //categoria = 12,
                        //marca = 44,
                        //segmento = 17,
                        //producto = "SMA00133",
                        //opcion = 1
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = __h,
                        categoria = Convert.ToInt32(__i),
                        marca = Convert.ToInt32(__j),
                        segmento = Convert.ToInt32(__k),
                        producto = __l,
                        opcion = 1,
                        tipoGiro = Convert.ToInt32(__m)
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PrecioSku(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m)
        {
            return View(new AlicorpMinoristaPrecioPdv()
                .Sku(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //grupo = 2,
                        //ubigeo = "589,15",
                        //anio = 2015,
                        //mes = 5,
                        //periodo = 0,
                        //giro = 0,
                        //pdv = "1007236",
                        //categoria = 12,
                        //marca = 44,
                        //segmento = 17,
                        //producto = "",
                        //opcion = 2
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = __h,
                        categoria = Convert.ToInt32(__i),
                        marca = Convert.ToInt32(__j),
                        segmento = 0,
                        producto = "",
                        opcion = 2,
                        tipoGiro = Convert.ToInt32(__m)
                    })
                );
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-08
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Pdv(string __a, string __b, string __c, string __d, string __e)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpMinoristaPdv()
                    .Pdv(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //anio = 2015,
                        //mes = 5,
                        //periodo = 0,
                        //pdv = "1345652"
                        campania = __a,
                        anio = Convert.ToInt32(__b),
                        mes = Convert.ToInt32(__c),
                        periodo = Convert.ToInt32(__d),
                        pdv = __e
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-08
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Fotografico(string __a, string __b, string __c, string __d, string __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpMinoristaFotografico()
                    .Lista(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //anio = 2015,
                        //mes = 5,
                        //periodo = 0,
                        //pdv = "1345652",
                        //categoria = 10401
                        campania = __a,
                        anio = Convert.ToInt32(__b),
                        mes = Convert.ToInt32(__c),
                        periodo = Convert.ToInt32(__d),
                        pdv = __e,
                        categoria = Convert.ToInt32(__f)
                    })),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult ShareOfDisplay(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            AlicorpMinoristaSod oBj = new AlicorpMinoristaSodCategoria()
                    .SOD(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = __a,//"002892382010",//__a, 002892382010
                        grupo = Convert.ToInt32(__b), //1, // 
                        ubigeo = __c, // "589", // 
                        anio = Convert.ToInt32(__d), //2015, // 
                        mes = Convert.ToInt32(__e),// 5, // ,
                        periodo = Convert.ToInt32(__f), //30257, // 
                        giro = Convert.ToInt32(__g), //0, // 
                        pdv = "", // __h
                        categoria = 0, // Convert.toInt32(__i),
                        segmento = Convert.ToInt32(__j), //17  // 
                        tipoGiro = Convert.ToInt32(__k)
                    });

            return View(oBj.categoria);
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        public JsonResult ExportarShareOfDisplayAlicorpMenor(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Presencia_ShareOfDisplay_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte ShareOfDisplay >>

                List<AlicorpMinoristaExportaSod> oShareOfDisplay = new AlicorpMinoristaSodCategoria().ExportaSOD(
                   new Resquest_AlicorpMinoristaCobertura()
                   {
                       campania = __a,//"002892382010",//__a,
                       grupo = Convert.ToInt32(__b), //1, // 
                       ubigeo = __c, // "589", // 
                       anio = Convert.ToInt32(__d), //2015, // 
                       mes = Convert.ToInt32(__e),// 5, // ,
                       periodo = Convert.ToInt32(__f), //30257, // 
                       giro = Convert.ToInt32(__g), //0, // 
                       pdv = "", // __h
                       categoria = 0, // Convert.toInt32(__i),
                       segmento = Convert.ToInt32(__j), //17  // 
                       tipoGiro = Convert.ToInt32(__k)
                   });

                #endregion

                #endregion

                #region <<< Reporte ShareOfDisplay >>>
                if ((oShareOfDisplay != null))
                {
                    Excel.ExcelWorksheet oWsShareOfDisplay = oEx.Workbook.Worksheets.Add("Rep_Presencia_ShareOfDisplay");
                    oWsShareOfDisplay.Cells[1, 1].Value = "ID";
                    oWsShareOfDisplay.Cells[1, 2].Value = "ULTIMA_VISITA";
                    oWsShareOfDisplay.Cells[1, 3].Value = "COD_PDV";
                    oWsShareOfDisplay.Cells[1, 4].Value = "NOMBRE_PDV";
                    oWsShareOfDisplay.Cells[1, 5].Value = "DISTRITO";
                    oWsShareOfDisplay.Cells[1, 6].Value = "CIUDAD";
                    oWsShareOfDisplay.Cells[1, 7].Value = "GIRO";
                    oWsShareOfDisplay.Cells[1, 8].Value = "GIE";
                    oWsShareOfDisplay.Cells[1, 9].Value = "CATEGORIA";
                    oWsShareOfDisplay.Cells[1, 10].Value = "MARCA";
                    oWsShareOfDisplay.Cells[1, 11].Value = "COMPETENCIA";
                    oWsShareOfDisplay.Cells[1, 12].Value = "ALICORP";


                    _fila = 2;
                    foreach (AlicorpMinoristaExportaSod oBj in oShareOfDisplay)
                    {
                        oWsShareOfDisplay.Cells[_fila, 1].Value = oBj.id;
                        oWsShareOfDisplay.Cells[_fila, 2].Value = oBj.ultima_visita;
                        oWsShareOfDisplay.Cells[_fila, 3].Value = oBj.pdv_cod;
                        oWsShareOfDisplay.Cells[_fila, 4].Value = oBj.pdv_descripcion;
                        oWsShareOfDisplay.Cells[_fila, 5].Value = oBj.distrito;
                        oWsShareOfDisplay.Cells[_fila, 6].Value = oBj.ciudad;
                        oWsShareOfDisplay.Cells[_fila, 7].Value = oBj.giro;
                        oWsShareOfDisplay.Cells[_fila, 8].Value = oBj.gie;
                        oWsShareOfDisplay.Cells[_fila, 9].Value = oBj.categoria;
                        oWsShareOfDisplay.Cells[_fila, 10].Value = oBj.marca;
                        oWsShareOfDisplay.Cells[_fila, 11].Value = oBj.competencia;
                        oWsShareOfDisplay.Cells[_fila, 12].Value = oBj.alicorp;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsShareOfDisplay.SelectedRange[1, 1, 1, 12].AutoFilter = true;
                    oWsShareOfDisplay.Row(1).Height = 25;
                    oWsShareOfDisplay.Row(1).Style.Font.Bold = true;
                    oWsShareOfDisplay.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsShareOfDisplay.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsShareOfDisplay.Column(1).AutoFit();
                    oWsShareOfDisplay.Column(2).AutoFit();
                    oWsShareOfDisplay.Column(3).AutoFit();
                    oWsShareOfDisplay.Column(4).AutoFit();
                    oWsShareOfDisplay.Column(5).AutoFit();
                    oWsShareOfDisplay.Column(6).AutoFit();
                    oWsShareOfDisplay.Column(7).AutoFit();
                    oWsShareOfDisplay.Column(8).AutoFit();
                    oWsShareOfDisplay.Column(9).AutoFit();
                    oWsShareOfDisplay.Column(10).AutoFit();
                    oWsShareOfDisplay.Column(11).AutoFit();
                    oWsShareOfDisplay.Column(12).AutoFit();
                    oWsShareOfDisplay.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }

        [HttpPost]
        public ActionResult ShareOfDisplay_PDV(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {

            return View(new AlicorpMinoristaSodCategoria()
                    .SODPdv(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = __h,
                        categoria = Convert.ToInt32(__i),
                        segmento = 0, //Convert.ToInt32(__j)
                        tipoGiro = Convert.ToInt32(__k)
                    })
                );
        }

        [HttpPost]
        public ActionResult ActividadCompetencia(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j,string __k, string __l)
        {

            return View(new ReporteActividadesMenor()
                    .Actividad(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = __a,
                        grupo =  Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes =  Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = "", // __h
                        categoria = Convert.ToInt32(__i),
                        segmento =  Convert.ToInt32(__j),
                        cod_tipoactividad =  __k,
                        tipoGiro = Convert.ToInt32(__l)
                        //campania = "002892382010",//__a,
                        //grupo = 1, // Convert.toInt32(__b),
                        //ubigeo = "589", // __c
                        //anio = 0, // Convert.toInt32(__d),
                        //mes = 0, // Convert.toInt32(__e),
                        //periodo = 32279, // Convert.toInt32(__f),
                        //giro = 0, // Convert.toInt32(__g),
                        //pdv = "", // __h
                        //categoria = 12, // Convert.toInt32(__i),
                        //segmento = 17,  //  Convert.toInt32(__j)
                        //cod_tipoactividad = "451" // __k
                    })
                );
        }

        [HttpPost]
        public ActionResult ActividadCompetenciaPDV(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {

            return View(new ReporteActividadesMenor()
                    .Actividad_x_PDV(new Resquest_AlicorpMinoristaCobertura()
                    {


                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = __h,
                        categoria = Convert.ToInt32(__i),
                        segmento = Convert.ToInt32(__j),
                        cod_tipoactividad = __k,
                        //tipoGiro = Convert.ToInt32(__l)
                        //campania = "002892382010",
                        //grupo = 1, 
                        //ubigeo = "589", 
                        //anio = 0, 
                        //mes = 0,
                        //periodo = 32279, //__a
                        //giro = 0,
                        //pdv = "12840", //__b
                        //categoria = 12, //__c
                        //segmento = 17,  
                        //cod_tipoactividad = "451" //__d
                    })
                );
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 27/08/15
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ActividadCompetenciaBK(int __a, string __b, int __c, int __d, string __e)
        {
            E_Parametros_MapBK oParametros = new E_Parametros_MapBK();
            oParametros.Cod_Equipo=null;
            oParametros.Cod_Tipo=__a;
            oParametros.Cod_Ubieo = __b;
            oParametros.cod_pais = "589";
            oParametros.Cod_Cadena = __c;
            oParametros.Periodo = __d;
            oParametros.Cod_Categoria = __e;

            return View(new ReporteActividadesMenor()
                    .ActividadCompeMapBK(new Request_ActividadCompetenciaBK()
                    {
                        oParametros=oParametros
                    })
                );
        }
        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 27/08/15
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DetalleActividadCompetenciaBK(int __a, string __b, int __c, int __d, string __e, string __f, string __g)
        {
            E_Parametros_MapBK oParametros = new E_Parametros_MapBK();
            oParametros.Cod_Equipo = __g;//canal
            oParametros.Cod_Tipo = __a;
            oParametros.Cod_Ubieo = __b;
            oParametros.cod_pais = "589";
            oParametros.Cod_Cadena = __c;
            oParametros.Periodo = __d;
            oParametros.Cod_Categoria = __e;
            oParametros.actividad = __f;
            oParametros.Cod_PDV = null;

            List<E_Detalle_Activi_CompeMapBK> oLs = new ReporteActividadesMenor().DetalleActividadCompeMapBK(
                new Request_ActividadCompetenciaBK()
                    {
                        oParametros = oParametros
                    });
            return Json(new { Archivo = oLs });
        }

        [HttpPost]
        public ActionResult DetxPDVActividadCompetenciaBK(int __a, string __b, int __c, int __d, string __e, string __f, string __g, string __h)
        {
            E_Parametros_MapBK oParametros = new E_Parametros_MapBK();
            oParametros.Cod_Equipo = __g;//canal
            oParametros.Cod_Tipo = __a;
            oParametros.Cod_Ubieo = __b;
            oParametros.cod_pais = "589";
            oParametros.Cod_Cadena = __c;
            oParametros.Periodo = __d;
            oParametros.Cod_Categoria = __e;
            oParametros.actividad = __f;
            oParametros.Cod_PDV = __h;

            return View(new ReporteActividadesMenor().DetalleActividadCompeMapBK(
                new Request_ActividadCompetenciaBK()
                {
                    oParametros = oParametros
                })
            );
        }

        [HttpPost]
        public ActionResult Filtros_GPS(string __a, string __b)
        {

            return Json(new New_XplSgGps_Filtro_Service()
                    .Filtro(new Resquest_New_XPL_SG_GPS()
                    {
                        opcion = Convert.ToInt32(__a),
                        parametros = __b
                    })
                );
        }

        [HttpPost]
        public ActionResult Filtros(string __a, string __b,string __c)
        {
            ViewBag.cod_opcion = __a;
            ViewBag.cod_elemento = __c;

            if (__a == "10") { 
                string[] lista = new string[2];
                lista =  __b.Split(',');
                ViewBag.cod_giro = lista[1];
            }

            return View(new ReporteAliMenor_Filtro_Service()
                    .Filtro(new Resquest_AlicorpMinoristaCobertura()
                    {
                        opcion= Convert.ToInt32(__a),
                        parametros = __b
                    })
                );
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Pop(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j)
        {
            return View(new AlicorpMinoristaPopPdv()
                    .Pop(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //grupo = 1,
                        //ubigeo = "589",
                        //anio = 2015,
                        //mes = 1,
                        //periodo = 0,
                        //giro = 0,
                        //pdv = "",
                        //categoria = 0,
                        //segmento = 17,
                        //elemento = 0,
                        //opcion = 0
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = "",
                        categoria = Convert.ToInt32(__h),
                        segmento = Convert.ToInt32(__i),
                        elemento = 0,
                        opcion = 0,
                        tipoGiro = Convert.ToInt32(__j)
                    })
                );
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        public JsonResult ExportarPopAlicorpMenor(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, int __k)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Presencia_Visibilidad_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Presencia Visibilidad >>

                List<AlicorpMinoristaExportaPopElemento> oVisibilidad = new AlicorpMinoristaPopPdv().ExportaPop(
                   new Resquest_AlicorpMinoristaCobertura()
                   {
                       campania = __a,
                       grupo = Convert.ToInt32(__b),
                       ubigeo = __c,
                       anio = Convert.ToInt32(__d),
                       mes = Convert.ToInt32(__e),
                       periodo = Convert.ToInt32(__f),
                       giro = Convert.ToInt32(__g),
                       pdv = "",
                       categoria = Convert.ToInt32(__h),
                       segmento = Convert.ToInt32(__i),
                       elemento = __k,
                       opcion = 3,
                       tipoGiro = Convert.ToInt32(__j)
                   });

                #endregion

                #endregion

                #region <<< Reporte Presencia Visibilidad >>>
                if ((oVisibilidad != null))
                {
                    Excel.ExcelWorksheet oWsVentanas = oEx.Workbook.Worksheets.Add("Rep_Presencia_Ventanas");
                    oWsVentanas.Cells[1, 1].Value = "ULTIMA_VISITA";
                    oWsVentanas.Cells[1, 2].Value = "COD_PDV";
                    oWsVentanas.Cells[1, 3].Value = "NOMBRE_PDV";
                    oWsVentanas.Cells[1, 4].Value = "DISTRITO";
                    oWsVentanas.Cells[1, 5].Value = "CIUDAD";
                    oWsVentanas.Cells[1, 6].Value = "GIRO";
                    oWsVentanas.Cells[1, 7].Value = "GIE";
                    oWsVentanas.Cells[1, 8].Value = "INSTALADOS CORRECTAMENTE";
                    oWsVentanas.Cells[1, 9].Value = "PDV CON PRESENCIA ANTES";
                    oWsVentanas.Cells[1, 10].Value = "PDV CON PRESENCIA DESPUES";

                    _fila = 2;
                    foreach (AlicorpMinoristaExportaPopElemento oBj in oVisibilidad)
                    {
                        oWsVentanas.Cells[_fila, 1].Value = oBj.ultima_visita;
                        oWsVentanas.Cells[_fila, 2].Value = oBj.pdv_codigo;
                        oWsVentanas.Cells[_fila, 3].Value = oBj.pdv_descripcion;
                        oWsVentanas.Cells[_fila, 4].Value = oBj.distrito;
                        oWsVentanas.Cells[_fila, 5].Value = oBj.ciudad;
                        oWsVentanas.Cells[_fila, 6].Value = oBj.giro;
                        oWsVentanas.Cells[_fila, 7].Value = oBj.gie;
                        oWsVentanas.Cells[_fila, 8].Value = oBj.instalado;
                        oWsVentanas.Cells[_fila, 9].Value = oBj.visita_antes;
                        oWsVentanas.Cells[_fila, 10].Value = oBj.visita_despues;
                        _fila++;
                    }

                    //Formato Cabecera
                    oWsVentanas.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    oWsVentanas.Row(1).Height = 25;
                    oWsVentanas.Row(1).Style.Font.Bold = true;
                    oWsVentanas.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsVentanas.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsVentanas.Column(1).AutoFit();
                    oWsVentanas.Column(2).AutoFit();
                    oWsVentanas.Column(3).AutoFit();
                    oWsVentanas.Column(4).AutoFit();
                    oWsVentanas.Column(5).AutoFit();
                    oWsVentanas.Column(6).AutoFit();
                    oWsVentanas.Column(7).AutoFit();
                    oWsVentanas.Column(8).AutoFit();
                    oWsVentanas.Column(9).AutoFit();
                    oWsVentanas.Column(10).AutoFit();

                    oWsVentanas.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PopPdv(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpMinoristaPopPdv()
                    .Pdv(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = "",
                        categoria = Convert.ToInt32(__h),
                        segmento = Convert.ToInt32(__i),
                        elemento = Convert.ToInt32(__j),
                        opcion = 1,
                        tipoGiro = Convert.ToInt32(__k)
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-06-04
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <param name="__j"></param>
        /// <param name="__k"></param>
        /// <param name="__l"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PopElemento(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j)
        {
            return View(new AlicorpMinoristaPopPdv()
                    .Elemento(new Resquest_AlicorpMinoristaCobertura()
                    {
                        //campania = "002892382010",
                        //grupo = 1,
                        //ubigeo = "589",
                        //anio = 2015,
                        //mes = 1,
                        //periodo = 0,
                        //giro = 0,
                        //pdv = "",
                        //categoria = 0,
                        //segmento = 17,
                        //elemento = 0,
                        //opcion = 2
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = __h,
                        categoria = Convert.ToInt32(__i),
                        segmento = 0,
                        elemento = 0,
                        opcion = 2,
                        tipoGiro = Convert.ToInt32(__j)
                    }));

            //return Content(new ContentResult
            //{
            //    Content = MvcApplication._Serialize(new AlicorpMinoristaPresencia()
            //        .Sku(new Resquest_AlicorpMinoristaCobertura()
            //        {
            //            campania = "002892382010",
            //            grupo = 2,
            //            ubigeo = "589,15",
            //            anio = 2015,
            //            mes = 5,
            //            periodo = 0,
            //            giro = 0,
            //            pdv = "1001085",
            //            categoria = 12,
            //            marca = 44,
            //            segmento = 17,
            //            producto = "",
            //            opcion = 2
            //            //campania = __a,
            //            //grupo = Convert.ToInt32(__b),
            //            //ubigeo = __c,
            //            //anio = Convert.ToInt32(__d),
            //            //mes = Convert.ToInt32(__e),
            //            //periodo = Convert.ToInt32(__f),
            //            //giro = Convert.ToInt32(__g),
            //            //pdv = __h,
            //            //categoria = Convert.ToInt32(__i),
            //            //marca = Convert.ToInt32(__j),
            //            //segmento = Convert.ToInt32(__k),
            //            //producto = __l,
            //            //opcion = 2
            //        })),
            //    ContentType = "application/json"
            //}.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-07-28
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Ventana(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i)
        {
            return View(new AlicorpMinoristaVentanaPdv()
                    .Ventana(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = "",
                        categoria = 0,
                        segmento = Convert.ToInt32(__i),
                        opcion = 0,
                        tipoGiro = Convert.ToInt32(__h)
                    })
                );
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 090915
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        public JsonResult ExportarVentanaAlicorpMenor(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, int __i, int __j)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Presencia_Ventanas_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Presencia Ventanas >>

                List<AlicorpMinoristaExportaVentana> oVentana = new AlicorpMinoristaVentanaPdv().ExportaVentana(
                   new Resquest_AlicorpMinoristaCobertura()
                   {
                       campania = __a,
                       grupo = Convert.ToInt32(__b),
                       ubigeo = __c,
                       anio = Convert.ToInt32(__d),
                       mes = Convert.ToInt32(__e),
                       periodo = Convert.ToInt32(__f),
                       giro = Convert.ToInt32(__g),
                       pdv = "",
                       categoria = __i,
                       segmento = Convert.ToInt32(__j),
                       opcion = 3,
                       tipoGiro = Convert.ToInt32(__h),
                   });

                #endregion

                #endregion

                #region <<< Reporte Presencia Visibilidad >>>
                if ((oVentana != null))
                {
                    Excel.ExcelWorksheet oWsVentanas = oEx.Workbook.Worksheets.Add("Rep_Presencia_Ventanas");
                    oWsVentanas.Cells[1, 1].Value = "ULTIMA_VISITA";
                    oWsVentanas.Cells[1, 2].Value = "COD_PDV";
                    oWsVentanas.Cells[1, 3].Value = "NOMBRE_PDV";
                    oWsVentanas.Cells[1, 4].Value = "DISTRITO";
                    oWsVentanas.Cells[1, 5].Value = "CIUDAD";
                    oWsVentanas.Cells[1, 6].Value = "GIRO";
                    oWsVentanas.Cells[1, 7].Value = "GIE";
                    oWsVentanas.Cells[1, 8].Value = "SEGMENTO";
                    oWsVentanas.Cells[1, 9].Value = "CATEGORIA";
                    oWsVentanas.Cells[1, 10].Value = "INSTALADOS";

                    _fila = 2;
                    foreach (AlicorpMinoristaExportaVentana oBj in oVentana)
                    {
                        oWsVentanas.Cells[_fila, 1].Value = oBj.ultima_visita;
                        oWsVentanas.Cells[_fila, 2].Value = oBj.pvd_codigo;
                        oWsVentanas.Cells[_fila, 3].Value = oBj.pdv_descripcion;
                        oWsVentanas.Cells[_fila, 4].Value = oBj.distrito;
                        oWsVentanas.Cells[_fila, 5].Value = oBj.ciudad;
                        oWsVentanas.Cells[_fila, 6].Value = oBj.giro;
                        oWsVentanas.Cells[_fila, 7].Value = oBj.gie;
                        oWsVentanas.Cells[_fila, 8].Value = oBj.segmento;
                        oWsVentanas.Cells[_fila, 9].Value = oBj.cat_descripcion;
                        oWsVentanas.Cells[_fila, 10].Value = oBj.instalados;

                        _fila++;
                    }

                    //Formato Cabecera
                    oWsVentanas.SelectedRange[1, 1, 1, 10].AutoFilter = true;
                    oWsVentanas.Row(1).Height = 25;
                    oWsVentanas.Row(1).Style.Font.Bold = true;
                    oWsVentanas.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsVentanas.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsVentanas.Column(1).AutoFit();
                    oWsVentanas.Column(2).AutoFit();
                    oWsVentanas.Column(3).AutoFit();
                    oWsVentanas.Column(4).AutoFit();
                    oWsVentanas.Column(5).AutoFit();
                    oWsVentanas.Column(6).AutoFit();
                    oWsVentanas.Column(7).AutoFit();
                    oWsVentanas.Column(8).AutoFit();
                    oWsVentanas.Column(9).AutoFit();
                    oWsVentanas.Column(10).AutoFit();

                    oWsVentanas.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-07-28
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult VentanaPdv(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new AlicorpMinoristaVentanaPdv()
                    .Pdv(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = "",
                        categoria = Convert.ToInt32(__h),
                        segmento = 0,
                        opcion = 1,
                        tipoGiro = Convert.ToInt32(__i)
                    })),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-07-28
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult VentanaCategoria(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i)
        {
            return View(new AlicorpMinoristaVentanaPdv()
                .Categoria(new Resquest_AlicorpMinoristaCobertura()
                {
                    campania = __a,
                    grupo = Convert.ToInt32(__b),
                    ubigeo = __c,
                    anio = Convert.ToInt32(__d),
                    mes = Convert.ToInt32(__e),
                    periodo = Convert.ToInt32(__f),
                    giro = Convert.ToInt32(__g),
                    pdv = __h,
                    categoria = 0,
                    segmento = 0,
                    opcion = 2,
                    tipoGiro = Convert.ToInt32(__i)
                })
            );
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-09-10
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <param name="__d"></param>
        /// <param name="__e"></param>
        /// <param name="__f"></param>
        /// <param name="__g"></param>
        /// <param name="__h"></param>
        /// <param name="__i"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Venta(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i)
        {
            return View(new AlicorpMinoristaVenta()
                    .Venta(new Resquest_AlicorpMinoristaCobertura()
                    {
                        campania = __a,
                        grupo = Convert.ToInt32(__b),
                        ubigeo = __c,
                        anio = Convert.ToInt32(__d),
                        mes = Convert.ToInt32(__e),
                        periodo = Convert.ToInt32(__f),
                        giro = Convert.ToInt32(__g),
                        pdv = "",
                        categoria = 0,
                        segmento = Convert.ToInt32(__i),
                        opcion = 0,
                        tipoGiro = Convert.ToInt32(__h)
                    })
                );
        }

        #endregion

        #region Seguimiento GPS
        /// <summary>
        /// Inicio GPS
        /// Jsulla
        /// 21/07/2015
        /// </summary>
        /// <param name="_a"></param>
        /// <param name="_b"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult SeguimientoGPS(string _a,string _b)
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
             ViewBag.tipo_perfil = ((Persona)Session["Session_Login"]).tpf_id;

             if (_a == "0134126102010" && _b == "222") { ViewBag.veq = "-1"; }
             else if (_a == "012011092692011" && _b == "223") { ViewBag.veq = "-2"; }
             else
             {
                 ViewBag.veq = _a;
             }

             //Se registra visita
             Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        /// <summary>
        /// Creado por  : Jsulla
        /// Fecha       : 23/07/2015
        /// </summary>
        /// <returns></returns>
        public ActionResult RecorridoGie(int __a,int __b,string __c,string __d,string __e,int __f,int __g,string __h,int __i ,int __j,int __k,int __l) {

            return View(new E_Tb_Recorrido_Gie()
                    .Recorrido(new Resquest_New_XPL_SG_GPS()
                    {
                        //vgie = 15851,//__a,
                        //vsupe = 8474,// __b,
                        //vequipo = "9020110152013",//__c
                        //vfec_inicio = "01/01/2015",//__d
                        //vfec_fin = "27/07/2015",//__e 
                        //vcadena = 25,//__f
                        //vcanal = 25,//__g  
                        //vciudad = "15,01"//__h
                        vgie = __a,
                        vsupe = __b,
                        vequipo = __c,
                        vfec_inicio = __d,
                        vfec_fin = __e, 
                        vcadena = __f,
                        vcanal = __g  ,
                        vciudad = __h,
                        vtipoperfil = __i ,
                        vprograma = __j,
                        vgiro = __k,
                        vdex = __l
                    }));
        }

        public ActionResult SGPSFiltro(string __a,string __b,int __c){
            ViewBag.cod_elemento = __a;
            return View(new New_XplSgGps_Filtro_Service()
                    .Filtro( new Resquest_New_XPL_SG_GPS()
                    {
                        opcion = __c,
                        parametros = __b
                    }));
        }

        /// <summary>
        /// Creado por  : rcontreras
        /// Fecha       : 27/07/2015
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult RecorridoGieExcel(int __a, int __b, string __c, string __d, string __e, int __f, int __g, string __h,int __i,int __j,int __k,int __l)
        {
            
            ViewBag.veq = __c;

            if (ViewBag.veq == "002892382010")
            {
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;

                _fileServer = String.Format("seguimientoGPS_por_GIE_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>

                    #region << Reporte Seguimiento GPS >>

                    List<E_Recorrido_Gie> oSeguimientoGPS = new E_Recorrido_Gie().downloadExcel(
                       new Resquest_New_XPL_SG_GPS()
                       {
                           //vgie = 15851,//__a,
                           //vsupe = 8474,// __b,   
                           //vequipo = "9020110152013",//__c
                           //vfec_inicio = "01/01/2015",//__d      
                           //vfec_fin = "27/07/2015",//__e 
                           //vcadena = 25,//__f
                           //vcanal = 25,//__g  
                           //vciudad = "15,01"//__h

                           vgie = __a,
                           vsupe = __b,
                           vequipo = __c,
                           vfec_inicio = __d,
                           vfec_fin = __e,
                           vcadena = __f,
                           vcanal = __g,
                           vciudad = __h,
                           vtipoperfil =__i,
                           vprograma = __j,
                           vgiro = __k,
                           vdex = __l

                       }
                   );

                    #endregion

                    #endregion

                    #region <<< Reporte Segimiento GPS >>>
                    if ((oSeguimientoGPS != null))
                    {
                        Excel.ExcelWorksheet oWsSeguimientoGPS = oEx.Workbook.Worksheets.Add("seguimientoGPS");
                        oWsSeguimientoGPS.Cells[1, 1].Value = "ID";
                        oWsSeguimientoGPS.Cells[1, 2].Value = "FEC. INICIO";
                        oWsSeguimientoGPS.Cells[1, 3].Value = "HORA INICIO";
                        oWsSeguimientoGPS.Cells[1, 4].Value = "FEC. FIN";
                        oWsSeguimientoGPS.Cells[1, 5].Value = "HORA FIN";
                        oWsSeguimientoGPS.Cells[1, 6].Value = "GESTION";
                        oWsSeguimientoGPS.Cells[1, 7].Value = "PDV: Codigo";
                        oWsSeguimientoGPS.Cells[1, 8].Value = "PROGRAMA";
                        oWsSeguimientoGPS.Cells[1, 9].Value = "DEX";
                        oWsSeguimientoGPS.Cells[1, 10].Value = "PDV: Descripcion";
                        oWsSeguimientoGPS.Cells[1, 11].Value = "PDV: Direccion";
                        oWsSeguimientoGPS.Cells[1, 12].Value = "PDV: Latitud";
                        oWsSeguimientoGPS.Cells[1, 13].Value = "PDV: Longitud";
                        oWsSeguimientoGPS.Cells[1, 14].Value = "SUPERVISOR";
                        oWsSeguimientoGPS.Cells[1, 15].Value = "GESTOR";
                        oWsSeguimientoGPS.Cells[1, 16].Value = "ESTADO";
                        oWsSeguimientoGPS.Cells[1, 17].Value = "MOTIVO";

                        _fila = 2;
                        foreach (E_Recorrido_Gie oBj in oSeguimientoGPS)
                        {
                            //if (((Persona)Session["Session_Login"]).tpf_id == 4)
                            //{
                            //    oWsSeguimientoGPS.Cells[_fila, 1].Value = oBj.Ids;
                            //    oWsSeguimientoGPS.Cells[_fila, 2].Value = oBj.Fecha_Inicio;
                            //    oWsSeguimientoGPS.Cells[_fila, 3].Value = oBj.Hora_Incio;
                            //    oWsSeguimientoGPS.Cells[_fila, 4].Value = oBj.Fecha_Fin;
                            //    oWsSeguimientoGPS.Cells[_fila, 5].Value = oBj.Hora_Fin;
                            //    oWsSeguimientoGPS.Cells[_fila, 6].Value = oBj.Gestion;
                            //    oWsSeguimientoGPS.Cells[_fila, 7].Value = oBj.pdv_cod;
                            //    oWsSeguimientoGPS.Cells[_fila, 8].Value = oBj.Programa;
                            //    oWsSeguimientoGPS.Cells[_fila, 9].Value = oBj.Dex;
                            //    oWsSeguimientoGPS.Cells[_fila, 10].Value = oBj.Nom_PDV;
                            //    oWsSeguimientoGPS.Cells[_fila, 11].Value = oBj.Direccion;
                            //    oWsSeguimientoGPS.Cells[_fila, 12].Value = oBj.Latitud;
                            //    oWsSeguimientoGPS.Cells[_fila, 13].Value = oBj.Longitud;
                            //    oWsSeguimientoGPS.Cells[_fila, 14].Value = oBj.Nom_Supervisor;
                            //    oWsSeguimientoGPS.Cells[_fila, 15].Value = oBj.Gie_Alias;
                            //    oWsSeguimientoGPS.Cells[_fila, 16].Value = oBj.Estado;
                            //    oWsSeguimientoGPS.Cells[_fila, 17].Value = oBj.No_Visita;
                            //    _fila++; 
                            //}
                            //else
                            //{
                                oWsSeguimientoGPS.Cells[_fila, 1].Value = oBj.Ids;
                                oWsSeguimientoGPS.Cells[_fila, 2].Value = oBj.Fecha_Inicio;
                                oWsSeguimientoGPS.Cells[_fila, 3].Value = oBj.Hora_Incio;
                                oWsSeguimientoGPS.Cells[_fila, 4].Value = oBj.Fecha_Fin;
                                oWsSeguimientoGPS.Cells[_fila, 5].Value = oBj.Hora_Fin;
                                oWsSeguimientoGPS.Cells[_fila, 6].Value = oBj.Gestion;
                                oWsSeguimientoGPS.Cells[_fila, 7].Value = oBj.pdv_cod;
                                oWsSeguimientoGPS.Cells[_fila, 8].Value = oBj.Programa;
                                oWsSeguimientoGPS.Cells[_fila, 9].Value = oBj.Dex;
                                oWsSeguimientoGPS.Cells[_fila, 10].Value = oBj.Nom_PDV;
                                oWsSeguimientoGPS.Cells[_fila, 11].Value = oBj.Direccion;
                                oWsSeguimientoGPS.Cells[_fila, 12].Value = oBj.Latitud;
                                oWsSeguimientoGPS.Cells[_fila, 13].Value = oBj.Longitud;
                                oWsSeguimientoGPS.Cells[_fila, 14].Value = oBj.Nom_Supervisor;
                                oWsSeguimientoGPS.Cells[_fila, 15].Value = oBj.Nom_Gie;
                                oWsSeguimientoGPS.Cells[_fila, 16].Value = oBj.Estado;
                                oWsSeguimientoGPS.Cells[_fila, 17].Value = oBj.No_Visita;
                                _fila++; 
                            //}


                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsSeguimientoGPS.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                        oWsSeguimientoGPS.Row(1).Height = 25;
                        oWsSeguimientoGPS.Row(1).Style.Font.Bold = true;
                        oWsSeguimientoGPS.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsSeguimientoGPS.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsSeguimientoGPS.Column(1).AutoFit();
                        oWsSeguimientoGPS.Column(2).AutoFit();
                        oWsSeguimientoGPS.Column(3).AutoFit();
                        oWsSeguimientoGPS.Column(4).AutoFit();
                        oWsSeguimientoGPS.Column(5).AutoFit();
                        oWsSeguimientoGPS.Column(6).AutoFit();
                        oWsSeguimientoGPS.Column(7).AutoFit();
                        oWsSeguimientoGPS.Column(8).AutoFit();
                        oWsSeguimientoGPS.Column(9).AutoFit();
                        oWsSeguimientoGPS.Column(10).AutoFit();
                        oWsSeguimientoGPS.Column(11).AutoFit();
                        oWsSeguimientoGPS.Column(12).AutoFit();
                        oWsSeguimientoGPS.Column(13).AutoFit();
                        oWsSeguimientoGPS.Column(14).AutoFit();
                        oWsSeguimientoGPS.Column(15).AutoFit();
                        oWsSeguimientoGPS.Column(16).AutoFit();
                        oWsSeguimientoGPS.Column(17).AutoFit();
                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion
                }
                return Json(new { Archivo = _fileServer });
            }
            else if (ViewBag.veq == "-1" || ViewBag.veq == "-2")
            {
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;

                _fileServer = String.Format("seguimientoGPS_por_GIE_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>

                    #region << Reporte Seguimiento GPS >>

                    List<E_Recorrido_Gie> oSeguimientoGPS = new E_Recorrido_Gie().downloadExcel(
                       new Resquest_New_XPL_SG_GPS()
                       {
                           //vgie = 15851,//__a,
                           //vsupe = 8474,// __b,   
                           //vequipo = "9020110152013",//__c
                           //vfec_inicio = "01/01/2015",//__d      
                           //vfec_fin = "27/07/2015",//__e 
                           //vcadena = 25,//__f
                           //vcanal = 25,//__g  
                           //vciudad = "15,01"//__h

                           vgie = __a,
                           vsupe = __b,
                           vequipo = __c,
                           vfec_inicio = __d,
                           vfec_fin = __e,
                           vcadena = __f,
                           vcanal = __g,
                           vciudad = __h,
                           vtipoperfil =__i,
                           vprograma = __j,
                           vgiro = __k,
                           vdex = __l

                       }
                   );

                    #endregion

                    #endregion

                    #region <<< Reporte Segimiento GPS >>>
                    if ((oSeguimientoGPS != null))
                    {
                        Excel.ExcelWorksheet oWsSeguimientoGPS = oEx.Workbook.Worksheets.Add("seguimientoGPS");
                        oWsSeguimientoGPS.Cells[1, 1].Value = "ID";
                        oWsSeguimientoGPS.Cells[1, 2].Value = "CANAL";
                        oWsSeguimientoGPS.Cells[1, 3].Value = "FEC. INICIO";
                        oWsSeguimientoGPS.Cells[1, 4].Value = "HORA INICIO";
                        oWsSeguimientoGPS.Cells[1, 5].Value = "FEC. FIN";
                        oWsSeguimientoGPS.Cells[1, 6].Value = "HORA FIN";
                        oWsSeguimientoGPS.Cells[1, 7].Value = "GESTION";
                        oWsSeguimientoGPS.Cells[1, 8].Value = "PDV: Codigo";
                        oWsSeguimientoGPS.Cells[1, 9].Value = "PDV: Descripcion";
                        oWsSeguimientoGPS.Cells[1, 10].Value = "PDV: Direccion";
                        oWsSeguimientoGPS.Cells[1, 11].Value = "PDV: Latitud";
                        oWsSeguimientoGPS.Cells[1, 12].Value = "PDV: Longitud";
                        oWsSeguimientoGPS.Cells[1, 13].Value = "SUPERVISOR";
                        oWsSeguimientoGPS.Cells[1, 14].Value = "GESTOR";
                        oWsSeguimientoGPS.Cells[1, 15].Value = "ESTADO";
                        oWsSeguimientoGPS.Cells[1, 16].Value = "MOTIVO";
                    
                        _fila = 2;
                        foreach (E_Recorrido_Gie oBj in oSeguimientoGPS)
                        {
                            oWsSeguimientoGPS.Cells[_fila, 1].Value = oBj.Ids;
                            oWsSeguimientoGPS.Cells[_fila, 2].Value = oBj.Descripcion_GPS;
                            oWsSeguimientoGPS.Cells[_fila, 3].Value = oBj.Fecha_Inicio;
                            oWsSeguimientoGPS.Cells[_fila, 4].Value = oBj.Hora_Incio;
                            oWsSeguimientoGPS.Cells[_fila, 5].Value = oBj.Fecha_Fin;
                            oWsSeguimientoGPS.Cells[_fila, 6].Value = oBj.Hora_Fin;
                            oWsSeguimientoGPS.Cells[_fila, 7].Value = oBj.Gestion;
                            oWsSeguimientoGPS.Cells[_fila, 8].Value = oBj.pdv_cod;
                            oWsSeguimientoGPS.Cells[_fila, 9].Value = oBj.Nom_PDV;
                            oWsSeguimientoGPS.Cells[_fila, 10].Value = oBj.Direccion;
                            oWsSeguimientoGPS.Cells[_fila, 11].Value = oBj.Latitud;
                            oWsSeguimientoGPS.Cells[_fila, 12].Value = oBj.Longitud;
                            oWsSeguimientoGPS.Cells[_fila, 13].Value = oBj.Nom_Supervisor;
                            oWsSeguimientoGPS.Cells[_fila, 14].Value = oBj.Nom_Gie;
                            oWsSeguimientoGPS.Cells[_fila, 15].Value = oBj.Estado;
                            oWsSeguimientoGPS.Cells[_fila, 16].Value = oBj.No_Visita;
                            _fila++;

                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsSeguimientoGPS.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                        oWsSeguimientoGPS.Row(1).Height = 25;
                        oWsSeguimientoGPS.Row(1).Style.Font.Bold = true;
                        oWsSeguimientoGPS.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsSeguimientoGPS.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsSeguimientoGPS.Column(1).AutoFit();
                        oWsSeguimientoGPS.Column(2).AutoFit();
                        oWsSeguimientoGPS.Column(3).AutoFit();
                        oWsSeguimientoGPS.Column(4).AutoFit();
                        oWsSeguimientoGPS.Column(5).AutoFit();
                        oWsSeguimientoGPS.Column(6).AutoFit();
                        oWsSeguimientoGPS.Column(7).AutoFit();
                        oWsSeguimientoGPS.Column(8).AutoFit();
                        oWsSeguimientoGPS.Column(9).AutoFit();
                        oWsSeguimientoGPS.Column(10).AutoFit();
                        oWsSeguimientoGPS.Column(11).AutoFit();
                        oWsSeguimientoGPS.Column(12).AutoFit();
                        oWsSeguimientoGPS.Column(13).AutoFit();
                        oWsSeguimientoGPS.Column(14).AutoFit();
                        oWsSeguimientoGPS.Column(15).AutoFit();
                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion
                }
                return Json(new { Archivo = _fileServer });
            }
            else
            {
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;

                _fileServer = String.Format("seguimientoGPS_por_GIE_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>

                    #region << Reporte Seguimiento GPS >>

                    List<E_Recorrido_Gie> oSeguimientoGPS = new E_Recorrido_Gie().downloadExcel(
                       new Resquest_New_XPL_SG_GPS()
                       {
                           //vgie = 15851,//__a,
                           //vsupe = 8474,// __b,   
                           //vequipo = "9020110152013",//__c
                           //vfec_inicio = "01/01/2015",//__d      
                           //vfec_fin = "27/07/2015",//__e 
                           //vcadena = 25,//__f
                           //vcanal = 25,//__g  
                           //vciudad = "15,01"//__h

                           vgie = __a,
                           vsupe = __b,
                           vequipo = __c,
                           vfec_inicio = __d,
                           vfec_fin = __e,
                           vcadena = __f,
                           vcanal = __g,
                           vciudad = __h,
                           vtipoperfil =__i,
                           vprograma = __j,
                           vgiro = __k,
                           vdex = __l

                       }
                   );

                    #endregion

                    #endregion

                    #region <<< Reporte Segimiento GPS >>>
                    if ((oSeguimientoGPS != null))
                    {
                        Excel.ExcelWorksheet oWsSeguimientoGPS = oEx.Workbook.Worksheets.Add("seguimientoGPS");
                        oWsSeguimientoGPS.Cells[1, 1].Value = "ID";
                        oWsSeguimientoGPS.Cells[1, 2].Value = "FEC. INICIO";
                        oWsSeguimientoGPS.Cells[1, 3].Value = "HORA INICIO";
                        oWsSeguimientoGPS.Cells[1, 4].Value = "FEC. FIN";
                        oWsSeguimientoGPS.Cells[1, 5].Value = "HORA FIN";
                        oWsSeguimientoGPS.Cells[1, 6].Value = "GESTION";
                        oWsSeguimientoGPS.Cells[1, 7].Value = "PDV: Codigo";
                        oWsSeguimientoGPS.Cells[1, 8].Value = "PDV: Descripcion";
                        oWsSeguimientoGPS.Cells[1, 9].Value = "PDV: Direccion";
                        oWsSeguimientoGPS.Cells[1, 10].Value = "PDV: Latitud";
                        oWsSeguimientoGPS.Cells[1, 11].Value = "PDV: Longitud";
                        oWsSeguimientoGPS.Cells[1, 12].Value = "SUPERVISOR";
                        oWsSeguimientoGPS.Cells[1, 13].Value = "GESTOR";
                        oWsSeguimientoGPS.Cells[1, 14].Value = "ESTADO";
                        oWsSeguimientoGPS.Cells[1, 15].Value = "MOTIVO";

                        _fila = 2;
                        foreach (E_Recorrido_Gie oBj in oSeguimientoGPS)
                        {
                            oWsSeguimientoGPS.Cells[_fila, 1].Value = oBj.Ids;
                            oWsSeguimientoGPS.Cells[_fila, 2].Value = oBj.Fecha_Inicio;
                            oWsSeguimientoGPS.Cells[_fila, 3].Value = oBj.Hora_Incio;
                            oWsSeguimientoGPS.Cells[_fila, 4].Value = oBj.Fecha_Fin;
                            oWsSeguimientoGPS.Cells[_fila, 5].Value = oBj.Hora_Fin;
                            oWsSeguimientoGPS.Cells[_fila, 6].Value = oBj.Gestion;
                            oWsSeguimientoGPS.Cells[_fila, 7].Value = oBj.pdv_cod;
                            oWsSeguimientoGPS.Cells[_fila, 8].Value = oBj.Nom_PDV;
                            oWsSeguimientoGPS.Cells[_fila, 9].Value = oBj.Direccion;
                            oWsSeguimientoGPS.Cells[_fila, 10].Value = oBj.Latitud;
                            oWsSeguimientoGPS.Cells[_fila, 11].Value = oBj.Longitud;
                            oWsSeguimientoGPS.Cells[_fila, 12].Value = oBj.Nom_Supervisor;
                            oWsSeguimientoGPS.Cells[_fila, 13].Value = oBj.Nom_Gie;
                            oWsSeguimientoGPS.Cells[_fila, 14].Value = oBj.Estado;
                            oWsSeguimientoGPS.Cells[_fila, 15].Value = oBj.No_Visita;
                            _fila++;

                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsSeguimientoGPS.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                        oWsSeguimientoGPS.Row(1).Height = 25;
                        oWsSeguimientoGPS.Row(1).Style.Font.Bold = true;
                        oWsSeguimientoGPS.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsSeguimientoGPS.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsSeguimientoGPS.Column(1).AutoFit();
                        oWsSeguimientoGPS.Column(2).AutoFit();
                        oWsSeguimientoGPS.Column(3).AutoFit();
                        oWsSeguimientoGPS.Column(4).AutoFit();
                        oWsSeguimientoGPS.Column(5).AutoFit();
                        oWsSeguimientoGPS.Column(6).AutoFit();
                        oWsSeguimientoGPS.Column(7).AutoFit();
                        oWsSeguimientoGPS.Column(8).AutoFit();
                        oWsSeguimientoGPS.Column(9).AutoFit();
                        oWsSeguimientoGPS.Column(10).AutoFit();
                        oWsSeguimientoGPS.Column(11).AutoFit();
                        oWsSeguimientoGPS.Column(12).AutoFit();
                        oWsSeguimientoGPS.Column(13).AutoFit();
                        oWsSeguimientoGPS.Column(14).AutoFit();
                        oWsSeguimientoGPS.Column(15).AutoFit();
                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion
                }
                return Json(new { Archivo = _fileServer });
            }
            
        }

        /// <summary>
        /// Creado por  : rcontreras
        /// Fecha       : 30/07/2015
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult RecorridoGieExcelxSuper(int __a, int __b, string __c, string __d, string __e, int __f, int __g, string __h,int __i,int __j,int __k,int __l)
        {
            ViewBag.veq = __c;
            if (ViewBag.veq == "002892382010")
            {
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;

                _fileServer = String.Format("seguimientoGPS_por_Super_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>

                    #region << Reporte Seguimiento GPS >>

                    List<E_Recorrido_Gie> oSeguimientoGPS = new E_Recorrido_Gie().downloadExcelxSuper(
                       new Resquest_New_XPL_SG_GPS()
                       {
                           //vgie = 15851,//__a,
                           //vsupe = 8474,// __b,   
                           //vequipo = "9020110152013",//__c
                           //vfec_inicio = "01/01/2015",//__d      
                           //vfec_fin = "27/07/2015",//__e 
                           //vcadena = 25,//__f
                           //vcanal = 25,//__g  
                           //vciudad = "15,01"//__h

                           vgie = __a,
                           vsupe = __b,
                           vequipo = __c,
                           vfec_inicio = __d,
                           vfec_fin = __e,
                           vcadena = __f,
                           vcanal = __g,
                           vciudad = __h,
                           vtipoperfil = __i ,
                           vprograma = __j,
                           vgiro= __k,
                           vdex= __l


                       }
                   );

                    #endregion

                    #endregion

                    #region <<< Reporte Segimiento GPS >>>
                    if ((oSeguimientoGPS != null))
                    {
                        Excel.ExcelWorksheet oWsSeguimientoGPS = oEx.Workbook.Worksheets.Add("seguimientoGPS");
                        oWsSeguimientoGPS.Cells[1, 1].Value = "ID";
                        oWsSeguimientoGPS.Cells[1, 2].Value = "FEC. INICIO";
                        oWsSeguimientoGPS.Cells[1, 3].Value = "HORA INICIO";
                        oWsSeguimientoGPS.Cells[1, 4].Value = "FEC. FIN";
                        oWsSeguimientoGPS.Cells[1, 5].Value = "HORA FIN";
                        oWsSeguimientoGPS.Cells[1, 6].Value = "GESTION";
                        oWsSeguimientoGPS.Cells[1, 7].Value = "PDV: Codigo";
                        oWsSeguimientoGPS.Cells[1, 8].Value = "PROGRAMA";
                        oWsSeguimientoGPS.Cells[1, 9].Value = "DEX";
                        oWsSeguimientoGPS.Cells[1, 10].Value = "PDV: Descripcion";
                        oWsSeguimientoGPS.Cells[1, 11].Value = "SUPERVISOR";
                        oWsSeguimientoGPS.Cells[1, 12].Value = "GESTOR";
                        oWsSeguimientoGPS.Cells[1, 13].Value = "ESTADO";
                        oWsSeguimientoGPS.Cells[1, 14].Value = "MOTIVO";

                        _fila = 2;
                        foreach (E_Recorrido_Gie oBj in oSeguimientoGPS)
                        {
                            //if (((Persona)Session["Session_Login"]).tpf_id == 4)
                            //{
                            //    oWsSeguimientoGPS.Cells[_fila, 1].Value = oBj.Ids;
                            //    oWsSeguimientoGPS.Cells[_fila, 2].Value = oBj.Fecha_Inicio;
                            //    oWsSeguimientoGPS.Cells[_fila, 3].Value = oBj.Hora_Incio;
                            //    oWsSeguimientoGPS.Cells[_fila, 4].Value = oBj.Fecha_Fin;
                            //    oWsSeguimientoGPS.Cells[_fila, 5].Value = oBj.Hora_Fin;
                            //    oWsSeguimientoGPS.Cells[_fila, 6].Value = oBj.Gestion;
                            //    oWsSeguimientoGPS.Cells[_fila, 7].Value = oBj.pdv_cod;
                            //    oWsSeguimientoGPS.Cells[_fila, 8].Value = oBj.Programa;
                            //    oWsSeguimientoGPS.Cells[_fila, 9].Value = oBj.Dex;
                            //    oWsSeguimientoGPS.Cells[_fila, 10].Value = oBj.Nom_PDV;
                            //    oWsSeguimientoGPS.Cells[_fila, 11].Value = oBj.Nom_Supervisor;
                            //    oWsSeguimientoGPS.Cells[_fila, 12].Value = oBj.Gie_Alias;
                            //    oWsSeguimientoGPS.Cells[_fila, 13].Value = oBj.Estado;
                            //    oWsSeguimientoGPS.Cells[_fila, 14].Value = oBj.No_Visita;
                            //    _fila++;
                            //}
                            //else
                            //{

                                oWsSeguimientoGPS.Cells[_fila, 1].Value = oBj.Ids;
                                oWsSeguimientoGPS.Cells[_fila, 2].Value = oBj.Fecha_Inicio;
                                oWsSeguimientoGPS.Cells[_fila, 3].Value = oBj.Hora_Incio;
                                oWsSeguimientoGPS.Cells[_fila, 4].Value = oBj.Fecha_Fin;
                                oWsSeguimientoGPS.Cells[_fila, 5].Value = oBj.Hora_Fin;
                                oWsSeguimientoGPS.Cells[_fila, 6].Value = oBj.Gestion;
                                oWsSeguimientoGPS.Cells[_fila, 7].Value = oBj.pdv_cod;
                                oWsSeguimientoGPS.Cells[_fila, 8].Value = oBj.Programa;
                                oWsSeguimientoGPS.Cells[_fila, 9].Value = oBj.Dex;
                                oWsSeguimientoGPS.Cells[_fila, 10].Value = oBj.Nom_PDV;
                                oWsSeguimientoGPS.Cells[_fila, 11].Value = oBj.Nom_Supervisor;
                                oWsSeguimientoGPS.Cells[_fila, 12].Value = oBj.Nom_Gie;
                                oWsSeguimientoGPS.Cells[_fila, 13].Value = oBj.Estado;
                                oWsSeguimientoGPS.Cells[_fila, 14].Value = oBj.No_Visita;
                                _fila++;
                            //}
                        
                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsSeguimientoGPS.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                        oWsSeguimientoGPS.Row(1).Height = 25;
                        oWsSeguimientoGPS.Row(1).Style.Font.Bold = true;
                        oWsSeguimientoGPS.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsSeguimientoGPS.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsSeguimientoGPS.Column(1).AutoFit();
                        oWsSeguimientoGPS.Column(2).AutoFit();
                        oWsSeguimientoGPS.Column(3).AutoFit();
                        oWsSeguimientoGPS.Column(4).AutoFit();
                        oWsSeguimientoGPS.Column(5).AutoFit();
                        oWsSeguimientoGPS.Column(6).AutoFit();
                        oWsSeguimientoGPS.Column(7).AutoFit();
                        oWsSeguimientoGPS.Column(8).AutoFit();
                        oWsSeguimientoGPS.Column(9).AutoFit();
                        oWsSeguimientoGPS.Column(10).AutoFit();
                        oWsSeguimientoGPS.Column(11).AutoFit();
                        oWsSeguimientoGPS.Column(12).AutoFit();
                        oWsSeguimientoGPS.Column(13).AutoFit();
                        oWsSeguimientoGPS.Column(14).AutoFit();
                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }
            else
            {
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;

                _fileServer = String.Format("seguimientoGPS_por_Super_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>

                    #region << Reporte Seguimiento GPS >>

                    List<E_Recorrido_Gie> oSeguimientoGPS = new E_Recorrido_Gie().downloadExcelxSuper(
                       new Resquest_New_XPL_SG_GPS()
                       {
                           //vgie = 15851,//__a,
                           //vsupe = 8474,// __b,   
                           //vequipo = "9020110152013",//__c
                           //vfec_inicio = "01/01/2015",//__d      
                           //vfec_fin = "27/07/2015",//__e 
                           //vcadena = 25,//__f
                           //vcanal = 25,//__g  
                           //vciudad = "15,01"//__h

                           vgie = __a,
                           vsupe = __b,
                           vequipo = __c,
                           vfec_inicio = __d,
                           vfec_fin = __e,
                           vcadena = __f,
                           vcanal = __g,
                           vciudad = __h,
                           vtipoperfil=__i,
                           vprograma = __j,
                           vgiro = __k,
                           vdex= __l

                       }
                   );

                    #endregion

                    #endregion

                    #region <<< Reporte Segimiento GPS >>>
                    if ((oSeguimientoGPS != null))
                    {
                        Excel.ExcelWorksheet oWsSeguimientoGPS = oEx.Workbook.Worksheets.Add("seguimientoGPS");
                        oWsSeguimientoGPS.Cells[1, 1].Value = "ID";
                        oWsSeguimientoGPS.Cells[1, 2].Value = "FEC. INICIO";
                        oWsSeguimientoGPS.Cells[1, 3].Value = "HORA INICIO";
                        oWsSeguimientoGPS.Cells[1, 4].Value = "FEC. FIN";
                        oWsSeguimientoGPS.Cells[1, 5].Value = "HORA FIN";
                        oWsSeguimientoGPS.Cells[1, 6].Value = "GESTION";
                        oWsSeguimientoGPS.Cells[1, 7].Value = "PDV: Codigo";
                        oWsSeguimientoGPS.Cells[1, 8].Value = "PDV: Descripcion";
                        oWsSeguimientoGPS.Cells[1, 9].Value = "SUPERVISOR";
                        oWsSeguimientoGPS.Cells[1, 10].Value = "GESTOR";
                        oWsSeguimientoGPS.Cells[1, 11].Value = "ESTADO";
                        oWsSeguimientoGPS.Cells[1, 12].Value = "MOTIVO";
                        oWsSeguimientoGPS.Cells[1, 13].Value = "LATITUD_INICIO";
                        oWsSeguimientoGPS.Cells[1, 14].Value = "LONGITUD_INICIO";
                        oWsSeguimientoGPS.Cells[1, 15].Value = "LATITUD_FIN";
                        oWsSeguimientoGPS.Cells[1, 16].Value = "LONGITUD_FIN";

                        _fila = 2;
                        foreach (E_Recorrido_Gie oBj in oSeguimientoGPS)
                        {
                            oWsSeguimientoGPS.Cells[_fila, 1].Value = oBj.Ids;
                            oWsSeguimientoGPS.Cells[_fila, 2].Value = oBj.Fecha_Inicio;
                            oWsSeguimientoGPS.Cells[_fila, 3].Value = oBj.Hora_Incio;
                            oWsSeguimientoGPS.Cells[_fila, 4].Value = oBj.Fecha_Fin;
                            oWsSeguimientoGPS.Cells[_fila, 5].Value = oBj.Hora_Fin;
                            oWsSeguimientoGPS.Cells[_fila, 6].Value = oBj.Gestion;
                            oWsSeguimientoGPS.Cells[_fila, 7].Value = oBj.pdv_cod;
                            oWsSeguimientoGPS.Cells[_fila, 8].Value = oBj.Nom_PDV;
                            oWsSeguimientoGPS.Cells[_fila, 9].Value = oBj.Nom_Supervisor;
                            oWsSeguimientoGPS.Cells[_fila, 10].Value = oBj.Nom_Gie;
                            oWsSeguimientoGPS.Cells[_fila, 11].Value = oBj.Estado;
                            oWsSeguimientoGPS.Cells[_fila, 12].Value = oBj.No_Visita;
                            oWsSeguimientoGPS.Cells[_fila, 13].Value = oBj.Latitud;
                            oWsSeguimientoGPS.Cells[_fila, 14].Value = oBj.Longitud;
                            oWsSeguimientoGPS.Cells[_fila, 15].Value = oBj.Latitud_Fin;
                            oWsSeguimientoGPS.Cells[_fila, 16].Value = oBj.Longitud_Fin;
                            _fila++;

                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsSeguimientoGPS.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                        oWsSeguimientoGPS.Row(1).Height = 25;
                        oWsSeguimientoGPS.Row(1).Style.Font.Bold = true;
                        oWsSeguimientoGPS.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsSeguimientoGPS.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsSeguimientoGPS.Column(1).AutoFit();
                        oWsSeguimientoGPS.Column(2).AutoFit();
                        oWsSeguimientoGPS.Column(3).AutoFit();
                        oWsSeguimientoGPS.Column(4).AutoFit();
                        oWsSeguimientoGPS.Column(5).AutoFit();
                        oWsSeguimientoGPS.Column(6).AutoFit();
                        oWsSeguimientoGPS.Column(7).AutoFit();
                        oWsSeguimientoGPS.Column(8).AutoFit();
                        oWsSeguimientoGPS.Column(9).AutoFit();
                        oWsSeguimientoGPS.Column(10).AutoFit();
                        oWsSeguimientoGPS.Column(11).AutoFit();
                        oWsSeguimientoGPS.Column(12).AutoFit();
                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }
        }
        #endregion

        #region Seguimiento GPS v2
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-03-29
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Seguimiento() {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }

            Persona oPersona = (Persona)Session["Session_Login"];

            ViewBag.tpf_id = oPersona.tpf_id;
            ViewBag.cam_codigo = Request.QueryString["_a"];

            /* Registra visita
            Utilitario.registrar_visita(Request.QueryString["_b"]); */

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-05-09
        /// </summary>
        /// <param name="__a"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Seguimiento(string __a)
        {
            Parametro oParametro = MvcApplication._Deserialize<Parametro>(__a);

            if (oParametro.opcion == 5 || oParametro.opcion == 7 || oParametro.opcion == 8 || oParametro.opcion == 10 || oParametro.opcion == 11 || oParametro.opcion == 12 || oParametro.opcion == 13 || oParametro.opcion == 14 || oParametro.opcion == 15 || oParametro.opcion == 16 || oParametro.opcion == 17 || oParametro.opcion == 18 || oParametro.opcion == 19 || oParametro.opcion == 20 || oParametro.opcion == 21 || oParametro.opcion == 22)
            {
                string Evento = Request["evento"];

                if (Evento == "Descarga")
                {
                    #region Descarga informacion
                    int Fila = 2;
                    string NombreServidor = "";
                    string RutaLocal = "";

                    NombreServidor = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);
                    RutaLocal = System.IO.Path.Combine(LocalTemp, NombreServidor);

                    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(RutaLocal)))
                    {
                        List<SeguimientoGpsSupervisor> lSeguimientoGpsSupervisor = MvcApplication._Deserialize<List<SeguimientoGpsSupervisor>>(Request["origen"]);

                        Excel.ExcelWorksheet oSh = oEx.Workbook.Worksheets.Add("Reporte Seguimiento GPS");

                        #region Encabezado
                        oSh.Cells["A1"].Value = "ID";
                        oSh.Cells["B1"].Value = "FEC. INICIO";
                        oSh.Cells["C1"].Value = "HORA INICIO";
                        oSh.Cells["D1"].Value = "FEC. FIN";
                        oSh.Cells["E1"].Value = "HORA FIN";
                        oSh.Cells["F1"].Value = "GESTION (MIN)";
                        oSh.Cells["G1"].Value = "PDV Codigo";
                        oSh.Cells["H1"].Value = "PDV Descripcion";
                        oSh.Cells["I1"].Value = "PDV Direccion";
                        oSh.Cells["J1"].Value = "PDV Latitud";
                        oSh.Cells["K1"].Value = "PDV Longitud";
                        oSh.Cells["L1"].Value = "SUPERVISOR: ID";
                        oSh.Cells["M1"].Value = "SUPERVISOR";
                        oSh.Cells["N1"].Value = "GESTOR: ID";
                        oSh.Cells["O1"].Value = "GESTOR";
                        oSh.Cells["P1"].Value = "DISTANCIA";
                        oSh.Cells["Q1"].Value = "ESTADO";
                        oSh.Cells["R1"].Value = "MOTIVO";
                        #endregion

                        foreach (SeguimientoGpsSupervisor oBj in lSeguimientoGpsSupervisor)
                        {

                            foreach (SeguimientoGpsSupervisorGie oSeguimientoGpsSupervisorGie in oBj.gie)
                            {
                                #region PDV
                                foreach (SeguimientoGpsSupervisorGiePdv oSeguimientoGpsSupervisorGiePdv in oSeguimientoGpsSupervisorGie.pdv)
                                {
                                    oSh.Cells[Fila, 1].Value = Fila - 1;

                                    oSh.Cells[Fila, 2].Value = oSeguimientoGpsSupervisorGiePdv.fecha_inicio;
                                    oSh.Cells[Fila, 3].Value = oSeguimientoGpsSupervisorGiePdv.hora_inicio;
                                    oSh.Cells[Fila, 4].Value = oSeguimientoGpsSupervisorGiePdv.fecha_fin;
                                    oSh.Cells[Fila, 5].Value = oSeguimientoGpsSupervisorGiePdv.hora_fin;
                                    oSh.Cells[Fila, 6].Value = oSeguimientoGpsSupervisorGiePdv.gestion;
                                    oSh.Cells[Fila, 7].Value = oSeguimientoGpsSupervisorGiePdv.pdv_codigo;
                                    oSh.Cells[Fila, 8].Value = oSeguimientoGpsSupervisorGiePdv.pdv_descripcion;
                                    oSh.Cells[Fila, 9].Value = oSeguimientoGpsSupervisorGiePdv.direccion;
                                    oSh.Cells[Fila, 10].Value = oSeguimientoGpsSupervisorGiePdv.latitud_pdv;
                                    oSh.Cells[Fila, 11].Value = oSeguimientoGpsSupervisorGiePdv.longitud_pdv;
                                    oSh.Cells[Fila, 12].Value = oBj.sup_id;
                                    oSh.Cells[Fila, 13].Value = oBj.sup_nombre;
                                    oSh.Cells[Fila, 14].Value = oSeguimientoGpsSupervisorGie.gie_id;
                                    oSh.Cells[Fila, 15].Value = oSeguimientoGpsSupervisorGie.gie_nombre;
                                    oSh.Cells[Fila, 16].Value = oSeguimientoGpsSupervisorGiePdv.distancia;
                                    oSh.Cells[Fila, 17].Value = oSeguimientoGpsSupervisorGiePdv.estado;
                                    oSh.Cells[Fila, 18].Value = oSeguimientoGpsSupervisorGiePdv.no_visita;

                                    //oSh.Row(Fila).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                    //oSh.Row(Fila).Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                    //oSh.Row(Fila).Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                    //oSh.Row(Fila).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;
                                    Fila++;
                                }
                                #endregion
                            }
                        }

                        oSh.Row(1).Style.Font.Bold = true;
                        oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        oSh.Column(1).Width = 5;
                        oSh.Column(2).Width = 13;
                        oSh.Column(3).Width = 13;
                        oSh.Column(4).Width = 13;
                        oSh.Column(5).Width = 13;
                        oSh.Column(6).Width = 15;
                        oSh.Column(7).Width = 15;
                        oSh.Column(8).Width = 25;
                        oSh.Column(9).Width = 25;
                        oSh.Column(10).Width = 18;
                        oSh.Column(11).Width = 18;
                        oSh.Column(12).Width = 15;
                        oSh.Column(13).Width = 15;
                        oSh.Column(14).Width = 15;
                        oSh.Column(15).Width = 15;
                        oSh.Column(16).Width = 25;

                        oEx.Save();
                    }

                    return new ContentResult
                    {
                        Content = "{ \"Archivo\": \"" + NombreServidor + "\" }",
                        ContentType = "application/json"
                    };
                    #endregion
                }
                else
                {
                    #region Consulta informacion
                    return Content(new ContentResult
                    {
                        Content = MvcApplication._Serialize(new Seguimiento().Seguimiento(oParametro)),
                        ContentType = "application/json"
                    }.Content);
                    #endregion
                }
            }
            else if (oParametro.opcion == 6)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Seguimiento().Valida(oParametro)),
                    ContentType = "application/json"
                }.Content);
            }
            else
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Seguimiento().Lista(oParametro)),
                    ContentType = "application/json"
                }.Content);
            }
        }

        #region Seguimiento_v2_Cantidad_Relevo
        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-03-13
        /// </summary>
        [HttpPost]
        public ActionResult Seguimiento_v2_Cantidad_Relevo(string __a)
        {
            Parametro oParametro = MvcApplication._Deserialize<Parametro>(__a);

            if (/* CP  */(11 <= oParametro.opcion && oParametro.opcion <= 16) || oParametro.opcion == 19 || 
                /* AJE */oParametro.opcion == 17 || oParametro.opcion == -1)
            {
                #region Consulta informacion
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new Seguimiento().l_Seguimiento_v2_Cantidad_Relevo(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            return null;
        }
        #endregion

        #endregion

        #region <<< Xplora Backus >>>

        [CustomAuthorize]
        public ActionResult MapBackus(string _a)
        {
            ViewBag.veq = _a;
            ViewBag.url_img_map = ConfigurationManager.AppSettings["url_foto_maps"]; ;

            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }
        public ActionResult CoberturaBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo)
        {
            return View(new LuckyBackus().Cobertura(new Consultar_RepMapsBKGeneral_Request()
                       { oParametros = new E_Parametros_MapBK(){
                           Cod_Equipo = _vcanal,
                           Cod_Tipo = _vgrupo,
                           Cod_Ubieo = _vubigeo,
                           cod_pais = "589",
                           Cod_Cadena = _vcadena,
                           Periodo = _vperiodo                       
                       }
                       })
                   );  
        }
        public ActionResult PDVsCoberturaBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, int _vopcobertura)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new LuckyBackus()
                    .PDVsCobertura(new Consultar_RepMapsBKGeneral_Request() { oParametros = new E_Parametros_MapBK() {
                        Cod_Equipo = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Tipo_General = _vopcobertura
                    } }
                    )),
                ContentType = "application/json"
            }.Content);
        }
        public JsonResult ExportarCoberturaBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, int _vopcobertura)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Cobertura_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Cobertura >>

                List<E_Exportar_Cobertura_MapBK> oCobertura = new LuckyBackus().Exportar_cobertura(
                   new Consultar_RepMapsBKGeneral_Request() { oParametros = new E_Parametros_MapBK() {
                       Cod_Equipo = _vcanal,
                       Cod_Tipo = _vgrupo,
                       Cod_Ubieo = _vubigeo,
                       cod_pais = "589",
                       Cod_Cadena = _vcadena,
                       Periodo = _vperiodo,
                       Tipo_General = _vopcobertura
                   } }
                );

                #endregion

                #endregion

                #region <<< Reporte Cobertura >>>
                if ((oCobertura != null))
                {
                    Excel.ExcelWorksheet oWsCobertura = oEx.Workbook.Worksheets.Add("Reporte_Cobertura");
                    oWsCobertura.Cells[1, 1].Value = "COD PDV";
                    oWsCobertura.Cells[1, 2].Value = "NOMBRE PDV";
                    oWsCobertura.Cells[1, 3].Value = "RAZ SOCIAL";
                    oWsCobertura.Cells[1, 4].Value = "DIRECCION";
                    oWsCobertura.Cells[1, 5].Value = "CADENA";
                    oWsCobertura.Cells[1, 6].Value = "PAIS";
                    oWsCobertura.Cells[1, 7].Value = "DEPARTAMENTO";
                    oWsCobertura.Cells[1, 8].Value = "PROVINCIA";
                    oWsCobertura.Cells[1, 9].Value = "DISTRITO";
                    oWsCobertura.Cells[1, 10].Value = "AÑO";
                    oWsCobertura.Cells[1, 11].Value = "MES";
                    oWsCobertura.Cells[1, 12].Value = "SEMANA";
                    oWsCobertura.Cells[1, 13].Value = "CLUSTER";

                    _fila = 2;
                    foreach (E_Exportar_Cobertura_MapBK oBj in oCobertura)
                    {
                        oWsCobertura.Cells[_fila, 1].Value = oBj.Cod_PDV;
                        oWsCobertura.Cells[_fila, 2].Value = oBj.Nombre_PDV;
                        oWsCobertura.Cells[_fila, 3].Value = oBj.Raz_Social;
                        oWsCobertura.Cells[_fila, 4].Value = oBj.Direccion;
                        oWsCobertura.Cells[_fila, 5].Value = oBj.Cadena;
                        oWsCobertura.Cells[_fila, 6].Value = oBj.Pais;
                        oWsCobertura.Cells[_fila, 7].Value = oBj.Departamento;
                        oWsCobertura.Cells[_fila, 8].Value = oBj.Provincia;
                        oWsCobertura.Cells[_fila, 9].Value = oBj.Distrito;
                        oWsCobertura.Cells[_fila, 10].Value = oBj.Anio;
                        oWsCobertura.Cells[_fila, 11].Value = oBj.Mes;
                        oWsCobertura.Cells[_fila, 12].Value = oBj.Semana;
                        oWsCobertura.Cells[_fila, 13].Value = oBj.Cluster;
                        _fila++;
                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsCobertura.SelectedRange[1, 1, 1, 13].AutoFilter = true;
                    oWsCobertura.Row(1).Height = 25;
                    oWsCobertura.Row(1).Style.Font.Bold = true;
                    oWsCobertura.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsCobertura.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsCobertura.Column(1).AutoFit();
                    oWsCobertura.Column(2).AutoFit();
                    oWsCobertura.Column(3).AutoFit();
                    oWsCobertura.Column(4).AutoFit();
                    oWsCobertura.Column(5).AutoFit();
                    oWsCobertura.Column(6).AutoFit();
                    oWsCobertura.Column(7).AutoFit();
                    oWsCobertura.Column(8).AutoFit();
                    oWsCobertura.Column(9).AutoFit();
                    oWsCobertura.Column(10).AutoFit();
                    oWsCobertura.Column(11).AutoFit();
                    oWsCobertura.Column(12).AutoFit();
                    oWsCobertura.Column(13).AutoFit();

                    oWsCobertura.View.FreezePanes(2, 1);
                    

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }
        public ActionResult ListaCategoria(string _vequipo,int _vreporte)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Categoria().Lista(new Listar_Categoria_Por_CodCampania_y_CodReporte_Request() { campania = _vequipo, reporte = _vreporte }
                    )),
                ContentType = "application/json"
            }.Content);
        }
        public ActionResult PresenciaProductoBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria)
        {
            return View(new LuckyBackus().PresenciaProducto(new Consultar_RepMapsBKGeneral_Request()
            {oParametros = new E_Parametros_MapBK()
                {
                    Cod_Equipo = _vcanal,
                    Cod_Tipo = _vgrupo,
                    Cod_Ubieo = _vubigeo,
                    cod_pais = "589",
                    Cod_Cadena = _vcadena,
                    Periodo = _vperiodo,
                    Cod_Categoria = _vcategoria,
                    Cod_PDV = ""
                }}));  
        }
        public ActionResult PDVsPresenciaProductoBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new LuckyBackus()
                    .PDVsPresenciaProducto(new Consultar_RepMapsBKGeneral_Request()
                    {
                        oParametros = new E_Parametros_MapBK()
                        {
                            Cod_Equipo = _vcanal,
                            Cod_Tipo = _vgrupo,
                            Cod_Ubieo = _vubigeo,
                           cod_pais = "589",
                            Cod_Cadena = _vcadena,
                            Periodo = _vperiodo,
                            Cod_Categoria = _vcategoria,
                            cod_sku = _vsku,
                           Tipo_General = 2
                        }
                    }
                    )),
                ContentType = "application/json"
            }.Content);
        }
        public JsonResult ExportarPresenciaProductoBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Presencia_Producto_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Presencia Producto >>

                List<E_Export_Presencia_MapBK> oPresencia = new LuckyBackus().Exportar_PresenciaProducto(
                   new Consultar_RepMapsBKGeneral_Request()
                   { oParametros = new E_Parametros_MapBK()
                       {
                            Cod_Equipo = _vcanal,
                            Cod_Tipo = _vgrupo,
                            Cod_Ubieo = _vubigeo,
                            cod_pais = "589",
                            Cod_Cadena = _vcadena,
                            Periodo = _vperiodo,
                            Cod_Categoria = _vcategoria,
                            cod_sku = _vsku,
                            Tipo_General = 1
                       }
                   }
                );

                #endregion

                #endregion

                #region <<< Reporte Presencia Producto >>>
                if ((oPresencia != null))
                {
                    Excel.ExcelWorksheet oWsPresencia = oEx.Workbook.Worksheets.Add("Rep_PresenciaProducto");
                    oWsPresencia.Cells[1, 1].Value = "COD PDV";
                    oWsPresencia.Cells[1, 2].Value = "NOMBRE PDV";
                    oWsPresencia.Cells[1, 3].Value = "RAZ SOCIAL";
                    oWsPresencia.Cells[1, 4].Value = "DIRECCION";
                    oWsPresencia.Cells[1, 5].Value = "CADENA";
                    oWsPresencia.Cells[1, 6].Value = "SECTOR";
                    oWsPresencia.Cells[1, 7].Value = "PAIS";
                    oWsPresencia.Cells[1, 8].Value = "DEPARTAMENTO";
                    oWsPresencia.Cells[1, 9].Value = "PROVINCIA";
                    oWsPresencia.Cells[1, 10].Value = "DISTRITO";
                    oWsPresencia.Cells[1, 11].Value = "CATEGORIA";
                    oWsPresencia.Cells[1, 12].Value = "MARCA";
                    oWsPresencia.Cells[1, 13].Value = "COD_SKU";
                    oWsPresencia.Cells[1, 14].Value = "PRODUCTO";
                    oWsPresencia.Cells[1, 15].Value = "AÑO";
                    oWsPresencia.Cells[1, 16].Value = "MES";
                    oWsPresencia.Cells[1, 17].Value = "SEMANA";

                    _fila = 2;
                    foreach (E_Export_Presencia_MapBK oBj in oPresencia)
                    {
                        oWsPresencia.Cells[_fila, 1].Value = oBj.Cod_PDV;
                        oWsPresencia.Cells[_fila, 2].Value = oBj.Nombre_PDV;
                        oWsPresencia.Cells[_fila, 3].Value = oBj.Raz_Social;
                        oWsPresencia.Cells[_fila, 4].Value = oBj.Direccion;
                        oWsPresencia.Cells[_fila, 5].Value = oBj.Cadena;
                        oWsPresencia.Cells[_fila, 6].Value = oBj.Sector;
                        oWsPresencia.Cells[_fila, 7].Value = oBj.Pais;
                        oWsPresencia.Cells[_fila, 8].Value = oBj.Departamento;
                        oWsPresencia.Cells[_fila, 9].Value = oBj.Provincia;
                        oWsPresencia.Cells[_fila, 10].Value = oBj.Distrito;
                        oWsPresencia.Cells[_fila, 11].Value = oBj.Categoria;
                        oWsPresencia.Cells[_fila, 12].Value = oBj.Marca;
                        oWsPresencia.Cells[_fila, 13].Value = oBj.Cod_SKU;
                        oWsPresencia.Cells[_fila, 14].Value = oBj.Producto;
                        oWsPresencia.Cells[_fila, 15].Value = oBj.Anio;
                        oWsPresencia.Cells[_fila, 16].Value = oBj.Mes;
                        oWsPresencia.Cells[_fila, 17].Value = oBj.Semana;
                        _fila++;
                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsPresencia.SelectedRange[1, 1, 1, 17].AutoFilter = true;
                    oWsPresencia.Row(1).Height = 25;
                    oWsPresencia.Row(1).Style.Font.Bold = true;
                    oWsPresencia.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPresencia.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPresencia.Column(1).AutoFit();
                    oWsPresencia.Column(2).AutoFit();
                    oWsPresencia.Column(3).AutoFit();
                    oWsPresencia.Column(4).AutoFit();
                    oWsPresencia.Column(5).AutoFit();
                    oWsPresencia.Column(6).AutoFit();
                    oWsPresencia.Column(7).AutoFit();
                    oWsPresencia.Column(8).AutoFit();
                    oWsPresencia.Column(9).AutoFit();
                    oWsPresencia.Column(10).AutoFit();
                    oWsPresencia.Column(11).AutoFit();
                    oWsPresencia.Column(12).AutoFit();
                    oWsPresencia.Column(13).AutoFit();
                    oWsPresencia.Column(14).AutoFit();
                    oWsPresencia.Column(15).AutoFit();
                    oWsPresencia.Column(16).AutoFit();
                    oWsPresencia.Column(17).AutoFit();

                    oWsPresencia.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }
        public ActionResult PresenciaProductoBackusPDV(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vpdv)
        {
            return View(new LuckyBackus().PresenciaProductoPDV(new Consultar_RepMapsBKGeneral_Request()
            { oParametros = new E_Parametros_MapBK()
                {
                    Cod_Equipo = _vcanal,
                    Cod_Tipo = _vgrupo,
                    Cod_Ubieo = _vubigeo,
                    cod_pais = "589",
                    Cod_Cadena = _vcadena,
                    Periodo = _vperiodo,
                    Cod_Categoria = "",
                    Cod_PDV = _vpdv
                }
            }));         
        
        }



        public ActionResult PresenciaVisibilidadBackus(string _vcanal, string _vgrupo, string _vubigeo, string _vcadena, string _vperiodo, string _vcategoria, string _vmarca)
        {
            return View(new LuckyBackus().PresenciaVisibilidad(new Request_Elem_Visi_MapBK()
            {
                TipoUbigeo = _vgrupo,
                cod_ubieo = _vubigeo,
                cod_pais = "589",
                cod_cadena = _vcadena,
                cod_perido = _vperiodo,
                cod_categoria = _vcategoria,
                cod_marca = _vmarca,
                cod_tipo_canal = _vcanal
            }));    
        }
        public ActionResult ListaMarca(string _veq,int _vcategoria) {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Marca().Lista(new Request_GetMarca_Opcion_Campania_Categoria() { opcion = 521, campania = _veq, categoria = _vcategoria }
                    )),
                ContentType = "application/json"
            }.Content);        
        }
        public ActionResult ListaCadena(string _veq, int _vcanal)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new Cadena().Lista(new Lucky.Xplora.Models.Map.Request_GetCadena_Campania_TipoCanal() { campania = _veq, tipocanal = _vcanal })),
                ContentType = "application/json"
            }.Content);
        }
        public JsonResult ExportarPresenciaVisibilidadBackus(string _vcanal, string _vgrupo, string _vubigeo, string _vcadena, string _vperiodo, string _vcategoria, string _vmarca, string _velemento)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Presencia_Visibilidad_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Presencia Visibilidad >>

                List<Presencia_Elemento_Visibilidad_Exporta> oPresencia = new LuckyBackus().Exportar_PresenciaVisibilidad(
                   new Request_Elem_Visi_Export_MapBK()
                   {
                       oParametros = new Parametros_Presencia_Elemento_Visibilidad()
                       {
                            TipoUbigeo = _vgrupo,
                            cod_ubieo = _vubigeo,
                            cod_pais = "589",
                            cadena = _vcadena,
                            id_reportsplanning = _vperiodo,
                            cod_categoria = _vcategoria,
                            cod_marca = _vmarca,
                            cod_tipo_canal = _vcanal,
                            cod_Elemento = _velemento,
                        opcion = 1
                       }
                   }
                );

                #endregion

                #endregion

                #region <<< Reporte Presencia Visibilidad >>>
                if ((oPresencia != null))
                {
                    Excel.ExcelWorksheet oWsPresencia = oEx.Workbook.Worksheets.Add("Rep_PresenciaVisibilidad");
                    oWsPresencia.Cells[1, 1].Value = "COD PDV";
                    oWsPresencia.Cells[1, 2].Value = "NOMBRE PDV";
                    oWsPresencia.Cells[1, 3].Value = "RAZ SOCIAL";
                    oWsPresencia.Cells[1, 4].Value = "DIRECCION";
                    oWsPresencia.Cells[1, 5].Value = "CADENA";
                    oWsPresencia.Cells[1, 6].Value = "SECTOR";
                    oWsPresencia.Cells[1, 7].Value = "PAIS";
                    oWsPresencia.Cells[1, 8].Value = "DEPARTAMENTO";
                    oWsPresencia.Cells[1, 9].Value = "PROVINCIA";
                    oWsPresencia.Cells[1, 10].Value = "DISTRITO";
                    oWsPresencia.Cells[1, 11].Value = "FEC_REG_CELDA";
                    oWsPresencia.Cells[1, 12].Value = "CATEGORIA";
                    oWsPresencia.Cells[1, 13].Value = "MATERIAL";
                    oWsPresencia.Cells[1, 14].Value = "CANTIDAD";
                    oWsPresencia.Cells[1, 15].Value = "AÑO";
                    oWsPresencia.Cells[1, 16].Value = "MES";

                    _fila = 2;
                    foreach (Presencia_Elemento_Visibilidad_Exporta oBj in oPresencia)
                    {
                        oWsPresencia.Cells[_fila, 1].Value = oBj.cod_pdv;
                        oWsPresencia.Cells[_fila, 2].Value = oBj.nom_pdv;
                        oWsPresencia.Cells[_fila, 3].Value = oBj.raz_social;
                        oWsPresencia.Cells[_fila, 4].Value = oBj.direccion;
                        oWsPresencia.Cells[_fila, 5].Value = oBj.nom_cadena;
                        oWsPresencia.Cells[_fila, 6].Value = oBj.Sector;
                        oWsPresencia.Cells[_fila, 7].Value = oBj.Name_Country;
                        oWsPresencia.Cells[_fila, 8].Value = oBj.Name_dpto;
                        oWsPresencia.Cells[_fila, 9].Value = oBj.Name_City;
                        oWsPresencia.Cells[_fila, 10].Value = oBj.District;
                        oWsPresencia.Cells[_fila, 11].Value = oBj.fec_reg_cel;
                        oWsPresencia.Cells[_fila, 12].Value = oBj.nom_categoria;
                        oWsPresencia.Cells[_fila, 13].Value = oBj.nom_material;
                        oWsPresencia.Cells[_fila, 14].Value = oBj.cantidad;
                        oWsPresencia.Cells[_fila, 15].Value = oBj.anio;
                        oWsPresencia.Cells[_fila, 16].Value = oBj.mes;
                        
                        _fila++;
                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsPresencia.SelectedRange[1, 1, 1, 16].AutoFilter = true;
                    oWsPresencia.Row(1).Height = 25;
                    oWsPresencia.Row(1).Style.Font.Bold = true;
                    oWsPresencia.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsPresencia.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsPresencia.Column(1).AutoFit();
                    oWsPresencia.Column(2).AutoFit();
                    oWsPresencia.Column(3).AutoFit();
                    oWsPresencia.Column(4).AutoFit();
                    oWsPresencia.Column(5).AutoFit();
                    oWsPresencia.Column(6).AutoFit();
                    oWsPresencia.Column(7).AutoFit();
                    oWsPresencia.Column(8).AutoFit();
                    oWsPresencia.Column(9).AutoFit();
                    oWsPresencia.Column(10).AutoFit();
                    oWsPresencia.Column(11).AutoFit();
                    oWsPresencia.Column(12).AutoFit();
                    oWsPresencia.Column(13).AutoFit();
                    oWsPresencia.Column(14).AutoFit();
                    oWsPresencia.Column(15).AutoFit();
                    oWsPresencia.Column(16).AutoFit();

                    oWsPresencia.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });        
        }
        public ActionResult PDVsPresenciaVisibilidadBackus(string _vcanal, string _vgrupo, string _vubigeo, string _vcadena, string _vperiodo, string _vcategoria, string _vmarca, string _velemento)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new LuckyBackus()
                    .PDVsPresenciaVisibilidad(new Request_Elem_Visi_Export_MapBK()
                    {
                        oParametros = new Parametros_Presencia_Elemento_Visibilidad()
                        {
                            TipoUbigeo = _vgrupo,
                            cod_ubieo = _vubigeo,
                            cod_pais = "589",
                            cadena = _vcadena,
                            id_reportsplanning = _vperiodo,
                            cod_categoria = _vcategoria,
                            cod_marca = _vmarca,
                            cod_tipo_canal = _vcanal,
                            cod_Elemento = _velemento,
                            opcion = 2
                        }
                    }
                    )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 28/08/15
        /// </summary>
        /// <param name="vcodpdv"></param>
        /// <param name="vperiodo"></param>
        /// <returns></returns>
        public ActionResult TrackingDetallePDV(string vcodpdv, string vperiodo)
        {
            E_PuntoVentaDatosMapa oLs = new LuckyBackus().DetallePDV(
                new Obtener_DatosPuntosVentaMapa_Request()
                {
                    codPtoVenta=vcodpdv,
                    reportsPlanning=vperiodo
                });  

            return Json(new { Jdpdv = oLs });
        }

        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 28/08/15
        /// </summary>
        /// <param name="vcodpdv"></param>
        /// <param name="vperiodo"></param>
        /// <returns></returns>
        public ActionResult TrackingFotoPDV(int vperiodo, int vcodtipo, string vcodpdv)
        {
            E_Parametros_MapBK oParametros = new E_Parametros_MapBK();
            oParametros.Periodo = vperiodo;
            oParametros.Cod_Tipo = vcodtipo;
            oParametros.Cod_PDV = vcodpdv;

            List<E_Foto> oLs = new LuckyBackus().FotoPDV(
                new Consultar_RepMapsBKGeneral_Request()
                {
                    oParametros = oParametros
                });

            return Json(new { Jfotopdv = oLs });

        }
        /// <summary>
        /// Autor: yrodriguezs
        /// Fecha: 28/08/15
        /// </summary>
        /// <param name="vcodpdv"></param>
        /// <param name="vperiodo"></param>
        /// <returns></returns>
        public ActionResult TrackingFotograficoPDV(int vperiodo, int vcodtipo, string vcodpdv)
        {
            E_Parametros_MapBK oParametros = new E_Parametros_MapBK();
            oParametros.Periodo = vperiodo;
            oParametros.Cod_Tipo = vcodtipo;
            oParametros.Cod_PDV = vcodpdv;

            return View(new LuckyBackus().FotoPDV(
                new Consultar_RepMapsBKGeneral_Request()
                {
                    oParametros = oParametros
                }));
        }

        public ActionResult TrackingPrecioBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria)
        {
            return View(new LuckyBackus().TrackingPrecio(new Consultar_RepMapsBKGeneral_Request()
            {
                oParametros = new E_Parametros_MapBK()
                {
                    Cod_Equipo = _vcanal,
                    Cod_Tipo = _vgrupo,
                    Cod_Ubieo = _vubigeo,
                    cod_pais = "589",
                    Cod_Cadena = _vcadena,
                    Periodo = _vperiodo,
                    Cod_Categoria = _vcategoria,
                    Cod_PDV = ""
                }
            }));  
        }
        public ActionResult PDVsTrackingPrecioBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new LuckyBackus()
                    .PDVsTrackingPrecio(new Consultar_RepMapsBKGeneral_Request()
                    {
                        oParametros = new E_Parametros_MapBK()
                        {
                            Cod_Equipo = _vcanal,
                            Cod_Tipo = _vgrupo,
                            Cod_Ubieo = _vubigeo,
                            cod_pais = "589",
                            Cod_Cadena = _vcadena,
                            Periodo = _vperiodo,
                            Cod_Categoria = _vcategoria,
                            cod_sku = _vsku
                        }
                    }
                    )),
                ContentType = "application/json"
            }.Content);
        }
        public JsonResult ExportarTrackingPrecioBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Tracking_de_Precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Tracking de Precio >>

                List<E_Exportar_Tracking_MapBK> oTracking = new LuckyBackus().Exportar_Tracking_precio(
                   new Consultar_RepMapsBKGeneral_Request()
                   {
                       oParametros = new E_Parametros_MapBK()
                       {
                           Cod_Equipo = _vcanal,
                            Cod_Tipo = _vgrupo,
                            Cod_Ubieo = _vubigeo,
                            cod_pais = "589",
                            Cod_Cadena = _vcadena,
                            Periodo = _vperiodo,
                            Cod_Categoria = _vcategoria,
                            cod_sku = _vsku
                       }
                   }
                );

                #endregion

                #endregion

                #region <<< Reporte Presencia Visibilidad >>>
                if ((oTracking != null))
                {
                    Excel.ExcelWorksheet oWsTracking = oEx.Workbook.Worksheets.Add("Rep_Tracking_Precio");
                    oWsTracking.Cells[1, 1].Value = "COD PDV";
                    oWsTracking.Cells[1, 2].Value = "NOMBRE PDV";
                    oWsTracking.Cells[1, 3].Value = "DIRECCION";
                    oWsTracking.Cells[1, 4].Value = "CADENA";
                    oWsTracking.Cells[1, 5].Value = "SECTOR";
                    oWsTracking.Cells[1, 6].Value = "PAIS";
                    oWsTracking.Cells[1, 7].Value = "DEPARTAMENTO";
                    oWsTracking.Cells[1, 8].Value = "PROVINCIA";
                    oWsTracking.Cells[1, 9].Value = "DISTRITO";
                    oWsTracking.Cells[1, 10].Value = "CATEGORIA";
                    oWsTracking.Cells[1, 11].Value = "MARCA";
                    oWsTracking.Cells[1, 12].Value = "SKU";
                    oWsTracking.Cells[1, 13].Value = "NOMBRE PRODUCTO";
                    oWsTracking.Cells[1, 14].Value = "PRECIO REVENTA";
                    oWsTracking.Cells[1, 15].Value = "PRECIO PVP";
                    oWsTracking.Cells[1, 16].Value = "PRECIO COSTO";
                    oWsTracking.Cells[1, 17].Value = "FECHA CELGA";
                    oWsTracking.Cells[1, 18].Value = "Año";
                    oWsTracking.Cells[1, 19].Value = "MES";
                    oWsTracking.Cells[1, 20].Value = "SEMANA";


                    _fila = 2;
                    foreach (E_Exportar_Tracking_MapBK oBj in oTracking)
                    {
                        oWsTracking.Cells[_fila, 1].Value = oBj.Cod_PDV;
                        oWsTracking.Cells[_fila, 2].Value = oBj.Nombre_PDV;
                        oWsTracking.Cells[_fila, 3].Value = oBj.Direccion;
                        oWsTracking.Cells[_fila, 4].Value = oBj.Cadena;
                        oWsTracking.Cells[_fila, 5].Value = oBj.Sector;
                        oWsTracking.Cells[_fila, 6].Value = oBj.Pais;
                        oWsTracking.Cells[_fila, 7].Value = oBj.Departamento;
                        oWsTracking.Cells[_fila, 8].Value = oBj.Provincia;
                        oWsTracking.Cells[_fila, 9].Value = oBj.Distrito;
                        oWsTracking.Cells[_fila, 10].Value = oBj.Categoria;
                        oWsTracking.Cells[_fila, 11].Value = oBj.Marca;
                        oWsTracking.Cells[_fila, 12].Value = oBj.SKU;
                        oWsTracking.Cells[_fila, 13].Value = oBj.Nombre_Producto;
                        oWsTracking.Cells[_fila, 14].Value = oBj.Precio_Reventa;
                        oWsTracking.Cells[_fila, 15].Value = oBj.Precio_PVP;
                        oWsTracking.Cells[_fila, 16].Value = oBj.Precio_Costo;
                        oWsTracking.Cells[_fila, 17].Value = oBj.Fecha_reg_celda;
                        oWsTracking.Cells[_fila, 18].Value = oBj.Anio;
                        oWsTracking.Cells[_fila, 19].Value = oBj.Mes;
                        oWsTracking.Cells[_fila, 20].Value = oBj.Semana;

                        _fila++;
                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsTracking.SelectedRange[1, 1, 1, 20].AutoFilter = true;
                    oWsTracking.Row(1).Height = 25;
                    oWsTracking.Row(1).Style.Font.Bold = true;
                    oWsTracking.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsTracking.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsTracking.Column(1).AutoFit();
                    oWsTracking.Column(2).AutoFit();
                    oWsTracking.Column(3).AutoFit();
                    oWsTracking.Column(4).AutoFit();
                    oWsTracking.Column(5).AutoFit();
                    oWsTracking.Column(6).AutoFit();
                    oWsTracking.Column(7).AutoFit();
                    oWsTracking.Column(8).AutoFit();
                    oWsTracking.Column(9).AutoFit();
                    oWsTracking.Column(10).AutoFit();
                    oWsTracking.Column(11).AutoFit();
                    oWsTracking.Column(12).AutoFit();
                    oWsTracking.Column(13).AutoFit();
                    oWsTracking.Column(14).AutoFit();
                    oWsTracking.Column(15).AutoFit();
                    oWsTracking.Column(16).AutoFit();
                    oWsTracking.Column(17).AutoFit();
                    oWsTracking.Column(18).AutoFit();
                    oWsTracking.Column(19).AutoFit();
                    oWsTracking.Column(20).AutoFit();

                    oWsTracking.View.FreezePanes(2, 1);


                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }
        public ActionResult TrackingPrecioBackusPDV(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vpdv)
        {
            return View(new LuckyBackus().TrackingPrecioPDV(new Consultar_RepMapsBKGeneral_Request()
            {
                oParametros = new E_Parametros_MapBK()
                {
                    Cod_Equipo = _vcanal,
                    Cod_Tipo = _vgrupo,
                    Cod_Ubieo = _vubigeo,
                    cod_pais = "589",
                    Cod_Cadena = _vcadena,
                    Periodo = _vperiodo,
                    Cod_Categoria = "",
                    Cod_PDV = _vpdv
                }
            }));
        }
        public ActionResult ListaFiltroSod(int _vtipo, string _vparametro)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new LuckyBackus().ListaFitroSod(new Consultar_RepMapsBKGeneral_Request()
                {
                    oParametros = new E_Parametros_MapBK() {
                        Cod_Tipo = _vtipo,
                        Parametros = _vparametro
                    }
                })),
                ContentType = "application/json"
            }.Content);   
        }
        public ActionResult ShareOfDisplayBackus(string _vcanal,int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria,int _velemento,string _vnivel) {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new LuckyBackus()
                    .ShareOfDisplay(new Consultar_RepMapsBKGeneral_Request()
                    {
                        oParametros = new E_Parametros_MapBK()
                        {
                            Cod_Equipo = "9020110152013",
                            Cod_Tipo = _vgrupo,
                            Cod_Ubieo = _vubigeo,
                            cod_pais = "589",
                            Cod_Cadena = _vcadena,
                            Periodo = _vperiodo,
                            Cod_Categoria = _vcategoria,
                            Cod_Canal = _vcanal,
                            Tipo_Exhi = 0,
                            Cod_Elemento = _velemento,
                            Cod_Nivel = _vnivel
                        }
                    }
                    )),
                ContentType = "application/json"
            }.Content);
        }

        public ActionResult StockOutBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria)
        {
            return View(new LuckyBackus().StockOut(new Consultar_RepMapsBKGeneral_Request()
            {
                oParametros = new E_Parametros_MapBK()
                {
                    Cod_Equipo = _vcanal,
                    Cod_Tipo = _vgrupo,
                    Cod_Ubieo = _vubigeo,
                    cod_pais = "589",
                    Cod_Cadena = _vcadena,
                    Periodo = _vperiodo,
                    Cod_Categoria = _vcategoria,
                    Cod_PDV = "",
                }
            }));
        }
        public ActionResult PDVsStockOutBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new LuckyBackus()
                    .PDVsStockOut(new Consultar_RepMapsBKGeneral_Request()
                    {
                        oParametros = new E_Parametros_MapBK()
                        {
                             Cod_Equipo = _vcanal,
                             Cod_Tipo = _vgrupo,
                             Cod_Ubieo = _vubigeo,
                             cod_pais = "589",
                             Cod_Cadena = _vcadena,
                             Periodo = _vperiodo,
                             Cod_Categoria = _vcategoria,
                             cod_sku = _vsku,
                             Tipo_General = 2
                        }
                    }
                    )),
                ContentType = "application/json"
            }.Content);
        }
        public JsonResult ExportarStockOutBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Reporte_Stock_Out_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Stock Out >>

                List<E_Export_Stock_Out_MapBK> oStockOut = new LuckyBackus().Exportar_Stock_Out(
                   new Consultar_RepMapsBKGeneral_Request()
                   {
                       oParametros = new E_Parametros_MapBK()
                       {
                            Cod_Equipo = _vcanal,
                            Cod_Tipo = _vgrupo,
                            Cod_Ubieo = _vubigeo,
                            cod_pais = "589",
                            Cod_Cadena = _vcadena,
                            Periodo = _vperiodo,
                            Cod_Categoria = _vcategoria,
                            cod_sku = _vsku,
                            Tipo_General = 1
                       }
                   }
                );

                #endregion

                #endregion

                #region <<< Reporte Stock Out >>>
                if ((oStockOut != null))
                {
                    Excel.ExcelWorksheet oWsStockOut = oEx.Workbook.Worksheets.Add("Rep_Stock_Out");
                    oWsStockOut.Cells[1, 1].Value = "COD PDV";
                    oWsStockOut.Cells[1, 2].Value = "NOMBRE PDV";
                    oWsStockOut.Cells[1, 3].Value = "RAZ SOCIAL";
                    oWsStockOut.Cells[1, 4].Value = "DIRECCION";
                    oWsStockOut.Cells[1, 5].Value = "CADENA";
                    oWsStockOut.Cells[1, 6].Value = "SECTOR";
                    oWsStockOut.Cells[1, 7].Value = "PAIS";
                    oWsStockOut.Cells[1, 8].Value = "DEPARTAMENTO";
                    oWsStockOut.Cells[1, 9].Value = "PROVINCIA";
                    oWsStockOut.Cells[1, 10].Value = "DISTRITO";
                    oWsStockOut.Cells[1, 11].Value = "FEC_REG_CELDA";
                    oWsStockOut.Cells[1, 12].Value = "CATEGORIA";
                    oWsStockOut.Cells[1, 13].Value = "MARCA";
                    oWsStockOut.Cells[1, 14].Value = "SKU";
                    oWsStockOut.Cells[1, 15].Value = "NOMBRE PRODUCTO";
                    oWsStockOut.Cells[1, 16].Value = "QUIEBRE";
                    oWsStockOut.Cells[1, 17].Value = "AÑO";
                    oWsStockOut.Cells[1, 18].Value = "MES";
                    oWsStockOut.Cells[1, 19].Value = "SEMANA";                    


                    _fila = 2;
                    foreach (E_Export_Stock_Out_MapBK oBj in oStockOut)
                    {
                        oWsStockOut.Cells[_fila, 1].Value = oBj.Cod_PDV;
                        oWsStockOut.Cells[_fila, 2].Value = oBj.Nombre_PDV;
                        oWsStockOut.Cells[_fila, 3].Value = oBj.Raz_Social;
                        oWsStockOut.Cells[_fila, 4].Value = oBj.Direccion;
                        oWsStockOut.Cells[_fila, 5].Value = oBj.Cadena;
                        oWsStockOut.Cells[_fila, 6].Value = oBj.Sector;
                        oWsStockOut.Cells[_fila, 7].Value = oBj.Pais;
                        oWsStockOut.Cells[_fila, 8].Value = oBj.Departamento;
                        oWsStockOut.Cells[_fila, 9].Value = oBj.Provincia;
                        oWsStockOut.Cells[_fila, 10].Value = oBj.Distrito;
                        oWsStockOut.Cells[_fila, 11].Value = oBj.Fec_reg_cel;
                        oWsStockOut.Cells[_fila, 12].Value = oBj.Categoria;
                        oWsStockOut.Cells[_fila, 13].Value = oBj.Marca;
                        oWsStockOut.Cells[_fila, 14].Value = oBj.Cod_SKU;
                        oWsStockOut.Cells[_fila, 15].Value = oBj.Producto;
                        oWsStockOut.Cells[_fila, 16].Value = oBj.Quiebre;
                        oWsStockOut.Cells[_fila, 17].Value = oBj.Anio;
                        oWsStockOut.Cells[_fila, 18].Value = oBj.Mes;
                        oWsStockOut.Cells[_fila, 19].Value = oBj.Semana;
                        

                        _fila++;
                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsStockOut.SelectedRange[1, 1, 1, 19].AutoFilter = true;
                    oWsStockOut.Row(1).Height = 25;
                    oWsStockOut.Row(1).Style.Font.Bold = true;
                    oWsStockOut.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsStockOut.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsStockOut.Column(1).AutoFit();
                    oWsStockOut.Column(2).AutoFit();
                    oWsStockOut.Column(3).AutoFit();
                    oWsStockOut.Column(4).AutoFit();
                    oWsStockOut.Column(5).AutoFit();
                    oWsStockOut.Column(6).AutoFit();
                    oWsStockOut.Column(7).AutoFit();
                    oWsStockOut.Column(8).AutoFit();
                    oWsStockOut.Column(9).AutoFit();
                    oWsStockOut.Column(10).AutoFit();
                    oWsStockOut.Column(11).AutoFit();
                    oWsStockOut.Column(12).AutoFit();
                    oWsStockOut.Column(13).AutoFit();
                    oWsStockOut.Column(14).AutoFit();
                    oWsStockOut.Column(15).AutoFit();
                    oWsStockOut.Column(16).AutoFit();
                    oWsStockOut.Column(17).AutoFit();
                    oWsStockOut.Column(18).AutoFit();
                    oWsStockOut.Column(19).AutoFit();                    

                    oWsStockOut.View.FreezePanes(2, 1);

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }
        public ActionResult StockOutBackusPDV(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vpdv)
        {
            return View(new LuckyBackus().StockOutPDV(new Consultar_RepMapsBKGeneral_Request()
            {
                oParametros = new E_Parametros_MapBK()
                {
                    Cod_Equipo = _vcanal,
                    Cod_Tipo = _vgrupo,
                    Cod_Ubieo = _vubigeo,
                    cod_pais = "589",
                    Cod_Cadena = _vcadena,
                    Periodo = _vperiodo,
                    Cod_Categoria = "",
                    Cod_PDV = _vpdv
                }
            }));
        }
        public ActionResult StockOutRepOpeBackus(string _vcanal,int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria,string _vfechas,int _vtipoquiebre) {

            objListRepOperativo = new LuckyBackus().StockOutRepOperativo(new Consultar_RepMapsBKGeneral_Request()
            {
                oParametros = new E_Parametros_MapBK()
                {
                    Cod_Canal = _vcanal,
                    Cod_Tipo = _vgrupo,
                    Cod_Ubieo = _vubigeo,
                    cod_pais = "589",
                    Cod_Cadena = _vcadena,
                    Id_Reportsplanning = _vperiodo,
                    Cod_Categoria = _vcategoria,
                    Parametros = _vfechas,
                    Tipo_General = _vtipoquiebre
                }
            });

            return View(objListRepOperativo);
            
        }
        public JsonResult ExportarStockOutOperativoBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vfechas, int _vtipoquiebre)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("Stock_Out_Reporte_Operativo" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Stock Out >>

                //List<E_Reportes_StockOut_MapBK> oStockOut = new LuckyBackus().StockOutRepOperativo(
                //   new Consultar_RepMapsBKGeneral_Request()
                //   {
                //       oParametros = new E_Parametros_MapBK()
                //       {
                //           Cod_Canal = _vcanal,
                //           Cod_Tipo = _vgrupo,
                //           Cod_Ubieo = _vubigeo,
                //           cod_pais = "589",
                //           Cod_Cadena = _vcadena,
                //           Id_Reportsplanning = _vperiodo,
                //           Cod_Categoria = _vcategoria,
                //           Parametros = _vfechas,
                //           Tipo_General = _vtipoquiebre  
                //       }
                //   }
                //);
                List<E_Reportes_StockOut_MapBK> oStockOut = objListRepOperativo;
                #endregion

                #endregion

                #region <<< Reporte Stock Out >>>
                if ((oStockOut != null))
                {
                    Excel.ExcelWorksheet oWsStockOut = oEx.Workbook.Worksheets.Add("Stock_Out_Rep_Operativo");
                    oWsStockOut.Cells[1, 1].Value = "FECHA";
                    oWsStockOut.Cells[1, 2].Value = "CANAL";
                    oWsStockOut.Cells[1, 3].Value = "OFICINA";
                    oWsStockOut.Cells[1, 4].Value = "CADENA";
                    oWsStockOut.Cells[1, 5].Value = "TIENDA";
                    oWsStockOut.Cells[1, 6].Value = "EMPRESA";
                    oWsStockOut.Cells[1, 7].Value = "CATEGORIA";
                    oWsStockOut.Cells[1, 8].Value = "NIVEL";
                    oWsStockOut.Cells[1, 9].Value = "MARCA";
                    oWsStockOut.Cells[1, 10].Value = "PRODUCTO";
                    oWsStockOut.Cells[1, 11].Value = "TIPO QUIEBRE";

                    _fila = 2;
                    foreach (E_Reportes_StockOut_MapBK oBj in oStockOut)
                    {
                        oWsStockOut.Cells[_fila, 1].Value = oBj.Fecha;
                        oWsStockOut.Cells[_fila, 2].Value = oBj.Canal;
                        oWsStockOut.Cells[_fila, 3].Value = oBj.Oficina;
                        oWsStockOut.Cells[_fila, 4].Value = oBj.Cadena;
                        oWsStockOut.Cells[_fila, 5].Value = oBj.Tienda;
                        oWsStockOut.Cells[_fila, 6].Value = oBj.Empresa;
                        oWsStockOut.Cells[_fila, 7].Value = oBj.Categoria;
                        oWsStockOut.Cells[_fila, 8].Value = oBj.Nivel;
                        oWsStockOut.Cells[_fila, 9].Value = oBj.Marca;
                        oWsStockOut.Cells[_fila, 10].Value = oBj.Producto;
                        oWsStockOut.Cells[_fila, 11].Value = oBj.Tipo_Quiebre;

                        _fila++;
                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsStockOut.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                    oWsStockOut.Row(1).Height = 25;
                    oWsStockOut.Row(1).Style.Font.Bold = true;
                    oWsStockOut.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsStockOut.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsStockOut.Column(1).AutoFit();
                    oWsStockOut.Column(2).AutoFit();
                    oWsStockOut.Column(3).AutoFit();
                    oWsStockOut.Column(4).AutoFit();
                    oWsStockOut.Column(5).AutoFit();
                    oWsStockOut.Column(6).AutoFit();
                    oWsStockOut.Column(7).AutoFit();
                    oWsStockOut.Column(8).AutoFit();
                    oWsStockOut.Column(9).AutoFit();
                    oWsStockOut.Column(10).AutoFit();
                    oWsStockOut.Column(11).AutoFit();

                    oWsStockOut.View.FreezePanes(2, 1);

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }
        public ActionResult StockOutRepRangPDVOpeBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, string _vanio, string _vcategoria, int _vtipoquiebre)
        {
            objListRepOpeRangPDV = new LuckyBackus().StockOutRepRangPDVOperativo(new Consultar_RepMapsBKGeneral_Request()
            {

                oParametros = new E_Parametros_MapBK()
                {
                    Cod_Canal = _vcanal,
                    Cod_Tipo = _vgrupo,
                    Cod_Ubieo = _vubigeo,
                    cod_pais = "589",
                    Cod_Cadena = _vcadena,
                    Anio = _vanio,
                    Cod_Categoria = _vcategoria,
                    Tipo_General = _vtipoquiebre
                }
            });
            return View(objListRepOpeRangPDV);
        }
        public JsonResult ExportarStockOutOpeRangPDVBackus(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, string _vanio, string _vcategoria, int _vtipoquiebre)
        {
            string ruta;
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;
            ruta = Convert.ToString(ConfigurationManager.AppSettings["xplora"]);

            _fileServer = String.Format("Stock_Out_Reporte_Ope_RangPDV" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(ruta + "/Temp", _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Stock Out >>

                //List<E_Reportes_RangPDV_StockOut_MapBK> oStockOut = new LuckyBackus().StockOutRepRangPDVOperativo(
                //   new Consultar_RepMapsBKGeneral_Request()
                //   {
                //       oParametros = new E_Parametros_MapBK()
                //       {
                //           Cod_Canal = _vcanal,
                //           Cod_Tipo = _vgrupo,
                //           Cod_Ubieo = _vubigeo,
                //           cod_pais = "589",
                //           Cod_Cadena = _vcadena,
                //           Anio = _vanio,
                //           Cod_Categoria = _vcategoria,
                //           Tipo_General = _vtipoquiebre
                //       }
                //   }
                //);
                List<E_Reportes_RangPDV_StockOut_MapBK> oStockOut = objListRepOpeRangPDV;
                #endregion

                #endregion

                #region <<< Reporte Stock Out >>>
                if ((oStockOut != null))
                {
                    Excel.ExcelWorksheet oWsStockOut = oEx.Workbook.Worksheets.Add("Stock_Out_Rep_RangPDV");
                    oWsStockOut.Cells[1, 1].Value = "CANAL";
                    oWsStockOut.Cells[1, 2].Value = "CADENA";
                    oWsStockOut.Cells[1, 3].Value = "NOMBRE PDV";
                    oWsStockOut.Cells[1, 4].Value = "ENE";
                    oWsStockOut.Cells[1, 5].Value = "FEB";
                    oWsStockOut.Cells[1, 6].Value = "MAR";
                    oWsStockOut.Cells[1, 7].Value = "ABR";
                    oWsStockOut.Cells[1, 8].Value = "MAY";
                    oWsStockOut.Cells[1, 9].Value = "JUN";
                    oWsStockOut.Cells[1, 10].Value = "JUL";
                    oWsStockOut.Cells[1, 11].Value = "AGO";
                    oWsStockOut.Cells[1, 12].Value = "SEP";
                    oWsStockOut.Cells[1, 13].Value = "OCT";
                    oWsStockOut.Cells[1, 14].Value = "NOV";
                    oWsStockOut.Cells[1, 15].Value = "DIC";
                    oWsStockOut.Cells[1, 16].Value = "TOTAL";

                    _fila = 2;
                    foreach (E_Reportes_RangPDV_StockOut_MapBK oBj in oStockOut)
                    {
                        oWsStockOut.Cells[_fila, 1].Value = oBj.Canal;
                        oWsStockOut.Cells[_fila, 2].Value = oBj.Cadena;
                        oWsStockOut.Cells[_fila, 3].Value = oBj.Nom_PDV;
                        oWsStockOut.Cells[_fila, 4].Value = oBj.Ene;
                        oWsStockOut.Cells[_fila, 5].Value = oBj.Feb;
                        oWsStockOut.Cells[_fila, 6].Value = oBj.Mar;
                        oWsStockOut.Cells[_fila, 7].Value = oBj.Abr;
                        oWsStockOut.Cells[_fila, 8].Value = oBj.May;
                        oWsStockOut.Cells[_fila, 9].Value = oBj.Jun;
                        oWsStockOut.Cells[_fila, 10].Value = oBj.Jul;
                        oWsStockOut.Cells[_fila, 11].Value = oBj.Ago;
                        oWsStockOut.Cells[_fila, 12].Value = oBj.Sep;
                        oWsStockOut.Cells[_fila, 13].Value = oBj.Oct;
                        oWsStockOut.Cells[_fila, 14].Value = oBj.Nov;
                        oWsStockOut.Cells[_fila, 15].Value = oBj.Dic;
                        oWsStockOut.Cells[_fila, 16].Value = oBj.Total;                        

                        _fila++;
                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsStockOut.SelectedRange[1, 1, 1, 16].AutoFilter = true;
                    oWsStockOut.Row(1).Height = 25;
                    oWsStockOut.Row(1).Style.Font.Bold = true;
                    oWsStockOut.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsStockOut.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsStockOut.Column(1).AutoFit();
                    oWsStockOut.Column(2).AutoFit();
                    oWsStockOut.Column(3).AutoFit();
                    oWsStockOut.Column(4).AutoFit();
                    oWsStockOut.Column(5).AutoFit();
                    oWsStockOut.Column(6).AutoFit();
                    oWsStockOut.Column(7).AutoFit();
                    oWsStockOut.Column(8).AutoFit();
                    oWsStockOut.Column(9).AutoFit();
                    oWsStockOut.Column(10).AutoFit();
                    oWsStockOut.Column(11).AutoFit();
                    oWsStockOut.Column(12).AutoFit();
                    oWsStockOut.Column(13).AutoFit();
                    oWsStockOut.Column(14).AutoFit();
                    oWsStockOut.Column(15).AutoFit();
                    oWsStockOut.Column(16).AutoFit();

                    oWsStockOut.View.FreezePanes(2, 1);

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }
        public ActionResult StockOutRepRangSKUOpeBackus(string _vanio, string _vtipoquiebre, string _vcategoria)
        {
            objListRepOpeRangSKU = new LuckyBackus().StockOutRepRangSKUOperativo(new Request_RankingSku_Anio_TipoQuiebre_Categoria()
            {
                anio = _vanio,
                tipo_quiebre = _vtipoquiebre,
                categoria = _vcategoria
            });
            return View(objListRepOpeRangSKU);
        }
        public JsonResult ExportarStockOutOpeRangSKUBackus(string _vanio, string _vtipoquiebre, string _vcategoria)
        {
            string LocalTemp;
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;
            LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

            _fileServer = String.Format("Stock_Out_Reporte_Ope_RangSKU" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte Stock Out >>

                //List<Ranking_Sku> oStockOut = new LuckyBackus().StockOutRepRangSKUOperativo(
                //   new Request_RankingSku_Anio_TipoQuiebre_Categoria()
                //   {
                //       anio = _vanio,
                //       tipo_quiebre = _vtipoquiebre,
                //       categoria = _vcategoria
                //   }
                //);
                List<Ranking_Sku> oStockOut = objListRepOpeRangSKU;
                #endregion

                #endregion

                #region <<< Reporte Stock Out >>>
                if ((oStockOut != null))
                {
                    Excel.ExcelWorksheet oWsStockOut = oEx.Workbook.Worksheets.Add("Stock_Out_Rep_RangSKU");
                    oWsStockOut.Cells[1, 1].Value = "SKU";
                    oWsStockOut.Cells[1, 2].Value = "ENE";
                    oWsStockOut.Cells[1, 3].Value = "FEB";
                    oWsStockOut.Cells[1, 4].Value = "MAR";
                    oWsStockOut.Cells[1, 5].Value = "ABR";
                    oWsStockOut.Cells[1, 6].Value = "MAY";
                    oWsStockOut.Cells[1, 7].Value = "JUN";
                    oWsStockOut.Cells[1, 8].Value = "JUL";
                    oWsStockOut.Cells[1, 9].Value = "AGO";
                    oWsStockOut.Cells[1, 10].Value = "SEP";
                    oWsStockOut.Cells[1, 11].Value = "OCT";
                    oWsStockOut.Cells[1, 12].Value = "NOV";
                    oWsStockOut.Cells[1, 13].Value = "DIC";
                    oWsStockOut.Cells[1, 14].Value = "TOTAL";

                    _fila = 2;
                    foreach (Ranking_Sku oBj in oStockOut)
                    {
                        oWsStockOut.Cells[_fila, 1].Value = oBj.sku_descripcion;
                        oWsStockOut.Cells[_fila, 2].Value = oBj.enero;
                        oWsStockOut.Cells[_fila, 3].Value = oBj.febrero;
                        oWsStockOut.Cells[_fila, 4].Value = oBj.marzo;
                        oWsStockOut.Cells[_fila, 5].Value = oBj.abril;
                        oWsStockOut.Cells[_fila, 6].Value = oBj.mayo;
                        oWsStockOut.Cells[_fila, 7].Value = oBj.junio;
                        oWsStockOut.Cells[_fila, 8].Value = oBj.julio;
                        oWsStockOut.Cells[_fila, 9].Value = oBj.agosto;
                        oWsStockOut.Cells[_fila, 10].Value = oBj.septiembre;
                        oWsStockOut.Cells[_fila, 11].Value = oBj.octubre;
                        oWsStockOut.Cells[_fila, 12].Value = oBj.noviembre;
                        oWsStockOut.Cells[_fila, 13].Value = oBj.diciembre;
                        oWsStockOut.Cells[_fila, 14].Value = oBj.total;


                        _fila++;
                    }

                    //Formato Cabecera
                    //Formato Cabecera
                    oWsStockOut.SelectedRange[1, 1, 1, 14].AutoFilter = true;
                    oWsStockOut.Row(1).Height = 25;
                    oWsStockOut.Row(1).Style.Font.Bold = true;
                    oWsStockOut.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                    oWsStockOut.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                    //Columnas auto ajustadas
                    oWsStockOut.Column(1).AutoFit();
                    oWsStockOut.Column(2).AutoFit();
                    oWsStockOut.Column(3).AutoFit();
                    oWsStockOut.Column(4).AutoFit();
                    oWsStockOut.Column(5).AutoFit();
                    oWsStockOut.Column(6).AutoFit();
                    oWsStockOut.Column(7).AutoFit();
                    oWsStockOut.Column(8).AutoFit();
                    oWsStockOut.Column(9).AutoFit();
                    oWsStockOut.Column(10).AutoFit();
                    oWsStockOut.Column(11).AutoFit();
                    oWsStockOut.Column(12).AutoFit();
                    oWsStockOut.Column(13).AutoFit();
                    oWsStockOut.Column(14).AutoFit();

                    oWsStockOut.View.FreezePanes(2, 1);

                    oEx.Save();
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion

            }
            return Json(new { Archivo = _fileServer });
        }
        #endregion

        #region Xplora Fusion Colgate
            public ActionResult MapsFusionColg(string _a)
            {
                //813622482010 Mayor
                //0133725102010 Menor
                //4111304112013 AASS
                //0134226102010 Farmacia IT
                //0134126102010 Farmacia DT
                //012011092692011 Bodega
                ViewBag.veq = _a;
                ViewBag.labelMarca = "";
                ViewBag.showMarca = true;
                if (_a == "813622482010" || _a == "0133725102010" || _a == "4111304112013" || _a == "0134226102010" || _a == "0134126102010")
                {
                    if (_a == "813622482010" || _a == "0133725102010") {
                        ViewBag.labelMarca="Mercado";
                    } else {
                        ViewBag.labelMarca="Cadena";
                    }
                    ViewBag.showMarca = true;
                } else {
                    ViewBag.showMarca = false;
                }

                //Se registra visita
                Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
                ViewBag.url_img_map = ConfigurationManager.AppSettings["url_foto_maps"];
           //  string con=   ConfigurationManager.ConnectionStrings[""].ConnectionString.ToString();
                return View();
            }

            [HttpPost]
            public ActionResult FusionColgFiltros(string __a, string __b, string __c)
            {
                ViewBag.cod_opcion = __a;
                ViewBag.cod_elemento = __c;

                return View(new New_XplMapsFsColg_Filtro_Service()
                        .Filtro(new Resquest_New_XPL_MapsFusionColg()
                        {
                            opcion = Convert.ToInt32(__a),
                            parametros = __b
                        })
                    );
            }

            [HttpPost]
            public JsonResult FusionColgFiltrosJson(string __a, string __b, string __c)
            {
                ViewBag.cod_opcion = __a;
                ViewBag.cod_elemento = __c;
                List<M_Combo> list = new New_XplMapsFsColg_Filtro_Service()
                        .Filtro(new Resquest_New_XPL_MapsFusionColg()
                        {
                            opcion = Convert.ToInt32(__a),
                            parametros = __b
                        });
                return Json(list, JsonRequestBehavior.AllowGet);
            }

            [HttpPost]
            public JsonResult ListaEmpresas(string __a, string __b, string __c)
            {
                ViewBag.cod_opcion = __a;
                ViewBag.cod_elemento = __c;
                List<M_Combo> list = new New_XplMapsFsColg_Filtro_Service()
                        .ListarEmpresas(new Resquest_ListaEmpresa()
                        {
                            campania = __a,
                            reporte_id = Convert.ToInt32(__b),
                            cod_categoria = __c
                        });
                return Json(list, JsonRequestBehavior.AllowGet);
            }

            public ActionResult CoberturaColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vpdv)
            {
                return View(new LuckyFusionColgate().Cobertura(new Consultar_RepMapsFCGeneral_Request()
                {
                    oParametros = new E_Parametros_MapFC()
                    {
                        Cod_Equipo = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_PDV = _vpdv
                    }
                })
                );
            }
            [HttpPost]
            public ActionResult PresenciaProductoColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca)
            {
                return View(new LuckyFusionColgate().PresenciaProducto(new Consultar_RepMapsFCGeneral_Request()
                {
                    oParametros = new E_Parametros_MapFC()
                    {
                        Cod_Equipo = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca

                    }
                }));
            }

            #region Presencia Visibilidad
                public ActionResult PresenciaVisibilidadColgate(string _vcanal, string _vgrupo, string _vubigeo, string _vcadena, string _vperiodo, string _vcategoria, string _vmarca, string _vpdv)
                {
                    ViewBag.veq = _vcanal;
                    return View(new LuckyFusionColgate().PresenciaVisibilidad(new Request_Elem_Visi_MapFC()
                    {
                        TipoUbigeo = _vgrupo,
                        cod_ubieo = _vubigeo,
                        cod_pais = "589",
                        cod_cadena = _vcadena,
                        cod_perido = _vperiodo,
                        cod_categoria = _vcategoria,
                        cod_marca = _vmarca,
                        cod_tipo_canal = _vcanal,
                        cod_pdv = _vpdv,
                        cod_sku = ""
                    }));
                }
                public ActionResult PDVsPresenciaVisibilidadColgate(string _vcanal, string _vgrupo, string _vubigeo, string _vcadena, string _vperiodo, string _vcategoria, string _vmarca, string _velemento, string _vpdv)
                {
                    return Content(new ContentResult
                    {
                        Content = MvcApplication._Serialize(new LuckyFusionColgate()
                            .PDVsPresenciaVisibilidad(new Request_Elem_Visi_Export_MapFC()
                            {
                                oParametros = new Parametros_Presencia_Elemento_VisibilidadFC()
                                {
                                    TipoUbigeo = _vgrupo,
                                    cod_ubieo = _vubigeo,
                                    cod_pais = "589",
                                    cadena = _vcadena,
                                    id_reportsplanning = _vperiodo,
                                    cod_categoria = _vcategoria,
                                    cod_marca = _vmarca,
                                    cod_tipo_canal = _vcanal,
                                    cod_Elemento = _velemento,
                                    cod_pdv = _vpdv,
                                    opcion = 2
                                }
                            }
                            )),
                        ContentType = "application/json"
                    }.Content);
                }
                public JsonResult ExportarPresenciaVisibilidadColgate(string _vcanal, string _vgrupo, string _vubigeo, string _vcadena, string _vperiodo, string _vcategoria, string _vmarca, string _velemento, string _vpdv)
            {
                string LocalTemp;
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;
                LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

                _fileServer = String.Format("Reporte_Presencia_Visibilidad_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>
                    #region << Reporte Presencia Visibilidad >>

                    List<Presencia_Elemento_Visibilidad_ExportaFC> oPresencia = new LuckyFusionColgate().Exportar_PresenciaVisibilidad(
                       new Request_Elem_Visi_Export_MapFC()
                       {
                           oParametros = new Parametros_Presencia_Elemento_VisibilidadFC()
                           {
                               TipoUbigeo = _vgrupo,
                               cod_ubieo = _vubigeo,
                               cod_pais = "589",
                               cadena = _vcadena,
                               id_reportsplanning = _vperiodo,
                               cod_categoria = _vcategoria,
                               cod_marca = _vmarca,
                               cod_pdv = _vpdv,
                               cod_Elemento = _velemento,
                               opcion = 1
                           }
                       }
                    );

                    #endregion

                    #endregion

                    #region <<< Reporte Presencia Visibilidad >>>
                    if ((oPresencia != null))
                    {
                        Excel.ExcelWorksheet oWsPresencia = oEx.Workbook.Worksheets.Add("Rep_PresenciaVisibilidad");
                        oWsPresencia.Cells[1, 1].Value = "COD PDV";
                        oWsPresencia.Cells[1, 2].Value = "NOMBRE PDV";
                        oWsPresencia.Cells[1, 3].Value = "RAZ SOCIAL";
                        oWsPresencia.Cells[1, 4].Value = "DIRECCION";
                        oWsPresencia.Cells[1, 5].Value = "CADENA";
                        oWsPresencia.Cells[1, 6].Value = "SECTOR";
                        oWsPresencia.Cells[1, 7].Value = "PAIS";
                        oWsPresencia.Cells[1, 8].Value = "DEPARTAMENTO";
                        oWsPresencia.Cells[1, 9].Value = "PROVINCIA";
                        oWsPresencia.Cells[1, 10].Value = "DISTRITO";
                        oWsPresencia.Cells[1, 11].Value = "FEC_REG_CELDA";
                        oWsPresencia.Cells[1, 12].Value = "CATEGORIA";
                        oWsPresencia.Cells[1, 13].Value = "MATERIAL";
                        oWsPresencia.Cells[1, 14].Value = "CANTIDAD";
                        oWsPresencia.Cells[1, 15].Value = "AÑO";
                        oWsPresencia.Cells[1, 16].Value = "MES";
                        oWsPresencia.Cells[1, 17].Value = "PRESENCIA";

                        _fila = 2;
                        foreach (Presencia_Elemento_Visibilidad_ExportaFC oBj in oPresencia)
                        {
                            oWsPresencia.Cells[_fila, 1].Value = oBj.cod_pdv;
                            oWsPresencia.Cells[_fila, 2].Value = oBj.nom_pdv;
                            oWsPresencia.Cells[_fila, 3].Value = oBj.raz_social;
                            oWsPresencia.Cells[_fila, 4].Value = oBj.direccion;
                            oWsPresencia.Cells[_fila, 5].Value = oBj.nom_cadena;
                            oWsPresencia.Cells[_fila, 6].Value = oBj.Sector;
                            oWsPresencia.Cells[_fila, 7].Value = oBj.Name_Country;
                            oWsPresencia.Cells[_fila, 8].Value = oBj.Name_dpto;
                            oWsPresencia.Cells[_fila, 9].Value = oBj.Name_City;
                            oWsPresencia.Cells[_fila, 10].Value = oBj.District;
                            oWsPresencia.Cells[_fila, 11].Value = oBj.fec_reg_cel;
                            oWsPresencia.Cells[_fila, 12].Value = oBj.nom_categoria;
                            oWsPresencia.Cells[_fila, 13].Value = oBj.nom_material;
                            oWsPresencia.Cells[_fila, 14].Value = oBj.cantidad;
                            oWsPresencia.Cells[_fila, 15].Value = oBj.anio;
                            oWsPresencia.Cells[_fila, 16].Value = oBj.mes;
                            oWsPresencia.Cells[_fila, 17].Value = oBj.presencia;

                            _fila++;
                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsPresencia.SelectedRange[1, 1, 1, 17].AutoFilter = true;
                        oWsPresencia.Row(1).Height = 25;
                        oWsPresencia.Row(1).Style.Font.Bold = true;
                        oWsPresencia.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsPresencia.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsPresencia.Column(1).AutoFit();
                        oWsPresencia.Column(2).AutoFit();
                        oWsPresencia.Column(3).AutoFit();
                        oWsPresencia.Column(4).AutoFit();
                        oWsPresencia.Column(5).AutoFit();
                        oWsPresencia.Column(6).AutoFit();
                        oWsPresencia.Column(7).AutoFit();
                        oWsPresencia.Column(8).AutoFit();
                        oWsPresencia.Column(9).AutoFit();
                        oWsPresencia.Column(10).AutoFit();
                        oWsPresencia.Column(11).AutoFit();
                        oWsPresencia.Column(12).AutoFit();
                        oWsPresencia.Column(13).AutoFit();
                        oWsPresencia.Column(14).AutoFit();
                        oWsPresencia.Column(15).AutoFit();
                        oWsPresencia.Column(16).AutoFit();
                        oWsPresencia.Column(17).AutoFit();

                        oWsPresencia.View.FreezePanes(2, 1);


                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }
                public ActionResult PresenciaVisibilidadColgatePDV(string _vcanal, int _vperiodo, string _vpdv, string _vmarca, string _vcategoria)
                {
                    return View(new LuckyFusionColgate().PresenciaVisibilidadPDV(new Consultar_RepMapsFCGeneral_Request()
                    {
                        oParametros = new E_Parametros_MapFC()
                        {
                            Cod_Equipo = _vcanal,
                            Periodo = _vperiodo,
                            Cod_Categoria = _vcategoria,
                            Cod_PDV = _vpdv,
                            Cod_Marca = _vmarca
                        }
                    }));

                }
            #endregion

            [HttpPost]
            public ActionResult ActividadCompetenciaColgate(int __a, string __b, int __c, int __d, string __e, string __f, string __g)
            {
                E_Parametros_MapFC oParametros = new E_Parametros_MapFC();
                oParametros.Cod_Equipo = __g;
                oParametros.Cod_Tipo = __a;
                oParametros.Cod_Ubieo = __b;
                oParametros.cod_pais = "589";
                oParametros.Cod_Cadena = __c;
                oParametros.Periodo = __d;
                oParametros.Cod_Categoria = __e;

                return View(new LuckyFusionColgate()
                        .ActividadCompeMapFC(new Request_ActividadCompetenciaFC()
                        {
                            oParametros = oParametros
                        })
                    );
            }
            [HttpPost]
            public ActionResult DetalleActividadCompetenciaFC(int __a, string __b, int __c, int __d, string __e, string __f, string __g, string __h)
            {
                E_Parametros_MapFC oParametros = new E_Parametros_MapFC();
                oParametros.Cod_Equipo = __g;//canal
                oParametros.Cod_Tipo = __a;
                oParametros.Cod_Ubieo = __b;
                oParametros.cod_pais = "589";
                oParametros.Cod_Cadena = __c;
                oParametros.Periodo = __d;
                oParametros.Cod_Categoria = __e;
                oParametros.actividad = __f;
                oParametros.Cod_PDV = __h;

                List<E_Detalle_Activi_CompeMapFC> oLs = new LuckyFusionColgate().DetalleActividadCompeMapFC(
                    new Request_ActividadCompetenciaFC()
                    {
                        oParametros = oParametros
                    });
                return Json(new { Archivo = oLs });
            }

            [HttpPost]
            public ActionResult DetxPDVActividadCompetenciaFC(int __a, string __b, int __c, int __d, string __e, string __f, string __g, string __h)
            {
                E_Parametros_MapFC oParametros = new E_Parametros_MapFC();
                oParametros.Cod_Equipo = __g;//canal
                oParametros.Cod_Tipo = __a;
                oParametros.Cod_Ubieo = __b;
                oParametros.cod_pais = "589";
                oParametros.Cod_Cadena = __c;
                oParametros.Periodo = __d;
                oParametros.Cod_Categoria = __e;
                oParametros.actividad = __f;
                oParametros.Cod_PDV = __h;

                return View(new LuckyFusionColgate().DetalleActividadCompeMapFC(
                    new Request_ActividadCompetenciaFC()
                    {
                        oParametros = oParametros
                    })
                );

                //return View(new ReporteActividadesMenor().DetalleActividadCompeMapBK(
                //    new Request_ActividadCompetenciaBK()
                //    {
                //        oParametros = oParametros
                //    })
                //);
            }


            public ActionResult PDVsCoberturaColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, int _vopcobertura, string _vpdv)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new LuckyFusionColgate()
                        .PDVsCobertura(new Consultar_RepMapsFCGeneral_Request()
                        {
                            oParametros = new E_Parametros_MapFC()
                            {
                                Cod_Equipo = _vcanal,
                                Cod_Tipo = _vgrupo,
                                Cod_Ubieo = _vubigeo,
                                cod_pais = "589",
                                Cod_Cadena = _vcadena,
                                Periodo = _vperiodo,
                                Tipo_General = _vopcobertura,
                                Cod_PDV = _vpdv
                            }
                        }
                        )),
                    ContentType = "application/json"
                }.Content);
            }

            public JsonResult ExportarCoberturaColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, int _vopcobertura, string _vpdv)
            {
                string LocalTemp;
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;
                LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

                _fileServer = String.Format("Reporte_Cobertura_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>
                    #region << Reporte Cobertura >>

                    List<E_Exportar_Cobertura_MapFC> oCobertura = new LuckyFusionColgate().Exportar_cobertura(
                       new Consultar_RepMapsFCGeneral_Request()
                       {
                           oParametros = new E_Parametros_MapFC()
                           {
                               Cod_Equipo = _vcanal,
                               Cod_Tipo = _vgrupo,
                               Cod_Ubieo = _vubigeo,
                               cod_pais = "589",
                               Cod_Cadena = _vcadena,
                               Periodo = _vperiodo,
                               Tipo_General = _vopcobertura,
                               Cod_PDV = _vpdv
                           }
                       }
                    );

                    #endregion

                    #endregion

                    #region <<< Reporte Cobertura >>>
                    if ((oCobertura != null))
                    {
                        Excel.ExcelWorksheet oWsCobertura = oEx.Workbook.Worksheets.Add("Reporte_Cobertura");
                        oWsCobertura.Cells[1, 1].Value = "COD PDV";
                        oWsCobertura.Cells[1, 2].Value = "NOMBRE PDV";
                        oWsCobertura.Cells[1, 3].Value = "RAZ SOCIAL";
                        oWsCobertura.Cells[1, 4].Value = "DIRECCION";
                        oWsCobertura.Cells[1, 5].Value = "CADENA";
                        oWsCobertura.Cells[1, 6].Value = "PAIS";
                        oWsCobertura.Cells[1, 7].Value = "DEPARTAMENTO";
                        oWsCobertura.Cells[1, 8].Value = "PROVINCIA";
                        oWsCobertura.Cells[1, 9].Value = "DISTRITO";
                        oWsCobertura.Cells[1, 10].Value = "AÑO";
                        oWsCobertura.Cells[1, 11].Value = "MES";
                        oWsCobertura.Cells[1, 12].Value = "SEMANA";
                        oWsCobertura.Cells[1, 13].Value = "CLUSTER";

                        _fila = 2;
                        foreach (E_Exportar_Cobertura_MapFC oBj in oCobertura)
                        {
                            oWsCobertura.Cells[_fila, 1].Value = oBj.Cod_PDV;
                            oWsCobertura.Cells[_fila, 2].Value = oBj.Nombre_PDV;
                            oWsCobertura.Cells[_fila, 3].Value = oBj.Raz_Social;
                            oWsCobertura.Cells[_fila, 4].Value = oBj.Direccion;
                            oWsCobertura.Cells[_fila, 5].Value = oBj.Cadena;
                            oWsCobertura.Cells[_fila, 6].Value = oBj.Pais;
                            oWsCobertura.Cells[_fila, 7].Value = oBj.Departamento;
                            oWsCobertura.Cells[_fila, 8].Value = oBj.Provincia;
                            oWsCobertura.Cells[_fila, 9].Value = oBj.Distrito;
                            oWsCobertura.Cells[_fila, 10].Value = oBj.Anio;
                            oWsCobertura.Cells[_fila, 11].Value = oBj.Mes;
                            oWsCobertura.Cells[_fila, 12].Value = oBj.Semana;
                            oWsCobertura.Cells[_fila, 13].Value = oBj.Cluster;
                            _fila++;
                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsCobertura.SelectedRange[1, 1, 1, 13].AutoFilter = true;
                        oWsCobertura.Row(1).Height = 25;
                        oWsCobertura.Row(1).Style.Font.Bold = true;
                        oWsCobertura.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsCobertura.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsCobertura.Column(1).AutoFit();
                        oWsCobertura.Column(2).AutoFit();
                        oWsCobertura.Column(3).AutoFit();
                        oWsCobertura.Column(4).AutoFit();
                        oWsCobertura.Column(5).AutoFit();
                        oWsCobertura.Column(6).AutoFit();
                        oWsCobertura.Column(7).AutoFit();
                        oWsCobertura.Column(8).AutoFit();
                        oWsCobertura.Column(9).AutoFit();
                        oWsCobertura.Column(10).AutoFit();
                        oWsCobertura.Column(11).AutoFit();
                        oWsCobertura.Column(12).AutoFit();
                        oWsCobertura.Column(13).AutoFit();

                        oWsCobertura.View.FreezePanes(2, 1);


                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }
            /// <summary>
            /// Autor: Pccopa
            /// Fecha: 01/12/2015
            /// </summary>
            /// <param name="vcodpdv"></param>
            /// <param name="vperiodo"></param>
            /// <returns></returns>
            public ActionResult TrackingDetallePDVColgate(string vcodpdv, string vperiodo)
            {
                E_PuntoVentaDatosMapa oLs = new LuckyFusionColgate().DetallePDV(
                    new Obtener_DatosPuntosVentaMapa_Request()
                    {
                        codPtoVenta = vcodpdv,
                        reportsPlanning = vperiodo
                    });

                return Json(new { Jdpdv = oLs });
            }

            /// <summary>
            /// Autor: pccopa
            /// Fecha: 01/12/2015
            /// </summary>
            /// <param name="vcodpdv"></param>
            /// <param name="vperiodo"></param>
            /// <returns></returns>
            public ActionResult TrackingFotoPDVColgate(int vperiodo, int vcodtipo, string vcodpdv)
            {
                E_Parametros_MapFC oParametros = new E_Parametros_MapFC();
                oParametros.Periodo = vperiodo;
                oParametros.Cod_Tipo = vcodtipo;
                oParametros.Cod_PDV = vcodpdv;

                List<E_FotoFC> oLs = new LuckyFusionColgate().FotoPDV(
                    new Consultar_RepMapsFCGeneral_Request()
                    {
                        oParametros = oParametros
                    });

                return Json(new { Jfotopdv = oLs });

            }
            public ActionResult TrackingPrecioColgatePDV(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vpdv)
            {
                ViewBag.veq = _vcanal;
                return View(new LuckyFusionColgate().TrackingPrecioPDV(new Consultar_RepMapsFCGeneral_Request()
                {
                    oParametros = new E_Parametros_MapFC()
                    {
                        Cod_Equipo = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = "",
                        Cod_PDV = _vpdv
                    }
                }));
            }
            public ActionResult TrackingPrecioColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca)
            {
                return View(new LuckyFusionColgate().TrackingPrecio(new Consultar_RepMapsFCGeneral_Request()
                {
                    oParametros = new E_Parametros_MapFC()
                    {
                        Cod_Equipo = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca
                    }
                }));
            }
            public ActionResult PDVsTrackingPrecioColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku, string _vpdv, string _vmarca)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new LuckyFusionColgate()
                        .PDVsTrackingPrecio(new Consultar_RepMapsFCGeneral_Request()
                        {
                            oParametros = new E_Parametros_MapFC()
                            {
                                Cod_Equipo = _vcanal,
                                Cod_Tipo = _vgrupo,
                                Cod_Ubieo = _vubigeo,
                                cod_pais = "589",
                                Cod_Cadena = _vcadena,
                                Periodo = _vperiodo,
                                Cod_Categoria = _vcategoria,
                                cod_sku = _vsku,
                                Cod_Marca = _vmarca,
                                Cod_PDV = _vpdv                                
                            }
                        }
                        )),
                    ContentType = "application/json"
                }.Content);
            }
            public JsonResult ExportarTrackingPrecioColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku, string _vpdv, string _vmarca)
            {
                string LocalTemp;
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;
                LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

                _fileServer = String.Format("Reporte_Tracking_de_Precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>
                    #region << Reporte Tracking de Precio >>

                    List<E_Exportar_Tracking_MapFC> oTracking = new LuckyFusionColgate().Exportar_Tracking_precio(
                       new Consultar_RepMapsFCGeneral_Request()
                       {
                           oParametros = new E_Parametros_MapFC()
                           {
                               Cod_Equipo = _vcanal,
                               Cod_Tipo = _vgrupo,
                               Cod_Ubieo = _vubigeo,
                               cod_pais = "589",
                               Cod_Cadena = _vcadena,
                               Periodo = _vperiodo,
                               Cod_Categoria = _vcategoria,
                               cod_sku = _vsku,
                               Cod_Marca = _vmarca,
                               Cod_PDV = _vpdv
                           }
                       }
                    );

                    #endregion

                    #endregion

                    #region <<< Reporte Presencia Visibilidad >>>
                    if ((oTracking != null))
                    {
                        Excel.ExcelWorksheet oWsTracking = oEx.Workbook.Worksheets.Add("Rep_Tracking_Precio");
                        oWsTracking.Cells[1, 1].Value = "COD PDV";
                        oWsTracking.Cells[1, 2].Value = "NOMBRE PDV";
                        oWsTracking.Cells[1, 3].Value = "DIRECCION";
                        //oWsTracking.Cells[1, 4].Value = "CADENA";
                        oWsTracking.Cells[1, 4].Value = "SECTOR";
                        //oWsTracking.Cells[1, 6].Value = "PAIS";
                        //oWsTracking.Cells[1, 7].Value = "DEPARTAMENTO";
                        oWsTracking.Cells[1, 5].Value = "PROVINCIA";
                        oWsTracking.Cells[1, 6].Value = "DISTRITO";
                        //oWsTracking.Cells[1, 10].Value = "CATEGORIA";
                        //oWsTracking.Cells[1, 11].Value = "MARCA";
                        //oWsTracking.Cells[1, 12].Value = "SKU";
                        oWsTracking.Cells[1, 7].Value = "NOMBRE PRODUCTO";
                        //oWsTracking.Cells[1, 14].Value = "PRECIO REVENTA";
                        oWsTracking.Cells[1, 8].Value = "PRECIO PVP";
                        //oWsTracking.Cells[1, 16].Value = "PRECIO COSTO";
                        //oWsTracking.Cells[1, 17].Value = "FECHA CELGA";
                        //oWsTracking.Cells[1, 18].Value = "Año";
                        //oWsTracking.Cells[1, 19].Value = "MES";
                        //oWsTracking.Cells[1, 20].Value = "SEMANA";
                        oWsTracking.Cells[1, 9].Value = "PRECIO SUGERIDO";


                        _fila = 2;
                        foreach (E_Exportar_Tracking_MapFC oBj in oTracking)
                        {
                            oWsTracking.Cells[_fila, 1].Value = oBj.Cod_PDV;
                            oWsTracking.Cells[_fila, 2].Value = oBj.Nombre_PDV;
                            oWsTracking.Cells[_fila, 3].Value = oBj.Direccion;
                            //oWsTracking.Cells[_fila, 4].Value = oBj.Cadena;
                            oWsTracking.Cells[_fila, 4].Value = oBj.Sector;
                           // oWsTracking.Cells[_fila, 6].Value = oBj.Pais;
                           // oWsTracking.Cells[_fila, 7].Value = oBj.Departamento;
                            oWsTracking.Cells[_fila, 5].Value = oBj.Provincia;
                            oWsTracking.Cells[_fila, 6].Value = oBj.Distrito;
                            //oWsTracking.Cells[_fila, 10].Value = oBj.Categoria;
                            //oWsTracking.Cells[_fila, 11].Value = oBj.Marca;
                            //oWsTracking.Cells[_fila, 12].Value = oBj.SKU;
                            oWsTracking.Cells[_fila, 7].Value = oBj.Nombre_Producto;
                            //oWsTracking.Cells[_fila, 14].Value = oBj.Precio_Reventa;
                            oWsTracking.Cells[_fila, 8].Value = oBj.Precio_PVP;
                            //oWsTracking.Cells[_fila, 16].Value = oBj.Precio_Costo;
                            //oWsTracking.Cells[_fila, 17].Value = oBj.Fecha_reg_celda;
                            //oWsTracking.Cells[_fila, 18].Value = oBj.Anio;
                            //oWsTracking.Cells[_fila, 19].Value = oBj.Mes;
                            //oWsTracking.Cells[_fila, 20].Value = oBj.Semana;
                            oWsTracking.Cells[_fila, 9].Value = oBj.Precio_Sugerido;

                            _fila++;
                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsTracking.SelectedRange[1, 1, 1, 20].AutoFilter = true;
                        oWsTracking.Row(1).Height = 25;
                        oWsTracking.Row(1).Style.Font.Bold = true;
                        oWsTracking.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsTracking.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsTracking.Column(1).AutoFit();
                        oWsTracking.Column(2).AutoFit();
                        oWsTracking.Column(3).AutoFit();
                        oWsTracking.Column(4).AutoFit();
                        oWsTracking.Column(5).AutoFit();
                        oWsTracking.Column(6).AutoFit();
                        oWsTracking.Column(7).AutoFit();
                        oWsTracking.Column(8).AutoFit();
                        oWsTracking.Column(9).AutoFit();
                        //oWsTracking.Column(10).AutoFit();
                        //oWsTracking.Column(11).AutoFit();
                        //oWsTracking.Column(12).AutoFit();
                        //oWsTracking.Column(13).AutoFit();
                        //oWsTracking.Column(14).AutoFit();
                        //oWsTracking.Column(15).AutoFit();
                        //oWsTracking.Column(16).AutoFit();
                        //oWsTracking.Column(17).AutoFit();
                        //oWsTracking.Column(18).AutoFit();
                        //oWsTracking.Column(19).AutoFit();
                        //oWsTracking.Column(20).AutoFit();

                        oWsTracking.View.FreezePanes(2, 1);


                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }
            
            //Presencia Producto
            public ActionResult PresenciaProductoColgatePDV(string _vcanal, int _vperiodo, string _vpdv, string _vmarca, string _vcategoria)
            {
                ViewBag.veq = _vcanal;
                return View(new LuckyFusionColgate().PresenciaProductoPDV(new Consultar_RepMapsFCGeneral_Request()
                {
                    oParametros = new E_Parametros_MapFC()
                    {
                        Cod_Equipo = _vcanal,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca
                    }
                }));

            }
            public ActionResult PDVsPresenciaProductoColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new LuckyFusionColgate()
                        .PDVsPresenciaProducto(new Consultar_RepMapsFCGeneral_Request()
                        {
                            oParametros = new E_Parametros_MapFC()
                            {
                                Cod_Equipo = _vcanal,
                                Cod_Tipo = _vgrupo,
                                Cod_Ubieo = _vubigeo,
                                cod_pais = "589",
                                Cod_Cadena = _vcadena,
                                Periodo = _vperiodo,
                                Cod_Categoria = _vcategoria,
                                cod_sku = _vsku,
                                Tipo_General = 2
                            }
                        }
                        )),
                    ContentType = "application/json"
                }.Content);
            }
            public JsonResult ExportarPresenciaProductoColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vsku)
            {
                string LocalTemp;
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;
                LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

                _fileServer = String.Format("Reporte_Presencia_Producto_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>
                    #region << Reporte Presencia Producto >>

                    List<E_Export_Presencia_MapFC> oPresencia = new LuckyFusionColgate().Exportar_PresenciaProducto(
                       new Consultar_RepMapsFCGeneral_Request()
                       {
                           oParametros = new E_Parametros_MapFC()
                           {
                               Cod_Equipo = _vcanal,
                               Cod_Tipo = _vgrupo,
                               Cod_Ubieo = _vubigeo,
                               cod_pais = "589",
                               Cod_Cadena = _vcadena,
                               Periodo = _vperiodo,
                               Cod_Categoria = _vcategoria,
                               cod_sku = _vsku,
                               Tipo_General = 1
                           }
                       }
                    );

                    #endregion

                    #endregion

                    #region <<< Reporte Presencia Producto >>>
                    if ((oPresencia != null))
                    {
                        Excel.ExcelWorksheet oWsPresencia = oEx.Workbook.Worksheets.Add("Rep_PresenciaProducto");
                        oWsPresencia.Cells[1, 1].Value = "COD PDV";
                        oWsPresencia.Cells[1, 2].Value = "NOMBRE PDV";
                        oWsPresencia.Cells[1, 3].Value = "RAZ SOCIAL";
                        oWsPresencia.Cells[1, 4].Value = "DIRECCION";
                        oWsPresencia.Cells[1, 5].Value = "CADENA";
                        oWsPresencia.Cells[1, 6].Value = "SECTOR";
                        oWsPresencia.Cells[1, 7].Value = "PAIS";
                        oWsPresencia.Cells[1, 8].Value = "DEPARTAMENTO";
                        oWsPresencia.Cells[1, 9].Value = "PROVINCIA";
                        oWsPresencia.Cells[1, 10].Value = "DISTRITO";
                        oWsPresencia.Cells[1, 11].Value = "CATEGORIA";
                        oWsPresencia.Cells[1, 12].Value = "MARCA";
                        oWsPresencia.Cells[1, 13].Value = "COD_SKU";
                        oWsPresencia.Cells[1, 14].Value = "PRODUCTO";
                        oWsPresencia.Cells[1, 15].Value = "AÑO";
                        oWsPresencia.Cells[1, 16].Value = "MES";
                        oWsPresencia.Cells[1, 17].Value = "SEMANA";
                        oWsPresencia.Cells[1, 18].Value = "PRESENCIA";

                        _fila = 2;
                        foreach (E_Export_Presencia_MapFC oBj in oPresencia)
                        {
                            oWsPresencia.Cells[_fila, 1].Value = oBj.Cod_PDV;
                            oWsPresencia.Cells[_fila, 2].Value = oBj.Nombre_PDV;
                            oWsPresencia.Cells[_fila, 3].Value = oBj.Raz_Social;
                            oWsPresencia.Cells[_fila, 4].Value = oBj.Direccion;
                            oWsPresencia.Cells[_fila, 5].Value = oBj.Cadena;
                            oWsPresencia.Cells[_fila, 6].Value = oBj.Sector;
                            oWsPresencia.Cells[_fila, 7].Value = oBj.Pais;
                            oWsPresencia.Cells[_fila, 8].Value = oBj.Departamento;
                            oWsPresencia.Cells[_fila, 9].Value = oBj.Provincia;
                            oWsPresencia.Cells[_fila, 10].Value = oBj.Distrito;
                            oWsPresencia.Cells[_fila, 11].Value = oBj.Categoria;
                            oWsPresencia.Cells[_fila, 12].Value = oBj.Marca;
                            oWsPresencia.Cells[_fila, 13].Value = oBj.Cod_SKU;
                            oWsPresencia.Cells[_fila, 14].Value = oBj.Producto;
                            oWsPresencia.Cells[_fila, 15].Value = oBj.Anio;
                            oWsPresencia.Cells[_fila, 16].Value = oBj.Mes;
                            oWsPresencia.Cells[_fila, 17].Value = oBj.Semana;
                            oWsPresencia.Cells[_fila, 18].Value = oBj.Presencia;
                            _fila++;
                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsPresencia.SelectedRange[1, 1, 1, 18].AutoFilter = true;
                        oWsPresencia.Row(1).Height = 25;
                        oWsPresencia.Row(1).Style.Font.Bold = true;
                        oWsPresencia.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsPresencia.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsPresencia.Column(1).AutoFit();
                        oWsPresencia.Column(2).AutoFit();
                        oWsPresencia.Column(3).AutoFit();
                        oWsPresencia.Column(4).AutoFit();
                        oWsPresencia.Column(5).AutoFit();
                        oWsPresencia.Column(6).AutoFit();
                        oWsPresencia.Column(7).AutoFit();
                        oWsPresencia.Column(8).AutoFit();
                        oWsPresencia.Column(9).AutoFit();
                        oWsPresencia.Column(10).AutoFit();
                        oWsPresencia.Column(11).AutoFit();
                        oWsPresencia.Column(12).AutoFit();
                        oWsPresencia.Column(13).AutoFit();
                        oWsPresencia.Column(14).AutoFit();
                        oWsPresencia.Column(15).AutoFit();
                        oWsPresencia.Column(16).AutoFit();
                        oWsPresencia.Column(17).AutoFit();
                        oWsPresencia.Column(18).AutoFit();

                        oWsPresencia.View.FreezePanes(2, 1);


                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }

            //SOS
            [HttpPost]
            public ActionResult ShareOfShelfColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv)
            {
                return View(new LuckyFusionColgate().ShareOfShelf(new Consultar_RepMapsFCGeneral_Request()
                {
                    oParametros = new E_Parametros_MapFC()
                    {
                        Cod_Equipo = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv
                    }
                }));
                //return View();
            }

            public ActionResult ShareOfShelfColgatePDV(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca)
            {
                return View(new LuckyFusionColgate().ShareOfShelfPDV(new Consultar_RepMapsFCGeneral_Request()
                {
                    oParametros = new E_Parametros_MapFC()
                    {
                        Cod_Equipo = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca
                    }
                }));
                //return View();
            }

            public ActionResult TrackingFotograficoColgatePDV(int vperiodo, int vcodtipo, string vcodpdv, string vcodcategoria)
            {
                E_Parametros_MapFC oParametros = new E_Parametros_MapFC();
                oParametros.Periodo = vperiodo;
                oParametros.Cod_Tipo = vcodtipo;
                oParametros.Cod_PDV = vcodpdv;
                oParametros.Cod_Categoria = vcodcategoria;

                return View(new LuckyFusionColgate().FotoPDV(
                    new Consultar_RepMapsFCGeneral_Request()
                    {
                        oParametros = oParametros
                    }));
            }

            public JsonResult ExportarShareOfShelfColgate(string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv)
            {
                string LocalTemp;
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;
                LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

                _fileServer = String.Format("Reporte_ShareOfShelf_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>
                    #region << Reporte ShareOfShelf >>

                    //List<AlicorpMinoristaExportaSod> oShareOfDisplay = new AlicorpMinoristaSodCategoria().ExportaSOD(
                    //   new Resquest_AlicorpMinoristaCobertura()
                    //   {
                    //       campania = __a,//"002892382010",//__a,
                    //       grupo = Convert.ToInt32(__b), //1, // 
                    //       ubigeo = __c, // "589", // 
                    //       anio = Convert.ToInt32(__d), //2015, // 
                    //       mes = Convert.ToInt32(__e),// 5, // ,
                    //       periodo = Convert.ToInt32(__f), //30257, // 
                    //       giro = Convert.ToInt32(__g), //0, // 
                    //       pdv = "", // __h
                    //       categoria = 0, // Convert.toInt32(__i),
                    //       segmento = Convert.ToInt32(__j), //17  // 
                    //       tipoGiro = Convert.ToInt32(__k)
                    //   });

                    List<M_ReporteSOS_MapFC> oShareOfShelf = new LuckyFusionColgate().ShareOfShelf(new Consultar_RepMapsFCGeneral_Request()
                        {
                            oParametros = new E_Parametros_MapFC()
                            {
                                Cod_Equipo = _vcanal,
                                Cod_Tipo = Convert.ToInt32(_vgrupo),
                                Cod_Ubieo = _vubigeo,
                                cod_pais = "589",
                                Cod_Cadena = _vcadena,
                                Periodo = Convert.ToInt32(_vperiodo),
                                Cod_Categoria = _vcategoria,
                                Cod_PDV = _vpdv
                            }
                        });

                    #endregion

                    #endregion

                    #region <<< Reporte ShareOfDisplay >>>
                    //public int Cod_Marca { get; set; }
                    //public string Nom_Marca { get; set; }
                    //public string Porcentaje { get; set; }
                    //public string Cod_Categoria { get; set; }
                    //public string Nom_Categoria { get; set; }
                    if ((oShareOfShelf != null))
                    {
                        Excel.ExcelWorksheet oWsShareOfShelf = oEx.Workbook.Worksheets.Add("Rep_Presencia_ShareOfShelf");
                        oWsShareOfShelf.Cells[1, 1].Value = "Cod. Empresa";
                        oWsShareOfShelf.Cells[1, 2].Value = "Empresa";
                        //oWsShareOfShelf.Cells[1, 3].Value = "Cod. Marca";
                        //oWsShareOfShelf.Cells[1, 4].Value = "Marca";
                        oWsShareOfShelf.Cells[1, 3].Value = "Porcentaje";


                        _fila = 2;
                        foreach (M_ReporteSOS_MapFC oBj in oShareOfShelf)
                        {
                            //oWsShareOfShelf.Cells[_fila, 1].Value = oBj.Cod_Categoria;
                            //oWsShareOfShelf.Cells[_fila, 2].Value = oBj.Nom_Categoria;
                            oWsShareOfShelf.Cells[_fila, 1].Value = oBj.Cod_Marca;
                            oWsShareOfShelf.Cells[_fila, 2].Value = oBj.Nom_Marca;
                            oWsShareOfShelf.Cells[_fila, 3].Value = oBj.Porcentaje;
                            _fila++;
                        }

                        //Formato Cabecera
                        oWsShareOfShelf.SelectedRange[1, 1, 1, 3].AutoFilter = true;
                        oWsShareOfShelf.Row(1).Height = 25;
                        oWsShareOfShelf.Row(1).Style.Font.Bold = true;
                        oWsShareOfShelf.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsShareOfShelf.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsShareOfShelf.Column(1).AutoFit();
                        oWsShareOfShelf.Column(2).AutoFit();
                        oWsShareOfShelf.Column(3).AutoFit();
                        //oWsShareOfShelf.Column(4).AutoFit();
                        //oWsShareOfShelf.Column(5).AutoFit();
                        //oWsShareOfShelf.Column(6).AutoFit();
                        //oWsShareOfShelf.Column(7).AutoFit();
                        //oWsShareOfShelf.Column(8).AutoFit();
                        //oWsShareOfShelf.Column(9).AutoFit();
                        //oWsShareOfShelf.Column(10).AutoFit();
                        //oWsShareOfShelf.Column(11).AutoFit();
                        //oWsShareOfShelf.Column(12).AutoFit();
                        oWsShareOfShelf.View.FreezePanes(2, 1);


                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }

            [HttpGet]
            public ActionResult Lista_PDV_Periodo(int _vperiodo, string _vpdv)
            {
                List<E_PuntoVentaMapa> listaPDV = new LuckyFusionColgate().Lista_PDV_Periodo(new Consultar_RepMapsFCGeneral_Request()
                {
                    oParametros = new E_Parametros_MapFC()
                    {
                        Periodo = _vperiodo,
                        Cod_PDV = _vpdv,
                    }
                });
                //return View();
                return Json(new { lista = listaPDV }, JsonRequestBehavior.AllowGet);
            }

        #endregion

        #region Xplora Alicorp AASS
            [CustomAuthorize]
            public ActionResult MapsAlicorp(string _a)
            {
                ViewBag.veq = _a;
                ViewBag.url_img_map = ConfigurationManager.AppSettings["url_foto_maps"]; ;

                //Se registra visita
                Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);

                return View();
            }

            //Store Filtros: UP_NEW_XPLORA_MAPS_FILTROS
            [HttpPost]
            public JsonResult AlicorpAASSFiltrosJson(string __a, string __b, string __c)
            {
                ViewBag.cod_opcion = __a;
                ViewBag.cod_elemento = __c;
                List<M_Combo> list = new New_XplMapsAlicorp_Filtro_Service().Filtro(new Resquest_New_XPL_MapsAlicorp()
                        {
                            opcion = Convert.ToInt32(__a),
                            parametros = __b
                        });
                return Json(list, JsonRequestBehavior.AllowGet);
            }

            public ActionResult DetallePDV_Alicorp(string vcodpdv, string vperiodo)
            {
                E_PuntoVentaDatosMapa oLs = new LuckyAlicorp().DetallePDV(
                    new Obtener_DatosPuntosVentaMapa_Request()
                    {
                        codPtoVenta = vcodpdv,
                        reportsPlanning = vperiodo
                    });

                return Json(new { Jdpdv = oLs });
            }

            public ActionResult FotoPDVAlicorp(int vperiodo, int vcodtipo, string vcodpdv)
            {
                E_Parametros_MapAlicorp oParametros = new E_Parametros_MapAlicorp();
                oParametros.Periodo = vperiodo;
                oParametros.Cod_Tipo = vcodtipo;
                oParametros.Cod_PDV = vcodpdv;

                List<E_FotoFC> oLs = new LuckyAlicorp().FotoPDV(
                    new Consultar_RepMapsAlicorpGeneral_Request()
                    {
                        oParametros = oParametros
                    });

                return Json(new { Jfotopdv = oLs });

            }
        #region Reporte Cobertura
            public ActionResult CoberturaAlicorpAASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vpdv)
            {
                return View("~/Views/Maps/Alicorp/CoberturaAlicorpAASS.cshtml", new LuckyAlicorp().Cobertura(new Consultar_RepMapsAlicorpGeneral_Request()
                {
                    oParametros = new E_Parametros_MapAlicorp()
                    {
                        Cod_Equipo = _vequipo,
                        Cod_Canal = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_PDV = _vpdv
                    }
                })
                );
            }
            public ActionResult PDVsCoberturaAlicorpAASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, int _vopcobertura, string _vpdv)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new LuckyAlicorp()
                        .PDVsCobertura(new Consultar_RepMapsAlicorpGeneral_Request()
                        {
                            oParametros = new E_Parametros_MapAlicorp()
                            {
                                Cod_Equipo = _vequipo,
                                Cod_Canal = _vcanal,
                                Cod_Tipo = _vgrupo,
                                Cod_Ubieo = _vubigeo,
                                cod_pais = "589",
                                Cod_Cadena = _vcadena,
                                Periodo = _vperiodo,
                                Tipo_General = _vopcobertura,
                                Cod_PDV = _vpdv
                            }
                        }
                        )),
                    ContentType = "application/json"
                }.Content);
            }
            public JsonResult ExportarCoberturaAASS(string _vequipo, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, int _vopcobertura, string _vpdv)
            {
                string LocalTemp;
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;
                LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

                _fileServer = String.Format("Reporte_Cobertura_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
               //_filePath = System.IO.Path.Combine(ruta + "/Temp", _fileServer);

                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);


                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>
                    #region << Reporte Cobertura >>

                    List<E_Exportar_Cobertura_MapAlicorp> oCobertura = new LuckyAlicorp().Exportar_cobertura(
                       new Consultar_RepMapsAlicorpGeneral_Request()
                       {
                           oParametros = new E_Parametros_MapAlicorp()
                           {
                               Cod_Equipo = _vequipo,
                               Cod_Tipo = _vgrupo,
                               Cod_Ubieo = _vubigeo,
                               cod_pais = "589",
                               Cod_Cadena = _vcadena,
                               Periodo = _vperiodo,
                               Tipo_General = _vopcobertura,
                               Cod_PDV = _vpdv
                           }
                       }
                    );

                    #endregion

                    #endregion

                    #region <<< Reporte Cobertura >>>
                    if ((oCobertura != null))
                    {
                        Excel.ExcelWorksheet oWsCobertura = oEx.Workbook.Worksheets.Add("Reporte_Cobertura");
                        oWsCobertura.Cells[1, 1].Value = "COD PDV";
                        oWsCobertura.Cells[1, 2].Value = "NOMBRE PDV";
                        oWsCobertura.Cells[1, 3].Value = "RAZ SOCIAL";
                        oWsCobertura.Cells[1, 4].Value = "DIRECCION";
                        oWsCobertura.Cells[1, 5].Value = "CADENA";
                        oWsCobertura.Cells[1, 6].Value = "PAIS";
                        oWsCobertura.Cells[1, 7].Value = "DEPARTAMENTO";
                        oWsCobertura.Cells[1, 8].Value = "PROVINCIA";
                        oWsCobertura.Cells[1, 9].Value = "DISTRITO";
                        oWsCobertura.Cells[1, 10].Value = "AÑO";
                        oWsCobertura.Cells[1, 11].Value = "MES";
                        oWsCobertura.Cells[1, 12].Value = "SEMANA";
                        oWsCobertura.Cells[1, 13].Value = "CLUSTER";

                        _fila = 2;
                        foreach (E_Exportar_Cobertura_MapAlicorp oBj in oCobertura)
                        {
                            oWsCobertura.Cells[_fila, 1].Value = oBj.Cod_PDV;
                            oWsCobertura.Cells[_fila, 2].Value = oBj.Nombre_PDV;
                            oWsCobertura.Cells[_fila, 3].Value = oBj.Raz_Social;
                            oWsCobertura.Cells[_fila, 4].Value = oBj.Direccion;
                            oWsCobertura.Cells[_fila, 5].Value = oBj.Cadena;
                            oWsCobertura.Cells[_fila, 6].Value = oBj.Pais;
                            oWsCobertura.Cells[_fila, 7].Value = oBj.Departamento;
                            oWsCobertura.Cells[_fila, 8].Value = oBj.Provincia;
                            oWsCobertura.Cells[_fila, 9].Value = oBj.Distrito;
                            oWsCobertura.Cells[_fila, 10].Value = oBj.Anio;
                            oWsCobertura.Cells[_fila, 11].Value = oBj.Mes;
                            oWsCobertura.Cells[_fila, 12].Value = oBj.Semana;
                            oWsCobertura.Cells[_fila, 13].Value = oBj.Cluster;
                            _fila++;
                        }

                        //Formato Cabecera
                        //Formato Cabecera
                        oWsCobertura.SelectedRange[1, 1, 1, 13].AutoFilter = true;
                        oWsCobertura.Row(1).Height = 25;
                        oWsCobertura.Row(1).Style.Font.Bold = true;
                        oWsCobertura.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsCobertura.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsCobertura.Column(1).AutoFit();
                        oWsCobertura.Column(2).AutoFit();
                        oWsCobertura.Column(3).AutoFit();
                        oWsCobertura.Column(4).AutoFit();
                        oWsCobertura.Column(5).AutoFit();
                        oWsCobertura.Column(6).AutoFit();
                        oWsCobertura.Column(7).AutoFit();
                        oWsCobertura.Column(8).AutoFit();
                        oWsCobertura.Column(9).AutoFit();
                        oWsCobertura.Column(10).AutoFit();
                        oWsCobertura.Column(11).AutoFit();
                        oWsCobertura.Column(12).AutoFit();
                        oWsCobertura.Column(13).AutoFit();

                        oWsCobertura.View.FreezePanes(2, 1);


                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }
        #endregion
        #region Reporte OSA
            public ActionResult OSA_AASS(string _vequipo,string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                return View("~/Views/Maps/Alicorp/OSA_AASS.cshtml", new LuckyAlicorp().OSA(new Consultar_RepMapsAlicorpGeneral_Request()
                {
                    oParametros = new E_Parametros_MapAlicorp()
                    {
                        Cod_Equipo = _vequipo,
                        Cod_Canal = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca,
                        cod_cluster = _vcluster,
                        cod_causal = _vcausal,
                        cod_tiporesultado = _vresultado
                    }
                }));
            }
            public ActionResult OSAPDV_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                return View("~/Views/Maps/Alicorp/OSAPDV_AASS.cshtml", new LuckyAlicorp().OSA(new Consultar_RepMapsAlicorpGeneral_Request()
                {
                    oParametros = new E_Parametros_MapAlicorp()
                    {
                        Cod_Equipo = _vequipo,
                        Cod_Canal = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca,
                        cod_cluster = _vcluster,
                        cod_causal = _vcausal,
                        cod_tiporesultado = _vresultado
                    }
                }));
            }
            public ActionResult ListarPDVs_OSA_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado, string _sku)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new LuckyAlicorp()
                        .PDVsOSA(new Consultar_RepMapsAlicorpGeneral_Request()
                        {
                            oParametros = new E_Parametros_MapAlicorp()
                            {
                                Cod_Equipo = _vequipo,
                                Cod_Canal = _vcanal,
                                Cod_Tipo = _vgrupo,
                                Cod_Ubieo = _vubigeo,
                                cod_pais = "589",
                                Cod_Cadena = _vcadena,
                                Periodo = _vperiodo,
                                Cod_Categoria = _vcategoria,
                                Cod_PDV = _vpdv,
                                Cod_Marca = _vmarca,
                                cod_cluster = _vcluster,
                                cod_causal = _vcausal,
                                cod_tiporesultado = _vresultado,
                                cod_sku = _sku,
                                opcion = 2
                            }
                        }
                        )),
                    ContentType = "application/json"
                }.Content);
            }
            public ActionResult Exportar_OSA_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado, string _sku)
            {
                string LocalTemp;
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;
                LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

                _fileServer = String.Format("Reporte_OSA_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
               // _filePath = System.IO.Path.Combine(ruta + "/Temp", _fileServer);

                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

               
                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>
                    #region << Reporte OSA >>

                    List<E_Export_OSA_MapAli> oOSA = new LuckyAlicorp().Exportar_OSA(
                       new Consultar_RepMapsAlicorpGeneral_Request()
                       {
                           oParametros = new E_Parametros_MapAlicorp
                           {
                               Cod_Equipo = _vequipo,
                               Cod_Canal = _vcanal,
                               Cod_Tipo = _vgrupo,
                               Cod_Ubieo = _vubigeo,
                               cod_pais = "589",
                               Cod_Cadena = _vcadena,
                               Periodo = _vperiodo,
                               Cod_Categoria = _vcategoria,
                               Cod_PDV = _vpdv,
                               Cod_Marca = _vmarca,
                               cod_cluster = _vcluster,
                               cod_causal = _vcausal,
                               cod_tiporesultado = _vresultado,
                               cod_sku = _sku,
                               opcion = 1
                           }
                       }
                    );

                    #endregion

                    #endregion

                    #region <<< Reporte Presencia Visibilidad >>>
                    if ((oOSA != null))
                    {
                        Excel.ExcelWorksheet oWsOSA = oEx.Workbook.Worksheets.Add("Rep_OSA");
                        oWsOSA.Cells[1, 1].Value = "COD PDV";
                        oWsOSA.Cells[1, 2].Value = "NOMBRE PDV";
                        oWsOSA.Cells[1, 3].Value = "DIRECCION";
                        oWsOSA.Cells[1, 4].Value = "DEPARTAMENTO";
                        oWsOSA.Cells[1, 5].Value = "PROVINCIA";
                        oWsOSA.Cells[1, 6].Value = "DISTRITO";
                        oWsOSA.Cells[1, 7].Value = "SECTOR";
                        oWsOSA.Cells[1, 8].Value = "NOMBRE PRODUCTO";
                        oWsOSA.Cells[1, 9].Value = "NOMBRE CATEGORIA";
                        oWsOSA.Cells[1, 10].Value = "NOMBRE MARCA";
                        oWsOSA.Cells[1, 11].Value = "NRO SEMANA";
                        oWsOSA.Cells[1, 12].Value = "CAUSALIDAD";
                        oWsOSA.Cells[1, 13].Value = "TIPO RESULTADO";
                        oWsOSA.Cells[1, 14].Value = "CLUSTER";


                        _fila = 2;
                        foreach (E_Export_OSA_MapAli oBj in oOSA)
                        {
                            oWsOSA.Cells[_fila, 1].Value = oBj.Cod_PDV;
                            oWsOSA.Cells[_fila, 2].Value = oBj.Nombre_PDV;
                            oWsOSA.Cells[_fila, 3].Value = oBj.Direccion;
                            oWsOSA.Cells[_fila, 4].Value = oBj.Departamento;
                            oWsOSA.Cells[_fila, 5].Value = oBj.Provincia;
                            oWsOSA.Cells[_fila, 6].Value = oBj.Distrito;
                            oWsOSA.Cells[_fila, 7].Value = oBj.Sector;
                            oWsOSA.Cells[_fila, 8].Value = oBj.Producto;
                            oWsOSA.Cells[_fila, 9].Value = oBj.Categoria;
                            oWsOSA.Cells[_fila, 10].Value = oBj.Marca;
                            oWsOSA.Cells[_fila, 11].Value = oBj.Semana;
                            oWsOSA.Cells[_fila, 12].Value = oBj.causal;
                            oWsOSA.Cells[_fila, 13].Value = oBj.tipo_resultado;
                            oWsOSA.Cells[_fila, 14].Value = oBj.Cluster;

                            _fila++;
                        }
                        //Formato Cabecera
                        oWsOSA.SelectedRange[1, 1, 1, 14].AutoFilter = true;
                        oWsOSA.Row(1).Height = 25;
                        oWsOSA.Row(1).Style.Font.Bold = true;
                        oWsOSA.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsOSA.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsOSA.Column(1).AutoFit();
                        oWsOSA.Column(2).AutoFit();
                        oWsOSA.Column(3).AutoFit();
                        oWsOSA.Column(4).AutoFit();
                        oWsOSA.Column(5).AutoFit();
                        oWsOSA.Column(6).AutoFit();
                        oWsOSA.Column(7).AutoFit();
                        oWsOSA.Column(8).AutoFit();
                        oWsOSA.Column(9).AutoFit();
                        oWsOSA.Column(10).AutoFit();
                        oWsOSA.Column(11).AutoFit();
                        oWsOSA.Column(12).AutoFit();
                        oWsOSA.Column(13).AutoFit();
                        oWsOSA.Column(14).AutoFit();

                        oWsOSA.View.FreezePanes(2, 1);


                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }
        #endregion
        #region Reporte Tracking Precio
            public ActionResult TrackingPrecioAli_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                return View("~/Views/Maps/Alicorp/TrackingPrecioAli_AASS.cshtml", new LuckyAlicorp().TrackingPrecio(new Consultar_RepMapsAlicorpGeneral_Request()
                {
                    oParametros = new E_Parametros_MapAlicorp()
                    {
                        Cod_Equipo = _vequipo,
                        Cod_Canal = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca,
                        cod_cluster = _vcluster,
                        cod_causal = _vcausal,
                        cod_tiporesultado = _vresultado
                    }
                }));
            }
            public ActionResult TrackingPrecioPDV_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster)
            {
                ViewBag.veq = _vcanal;
                return View("~/Views/Maps/Alicorp/TrackingPrecioPDV_AASS.cshtml", new LuckyAlicorp().TrackingPrecio(new Consultar_RepMapsAlicorpGeneral_Request()
                //return View(new LuckyAlicorp().TrackingPrecio(new Consultar_RepMapsAlicorpGeneral_Request()
                {
                    oParametros = new E_Parametros_MapAlicorp()
                    {
                        Cod_Equipo = _vequipo,
                        Cod_Canal = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca,
                        cod_cluster = _vcluster,
                        cod_causal = "0",
                        cod_tiporesultado = "0"
                    }
                }));
            }
            public ActionResult PDVsTrackingPrecio_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new LuckyAlicorp()
                        .PDVsTrackingPrecio(new Consultar_RepMapsAlicorpGeneral_Request()
                        {
                            oParametros = new E_Parametros_MapAlicorp()
                            {
                                Cod_Equipo = _vequipo,
                                Cod_Canal = _vcanal,
                                Cod_Tipo = _vgrupo,
                                Cod_Ubieo = _vubigeo,
                                cod_pais = "589",
                                Cod_Cadena = _vcadena,
                                Periodo = _vperiodo,
                                Cod_Categoria = _vcategoria,
                                Cod_PDV = _vpdv,
                                Cod_Marca = _vmarca,
                                cod_cluster = _vcluster,
                                cod_causal = _vcausal,
                                cod_tiporesultado = _vresultado,
                                opcion = 2
                            }
                        }
                        )),
                    ContentType = "application/json"
                }.Content);
            }
            
        public JsonResult ExportarTrackingPrecio_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                string LocalTemp;
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;
                LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
                
                _fileServer = String.Format("Reporte_Tracking_de_Precio_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
             //   _filePath = System.IO.Path.Combine(ruta + "/Temp", _fileServer);

                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);
                
                
                FileInfo _fileNew = new FileInfo(_filePath);
                
                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>
                    #region << Reporte Tracking de Precio >>
                        // E_Excel_Tracking_Precio_MapAli
                    List<E_Excel_Tracking_Precio_MapAli> oTracking = new LuckyAlicorp().Exportar_Tracking_precio(
                       new Consultar_RepMapsAlicorpGeneral_Request()
                       {
                           oParametros = new E_Parametros_MapAlicorp
                           {
                               Cod_Equipo = _vequipo,
                                Cod_Canal = _vcanal,
                                Cod_Tipo = _vgrupo,
                                Cod_Ubieo = _vubigeo,
                                cod_pais = "589",
                                Cod_Cadena = _vcadena,
                                Periodo = _vperiodo,
                                Cod_Categoria = _vcategoria,
                                Cod_PDV = _vpdv,
                                Cod_Marca = _vmarca,
                                cod_cluster = _vcluster,
                                cod_causal = _vcausal,
                                cod_tiporesultado = _vresultado,
                                opcion = 1
                           }
                       }
                    );

                    #endregion

                    #endregion

                    #region <<< Reporte Presencia Visibilidad >>>
                    if ((oTracking != null))
                    {
                        Excel.ExcelWorksheet oWsTracking = oEx.Workbook.Worksheets.Add("Rep_Tracking_Precio");
                        oWsTracking.Cells[1, 1].Value = "COD PDV";
                        oWsTracking.Cells[1, 2].Value = "NOMBRE PDV";
                        oWsTracking.Cells[1, 3].Value = "DIRECCION";
                        oWsTracking.Cells[1, 4].Value = "DEPARTAMENTO";
                        oWsTracking.Cells[1, 5].Value = "PROVINCIA";
                        oWsTracking.Cells[1, 6].Value = "DISTRITO";
                        oWsTracking.Cells[1, 7].Value = "SECTOR";
                        oWsTracking.Cells[1, 8].Value = "NOMBRE PRODUCTO";
                        oWsTracking.Cells[1, 9].Value = "NOMBRE CATEGORIA";
                        oWsTracking.Cells[1, 10].Value = "NOMBRE MARCA";
                        oWsTracking.Cells[1, 11].Value = "PRECIO";


                        _fila = 2;
                        foreach (E_Excel_Tracking_Precio_MapAli oBj in oTracking)
                        {
                            oWsTracking.Cells[_fila, 1].Value = oBj.Cod_PDV;
                            oWsTracking.Cells[_fila, 2].Value = oBj.Nom_PDV;
                            oWsTracking.Cells[_fila, 3].Value = oBj.Direccion;
                            oWsTracking.Cells[_fila, 4].Value = oBj.Nom_Departamento;
                            oWsTracking.Cells[_fila, 5].Value = oBj.Nom_Provincia;
                            oWsTracking.Cells[_fila, 6].Value = oBj.Nom_Distrito;
                            oWsTracking.Cells[_fila, 7].Value = oBj.Nom_Sector;
                            oWsTracking.Cells[_fila, 8].Value = oBj.Nom_Producto;
                            oWsTracking.Cells[_fila, 9].Value = oBj.Nom_Categoria;
                            oWsTracking.Cells[_fila, 10].Value = oBj.Nom_Marca;
                            oWsTracking.Cells[_fila, 11].Value = oBj.Precio_PDV;

                            _fila++;
                        }
                        //Formato Cabecera
                        oWsTracking.SelectedRange[1, 1, 1, 11].AutoFilter = true;
                        oWsTracking.Row(1).Height = 25;
                        oWsTracking.Row(1).Style.Font.Bold = true;
                        oWsTracking.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsTracking.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsTracking.Column(1).AutoFit();
                        oWsTracking.Column(2).AutoFit();
                        oWsTracking.Column(3).AutoFit();
                        oWsTracking.Column(4).AutoFit();
                        oWsTracking.Column(5).AutoFit();
                        oWsTracking.Column(6).AutoFit();
                        oWsTracking.Column(7).AutoFit();
                        oWsTracking.Column(8).AutoFit();
                        oWsTracking.Column(9).AutoFit();
                        oWsTracking.Column(10).AutoFit();
                        oWsTracking.Column(11).AutoFit();

                        oWsTracking.View.FreezePanes(2, 1);


                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }
        #endregion
        #region Reporte SOD
            public ActionResult SOD_Ali_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                return View("~/Views/Maps/Alicorp/SOD_Ali_AASS.cshtml", new LuckyAlicorp().SOD(new Consultar_RepMapsAlicorpGeneral_Request()
                {
                    oParametros = new E_Parametros_MapAlicorp()
                    {
                        Cod_Equipo = _vequipo,
                        Cod_Canal = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca,
                        cod_cluster = _vcluster,
                        cod_causal = _vcausal,
                        cod_tiporesultado = _vresultado
                    }
                }));
            }
            public ActionResult SODPDV_Ali_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                return View("~/Views/Maps/Alicorp/SODPDV_Ali_AASS.cshtml", new LuckyAlicorp().SOD(new Consultar_RepMapsAlicorpGeneral_Request()
                {
                    oParametros = new E_Parametros_MapAlicorp()
                    {
                        Cod_Equipo = _vequipo,
                        Cod_Canal = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca,
                        cod_cluster = _vcluster,
                        cod_causal = _vcausal,
                        cod_tiporesultado = _vresultado
                    }
                }));
            }
        #endregion
        #region Quiebre
            [HttpPost]
            public ActionResult Quiebre_Ali_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                return View("~/Views/Maps/Alicorp/Quiebre_Ali_AASS.cshtml", new LuckyAlicorp().Quiebre(new Consultar_RepMapsAlicorpGeneral_Request()
                {
                    oParametros = new E_Parametros_MapAlicorp()
                    {
                        Cod_Equipo = _vequipo,
                        Cod_Canal = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca,
                        cod_cluster = _vcluster,
                        cod_causal = _vcausal,
                        cod_tiporesultado = _vresultado
                    }
                }));
            }
            public ActionResult QuiebrePDV_Ali_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                return View("~/Views/Maps/Alicorp/QuiebrePDV_Ali_AASS.cshtml", new LuckyAlicorp().Quiebre(new Consultar_RepMapsAlicorpGeneral_Request()
                {
                    oParametros = new E_Parametros_MapAlicorp()
                    {
                        Cod_Equipo = _vequipo,                         
                        Cod_Canal = _vcanal,
                        Cod_Tipo = _vgrupo,
                        Cod_Ubieo = _vubigeo,
                        cod_pais = "589",
                        Cod_Cadena = _vcadena,
                        Periodo = _vperiodo,
                        Cod_Categoria = _vcategoria,
                        Cod_PDV = _vpdv,
                        Cod_Marca = _vmarca,
                        cod_cluster = _vcluster,
                        cod_causal = _vcausal,
                        cod_tiporesultado = _vresultado
                    }
                }));
            }
            public JsonResult ExportarQuiebre_Ali_AASS(string _vequipo, string _vcanal, int _vgrupo, string _vubigeo, int _vcadena, int _vperiodo, string _vcategoria, string _vpdv, string _vmarca, string _vcluster, string _vcausal, string _vresultado)
            {
                string LocalTemp;
                string _fileServer = "";
                string _filePath = "";

                int _fila = 0;
                LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

                _fileServer = String.Format("Reporte_Quiebre_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
               // _filePath = System.IO.Path.Combine(ruta + "/Temp", _fileServer);


                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);


                FileInfo _fileNew = new FileInfo(_filePath);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
                {
                    #region <<< Instancias >>>
                    #region << Reporte ShareOfShelf >>

                    List<M_Quiebre_MapAli_AASS> oQuiebre = new LuckyAlicorp().Quiebre(new Consultar_RepMapsAlicorpGeneral_Request()
                    {
                        oParametros = new E_Parametros_MapAlicorp()
                        {
                            Cod_Equipo = _vequipo,
                            Cod_Canal = _vcanal,
                            Cod_Tipo = _vgrupo,
                            Cod_Ubieo = _vubigeo,
                            cod_pais = "589",
                            Cod_Cadena = _vcadena,
                            Periodo = _vperiodo,
                            Cod_Categoria = _vcategoria,
                            Cod_PDV = _vpdv,
                            Cod_Marca = _vmarca,
                            cod_cluster = _vcluster,
                            cod_causal = _vcausal,
                            cod_tiporesultado = _vresultado
                        }
                    });

                    #endregion

                    #endregion

                    #region <<< Reporte Quiebre >>>
                    if ((oQuiebre != null))
                    {
                        Excel.ExcelWorksheet oWsQuiebre = oEx.Workbook.Worksheets.Add("Rep_Quiebre");
                        oWsQuiebre.Cells[1, 1].Value = "Cod. Categoria";
                        oWsQuiebre.Cells[1, 2].Value = "Categoria";
                        oWsQuiebre.Cells[1, 3].Value = "Porcentaje";


                        _fila = 2;
                        foreach (M_Quiebre_MapAli_AASS oBj in oQuiebre)
                        {
                            oWsQuiebre.Cells[_fila, 1].Value = oBj.Cod_Categoria;
                            oWsQuiebre.Cells[_fila, 2].Value = oBj.Nom_Categoria;
                            oWsQuiebre.Cells[_fila, 3].Value = oBj.Porcentaje;
                            _fila++;
                        }

                        //Formato Cabecera
                        oWsQuiebre.SelectedRange[1, 1, 1, 3].AutoFilter = true;
                        oWsQuiebre.Row(1).Height = 25;
                        oWsQuiebre.Row(1).Style.Font.Bold = true;
                        oWsQuiebre.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWsQuiebre.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        //Columnas auto ajustadas
                        oWsQuiebre.Column(1).AutoFit();
                        oWsQuiebre.Column(2).AutoFit();
                        oWsQuiebre.Column(3).AutoFit();
                        oWsQuiebre.View.FreezePanes(2, 1);


                        oEx.Save();
                    }
                    else
                    {
                        _fileServer = "0";
                    }
                    #endregion

                }
                return Json(new { Archivo = _fileServer });
            }
        #endregion
        #endregion
    }
}