﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Unacem;

using Word = Microsoft.Office.Interop.Word;
using Excel = OfficeOpenXml;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Style = OfficeOpenXml.Style;

namespace Lucky.Xplora.Controllers
{
    public class FachadaController : Controller
    {
        string Local = Convert.ToString(ConfigurationManager.AppSettings["Local"]);
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        string LocalFachada = Convert.ToString(ConfigurationManager.AppSettings["LocalFachada"]);
        string LocalFormat = Convert.ToString(ConfigurationManager.AppSettings["LocalFormat"]);

        string Xplora = Convert.ToString(ConfigurationManager.AppSettings["Xplora"]);
        string XploraTemp = Convert.ToString(ConfigurationManager.AppSettings["XploraTemp"]);
        string XploraFachada = Convert.ToString(ConfigurationManager.AppSettings["XploraFachada"]);

        #region WorkFlow Unacem
        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2017-03-20
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WorkFlow()
        {
            Persona oPersona = ((Persona)Session["Session_Login"]);
            E_WF_Grupo_Per oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = oPersona.Person_id });

            ViewBag.cod_grupo = oResper.Cod_Grupo;
            ViewBag.cod_persona = oPersona.Person_id;
            ViewBag.cod_equipo = Request.QueryString["_a"];
            ViewBag.nombre_grupo = oResper.Grupo;
            ViewBag.nombre_persona = oPersona.Person_Firtsname + " " + oPersona.Person_LastName + " " + oPersona.Person_Surname;
            ViewBag.Xplora = Xplora;
            ViewBag.XploraTemp = XploraTemp;
            ViewBag.XploraFachada = XploraFachada;

            TempData["objlisProvider"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 4, parametros = "1560" });
            TempData["objlisTipoTramite"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 6, parametros = "" });
            TempData["objlisTiempoPermiso"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 7, parametros = "" });
            TempData["objlisZonaFiltro"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 1, parametros = "1560" });
            TempData["objlisAnioFiltro"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 8, parametros = "9020111072013" });
            TempData["objlisMesFiltro"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 9, parametros = "" });
            TempData["objlisDistribuidoraFiltro"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 2, parametros = "9020111072013" });
            TempData["objlisUsuarioFiltro"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 3, parametros = "9020111072013" });
            TempData["objlisEstadoSolicitudFiltro"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 10, parametros = "" });
            TempData["objlisMarca"] = new WorkFlow().Filtro(new Resquest_New_XPL_Una_WF { opcion = 5, parametros = "1560" });

            return View();
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 2017-03-20
        /// </summary>
        /// <param name="o"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult WorkFlow(int cod_zona, string fecha_solicitud, string fecha_foto, string distribuidora, int cod_persona, string fecha_presupuesto, string id_planning, int estado_solicitud, int cod_marca)
        {
            #region Fachada
            Persona oPersona = ((Persona)Session["Session_Login"]);
            E_WF_Grupo_Per oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = oPersona.Person_id });

            return new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Consulta_Solicitud(new Request_WF_General() { Cod_Grupo = oResper.Cod_Grupo, cod_zona = cod_zona, fecha_solicitud = fecha_solicitud, fecha_foto = fecha_foto, distribuidora = distribuidora, Cod_Persona = cod_persona, fecha_presupuesto = fecha_presupuesto, cod_equipo = id_planning, Estado = estado_solicitud, Cod_Marca = cod_marca })),
                ContentType = "application/json"
            };
            #endregion
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 30/03/2017
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetResumenPresuGrilla()
        {

            List<E_Resumen_Presupuesto> oLsWf = new WorkFlow().Consulta_Resumen_Presupuesto(
                new Request_WF_General()
                {
                    cod_equipo = Convert.ToString(Session["Session_Campania"])
                });

            return Json(new { Archivo = oLsWf });
        }

        [HttpPost]
        public ActionResult AddSolicitud(int vids, string vpdv, string vruc, string vcodigo, string vdireccion, string vdistrito, string vprovincia, string vdepartamento, string vcontacto, string vnumtelef, string vnumcel,
            string vdistribuidora, string vfoto, string vven_prom_soles, string vven_prom_ton, string vlinea_credito, string vdeu_actual, string vop_grabado, int vop_opera, int p1_codperson, string p1_codTipo, int p1_codZona,
            string p1_comentarios, int vmarca, int v_tipo_gral_solicitud, string v_referencia, string v_cod_equipo)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Agregar_solicitud(
                    new Request_FW_add_solicitud()
                    {
                        cod_equipo = v_cod_equipo,
                        ids = vids,
                        pdv = vpdv,
                        ruc = vruc,
                        codigo = vcodigo,
                        direccion = vdireccion,
                        distrito = vdistrito,
                        provincia = vprovincia,
                        departamento = vdepartamento,
                        contacto = vcontacto,
                        numtelef = vnumtelef,
                        numcel = vnumcel,
                        distribuidora = vdistribuidora,
                        foto = vfoto,
                        ven_prom_soles = vven_prom_soles,
                        ven_prom_ton = vven_prom_ton,
                        linea_credito = vlinea_credito,
                        deu_actual = vdeu_actual,
                        op_grabado = vop_grabado,
                        op_opera = vop_opera,
                        p1_codperson = p1_codperson,
                        p1_codTipo = p1_codTipo,
                        p1_codZona = p1_codZona,
                        p1_comentarios = p1_comentarios,
                        marca = vmarca,
                        tipo_gralSolicitud = v_tipo_gral_solicitud,
                        referencia = v_referencia
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult AproSolicitudp2(int vnumero, int vestado, string vcomentario)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().P2_Aprobacion_Solicitud_Trade(
                    new Request_WF_General()
                    {
                        Num_Solicitud = vnumero,
                        Estado = vestado,
                        Comen_Apro = vcomentario
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Autocomplete(int opcion, string term, int tipo)
        {
            Request_Autocomplete_Solicitud oParametro = new Request_Autocomplete_Solicitud() { Opcion = opcion, Parametro = term, tipo = tipo };
            List<Autocomp_Nueva_Solicitud> lObj = new Autocomp_Nueva_Solicitud().ListaData_Nueva_solicitud(oParametro);


            return Json(lObj, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult Eliminar_SolicitudVentas(int __a, int __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Remove_SolicitudVentas(
                    new Request_WF_General() { Num_Solicitud = __a, Cod_Op = __b }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_Paso3_Lucky(int __a, int __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso3_lucky(
                    new Request_WF_General() { Cod_Op = __a, Provider_code = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_Paso6_lucky(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso6_lucky(
                    new Request_WF_General() { Cod_Op = __a, comen_expediente = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }

        /// <summary>
        /// Autor: wlopez
        /// Fecha: 14/04/2017
        /// Descripcion: Permite Actualizar Fecha de Entrega en la malla de Fotomontaje
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult WF_Una_FechaEntrega_Fotomontaje(int __a, string __b)
        {
            E_Nw_DetailGanttRegistro StatusRegistro = new E_Nw_DetailGanttRegistro();
            StatusRegistro = new WorkFlow().Actualizar_FechaEntrega_Foto(
                   new Request_WF_DetailGantt()
                   {
                       Cod_Reg_Tabla = __a,
                       fecha_entrega = __b
                   });
            return Json(StatusRegistro, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult WF_Una_Fechaimplementacion(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Actualizar_FechaImplementacion(
                    new Request_WF_General() { Cod_Op = __a, fecha_implementacion = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Consultar_Fotomontaje(int __a)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Consul_FotoMontaje(
                    new Request_WF_General() { Id_Reg = __a }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Carga_Img_FotoMon(HttpPostedFileBase __f, int __a, int __b)
        {
            string _archivo = "";
            string _fileLocal = "";
            string _doc_url = "";

            if (__f != null)
            {
                _fileLocal = Path.GetFileName(__f.FileName);
                _archivo = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                string vtipoarchi = _fileLocal.Substring(_fileLocal.LastIndexOf("."), 4);

                switch (vtipoarchi)
                {
                    case ".jpg":
                        __f.SaveAs(Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".jpg"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".jpg";
                        break;
                    case ".png":
                        __f.SaveAs(Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".png"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".png";
                        break;
                }
            }

            WorkFlow objService = new WorkFlow();
            E_WF_Fotomontaje objresponse = objService.AddUpt_Fotomontaje(new Request_WF_General
            {
                Id_Reg = __a,
                img = _doc_url,
                orden = __b,
                Cod_Op = 1
            });
            var headerAcceptVariable1 = this.Request.Headers["ACCEPT"] as string;

            if (headerAcceptVariable1 != null && headerAcceptVariable1.Contains("application/json"))
            {
                return Json(_doc_url);
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var response = serializer.Serialize(_doc_url);
                return Content(response);
            }
        }

        [HttpPost]
        public ActionResult Eliminar_Img_FotoMon(int __a, int __b, string __c)
        {
            WorkFlow objService = new WorkFlow();
            E_WF_Fotomontaje objresponse = objService.AddUpt_Fotomontaje(new Request_WF_General { Id_Reg = __a, img = "", orden = __b, Cod_Op = 2 });

            string sms = "0";
            if (objresponse.Cod_Reg_Table == 0)
            {
                sms = "1";
                if (System.IO.File.Exists(Path.Combine(LocalTemp, __c)))
                {
                    try
                    {
                        System.IO.File.Delete(Path.Combine(LocalTemp, __c));
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            return Json(sms);
        }

        [HttpPost]
        public JsonResult Generar_Documento(string __a, string __b, string __c, string __d, string __e, int __f, string __g)
        {
            string vdocumento = Word_Generar();

            if (__b == null || __b == "") { __b = "/Fachada/Img/blanco.jpg"; }
            if (__c == null || __c == "") { __c = "/Fachada/Img/blanco.jpg"; }
            if (__d == null || __d == "") { __d = "/Fachada/Img/blanco.jpg"; }
            if (__e == null || __e == "") { __e = "/Fachada/Img/blanco.jpg"; }


            string vresp = Word_Insertar_Img(vdocumento, __b, __c, __d, __e, __a);
            if (vdocumento == null || vdocumento == "" || vdocumento == " ") { vdocumento = "0"; }
            else
            {
                vdocumento = vresp;
                WorkFlow objService = new WorkFlow();
                string objresponse = objService.AddFotomont_PrePresupuesto(new Request_WF_General { Id_Reg = __f, Comen_Apro = vdocumento, Cod_Op = 1, Coment_Fotomontaje = __g });
            }
            return Json(vdocumento, JsonRequestBehavior.AllowGet);
        }

        private string Word_Insertar_Img(string nomdoc, string __a, string __b, string __c, string __d, string __e)
        {
            // __e : nombre de punto de venta
            object objMiss = System.Reflection.Missing.Value;
            Word.Application objWord = new Word.Application();

            objWord.Visible = false;
            objWord.DisplayAlerts = Word.WdAlertLevel.wdAlertsNone;

            try
            {
                string ruta = Path.Combine(LocalFachada, "Word/" + nomdoc);
                //string ruta2 = HttpContext.Server.MapPath("~/Temp/Fachada/Img/Img_13012016_030632.jpg");
                //string rtimg1 = HttpContext.Server.MapPath("~" + __a);
                //string rtimg2 = HttpContext.Server.MapPath("~" + __b);
                //string rtimg3 = HttpContext.Server.MapPath("~" + __c);
                //string rtimg4 = HttpContext.Server.MapPath("~" + __d);
                string rtimg1 = Path.Combine(new string[] { Local, __a.Replace("/Fachada", "Fachada") });
                string rtimg2 = Path.Combine(new string[] { Local, __b.Replace("/Fachada", "Fachada") });
                string rtimg3 = Path.Combine(new string[] { Local, __c.Replace("/Fachada", "Fachada") });
                string rtimg4 = Path.Combine(new string[] { Local, __d.Replace("/Fachada", "Fachada") });

                DateTime thisDay = DateTime.Today;
                string fecha_generacon = thisDay.ToString("D").Replace(thisDay.ToString("dddd", CultureInfo.CreateSpecificCulture("es-ES")), "Lima");

                //object parametro = ruta;

                Word.Document objDoc = objWord.Documents.Open(ruta);

                object nombre1 = "Img1";
                object nombre2 = "Img2";
                object nombre3 = "Img3";
                object nombre4 = "Img4";
                object nombre5 = "nompdv";
                object nombre6 = "fechagen";

                Word.Range nom = objDoc.Bookmarks.get_Item(ref nombre1).Range;
                Word.Range nom2 = objDoc.Bookmarks.get_Item(ref nombre2).Range;
                Word.Range nom3 = objDoc.Bookmarks.get_Item(ref nombre3).Range;
                Word.Range nom4 = objDoc.Bookmarks.get_Item(ref nombre4).Range;
                Word.Range nom5 = objDoc.Bookmarks.get_Item(ref nombre5).Range;
                Word.Range nom6 = objDoc.Bookmarks.get_Item(ref nombre6).Range;

                nom.Text = "";
                nom2.Text = "";
                nom3.Text = "";
                nom4.Text = "";
                nom5.Text = __e;
                nom6.Text = fecha_generacon;

                var obj1 = objDoc.InlineShapes.AddPicture(rtimg1, Type.Missing, Type.Missing, nom);
                var obj2 = objDoc.InlineShapes.AddPicture(rtimg2, Type.Missing, Type.Missing, nom2);
                var obj3 = objDoc.InlineShapes.AddPicture(rtimg3, Type.Missing, Type.Missing, nom3);
                var obj4 = objDoc.InlineShapes.AddPicture(rtimg4, Type.Missing, Type.Missing, nom4);

                obj1.Width = 150; obj1.Height = 100;
                obj2.Width = 150; obj2.Height = 100;
                obj3.Width = 150; obj3.Height = 100;
                obj4.Width = 150; obj4.Height = 100;

                objWord.Documents.Save();
                objWord.Documents.Close();
                objWord.Quit();
                objWord = null;

                GC.Collect();
                //objWord.Visible = false;
                //return "/Temp/Fachada/Word/" + nomdoc;
            }
            catch (System.IO.IOException)
            {
                objWord.Documents.Close();
                //objWord.Quit(ref saveChanges, ref Unknown, ref Unknown);
                objWord = null;
                GC.Collect();
            }
            return nomdoc;
        }
        private string Word_Generar()
        {

            string fileName = "Carta_de_conformidad_de_fotomontaje.docx";
            string vnom = String.Format("{0:yyyyMMdd_HHmmss}", DateTime.Now);
            //string vnom = String.Format("{0:ddMMyyyy}", DateTime.Now);
            string fileName2 = "Carta_" + vnom + ".doc";
            //string sourcePath = @"\Temp\Fachada\Word";
            //string targetPath = @"C:\Users\Public\TestFolder\SubDir";

            string sourceFile = System.IO.Path.Combine(LocalFachada, "Word", fileName);
            string destFile = System.IO.Path.Combine(LocalFachada, "Word", fileName2);
            //string destFile = System.IO.Path.Combine(targetPath, fileName);

            System.IO.File.Copy(sourceFile, destFile, true);

            return fileName2;
        }

        [HttpPost]
        public ActionResult RegDetalle_PrePresupuesto(int __a, string __b, string __c, string __d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AddFotomont_PrePresupuesto(
                    new Request_WF_General() { Id_Reg = __a, Monto_Presupuesto = __b, Codigo_Presupuesto = __c, Cod_Op = 3, str_codigo_Presupuesto = __d }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_PrePresupuesto(int __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AddFotomont_PrePresupuesto(
                    new Request_WF_General() { Id_Reg = __a, Comen_Apro = __b, Cod_Op = 2 }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Asociacion_PrePresupuesto(string __a, string __b)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().AsociacionPdv_PrePresupuesto(
                    new Request_WF_General() { str_codigo_Presupuesto = __a, Comen_Apro = __b }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_Paso4_Ventas(int __a, int __b, string __c, int __d)
        {
            if (__d == 2 && __b == 2)
            {
                if (System.IO.File.Exists(Path.Combine(LocalFachada, __c)))
                {
                    try
                    {
                        System.IO.File.Delete(Path.Combine(LocalFachada, __c));
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso4_Ventas(
                    new Request_WF_General() { Id_Reg = __a, Estado = __b, Comen_Apro = __c, Cod_Op = __d }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_Paso5_Trade(int __a, string __b, string __c, string __d, int __e, int __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso5_Trade(
                    new Request_WF_General() { Id_Reg = __a, Ord_Pago = __b, Hes = __c, Ord_Comentario = __d, Estado = __e, Cod_Op = __f }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_Paso6_LuckyAnalis(int __a, string __b, string __c, int __d, int __e, string __f)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso6_LuckyAnalista(
                    new Request_WF_General() { Id_Reg = __a, Documento = __b, Fecha = __c, orden = __d, Cod_Op = __e, Nombre_Registro = __f }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_Paso6_Lucky_TipoTramite(int __a, int __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso3_lucky_Tipo_Tramite(
                    new Request_WF_General() { Cod_Op = __a, Tipo_Tramite = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_Paso6_Lucky_TipoPermiso(int __a, int __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso3_lucky_Tiempo_Permiso(
                    new Request_WF_General() { Cod_Op = __a, Tiempo_Permiso = __b, Id_Reg = __c }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_Paso7_LuckyTramite(int __a, string __b, string __c, string __d, string __e, string __f, string __g, int __h, string __i, string __j, string __k)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso7_LuckyTramite(
                    new Request_WF_General()
                    {
                        Id_Reg = __a
                        ,
                        fec_ing_expe = __b
                        ,
                        ruta_recibo = __c
                        ,
                        obs_recibo = __d
                        ,
                        fec_licencia = __e
                        ,
                        fec_vencimiento = __f
                        ,
                        ruta_licencia = __g
                        ,
                        Cod_Op = __h
                        ,
                        Monto_Presupuesto_Final = __i
                        ,
                        Codigo_Presupuesto_Final = __j
                        ,
                        Url_Presu_Final = __k
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Carga_Archivo(HttpPostedFileBase __f, string _b)
        {
            string _archivo = "";
            string _fileLocal = "";
            string _doc_url = "";

            if (__f != null)
            {
                _fileLocal = Path.GetFileName(__f.FileName);
                _archivo = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

                string vtipoarchi = _fileLocal.Substring(_fileLocal.LastIndexOf("."), 4).ToLower();

                switch (vtipoarchi)
                {
                    case ".xls":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(Path.Combine(LocalFachada, "Excel", "Excel_" + _archivo + ".xlsx"));
                            _doc_url = "/Fachada/Excel" + "/Excel_" + _archivo + ".xlsx";
                        }

                        break;
                    case ".pdf":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(Path.Combine(LocalFachada, "Pdf", "PDF_" + _archivo + ".pdf"));
                            _doc_url = "/Fachada/Pdf" + "/PDF_" + _archivo + ".pdf";
                        }
                        break;
                    case ".jpg":
                        __f.SaveAs(Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".jpg"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".jpg";
                        break;
                    case ".png":
                        __f.SaveAs(Path.Combine(LocalFachada, "Img", "Img_" + _archivo + ".png"));
                        _doc_url = "/Fachada/Img" + "/Img_" + _archivo + ".png";
                        break;
                    case ".pptx":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(Path.Combine(LocalFachada, "Ppt", "PPTX_" + _archivo + ".pptx"));
                            _doc_url = "/Fachada/Ppt" + "/PPTX_" + _archivo + ".pptx";
                        }
                        break;
                    case ".ppt":
                        if (System.IO.File.Exists(Path.Combine(LocalTemp, _b)))
                        {
                            __f.SaveAs(Path.Combine(LocalTemp, _b));
                            _doc_url = _b;
                        }
                        else
                        {
                            __f.SaveAs(Path.Combine(LocalFachada, "Ppt", "PPT_" + _archivo + ".ppt"));
                            _doc_url = "/Fachada/Ppt" + "/PPT_" + _archivo + ".ppt";
                        }
                        break;
                }
            }

            var headerAcceptVariable1 = this.Request.Headers["ACCEPT"] as string;

            if (headerAcceptVariable1 != null && headerAcceptVariable1.Contains("application/json"))
            {
                return Json(_doc_url);
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var response = serializer.Serialize(_doc_url);
                return Content(response);
            }
        }

        [HttpPost]
        public ActionResult GetGrillaTramite(int __a, string __b, string __c, string __d, string __e, string __f, string __g, int __h, string __i, string __j, string __k)
        {
            List<E_WF_Tramite> oLsWf = new WorkFlow().Consulta_Grilla_Tramite(
                new Request_WF_General()
                {
                    Id_Reg = __a
                        ,
                    fec_ing_expe = __b
                        ,
                    ruta_recibo = __c
                        ,
                    obs_recibo = __d
                        ,
                    fec_licencia = __e
                        ,
                    fec_vencimiento = __f
                        ,
                    ruta_licencia = __g
                        ,
                    Cod_Op = __h
                        ,
                    Monto_Presupuesto_Final = __i
                        ,
                    Codigo_Presupuesto_Final = __j
                        ,
                    Url_Presu_Final = __k
                });

            return Json(new { Archivo = oLsWf });
        }

        [HttpPost]
        public ActionResult Reg_Paso7_LuckyAnalista(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso7_LuckyAnalista(
                    new Request_WF_General()
                    {
                        Id_Reg = __a,
                        Documento = __b,
                        Cod_Op = __c
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public ActionResult Reg_Paso8_Trade(int __a, string __b, int __c)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(new WorkFlow().Ope_Paso8_Trade(
                    new Request_WF_General()
                    {
                        Id_Reg = __a,
                        Documento = __b,
                        Cod_Op = __c
                    }
                )),
                ContentType = "application/json"
            }.Content);
        }

        [HttpPost]
        public JsonResult WFExcel(int __a, string __b, string __c, string __d, int __e, string __f, int __g, int __h, string __i)
        {
            string _fileServer = "";
            string _filePath = "";

            int _fila = 0;

            _fileServer = String.Format("WF_" + "{0:ddMMyyyy_hhmmss}.xlsx", DateTime.Now);
            _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

            FileInfo _fileNew = new FileInfo(_filePath);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(_fileNew))
            {
                #region <<< Instancias >>>
                #region << Reporte WorkFlow >>

                int Grupo = Convert.ToInt32(Request["__j"]);
                /*int vcod_persona;
                vcod_persona = ((Persona)Session["Session_Login"]).Person_id;
                E_WF_Grupo_Per oResper = new E_WF_Grupo_Per();
                oResper = new WorkFlow().Consultar_Grupo_Persona(new Request_WF_Grupo_Per { Codigo = vcod_persona });*/

                List<E_Fachada_Unacem> oWfUna = new WorkFlow().Consulta_Solicitud(
                   new Request_WF_General()
                   {
                       Cod_Grupo = Grupo,
                       cod_zona = __a,
                       fecha_solicitud = __b,
                       fecha_foto = __c,
                       distribuidora = __d,
                       Cod_Persona = __e,
                       fecha_presupuesto = __f,
                       cod_equipo = __i,
                       Estado = __g,
                       Cod_Marca = __h
                   });

                #endregion

                #endregion


                #region <<< Reporte WorkFlow >>>

                if ((oWfUna != null))
                {
                    if (__i == "9020111072013")
                    {
                        int columnsuser1 = 46;
                        int columnsuser2 = 52;
                        int columnsuser3 = 52;
                        int columnsuser4 = 27;
                        if (Grupo == 1)
                        {
                            #region Perfil 1

                            Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Ventas");

                            #region Combinado de Celdas de Cabecera
                            //Combinacion de celdas
                            oWsUna.Cells["A1:P1"].Merge = true;
                            oWsUna.Cells["Q1:S1"].Merge = true;
                            oWsUna.Cells["T1:W1"].Merge = true;
                            oWsUna.Cells["X1:AA1"].Merge = true;
                            oWsUna.Cells["AB1:AC1"].Merge = true;
                            oWsUna.Cells["AD1:AQ1"].Merge = true;
                            oWsUna.Cells["AR1:AS1"].Merge = true;
                            #endregion

                            #region Valor a celdas combinadas
                            //Dando valor a celdas combinadas
                            oWsUna.Cells["A1:P1"].Value = "VENTAS";
                            oWsUna.Cells["Q1:S1"].Value = "TRADE MARKETING";
                            oWsUna.Cells["T1:W1"].Value = "LUCKY";
                            oWsUna.Cells["X1:AA1"].Value = "VENTAS";
                            oWsUna.Cells["AB1:AC1"].Value = "TRADE MARKETING";
                            oWsUna.Cells["AD1:AQ1"].Value = "LUCKY";
                            oWsUna.Cells["AR1:AS1"].Value = "TRADE MARKETING";
                            oWsUna.Cells["AT1:AT1"].Value = "";

                            #endregion

                            #region Valor a celdas Individuales
                            //Dando valor a celdas individuales
                            //COMIENZA PRIMERA PARTE VENTAS
                            oWsUna.Cells["A2"].Value = "USUARIO DE SOLICITUD";
                            oWsUna.Cells["B2"].Value = "FECHA DE SOLICITUD";
                            oWsUna.Cells["C2"].Value = "PUNTO DE VENTA";
                            oWsUna.Cells["D2"].Value = "CODIGO DE PDV";
                            oWsUna.Cells["E2"].Value = "DEPARTAMENTO";
                            oWsUna.Cells["F2"].Value = "PROVINCIA";
                            oWsUna.Cells["G2"].Value = "DISTRITO";
                            oWsUna.Cells["H2"].Value = "DIRECCION";
                            oWsUna.Cells["I2"].Value = "DISTRIBUIDORA";
                            oWsUna.Cells["J2"].Value = "ZONA";
                            oWsUna.Cells["K2"].Value = "RUC";
                            oWsUna.Cells["L2"].Value = "CELULAR";
                            oWsUna.Cells["M2"].Value = "CONTACTO";
                            oWsUna.Cells["N2"].Value = "MARCA";
                            oWsUna.Cells["O2"].Value = "TIPO";
                            oWsUna.Cells["P2"].Value = "ESTADO DE SOLICITUD";
                            //FIN PRIMERA PARTE VENTAS
                            //COMIENZA PRIMERA PARTE TRADE
                            oWsUna.Cells["Q2"].Value = "ESTADO DE SOLICITUD";
                            oWsUna.Cells["R2"].Value = "FECHA DE APROBACION";
                            oWsUna.Cells["S2"].Value = "COMENTARIO";
                            //FIN PRIMERA PARTE TRADE
                            //COMINZA PRIMERA PARTE LUCKYANALIS
                            oWsUna.Cells["T2"].Value = "FECHA DE VISITA";
                            oWsUna.Cells["U2"].Value = "COMENTARIO";
                            oWsUna.Cells["V2"].Value = "FOTOMONTAJE";
                            oWsUna.Cells["W2"].Value = "MONTO (S/.)";
                            //FIN PRIMERA PARTE LUCKYANALIS
                            //COMIENZA SEGUNDA PARTE VENTAS
                            oWsUna.Cells["X2"].Value = "APROBACION DE FOTOMONTAJE";
                            oWsUna.Cells["Y2"].Value = "FECHA DE APROBACION";
                            oWsUna.Cells["Z2"].Value = "COMENTARIO";
                            oWsUna.Cells["AA2"].Value = "CARTA DE APROBACION";
                            //FIN SEGUNDA PARTE VENTAS
                            //COMINEZA SEGUNDA PARTE TRADE
                            oWsUna.Cells["AB2"].Value = "APROBACION DE PRE-PRESUPUESTO";
                            oWsUna.Cells["AC2"].Value = "FECHA DE APROBACION";
                            //FIN SEGUNDA PARTE TRADE
                            //COMENZA SEGUNDA PARTE LUCKYANALIS
                            oWsUna.Cells["AD2"].Value = "FECHA DE IMPLEMENTACION";
                            oWsUna.Cells["AE2"].Value = "PROGRAMACION Y STATUS";
                            oWsUna.Cells["AF2"].Value = "TIPO DE TRAMITE";
                            oWsUna.Cells["AG2"].Value = "TIEMPO DE PERMISO";
                            oWsUna.Cells["AH2"].Value = "FECHA DE INGRESO DE EXPEDIENTE O CARTA";
                            oWsUna.Cells["AI2"].Value = "RECIBO DE TRAMITE MUNICIPAL O CARTA";
                            oWsUna.Cells["AJ2"].Value = "OBSERVACION";
                            oWsUna.Cells["AK2"].Value = "FECHA DE ENTREGA DE PERMISO";
                            oWsUna.Cells["AL2"].Value = "FECHA DE CADUCIDAD DE PERMISO";
                            oWsUna.Cells["AM2"].Value = "PERMISO";
                            oWsUna.Cells["AN2"].Value = "MONTO (S/.)";
                            oWsUna.Cells["AO2"].Value = "CODIGO";
                            oWsUna.Cells["AP2"].Value = "PRESUPUESTO FINAL";
                            oWsUna.Cells["AQ2"].Value = "REPORTE FOTOGRAFICO";
                            //FIN SEGUNDA PARTE LUCKYANALIS
                            //COMIENZA TERCERA PARTE TRADE
                            oWsUna.Cells["AR2"].Value = "VALIDACION DE FACHADA";
                            oWsUna.Cells["AS2"].Value = "FECHA DE VALIDACION";
                            //FIN TERCERA PARTE TRADE

                            oWsUna.Cells["AT2"].Value = "NRO REGISTRO";
                            #endregion

                            #region Valor al Cuerpo de Celdas
                            _fila = 3;

                            foreach (E_Fachada_Unacem oBj in oWfUna)
                            {
                                oWsUna.Cells[_fila, 1].Value = (oBj.P1_Usuario_Solicita).ToUpper();
                                oWsUna.Cells[_fila, 2].Value = (oBj.Fec_Solicitud).ToUpper();
                                oWsUna.Cells[_fila, 3].Value = (oBj.Nom_PDV).ToUpper();
                                oWsUna.Cells[_fila, 4].Value = (oBj.Codigo).ToUpper();
                                oWsUna.Cells[_fila, 5].Value = (oBj.Departamento).ToUpper();
                                oWsUna.Cells[_fila, 6].Value = (oBj.Provincia).ToUpper();
                                oWsUna.Cells[_fila, 7].Value = (oBj.Distrito).ToUpper();
                                oWsUna.Cells[_fila, 8].Value = (oBj.Direccion).ToUpper();
                                oWsUna.Cells[_fila, 9].Value = (oBj.Distribuidora).ToUpper();
                                oWsUna.Cells[_fila, 10].Value = (oBj.DescripcionZona).ToUpper();
                                oWsUna.Cells[_fila, 11].Value = (oBj.Num_RUC).ToUpper();
                                oWsUna.Cells[_fila, 12].Value = (oBj.Num_Cel).ToUpper();
                                oWsUna.Cells[_fila, 13].Value = (oBj.Contacto).ToUpper();
                                oWsUna.Cells[_fila, 14].Value = (oBj.DescripcionMarca).ToUpper();
                                oWsUna.Cells[_fila, 15].Value = (oBj.DescripcionTipoSolicitud).ToUpper();
                                oWsUna.Cells[_fila, 16].Value = (oBj.Est_Solicitud).ToUpper();
                                switch (oBj.Apro_Solicitud.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 17].Value = "APROBADO";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 17].Value = "DESAPROBADO";
                                        break;
                                }
                                oWsUna.Cells[_fila, 18].Value = (oBj.Fec_Apro_Solicitud).ToUpper();
                                oWsUna.Cells[_fila, 19].Value = (oBj.Comen_Apro_Solicitud).ToUpper();
                                oWsUna.Cells[_fila, 20].Value = (oBj.P2_Fecha_Entrega_Foto).ToUpper();
                                oWsUna.Cells[_fila, 21].Value = (oBj.comen_expediente).ToUpper();
                                if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                                {
                                    oWsUna.Cells[_fila, 22].Value = "CON FOTO";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 22].Value = "";
                                }
                                oWsUna.Cells[_fila, 23].Value = (oBj.p3_monto_presupuesto).ToUpper();
                                if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                                {
                                    switch (oBj.Apro_Fotomontaje.ToUpper())
                                    {
                                        case "TRUE":
                                            oWsUna.Cells[_fila, 24].Value = "APROBADO";
                                            break;
                                        case "FALSE":
                                            oWsUna.Cells[_fila, 24].Value = "DESAPROBADO";
                                            break;
                                    }
                                }
                                oWsUna.Cells[_fila, 25].Value = (oBj.Fec_Apro_Fotomonataje).ToUpper();
                                oWsUna.Cells[_fila, 26].Value = (oBj.Comen_Apro_Fotomontaje).ToUpper();
                                if (oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje.Length > 0)
                                {
                                    if (oBj.Url_Carta_Fotomontaje != null && oBj.Url_Carta_Fotomontaje.Replace(" ", "").Length > 0)
                                    {
                                        oWsUna.Cells[_fila, 27].Value = "CON CARTA";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 27].Value = "FALTA CARGAR CARTA";
                                    }
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 27].Value = "";
                                }
                                switch (oBj.Apro_pre_Presu.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 28].Value = "APROBADO";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 28].Value = "DESAPROBADO";
                                        break;
                                }
                                oWsUna.Cells[_fila, 29].Value = (oBj.Fec_Apro_pre_Presu).ToUpper();
                                if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                                {
                                    oWsUna.Cells[_fila, 30].Value = (oBj.fecha_implementacion).ToUpper();
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 30].Value = "";
                                }
                                string _vcolor = "";
                                if (oBj.Cantidad == 0)
                                {
                                    _vcolor = "PENDIENTES";
                                }
                                else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                                {
                                    _vcolor = "ALGUNOS PENDIENTES";
                                }
                                else if (oBj.Cantidad == 10)
                                {
                                    _vcolor = "REVISADO";
                                }
                                if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                                {
                                    oWsUna.Cells[_fila, 31].Value = _vcolor;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 31].Value = _vcolor;
                                }
                                oWsUna.Cells[_fila, 32].Value = (oBj.DescripcionTipoTramite).ToUpper();
                                oWsUna.Cells[_fila, 33].Value = (oBj.DescripcionTiempoPermiso).ToUpper();
                                if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                                {
                                    oWsUna.Cells[_fila, 34].Value = (oBj.Fec_ingre_expe).ToUpper();
                                    if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                    {
                                        oWsUna.Cells[_fila, 35].Value = "CON RECIBO";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 35].Value = "";
                                    }
                                    oWsUna.Cells[_fila, 36].Value = (oBj.Obs_Tramite).ToUpper();
                                    if (oBj.Fec_Entre_Licencia != "" && oBj.Fec_Entre_Licencia != "01/01/1900")
                                    {
                                        oWsUna.Cells[_fila, 37].Value = (oBj.Fec_Entre_Licencia).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 37].Value = "";
                                    }
                                    if (oBj.Fec_Cadu_Licencia != "" && oBj.Fec_Cadu_Licencia != "01/01/1900")
                                    {
                                        oWsUna.Cells[_fila, 38].Value = (oBj.Fec_Cadu_Licencia).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 38].Value = "";
                                    }
                                    if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                    {
                                        oWsUna.Cells[_fila, 39].Value = "CON PERMISO";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 39].Value = "";
                                    }
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 34].Value = "";
                                    oWsUna.Cells[_fila, 35].Value = "";
                                    oWsUna.Cells[_fila, 36].Value = "";
                                    oWsUna.Cells[_fila, 37].Value = "";
                                    oWsUna.Cells[_fila, 38].Value = "";
                                    oWsUna.Cells[_fila, 39].Value = "";
                                }
                                if (oBj.p3_monto_presupuesto_final != null && oBj.p3_monto_presupuesto_final != "")
                                {
                                    oWsUna.Cells[_fila, 40].Value = (oBj.p3_monto_presupuesto_final).ToUpper();
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 40].Value = "";
                                }

                                if (oBj.p3_codigo_presupuesto_final != null && oBj.p3_codigo_presupuesto_final != "")
                                {
                                    oWsUna.Cells[_fila, 41].Value = (oBj.p3_codigo_presupuesto_final).ToUpper();
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 41].Value = "";
                                }
                                if (oBj.Url_Presu_Final != null && oBj.p3_codigo_presupuesto_final != "")
                                {
                                    oWsUna.Cells[_fila, 42].Value = "CON PRESUPUESTO FINAL";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 42].Value = "";
                                }

                                if (oBj.Url_Report_Fotografico != null && oBj.Url_Report_Fotografico != "")
                                {
                                    oWsUna.Cells[_fila, 43].Value = "CON REPORTE FOTOGRAFICO";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 43].Value = "";
                                }

                                switch (oBj.Vali_Fechada_Imple.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 44].Value = "APROBADO";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 44].Value = "DESAPROBADO";
                                        break;
                                }
                                oWsUna.Cells[_fila, 45].Value = oBj.Fec_Vali_Fachada_Imple;
                                if (Convert.ToInt32(oBj.Id_Registro) < 10)
                                {
                                    oWsUna.Cells[_fila, 46].Value = "WF_0000" + Convert.ToInt32(oBj.Id_Registro);
                                }
                                else
                                {
                                    if (Convert.ToInt32(oBj.Id_Registro) >= 10 && Convert.ToInt32(oBj.Id_Registro) < 100)
                                    {
                                        oWsUna.Cells[_fila, 46].Value = "WF_000" + Convert.ToInt32(oBj.Id_Registro);
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(oBj.Id_Registro) >= 100 && Convert.ToInt32(oBj.Id_Registro) < 1000)
                                        {
                                            oWsUna.Cells[_fila, 46].Value = "WF_00" + Convert.ToInt32(oBj.Id_Registro);
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(oBj.Id_Registro) >= 1000 && Convert.ToInt32(oBj.Id_Registro) < 10000)
                                            {
                                                oWsUna.Cells[_fila, 46].Value = "WF_0" + Convert.ToInt32(oBj.Id_Registro);
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(oBj.Id_Registro) >= 10000)
                                                {
                                                    oWsUna.Cells[_fila, 46].Value = "WF_" + Convert.ToInt32(oBj.Id_Registro);
                                                }
                                                else
                                                {
                                                    oWsUna.Cells[_fila, 46].Value = "";
                                                }
                                            }
                                        }
                                    }
                                }
                                



                                for (int i = 1; i <= columnsuser1; i++)
                                {
                                    oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                    //ajustando texto
                                    oWsUna.Cells[_fila, i].Style.WrapText = true;
                                }



                                _fila++;
                            }

                            #endregion

                            #region Estilos
                            //Formato Cabecera 1
                            oWsUna.Row(1).Height = 25;
                            oWsUna.Row(1).Style.Font.Bold = true;
                            oWsUna.Cells[2, 1, 2, columnsuser1].Style.WrapText = true;
                            oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                            oWsUna.Cells["A1:AT1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                            oWsUna.Cells["A1:AT1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                            //Formato Cabecera 2
                            oWsUna.Row(2).Height = 45;
                            oWsUna.Row(2).Style.Font.Bold = true;
                            oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                            oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                            oWsUna.Cells["A2:AT2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                            oWsUna.Cells["A2:AT2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                            oWsUna.SelectedRange["A2:AT2"].AutoFilter = true;


                            //Formato ambas cabeceras
                            oWsUna.Cells["A1:AT2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AT2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AT2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AT2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                            #endregion

                            #region Ancho a Columnas
                            //Formato por columnas
                            //Ventas
                            oWsUna.Column(1).Width = 29;
                            oWsUna.Column(2).Width = 13;
                            oWsUna.Column(3).Width = 38;
                            oWsUna.Column(4).Width = 18;
                            oWsUna.Column(5).Width = 18;
                            oWsUna.Column(6).Width = 13;
                            oWsUna.Column(7).Width = 18;
                            oWsUna.Column(8).Width = 55;
                            oWsUna.Column(9).Width = 17;
                            oWsUna.Column(10).Width = 14;
                            oWsUna.Column(11).Width = 13;
                            oWsUna.Column(12).Width = 11;
                            oWsUna.Column(13).Width = 21;
                            oWsUna.Column(14).Width = 12;
                            oWsUna.Column(15).Width = 35;
                            oWsUna.Column(16).Width = 13;
                            //trade marketing
                            oWsUna.Column(17).Width = 14;//Estado Solicitud
                            oWsUna.Column(18).Width = 15;//Fecha de Aprobacion
                            oWsUna.Column(19).Width = 44;//Comentario
                            //lucky
                            oWsUna.Column(20).Width = 12;//Fecha Visita
                            oWsUna.Column(21).Width = 44;//Comentario
                            oWsUna.Column(22).Width = 17;//Fotomontaje
                            oWsUna.Column(23).Width = 14;//Monto (S/.)
                            //ventas
                            oWsUna.Column(24).Width = 18;//Aprobacion de fotomontaje
                            oWsUna.Column(25).Width = 15;//Fecha de Aprobacion
                            oWsUna.Column(26).Width = 44;//Comentario
                            oWsUna.Column(27).Width = 20;//Carta de Aprobacion
                            //trade marketing
                            oWsUna.Column(28).Width = 21;//Aprobacion de Pre-Presupuesto
                            oWsUna.Column(29).Width = 15;//Fecha de Aprobacion
                            //lucky
                            oWsUna.Column(30).Width = 19;//Fecha de Implementacion
                            oWsUna.Column(31).Width = 21;//Programacion y Status
                            oWsUna.Column(32).Width = 14;//Tipo de Tramite
                            oWsUna.Column(33).Width = 12;//Tiempo de Permiso
                            oWsUna.Column(34).Width = 14;//Fecha de Ingreso de Expediente
                            oWsUna.Column(35).Width = 14;//Recibode Tramite Municipal
                            oWsUna.Column(36).Width = 44;//OBSERVACION
                            oWsUna.Column(37).Width = 14;//Fecha DE Entrega de Permiso
                            oWsUna.Column(38).Width = 14;//Fecha de Caducidad de Permiso
                            oWsUna.Column(39).Width = 14;//Permiso
                            oWsUna.Column(40).Width = 14;//Monto (S/.)
                            oWsUna.Column(41).Width = 14;//Codigo
                            oWsUna.Column(42).Width = 23;//Presupuesto Final
                            oWsUna.Column(43).Width = 26;//Reporte Fotografico
                            //trade marketing
                            oWsUna.Column(44).Width = 15;//Validacion de Fachada
                            oWsUna.Column(45).Width = 14;//Fecha de Validacion
                            oWsUna.Column(46).Width = 17;//Id Registro

                            #endregion

                            oWsUna.View.FreezePanes(3, 1);


                            oEx.Save();

                            #endregion
                        }
                        else if (Grupo == 2)
                        {
                            #region Perfil 2

                            Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Trade");

                            #region Combinado de Celdas de Cabecera
                            //Combinacion de celdas
                            oWsUna.Cells["A1:P1"].Merge = true;
                            oWsUna.Cells["Q1:S1"].Merge = true;
                            oWsUna.Cells["T1:Y1"].Merge = true;
                            oWsUna.Cells["Z1:AC1"].Merge = true;
                            oWsUna.Cells["AD1:AI1"].Merge = true;
                            oWsUna.Cells["AJ1:AW1"].Merge = true;
                            oWsUna.Cells["AX1:AY1"].Merge = true;
                            #endregion

                            #region valor a celdas combinadas
                            //Dando valor a celdas combinadas
                            oWsUna.Cells["A1:P1"].Value = "VENTAS";
                            oWsUna.Cells["Q1:S1"].Value = "TRADE MARKETING";
                            oWsUna.Cells["T1:Y1"].Value = "LUCKY";
                            oWsUna.Cells["Z1:AC1"].Value = "VENTAS";
                            oWsUna.Cells["AD1:AI1"].Value = "TRADE MARKETING";
                            oWsUna.Cells["AJ1:AW1"].Value = "LUCKY";
                            oWsUna.Cells["AX1:AY1"].Value = "TRADE MARKETING";
                            oWsUna.Cells["AZ1:AZ1"].Value = "";
                            #endregion

                            #region valor a celdas indiviuales
                            //Dando valor a celdas individuales
                            //COMIENZA PRIMERA PARTE VENTAS
                            oWsUna.Cells["A2"].Value = "USUARIO DE SOLICITUD";
                            oWsUna.Cells["B2"].Value = "FECHA DE SOLICITUD";
                            oWsUna.Cells["C2"].Value = "PUNTO DE VENTA";
                            oWsUna.Cells["D2"].Value = "CODIGO DE PDV";
                            oWsUna.Cells["E2"].Value = "DEPARTAMENTO";
                            oWsUna.Cells["F2"].Value = "PROVINCIA";
                            oWsUna.Cells["G2"].Value = "DISTRITO";
                            oWsUna.Cells["H2"].Value = "DIRECCION";
                            oWsUna.Cells["I2"].Value = "DISTRIBUIDORA";
                            oWsUna.Cells["J2"].Value = "ZONA";
                            oWsUna.Cells["K2"].Value = "RUC";
                            oWsUna.Cells["L2"].Value = "CELULAR";
                            oWsUna.Cells["M2"].Value = "CONTACTO";
                            oWsUna.Cells["N2"].Value = "MARCA";
                            oWsUna.Cells["O2"].Value = "TIPO";
                            oWsUna.Cells["P2"].Value = "ESTADO DE SOLICITUD";
                            //FIN PRIMERA PARTE VENTAS
                            //COMIENZA PRIMERA PARTE TRADE
                            oWsUna.Cells["Q2"].Value = "ESTADO DE SOLICITUD";
                            oWsUna.Cells["R2"].Value = "FECHA DE APROBACION";
                            oWsUna.Cells["S2"].Value = "COMENTARIO";
                            //FIN PRIMERA PARTE TRADE
                            //COMIENZA PRIMERA PARTE LUCKYANALIS
                            oWsUna.Cells["T2"].Value = "FECHA DE VISITA";
                            oWsUna.Cells["U2"].Value = "COMENTARIO";
                            oWsUna.Cells["V2"].Value = "FOTOMONTAJE";
                            oWsUna.Cells["W2"].Value = "MONTO (S/.)";
                            oWsUna.Cells["X2"].Value = "CODIGO";
                            oWsUna.Cells["Y2"].Value = "PRE-PRESUPUESTO";
                            //FIN PRIMERA PARTE LUCKYANALIS
                            //COMIENZA SEGUNDA PARTE VENTAS
                            oWsUna.Cells["Z2"].Value = "APROBACION DE FOTOMONTAJE";
                            oWsUna.Cells["AA2"].Value = "FECHA DE APROBACION";
                            oWsUna.Cells["AB2"].Value = "COMENTARIO";
                            oWsUna.Cells["AC2"].Value = "CARTA DE APROBACION";
                            //FIN SEGUNDA PARTE VENTAS
                            //COMIENZA SEGUNDA PARTE TRADE
                            oWsUna.Cells["AD2"].Value = "APROBACION DE PRE-PRESUPUESTO";
                            oWsUna.Cells["AE2"].Value = "FECHA DE APROBACION";
                            oWsUna.Cells["AF2"].Value = "ORDEN DE COMPRA";
                            oWsUna.Cells["AG2"].Value = "FECHA";
                            oWsUna.Cells["AH2"].Value = "H.E.S.";
                            oWsUna.Cells["AI2"].Value = "COMENTARIO";
                            //FIN SEGUNDA PARTE TRADE
                            //COMIENZA SEGUNDA PARTE LUCKYANALIS
                            oWsUna.Cells["AJ2"].Value = "FECHA DE IMPLEMENTACION";
                            oWsUna.Cells["AK2"].Value = "PROGRAMACION Y STATUS";
                            oWsUna.Cells["AL2"].Value = "TIPO DE TRAMITE";
                            oWsUna.Cells["AM2"].Value = "TIEMPO DE PERMISO";
                            oWsUna.Cells["AN2"].Value = "FECHA DE INGRESO DE EXPEDIENTE O CARTA";
                            oWsUna.Cells["AO2"].Value = "RECIBO DE TRAMITE MUNICIPAL O CARTA";
                            oWsUna.Cells["AP2"].Value = "OBSERVACION";
                            oWsUna.Cells["AQ2"].Value = "FECHA DE ENTREGA DE PERMISO";
                            oWsUna.Cells["AR2"].Value = "FECHA DE CADUCIDAD DE PERMISO";
                            oWsUna.Cells["AS2"].Value = "PERMISO";
                            oWsUna.Cells["AT2"].Value = "MONTO (S/.)";
                            oWsUna.Cells["AU2"].Value = "CODIGO";
                            oWsUna.Cells["AV2"].Value = "PRESUPUESTO FINAL";
                            oWsUna.Cells["AW2"].Value = "REPORTE FOTOGRAFICO";
                            //FIN SEGUNDA PARTE LUCKYANALIS
                            //COMIENZA TERCERA PARTE TRADE
                            oWsUna.Cells["AX2"].Value = "VALIDACION DE FACHADA";
                            oWsUna.Cells["AY2"].Value = "FECHA DE VALIDACION";
                            //FIN TERCERA PARTE TRADE
                            oWsUna.Cells["AZ2"].Value = "NRO REGISTRO";
                            #endregion

                            #region valor al cuerpo de celdas
                            _fila = 3;
                            foreach (E_Fachada_Unacem oBj in oWfUna)
                            {
                                //COMIENZA PRIMERA PARTE VENTAS
                                oWsUna.Cells[_fila, 1].Value = (oBj.P1_Usuario_Solicita).ToUpper();
                                oWsUna.Cells[_fila, 2].Value = (oBj.Fec_Solicitud).ToUpper();
                                oWsUna.Cells[_fila, 3].Value = (oBj.Nom_PDV).ToUpper();
                                oWsUna.Cells[_fila, 4].Value = (oBj.Codigo).ToUpper();
                                oWsUna.Cells[_fila, 5].Value = (oBj.Departamento).ToUpper();
                                oWsUna.Cells[_fila, 6].Value = (oBj.Provincia).ToUpper();
                                oWsUna.Cells[_fila, 7].Value = (oBj.Distrito).ToUpper();
                                oWsUna.Cells[_fila, 8].Value = (oBj.Direccion).ToUpper();
                                oWsUna.Cells[_fila, 9].Value = (oBj.Distribuidora).ToUpper();
                                oWsUna.Cells[_fila, 10].Value = (oBj.DescripcionZona).ToUpper();
                                oWsUna.Cells[_fila, 11].Value = (oBj.Num_RUC).ToUpper();
                                oWsUna.Cells[_fila, 12].Value = (oBj.Num_Cel).ToUpper();
                                oWsUna.Cells[_fila, 13].Value = (oBj.Contacto).ToUpper();
                                oWsUna.Cells[_fila, 14].Value = (oBj.DescripcionMarca).ToUpper();
                                oWsUna.Cells[_fila, 15].Value = (oBj.DescripcionTipoSolicitud).ToUpper();
                                oWsUna.Cells[_fila, 16].Value = (oBj.Est_Solicitud).ToUpper();
                                //TERMINA PRIMERA PARTE VENTAS
                                //COMIENZA PRIMERA PARTE TRADE
                                switch (oBj.Apro_Solicitud.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 17].Value = "APROBADO";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 17].Value = "DESAPROBADO";
                                        break;
                                }
                                oWsUna.Cells[_fila, 18].Value = (oBj.Fec_Apro_Solicitud).ToUpper();
                                oWsUna.Cells[_fila, 19].Value = (oBj.Comen_Apro_Solicitud).ToUpper();
                                //TERMINA PRIMERA PARTE TRADE
                                //COMIENZA PRIMERA PARTE LUCKYANALIS
                                oWsUna.Cells[_fila, 20].Value = (oBj.P2_Fecha_Entrega_Foto).ToUpper();
                                oWsUna.Cells[_fila, 21].Value = (oBj.comen_expediente).ToUpper();
                                if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                                {
                                    oWsUna.Cells[_fila, 22].Value = "CON FOTO";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 22].Value = "";
                                }
                                oWsUna.Cells[_fila, 23].Value = (oBj.p3_monto_presupuesto).ToUpper();
                                oWsUna.Cells[_fila, 24].Value = (oBj.p3_codigo_presupuesto).ToUpper();
                                if (oBj.Url_Presupuesto != "" && oBj.Url_Presupuesto != null)
                                {
                                    oWsUna.Cells[_fila, 25].Value = "CON PRESUPUESTO";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 25].Value = "";
                                }
                                if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                                {
                                    switch (oBj.Apro_Fotomontaje.ToUpper())
                                    {
                                        case "TRUE":
                                            oWsUna.Cells[_fila, 26].Value = "APROBADO";
                                            break;
                                        case "FALSE":
                                            oWsUna.Cells[_fila, 26].Value = "DESAPROBADO";
                                            break;
                                    }
                                }
                                oWsUna.Cells[_fila, 27].Value = (oBj.Fec_Apro_Fotomonataje).ToUpper();
                                oWsUna.Cells[_fila, 28].Value = (oBj.Comen_Apro_Fotomontaje).ToUpper();
                                if (oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje.Length > 0)
                                {
                                    if (oBj.Url_Carta_Fotomontaje != null && oBj.Url_Carta_Fotomontaje.Replace(" ", "").Length > 0)
                                    {
                                        oWsUna.Cells[_fila, 29].Value = "CON CARTA";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 29].Value = "FALTA CARGAR CARTA";
                                    }
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 29].Value = "";
                                }
                                switch (oBj.Apro_pre_Presu.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 30].Value = "APROBADO";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 30].Value = "DESAPROBADO";
                                        break;
                                }
                                oWsUna.Cells[_fila, 31].Value = (oBj.Fec_Apro_pre_Presu).ToUpper();
                                oWsUna.Cells[_fila, 32].Value = (oBj.Orden_Compra).ToUpper();
                                oWsUna.Cells[_fila, 33].Value = (oBj.fecha_p5_trade).ToUpper();
                                oWsUna.Cells[_fila, 34].Value = (oBj.HES).ToUpper();
                                oWsUna.Cells[_fila, 35].Value = (oBj.Comen_Apro_Pre_Presu).ToUpper();
                                if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                                {
                                    oWsUna.Cells[_fila, 36].Value = (oBj.fecha_implementacion).ToUpper();
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 36].Value = "";
                                }
                                string _vcolor = "";
                                if (oBj.Cantidad == 0)
                                {
                                    _vcolor = "PENDIENTES";
                                }
                                else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                                {
                                    _vcolor = "ALGUNOS PENDIENTES";
                                }
                                else if (oBj.Cantidad == 10)
                                {
                                    _vcolor = "REVISADO";
                                }
                                if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                                {
                                    oWsUna.Cells[_fila, 37].Value = _vcolor;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 37].Value = _vcolor;
                                }
                                oWsUna.Cells[_fila, 38].Value = (oBj.DescripcionTipoTramite).ToUpper();
                                oWsUna.Cells[_fila, 39].Value = (oBj.DescripcionTiempoPermiso).ToUpper();
                                if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                                {
                                    if (oBj.Fec_ingre_expe != null && oBj.Fec_ingre_expe != "" && oBj.Fec_ingre_expe != "01/01/1900" && oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                    {
                                        oWsUna.Cells[_fila, 40].Value = (oBj.Fec_ingre_expe).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 40].Value = "";
                                    }
                                    if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                    {
                                        oWsUna.Cells[_fila, 41].Value = "CON RECIBO";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 41].Value = "";
                                    }
                                    oWsUna.Cells[_fila, 42].Value = (oBj.Obs_Tramite).ToUpper();
                                    if (oBj.Fec_Entre_Licencia != null && oBj.Fec_Entre_Licencia != "" && oBj.Fec_Entre_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                    {
                                        oWsUna.Cells[_fila, 43].Value = (oBj.Fec_Entre_Licencia).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 43].Value = "";
                                    }
                                    if (oBj.Fec_Cadu_Licencia != null && oBj.Fec_Cadu_Licencia != "" && oBj.Fec_Cadu_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                    {
                                        oWsUna.Cells[_fila, 44].Value = (oBj.Fec_Cadu_Licencia).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 44].Value = "";
                                    }
                                    if (oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                    {
                                        oWsUna.Cells[_fila, 45].Value = "CON PERMISO";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 45].Value = "";
                                    }
                                    oWsUna.Cells[_fila, 46].Value = (oBj.p3_monto_presupuesto_final).ToUpper();
                                    oWsUna.Cells[_fila, 47].Value = (oBj.p3_codigo_presupuesto_final).ToUpper();
                                    if (oBj.Url_Presu_Final != null && oBj.Url_Presu_Final != "")
                                    {
                                        oWsUna.Cells[_fila, 48].Value = "CON PRESUPUESTO";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 48].Value = "";
                                    }
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 40].Value = "";
                                    oWsUna.Cells[_fila, 41].Value = "";
                                    oWsUna.Cells[_fila, 42].Value = "";
                                    oWsUna.Cells[_fila, 43].Value = "";
                                    oWsUna.Cells[_fila, 44].Value = "";
                                    oWsUna.Cells[_fila, 45].Value = "";
                                    oWsUna.Cells[_fila, 46].Value = "";
                                    oWsUna.Cells[_fila, 47].Value = "";
                                    oWsUna.Cells[_fila, 48].Value = "";
                                }
                                if (oBj.Url_Report_Fotografico != null && oBj.Url_Report_Fotografico != "")
                                {
                                    oWsUna.Cells[_fila, 49].Value = "CON REPORTE FOTOGRAFICO";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 49].Value = "";
                                }
                                switch (oBj.Vali_Fechada_Imple.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 50].Value = "APROBADO";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 50].Value = "DESAPROBADO";
                                        break;
                                }
                                oWsUna.Cells[_fila, 51].Value = (oBj.Fec_Vali_Fachada_Imple).ToUpper();
                                if (Convert.ToInt32(oBj.Id_Registro) < 10)
                                {
                                    oWsUna.Cells[_fila, 52].Value = "WF_0000" + Convert.ToInt32(oBj.Id_Registro);
                                }
                                else
                                {
                                    if (Convert.ToInt32(oBj.Id_Registro) >= 10 && Convert.ToInt32(oBj.Id_Registro) < 100)
                                    {
                                        oWsUna.Cells[_fila, 52].Value = "WF_000" + Convert.ToInt32(oBj.Id_Registro);
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(oBj.Id_Registro) >= 100 && Convert.ToInt32(oBj.Id_Registro) < 1000)
                                        {
                                            oWsUna.Cells[_fila, 52].Value = "WF_00" + Convert.ToInt32(oBj.Id_Registro);
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(oBj.Id_Registro) >= 1000 && Convert.ToInt32(oBj.Id_Registro) < 10000)
                                            {
                                                oWsUna.Cells[_fila, 52].Value = "WF_0" + Convert.ToInt32(oBj.Id_Registro);
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(oBj.Id_Registro) >= 10000)
                                                {
                                                    oWsUna.Cells[_fila, 52].Value = "WF_" + Convert.ToInt32(oBj.Id_Registro);
                                                }
                                                else
                                                {
                                                    oWsUna.Cells[_fila, 52].Value = "";
                                                }
                                            }
                                        }
                                    }
                                }



                                for (int i = 1; i <= columnsuser2; i++)
                                {
                                    oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                    //ajustando texto
                                    oWsUna.Cells[_fila, i].Style.WrapText = true;
                                }



                                _fila++;
                            }
                            #endregion

                            #region estilos
                            //Formato Cabecera 1
                            oWsUna.Row(1).Height = 25;
                            oWsUna.Row(1).Style.Font.Bold = true;
                            oWsUna.Cells[2, 1, 2, columnsuser2].Style.WrapText = true;
                            oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                            oWsUna.Cells["A1:AZ1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                            oWsUna.Cells["A1:AZ1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                            //Formato Cabecera 2
                            oWsUna.Row(2).Height = 45;
                            oWsUna.Row(2).Style.Font.Bold = true;
                            oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                            oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                            oWsUna.Cells["A2:AZ2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                            oWsUna.Cells["A2:AZ2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                            oWsUna.SelectedRange["A2:AZ2"].AutoFilter = true;


                            //Formato ambas cabeceras
                            oWsUna.Cells["A1:AZ2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AZ2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AZ2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AZ2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                            #endregion

                            #region Ancho a Columnas
                            //Formato por columnas
                            //Ventas
                            oWsUna.Column(1).Width = 29;
                            oWsUna.Column(2).Width = 13;
                            oWsUna.Column(3).Width = 38;
                            oWsUna.Column(4).Width = 18;
                            oWsUna.Column(5).Width = 18;
                            oWsUna.Column(6).Width = 13;
                            oWsUna.Column(7).Width = 18;
                            oWsUna.Column(8).Width = 55;
                            oWsUna.Column(9).Width = 17;
                            oWsUna.Column(10).Width = 14;
                            oWsUna.Column(11).Width = 13;
                            oWsUna.Column(12).Width = 11;
                            oWsUna.Column(13).Width = 21;
                            oWsUna.Column(14).Width = 12;
                            oWsUna.Column(15).Width = 35;
                            oWsUna.Column(16).Width = 13;
                            //trade marketing
                            oWsUna.Column(17).Width = 14;
                            oWsUna.Column(18).Width = 15;
                            oWsUna.Column(19).Width = 44;
                            //lucky
                            oWsUna.Column(20).Width = 12;
                            oWsUna.Column(21).Width = 44;
                            oWsUna.Column(22).Width = 17;
                            oWsUna.Column(23).Width = 14;
                            oWsUna.Column(24).Width = 9;
                            oWsUna.Column(25).Width = 18;
                            //ventas
                            oWsUna.Column(26).Width = 18;//Aprobacion de Fotomontaje
                            oWsUna.Column(27).Width = 15;//Fecha de Aprobacion
                            oWsUna.Column(28).Width = 44;//Comentario
                            oWsUna.Column(29).Width = 20;//Carta de Aprobacion
                            //trade marketing
                            oWsUna.Column(30).Width = 21;
                            oWsUna.Column(31).Width = 15;
                            oWsUna.Column(32).Width = 13;
                            oWsUna.Column(33).Width = 12;
                            oWsUna.Column(34).Width = 13;
                            oWsUna.Column(35).Width = 44;
                            //lucky
                            oWsUna.Column(36).Width = 19;//Fecha de Implementacion
                            oWsUna.Column(37).Width = 21;//Status Documentario
                            oWsUna.Column(38).Width = 14;//Tipo de Tramite
                            oWsUna.Column(39).Width = 12;//Tiempo de Permiso
                            oWsUna.Column(40).Width = 14;//Fecha de Ingreso de Expediente o Carta
                            oWsUna.Column(41).Width = 14;//Recibo de Tramite Municipal o Carta
                            oWsUna.Column(42).Width = 44;//Observacion
                            oWsUna.Column(43).Width = 14;//Fecha de Entrega de Permiso
                            oWsUna.Column(44).Width = 14;//Fecha de Caducidad de permiso
                            oWsUna.Column(45).Width = 14;//Permiso
                            oWsUna.Column(46).Width = 14;//Monto (S/.)
                            oWsUna.Column(47).Width = 14;//Codigo
                            oWsUna.Column(48).Width = 23;//Presupuetso de Tramite
                            oWsUna.Column(49).Width = 26;//Reporte Fotografico
                            //trade marketing
                            oWsUna.Column(50).Width = 15;
                            oWsUna.Column(51).Width = 14;
                            oWsUna.Column(52).Width = 17;
                            #endregion

                            oWsUna.View.FreezePanes(3, 1);


                            oEx.Save();

                            #endregion
                        }
                        else if (Grupo == 3)
                        {
                            #region Perfil 3

                            Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Analista");

                            #region Combinado de Celdas de Cabecera
                            //Combinacion de celdas
                            oWsUna.Cells["A1:P1"].Merge = true;
                            oWsUna.Cells["Q1:S1"].Merge = true;
                            oWsUna.Cells["T1:Z1"].Merge = true;
                            oWsUna.Cells["AA1:AD1"].Merge = true;
                            oWsUna.Cells["AE1:AI1"].Merge = true;
                            oWsUna.Cells["AJ1:AW1"].Merge = true;
                            oWsUna.Cells["AX1:AY1"].Merge = true;
                            #endregion

                            #region Valor a Celdas Conbinadas
                            //Dando valor a celdas combinadas
                            oWsUna.Cells["A1:P1"].Value = "VENTAS";
                            oWsUna.Cells["Q1:S1"].Value = "TRADE MARKETING";
                            oWsUna.Cells["T1:Z1"].Value = "LUCKY";
                            oWsUna.Cells["AA1:AD1"].Value = "VENTAS";
                            oWsUna.Cells["AE1:AI1"].Value = "TRADE MARKETING";
                            oWsUna.Cells["AJ1:AW1"].Value = "LUCKY";
                            oWsUna.Cells["AX1:AY1"].Value = "TRADE MARKETING";
                            #endregion

                            #region Valor a celdas individuales
                            //Dando valor a celdas individuales
                            //COMIENZA PRIMERA PARTE VENTAS
                            oWsUna.Cells["A2"].Value = "USUARIO DE SOLICITUD";
                            oWsUna.Cells["B2"].Value = "FECHA DE SOLICITUD";
                            oWsUna.Cells["C2"].Value = "PUNTO DE VENTA";
                            oWsUna.Cells["D2"].Value = "CODIGO DE PDV";
                            oWsUna.Cells["E2"].Value = "DEPARTAMENTO";
                            oWsUna.Cells["F2"].Value = "PROVINCIA";
                            oWsUna.Cells["G2"].Value = "DISTRITO";
                            oWsUna.Cells["H2"].Value = "DIRECCION";
                            oWsUna.Cells["I2"].Value = "DISTRIBUIDORA";
                            oWsUna.Cells["J2"].Value = "ZONA";
                            oWsUna.Cells["K2"].Value = "RUC";
                            oWsUna.Cells["L2"].Value = "CELULAR";
                            oWsUna.Cells["M2"].Value = "CONTACTO";
                            oWsUna.Cells["N2"].Value = "MARCA";
                            oWsUna.Cells["O2"].Value = "TIPO";
                            oWsUna.Cells["P2"].Value = "ESTADO DE SOLICITUD";
                            //TERMINA PRIMERA PARTE VENTAS
                            //COMIENZA PRIMERA PARTE TRADE
                            oWsUna.Cells["Q2"].Value = "ESTADO DE SOLICITUD";
                            oWsUna.Cells["R2"].Value = "FECHA DE APROBACION";
                            oWsUna.Cells["S2"].Value = "COMENTARIO";
                            //TERMINA PRIMERA PARTE TRADE
                            //COMIENZA PRIMERA PARTE LUCKYANALIS
                            oWsUna.Cells["T2"].Value = "FECHA DE VISITA";
                            oWsUna.Cells["U2"].Value = "PROVEEDOR";
                            oWsUna.Cells["V2"].Value = "COMENTARIO";
                            oWsUna.Cells["W2"].Value = "FOTOMONTAJE";
                            oWsUna.Cells["X2"].Value = "MONTO (S/.)";
                            oWsUna.Cells["Y2"].Value = "CODIGO";
                            oWsUna.Cells["Z2"].Value = "PRE-PRESUPUESTO";
                            //TERMINA PRIMERA PARTE LUCKYANALIS
                            //COMIENZA SEGUNDA PARTE VENTAS
                            oWsUna.Cells["AA2"].Value = "APROBACION DE FOTOMONTAJE";
                            oWsUna.Cells["AB2"].Value = "FECHA DE APROBACION";
                            oWsUna.Cells["AC2"].Value = "COMENTARIO";
                            oWsUna.Cells["AD2"].Value = "CARTA DE APROBACION";
                            //TERMINA SEGUNDA PARTE VENTAS
                            //COMIENZA SEGUNDA PARTE TRADE
                            oWsUna.Cells["AE2"].Value = "APROBACION DE PRE-PRESUPUESTO";
                            oWsUna.Cells["AF2"].Value = "FECHA DE APROBACION";
                            oWsUna.Cells["AG2"].Value = "ORDEN DE COMPRA";
                            oWsUna.Cells["AH2"].Value = "H.E.S.";
                            oWsUna.Cells["AI2"].Value = "COMENTARIO";
                            //TERMINA SEGUNDA PARTE TRADE
                            //COMIENZA SEGUNDA PARTE LUCKYANALIS
                            oWsUna.Cells["AJ2"].Value = "FECHA DE IMPLEMENTACION";
                            oWsUna.Cells["AK2"].Value = "PROGRAMACION Y STATUS";
                            oWsUna.Cells["AL2"].Value = "TIPO DE TRAMITE";
                            oWsUna.Cells["AM2"].Value = "TIEMPO DE PERMISO";
                            oWsUna.Cells["AN2"].Value = "FECHA DE INGRESO DE EXPEDIENTE O CARTA";
                            oWsUna.Cells["AO2"].Value = "RECIBO DE TRAMITE MUNICIPAL O CARTA";
                            oWsUna.Cells["AP2"].Value = "OBSERVACION";
                            oWsUna.Cells["AQ2"].Value = "FECHA DE ENTREGA DE PERMISO";
                            oWsUna.Cells["AR2"].Value = "FECHA DE CADUCIDAD DE PERMISO";
                            oWsUna.Cells["AS2"].Value = "PERMISO";
                            oWsUna.Cells["AT2"].Value = "MONTO (S/.)";
                            oWsUna.Cells["AU2"].Value = "CODIGO";
                            oWsUna.Cells["AV2"].Value = "PRESUPUESTO DE TRAMITE";
                            oWsUna.Cells["AW2"].Value = "REPORTE FOTOGRAFICO";
                            //TERMINA SEGUNDA PARTE LUCKYANALIS
                            //COMIENZA TERCERA PARTE TRADE
                            oWsUna.Cells["AX2"].Value = "VALIDACION DE FACHADA";
                            oWsUna.Cells["AY2"].Value = "FECHA DE VALIDACION";
                            //TERMINA TERCERA PARTE TRADE
                            oWsUna.Cells["AZ2"].Value = "NRO REGISTRO";
                            #endregion

                            #region Valor al Cuerpo de Celdas
                            _fila = 3;

                            foreach (E_Fachada_Unacem oBj in oWfUna)
                            {
                                //COMIENZA PRIMERA PARTE VENTAS
                                oWsUna.Cells[_fila, 1].Value = (oBj.P1_Usuario_Solicita).ToUpper();
                                oWsUna.Cells[_fila, 2].Value = (oBj.Fec_Solicitud).ToUpper();
                                oWsUna.Cells[_fila, 3].Value = (oBj.Nom_PDV).ToUpper();
                                oWsUna.Cells[_fila, 4].Value = (oBj.Codigo).ToUpper();
                                oWsUna.Cells[_fila, 5].Value = (oBj.Departamento).ToUpper();
                                oWsUna.Cells[_fila, 6].Value = (oBj.Provincia).ToUpper();
                                oWsUna.Cells[_fila, 7].Value = (oBj.Distrito).ToUpper();
                                oWsUna.Cells[_fila, 8].Value = (oBj.Direccion).ToUpper();
                                oWsUna.Cells[_fila, 9].Value = (oBj.Distribuidora).ToUpper();
                                oWsUna.Cells[_fila, 10].Value = (oBj.DescripcionZona).ToUpper();
                                oWsUna.Cells[_fila, 11].Value = (oBj.Num_RUC).ToUpper();
                                oWsUna.Cells[_fila, 12].Value = (oBj.Num_Cel).ToUpper();
                                oWsUna.Cells[_fila, 13].Value = (oBj.Contacto).ToUpper();
                                oWsUna.Cells[_fila, 14].Value = (oBj.DescripcionMarca).ToUpper();
                                oWsUna.Cells[_fila, 15].Value = (oBj.DescripcionTipoSolicitud).ToUpper();
                                oWsUna.Cells[_fila, 16].Value = (oBj.Est_Solicitud).ToUpper();
                                //TERMINA PRIMERA PARTE VENTAS
                                //COMIENZA PRIMERA PARTE TRADE
                                switch (oBj.Apro_Solicitud.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 17].Value = "APROBADO";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 17].Value = "DESAPROBADO";
                                        break;
                                }
                                oWsUna.Cells[_fila, 18].Value = (oBj.Fec_Apro_Solicitud).ToUpper();
                                oWsUna.Cells[_fila, 19].Value = (oBj.Comen_Apro_Solicitud).ToUpper();
                                //TERMINA PRIMERA PARTE TRADE
                                //COMENZA PRIMERA PARTE LUCKYANALIS
                                oWsUna.Cells[_fila, 20].Value = (oBj.P2_Fecha_Entrega_Foto).ToUpper();
                                oWsUna.Cells[_fila, 21].Value = (oBj.DescripcionProveedor).ToUpper();
                                oWsUna.Cells[_fila, 22].Value = (oBj.comen_expediente).ToUpper();
                                if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                                {
                                    oWsUna.Cells[_fila, 23].Value = "CON FOTO";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 23].Value = "";
                                }
                                oWsUna.Cells[_fila, 24].Value = (oBj.p3_monto_presupuesto).ToUpper();
                                oWsUna.Cells[_fila, 25].Value = (oBj.p3_codigo_presupuesto).ToUpper();
                                if (oBj.Url_Presupuesto != "" && oBj.Url_Presupuesto != null)
                                {
                                    oWsUna.Cells[_fila, 26].Value = "CON PRESUPUESTO";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 26].Value = "";
                                }
                                //TERMINA PRIMERA PARTE LUCKYANALIS
                                //COMIENZA SEGUNDA PARTE VENTAS
                                if (oBj.Url_Gantt_Foto != null && oBj.Url_Gantt_Foto != "" && oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje != "")
                                {
                                    switch (oBj.Apro_Fotomontaje.ToUpper())
                                    {
                                        case "TRUE":
                                            oWsUna.Cells[_fila, 27].Value = "APROBADO";
                                            break;
                                        case "FALSE":
                                            oWsUna.Cells[_fila, 27].Value = "DESAPROBADO";
                                            break;
                                    }
                                }
                                oWsUna.Cells[_fila, 28].Value = (oBj.Fec_Apro_Fotomonataje).ToUpper();
                                oWsUna.Cells[_fila, 29].Value = (oBj.Comen_Apro_Fotomontaje).ToUpper();
                                if (oBj.Url_Fotomontaje != null && oBj.Url_Fotomontaje.Length > 0)
                                {
                                    if (oBj.Url_Carta_Fotomontaje != null && oBj.Url_Carta_Fotomontaje.Replace(" ", "").Length > 0)
                                    {
                                        oWsUna.Cells[_fila, 30].Value = "CON CARTA";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 30].Value = "FALTA CARGAR CARTA";
                                    }
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 30].Value = "";
                                }
                                //TERMINA SEGUNDA PARTE VENTAS
                                //COMIENZA SEGUNDA PARTE TRADE
                                switch (oBj.Apro_pre_Presu.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 31].Value = "APROBADO";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 31].Value = "DESAPROBADO";
                                        break;
                                }
                                oWsUna.Cells[_fila, 32].Value = (oBj.Fec_Apro_pre_Presu).ToUpper();
                                oWsUna.Cells[_fila, 33].Value = (oBj.Orden_Compra).ToUpper();
                                oWsUna.Cells[_fila, 34].Value = (oBj.HES).ToUpper();
                                oWsUna.Cells[_fila, 35].Value = (oBj.Comen_Apro_Pre_Presu).ToUpper();
                                //TERMINA SEGUNDA PARTE TRADE
                                //COMIENZA SEGUNDA PARTE LUCKYANALIS
                                if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "" && oBj.Apro_Fotomontaje.ToUpper() == "TRUE")
                                {
                                    oWsUna.Cells[_fila, 36].Value = (oBj.fecha_implementacion).ToUpper();
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 36].Value = "";
                                }
                                string _vcolor = "";
                                if (oBj.Cantidad == 0)
                                {
                                    _vcolor = "PENDIENTES";
                                }
                                else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                                {
                                    _vcolor = "ALGUNOS PENDIENTES";
                                }
                                else if (oBj.Cantidad == 10)
                                {
                                    _vcolor = "REVISADO";
                                }
                                if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                                {
                                    oWsUna.Cells[_fila, 37].Value = _vcolor;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 37].Value = _vcolor;
                                }
                                oWsUna.Cells[_fila, 38].Value = (oBj.DescripcionTipoTramite).ToUpper();
                                oWsUna.Cells[_fila, 39].Value = (oBj.DescripcionTiempoPermiso).ToUpper();
                                if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                                {
                                    if (oBj.Fec_ingre_expe != null && oBj.Fec_ingre_expe != "" && oBj.Fec_ingre_expe != "01/01/1900" && oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                    {
                                        oWsUna.Cells[_fila, 40].Value = (oBj.Fec_ingre_expe).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 40].Value = "";
                                    }

                                    if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                    {
                                        oWsUna.Cells[_fila, 41].Value = "CON RECIBO";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 41].Value = "";
                                    }
                                    oWsUna.Cells[_fila, 42].Value = (oBj.Obs_Tramite).ToUpper();
                                    if (oBj.Fec_Entre_Licencia != null && oBj.Fec_Entre_Licencia != "" && oBj.Fec_Entre_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                    {
                                        oWsUna.Cells[_fila, 43].Value = (oBj.Fec_Entre_Licencia).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 43].Value = "";
                                    }
                                    if (oBj.Fec_Cadu_Licencia != null && oBj.Fec_Cadu_Licencia != "" && oBj.Fec_Cadu_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                    {
                                        oWsUna.Cells[_fila, 44].Value = (oBj.Fec_Cadu_Licencia).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 44].Value = "";
                                    }
                                    if (oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                    {
                                        oWsUna.Cells[_fila, 45].Value = "CON PERMISO";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 45].Value = "";
                                    }
                                    oWsUna.Cells[_fila, 46].Value = (oBj.p3_monto_presupuesto_final).ToUpper();
                                    oWsUna.Cells[_fila, 47].Value = (oBj.p3_codigo_presupuesto_final).ToUpper();
                                    if (oBj.Url_Presu_Final != null && oBj.Url_Presu_Final != "")
                                    {
                                        oWsUna.Cells[_fila, 48].Value = "CON PRESUPUESTO DE TRAMITE";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 48].Value = "";
                                    }
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 40].Value = "";
                                    oWsUna.Cells[_fila, 41].Value = "";
                                    oWsUna.Cells[_fila, 42].Value = "";
                                    oWsUna.Cells[_fila, 43].Value = "";
                                    oWsUna.Cells[_fila, 44].Value = "";
                                    oWsUna.Cells[_fila, 45].Value = "";
                                    oWsUna.Cells[_fila, 46].Value = "";
                                    oWsUna.Cells[_fila, 47].Value = "";
                                    oWsUna.Cells[_fila, 48].Value = "";
                                }
                                if (oBj.Url_Report_Fotografico != null && oBj.Url_Report_Fotografico != "")
                                {
                                    oWsUna.Cells[_fila, 49].Value = "CON REPORTE FOTOGRAFICO";
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 49].Value = "";
                                }
                                //TERMINA SEGUNDA PARTE LUCKYANALIS
                                //COMIENZA TERCERA PARTE TRADE
                                switch (oBj.Vali_Fechada_Imple.ToUpper())
                                {
                                    case "TRUE":
                                        oWsUna.Cells[_fila, 50].Value = "APROBADO";
                                        break;
                                    case "FALSE":
                                        oWsUna.Cells[_fila, 50].Value = "DESAPROBADO";
                                        break;
                                }
                                oWsUna.Cells[_fila, 51].Value = (oBj.Fec_Vali_Fachada_Imple).ToUpper();
                                //TERMINA TERCERA PARTE TRADE
                                if (Convert.ToInt32(oBj.Id_Registro) < 10)
                                {
                                    oWsUna.Cells[_fila, 52].Value = "WF_0000" + Convert.ToInt32(oBj.Id_Registro);
                                }
                                else
                                {
                                    if (Convert.ToInt32(oBj.Id_Registro) >= 10 && Convert.ToInt32(oBj.Id_Registro) < 100)
                                    {
                                        oWsUna.Cells[_fila, 52].Value = "WF_000" + Convert.ToInt32(oBj.Id_Registro);
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(oBj.Id_Registro) >= 100 && Convert.ToInt32(oBj.Id_Registro) < 1000)
                                        {
                                            oWsUna.Cells[_fila, 52].Value = "WF_00" + Convert.ToInt32(oBj.Id_Registro);
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(oBj.Id_Registro) >= 1000 && Convert.ToInt32(oBj.Id_Registro) < 10000)
                                            {
                                                oWsUna.Cells[_fila, 52].Value = "WF_0" + Convert.ToInt32(oBj.Id_Registro);
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(oBj.Id_Registro) >= 10000)
                                                {
                                                    oWsUna.Cells[_fila, 52].Value = "WF_" + Convert.ToInt32(oBj.Id_Registro);
                                                }
                                                else
                                                {
                                                    oWsUna.Cells[_fila, 52].Value = "";
                                                }
                                            }
                                        }
                                    }
                                }



                                for (int i = 1; i <= columnsuser3; i++)
                                {
                                    oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                    //ajustando texto
                                    oWsUna.Cells[_fila, i].Style.WrapText = true;
                                }



                                _fila++;
                            }
                            #endregion

                            #region Estilos
                            //Formato Cabecera 1
                            oWsUna.Row(1).Height = 25;
                            oWsUna.Cells[2, 1, 2, columnsuser3].Style.WrapText = true;
                            oWsUna.Row(1).Style.Font.Bold = true;
                            oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                            oWsUna.Cells["A1:AZ1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                            oWsUna.Cells["A1:AZ1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                            //Formato Cabecera 2
                            oWsUna.Row(2).Height = 45;
                            oWsUna.Row(2).Style.Font.Bold = true;
                            oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                            oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                            oWsUna.Cells["A2:AZ2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                            oWsUna.Cells["A2:AZ2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                            oWsUna.SelectedRange["A2:AZ2"].AutoFilter = true;


                            //Formato ambas cabeceras
                            oWsUna.Cells["A1:AZ2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AZ2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AZ2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AZ2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;
                            #endregion

                            #region Ancho de Columnas
                            //Formato por columnas
                            //Ventas
                            oWsUna.Column(1).Width = 29;
                            oWsUna.Column(2).Width = 13;
                            oWsUna.Column(3).Width = 38;
                            oWsUna.Column(4).Width = 18;
                            oWsUna.Column(5).Width = 18;
                            oWsUna.Column(6).Width = 13;
                            oWsUna.Column(7).Width = 18;
                            oWsUna.Column(8).Width = 55;
                            oWsUna.Column(9).Width = 17;
                            oWsUna.Column(10).Width = 14;
                            oWsUna.Column(11).Width = 13;
                            oWsUna.Column(12).Width = 11;
                            oWsUna.Column(13).Width = 21;
                            oWsUna.Column(14).Width = 12;
                            oWsUna.Column(15).Width = 35;
                            oWsUna.Column(16).Width = 13;
                            //trade marketing
                            oWsUna.Column(17).Width = 14;//Estado Solicitud
                            oWsUna.Column(18).Width = 15;//Fecha de Aprobacion
                            oWsUna.Column(19).Width = 44;//Comentario
                            //lucky
                            oWsUna.Column(20).Width = 12;//Fecha de Visita
                            oWsUna.Column(21).Width = 13;//Proveedor
                            oWsUna.Column(22).Width = 44;//Comentario
                            oWsUna.Column(23).Width = 17;//Fotomontaje
                            oWsUna.Column(24).Width = 14;//Monto (S/.)
                            oWsUna.Column(25).Width = 9;//Codigo
                            oWsUna.Column(26).Width = 18;//Pre-Presupuesto
                            //ventas
                            oWsUna.Column(27).Width = 18;//Aprobacion de Fotomontaje
                            oWsUna.Column(28).Width = 15;//Fecha de Aprobacion
                            oWsUna.Column(29).Width = 44;//Comentario
                            oWsUna.Column(30).Width = 20;//Carta de Aprobacion
                            //trade marketing
                            oWsUna.Column(31).Width = 21;//Aprobacion de Pre-Presupuesto
                            oWsUna.Column(32).Width = 15;//Fecha de Aprobacion
                            oWsUna.Column(33).Width = 13;//Orden de Compra
                            oWsUna.Column(34).Width = 13;//H.E.S.
                            oWsUna.Column(35).Width = 44;//Comentario
                            //lucky
                            oWsUna.Column(36).Width = 19;//Fecha de Implementacion
                            oWsUna.Column(37).Width = 21;//Status Documentario
                            oWsUna.Column(38).Width = 14;//Tipo de Tramite
                            oWsUna.Column(39).Width = 12;//Tiempo de Permiso
                            oWsUna.Column(40).Width = 14;//Fecha de Ingreso de Expediente o Carta
                            oWsUna.Column(41).Width = 14;//Recibo de Tramite Municipal o Carta
                            oWsUna.Column(42).Width = 44;//Observacion
                            oWsUna.Column(43).Width = 14;//Fecha de Entrega de Permiso
                            oWsUna.Column(44).Width = 14;//Fecha de Caducidad de permiso
                            oWsUna.Column(45).Width = 14;//Permiso
                            oWsUna.Column(46).Width = 14;//Monto (S/.)
                            oWsUna.Column(47).Width = 14;//Codigo
                            oWsUna.Column(48).Width = 23;//Presupuetso de Tramite
                            oWsUna.Column(49).Width = 26;//Reporte Fotografico
                            //trade marketing
                            oWsUna.Column(50).Width = 15;//Validacion de Fachada
                            oWsUna.Column(51).Width = 14;//Fecha de Validacion
                            oWsUna.Column(52).Width = 17;//Fecha de Validacion

                            #endregion

                            oWsUna.View.FreezePanes(3, 1);


                            oEx.Save();

                            #endregion
                        }
                        else if (Grupo == 4)
                        {
                            #region Perfil 4

                            Excel.ExcelWorksheet oWsUna = oEx.Workbook.Worksheets.Add("Reporte Work Flow Tramite");

                            #region Combinacion de celdas de cabecera
                            //Combinacion de celdas
                            oWsUna.Cells["A1:P1"].Merge = true;
                            oWsUna.Cells["Q1:Z1"].Merge = true;
                            #endregion

                            #region Valor a Celdas Combinadas
                            //Dando valor a celdas combinadas
                            oWsUna.Cells["A1:P1"].Value = "VENTAS";
                            oWsUna.Cells["Q1:Z1"].Value = "LUCKY";
                            oWsUna.Cells["AA1"].Value = "";
                            #endregion

                            #region Valor a celdas Individuales
                            //Dando valor a celdas individuales
                            //COMIENZA PRIMERA PARTE VENTAS
                            oWsUna.Cells["A2"].Value = "USUARIO DE SOLICITUD";
                            oWsUna.Cells["B2"].Value = "FECHA DE SOLICITUD";
                            oWsUna.Cells["C2"].Value = "PUNTO DE VENTA";
                            oWsUna.Cells["D2"].Value = "CODIGO DE PDV";
                            oWsUna.Cells["E2"].Value = "DEPARTAMENTO";
                            oWsUna.Cells["F2"].Value = "PROVINCIA";
                            oWsUna.Cells["G2"].Value = "DISTRITO";
                            oWsUna.Cells["H2"].Value = "DIRECCION";
                            oWsUna.Cells["I2"].Value = "DISTRIBUIDORA";
                            oWsUna.Cells["J2"].Value = "ZONA";
                            oWsUna.Cells["K2"].Value = "RUC";
                            oWsUna.Cells["L2"].Value = "CELULAR";
                            oWsUna.Cells["M2"].Value = "CONTACTO";
                            oWsUna.Cells["N2"].Value = "MARCA";
                            oWsUna.Cells["O2"].Value = "TIPO";
                            oWsUna.Cells["P2"].Value = "ESTADO DE SOLICITUD";
                            //TERMINA PRIMERA PARTE VENTAS
                            //COMIENZA PRIMERA PARTE LUCKYANALIS
                            oWsUna.Cells["Q2"].Value = "FECHA DE IMPLEMENTACION";
                            oWsUna.Cells["R2"].Value = "PROGRAMACION Y STATUS";
                            oWsUna.Cells["S2"].Value = "TIPO DE TRAMITE";
                            oWsUna.Cells["T2"].Value = "TIEMPO DE PERMISO";
                            oWsUna.Cells["U2"].Value = "FECHA DE INGRESO DE EXPEDIENTE O CARTA";
                            oWsUna.Cells["V2"].Value = "RECIBO DE TRAMITE MUNICIPAL O CARTA";
                            oWsUna.Cells["W2"].Value = "OBSERVACION";
                            oWsUna.Cells["X2"].Value = "FECHA DE ENTREGA DE PERMISO";
                            oWsUna.Cells["Y2"].Value = "FECHA DE CADUCIDAD DE PERMISO";
                            oWsUna.Cells["Z2"].Value = "PERMISO";
                            //TERMINA PRIMERA PARTE LUCKYANALIS
                            oWsUna.Cells["AA2"].Value = "NRO REGISTRO";
                            #endregion

                            #region Valor al cuerpo de Celdas
                            _fila = 3;

                            foreach (E_Fachada_Unacem oBj in oWfUna)
                            {
                                //COMIENZA PRIMERA PARTE VENTAS
                                oWsUna.Cells[_fila, 1].Value = (oBj.P1_Usuario_Solicita).ToUpper();
                                oWsUna.Cells[_fila, 2].Value = (oBj.Fec_Solicitud).ToUpper();
                                oWsUna.Cells[_fila, 3].Value = (oBj.Nom_PDV).ToUpper();
                                oWsUna.Cells[_fila, 4].Value = (oBj.Codigo).ToUpper();
                                oWsUna.Cells[_fila, 5].Value = (oBj.Departamento).ToUpper();
                                oWsUna.Cells[_fila, 6].Value = (oBj.Provincia).ToUpper();
                                oWsUna.Cells[_fila, 7].Value = (oBj.Distrito).ToUpper();
                                oWsUna.Cells[_fila, 8].Value = (oBj.Direccion).ToUpper();
                                oWsUna.Cells[_fila, 9].Value = (oBj.Distribuidora).ToUpper();
                                oWsUna.Cells[_fila, 10].Value = (oBj.DescripcionZona).ToUpper();
                                oWsUna.Cells[_fila, 11].Value = (oBj.Num_RUC).ToUpper();
                                oWsUna.Cells[_fila, 12].Value = (oBj.Num_Cel).ToUpper();
                                oWsUna.Cells[_fila, 13].Value = (oBj.Contacto).ToUpper();
                                oWsUna.Cells[_fila, 14].Value = (oBj.DescripcionMarca).ToUpper();
                                oWsUna.Cells[_fila, 15].Value = (oBj.DescripcionTipoSolicitud).ToUpper();
                                oWsUna.Cells[_fila, 16].Value = (oBj.Est_Solicitud).ToUpper();
                                //TERMINA PRIMERA PARTE VENTAS
                                //COMIENZA PRIMERA PARTE LUCKYANALIS
                                if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                                {
                                    oWsUna.Cells[_fila, 17].Value = (oBj.fecha_implementacion).ToUpper();
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 17].Value = "";
                                }
                                string _vcolor = "";
                                if (oBj.Cantidad == 0)
                                {
                                    _vcolor = "PENDIENTES";
                                }
                                else if (oBj.Cantidad > 0 && oBj.Cantidad < 10)
                                {
                                    _vcolor = "ALGUNOS PENDIENTES";
                                }
                                else if (oBj.Cantidad == 10)
                                {
                                    _vcolor = "REVISADO";
                                }
                                if (oBj.fecha_implementacion != null && oBj.fecha_implementacion != "")
                                {
                                    oWsUna.Cells[_fila, 18].Value = _vcolor;
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 18].Value = _vcolor;
                                }
                                oWsUna.Cells[_fila, 19].Value = (oBj.DescripcionTipoTramite).ToUpper();
                                oWsUna.Cells[_fila, 20].Value = (oBj.DescripcionTiempoPermiso).ToUpper();
                                if (oBj.Apro_pre_Presu.ToUpper() == "TRUE")
                                {
                                    if (oBj.Fec_ingre_expe != null && oBj.Fec_ingre_expe != "" && oBj.Fec_ingre_expe != "01/01/1900" && oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                    {
                                        oWsUna.Cells[_fila, 21].Value = (oBj.Fec_ingre_expe).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 21].Value = "__________";
                                    }
                                    if (oBj.Url_Rec_Tramite_Muni != null && oBj.Url_Rec_Tramite_Muni != "")
                                    {
                                        oWsUna.Cells[_fila, 22].Value = "CON RECIBO";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 22].Value = "__________";
                                    }
                                    if (oBj.Obs_Tramite != null && oBj.Obs_Tramite != "")
                                    {
                                        oWsUna.Cells[_fila, 23].Value = (oBj.Obs_Tramite).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 23].Value = "__________";
                                    }
                                    if (oBj.Fec_Entre_Licencia != null && oBj.Fec_Entre_Licencia != "" && oBj.Fec_Entre_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                    {
                                        oWsUna.Cells[_fila, 24].Value = (oBj.Fec_Entre_Licencia).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 24].Value = "__________";
                                    }
                                    if (oBj.Fec_Cadu_Licencia != null && oBj.Fec_Cadu_Licencia != "" && oBj.Fec_Cadu_Licencia != "01/01/1900" && oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                    {
                                        oWsUna.Cells[_fila, 25].Value = (oBj.Fec_Cadu_Licencia).ToUpper();
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 25].Value = "__________";
                                    }
                                    if (oBj.Url_Licencia != null && oBj.Url_Licencia != "")
                                    {
                                        oWsUna.Cells[_fila, 26].Value = "CON PERMISO";
                                    }
                                    else
                                    {
                                        oWsUna.Cells[_fila, 26].Value = "__________";
                                    }
                                }
                                else
                                {
                                    oWsUna.Cells[_fila, 21].Value = "";
                                    oWsUna.Cells[_fila, 22].Value = "";
                                    oWsUna.Cells[_fila, 23].Value = "";
                                    oWsUna.Cells[_fila, 24].Value = "";
                                    oWsUna.Cells[_fila, 25].Value = "";
                                    oWsUna.Cells[_fila, 26].Value = "";
                                }
                                if (Convert.ToInt32(oBj.Id_Registro) < 10)
                                {
                                    oWsUna.Cells[_fila, 27].Value = "WF_0000" + Convert.ToInt32(oBj.Id_Registro);
                                }
                                else
                                {
                                    if (Convert.ToInt32(oBj.Id_Registro) >= 10 && Convert.ToInt32(oBj.Id_Registro) < 100)
                                    {
                                        oWsUna.Cells[_fila, 27].Value = "WF_000" + Convert.ToInt32(oBj.Id_Registro);
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(oBj.Id_Registro) >= 100 && Convert.ToInt32(oBj.Id_Registro) < 1000)
                                        {
                                            oWsUna.Cells[_fila, 27].Value = "WF_00" + Convert.ToInt32(oBj.Id_Registro);
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(oBj.Id_Registro) >= 1000 && Convert.ToInt32(oBj.Id_Registro) < 10000)
                                            {
                                                oWsUna.Cells[_fila, 27].Value = "WF_0" + Convert.ToInt32(oBj.Id_Registro);
                                            }
                                            else
                                            {
                                                if (Convert.ToInt32(oBj.Id_Registro) >= 10000)
                                                {
                                                    oWsUna.Cells[_fila, 27].Value = "WF_" + Convert.ToInt32(oBj.Id_Registro);
                                                }
                                                else
                                                {
                                                    oWsUna.Cells[_fila, 27].Value = "";
                                                }
                                            }
                                        }
                                    }
                                }




                                for (int i = 1; i <= columnsuser4; i++)
                                {
                                    oWsUna.Cells[_fila, i].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                                    oWsUna.Cells[_fila, i].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;

                                    //ajustando texto
                                    oWsUna.Cells[_fila, i].Style.WrapText = true;
                                }



                                _fila++;
                            }
                            #endregion

                            #region Estilos
                            //Formato Cabecera 1
                            oWsUna.Row(1).Height = 25;
                            oWsUna.Row(1).Style.Font.Bold = true;
                            oWsUna.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oWsUna.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#3E7BD0");
                            oWsUna.Cells["A1:AA1"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                            oWsUna.Cells["A1:AA1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.None);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Thin);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Medium);
                            //oWsUna.Cells["A1:AB1"].Style.Border.BorderAround(Style.ExcelBorderStyle.Hair);

                            //Formato Cabecera 2
                            oWsUna.Row(2).Height = 45;
                            oWsUna.Row(2).Style.Font.Bold = true;
                            oWsUna.Row(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left;
                            oWsUna.Row(2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#6DB5CD");
                            oWsUna.Cells["A2:AA2"].Style.Fill.PatternType = Style.ExcelFillStyle.Solid;
                            oWsUna.Cells["A2:AA2"].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                            oWsUna.SelectedRange["A2:AA2"].AutoFilter = true;


                            //Formato ambas cabeceras
                            oWsUna.Cells["A1:AA2"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AA2"].Style.Border.Left.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AA2"].Style.Border.Right.Style = Style.ExcelBorderStyle.Thin;
                            oWsUna.Cells["A1:AA2"].Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin;
                            #endregion

                            #region Ancho de Columnas
                            //Formato por columnas
                            //Ventas
                            oWsUna.Column(1).Width = 29;
                            oWsUna.Column(2).Width = 13;
                            oWsUna.Column(3).Width = 38;
                            oWsUna.Column(4).Width = 18;
                            oWsUna.Column(5).Width = 18;
                            oWsUna.Column(6).Width = 13;
                            oWsUna.Column(7).Width = 18;
                            oWsUna.Column(8).Width = 55;
                            oWsUna.Column(9).Width = 17;
                            oWsUna.Column(10).Width = 14;
                            oWsUna.Column(11).Width = 13;
                            oWsUna.Column(12).Width = 11;
                            oWsUna.Column(13).Width = 21;
                            oWsUna.Column(14).Width = 12;
                            oWsUna.Column(15).Width = 35;
                            oWsUna.Column(16).Width = 13;
                            //lucky
                            oWsUna.Column(17).Width = 26;
                            oWsUna.Column(18).Width = 23;
                            oWsUna.Column(19).Width = 23;
                            oWsUna.Column(20).Width = 23;
                            oWsUna.Column(21).Width = 32;
                            oWsUna.Column(22).Width = 29;
                            oWsUna.Column(23).Width = 35;
                            oWsUna.Column(24).Width = 29;
                            oWsUna.Column(25).Width = 31;
                            oWsUna.Column(26).Width = 12;
                            oWsUna.Column(27).Width = 17;
                            #endregion

                            oWsUna.View.FreezePanes(3, 1);


                            oEx.Save();

                            #endregion
                        }
                    }
                }
                else
                {
                    _fileServer = "0";
                }
                #endregion
            }
            return Json(new { Archivo = _fileServer });
        }
        #endregion
    }
}