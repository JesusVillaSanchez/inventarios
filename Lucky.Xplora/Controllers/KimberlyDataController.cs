﻿using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.AlicorpMulticategoriaCanje;
using Lucky.Xplora.Models.Kimberly.Data;
using Lucky.Xplora.Security;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;

namespace Lucky.Xplora.Controllers
{
    public class KimberlyDataController : Controller
    {
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        string LocalFoto = Convert.ToString(ConfigurationManager.AppSettings["LocalFoto"]);
        string XploraFoto = Convert.ToString(ConfigurationManager.AppSettings["XploraFoto"]);

        #region Kimberly - Quiebre
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-08
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        [CustomAuthorize]
        public ActionResult Quiebre()
        {
            Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-08
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        [CustomAuthorize]
        public ActionResult Quiebre(string __a, string __b)
        {
            int fila = 0;
            int columna = 0;
            int cuentaFila = 0;
            int cuentaColumna = 0;
            int resultado = 0;

            string nombre = "";
            string destino = "";
            string id = "";
            string gie = "";
            string pdv = "";
            string fecha = "";
            string motivo = "";
            string validado = "";
            string producto = "";

            Parametro oParametro = new Parametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            if (oParametro.opcion == 0 || oParametro.opcion == 1)
            {
                #region Gestor & Categorias
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyQuiebre().Lista(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (oParametro.opcion == 2)
            {
                #region Descarga Quiebres
                KimberlyQuiebre oQuiebre = new KimberlyQuiebre().Quiebre(oParametro);

                nombre = DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                destino = Path.Combine(LocalTemp, nombre);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(destino)))
                {
                    Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets.Add("Quiebre");

                    oWs.Cells[1, 1].Value = "Gestor: ID";
                    oWs.Cells[1, 2].Value = "Gestor";
                    oWs.Cells[1, 3].Value = "Supervisor: ID";
                    oWs.Cells[1, 4].Value = "Supervisor";
                    oWs.Cells[1, 5].Value = "Fecha";
                    oWs.Cells[1, 6].Value = "Cadena: ID";
                    oWs.Cells[1, 7].Value = "Cadena";
                    oWs.Cells[1, 8].Value = "PDV: Codigo";
                    oWs.Cells[1, 9].Value = "P.D.V.";

                    fila = 2;

                    foreach (KimberlyQuiebreInformacion vQuiebre in oQuiebre.quiebre) {
                        oWs.Cells[fila, 1].Value = vQuiebre.gie_id;
                        oWs.Cells[fila, 2].Value = vQuiebre.gie_nombre;
                        oWs.Cells[fila, 3].Value = vQuiebre.sup_id;
                        oWs.Cells[fila, 4].Value = vQuiebre.sup_nombre;
                        oWs.Cells[fila, 5].Value = vQuiebre.fecha;
                        oWs.Cells[fila, 6].Value = vQuiebre.cad_id;
                        oWs.Cells[fila, 7].Value = vQuiebre.cad_descripcion;
                        oWs.Cells[fila, 8].Value = vQuiebre.pdv_codigo;
                        oWs.Cells[fila, 9].Value = vQuiebre.pdv_descripcion;

                        columna = 10;

                        foreach (KimberlyQuiebreProducto vProducto in vQuiebre.producto) {
                            if (oQuiebre.quiebre.First() == vQuiebre)
                            {
                                oWs.Cells[1, columna].Value = vProducto.pro_sku;
                                oWs.Cells[1, columna + 1].Value = vProducto.pro_descripcion;

                                oWs.Column(columna).Hidden = true;
                                oWs.Cells[1, columna + 1, 1, columna + 2].Merge = true;
                            }

                            oWs.Cells[fila, columna].Value = (vProducto.cab_id.Length > 0 ? vProducto.cab_id + "_" + vProducto.det_id : "");
                            oWs.Cells[fila, columna + 1].Value = vProducto.validado;
                            oWs.Cells[fila, columna + 2].Value = vProducto.motivo;

                            oWs.Cells[fila, columna + 1].Style.Locked = false;
                            oWs.Cells[fila, columna + 2].Style.Locked = false;

                            columna += 3;
                        }

                        oWs.Column(1).Hidden = true;
                        oWs.Column(3).Hidden = true;
                        oWs.Column(6).Hidden = true;
                        oWs.Column(8).Hidden = true;

                        oWs.Row(1).Height = 35;
                        oWs.Row(1).Style.WrapText = true;
                        oWs.Row(1).Style.Font.Bold = true;
                        oWs.Row(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oWs.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;
                        oWs.Row(1).Style.Font.Size = 9;

                        oWs.Column(2).AutoFit();
                        oWs.Column(4).AutoFit();
                        oWs.Column(5).AutoFit();
                        oWs.Column(7).AutoFit();
                        oWs.Column(9).AutoFit(20, 30);

                        oWs.View.FreezePanes(2, 10);

                        oWs.Protection.AllowInsertRows = false;
                        oWs.Protection.AllowInsertColumns = false;
                        oWs.Protection.AllowDeleteColumns = false;
                        oWs.Protection.AllowPivotTables = false;
                        oWs.Protection.AllowSelectLockedCells = true;
                        oWs.Protection.AllowSelectUnlockedCells = true;
                        oWs.Protection.AllowAutoFilter = true;
                        oWs.Protection.AllowSort = false;
                        oWs.Protection.IsProtected = true;
                        oWs.Protection.SetPassword("juanjolucerog");

                        fila++;
                    }

                    oEx.Save();
                }

                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"" + nombre + "\" }",
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (oParametro.opcion == 3)
            {
                #region Carga Quiebres
                destino = Path.Combine(LocalTemp, __b);

                using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(destino)))
                {
                    Excel.ExcelWorksheet oWs = oEx.Workbook.Worksheets["Quiebre"];

                    List<KimberlyQuiebreTemporal> lTemporal = new List<KimberlyQuiebreTemporal>();

                    cuentaFila = oWs.Dimension.End.Row;
                    cuentaColumna = oWs.Dimension.End.Column;

                    for (fila = 2; fila <= cuentaFila; fila++)
                    {
                        for (columna = 10; columna <= cuentaColumna; columna += 3)
                        {
                            KimberlyQuiebreTemporal oTemporal = new KimberlyQuiebreTemporal();

                            id = Convert.ToString(oWs.Cells[fila, columna].Value);
                            gie = Convert.ToString(oWs.Cells[fila, 1].Value);
                            pdv = Convert.ToString(oWs.Cells[fila, 8].Value);
                            fecha = Convert.ToString(oWs.Cells[fila, 5].Value);
                            producto = Convert.ToString(oWs.Cells[1, columna].Value);
                            validado = Convert.ToString(oWs.Cells[fila, columna + 1].Value);
                            motivo = Convert.ToString(oWs.Cells[fila, columna + 2].Value);

                            if (id.Length > 0)
                            {
                                #region Actualiza & Elimina
                                if (validado.Length > 0 || motivo.Length > 0)
                                {
                                    oTemporal.cab_id = Convert.ToString(id.Split('_')[0]);
                                    oTemporal.det_id = Convert.ToString(id.Split('_')[1]);
                                    oTemporal.validado = validado;
                                    oTemporal.motivo = motivo;
                                    oTemporal.crud = "u";
                                    oTemporal.archivo = __b;
                                }
                                else
                                {
                                    oTemporal.cab_id = Convert.ToString(id.Split('_')[0]);
                                    oTemporal.det_id = Convert.ToString(id.Split('_')[1]);
                                    oTemporal.crud = "d";
                                    oTemporal.archivo = __b;
                                }

                                lTemporal.Add(oTemporal);
                                #endregion
                            }
                            else
                            {
                                #region Inserta
                                if (validado.Length > 0 || motivo.Length > 0)
                                {
                                    oTemporal.gie_id = gie;
                                    oTemporal.pdv_codigo = pdv;
                                    oTemporal.fecha = fecha;
                                    oTemporal.pro_sku = producto;
                                    oTemporal.validado = validado;
                                    oTemporal.motivo = motivo;
                                    oTemporal.crud = "i";
                                    oTemporal.archivo = __b;

                                    lTemporal.Add(oTemporal);
                                }
                                #endregion
                            }
                        }
                    }

                    if (lTemporal.Count > 0) {
                        resultado = new KimberlyQuiebre().Carga(new Parametro() { opcion = 3, parametro = MvcApplication._Serialize(lTemporal) });
                    }
                }

                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"" + resultado + "\" }",
                    ContentType = "application/json"
                }.Content);
                #endregion
            }

            return View();
        }
        #endregion

        #region Kimberly - Evaluacion
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-16
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        [CustomAuthorize]
        public ActionResult Evaluacion()
        {
            ViewBag.CamCodigo = Convert.ToString(Request["_a"]);
            Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-16
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        [CustomAuthorize]
        public ActionResult Evaluacion(string __a, string __b)
        {
            Parametro oParametro = new Parametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            if (oParametro.opcion == 0 || oParametro.opcion == 1 || oParametro.opcion == 9 || oParametro.opcion == 2)
            {
                #region Categoria & Pdv & Motivo
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyEvaluacion().Lista(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (oParametro.opcion == 3)
            {
                #region Encuesta
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyEvaluacion().Encuesta(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (oParametro.opcion == 4)
            {
                #region InsertaActualiza
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyEvaluacion().InsertaActualiza(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
        

            return View();
        }
        #endregion

        #region Kimberly - OSA
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-19
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        [CustomAuthorize]
        public ActionResult Osa()
        {
            ViewBag.CamCodigo = Convert.ToString(Request["_a"]);
            Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-16
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        [CustomAuthorize]
        public ActionResult Osa(string __a, string __b)
        {
            Parametro oParametro = new Parametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            if (oParametro.opcion == 5)
            {
                #region OSA
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyOsa().Lista(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (oParametro.opcion == 6)
            {
                #region OSA - InsertaActualiza
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyOsa().InsertaActualiza(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }

            return View();
        }
        #endregion

        #region Kimberly - Arriendo
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-19
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        [CustomAuthorize]
        public ActionResult Arriendo()
        {
            ViewBag.CamCodigo = Convert.ToString(Request["_a"]);
            Utilitario.registrar_visita(Request.QueryString["_b"]);

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-16
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        [CustomAuthorize]
        public ActionResult Arriendo(string __a, string __b)
        {
            Parametro oParametro = new Parametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            if (oParametro.opcion == 7)
            {
                #region Arriendo
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyArriendo().Lista(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (oParametro.opcion == 8)
            {
                #region Arriendo - InsertaActualiza
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyArriendo().InsertaActualiza(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }

            return View();
        }
        #endregion

        #region Kimberly - Cuestionario
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-16
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        //[CustomAuthorize]
        public ActionResult Cuestionario()
        {
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-01-16
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        //[CustomAuthorize]
        public ActionResult Cuestionario(string __a, string __b)
        {
            Parametro oParametro = new Parametro() { opcion = Convert.ToInt32(__a), parametro = __b };

            if (oParametro.opcion == 9 || oParametro.opcion == 13)
            {
                #region Filtro
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyCuestionario().Filtro(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (oParametro.opcion == 10)
            {
                #region Cuestionario
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyCuestionario().Cuestionario(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (oParametro.opcion == 12)
            {
                #region Inserta
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyCuestionario().InsertaActualiza(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }

            return View();
        }
        #endregion

        #region Kimberly - Poseidon
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult PoseidonPresencia()
        {
            string PlaCodigo = Convert.ToString(Request["_a"]);
            Persona oPersona = ((Persona)Session["Session_Login"]);

            List<KimberlySeleccion> lTipoCliente = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 0 });
            List<KimberlySeleccion> lOficina = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 2 });
            List<KimberlySeleccion> lCadena = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 3, parametro = PlaCodigo });
            List<KimberlySeleccion> lPdv = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 1, parametro = PlaCodigo + "," + Convert.ToString(oPersona.Person_id) + ",0,0" });

            TempData["lTipoCliente"] = lTipoCliente;
            TempData["lOficina"] = lOficina;
            TempData["lCadena"] = lCadena;
            TempData["lPdv"] = lPdv;

            ViewBag.RepId = 58;
            ViewBag.PlaCodigo = PlaCodigo;
            ViewBag.PerId = oPersona.Person_id;

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <param name="o"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult PoseidonPresencia(int o, string p)
        {
            Parametro oParametro = new Parametro() { opcion = o, parametro = p };

            if (oParametro.opcion == 4)
            {
                #region Cuestionario
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyCuestionario().PoseidonCuestionario(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else if (oParametro.opcion == 5)
            {
                #region Cuestionario Inserta&Actualiza
                int Resultado = 0;
                List<string> lString = p.Split(new Char[] { '&' }).ToList();

                foreach (var item in lString)
                {
                    Resultado += new KimberlyCuestionario().PoseidonInsertaActualiza(new Parametro() { opcion = 5, parametro = item });
                }

                return Content(new ContentResult
                {
                    Content = "{ \"Resultado\": " + Resultado + " }",
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
            else
            {
                #region Seleccione
                return Content(new ContentResult
                {
                    Content = MvcApplication._Serialize(new KimberlyCuestionario().PoseidonSelecciona(oParametro)),
                    ContentType = "application/json"
                }.Content);
                #endregion
            }
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult PoseidonVisibilidad()
        {
            string PlaCodigo = Convert.ToString(Request["_a"]);
            Persona oPersona = ((Persona)Session["Session_Login"]);

            List<KimberlySeleccion> lTipoCliente = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 0 });
            List<KimberlySeleccion> lOficina = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 2 });
            List<KimberlySeleccion> lCadena = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 3, parametro = PlaCodigo });
            List<KimberlySeleccion> lPdv = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 1, parametro = PlaCodigo + "," + Convert.ToString(oPersona.Person_id) + ",0,0" });

            TempData["lTipoCliente"] = lTipoCliente;
            TempData["lOficina"] = lOficina;
            TempData["lCadena"] = lCadena;
            TempData["lPdv"] = lPdv;

            ViewBag.RepId = 140;
            ViewBag.PlaCodigo = PlaCodigo;
            ViewBag.PerId = oPersona.Person_id;

            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2017-03-10
        /// </summary>
        /// <returns></returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult PoseidonMontoCompra()
        {
            string PlaCodigo = Convert.ToString(Request["_a"]);
            Persona oPersona = ((Persona)Session["Session_Login"]);

            List<KimberlySeleccion> lTipoCliente = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 0 });
            List<KimberlySeleccion> lOficina = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 2 });
            List<KimberlySeleccion> lCadena = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 3, parametro = PlaCodigo });
            List<KimberlySeleccion> lPdv = new KimberlyCuestionario().PoseidonSelecciona(new Parametro() { opcion = 1, parametro = PlaCodigo + "," + Convert.ToString(oPersona.Person_id) + ",0,0" });

            TempData["lTipoCliente"] = lTipoCliente;
            TempData["lOficina"] = lOficina;
            TempData["lCadena"] = lCadena;
            TempData["lPdv"] = lPdv;

            ViewBag.RepId = 272;
            ViewBag.PlaCodigo = PlaCodigo;
            ViewBag.PerId = oPersona.Person_id;
            ViewBag.XploraFoto = Path.Combine(XploraFoto, @"c1987/Media/Imagen/Foto/");

            return View();
        }
        #endregion

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-08
        /// Detalle: Carga de archivos
        /// </summary>
        /// <param name="archivo"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult Upload(HttpPostedFileBase archivo)
        {
            if (archivo == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            string archivoNombre;
            string pathArchivo;
            string TipoArchivo = Convert.ToString(Request["TipoEnvio"]);

            try
            {
                archivoNombre = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now) + Path.GetExtension(archivo.FileName);

                if (TipoArchivo == "PoseidonImagen")
                {
                    pathArchivo = Path.Combine(LocalFoto, @"c1987/Media/Imagen/Foto/", archivoNombre);
                }
                else
                {
                    pathArchivo = Path.Combine(LocalTemp, archivoNombre);
                }

                #region Crea archivo
                archivo.SaveAs(pathArchivo);
                #endregion

                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"" + archivoNombre + "\", \"_b\":true,\"_c\":\"Ok\" }",
                    ContentType = "application/json"
                }.Content);
            }
            catch (Exception e)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"\", \"_b\":false,\"_c\":\"" + e.Message + "\" }",
                    ContentType = "application/json"
                }.Content);
            }
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2016-09-08
        /// Detalle: Carga de archivos
        /// </summary>
        /// <param name="archivo"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult UploadMovil()
        {
            HttpPostedFileBase archivo = Request.Files["archivo"];

            if (archivo == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            string archivoNombre;
            string pathArchivo;
            string TipoArchivo = Convert.ToString(Request["TipoEnvio"]);

            try
            {
                archivoNombre = Path.GetFileName(archivo.FileName);

                if (TipoArchivo == "PoseidonImagen")
                {
                    pathArchivo = Path.Combine(LocalFoto, @"c1987/Media/Imagen/Foto/", archivoNombre);
                }
                else
                {
                    pathArchivo = Path.Combine(LocalTemp, archivoNombre);
                }

                #region Crea archivo
                archivo.SaveAs(pathArchivo);
                #endregion

                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"" + archivoNombre + "\", \"_b\":true,\"_c\":\"Ok\" }",
                    ContentType = "application/json"
                }.Content);
            }
            catch (Exception e)
            {
                return Content(new ContentResult
                {
                    Content = "{ \"_a\":\"\", \"_b\":false,\"_c\":\"" + e.Message + "\" }",
                    ContentType = "application/json"
                }.Content);
            }
        }
    }
}