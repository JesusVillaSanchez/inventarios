﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
//using Lucky.Data;

using Lucky.Xplora;
using Lucky.Xplora.Models;
using Lucky.Xplora.Models.Reporting;
using Lucky.Xplora.Models.ColgateReporting;
using System.IO;
using Lucky.Business.Common.Servicio;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using Lucky.Xplora.Security;


namespace Lucky.Xplora.Controllers
{
    public class ColgateDashboardController : Controller
    {
        //
        // GET: /ColgateDashboard/
        [CustomAuthorize]
        public ActionResult Index()
        {
            //Se registra visita
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            
            return View();
        }

        public ActionResult Dsh_Coverage(int anio, int mes, int typegraf, int channel)
        {
            ViewBag.vOpcion = typegraf;
            ViewBag.vCanal = channel;
            return View(new Dashboard_Coverage().Objeto_Coverage(
                    new Request_ColgateDashboard_Parametros()
                    {
                        compania = 1561
                        ,anio = Convert.ToInt32(anio)
                        , mes=Convert.ToInt32(mes)
                        ,opcion = 0
                    }
                ));
        }

        public ActionResult Dsh_CallValue(int anio, int mes, int typegraf, int channel)
        {
            ViewBag.vOpcion = typegraf;
            ViewBag.vCanal = channel;
            return View(new Dashboard_CallValue().Objeto_CallValue(
                    new Request_ColgateDashboard_Parametros()
                    {
                        compania = 1561
                        ,anio = Convert.ToInt32(anio)
                        ,mes = Convert.ToInt32(mes)
                        ,opcion = 1
                    }
                ));
        }

        public ActionResult Dsh_Visibility(int anio, int mes, int channel, int Element)
        {
            ViewBag.vChannel = channel;
            ViewBag.vElement = Element;
            return View(new Dashboard_visibility().Objeto_Visibility(
                    new Request_ColgateDashboard_Parametros()
                    {
                        compania = 1561
                        ,anio = Convert.ToInt32(anio)
                        ,mes = Convert.ToInt32(mes)
                        ,opcion = 2
                    }
                ));

        }

        public ActionResult Dsh_Rotation(int anio, int mes, int typegraf)
        {
            ViewBag.vOpcion = typegraf;
            return View(new Dashboard_Rotation().Objeto_Rotation(
                    new Request_ColgateDashboard_Parametros()
                    {
                        compania = 1561
                        ,anio = Convert.ToInt32(anio)
                        ,mes = Convert.ToInt32(mes)
                        ,opcion = 3
                    }
                ));
        }

        public ActionResult Dsh_Compliance(int anio, int mes, int channel)
        {
            ViewBag.vChannel = channel;
            ViewBag.vAnio = anio;
            ViewBag.vMes = mes;
            return View(new Dashboard_compliancerepotrs().Objeto_Compliance(
                    new Request_ColgateDashboard_Parametros()
                    {
                        compania = 1561
                        ,anio = Convert.ToInt32(anio)
                        ,mes = Convert.ToInt32(mes)
                        ,opcion = 4
                    }
                ));
        }

    }
}
