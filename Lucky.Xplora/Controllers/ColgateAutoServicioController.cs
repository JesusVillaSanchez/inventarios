﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucky.Business.Common.Servicio;
using Lucky.Entity.Common.Servicio.AlicorpMulticategoriaCanje;
using Newtonsoft.Json.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using Excel = OfficeOpenXml;
using Style = OfficeOpenXml.Style;
using System.IO;
using System.Configuration;

namespace Lucky.Xplora.Controllers
{
    public class ColgateAutoServicioController : Controller
    {
        //
        // GET: /ColgateAutoServicio/
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);

        BL_GES_Operativa oOperativa = new BL_GES_Operativa();
        public ActionResult Index()
        {
            return View();
        }
        #region -- Visibilidad --
        public ActionResult Visibilidad() {
            return View();
        }        
        public ActionResult getVisibilidad(string __a,int __b,int __c,int __d,int __e,int __f,int __g,int __h) {
            
            return Json(new
            {
                //vanio,vtienda,vzona,vtipomat,vop,vsubcategoria,vcadena,vmaterial
                response = oOperativa.BL_BI_ColgAASS_Visibilidad(__a, __b, __c, __d, __e,__f,__g,__h)
            });
        }
        [HttpPost]
        public ActionResult getVisibilidadGraficoExcel(string __a, int __b, int __c, int __d, int __e, int __f, int __g, int __h,int __i,string __j)
        {
            
            #region Descarga informacion
                    int Fila = 2;
                    string NombreServidor = "";
                    string RutaLocal = "";

                    NombreServidor = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);
                    RutaLocal = System.IO.Path.Combine(LocalTemp, NombreServidor);

                    using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(RutaLocal)))
                    {
                        var ObjTable = oOperativa.BL_BI_ColgAASS_Visibilidad_Excel(__a, __b, __c, __d, __e, __f, __g, __h, __i);

                        Excel.ExcelWorksheet oSh = oEx.Workbook.Worksheets.Add(__j);

                        if (__e == 1)
                        {
                            if (__i == 1)
                            {
                                #region Encabezado
                                oSh.Cells["A1"].Value = "Sub Categoria";
                                //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oSh.Cells["B1"].Value = "Empresa";
                                oSh.Cells["C1"].Value = "Cantidad";
                                oSh.Cells["D1"].Value = "Monto x Subcategoria";
                                oSh.Cells["E1"].Value = "Porcentaje x Subcategoria";
                                #endregion
                                #region Body
                                foreach (var objFila in ObjTable.grafico)
                                {

                                    oSh.Cells[Fila, 1].Value = objFila.subcategoria;
                                    oSh.Cells[Fila, 2].Value = objFila.empresa;
                                    oSh.Cells[Fila, 3].Value = objFila.cantidad;
                                    oSh.Cells[Fila, 4].Value = objFila.monto;
                                    oSh.Cells[Fila, 5].Value = objFila.porcentaje;
                                    Fila++;
                                }

                                oSh.Row(1).Style.Font.Bold = true;
                                oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                                oSh.Column(1).AutoFit();
                                oSh.Column(2).AutoFit();
                                oSh.Column(3).AutoFit();
                                oSh.Column(4).AutoFit();
                                oSh.Column(5).AutoFit();

                                oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                #endregion
                            }
                            if (__i == 2)
                            {
                                #region Encabezado
                                oSh.Cells["A1"].Value = "Sub Categoria";
                                //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oSh.Cells["B1"].Value = "Empresa";
                                oSh.Cells["C1"].Value = "Cantidad";
                                oSh.Cells["D1"].Value = "Porcentaje";
                                #endregion
                                #region Body
                                foreach (var objFila in ObjTable.grafico)
                                {
                                    oSh.Cells[Fila, 1].Value = objFila.subcategoria;
                                    oSh.Cells[Fila, 2].Value = objFila.empresa;
                                    oSh.Cells[Fila, 3].Value = objFila.cantidad;
                                    oSh.Cells[Fila, 4].Value = objFila.porcentaje;
                                    Fila++;
                                }

                                oSh.Row(1).Style.Font.Bold = true;
                                oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                                oSh.Column(1).AutoFit();
                                oSh.Column(2).AutoFit();
                                oSh.Column(3).AutoFit();
                                oSh.Column(4).AutoFit();

                                oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                #endregion
                            }
                            if (__i == 3)
                            {
                                #region Encabezado
                                oSh.Cells["A1"].Value = "Cadena";
                                //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oSh.Cells["B1"].Value = "Empresa";
                                oSh.Cells["C1"].Value = "Cantidad";
                                oSh.Cells["D1"].Value = "Monto x Cadena";
                                oSh.Cells["E1"].Value = "Porcentaje x Cadena";
                                #endregion
                                #region Body
                                foreach (var objFila in ObjTable.grafico)
                                {

                                    oSh.Cells[Fila, 1].Value = objFila.cadena;
                                    oSh.Cells[Fila, 2].Value = objFila.empresa;
                                    oSh.Cells[Fila, 3].Value = objFila.cantidad;
                                    oSh.Cells[Fila, 4].Value = objFila.monto;
                                    oSh.Cells[Fila, 5].Value = objFila.porcentaje;
                                    Fila++;
                                }

                                oSh.Row(1).Style.Font.Bold = true;
                                oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                                oSh.Column(1).AutoFit();
                                oSh.Column(2).AutoFit();
                                oSh.Column(3).AutoFit();
                                oSh.Column(4).AutoFit();
                                oSh.Column(5).AutoFit();

                                oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                #endregion
                            }
                            if (__i == 4)
                            {
                                #region Encabezado
                                oSh.Cells["A1"].Value = "Material";
                                //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oSh.Cells["B1"].Value = "Empresa";
                                oSh.Cells["C1"].Value = "Cantidad";
                                oSh.Cells["D1"].Value = "Monto x Material";
                                oSh.Cells["E1"].Value = "Porcentaje x Material";
                                #endregion
                                #region Body
                                foreach (var objFila in ObjTable.grafico)
                                {

                                    oSh.Cells[Fila, 1].Value = objFila.material;
                                    oSh.Cells[Fila, 2].Value = objFila.empresa;
                                    oSh.Cells[Fila, 3].Value = objFila.cantidad;
                                    oSh.Cells[Fila, 4].Value = objFila.monto;
                                    oSh.Cells[Fila, 5].Value = objFila.porcentaje;
                                    Fila++;
                                }

                                oSh.Row(1).Style.Font.Bold = true;
                                oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                                oSh.Column(1).AutoFit();
                                oSh.Column(2).AutoFit();
                                oSh.Column(3).AutoFit();
                                oSh.Column(4).AutoFit();
                                oSh.Column(5).AutoFit();

                                oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                #endregion
                            }
                            if (__i == 5)
                            {
                                #region Encabezado
                                oSh.Cells["A1"].Value = "Cod Pdv";
                                //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                                oSh.Cells["B1"].Value = "Pdv";
                                oSh.Cells["C1"].Value = "Valorizado";
                                oSh.Cells["D1"].Value = "Empresa";
                                oSh.Cells["E1"].Value = "Porcentaje";
                                #endregion
                                #region Body
                                foreach (var objFila in ObjTable.table)
                                {

                                    oSh.Cells[Fila, 1].Value = objFila.cod_pdv;
                                    oSh.Cells[Fila, 2].Value = objFila.pdv;
                                    oSh.Cells[Fila, 3].Value = objFila.valorizado;
                                    oSh.Cells[Fila, 4].Value = objFila.empresa;
                                    oSh.Cells[Fila, 5].Value = objFila.porcentaje;
                                    Fila++;
                                }

                                oSh.Row(1).Style.Font.Bold = true;
                                oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                                oSh.Column(1).AutoFit();
                                oSh.Column(2).AutoFit();
                                oSh.Column(3).AutoFit();
                                oSh.Column(4).AutoFit();
                                oSh.Column(5).AutoFit();

                                oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                                #endregion
                            }
                        }
                        if (__e == 2)
                        {
                            #region Encabezado
                            oSh.Cells["A1"].Value = "Año";
                            //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oSh.Cells["B1"].Value = "Mes";
                            oSh.Cells["C1"].Value = "Semana";
                            oSh.Cells["D1"].Value = "Valorizado";
                            oSh.Cells["E1"].Value = "Pdv con elemento";
                            oSh.Cells["F1"].Value = "Pdv visitados";
                            oSh.Cells["G1"].Value = "Empresa";
                            oSh.Cells["H1"].Value = "Cantidad";
                            #endregion
                            #region Body
                            foreach (var objFila in ObjTable.grafico)
                            {

                                oSh.Cells[Fila, 1].Value = objFila.anio;
                                oSh.Cells[Fila, 2].Value = objFila.mes;
                                oSh.Cells[Fila, 3].Value = objFila.semana;
                                oSh.Cells[Fila, 4].Value = objFila.valorizado;
                                oSh.Cells[Fila, 5].Value = objFila.pdv_element;
                                oSh.Cells[Fila, 6].Value = objFila.pdv_visit;
                                oSh.Cells[Fila, 7].Value = objFila.empresa;
                                oSh.Cells[Fila, 8].Value = objFila.cantidad;
                                Fila++;
                            }

                            oSh.Row(1).Style.Font.Bold = true;
                            oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            oSh.Column(1).AutoFit();
                            oSh.Column(2).AutoFit();
                            oSh.Column(3).AutoFit();
                            oSh.Column(4).AutoFit();
                            oSh.Column(5).AutoFit();
                            oSh.Column(6).AutoFit();
                            oSh.Column(7).AutoFit();
                            oSh.Column(8).AutoFit();
                            oSh.Column(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oSh.Column(6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oSh.Column(8).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            #endregion
                        }
                        if (__e == 3)
                        {
                            #region Encabezado
                            oSh.Cells["A1"].Value = "Año";
                            //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                            oSh.Cells["B1"].Value = "Mes";
                            oSh.Cells["C1"].Value = "Periodo";
                            oSh.Cells["D1"].Value = "Zona";
                            oSh.Cells["E1"].Value = "Oficina";
                            oSh.Cells["F1"].Value = "Cadena";
                            oSh.Cells["G1"].Value = "Tipo PDV";
                            oSh.Cells["H1"].Value = "PDV";
                            oSh.Cells["I1"].Value = "Sub Categoria";
                            oSh.Cells["J1"].Value = "Empresa";
                            oSh.Cells["K1"].Value = "Tipo Elemento";
                            oSh.Cells["L1"].Value = "Material";
                            oSh.Cells["M1"].Value = "Cantidad";
                            oSh.Cells["N1"].Value = "Valorizado";
                            #endregion
                            #region Body
                            foreach (var objFila in ObjTable.base_rep)
                            {

                                oSh.Cells[Fila, 1].Value = objFila.anio;
                                oSh.Cells[Fila, 2].Value = objFila.mes;
                                oSh.Cells[Fila, 3].Value = objFila.semana;
                                oSh.Cells[Fila, 4].Value = objFila.zona;
                                oSh.Cells[Fila, 5].Value = objFila.oficina;
                                oSh.Cells[Fila, 6].Value = objFila.cadena;
                                oSh.Cells[Fila, 7].Value = objFila.pdv_top;
                                oSh.Cells[Fila, 8].Value = objFila.pdv;
                                oSh.Cells[Fila, 9].Value = objFila.subcategoria;
                                oSh.Cells[Fila, 10].Value = objFila.empresa;
                                oSh.Cells[Fila, 11].Value = objFila.tipo_material;
                                oSh.Cells[Fila, 12].Value = objFila.material;
                                oSh.Cells[Fila, 13].Value = objFila.cantidad;
                                oSh.Cells[Fila, 14].Value = objFila.monto;
                                Fila++;
                            }

                            oSh.Row(1).Style.Font.Bold = true;
                            oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                            oSh.Column(1).AutoFit();
                            oSh.Column(2).AutoFit();
                            oSh.Column(3).AutoFit();
                            oSh.Column(4).AutoFit();
                            oSh.Column(5).AutoFit();
                            oSh.Column(6).AutoFit();
                            oSh.Column(7).AutoFit();
                            oSh.Column(8).AutoFit();
                            oSh.Column(9).AutoFit();
                            oSh.Column(10).AutoFit();
                            oSh.Column(11).AutoFit();
                            oSh.Column(12).AutoFit();
                            oSh.Column(13).AutoFit();
                            oSh.Column(14).AutoFit();
                            oSh.Column(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oSh.Column(13).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                            oSh.Column(14).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                            #endregion
                        }
                        oEx.Save();
                    }

                    return new ContentResult
                    {
                        Content = "{ \"Archivo\": \"" + NombreServidor + "\" }",
                        ContentType = "application/json"
                    };
                    #endregion

        }
        #endregion
        #region -- EEAA --
        public ActionResult Exhibicion() {
            return View();
        }
        public ActionResult getExhibiciones(string __a, int __b, int __c, int __d, int __e, int __f, int __g)
        {
            return Json(new { response = oOperativa.BL_BI_ColgAASS_Exhibicones(__a, __b, __c, __d, __e, __f, __g) });
        }
        [HttpPost]
        public ActionResult getExhibicionesExcel(string __a, int __b, int __c, int __d, int __e, int __f, int __g,int __h,string __i)
        {
            #region Descarga informacion
            int Fila = 2;
            string NombreServidor = "";
            string RutaLocal = "";

            NombreServidor = String.Format("{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);
            RutaLocal = System.IO.Path.Combine(LocalTemp, NombreServidor);

            using (Excel.ExcelPackage oEx = new Excel.ExcelPackage(new FileInfo(RutaLocal)))
            {
                var ObjTable = oOperativa.BL_BI_ColgAASS_Exhibicones_Excel(__a, __b, __c, __d, __e, __f, __g, __h);

                Excel.ExcelWorksheet oSh = oEx.Workbook.Worksheets.Add(__i);

                if (__d == 1)
                {
                    if (__h == 1)
                    {
                        #region Encabezado
                        oSh.Cells["A1"].Value = "Sub Categoria";
                        //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oSh.Cells["B1"].Value = "Cantidad";
                        oSh.Cells["C1"].Value = "Monto";
                        oSh.Cells["D1"].Value = "Porcentaje";
                        oSh.Cells["E1"].Value = "Valorizado Total";
                        oSh.Cells["F1"].Value = "Cantidad Total";
                        #endregion
                        #region Body
                        foreach (var objFila in ObjTable.exportable)
                        {

                            oSh.Cells[Fila, 1].Value = objFila.subcategoria;
                            oSh.Cells[Fila, 2].Value = objFila.cantidad;
                            oSh.Cells[Fila, 3].Value = objFila.monto;
                            oSh.Cells[Fila, 4].Value = objFila.porcentaje;
                            oSh.Cells[Fila, 5].Value = objFila.monto_g;
                            oSh.Cells[Fila, 6].Value = objFila.cantidad_g;
                            Fila++;
                        }

                        oSh.Row(1).Style.Font.Bold = true;
                        oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        oSh.Column(1).AutoFit();
                        oSh.Column(2).AutoFit();
                        oSh.Column(3).AutoFit();
                        oSh.Column(4).AutoFit();
                        oSh.Column(5).AutoFit();
                        oSh.Column(6).AutoFit();

                        oSh.Column(2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        #endregion
                    }
                    if (__h == 2)
                    {
                        #region Encabezado
                        oSh.Cells["A1"].Value = "Sub Categoria";
                        //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oSh.Cells["B1"].Value = "cadena";
                        oSh.Cells["C1"].Value = "Monto";
                        oSh.Cells["D1"].Value = "Cantidad";
                        oSh.Cells["E1"].Value = "Porcentaje";
                        oSh.Cells["F1"].Value = "Cantidad Total";
                        oSh.Cells["G1"].Value = "Monto Total";
                        #endregion
                        #region Body
                        foreach (var objFila in ObjTable.exportable)
                        {
                            oSh.Cells[Fila, 1].Value = objFila.subcategoria;
                            oSh.Cells[Fila, 2].Value = objFila.cadena;
                            oSh.Cells[Fila, 3].Value = objFila.monto;
                            oSh.Cells[Fila, 4].Value = objFila.cantidad;
                            oSh.Cells[Fila, 5].Value = objFila.porcentaje;
                            oSh.Cells[Fila, 6].Value = objFila.cantidad_g;
                            oSh.Cells[Fila, 7].Value = objFila.monto_g;
                            Fila++;
                        }

                        oSh.Row(1).Style.Font.Bold = true;
                        oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        oSh.Column(1).AutoFit();
                        oSh.Column(2).AutoFit();
                        oSh.Column(3).AutoFit();
                        oSh.Column(4).AutoFit();
                        oSh.Column(5).AutoFit();
                        oSh.Column(6).AutoFit();
                        oSh.Column(7).AutoFit();

                        oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(7).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        #endregion
                    }
                    if (__h == 3)
                    {
                        #region Encabezado
                        oSh.Cells["A1"].Value = "Sub Categoria";
                        //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oSh.Cells["B1"].Value = "Cadena";
                        oSh.Cells["C1"].Value = "Material";
                        oSh.Cells["D1"].Value = "Monto";
                        oSh.Cells["E1"].Value = "Cantidad";
                        oSh.Cells["F1"].Value = "Porcentaje";

                        #endregion
                        #region Body
                        foreach (var objFila in ObjTable.exportable)
                        {
                            oSh.Cells[Fila, 1].Value = objFila.subcategoria;
                            oSh.Cells[Fila, 2].Value = objFila.cadena;
                            oSh.Cells[Fila, 3].Value = objFila.material;
                            oSh.Cells[Fila, 4].Value = objFila.monto;
                            oSh.Cells[Fila, 5].Value = objFila.cantidad;
                            oSh.Cells[Fila, 6].Value = objFila.porcentaje;
                            Fila++;
                        }
                        oSh.Row(1).Style.Font.Bold = true;
                        oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        oSh.Column(1).AutoFit();
                        oSh.Column(2).AutoFit();
                        oSh.Column(3).AutoFit();
                        oSh.Column(4).AutoFit();
                        oSh.Column(5).AutoFit();
                        oSh.Column(6).AutoFit();
                        
                        oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        #endregion
                    }
                    if (__h == 4)
                    {
                        #region Encabezado
                        oSh.Cells["A1"].Value = "Sub Categoria";
                        //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oSh.Cells["B1"].Value = "Cadena";
                        oSh.Cells["C1"].Value = "Material";
                        oSh.Cells["D1"].Value = "Cod PDV";
                        oSh.Cells["E1"].Value = "PDV";
                        oSh.Cells["F1"].Value = "Monto";
                        oSh.Cells["G1"].Value = "Cantidad";
                        oSh.Cells["H1"].Value = "Porcentaje";
                        #endregion
                        #region Body
                        foreach (var objFila in ObjTable.exportable)
                        {

                            oSh.Cells[Fila, 1].Value = objFila.subcategoria;
                            oSh.Cells[Fila, 2].Value = objFila.cadena;
                            oSh.Cells[Fila, 3].Value = objFila.material;
                            oSh.Cells[Fila, 4].Value = objFila.id_pdv;
                            oSh.Cells[Fila, 5].Value = objFila.pdv;
                            oSh.Cells[Fila, 6].Value = objFila.monto;
                            oSh.Cells[Fila, 7].Value = objFila.cantidad;
                            oSh.Cells[Fila, 8].Value = objFila.porcentaje;
                            Fila++;
                        }

                        oSh.Row(1).Style.Font.Bold = true;
                        oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        oSh.Column(1).AutoFit();
                        oSh.Column(2).AutoFit();
                        oSh.Column(3).AutoFit();
                        oSh.Column(4).AutoFit();
                        oSh.Column(5).AutoFit();
                        oSh.Column(6).AutoFit();
                        oSh.Column(7).AutoFit();
                        oSh.Column(8).AutoFit();

                        oSh.Column(6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(7).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(8).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        #endregion
                    }
                    if (__h == 6)
                    {
                        #region Encabezado
                        oSh.Cells["A1"].Value = "Cod Pdv";
                        //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oSh.Cells["B1"].Value = "Pdv";
                        oSh.Cells["C1"].Value = "Valorizado";
                        oSh.Cells["D1"].Value = "Material";
                        oSh.Cells["E1"].Value = "Porcentaje";
                        #endregion
                        #region Body
                        foreach (var objFila in ObjTable.exportable)
                        {

                            oSh.Cells[Fila, 1].Value = objFila.id_pdv;
                            oSh.Cells[Fila, 2].Value = objFila.pdv;
                            oSh.Cells[Fila, 3].Value = objFila.valorizado;
                            oSh.Cells[Fila, 4].Value = objFila.material;
                            oSh.Cells[Fila, 5].Value = objFila.porcentaje;
                            Fila++;
                        }

                        oSh.Row(1).Style.Font.Bold = true;
                        oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        oSh.Column(1).AutoFit();
                        oSh.Column(2).AutoFit();
                        oSh.Column(3).AutoFit();
                        oSh.Column(4).AutoFit();
                        oSh.Column(5).AutoFit();

                        oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        #endregion
                    }
                }
                if (__d == 2)
                {
                    if (__h == 5)
                    {
                        #region Encabezado
                        oSh.Cells["A1"].Value = "Año";
                        //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oSh.Cells["B1"].Value = "Mes";
                        oSh.Cells["C1"].Value = "Semana";
                        oSh.Cells["D1"].Value = "Monto";
                        oSh.Cells["E1"].Value = "Pdv con elemento";
                        oSh.Cells["F1"].Value = "Pdv visitados";
                        #endregion
                        #region Body
                        foreach (var objFila in ObjTable.exportable)
                        {

                            oSh.Cells[Fila, 1].Value = objFila.anio;
                            oSh.Cells[Fila, 2].Value = objFila.mes;
                            oSh.Cells[Fila, 3].Value = objFila.semana;
                            oSh.Cells[Fila, 4].Value = objFila.monto;
                            oSh.Cells[Fila, 5].Value = objFila.cant_pdv;
                            oSh.Cells[Fila, 6].Value = objFila.cant_pdv_visitados;
                            Fila++;
                        }

                        oSh.Row(1).Style.Font.Bold = true;
                        oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        oSh.Column(1).AutoFit();
                        oSh.Column(2).AutoFit();
                        oSh.Column(3).AutoFit();
                        oSh.Column(4).AutoFit();
                        oSh.Column(5).AutoFit();
                        oSh.Column(6).AutoFit();

                        oSh.Column(4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        #endregion
                    }
                    if (__h == 7) {
                        #region Encabezado
                        oSh.Cells["A1"].Value = "Año";
                        //oSh.Cells["A1"].Style.Border.Top.Style = Style.ExcelBorderStyle.Thin;
                        oSh.Cells["B1"].Value = "Mes";
                        oSh.Cells["C1"].Value = "Semana";
                        oSh.Cells["D1"].Value = "Zona";
                        oSh.Cells["E1"].Value = "Cadena";
                        oSh.Cells["F1"].Value = "Empresa";
                        oSh.Cells["G1"].Value = "Zona";
                        oSh.Cells["H1"].Value = "Material";
                        oSh.Cells["I1"].Value = "Sub Categoria";
                        oSh.Cells["J1"].Value = "Cod PDV";
                        oSh.Cells["K1"].Value = "PDV";
                        oSh.Cells["L1"].Value = "Cantidad";
                        oSh.Cells["M1"].Value = "Monto";
                        #endregion
                        #region Body
                        foreach (var objFila in ObjTable.exportable_base)
                        {

                            oSh.Cells[Fila, 1].Value = objFila.anio;
                            oSh.Cells[Fila, 2].Value = objFila.mes;
                            oSh.Cells[Fila, 3].Value = objFila.semana;
                            oSh.Cells[Fila, 4].Value = objFila.zona;
                            oSh.Cells[Fila, 5].Value = objFila.cadena ;
                            oSh.Cells[Fila, 6].Value = objFila.empresa;
                            oSh.Cells[Fila, 7].Value = objFila.zona;
                            oSh.Cells[Fila, 8].Value = objFila.material;
                            oSh.Cells[Fila, 9].Value = objFila.subcategoria;
                            oSh.Cells[Fila, 10].Value = objFila.id_pdv;
                            oSh.Cells[Fila, 11].Value = objFila.pdv;
                            oSh.Cells[Fila, 12].Value = objFila.cantidad;
                            oSh.Cells[Fila, 13].Value = objFila.monto;
                            
                            Fila++;
                        }

                        oSh.Row(1).Style.Font.Bold = true;
                        oSh.Row(1).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center;

                        oSh.Column(1).AutoFit();
                        oSh.Column(2).AutoFit();
                        oSh.Column(3).AutoFit();
                        oSh.Column(4).AutoFit();
                        oSh.Column(5).AutoFit();
                        oSh.Column(6).AutoFit();
                        oSh.Column(7).AutoFit();
                        oSh.Column(8).AutoFit();
                        oSh.Column(9).AutoFit();
                        oSh.Column(10).AutoFit();
                        oSh.Column(11).AutoFit();
                        oSh.Column(12).AutoFit();
                        oSh.Column(13).AutoFit();
                        oSh.Column(14).AutoFit();
                        oSh.Column(1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(13).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;
                        oSh.Column(14).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center;

                        #endregion
                    }
                }
                
                oEx.Save();
            }

            return new ContentResult
            {
                Content = "{ \"Archivo\": \"" + NombreServidor + "\" }",
                ContentType = "application/json"
            };
            #endregion
        }
        #endregion
        #region -- Precios --
        public ActionResult Precio()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            } 
            return View();
        }
        #endregion
        #region -- Dashboard --
        public ActionResult Dashboard()
        {
            return View();
        }
        #endregion
        #region -- Actividades --
        public ActionResult Actividades() {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            return View();
        }
        public ActionResult getActividades(int _a, string _b, int _c, int _d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(oOperativa.Bl_ColgAASS_Actividades(_a, _b, _c, _d)),//MvcApplication._Serialize(new Seguimiento().l_Seguimiento_v2_Cantidad_Relevo(oParametro)),
                ContentType = "application/json"
            }.Content);
            /*return Json(new
            {
                response = oOperativa.Bl_ColgAASS_Actividades(_a, _b, _c, _d)
            });*/
        }
        #endregion
        #region -- Encartes --
        public ActionResult Encartes(){
            if (Session["Session_Login"] == null){
                return RedirectToAction("login", "LogOn");
            }
            return View();
        }
        public ActionResult getEncartes(int _a, string _b, int _c, int _d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(oOperativa.Bl_ColgAASS_Encartes(_a, _b, _c, _d)),//MvcApplication._Serialize(new Seguimiento().l_Seguimiento_v2_Cantidad_Relevo(oParametro)),
                ContentType = "application/json"
            }.Content);
            /*return Json(new
            {
                response = oOperativa.Bl_ColgAASS_Encartes(_a, _b, _c, _d)
            });*/
        }
        public ActionResult getEncartes_Detalle_Anuncios(int _a, string _b, int _c, int _d)
        {
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(oOperativa.Bl_ColgAASS_Encartes_Detalle_Anuncios(_a, _b, _c, _d)),//MvcApplication._Serialize(new Seguimiento().l_Seguimiento_v2_Cantidad_Relevo(oParametro)),
                ContentType = "application/json"
            }.Content);
            /*return Json(new
            {
                response = oOperativa.Bl_ColgAASS_Encartes(_a, _b, _c, _d)
            });*/
        }
        #endregion
        #region -- Of Out Stock--
        public ActionResult OOS() 
        {
            return View();
        }
        #endregion
        #region -- Participacion --
        public ActionResult Participacion()
        {
            return View();
        }
        public ActionResult getParticipacion( int Opcion, string Parametros)
        {
            return Json(new
                {
                    response = oOperativa.Bl_ColgAASS_Participacion(Opcion,Parametros)
                });
        }
        #endregion
        #region -- Push Girl--
        public ActionResult PushGirl() 
        {
            return View();
        }

        public ActionResult getPushGirl(int Opcion, string Parametros)
        {
            return Json(new
                {
                    response = oOperativa.Bl_ColgateAASS_PushGirl(Opcion,Parametros)
                });
        }
        #endregion
        #region -- Filtros --
        public ActionResult Filtros(int __a,int __b,string __c) {
            return Json(new
            {
                response = oOperativa.BL_BI_ColgAASS_Filtros(__a, __b, __c)
            });
        }
        #endregion
    }
}
