﻿using Lucky.Xplora.Models;
using Lucky.Xplora.Models.BackusReporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Configuration;
using System.Web.Services;

namespace Lucky.Xplora.Controllers
{
    public class FotograficoController : Controller
    {
        string LocalTemp = Convert.ToString(ConfigurationManager.AppSettings["LocalTemp"]);
        string LocalFormat = Convert.ToString(ConfigurationManager.AppSettings["LocalFormat"]);
        
        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-06
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ActionResult Fotografico()
        {
            if (Session["Session_Login"] == null)
            {
                return RedirectToAction("login", "LogOn");
            }
            Persona oPersona = (Persona)Session["Session_Login"];
            ViewBag.tpf_id = oPersona.tpf_id;
            ViewBag.cam_codigo = Request.QueryString["_a"];
            if (ViewBag.cam_codigo != "")
            {
                //ViewBag.campania_descripcion = new Campania().Objeto(new Obtener_Campania_por_idCampania_Request() { campania = _a }).cam_descripcion; yr
                List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = Request["_a"] });
                ViewBag.campania_descripcion = oLstCampania[0].Planning_Name.ToString();
            }
            return View();
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-16
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult GetGaleriaFotos(string __a, string __b, string __c, string __d, string __e, string __f, string __g, string __h, string __i, string __j, string __k, string __l, string __m, string __n, string __o, string __p, string __q)
        {
            //return View(
            //        new Fotografico().Lista(new Request_Fotografico()
            //        {
            //            campania = __a,
            //            tipocanal = Convert.ToInt32(__b),
            //            anio = Convert.ToInt32(__c),
            //            mes = Convert.ToInt32(__d),
            //            periodo = Convert.ToInt32(__e),
            //            nodocomercial = Convert.ToInt32(__f),
            //            categoria = Convert.ToInt32(__g),
            //            puntoventa = __h,
            //            dia = Convert.ToInt32(__i),
            //            ciudad = __j,
            //            departamento = __k,
            //            fechaInicio = __l,
            //            fechaFin = __m,
            //            dex = __n,
            //            supervisor = __o,
            //            gestor = __p,
            //            programa = __q
            //        })
            //    );
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                     new Fotografico().Lista(new Request_Fotografico()
                     {
                         campania = __a,
                         tipocanal = Convert.ToInt32(__b),
                         anio = Convert.ToInt32(__c),
                         mes = Convert.ToInt32(__d),
                         periodo = Convert.ToInt32(__e),
                         nodocomercial = Convert.ToInt32(__f),
                         categoria = Convert.ToInt32(__g),
                         puntoventa = __h,
                         dia = Convert.ToInt32(__i),
                         ciudad = __j,
                         departamento = __k,
                         fechaInicio = __l,
                         fechaFin = __m,
                         dex = __n,
                         supervisor = __o,
                         gestor = __p,
                         programa = __q
                     })
                    ),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-17
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult GetCampania()
        {
            return new ContentResult
             {
                 Content = MvcApplication._Serialize(
                    new Campania().Lista(
                        new Request_GetCampania_Compania() { 
                            compania = ((Persona)Session["Session_Login"]).Company_id 
                        }
                    )
                 ),
                 ContentType = "application/json"
             };
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-17
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult GetAnio(string __a)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Año().Lista(
                        new Request_Anio_Por_Planning_Reports()
                        {
                            campania = __a
                        }
                    )
                ),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-20
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult GetMes(string __a, string __b, string __c, string __d, string __e)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                   new Lucky.Xplora.Models.Mes().Lista(
                        new Request_GetMes_Campania_Reporte_Anio_Formato_TipoPerfil()
                        {
                            campania = __a,
                            reporte = Convert.ToInt32(__b),
                            anio = Convert.ToInt32(__c),
                            formato = Convert.ToInt32(__d),
                            tipo_perfil = Convert.ToInt32(__e),
                        }
                    )
                ),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-20
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult GetPeriodo(string __a, string __b, string __c, string __d, string __e, string __f)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Periodo().Lista(
                        new Request_GetPeriodo_Campania_Reporte_Anio_Mes_Formato_TipoPerfil()
                        {
                            campania = __a,
                            reporte = Convert.ToInt32(__b),
                            anio = Convert.ToInt32(__c),
                            mes = Convert.ToInt32(__d),
                            formato = Convert.ToInt32(__e),
                            tipo_perfil = Convert.ToInt32(__f)
                        }
                    )
                ),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-20
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult GetMercado(string __a, string __b)
        {
            Lucky.Xplora.Models.Utilitario.registrar_visita(Request.QueryString["_b"]);
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                new NodoComercial().Lista(
                        new Request_GetCadena_Campania_TipoCanal()
                        {
                            campania = __a,
                            tipocanal = Convert.ToInt32(__b)
                        }
                    )
                ),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-21
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult GetCategoria(string __a)
        {
           return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Categoria().Lista(
                        new Listar_Categoria_Por_CodCampania_y_CodReporte_Request()
                        {
                            campania = __a,
                            reporte = 23
                        }
                    )
                ),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-21
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult GetCiudad(string __a)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Lucky.Xplora.Models.BackusPlanning.Ciudad().ListaCiudades()
                ),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-21
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult GetOficina(string __a)
        {
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Lucky.Xplora.Models.BackusPlanning.Ciudad().oLstOficina(
                        new Lucky.Xplora.Models.BackusPlanning.Request_GetCiudad
                        {
                            Cod_Campania = __a
                        }
                    )
                ),
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// Autor: yCueva
        /// Fecha: 2017-02-24
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public JsonResult DescargarArchivo(string __a, string __b)
        {
            string error = "";
            string _filePath = "";
            string _fileServer = "";
            string _fileFormatServer = "";
            int iCompania = ((Persona)Session["Session_Login"]).Company_id;
            string extension = __b;

            try
            {
                List<Fotografico> oLs = MvcApplication._Deserialize<List<Fotografico>>(__a);

                _fileServer = String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);
                if (iCompania == 1646)
                {
                    _fileFormatServer = System.IO.Path.Combine(LocalFormat, "lucky-Formato-Fotografico.pptx");
                    //_fileFormatServer = System.IO.Path.Combine(LocalFormat, "lucky-Formato-Fotografico-aje.pptx");
                }
                else if (iCompania == 1637)
                {
                    _fileFormatServer = System.IO.Path.Combine(LocalFormat, "lucky-Formato-Fotografico.pptx");
                    //_fileFormatServer = System.IO.Path.Combine(LocalFormat, "lucky-Formato-Fotografico-Backus.pptx");
                }
                else
                {
                    _fileFormatServer = System.IO.Path.Combine(LocalFormat, "lucky-Formato-Fotografico.pptx");
                }
                _filePath = System.IO.Path.Combine(LocalTemp, _fileServer);

                /*copia formato y lo ubica en carpeta temporal*/
                System.IO.File.Copy(_fileFormatServer, _filePath + ".pptx", true);

                PowerPoint.Application App = new PowerPoint.Application();
                PowerPoint.Presentation Pre = App.Presentations.Open(_filePath + ".pptx", MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);

                if (iCompania == 1646)
                {
                    //PowerPoint.Slide slidePortada = Pre.Slides[1];
                    //slidePortada.Shapes["CANAL_AJE"].TextFrame.TextRange.Text = textoCampania;
                }

                foreach (Fotografico oBj in oLs)
                {
                    PowerPoint.Slide Sli;
                    Sli = Pre.Slides.Add(Pre.Slides.Count + 1, PowerPoint.PpSlideLayout.ppLayoutPictureWithCaption);
                    PowerPoint.Shape Sha = Sli.Shapes[2];
                    
                    Sli.Shapes.AddPicture(/*ConfigurationManager.AppSettings["Rutafoto_reporte"] +*/ oBj.foto, MsoTriState.msoFalse, MsoTriState.msoTrue, Sha.Left, Sha.Top, Sha.Width, Sha.Height);
                    Sli.Shapes[1].TextFrame.TextRange.Text = oBj.pdv_razonsocial;
                    if (iCompania == 1646){
                        Sli.Shapes[3].TextFrame.TextRange.Text = "Cod. PDV: " + oBj.pdv_codigo + (char)13 + @"Dirección: " + oBj.pdv_direccion + " - " + oBj.prv_descripcion + @" \ " + oBj.dis_descripcion + (char)13 + @"Fecha\Hora: " + oBj.fecha + " " + oBj.hora;
                    }
                    else if (iCompania == 1637){
                        Sli.Shapes[3].TextFrame.TextRange.Text = "Cod. Cliente: " + oBj.TMPPdvCod + (char)13 + @"Nombre Cliente: " + oBj.TMPPdvDescripcion + (char)13 + @"Ciudad - Distrito: " + oBj.TMPCiudadDescripcion + (char)13 + @"Comentarios: " + oBj.comentario;
                    }else{
                        Sli.Shapes[3].TextFrame.TextRange.Text = "Cod. PDV: " + oBj.pdv_codigo + (char)13 + @"Dirección: " + oBj.pdv_direccion + " - " + oBj.prv_descripcion + @" \ " + oBj.dis_descripcion + (char)13 + @"Fecha\Hora: " + oBj.fecha + " " + oBj.hora;
                    }
                }

                if (extension == ".pdf")
                {
                    Pre.ExportAsFixedFormat(_filePath + ".pdf", PowerPoint.PpFixedFormatType.ppFixedFormatTypePDF, PowerPoint.PpFixedFormatIntent.ppFixedFormatIntentScreen);
                }

                Pre.Save();
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            return Json(new { Archivo = _fileServer, Error = error });
        }

        [HttpPost]
        public ActionResult GetCampaniaDescription(string __d)
        {
            List<E_Campania> oLstCampania = new Campania().Lista(new Obtener_Campania_por_idCampania_Request() { campania = __d });
            return new ContentResult
            {
                Content = MvcApplication._Serialize(
                    oLstCampania[0].Planning_Name.ToString()
                ),
                ContentType = "application/json"
            };
        }
    }
}
