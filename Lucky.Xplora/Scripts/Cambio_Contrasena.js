﻿$(function () {
    var $inputContraActual = $('#config-usuario-modal').find('input[name=c-actual]');
    var $inputContraNuevo1 = $('#config-usuario-modal').find('input[name=c-nuevo1]');
    var $inputContraNuevo2 = $('#config-usuario-modal').find('input[name=c-nuevo2]');
    var $instancia1 = false, $instancia2 = false;
    $inputContraActual.blur(function () {
        if ($inputContraActual.val().length != 0) {
            $.ajax({
                beforeSend: function (xhr) { }, 
                url: $ValidarContrasena_,
                type: 'POST',
                dataType: 'html',
                data: {
                    __a: $(this).val() 
                },
                success: function (response) {
                    if (response == 0) {
                        $inputContraActual.parents('div').eq(2).removeClass('has-success').removeClass('has-error').removeClass('has-warning').addClass('has-success').find('.tooltips').css('display', 'none').parents('div').eq(0).find('.fa-check').css('display', 'block');
                        $instancia1 = true;
                        if ($instancia2 == true) {
                            $('#GrabarNuevaContrasena').removeAttr('disabled');
                        }
                    }
                    if (response == 1) {
                        $inputContraActual.parents('div').eq(2).removeClass('has-success').removeClass('has-error').removeClass('has-warning').addClass('has-error').find('.tooltips').css('display', 'none').parents('div').eq(0).find('.fa-warning').css('display', 'block');
                        $('#GrabarNuevaContrasena').attr('disabled', 'disabled');
                        $instancia1 = false;
                    }
                    if (response == 2) {
                        $inputContraActual.parents('div').eq(2).removeClass('has-success').removeClass('has-error').removeClass('has-warning').addClass('has-error').find('.tooltips').css('display', 'none').parents('div').eq(0).find('.fa-exclamation').css('display', 'block');
                        $('#GrabarNuevaContrasena').attr('disabled', 'disabled');
                        $instancia1 = false;
                    }
                },
                complete: function () {

                },
                error: function (xhr) {
                    alert('Algo salió mal, por favor intente de nuevo.');
                }
            });
        } else {
            $inputContraActual.parents('div').eq(2).removeClass('has-success').removeClass('has-error').removeClass('has-warning').find('.tooltips').css('display', 'none').parents('div');
            $('#GrabarNuevaContrasena').attr('disabled', 'disabled');
        }
    })

    function verificar_cambio_contrasena() {
        var $c1 = $inputContraNuevo1.val();
        var $c2 = $inputContraNuevo2.val();

        var $validado = true, $numeral = "0123456789", $numeros = false, $mayuscula = false, $cont = 0, $simbolo="$!#$%&/()=?¡@", $simbol=false;

        if ($c1 != "" && $c2 != "") {
            //Verificación de espacio
            $cont = 0;
            while ($validado && ($cont < $c1.length)) {
                if ($c1.charAt($cont) == " ")
                    $validado = false;
                $cont++;
            }

            //Verificación de numeral
            $cont = 0;
            while ($validado && ($cont < $c1.length)) {
                if ($numeral.indexOf($c1.charAt($cont), 0) != -1)
                    $numeros = true;
                $cont++
            }
            $validado = $numeros;

            $cont = 0;
            //Verificación de mayuscula
            while ($validado && ($cont < $c1.length)) {
                if ($c1.charAt($cont) == $c1.charAt($cont).toUpperCase() && ($numeral.indexOf($c1.charAt($cont), 0) == -1))
                    $mayuscula = true;
                $cont++
            }
            $validado = $mayuscula;

            $cont = 0;
            //Verificación de simbolos
            while ($validado && ($cont < $c1.length)) {
                if ($simbolo.indexOf($c1.charAt($cont), 0) != -1)
                    $simbol = true;
                $cont++
            }
            $validado = $simbol;


            if ($c2 != $c1 || $c1.length < 6 || $validado == false) {
                //if ($c2 != $c1 || $c1.length < 6) {
                $inputContraNuevo1.parents('div').eq(2).removeClass('has-success').addClass('has-error').find('.tooltips').hide().parents('div').eq(0).find('.fa-warning').show();
                $inputContraNuevo2.parents('div').eq(2).removeClass('has-success').addClass('has-error').find('.tooltips').hide().parents('div').eq(0).find('.fa-warning').show();
                $('#GrabarNuevaContrasena').attr('disabled', 'disabled');
                $('form.form-horizontal').find("p.message-error").show();
                $instancia2 = false;
            } else {
                //Si todo esta ok
                $inputContraNuevo1.parents('div').eq(2).removeClass('has-error').addClass('has-success').find('.tooltips').hide().parents('div').eq(0).find('.fa-check').show();
                $inputContraNuevo2.parents('div').eq(2).removeClass('has-error').addClass('has-success').find('.tooltips').hide().parents('div').eq(0).find('.fa-check').show();
                $('form.form-horizontal').find("p.message-error").hide();
                $instancia2 = true;
                if ($instancia1 == true) {
                    $('#GrabarNuevaContrasena').removeAttr('disabled');
                }
            }
        } else {
            $inputContraNuevo1.parents('div').eq(2).removeClass('has-error').removeClass('has-success').find('.tooltips').hide();
            $inputContraNuevo2.parents('div').eq(2).removeClass('has-error').removeClass('has-success').find('.tooltips').hide();
            $('#GrabarNuevaContrasena').attr('disabled', 'disabled');
            $('form.form-horizontal').find("p.message-error").hide();
            $instancia2 = false;
        }
    }

    $('#config-usuario-modal').find('input[name=c-nuevo1],input[name=c-nuevo2]').blur(function () {
        verificar_cambio_contrasena();
    })

    $('#GrabarNuevaContrasena').click(function (e) {
        e.preventDefault();

        $.ajax({
            beforeSend: function (xhr) { },
            url: $GrabarNuevaContrasena_,
            type: 'POST',
            dataType: 'json',
            data: {
                __a: $('#config-usuario-modal').find('input[name=c-actual]').val(),
                __b: $('#config-usuario-modal').find('input[name=c-nuevo1]').val(),
                __c: $('#config-usuario-modal').find('input[name=c-nuevo2]').val()
            },
            success: function (response) {
                //$('#config-usuario-modal').find('div.message p').text(response.message);
                if (response.status == "SUCCESS") {
                    $('#config-usuario-modal').find('div.message').addClass('alert-success');
                } else {
                    $('#config-usuario-modal').find('div.message').addClass('alert-danger');
                }
                $('#config-usuario-modal').find('div.message p').text(response.message);
            },
            complete: function () {
                $('#config-usuario-modal').find('div.message').show(1000);
                setTimeout(function () {
                    $('#config-usuario-modal').find('div.message').hide(500);
                    $('#config-usuario-modal').find('input').val('');
                    $('#config-usuario-modal').find('div').removeClass('has-warning').removeClass('has-error').removeClass('has-success');
                    $('#config-usuario-modal').find('i.tooltips').hide();
                    setTimeout(function () { location.href = $LogOff_; }, 500)
                }, 4000)
            },
            error: function (xhr) {
                alert('Algo salió mal, por favor intente de nuevo.');
            }
        });

    })//GrabarNuevaContrasena

})