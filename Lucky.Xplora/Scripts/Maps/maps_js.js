﻿/* Inicio de Capa */
var _layer_Pais = null;
var _layer_Region = null;
function funct_cp_pais() {

    _layer_Pais = new ol.layer.Image({
        source: new ol.source.ImageVector({
            source: new ol.source.Vector({
                url: '/LuckyServiceMap/MapJson?__a=1023&__b=1',
                format: new ol.format.GeoJSON()
            }),
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#319FD3',
                    width: 1
                })
            })
        })
    });

    map.addLayer(_layer_Pais);

}
function funct_cp_region() {

    _layer_Region = new ol.layer.Image({
        source: new ol.source.ImageVector({
            source: new ol.source.Vector({
                url: '/LuckyServiceMap/MapJson?__a=1023&__b=6',
                format: new ol.format.GeoJSON()
            }),
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#319FD3',
                    width: 1
                })
            })
        })
    });

    map.addLayer(_layer_Region);
}

function funct_cp_distrito() {

    _layer_Region = new ol.layer.Image({
        source: new ol.source.ImageVector({
            source: new ol.source.Vector({
                url: '/LuckyServiceMap/MapJson?__a=1023&__b=4',
                format: new ol.format.GeoJSON()
            }),
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#319FD3',
                    width: 1
                })
            })
        })
    });

    map.addLayer(_layer_Region);
}

function funt_style_capa() {
    var featureOverlay = new ol.FeatureOverlay({
        map: map,
        style: new ol.style.Style({
            stroke: new ol.style.Stroke({ color: '#f00', width: 1 }),
            fill: new ol.style.Fill({ color: 'rgba(255,0,0,0.1)' })
        })
    });

    var highlight;
    var displayFeatureInfo = function (pixel) {

        var feature = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
            return feature;
        });

        var info = document.getElementById('info');
        if (feature) { info.innerHTML = feature.get('code') + ': ' + feature.get('name'); }
        else { info.innerHTML = '&nbsp;'; }

        if (feature !== highlight) {
            if (highlight) { featureOverlay.removeFeature(highlight); }
            if (feature) { featureOverlay.addFeature(feature); }
            highlight = feature;
        }
    };

    var displayjs = function (pixel) {
        var feature = map.forEachFeatureAtPixel(pixel, function (feature, layer) { return feature; });
        return { grupo: feature.get('group'), codigo: feature.get('code') };
    };

    map.on('pointermove', function (evt) {
        if (evt.dragging) { return; }
        var pixel = map.getEventPixel(evt.originalEvent);
        displayFeatureInfo(pixel);
    });

    map.on('click', function (evt) {
        displayFeatureInfo(evt.pixel);
        alert(displayjs(evt.pixel).grupo);
    });
}

$(function () {
    //funct_cp_distrito();

    //funt_style_capa();

});
/* Fin Capa */