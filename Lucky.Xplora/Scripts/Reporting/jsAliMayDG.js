﻿function funt_llenar_anio() {
    $.ajax({
        async: true,
        beforeSend: function (xhr) {
        },
        url: 'ConsulFiltro',
        type: 'POST',
        dataType: 'json',
        data: {
            _vparam: ""
            , _vop: 1
        },
        success: function (response) {
            $("#cbo_anio").empty();
            $.each(response, function (key, value) {
                $("#cbo_anio").append('<option value="' + value.Value + '" >' + value.Text + "</option>");
            });
            funt_llenar_mes();
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funt_llenar_mes() {
    $.ajax({
        async: true,
        beforeSend: function (xhr) {
        },
        url: 'ConsulFiltro',
        type: 'POST',
        dataType: 'json',
        data: {
            _vparam: ""
            , _vop: 3
        },
        success: function (response) {
            $("#cbo_mes").empty();
            $.each(response, function (key, value) {
                $("#cbo_mes").append('<option value="' + value.Value + '" >' + value.Text + "</option>");
            });
            funt_llenar_periodo();
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}
function funt_llenar_periodo() {
    $.ajax({
        async: true,
        beforeSend: function (xhr) {
        },
        url: 'ConsulFiltro',
        type: 'POST',
        dataType: 'json',
        data: {
            _vparam: $("#cbo_anio").val()+','+$("#cbo_mes").val()
            , _vop: 4
        },
        success: function (response) {
            $("#cbo_periodo").empty();
            $.each(response, function (key, value) {
                $("#cbo_periodo").append('<option value="' + value.Value + '" >' + value.Text + "</option>");
            });
            $("#cbo_periodo").trigger("change");
        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}

function funt_llenar_categoria() {
    $.ajax({
        async: true,
        beforeSend: function (xhr) {
        },
        url: 'ConsulFiltro',
        type: 'POST',
        dataType: 'json',
        data: {
            _vparam: ""
            , _vop: 2
        },
        success: function (response) {
            $("#cbo_categoria").empty();
            $.each(response, function (key, value) {
                $("#cbo_categoria").append('<option value="' + value.Value + '" >' + value.Text + "</option>");
            });

        },
        error: function (xhr) {
            alert('Algo salió mal, por favor intente de nuevo.');
        }
    });
}