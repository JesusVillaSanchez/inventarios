main()

wscript.Quit

sub main()
	dim oPowerPoint
	dim oPresentation
	dim oPath

	set oPowerPoint = CreateObject("PowerPoint.Application")
	set oPath = CreateObject("Scripting.FileSystemObject")

	Set oPresentation = oPowerPoint.Presentations.Open(oPath.GetParentFolderName(WScript.ScriptFullName) + "\PowerPoint", , , msoFalse)

	oPresentation.Export oPath.GetParentFolderName(WScript.ScriptFullName), "PNG"

	oPresentation.Close
	oPowerPoint.Quit
end sub

'option explicit

'dim oPowerPoint
'dim oPath

'main

'sub main()
	'set oPowerPoint = CreateObject("PowerPoint.Application")
	'set oPath = CreateObject("Scripting.FileSystemObject")
	
	'oPowerPoint.Presentations.Open oPath.GetParentFolderName(WScript.ScriptFullName) + "\PowerPoint"

	'oPowerPoint.ActivePresentation.Export oPath.GetParentFolderName(WScript.ScriptFullName), "PNG"
	
	'oPowerPoint.Quit
'end sub