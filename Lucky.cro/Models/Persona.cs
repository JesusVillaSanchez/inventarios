﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.cro.Models
{
    public class Persona : APersona
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public int Person_id { get; set; }

        [JsonProperty("b", NullValueHandling = NullValueHandling.Ignore)]
        public string id_typeDocument { get; set; }

        [JsonProperty("c", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_nd { get; set; }

        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Firtsname { get; set; }

        [JsonProperty("e", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_LastName { get; set; }

        [JsonProperty("f", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Surname { get; set; }

        [JsonProperty("g", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_SeconName { get; set; }

        [JsonProperty("h", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Email { get; set; }

        [JsonProperty("i", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Phone { get; set; }

        [JsonProperty("j", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_Addres { get; set; }

        [JsonProperty("k", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_Country { get; set; }

        [JsonProperty("l", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_dpto { get; set; }

        [JsonProperty("m", NullValueHandling = NullValueHandling.Ignore)]
        public string cod_city { get; set; }

        [JsonProperty("n", NullValueHandling = NullValueHandling.Ignore)]
        [Required(ErrorMessage="Ingrese un usuario", AllowEmptyStrings=false)]
        public string name_user { get; set; }

        [JsonProperty("o", NullValueHandling = NullValueHandling.Ignore)]
        [Required(ErrorMessage = "Ingrese una contraseña", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        public string User_Password { get; set; }

        [JsonProperty("p", NullValueHandling = NullValueHandling.Ignore)]
        public string Perfil_id { get; set; }

        [JsonProperty("q", NullValueHandling = NullValueHandling.Ignore)]
        public string Modulo_id { get; set; }

        [JsonProperty("r", NullValueHandling = NullValueHandling.Ignore)]
        public string User_Recall { get; set; }

        [JsonProperty("s", NullValueHandling = NullValueHandling.Ignore)]
        public int Company_id { get; set; }

        [JsonProperty("t", NullValueHandling = NullValueHandling.Ignore)]
        public bool Person_Status { get; set; }

        [JsonProperty("u", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_CreateBy { get; set; }

        [JsonProperty("v", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Person_DateBy { get; set; }

        [JsonProperty("w", NullValueHandling = NullValueHandling.Ignore)]
        public string Person_ModiBy { get; set; }

        [JsonProperty("x", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Person_DateModiBy { get; set; }

        [JsonProperty("y", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Person_ExpirationDate { get; set; }

        [JsonProperty("z", NullValueHandling = NullValueHandling.Ignore)]
        public bool Person_aceptoCLUF { get; set; }

        //-- estado de login
        [JsonProperty("aa", NullValueHandling = NullValueHandling.Ignore)]
        public bool existe { get; set; }

        [JsonProperty("ab", NullValueHandling = NullValueHandling.Ignore)]
        public string company_name { get; set; }
        //------------------

        [JsonProperty("ac", NullValueHandling = NullValueHandling.Ignore)]
        public string per_nombrecompleto { get; set; }

        [JsonProperty("ad")]
        public string Cod_Equipo { get; set; }

        [JsonProperty("ae")]
        public string Cod_Canal { get; set; }

        //-----------------------------------------
        // Tipo de perfil
        [JsonProperty("_af")]
        public int tpf_id { get; set; }

        [JsonProperty("_ag")]
        public string tpf_descripcion { get; set; }

        [JsonProperty("_ah")]
        public string nombre_equipo { get; set; }

        [JsonProperty("_ai")]
        public string nombre_canal { get; set; }
    }

    public abstract class APersona {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-04-10
        /// </summary>
        /// <param name="usuario">usuario</param>
        /// <param name="contrasenia">contraseña</param>
        /// <returns></returns>
        public Persona Login(Persona_Login_Request a)
        {
            var oRp = new Persona_Login_Response();

            a.key_encrypt = "YourUglyRandomKeyLike-lkj54923c478";

            oRp = MvcApplication._Deserialize<Persona_Login_Response>(
                MvcApplication._Servicio_Operativa.Valida_Login(MvcApplication._Serialize(a))
            );

            return oRp.Objeto;
        }

        public Person_Bloqueo Login_Bloqueo(Persona_Login_Request a)
        {
            var oRp = new Response_Person_Bloqueo();

            oRp = MvcApplication._Deserialize<Response_Person_Bloqueo>(
                MvcApplication._Servicio_Operativa.NW_Person_Bloqueo(MvcApplication._Serialize(a))
            );

            return oRp.Lista;
        }

        public Persona Login_new(Persona_Login_Request a)
        {
            var oRp = new Persona_Login_Response();

            oRp = MvcApplication._Deserialize<Persona_Login_Response>(
                MvcApplication._Servicio_Operativa.Person_Login_NW(MvcApplication._Serialize(a))
            );

            return oRp.Objeto;
        }
    }

    #region Request y Response

    #region¨Login - Persona, por name_user, user_password, key_encrypt

    /// <summary>
    /// Autor:  jlucero
    /// Fecha:  2014-04-10
    /// </summary>
    public class Persona_Login_Request
    {
        [JsonProperty("a")]
        public string name_user { get; set; }

        [JsonProperty("b")]
        public string user_password { get; set; }

        [JsonProperty("c")]
        public string key_encrypt { get; set; }
    }

    /// <summary>
    /// Autor:  jlucero
    /// Fecha:  2014-04-10
    /// </summary>
    public class Persona_Login_Response
    {
        [JsonProperty("a")]
        public Persona Objeto { get; set; }
    }

    #endregion

    #endregion
    public class Person_Bloqueo
    {
        [JsonProperty("a")]
        public int Estado { get; set; }
        [JsonProperty("b")]
        public string Comentario { get; set; }
    }
    public class Response_Person_Bloqueo
    {
        [JsonProperty("_a")]
        public Person_Bloqueo Lista { get; set; }
    }
}