﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft;
using Newtonsoft.Json;

namespace Lucky.cro.Models.Cro
{
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-10-05
    /// </summary>
    public class Cro : ACro
    {
        [JsonProperty("_a")]
        public int ope_id { get; set; }

        [JsonProperty("_b")]
        public string ope_nombre { get; set; }

        [JsonProperty("_c")]
        public string ope_usuario { get; set; }

        [JsonProperty("_d")]
        public int objetivo { get; set; }

        [JsonProperty("_e")]
        public int visita { get; set; }

        [JsonProperty("_f")]
        public decimal cumplimiento { get; set; }

        [JsonProperty("_g")]
        public List<Cro_Pdv> pdv { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-10-05
    /// </summary>
    public class Cro_Pdv
    {
        [JsonProperty("_a")]
        public int ope_id { get; set; }

        [JsonProperty("_b")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_c")]
        public string pdv_descripcion { get; set; }

        [JsonProperty("_d")]
        public string latitud { get; set; }

        [JsonProperty("_e")]
        public string longitud { get; set; }

        [JsonProperty("_f")]
        public string hora_inicio { get; set; }

        [JsonProperty("_g")]
        public string latitud_inicio { get; set; }

        [JsonProperty("_h")]
        public string longitud_inicio { get; set; }

        [JsonProperty("_i")]
        public string nvi_codigo { get; set; }

        [JsonProperty("_j")]
        public string nvi_descripcion { get; set; }

        [JsonProperty("_k")]
        public string hora_fin { get; set; }

        [JsonProperty("_l")]
        public string latitud_fin { get; set; }

        [JsonProperty("_m")]
        public string longitud_fin { get; set; }

        [JsonProperty("_n")]
        public List<Cro_Pdv_Reporte> reporte { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-10-05
    /// </summary>
    public class Cro_Pdv_Reporte
    {
        [JsonProperty("_a")]
        public int ope_id { get; set; }

        [JsonProperty("_b")]
        public string pdv_codigo { get; set; }

        [JsonProperty("_c")]
        public int rep_id { get; set; }

        [JsonProperty("_d")]
        public string rep_descripcion { get; set; }

        [JsonProperty("_e")]
        public string estado { get; set; }

        [JsonProperty("_f")]
        public int cantidad { get; set; }
    }

    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-10-05
    /// </summary>
    public abstract class ACro {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-10-05
        /// </summary>
        /// <param name="oRq"></param>
        /// <returns></returns>
        public List<Cro> Lista(Request_Cro_Filtro oRq)
        {
            Response_Cro oRp = MvcApplication._Deserialize<Response_Cro>(
                    MvcApplication._Servicio_Maps.Cro(
                        MvcApplication._Serialize(oRq)
                    )
                );

            return oRp.Lista;
        }
    }

    #region Request & Response
    /// <summary>
    /// Autor: jlucero
    /// Fecha: 2015-10-05
    /// </summary>
    public class Request_Cro_Filtro
    {
        [JsonProperty("_a")]
        public string campania { get; set; }

        [JsonProperty("_b")]
        public int supervisor { get; set; }

        [JsonProperty("_c")]
        public string fecha { get; set; }
    }
    public class Response_Cro
    {
        [JsonProperty("_a")]
        public List<Cro> Lista { get; set; }
    }
    #endregion
}