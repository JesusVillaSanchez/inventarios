﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Http;
//using System.Web.Mvc;
//using System.Web.Optimization;
//using System.Web.Routing;

//using Lucky.cro;
//using Lucky.cro.ServiceMaps;
//using Lucky.cro.ServiceCampania;
//using Lucky.cro.ServiceOperativa;

//using Newtonsoft;
//using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using Lucky.cro;
using Lucky.cro.ServiceMaps;
using Lucky.cro.ServiceCampania;
using Lucky.cro.ServiceOperativa;

using Newtonsoft;
using Newtonsoft.Json;


namespace Lucky.cro
{
    // Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
    // visite http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static Ges_MapsServiceClient _Servicio_Maps;
        public static Ges_CampaniaServiceClient _Servicio_Campania;
        public static Ges_OperativaServiceClient _Servicio_Operativa;

        protected void Application_Start()
        {
            BundleTable.EnableOptimizations = false;
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            _Servicio_Maps = new Ges_MapsServiceClient("BasicHttpBinding_IGes_MapsService");
            _Servicio_Campania = new Ges_CampaniaServiceClient("BasicHttpBinding_IGes_CampaniaService");
            _Servicio_Operativa = new Ges_OperativaServiceClient("BasicHttpBinding_IGes_OperativaService");
        }

        #region Utilitarios
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2014-12-01
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string _Serialize(object value)
        {
            return JsonConvert.SerializeObject(value, new JsonSerializerSettings() { MaxDepth = Int32.MaxValue });
        } 

        /// <summary>
        ///  Autor: jlucero
        /// Fecha: 2014-12-01
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T _Deserialize<T>(string value)
        {
            T obj = Activator.CreateInstance<T>();
            obj = JsonConvert.DeserializeObject<T>(value);
            return obj;
        }
        #endregion
    }
}