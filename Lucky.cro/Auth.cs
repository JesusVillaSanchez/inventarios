﻿using Lucky.cro.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Lucky.cro
{
    public static class Auth
    {
        private const int AuthenticationTicketVersion = 1;

        public static void SetAuthCookie(string user, AuthTicketData data, bool createPersistentCookie = false)
        {
            var now = DateTime.Now;

            var ticket = new FormsAuthenticationTicket(
                AuthenticationTicketVersion,
                user,
                now,
                now.AddMinutes(FormsAuthentication.Timeout.TotalMinutes),
                createPersistentCookie,
                JsonConvert.SerializeObject(data));

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket))
            {
                HttpOnly = true,
                Secure = FormsAuthentication.RequireSSL,
                Path = FormsAuthentication.FormsCookiePath,
                Domain = FormsAuthentication.CookieDomain,
            };

            HttpContext.Current.Response.SetCookie(cookie);
        }

        public static AuthTicketData GetTicketData()
        {
            return JsonConvert.DeserializeObject<AuthTicketData>(((FormsIdentity)HttpContext.Current.User.Identity).Ticket.UserData);
        }
    }

    public class AuthTicketData
    {
        public bool Admin { get; set; }
        public Persona userData { get; set; }
    }
}