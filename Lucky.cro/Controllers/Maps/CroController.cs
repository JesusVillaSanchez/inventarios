﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lucky.cro;
using Lucky.cro.Models;
using Lucky.cro.Models.Cro;

namespace Lucky.cro.Controllers.Maps
{
    public class CroController : Controller
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-10-05
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Inicio()
        {
            ViewBag.nom_equipo = Auth.GetTicketData().userData.nombre_equipo;
            ViewBag.nom_canal = Auth.GetTicketData().userData.nombre_canal;
            ViewBag.nombre_usuario = Auth.GetTicketData().userData.Person_Firtsname;
            return View();
        }

        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-10-05
        /// </summary>
        /// <param name="__a"></param>
        /// <param name="__b"></param>
        /// <param name="__c"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Cro(string __a)
        {
            string cod_equipo = Auth.GetTicketData().userData.Cod_Equipo;
            int person_id = Auth.GetTicketData().userData.Person_id;
            return Content(new ContentResult
            {
                Content = MvcApplication._Serialize(
                    new Cro().Lista(
                        new Request_Cro_Filtro()
                        {
                            campania = cod_equipo,//"813622482010",
                            supervisor = person_id,//12294,
                            fecha = Convert.ToDateTime(__a).ToString("yyyy-MM-dd")
                        }
                    )
                ),
                ContentType = "application/json"
            }.Content);
        }
    }
}