﻿using Lucky.cro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Lucky.cro.Controllers
{
    public class InicioController : Controller
    {
        /// <summary>
        /// Autor: jlucero
        /// Fecha: 2015-10-05
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return View();
        }

        [HttpPost]
        public ActionResult Login(Persona persona)
        {
            //return RedirectToAction("Inicio");
            string msg = "";
            try
            {
                Persona person = new Persona().Login(
                        new Persona_Login_Request()
                        {
                            name_user = persona.name_user.Trim(),
                            user_password = persona.User_Password.Trim(),
                            key_encrypt = ""
                        }
                    );
                if (person.name_user != null && person.name_user.ToUpper() == persona.name_user.ToUpper())
                {
                    Auth.SetAuthCookie(person.Person_Firtsname.Trim(), new AuthTicketData
                    {
                        Admin = true,
                        userData = person
                    });
                    return Redirect("~/");
                }
                else
                {
                    ViewBag.Message = "Usuario o password incorrectos.";
                    //msg = "Usuario o password incorrectos.";
                }
            }
            catch
            {
                ViewBag.Message = "Usuario o password incorrectos.";
            }
            return View();
        }
    }
}